<style>
	input[type="number"] {
		max-width: 100px;
		min-width: 100px;
	}
	input[type="text"] {
		max-width: 200px;
		min-width: 200px;
	}
	.xtable-responsive {
    width: 100%;
    margin-bottom: 15px;
    overflow-y: hidden;
    overflow-x: scroll;
    -ms-overflow-style: -ms-autohiding-scrollbar;
    border: 1px solid #DDD;
    -webkit-overflow-scrolling: touch;
}
</style>
<?php echo panel_head2('<strong>Grading Table</strong>','','default') ?>
	<?if(isset($grading_system) && $grading_system):?>
		<div class="xtable-responsive">
			<?php echo form_open(''); ?>
				<input type="hidden" name="token" value="<?php echo $form_token ?>">
				<table class="table">
					<thead>
						<tr class='bold'>
							<td width="10%">From</td>
							<td width="10%">To</td>
							<td width="30%">Value</td>
							<td>Description</td>
							<td>Action</td>
						</tr>
					</thead>
					<tbody>
						<?foreach ($grading_system as $k => $v):?>
							<tr>
								<td><input type="number" name="gs[<?=$v->id?>][range_start]" class='form-control grid' min='0' value='<?=$v->range_start?>' size = "20" required></td>
								<td><input type="number" name="gs[<?=$v->id?>][range_end]" class='form-control grid' min='0' value='<?=$v->range_end?>' size = "20" required></td>
								<td><input type="text" name="gs[<?=$v->id?>][value]" value='<?=$v->value?>' class='grid' required></td>
								<td><input type="text" name="gs[<?=$v->id?>][desc]" value='<?=$v->desc?>' class='grid' id=""></td>
								<td><a href="<?=site_url('grading_system/delete/'.__link($v->id))?>" class="btn btn-danger btn-xs confirm">Delete</a></td>
							</tr>
						<?endforeach;?>
					</tbody>
				</table>
				<input type="submit" value="Update Table" name='update' class='btn btn-primary'>
			<?php echo form_close(); ?>
		</div>
	<?endif;?>
<?php echo panel_tail2(); ?>

<?php echo panel_head2('<strong>Create Record</strong>','','default') ?>
	<div class="xtable-responsive">
		<?php echo form_open(''); ?>
			<input type="hidden" name="token" value="<?php echo $form_token ?>">
			<table class="table table-bordered table-condensed">
				<tr>
					<td><label for="">Range From</label><input type="number" name="grading_system[range_start]" value="<?php echo set_value('grading_system[range_start]') ?>" class='form-control' min='0' required></td>
					<td><label for="">Range To</label><input type="number" name="grading_system[range_end]" value="<?php echo set_value('grading_system[range_end]') ?>" class='form-control' min='0' required></td>
					<td><label for="">Value</label><input type="text" name="grading_system[value]" value="<?php echo set_value('grading_system[value]') ?>" required></td>
					<td><label for="">Description</label><input type="text" name="grading_system[desc]" value="<?php echo set_value('grading_system[desc]') ?>"></td>
				</tr>
			</table>
			<input type="submit" value="Save" name='create' class='btn btn-primary'>
		<?php echo form_close(); ?>
	</div>
<?php echo panel_tail2(); ?>