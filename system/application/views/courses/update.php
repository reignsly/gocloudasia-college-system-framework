<?=form_open('',' class="form-horizontal" ')?>
	
	<? @$this->load->view('courses/_form'); ?>
	<?php echo form_hidden('id' ,isset($courses)&&$courses?$courses->id:0) ?>
	<!-- Button (Double) -->
	<div class="form-group">
	  <label class="col-md-2 control-label" for="button1id">Action</label>
	  <div class="col-md-8">
	    <button name="update_course" value="update_course" id="update_course" class="btn btn-primary btn-sm">Update Course</button>
	    <a href="<?=site_url('courses')?>" class="btn btn-default btn-sm">Cancel</a>
	  </div>
	</div>
<?=form_close()?>