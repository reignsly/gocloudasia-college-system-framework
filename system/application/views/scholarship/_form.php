<script type="text/javascript">
	
	$(document).ready(function(){

		$('.type_radio').change(function(event){

			var xval = $(this).val();
			
			if(xval == "CASH"){
				$('#div_cash').show('slow');
				$('#div_percent').hide();
			}else{
				$('#div_percent').show('slow');
				$('#div_cash').hide();
			}

		})

	})

  function validate()
  {
  	var xvalid = true;

		$('input[type="text"]').each(function(){
			if($(this).attr('required')){
				
				if($(this).val().trim() == ""){

					xvalid = false;

					return false;

				}				

			}	
		});

		if(xvalid == false){
			
			custom_modal('Required','Please fill up required fields');
			return xvalid = false;
		}else{
			if($('#type_cash').is(':checked')){
				var xcash = $('#cash_amount').val().trim();

				if(isNumber(xcash) == false){
					custom_modal('Error','Cash Amount must be valid');
					return xvalid = false;
				}else{
					if(parseFloat(xcash) <= 0){
						custom_modal('Error','Cash Amount must be valid');
						return xvalid = false;
					}
				}
			}else{
				var xpercentage = $('#percentage').val().trim();

				if(isNumber(xpercentage) == false){
					custom_modal('Error','Percentage must be valid');
					return xvalid = false;	
				}else{
					if(parseFloat(xpercentage) <= 0){
						custom_modal('Error','Percentage must be valid');
						return xvalid = false;
					}
				}

				if($('#div_percent input[type="checkbox"]:checked').length == 0){
					custom_modal('Error','You must select where the percentage will be applied to.');
						return xvalid = false;
				}
			}
		}

		
		return xvalid;
  }
</script>
	
<fieldset>

<?if(isset($scholarship) && $scholarship):?>

<?$s = $scholarship;?>

<!-- Prepended text-->
<div class="form-group">
  <div class="col-md-8">
    <div class="input-group">
      <span class="input-group-addon">Scholarship Name</span>
      <input id="name" name="name" class="form-control" placeholder="Name" value="<?=set_value('name',$s->name)?>" type="text" required>
    </div>
    <!-- <p class="help-block">help</p> -->
  </div>
</div>

<div class="form-group">
  <div class="col-md-8">
    <div class="input-group">
      <span class="input-group-addon">Description</span>
      <input id="desc" name="desc" class="form-control" placeholder="Scholarship Description" value="<?=set_value('desc',$s->desc)?>" type="text" required>
    </div>
    <!-- <p class="help-block">help</p> -->
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="radios">Type of Deduction</label>
  <div class="col-md-4"> 
    <span class=''>
      <input id="type_cash" type="radio" value="CASH" class="type_radio" name="type" <?=$s->type=="CASH" ? "checked" : ''?> > CASH
    </span>
    <span class=''>
      <input id="type_percentage" type="radio" value="PERCENTAGE" class="type_radio" name="type"  <?=$s->type=="PERCENTAGE" ? "checked" : ''?>> PERCENTAGE
    </span>
  </div>
</div>

 <div class='alert alert-info' <?=$s->type=="CASH" ? "" : "style='display:none'"?> id='div_cash'>

    <div class="form-group">
      <div class="col-md-8">
        <div class="input-group">
          <span class="input-group-addon">Cash Amount</span>
          <input id="cash_amount" name="cash_amount" class="form-control currency" placeholder="Amount" value="<?=set_value('cash_amount', $s->cash_amount)?>" type="text">
        </div>
        <p class="help-block">If type is Cash.</p>
      </div>
    </div>

  </div>

  <div class='alert alert-success' <?=$s->type=="PERCENTAGE" ? "" : "style='display:none'"?> id='div_percent'>

    <div class="form-group">
      <div class="col-md-8">
        <div class="input-group">
          <span class="input-group-addon">Percentage Value</span>
          <input id="percentage" name="percentage" class="form-control currency" placeholder="Percentage" value="<?=set_value('percentage',$s->percentage)?>" type="text">
        </div>
        <p class="help-block">If type is Percentage.</p>
      </div>
    </div>

  <!-- Multiple Checkboxes -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="checkboxes">Percentage will Apply to?</label>
      <div class="col-md-4">
      <div class="checkbox">
        <label for="checkboxes-0">
          <input id="per_tuition" type="checkbox" class="" name="applied_to[]" value="TUITION" <?=(strpos($s->applied_to,'TUITION') !== false) ? 'checked' : '' ?> />
          Tuition
        </label>
      </div>
      <div class="checkbox">
        <label for="checkboxes-1">
          <input id="per_misc" type="checkbox" class="" name="applied_to[]" value="MISC" <?=(strpos($s->applied_to,'MISC') !== false) ? 'checked' : '' ?> />
          Miscellaneous
        </label>
      </div>
      </div>
    </div>

  </div>

</fieldset>

<?else:?>
<!-- Prepended text-->
<div class="form-group">
  <div class="col-md-8">
    <div class="input-group">
      <span class="input-group-addon">Scholarship Name</span>
      <input id="name" name="name" class="form-control" placeholder="Name" value="<?=set_value('name')?>" type="text" required>
    </div>
    <!-- <p class="help-block">help</p> -->
  </div>
</div>

<div class="form-group">
  <div class="col-md-8">
    <div class="input-group">
      <span class="input-group-addon">Description</span>
      <input id="desc" name="desc" class="form-control" placeholder="Scholarship Description" value="<?=set_value('desc')?>" type="text" required>
    </div>
    <!-- <p class="help-block">help</p> -->
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="radios">Type of Deduction</label>
  <div class="col-md-4"> 
    <span class=''>
      <input id="type_cash" type="radio" class="type_radio" name="type" value="CASH" checked > CASH
    </span>
    <span class=''>
      <input id="type_percentage" type="radio" class="type_radio" name="type" value="PERCENTAGE"> PERCENTAGE
    </span>
  </div>
</div>

 <div class='alert alert-info' id='div_cash'>

    <div class="form-group">
      <div class="col-md-8">
        <div class="input-group">
          <span class="input-group-addon">Cash Amount</span>
          <input id="cash_amount" name="cash_amount" class="form-control currency" placeholder="Amount" value="<?=set_value('cash_amount')?>" type="text">
        </div>
        <p class="help-block">If type is Cash.</p>
      </div>
    </div>

  </div>

  <div class='alert alert-success' style='display:none' id='div_percent'>

    <div class="form-group">
      <div class="col-md-8">
        <div class="input-group">
          <span class="input-group-addon">Percentage Value</span>
          <input id="percentage" name="percentage" class="form-control currency" placeholder="Percentage" value="<?=set_value('percentage')?>" type="text">
        </div>
        <p class="help-block">If type is Percentage.</p>
      </div>
    </div>

  <!-- Multiple Checkboxes -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="checkboxes">Percentage will Apply to?</label>
      <div class="col-md-4">
      <div class="checkbox">
        <label for="checkboxes-0">
          <input id="per_tuition" type="checkbox" class="" name="applied_to[]" value="TUITION" />
          Tuition
        </label>
      </div>
      <div class="checkbox">
        <label for="checkboxes-1">
          <input id="per_misc" type="checkbox" class="" name="applied_to[]" value="MISC" />
          Miscellaneous
        </label>
      </div>
      </div>
    </div>

  </div>

</fieldset>

<?endif?>