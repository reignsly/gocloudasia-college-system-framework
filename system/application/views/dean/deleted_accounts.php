<div id="right">
	<?php 
		$formAttrib = array('id' => 'search_enrollee', 'class' => 'registrar-form', 'method' => 'POST');
		
		$lastNameAttrib = array('name' => 'lastname', 'id' => 'lastname');
		$firstNameAttrib = array('name' => 'firstname', 'id' => 'firstname');
		$idnoAttrib = array('name' => 'idno', 'id' => 'idno');
		echo form_open('dean/deleted_accounts', $formAttrib);
		
	?>
    
    <p>
	Last Name: <?php echo form_input($lastNameAttrib); ?>
    </p>
    <p>
	First Name: <?php echo form_input($firstNameAttrib); ?>
    </p>
    <p>
	Student ID: <?php echo form_input($idnoAttrib); ?>
    </p>
    <p>
	<?php
	echo form_hidden('semester_id_eq', $this->open_semester->id);
	echo form_hidden('sy_from_eq', $this->open_semester->year_from);
	echo form_hidden('sy_to_eq', $this->open_semester->year_to);
	?>
   		<?php echo form_submit('search_deleted_user', 'Submit'); ?>
	</p>
	<?php
	if(isset($search)){
	?>
	<table>
		<tr>
		  <th>ID No.</th>
		</tr>
		<?php
		if(!empty($search))
		{
			foreach($search as $student):
			?>
			<tr class="profile_box">
				<td>
				  <?php echo ucfirst($student->last_name).', '.ucfirst($student->first_name); ?><br />
				  <?php echo $student->studid; ?><br />
				  <?php echo $student->year." Year | ".$student->name." Semester"; ?><br />
				  <?php echo $student->course; ?>
				</td>
				<td>
				<div id="action_links2">
				<ul>
				<li>
					<ul class="actionlink">
					<li>
						<a href="<?php echo site_url('dean/restore_enrollment').'/'.$student->id; ?>" class="confirm-auto" title="Are you sure you want to restore <?=ucfirst($student->first_name);?> ?">Restore</a>
					</li>
					</ul>
				</li>
				</ul>
				</div>
				</td>
			</tr>
			<?php 
			endforeach; 
		}
		else
		{
		?>
		<tr>
		<td>
			Student Not Found
		</td>
		</tr>
		<?php
		}
		?>
	</table>
	<?php
	}
	?>
	</div>
	
	<div class="clear"></div>
</div>
