<?php
$count = 0;
/* #################################### end PHP Config File #####################################*/
?>

<div id="right">
	<div id="right_top" >
		<div id="form" style="margin:2px;">
			<form action="<?=site_url('dean/master_list');?>" method="POST">
				<span class="uniq">
					<p>Course</p>
					<?=course_dropdown('course',$course_id);?>
				</span>
				<span class="uniq">
					<p>Year</p>
					<?$years = set_value('years') == false ? NULL : set_value('years');?>
					<?=year_dropdown('years',$years);?>
				</span>
				<span class="uniq">
					<p>&nbsp;</p>
					<input type="submit" name="masterlist" value="Search" >
				</span>
			</form>
		</div>
	
	
		<div class="clear" style="margin-bottom:10px;"></div>
	
	
		<table>
			<tr>
				<th>#</th>
				<th>Fullname</th>
				<th>Action</th>
			</tr>

			<?php if(empty($search_results) == FALSE):?>
				<? foreach($search_results as $s): ?>
					<tr>
						<td style="width:35px;"><?=$count+1;?></td>
						<td><?=strtoupper($s->last_name.' , '.$s->first_name.'  '.$s->middle_name);?></td>
						<td><a href="<?=site_url('profile/view').'/'.$s->enrollment_id;?>">view profile</a></td>
					</tr>
				<?
					++$count;
					endforeach;
				?>
			<?php else:?>
				<tr>
					<td>&nbsp;</td>
					<td>----NO data to show---</td>
					<td>----NO data to show---</td>
				</tr>	
			<?php endif;?>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>	
				<tfoot>
					<th>&nbsp;</th>
					<th>TOTAL:</th>
					<th><? echo $count > 0 ? $count : 0;?></th>
				</tfoot>
		</table>
	
	</div>
</div>
