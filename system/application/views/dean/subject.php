
<div id="right">

	<div id="right_top" ><p id="right_title">Subjects</p></div>
	
	<div id="right_bottom">
	
		<a href="/subjects/new" class="confirm" title="Add a New Subject?">New Subject</a><br />

		<form action="<?=site_url('subjects');?>" method="POST">
			<p>
				Subject Code: <input id="search_sc_id_eq" name="sc_id" size="30" type="text" value="<?=set_value('sc_id');?>"/>
			</p>
			<p>
				Section Code: <input id="search_code_eq" name="code" size="30" type="text" value="<?=set_value('code');?>"/>
			</p>
			<p>
				Description: <input id="search_subject_eq" name="subject" size="30" type="text" value="<?=set_value('subject');?>"/>
			</p>
			
			<input id="search_semester_id_eq" name="semester_id" type="hidden" value="<?=$this->open_semester->id;?>" />
			<input id="search_year_from_eq" name="year_from" type="hidden" value="<?=$this->open_semester->year_from;?>" />
			<input id="search_year_to_eq" name="year_to" type="hidden" value="<?=$this->open_semester->year_to;?>" />
		  
			<p>
				<input id="search_submit" name="search_subjects" type="submit" value="Submit" />
			</p>
		</form>
<table id="table">
  <tr>
    <th>Subject Code</th>
    <th>Section Code</th>
    <th>Description</th>
    <th>Unit</th>
    <th>Lec</th>
    <th>Lab</th>
    <th>Time</th>
    <th>Day</th>
    <th>Room</th>
    <th>Remaining Slots</th>
    <th>Instructor</th>
    <th>Academic Year</th>
    <th>Action</th>
  </tr>
  
    
		<?php if(!empty($subjects)):?>
			<?php foreach($subjects as $s):?>
				<tr>
					 <td><?php echo $s->sc_id;?></td>
					  <td><?php echo $s->code;?></td>
					  <td><?php echo $s->subject;?></td>
					  <td><?php echo $s->units;?></td>
					  <td><?php echo $s->lec;?></td>
					  <td><?php echo $s->lab;?></td>
					  <td><?php echo $s->time;?></td>
					  <td><?php echo $s->day;?></td>
					  <td><?php echo $s->room;?></td>
					  <td><?php echo $s->subject_load.'/'.$s->original_load;?></td>
					  <td> ------------</td>
					  <td><?php echo $s->year_from.' - '.$s->year_to;?></td>
					  <td>
					   <div id="action_links">
					   <ul>
							<li>
								<ul class="actionlink">
									<li><a href="#">View Class List</a></li>
									<li><a href="<?=site_url('dean/edit_subject').'/'.$s->id;?>" class="confirm" title="Edit subject <?=$s->subject;?>?">Edit</a></li>
									<li><a href="#" class="confirm" title="destroy subject <?=$s->subject;?>?">Destroy</a></li>
								</ul>
							</li>
					   </ul>
					   </div>
					</td>
				</tr>
			<?php endforeach;?>
		<?php endif;?>

  

</table>		
		

	</div>
	
</div>

