<div id="right">

	<div id="right_top" >
		  <p id="right_title">Master List By Course</p>
	</div>
	
	<div id="right_bottom">
		<table>
		  <tr>
			<th>Course</th>
			<th>Number Of Enrollees</th>
		  </tr>

		  <?php if(!empty($masterlist)):?>
				<?php foreach($masterlist as $m):?>
				<tr>
					<td><?=$m->course;?></td>
					<td><?=$m->total_studs;?> <a href="<?=site_url('dean/master_list/').'/'.$m->id;?>">view</a></td>
				</tr>
				<?$total[] = $m->total_studs;?>
				<?php endforeach;?>
		  <?php else:?>
			  <tr>
					<td>----Unable to Fetch data---</td>
					<td>----Unable to Fetch data---</td>
			  </tr>
		 <?php endif;?>
				<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		 <?php if(!empty($unassigned)):?>
				<tr>
					<td>Unassigned</td>
					<td><?=$unassigned->unassigned;?></td>
				</tr>
				<?$total[] = $unassigned->unassigned;?>
		  <?php else:?>
			  <tr>
					<td>----Unable to Fetch data---</td>
					<td>----Unable to Fetch data---</td>
			  </tr>
		 <?php endif;?>
		 
		 	<tfoot>
				<th>Total</th>
				<th><?=array_sum($total);?></th>
		  </tfoot>
		</table>
	</div>
	
	<div class="clear"></div>

</div>
