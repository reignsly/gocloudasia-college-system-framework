<div class="well">
	<div class="alert alert-danger">
		<p>Note : This will drop all the tables and re-create them.</p>
		<p>This eature should only be used after a fresh install and the database is merely blank.</p>
		<p>Please don't used this feature when you are not told to do so.</p>
		<p>This may take a while so please don't refresh, change page or power off your unit while the system will run this process.</p>
	</div>
	<a href="<?=site_url('cp_default_db/create_def_structure/generate')?>" class="btn btn-facebook btn-sm"> Generate</a>
</div>