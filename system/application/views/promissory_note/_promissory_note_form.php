<?php
$valueAttrib = array('name' => 'value', 'id' => 'value', 'size' => '8');
$dateAttrib = array('name' => 'date', 'id' => 'date', 'class' => 'datepicker');
echo form_hidden('enrollment_id', $enrollment_id);
?>
  <p>
	<label for="promissory_note_value">Value</label><br />
    <?php echo form_input($valueAttrib); ?>
  </p>
  <p>
    <label for="promissory_note_date">Date</label><br />
    <?php echo form_input($dateAttrib); ?>
  </p>
