<div class="table-responsive">

	<table class="paginated table table-condensed table-bordered table-sortable" no-sort-icon = "1">
		<thead>
	  <tr>
	    <th>Course No.</th>
	    <th>Description</th>
	    <th>Unit</th>
	    <th>Lec</th>
	    <th>Lab</th>
	    <th>Select</th>
	  </tr>
	  </thead>
	  <tbody>
			<?php if(!empty($results)):?>
				<?php foreach($results as $s):?>
					<tr>
						  <td><strong><?php echo $s->code;?></strong></td>
						  <td><strong><?php echo $s->subject;?></strong></td>
						  <td><?php echo $s->units;?></td>
						  <td><?php echo $s->lec;?></td>
						  <td><?php echo $s->lab;?></td>
						  <td><button type="button" class="btn btn-xs btn-primary select-subject" ref_id="<?php echo $s->ref_id ?>" >Select</button></td>
						</td>
					</tr>
				<?php endforeach;?>
			<?php else:?>
				<tr><td colspan="5" > No Subject found.</td></tr>
			<?php endif;?>
		</tbody> 
	</table>	
</div>

