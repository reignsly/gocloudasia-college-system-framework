<?php
	$category= array(
              'name'        => 'coursefinances[category]',
              'value'       => set_value('coursefinances[category]',isset($coursefinances->category) ? $coursefinances->category : ''),
              'maxlength'   => '100',
              'required'    => ''
            );
	$code= array(
              'name'        => 'coursefinances[code]',
              'value'       => set_value('coursefinances[code]',isset($coursefinances->code) ? $coursefinances->code : ''),
              'maxlength'   => '100',
              'required'    => ''
            );
?>
<div class="row">
  <div class="col-md-2"><label for="category">Category</label> </div>
  <div class="col-md-10"><?php echo form_input($category);?></div>
</div>
<br/>

<div class="row">
  <div class="col-md-2"><label for="code">Code</label></div>
  <div class="col-md-10"><?php echo form_input($code);?></div>
</div>

