<?echo form_open('');?>
	<?$this->load->view('coursefinances/_form')?>
	<?$this->load->view('coursefinances/fees_list')?>
	<div class="btn-group">
		<label for="mySubmit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp; Save Changes</label>
		<input id="mySubmit" type="submit" value="Save Changes" name="" class="hidden" />
		<a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>coursefinances" rel="facebox" class="actionlink"><i class="fa fa-arrow-left"></i>&nbsp; Back to list of Course Finance</a>
	</div>
<?echo form_close();?>