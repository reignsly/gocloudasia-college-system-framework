<?$this->load->view('coursefinances/_tab');?>
<a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>coursefinances/assign_course/<?=$id?>" rel="facebox" class='btn btn-default btn-sm btn-sm' ><span class = 'glyphicon glyphicon-plus'></span>&nbsp;  Assign Course</a>
<br/>
<br/>
<div class="table-responsive">
<table id="table" class="table table-sortable">
  <thead>
  	<tr>
      <th>Year</th>
      <th>Semester</th>
      <th>Course</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  	
    <?php if($assign_coursefees):?>
    <?php foreach( $assign_coursefees as $value): ?>
      <tr>
        <td class="bold"><?php echo $value->year ; ?></td>
        <td class="bold"><?php echo $value->semester ; ?></td>
        <td class="bold"><?php echo $value->course; ?>(<?php echo $value->course_code; ?>)</td>
        <td>
    		  <a class="btn btn-xs btn-danger confirm" href="<?php echo base_url()."coursefinances/destroy_assign_course/".$value->id."/".$id; ?>"><span class='glyphicon glyphicon-remove' ></span>&nbsp; Destroy</a>
    		</td>
      </tr>
    <?php endforeach; ?>
    <?else:?>
      <tr>
        <td colspan="3">No Record Available</td>
      </tr>
    <?endif;?>
    
  </tbody>
</table>
</div>
<a class="btn btn-default btn-sm" href="<?php echo base_url(); ?>coursefinances" rel="facebox" class='btn btn-default btn-sm btn-sm' ><span class='glyphicon glyphicon-backward'></span>&nbsp;  Back to list</a>