<?php 
	$formAttrib = array('id' => 'search_enrollee', 'class' => 'form-inline', 'method' => 'GET');
	echo form_open('coursefinances/index/0', $formAttrib);
?>

	<div class="form-group">
		<input id="category2" class="form-control" type="text" value="<?=isset($category2)?$category2:''?>" name="category2" placeholder="Name">
	</div>

	<div class="form-group">
		<input id="category" class="form-control" type="text" value="<?=isset($category)?$category:''?>" name="category" placeholder="Category">
	</div>

	<div class="form-group">
		<input id="code" class="form-control" type="text" value="<?=isset($code)?$code:''?>" name="code" placeholder="Code">
	</div>

	<div class="form-group">
		<?php echo form_submit('submit', 'Search','class="btn btn-sm btn-success"'); ?>
	</div>

	<div class="form-group">
		<a class='btn btn-primary btn-sm' href="<?php echo base_url(); ?>coursefinances/create" rel="facebox"><span class = 'glyphicon glyphicon-plus'></span>&nbsp;  Create New Course Fee</a>
	</div>

<?echo form_close();?>
<p></p>
<div class="table-responsive">
	<table class="table table-condensed table-bordered table-sortable">
		<thead>
			<tr>
				<th>Code</th>
				<th>Category</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?if($coursefinances):?>
				<?foreach ($coursefinances as $k => $coursefinance):?>
					<tr>
					  <td><?php echo $coursefinance->code ; ?></td>	
					  <td><?php echo $coursefinance->category ; ?></td>
					
					  <td>
					  	<div class="btn-group btn-group-sm">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-caret-square-o-down"></i> <span class="caret"></span>
								</button>
								<ul class="dropdown-menu pull-right" role="menu">
									<li><a href="<?php echo base_url()."coursefinances/create_fees/".$coursefinance->id; ?>" rel="facebox" class="actionlink">Add Fees</a></li>
								  <li><a href="<?php echo base_url()."coursefinances/edit_fees/".$coursefinance->id; ?>" rel="facebox" class="actionlink">Edit Fees Values</a></li>
								  <li><a href="<?php echo base_url()."coursefinances/edit/".$coursefinance->id; ?>" rel="facebox" class="actionlink">Edit Course</a></li>
								  <li><a href="<?php echo base_url()."coursefinances/view_assigned_course/".$coursefinance->id; ?>" rel="facebox" class="actionlink">View Assigned Course</a></li>
								  <li><a href="<?php echo base_url()."coursefinances/destroy/".$coursefinance->id; ?>" rel="facebox" class="actionlink confirm">Destroy</a></li>
								</ul>
							</div>
					  </td>
					</tr>
				<?endforeach;?>
			<?else:?>
				<tr>
					<td colspan="3">No Record found.</td>
				</tr>
			<?endif;?>
		</tbody>
	</table>
</div>
<?echo isset($links) ? $links : NULL;?> 