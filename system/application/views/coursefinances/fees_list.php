<table class="paginated">
  <?php
	if($fees_list){
		$tuition_fee = array();
		$misc_fee = array();
		$other_fee = array();
		$lab_fee = array();
		$nstp_fee = array();
		$other_school_fee = array();

		foreach ($fees_list as $key => $value) {
			if($value->is_tuition_fee == 1){
				$tuition_fee[] = $value;
			}

			if($value->is_misc == 1){
				$misc_fee[] = $value;
			}

			if($value->is_other == 1){
				$other_fee[] = $value;
			}

			if($value->is_lab == 1){
				$lab_fee[] = $value;
			}

			if($value->is_nstp == 1){
				$nstp_fee[] = $value;
			}

			if($value->is_other_school == 1){
				$other_school_fee[] = $value;
			}
		}
?>
	<?if(count($tuition_fee) > 0):?>
	<tr><th colspan='11' style='text-align:center;'>Tuition Fee (Select One)</th><tr>
	<?foreach ($tuition_fee as $key => $value):?>
		<tr>
			<td class="bold"><input type='radio' name='tuition_fee' value='<?=$value->id;?>' />&nbsp;&nbsp;<?=$value->name;?></td>
			<td><span class="badge badge-success"><?=m($value->value);?></span></td>
		</tr>
	<?endforeach;?>
	<?endif;?>

	<?if(count($lab_fee) > 0):?>
	<tr><th colspan='11' style='text-align:center;'>Lab Fees</th><tr>
	<?foreach ($lab_fee as $key => $value): ?>
		<tr>
			<td class="bold"><input type='checkbox' name='fees[]' value='<?=$value->id;?>' /> &nbsp;&nbsp;<?=$value->name;?></td>
			<td><span class="badge badge-success"><?=m($value->value);?></span></td>
		</tr>
	<?endforeach;?>
	<?endif;?>

	<?if(count($misc_fee) > 0):?>
	<tr><th colspan='11' style='text-align:center;'>Miscellaneous Fees</th><tr>
	<?foreach ($misc_fee as $key => $value):?>
		<tr>
			<td class="bold"><input type='checkbox' name='fees[]' value='<?=$value->id;?>' /> &nbsp;&nbsp;<?=$value->name;?></td>
			<td><span class="badge badge-success"><?=m($value->value);?></span></td>
		</tr>
	<?endforeach;?>
	<?endif;?>

	<?if(count($other_school_fee) > 0):?>
	<tr><th colspan='11' style='text-align:center;'>Other School Fees</th><tr>
	<?foreach ($other_school_fee as $key => $value):?>
		<tr>
			<td class="bold"><input type='checkbox' name='fees[]' value='<?=$value->id;?>' />&nbsp;&nbsp;<?=$value->name;?></td>
			<td><span class="badge badge-success"><?=m($value->value);?></span></td>
		</tr>
	<?endforeach;?>
	<?endif;?>

	<?if(count($other_fee) > 0):?>
	<tr><th colspan='11' style='text-align:center;'>Other / Clearance Fees</th><tr>
	<?foreach ($other_fee as $key => $value):?>
		<tr>
			<td class="bold"><input type='checkbox' name='fees[]' value='<?=$value->id;?>' /> &nbsp;&nbsp;<?=$value->name;?></td>
			<td><span class="badge badge-success"><?=m($value->value);?></span></td>
		</tr>
	<?endforeach;?>
	<?endif;?>

	<?if(count($nstp_fee) > 0):?>
	<tr><th colspan='11' style='text-align:center;'>NSTP Fees (Only Applies if student has NSTP subject)</th><tr>
	<?foreach ($nstp_fee as $key => $value):?>
		<tr>
			<td class="bold"><input type='radio' name='nstp_fee' value='<?=$value->id;?>' />&nbsp;&nbsp;<?=$value->name;?></td>
			<td><span class="badge badge-success"><?=m($value->value);?></span></td>
		</tr>
	<?endforeach;?>
	<?endif;?>

	<?php
	}
  ?>

</table>	