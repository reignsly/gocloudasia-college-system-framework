<script type="text/javascript">
  function validate()
  {
	var xname = $('#payment_type');
	
	if(xname.val() == ""){
		custom_modal('Required','No payment type to save.');
		return false;
	}
  }
</script>
 
  <p>
    <label for="year">Payment Type</label>
	<?php 
		$data = array(
              'name'        => 'payment_type[payment_type]',
              'id'        	=> 'payment_type',
              'value'       => isset($payment_type->payment_type) ? $payment_type->payment_type : '',
              'maxlength'   => '100',
              'size'        => '50',
              'style'       => 'width:50%',
            );

		echo form_input($data);
	?>
  </p>
  <div class="clear"></div>

