<style>
	.label{
		/*width:80px; */
	}
	.day-names{
		font-size:1.2em;
		color:#337ab7;
	}
	.day-cell-Sunday{
		color:#cb2027 !important;
	}
	.icon-step-backward, .icon-step-forward{
		color:#cb2027;
	}
	
	#icon-step-backward{
		float:right;
	}
	
	#icon-step-forward{
		float:left;
	}
	
	.day_Sunday{
		color:#cb2027;
	}

	.is_holiday{
		color:#cb2027;
	}
	
	#day_today{
		color:green !important;
	}
	
	#day_today a{
		color:green;
	}
	
</style>

<div class="table-responsive">
	<?=$calendar;?>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('.is_holiday').parents('td').addClass('danger');
		$('#day_today').parents('td').removeClass('danger').addClass('success');

		$(document).on('click','a.btn-view-event',function(e){
			e.preventDefault();
			var xurl = $(this).attr('href');
			$.ajax({
				type: "POST",
				url: xurl,
				dataType : 'html',
			}).done(function(output){
				bsAlert('View Event', clean_html(output), 'success','wide');
			}).fail(function(jqXHR, textStatus, errorThrown){
				bsAlert('Ajax Error', jqXHR.responseText);	
			});
		})
	})
</script>
