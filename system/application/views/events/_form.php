<style>
	/* css for timepicker */
	.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
	.ui-timepicker-div dl { text-align: left; }
	.ui-timepicker-div dl dt { float: left; clear:left; padding: 0 0 0 5px; }
	.ui-timepicker-div dl dd { margin: 0 10px 10px 45%; }
	.ui-timepicker-div td { font-size: 90%; }
	.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }

	.ui-timepicker-rtl{ direction: rtl; }
	.ui-timepicker-rtl dl { text-align: right; padding: 0 5px 0 0; }
	.ui-timepicker-rtl dl dt{ float: right; clear: right; }
	.ui-timepicker-rtl dl dd { margin: 0 45% 10px 10px; }
	.ui_tpicker_unit_hide { display: none; }
</style>
<?
	if($event)
	{
		$title_val = $event->title;
		$start_date_val = date('Y-m-d',strtotime($event->start_date));
		$end_date_val = date('Y-m-d',strtotime($event->end_date));
		$description_val = html_entity_decode($event->description,ENT_COMPAT);
	}else{
		$title_val = '';
		$start_date_val = $date;
		$end_date_val = '';
		$description_val = '';
	}
	$t_att = array('name'=>'title','value'=>$title_val ,'required'=>true);
	$sd_att = array('name'=>'start_date','value'=>$start_date_val, 'class'=>'dateselect', 'size'=>'20x20','required'=>true);
	$ed_att = array('name'=>'end_date','value'=>$end_date_val, 'class'=>'dateselect', 'size'=>'20x20','required'=>true);
	$d_att = array('name'=>'description','value'=>$description_val);
 ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui-timepicker-addon.js"></script>
<script src="<?=base_url('assets/tinymce/tinymce.min.js')?>"></script>
<script>
	$(function(){
		$('.dateselect').datetimepicker({
			dateFormat: "yy-mm-dd",
			timeFormat: "hh:mm tt"
		});
	})


        tinymce.init({selector:'textarea',
					  resize: false,
					  theme:'modern',
					  height:300,
					  entity_encoding :'raw',
					  plugins: [
        "advlist autolink lists link image charmap textcolor print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
					  toolbar: " undo redo | fontsizeselect styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | preview  fullpage anchor| forecolor backcolor emoticons"
					  });
</script>

<div class="col-md-12">
	<div class="row">
		<div class="col-md-6">
			<label for="start_date">Start Date <code>(yyyy-mm-dd)</code></label><br />
    	<?php echo form_input($sd_att); ?>
		</div>
		<div class="col-md-6">
			<label for="end_date">End Date <code>(yyyy-mm-dd)</code></label><br />
    	<?php echo form_input($ed_att); ?>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="row">
		<div class="col-md-6">
			<label for="title">Title</label><br />
    	<?php echo form_input($t_att); ?>
		</div>
		<div class="col-md-6">
			<label for="is_holiday"></label>
			<p>
			<input type="checkbox" name="to_what[]" value="is_holiday" 
			<?php if($event)
			{
			echo $event->is_holiday=='' || $event->is_holiday=='0' ? "":"checked"; 
			}
			?> 
			/>
			<label for="is_holiday">Is Holiday</label>
			</p>
		</div>
	</div>
</div>

<div class="col-md-12">
	<label for="description">Description</label><br />
	<textarea name="description"><?=$description_val;?></textarea>
	<p></p>
</div>