<style>
/*.calendar {
    font-family: Arial, Verdana, Sans-serif;
    width: 100%;
    min-width: 500px;
    border-collapse: collapse;
}

.calendar tbody tr:first-child th {
    color: #505050;
    margin: 0 0 10px 0;
}

.calendar tbody tr:first-child th#title_cell {
    margin: 0 0 10px 0;
	text-align: center;
	font-size: 12px;
    color: #2C2C2C;
	font-weight: bold;
}*/

.day_header {
    font-weight: normal;
    text-align: center;
    color: #757575;
    font-size: 10px;
}

.calendar td {
    width: 14%; /* Force all cells to be about the same width regardless of content */
    border:1px solid #CCC;
    height: 100px;
    vertical-align: top;
    font-size: 10px;
    padding: 0;
}

.calendar td {
    background-color: #FFF;
}

.calendar td:hover {
    background: #F3F3F3;
}

.day_listing {
    display: block;
    text-align: right;
    font-size: 12px;
    color: #2C2C2C;
    padding: 5px 5px 0 0;
}

span.with_content{
    display: block;
    text-align: right;
    font-size: 12px;
    color: #2C2C2C;
    padding: 5px 5px 0 0;
	background: #99ccff;
    height: 100%;
	cursor:pointer;
}

span.today_with_content{
    display: block;
    text-align: right;
    font-size: 12px;
    color: #2C2C2C;
    padding: 5px 5px 0 0;
	background: #00ff99;
    height: 100%;
	cursor:pointer;
}

div.today {
    background: #E9EFF7;
    height: 100%;
}  
div.not_today {
    background: #FFFFFF;
    height: 100%;
} 
</style>

<?@$this->load->view('events/calendar');?>  

<?php if($dates): ?>
<div class="table-responsive">
    <table class = "table table-stripped table-sortable table-bordered">
        <thead>
            <tr class='gray' >
                <th width="15%">Event</th>
                <th>From</th>
                <th>To</th>
                <th width="45%" >Description</th>
                <th>Holiday?</th>
                <?if($mydepartment->edit_calendar == "1"):?>
                <th>Action</th>
                <?endif;?>
            </tr>
        </thead>
        <tbody>
          <?php foreach( $dates as $date): ?>
            <tr>
                <td class='bold txt-facebook' ><?php echo $date->title ; ?></td>
                <td class='bold' ><?=date('m-d-Y h:i a', strtotime($date->start_date))?></td>
                <td class='bold' ><?=date('m-d-Y h:i a', strtotime($date->end_date))?></td>
                <td><?=html_entity_decode($date->description,ENT_COMPAT);?></td>
                <td class="bold" ><?php echo $date->is_holiday == 1 ? "YES" : "NO" ; ?></td>
                <?if($mydepartment->edit_calendar == "1"):?>
                    <td>
                        <div class="btn-group">
                            <a class="btn btn-xs btn-default" href="<?php echo base_url()."events/edit/".__link($date->id); ?>" ><span class='glyphicon glyphicon-pencil' ></span></a>
                            <a class="btn btn-xs btn-google-plus confirm" href="<?php echo base_url()."events/destroy/".__link($date->id); ?>"><span class='glyphicon glyphicon-trash' ></span></a>
                        </div>
                    </td>
                <?endif;?>
            </tr>
          <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?endif;?>