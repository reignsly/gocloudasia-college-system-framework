<?if(isset($system_custom_message) AND $system_custom_message):?>

	<div class="table-responsive">
		<table class="table">
			<tr class="success bold">
				<td>Description</td>
				<td>Action</td>
			</tr>
			<?foreach ($system_custom_message as $k => $v):?>
				<tr>
					<td><?=$v->desc;?></td>
					<td><a href="<?=site_url('system_settings/edit_custom_message/'.$v->id)?>" class="btn btn-xs btn-primary confirm"><i class="fa fa-edit"></i>&nbsp; Edit</a></td>
				</tr>
			<?endforeach;?>
		</table>
	</div>
	
<?else:?>

	<div class="alert alert-danger"><i class="fa fa-warning"></i>&nbsp;</i>&nbsp;
		No system custom message set, please call your system administrator or provider.
	</div>

<?endif;?>