<script type='text/javascript'>
	$(function() {
		$( "#select_student" ).dialog({
		  autoOpen: false,
		  height: 300,
		  width: 550,
		  modal: true,
		  title: "Generate Grade Slip Per Student",
		});

		$(document).on('click','#btn-select-stud', function(e){
			e.preventDefault();
			$('#select_student').dialog("open");
		})
	});
	
	function select_student(e)
	{
		event.preventDefault();
		alert('x');
	}
	
	function Search_students(page)
	{
		var page = page == null || page == '' || page == '#' ?  0 : page;
		$.blockUI({ message: "" }); 
		var xreturn = false;
		var controller = 'ajax';
		var base_url = '<?php echo base_url(); ?>'; 
		var year_from = '<?php echo $this->open_semester->year_from; ?>'; 
		var year_to = '<?php echo $this->open_semester->year_to; ?>'; 
		var year_id = $('#year_id').val();
		var semester_id = $('#semester_id').val();
		var course_id = $('#course_id').val();
		var type = $('#type').val() == 'excel' ? 1 : 0;
		
		var xdata = $('#select_student *').serialize()
					+"&year_id="+year_id
					+"&semester_id="+semester_id
					+"&course_id="+course_id;
		
		$.ajax({
			'url' : base_url + '' + controller + '/search_students_for_grade_slip/'+page+'/'+year_from+'/'+year_to,
			'type' : 'POST', 
			'async': false,
			'data' : xdata,
			'dataType' : 'json',
			'success' : function(data){ 
				// console.log(data);
				$('#search_links').html(data.links);
				
				
				$('#search_links li a').click(function(event){
					event.preventDefault(); //PREVENT LINKS FROM REDIRECTING
					var href = $(this).attr("href");
					next_page = (href.substr(href.lastIndexOf('/') + 1));
			
					// Search_students(next_page);
				})
				$('#tbody').html('');
				
				var tr = "";
				
				if(data.total_rows > 0){
					for(var x = 0 ; x <= data.count; x++){
						if(data.users[x] != null){
						var xid = data.users[x].id;
						var xstudid = data.users[x].studid;
						var xname = data.users[x].name;
					
						tr += "<tr>";
						tr += "<td>"+xstudid+"</td>";
						tr += "<td>"+xname+"</td>";
						tr += "<td><a href='"+base_url+"generate_grade_slip/generate_grade_slip_per_student/"+xid+"/"+type+"' target='_blank'><div class='badge'>Generate Grade Slip</div></a></td>";
						tr += "</tr>";
						}
					}
					tr += "<tr><td colspan='2'>No of Records</td><td><span class='badge'>"+data.total_rows+"</span></td></tr>";
					
				}else{
					tr = "<tr><td colspan='2'>No records found.</td></tr>";
				}
				
				$('#items_tbody').html(tr);
			}
		})
		.done(function() {
			  $.unblockUI(); 
		  });
		
	}
</script>

<div class="row">
	<div class="col-lg-12">
		<ul class="nav nav-tabs">
		  <li class="<?php echo $tab === 'all' ? 'active' : '' ?>"><a href="#all" data-toggle="tab">All</a></li>
		  <li class="<?php echo $tab !== 'all' ? 'active' : '' ?>"><a href="#perstudent_tab" data-toggle="tab">Per Student</a></li>
		</ul>
		<div class="tab-content">
		  <div class="tab-pane fade <?php echo $tab === 'all' ? 'in active' : '' ?>" id="all">
		  		<p></p>
		  		<div class="row">
		  			<div class="col-lg-12">
							<?php $formAttrib = array('id' => 'search_enrollee', 'class' => 'form-inline', 'method' => 'POST', 'target'=>'_blank');?>
							<?echo form_open('',$formAttrib);?>
								<div class="panel panel-default">
								  <div class="panel-body">
											<div class="form-group">
												<label for="">Select Year Level</label>
										    <?=year_dropdown('year_id',isset($year_id)?$year_id:'','id="year_id"', '')?>
										  </div><br/><br/>
										  
										  <div class="form-group">
										  	<label for="">Select Semester</label>
										    <?=semester_dropdown('semester_id',isset($sem_id)?$sem_id:$this->cos->user->semester_id,'id="semester_id"', '')?>
										  </div><br/><br/>
										  
										  <div class="form-group">
										  	<label for="">Select Course</label>
										    <?=course_dropdown('course_id',isset($course_id)?$course_id:'',"id='course_id'", '')?>
										  </div>

										 	<hr>
										  
											<div class="form-group">
												<label for="">Choose Report Type</label>
										    <?
												$rep = array(
													'pdf' => 'PDF',
													'excel' => 'EXCEL',
												);
												
												echo form_dropdown('type', $rep, isset($type)?$type:'','id="type"');
											?>
											</div>
											<br>
										  
											<div class="form-group">
												<label for="">Choose Number of Grade Slip Per Page (for PDF) </label>
										    <?
														$rep = array(
															'2' => '2',
															'1' => '1',
														);
														
														echo form_dropdown('per_page', $rep, isset($per_page)?$per_page:'2',"id='per_page'");
													?>
											</div>
											
											 
											 <div id='select_student'>
													<table>
													  <tr>
														<td> <div class="form-group">
															<?
																$xfields = array(
																	"enrollments.studid" => "Student ID No.",
																	"enrollments.name" => "Name",
																);
																echo form_dropdown('fields', $xfields, isset($item_field)?$item_field:'serial','id="item_field"');
															?>
															</div>
														</td>
														<td>
															<div class="form-group">
																<label class="sr-only" for="author">Keyword</label>
																<input type='text' name='keyword' id='keyword' value="<?=isset($keyword)?$keyword:''?>" placeHolder="Keyword" />
															</div>
														</td>
														<td><button type="button" class="btn btn-default btn-sm" onclick='Search_students()'>Search</button></td>
													  </tr>
													</table>
														<table id='results' style='font-size:7pt'>
														  <tr class='gray' >
															<th>Student ID</th>
															<th>Name</th>
															<th>ACTION</th>
														  </tr>
															<tbody id='items_tbody'>
															</tbody>
														</table>
														<div id='search_links' style='font-size:7pt'></div>
											</div>
								  </div>
								  <div class="panel-footer">
								  	<?echo form_submit('submit','Generate Grade Slip');?>
								  </div>
								</div>
							<?echo form_close();?>
		  			</div>
		  		</div>
			</div>
		  <div class="tab-pane fade <?php echo $tab !== 'all' ? 'in active' : '' ?>" id="perstudent_tab">
					<p></p>
					<div id="right">
						<?php 
							$formAttrib = array('class' => 'form-inline', 'method' => 'GET');
							echo form_open('generate_grade_slip/index/0/perstudent', $formAttrib);
						?>
					    
					 <div class="form-group">
					    <input id="lastname" class="form-control" type="text" value="<?=isset($lastname)?$lastname:''?>" name="lastname" placeholder="Last Name">
					  </div>
					   
					  <div class="form-group">
					    <input id="fname" class="form-control" type="text" value="<?=isset($fname)?$fname:''?>" name="fname" placeholder="First Name">
					  </div>
					   
					  <div class="form-group">
					    <input id="studid" class="form-control" type="text" value="<?=isset($studid)?$studid:''?>" name="studid" placeholder="Student ID">
					  </div>
					  <div class="form-group">
					    <?=year_dropdown('year_id',isset($year_id)?$year_id:'',"", '')?>
					  </div>
					   		<?php echo form_submit('submit', 'Search','class="btn btn-default btn-sm"'); ?>
							<?echo form_close();?>
					<br>
					<?echo isset($links) ? $links : NULL;?>
						<?php
						if(isset($search)){
						?>
						<br />
						<table class="table table-hover" >
							<thead>
								<tr>
								  <th>Student ID</th>
								  <th>Name</th>
								  <th>Year</th>
								  <th>Course</th>
								  <th>Action</th>
								</tr>
							</thead>
							<tbody>
							<?php
							if(!empty($search))
							{
								foreach($search as $student):?>
								<tr>
								<td class='bold txt-facebook' ><?php echo $student->studid; ?></td>
								<td class='bold txt-facebook' ><?php echo ucfirst($student->name); ?></td>
								<td><?php echo $student->year; ?></td>
								<td><?php echo $student->course; ?></td>
								<td>
									<a target="_blank" href="<?=site_url('generate_grade_slip/generate_grade_slip_per_student/'.$student->id)?>" class="btn btn-facebook"><i class="fa fa-print"></i>&nbsp; Print</a>
									<a target="_blank" href="<?=site_url('generate_grade_slip/generate_grade_slip_per_student/'.$student->id.'/1')?>" class="btn btn-facebook"><i class="fa fa-download"></i>&nbsp; Excel</a>
								</td>
								</tr>
								<?php 
								endforeach; ?>
								<tr>
									<td colspan='4' align='right'><b>Total Records</b></td>
									<td><span class='badge'><?=($total_rows) ? $total_rows : 0;?></span></td>
								</tr>
								<?php
							}
							else
							{
							?>
							<tr>
							<td colspan="5">
								Student Not Found
							</td>
							</tr>
							<?php
							}
							?>
							</tbody>
						</table>
						<?echo isset($links) ? $links : NULL;?>
						<?php
						}
						?>
						</div>
						
						<div class="clear"></div>
					</div>

		  </div>
		</div>
	</div>
</div>

