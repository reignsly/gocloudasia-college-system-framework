<?php $this->load->view('layouts/_student_data'); ?>
<script type="text/javascript">
  $(document).ready(function(){
    $("#unlock_grade").click(function(event) {
      event.preventDefault();
      if($(this).text().trim() == "Click to Edit Grades"){
        $('.grade_tables input[type="text"]').removeClass('hidden');  
        $('.grade_tables input[type="number"]').removeClass('hidden');  
        $('.txt-remarks').removeClass('hidden');  
        $('.grade-badge').addClass('hidden');  
        $('.span-remarks').addClass('hidden');  
        $('#udpate_grade').attr('disabled', false);
        $(this).text("Lock Grades");
      }else{
        $('.grade_tables input[type="text"]').addClass('hidden');  
        $('.grade_tables input[type="number"]').addClass('hidden');  
        $('.txt-remarks').addClass('hidden');  
        $('.grade-badge').removeClass('hidden');  
        $('.span-remarks').removeClass('hidden');  
        $(this).text("Click to Edit Grades");
        $('#udpate_grade').attr('disabled', true);
      }
    })

    $("#sform").validate({
      submitHandler: function(form) {
        form.submit();
      }
    });
  })
</script>
<div class="tab-content">
  <?echo form_open('','id="sform"')?>     
    <?if($mydepartment->stud_edit_grade === "1" && isset($grades->subjects) && $grades->subjects):?>
      <div class='btn-group'>
        <button class="btn btn-sm btn-success" type="submit" value="update_grade" name="update_grade" id="udpate_grade" disabled ><i class="fa fa-edit"></i>&nbsp; Save Grade</button>
        <button  data-toggle="tooltip" data-placement="top" title="Unlock/Lock the grades input fields" class='tp btn btn-danger btn-sm' id='unlock_grade' > Click to Edit Grades</button>
      </div>
    <?endif;?>
    
    <div class='btn-group pull-right hidden-xs'>
      <?if($recalculate_grade):?>
        <!-- <a href="<?php echo site_url('grade/recalculate/'.__link($id)."/".$search_type) ?>" class='btn btn-facebook btn-sm  confirm' title='Student grade conversion will recompute based on the grading system table. Do you want to continue?' ><i class="fa fa-refresh"></i>&nbsp; Recompute Grade Conversion</a> -->
      <?endif;?>
      <button type='button' modal-id = "myModal_grading_system" modal-title="Grading System Table" modal-size ="wide" class='tp btn btn-default btn-sm  button-modal'  > Grading Table</button>
      <br>
      <br>
    </div>
    <div class='btn-group pull-right visible-xs'>
      <?if($recalculate_grade):?>
        <!-- <a href="<?php echo site_url('grade/recalculate/'.__link($id)."/".$search_type) ?>" class='btn btn-facebook btn-sm  confirm' title='Student grade conversion will recompute based on the grading system table. Do you want to continue?' ><i class="fa fa-refresh"></i>&nbsp; Recompute</a> -->
      <?endif;?>
      <button type='button' modal-id = "myModal_grading_system" modal-title="Grading System Table" modal-size ="wide" class='tp btn btn-default btn-sm  button-modal'  >Grading Table</button>

    </div>
      
    <p></p>
    <div class="table-responsive">
      <table class="table grade_tables table-bordered table-condensed">
        <thead>
          <tr class="gray bold" >
            <td class="hidden-xs" >Course No.</td>
            <td class="hidden-xs" >Description</td>
            <td class="visible-xs" >Description</td>
            <?if(isset($grades->periods) && $grades->periods):?>
              <?foreach ($grades->periods as $gp_id => $p):?>
                <td width="10%" ><?=$p?></td>
              <?endforeach;?>
            <?endif;?>
            <td>Final Grade</td>
            <td>Remarks</td>
          </tr>
        </thead>

        <?if(isset($grades->subjects) && $grades->subjects):?>
          <?foreach ($grades->subjects as $k => $subjects):?>

            <? $grade = $subjects['grade']; ?>
            <? $subject = $subjects['subject']; ?>
            
            <?if($subject):?>
              <tr>
                <td class="hidden-xs" ><strong><?= $subject->code; ?></strong></td>
                <td class="hidden-xs" ><strong><?= ucwords(strtolower($subject->subject)); ?></strong></td>
                <td class="visible-xs" ><strong><?= ucwords(character_limiter(strtolower($subject->subject), 10)); ?></strong></td>
                
                <?if($grade):?>
                  <?foreach ($grade as $g_k => $g):?>
                    <?if($mydepartment->stud_edit_grade === "1"):?>
                      <?if(isset($edit_grade_by_period) && $edit_grade_by_period):?>
                        <?if($g->gp_id == $c_grading_period->id):?>
                          <td align="center">
                            <span class='label label-as-badge label-default grade-badge' ><?php echo ($g->converted); ?></span>
                            <input class='grid hidden form-control' type='text' name='subject[<?=$g->id?>]' value='<?=$g->value?>' max="<?php echo $max_grade ?>" min="<?php echo $min_grade ?>" step="any" placeHolder="0.00">
                          </td>
                        <?else:?>
                          <td align="center"><span class="label label-as-badge label-default"><?=$g->converted?></span></td>
                        <?endif;?>
                      <?else:?>
                        <td align="center">
                          <span class='label label-as-badge label-default grade-badge' ><?php echo ($g->converted); ?></span>
                          <input class='grid hidden form-control' type='text' name='subject[<?=$g->id?>]' value='<?=$g->value?>' max="<?php echo $max_grade ?>" min="<?php echo $min_grade ?>" step="any" placeHolder="0.00">
                         </td>
                      <?endif;?>
                    <?else:?>
                      <td align="center"><span class='label label-as-badge label-default grade-badge' ><?php echo ($g->converted); ?></span></td>
                    <?endif;?>
                  <?endforeach;?>
                <?endif;?>
                
                <?if($mydepartment->stud_edit_grade === "1"):?>
                  <?if(isset($edit_grade_by_period) && $edit_grade_by_period):?>
                    <?if($is_final_gp):?>
                      <td>
                        <span class='label label-as-badge label-facebook grade-badge' ><?php echo ($subject->converted); ?></span>
                        <input class='grid txt-remarks hidden' type='text' name='final[<?=$subject->id?>][value]' value='<?=$subject->value?>' placeHolder="Final Grade">
                      </td>
                      <td>
                        <span class='span-remarks' ><?=$subject->remarks?></span>
                        <input class='grid txt-remarks hidden' type='text' name='final[<?=$subject->id?>][remarks]' value='<?=$subject->remarks?>' placeHolder="Remarks">
                      </td>
                    <?else:?>
                      <td><?=$subject->remarks?></td>
                    <?endif;?>
                  <?else:?>
                    <td align="center" >
                      <span class='label label-as-badge label-facebook grade-badge' ><?php echo ($subject->converted); ?></span>
                      <input class='grid txt-remarks hidden' type='text' name='final[<?=$subject->id?>][value]' value='<?=$subject->value?>' placeHolder="Final Grade">
                    </td>
                    <td>
                      <span class='span-remarks' ><?=$subject->remarks?></span>
                      <input class='grid txt-remarks hidden' type='text' name='final[<?=$subject->id?>][remarks]' value='<?=$subject->remarks?>' placeHolder="Remarks">
                    </td>
                  <?endif;?>
                <?else:?>
                  <td align="center"><span class='label label-as-badge label-facebook grade-badge' ><?php echo ($subject->converted); ?></span></td>
                  <td><?=$subject->remarks?></td>
                <?endif;?>

              </tr>
            <?endif;?>

          <?endforeach;?>
        <?else:?>
          <tr><td colspan="3" ><h4>&nbsp;<i class="fa fa-warning"></i>&nbsp; No Grades Available</h4></td></tr>
        <?endif;?>
      </table>
    </div>
  <?=form_close();?>
</div>

<div style="display:none;">
  <?php @$this->load->view('layouts/_grading_system'); ?>
</div>