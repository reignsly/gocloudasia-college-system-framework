<?php $this->load->view('layouts/_student_data'); ?>

<?if($payment_details):?>
	<table width='100%'>
		<?$ctr = 1;?>
		<tr>
			<th>Grading Period</th>
			<th>Amount</th>
			<th>Paid Amount</th>
			<th>Balance</th>
			<th>Is Paid?</th>
			<th>Has Promisory?</th>
			<?if(strtolower($this->session->userdata['userType']) == "student_affairs"):?>
			<th>Action</th>
			<?endif;?>
		</tr>
		<?php 
			$ctr = 1; 
			$pass = false;
			$pass2 = false;
		?>
		<?foreach($payment_details as $obj):?>
			<?php
				$xstyle = "";
				$pointer = "";
				
				if($pass == false)
				{
					if(count($payment_details) == 1) //FULLTIME PAYMENT
					{
						$xstyle = "style='color:#3b5998;font-weight:bold;font-size:12pt;'";
						$pointer = "<span class='glyphicon glyphicon-hand-right'></span>";
						$pass = true;
					}
					else //INSTALLMENT
					{
						//HIGHLIGHT CURRENT GRADING PERIOD
						if($obj->is_downpayment == 1){
							if($obj->is_paid == 0 && $obj->is_promisory == 0){
								$xstyle = "style='color:#3b5998;font-weight:bold;font-size:12pt;'";
								$pointer = "<span class='glyphicon glyphicon-hand-right'></span>";
								$pass = true;
							}
						}else{
							if($obj->grading_period_id == $current_period->id)
							{
								$xstyle = "style='color:#3b5998;font-weight:bold;font-size:12pt;'";
								$pointer = "<span class='glyphicon glyphicon-hand-right'></span>";
								$pass = true;
							}
						}
					}
				}
			?>
			<tr <?=$xstyle?> >
				<td><?=$pointer;?>
					<?
						if(count($payment_details) == 1)
						{
							echo strtoupper($current_period->grading_period);
						}
						else
						{
							if($obj->is_downpayment == 1)	{
								echo "DOWNPAYMENT";
							}
							else{
								echo strtoupper($obj->grading_period);
							}
						}
					?>
				</td>
				<td>&nbsp; &#8369; &nbsp;<?=number_format($obj->amount, 2, '.',' ')?></td>
				<td>&#8369; &nbsp;<?=number_format($obj->amount_paid, 2, '.',' ')?></td>
				<td>&#8369; &nbsp;<?=number_format($obj->balance, 2, '.',' ')?></td>
				<td><?=$obj->is_paid == 1 ? 'YES' : 'NO' ?></td>
				<td><?=$obj->is_promisory == 1 ? 'YES' : 'NO' ?></td>
				<?if(strtolower($this->session->userdata['userType']) == "student_affairs"):?>
				<td>
					<?php						
						if($pass){	
							if(!$pass2) //CURRENT GRADING PERIOD WITH ADD & EDIT
							{
								if($obj->is_paid != 1 && $obj->is_promisory == 1){?>
									<a class='btn btn-danger btn-sm confirm' href="<?=base_url()?>promisory/ungrant/<?=$obj->id;?>/<?=$enrollment_id;?>"><span class='glyphicon glyphicon-thumbs-down'></span>  Remove Promisory</a>
								<?
								}else
								{
									if($obj->is_paid != 1){?>
									<a class='btn btn-default btn-sm btn-sm' href="<?=base_url()?>promisory/grant/<?=$obj->id;?>/<?=$enrollment_id;?>"><span class='glyphicon glyphicon-thumbs-up'></span>  Grant Promisory</a>
									<?}
								}
								$pass2 = true;
							}
						}else{ //PREVIOUS GRADING PERIOD WITH EDIT
							
						}
					?>
				</td>
				<?endif;?>
			</tr>
		<?$ctr++;?>
		<?endforeach;?>
	</table>
<?endif;?>