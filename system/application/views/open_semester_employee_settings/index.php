<div class="alert alert-warning" >
  <h4>Warning!</h4>
  <p class="bold" ><span class="fa fa-hand-o-right" ></span> Change your open semester only for viewing data from previous data, please avoid editing and deleting from previous semesters.</p>
</div>

<?php if($open_semesters): ?>
  <?php echo form_open('','class="confirm"'); ?>
    <div class="table-responsive" >
      <table class="table table-condensed table-sortable table-bordered">
        <thead>
          <tr>
            <th>Your Open Semester</th>
            <th>System Open Semester</th>
            <th>School Year</th>
            <th>Semester</th>
          </tr>
        </thead>

        <tbody>
          <?php foreach ($open_semesters as $k => $v):?>
            <?php
              $current = isset($user_open_semester) && $user_open_semester ? $user_open_semester->open_semester_id : '0';
              $sel = $v->id === $current ? 'checked' : '';
              $sel_caption = $sel == 'checked' ? 'Current' : '';

              $xsystem = $v->use === "1" ? true : false;
              $sys_caption = $xsystem ? 'Current' : '';
            ?>
            <tr class="<?php echo $sel == 'checked' ? 'success' : "" ?>" >
              <td>
                <input type='radio' name='current' <?=$sel?> value='<?=$v->id;?>' required />&nbsp;&nbsp;&nbsp; <strong><?=$sel_caption?></strong>
                <span class="pull-right"><?php echo $sel_caption ? '<span class="badge badge-success"><span class="glyphicon glyphicon-check" ></span></span>' : ''; ?></span>
              </td>
              <td><span class="bold">&nbsp;&nbsp;&nbsp;<?php echo $sys_caption ?></span> <span class="pull-right"><?php echo $sys_caption ? '<span class="badge badge-success"><span class="glyphicon glyphicon-check" ></span></span>' : ''; ?></span></td>
              <td><strong><?php echo $v->academic_year ?></strong></td>
              <td><strong><?php echo $v->name ?></strong></td>
            </tr>  
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
    <button type="submit" name="change_open_semester" value="change_open_semester" class="btn btn-primary" ><span class="entypo-floppy"></span> Update My Open Semester</button>
  <?php echo form_close(); ?>
<?php else: ?>

  <div class="alert alert-danger" ><span class="entypo-alert" ></span>&nbsp; No Open Semester Record was found. </div>

<?php endif; ?>
