<?@$this->load->view('_pdf/_portrait_head');?>
<div class="">
	<table class="table bold">
		<tr>
			<td>Name &nbsp; :</td>
			<td><?=ucwords($_student->profile->fullname)?></td>
			<td>Student ID &nbsp; :</td>
			<td><?=$_student->profile->studid?></td>
		</tr>
		<tr>
			<td>Course &nbsp; :</td>
			<td><?=ucwords($_student->profile->course)?></td>
      <?if($_student->profile->specialization):?>
        <td>Major/Specialization &nbsp; :</td>
        <td><?=ucwords($_student->profile->specialization)?></td>
      <?endif;?>
		</tr>
	</table>
</div>

<div class="">
  <div class="">

    <?if($transcript): foreach ($transcript as $k => $tr):  $profile = $tr['profile']; $subjects = $tr['subjects'];  ?>
      <table class="table" border="1" style="border-collapse: collapse;">
        <thead>
          <tr>
            <td colspan="4" class="bold text-center success" >
              <div class="btn-group">
                <?=$profile->year?> &nbsp; * &nbsp; 
                <?=$profile->name?> &nbsp; * &nbsp; 
                <?=$profile->course_code?>
              </div>
            </td>
          </tr>

          <tr>
            <th>Course No.</th>
            <th>Description</th>
            <th>Units</th>
            <th>Grade</th>
          </tr>      
        </thead>
        
        <?if($subjects): foreach ($subjects as $k => $s):?>
          <tr>
            <td><?=$s->code?></td>
            <td><?=ucwords($s->subject)?></td>
            <td align="center" ><?php echo $s->units ?></td>
            <td align="center" ><?=$get_final_grade($enrollment_id,$s->id)?></td>
          </tr>
        <?endforeach; endif;?>    

      </table>      
      <p></p>
    <?endforeach; endif;?>

  </div>
</div>

<?@$this->load->view('_pdf/_portrait_foot');?>