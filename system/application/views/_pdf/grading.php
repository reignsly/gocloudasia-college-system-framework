<?php
	$subject = $record['subject'];
	$current_gp = $record['current_gp'];
	$students = $record['students'];
	$grading_period = $record['grading_period'];
?>
<?@$this->load->view('_pdf/_portrait_head');?>
<b>Subject Student Grades Report</b><p></p>
<table class="table table-border">
	
	<tr>
		<td class="bold" colspan="3" >Instructor : <?=$subject->teacher?></td>
	</tr>
	<tr>
		<td class="bold" colspan="2" >Subject : <?=$subject->subject?></td>
		<td class="bold" >Grading Period : &nbsp; <?=$current_gp->grading_period?></td>
	</tr>

	<tr>
		<td class="bold " colspan="2" >Course No. : &nbsp; <?=$subject->code?></td>
		<td class="bold " >Room : &nbsp; <?=$subject->room?></td>
	</tr>
</table>
<b>Students (<?=count($students)?>)</b>
<?if($students):?> 

	<table class="table table-border">
		<tr>
			<th>ID</th>
			<th>Fullname</th>

			<?if($grading_period): foreach ($grading_period as $k => $gp):?>
				<th><?=$gp->grading_period?></th>	
			<?endforeach; endif;?>
			<th>Remarks</th>
		</tr>

		<?foreach ($students as $k => $s): $profile = $s['profile']; $_grade = $s['grades']?>
			<tr>
				<td><?=$profile->studid?></td>
				<td><?=$profile->fullname?></td>
				<?php
					$subject_profile = $_grade['subject_profile'];
					$grade = $_grade['subject_grade'];
				?>
				<?if($grading_period): foreach ($grading_period as $k => $gp):?>
					
					<td class="bold text-center">
						
						<?php
							$xgrade = "";
							$xgf_id = 0;
							if($grade): foreach ($grade as $gk => $g):
								if($g->gp_id === $gp->id){
									$xgrade = $g->converted;
									$xgf_id = $g->id;
								}
							endforeach; endif;
						?>

						<?php echo ($xgrade); ?>

					</td>	
				<?endforeach; endif;?>
				
				<td><?=$subject_profile->remarks?></td>

			</tr>
		<?endforeach;?>
	</table>
<?else:?>
	<p></p>
	<p>No Students Found for this Subject.</p>
<?endif;?>	
<?@$this->load->view('_pdf/_portrait_foot');?>