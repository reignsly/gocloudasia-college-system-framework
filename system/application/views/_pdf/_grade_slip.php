<?php
	vd($students);
	$height = (100 / (int)$per_page) - 1;
	for($x = 0; $x <= $per_page-1; $x++)
	{
		vd($students);
	}
?>
			$html .= "
				<div class='maindiv' style='height: {$height}%; width: 100%; float:left;'>";
					$student = $students[$x];
					
						$html.=	"
								<div class='header_title mydiv' >
									<div class='center'>
											<span class='school_name'>
												<img style='width:45px;height:45px;' src='{$base_url}{$ci->setting->logo}'/>
												{$ci->setting->school_name}
											</span><br/>
											<span>{$ci->setting->school_address}</span><br/>
											<span>{$ci->setting->school_telephone}</span><br/>
											<span>{$ci->setting->email}</span><br/>
									</div>
								</div>
								
								<div class='grade_slip mydiv center'>
									<p>Grade Slip</p>
								</div>
								
								<div class='sub_title mydiv'>
									<table><tr>
										<td style='width:20%;'>Time : &nbsp; ".date('g:h A')."</td>
										<td style='width:20%;'>Date : &nbsp; ".date('M d, Y')."</td>
										<td style='width:20%;'>Term : &nbsp; ".$student->semester."</td>
										<td style='width:20%;'>SY : &nbsp; ".$student->sy_from."-".$student->sy_to."</td>
										<td style='width:20%;'>Admission Status : &nbsp; ".$student->status."</td>
									</tr></table>
								</div>
								
								<div class='sub_title mydiv'>
									<table><tr>
										<td style='width:20%;'><b>ID Number : &nbsp; ".$student->studid."</b></td>
										<td style='width:20%;text-align:right;'><b>Student Name : &nbsp; ".strtoupper($student->name)."</b></td>
									</tr></table>
								</div>
								
								<div class='grade_slip mydiv center'>
									<table><tr>
										<td style='width:20%;'><b>Course : &nbsp; ".$student->course."</b></td>
									</tr></table>
								</div>
								
								<div class='subjects mydiv'>
									<table width='100%'>
										<tr>
											<th>Subject Code</th>
											<th>Section Code</th>
											<th>Subject Description</th>
											<th>Final Grade</th>
											<th>Final Remarks</th>
										</tr>";
										// vd($subjects[$student->id]);
										if(isset($subjects[$student->id]) && is_array($subjects[$student->id])){
											
											foreach($subjects[$student->id] as $sub):
									// vd($sub);		
									$html .= "
												<tr>
													<td>{$sub->sc_id}</td>
													<td>{$sub->code}</td>
													<td>{$sub->subject}</td>
													<td>{$sub->value}</td>
													<td>{$sub->remarks}</td>
												</tr>
											";
											
											endforeach;
											
										}
										
										
						$html .=	"</table>
								</div>
								
								<div class='grade_slip mydiv center'>
									<br/>
									<br/>
									<table><tr>
										<td style='border-top: thin solid black;'>".$ci->session->userdata['username']."</td>
									</tr></table>
								</div>
				</div>";
		}
		
		return $html;
	}