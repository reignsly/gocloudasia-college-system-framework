<div class="table-responsive">
	<table class="table table-condensed table-sortable table-bordered">
		<thead>
			<tr>
				<th>Student ID</th>
				<th>Name</th>
				<th>Course</th>
				<th>Year Level</th>
				<th>Units</th>
				<th>Payable</th>
				<th>Deduction</th>
				<th>Amount Paid</th>
				<th>Balance</th>
			</tr>
		</thead>
		<?if($enrollments):?>
			<tbody>
				<?foreach ($enrollments as $k => $v):?>
					<?php
						$profile = $v['profile'];
						$fee = $v['fees'];
						$total = $v['total'];
					?>
					<tr>
						<td class='bold' ><?php echo $profile->studid ?></td>
						<td class='bold' ><?php echo ucwords($profile->fullname) ?></td>
						<td><?php echo $profile->course ?></td>
						<td><?php echo $profile->year ?></td>
						<td><div class="badge"><?php echo $fee->tuition_fee['units'] ?></div></td>
						<td align="right" class='bold' ><?php echo num_format(floatval($total->total_amount_due));?></td>
						<td align="right" class='bold' ><?php echo num_format(floatval($total->total_deduction_amount));?></td>
						<td align="right" class='bold' ><?php echo num_format(floatval($total->total_amount_paid));?></td>
						<td align="right" class='bold text-danger' ><?php echo num_format(floatval($total->total_balance));?></td>
					</tr>
				<?endforeach;?>
			</tbody>
		<?endif;?>
	</table>

	<?php if ($enrollments): ?>
		<p class='bold' >Total Payable : &nbsp; <span class='badge'><?php echo num_format(floatval($total_amount_due));?></span></p>
		<p class='bold' >Total Deduction : &nbsp; <span class='badge'><?php echo num_format(floatval($total_deduction));?><span></p>
		<p class='bold' >Total Amount Paid : &nbsp; <span class='badge'><?php echo num_format(floatval($total_amount_paid));?><span></p>
		<p class='bold' >Total Balance : &nbsp; <span class='badge badge-important'><?php echo num_format(floatval($total_balance));?><span></p>
	<?php endif ?>
</div>