<?$enrollment_id = "";?>
<?$total_amount_balance = 0;?>
<?if($student == false):?>
<h4>No student match for this ID number.</h4>
<?else:?>
	<?
		if($student_total){
			$total_amount_balance = $student_total->balance;
		}
	?>
	<?if($profile):?>
		<?$enrollment_id = $profile->id;?>
		<table border = '1' width='100%' style='border-collapse:collapse;'>
			<tr>
				<th>Name : </th>
				<td><?=$profile->full_name;?></td>
				
				<th>Course & Year: </th>
				<td><?=$profile->course;?>&nbsp;|&nbsp;<?=$profile->year;?></td>
			</tr>
			<tr>
				<th>Current Grading Period : </th>
				<td>
					<?=$current_period->grading_period;?>
				</td>
				
				<th>Payment Plan : </th>
				<td><?=$profile->payment_plan;?></td>
			</tr>
			
			<tr>
				<td colspan=4 style='text-align:center;font-weight:bold;'>Payment Distribution</td>
			</tr>
			
			<tr>
				<td colspan=4>
					<?if($payment_details):?>
						<table width='100%' border=1>
							<?$ctr = 1;?>
							<tr>
								<th>#</th>
								<th>Amount</th>
								<th>Paid Amount</th>
								<th>Balance</th>
								<th>Is Paid?</th>
								<th>Has Promisory?</th>
								<th>Remarks</th>
							</tr>
							<?php
								$cur_obj = false; 
								$cur_ctr = false;
								$pass = false;
								$xstyle = "";
								$pointer = "";
								
								$obj_array = array(); //SAVE ALL DETAILS HERE WITH THE CTR AS INDEX
								$prev_balance = 0;
							?>
							<?foreach($payment_details as $obj):?>
								<?php
									$obj_array[$ctr] = $obj;
									
									$xstyle = "";
									$pointer = "";
									
									if(count($payment_details) == 1) //FULLTIME PAYMENT
									{
										$cur_obj = $obj;
										$cur_ctr = $ctr;
										
										$xstyle = "style='color:#3b5998;font-weight:bold;font-size:12pt;'";
										$pointer = "<span class='glyphicon glyphicon-hand-right'></span>";
										
										$pass = true;
									}
									else
									{
										
										//GET FIRST DETAIL WITH NO PAYMENT AND NO PROMISORY & COLOR THE ROW
										if(!$pass)
										{
											if(floatval($obj->balance) > 0 && $obj->is_promisory == 0){
												$xstyle = "style='color:#3b5998;font-weight:bold;font-size:12pt;'";
												$pointer = "<span class='glyphicon glyphicon-hand-right'></span>";
												$cur_obj = $obj;
												$cur_ctr = $ctr;
												$pass = true;
											}
										}
									}
								?>
								<tr <?=$xstyle?> >
									<td style='text-align:center'><?=$ctr?>. &nbsp;</td>
									<td><?=$pointer;?>&nbsp; &#8369; &nbsp;<?=number_format($obj->amount, 2, '.',' ')?></td>
									<td>&#8369; &nbsp;<?=number_format($obj->amount_paid, 2, '.',' ')?></td>
									<td>&#8369; &nbsp;<?=number_format($obj->balance, 2, '.',' ')?></td>
									<td><?=$obj->is_paid == 1 ? 'YES' : 'NO' ?></td>
									<td>
										<?
											if($obj->is_promisory == 1)
											{
												echo "YES ";
											}
											else
											{
												echo 'NO';
											}
										?>
									</td>
									<td>
										<?
											if($obj->is_downpayment == 1)	{
												echo "DOWNPAYMENT";
											}
											else{
												echo $obj->grading_period;
											}
										?>
									</td>
								</tr>
							<?$ctr++;?>
							<?endforeach;?>
							<?php
								//GET PREVIOUS BALANCE
								for($i = $cur_ctr-1; $i > 0; $i--)
								{
									if(($obj_array[$i]))
									{
										$prev_balance += $obj_array[$i]->balance;
									}
								}
								
							?>
							
							<!--TOTALS-->
							<tr>
								<td><span class='glyphicon glyphicon-chevron-right' ></span></td>
								<td class='text-bold text-center total_number' style='font-weight:bold;' >&#8369; &nbsp;<?=number_format($student_total->total_amount_due - $student_total->less_deduction, 2, '.',' ');?></td>
								
								<td class='text-bold text-center total_number' style='font-weight:bold;' >&#8369; &nbsp;<?=number_format($student_total->total_payment, 2, '.',' ');?></td>
								
								
								<td class='bold text-center total_number' style='font-weight:bold;' >&#8369; &nbsp;<?=number_format($student_total->balance, 2, '.',' ');?></td>
							</tr>
							
						</table>
					<?endif;?>
				</td>
			</tr>
			
			<tr>
				<td colspan=4 style='text-align:center;font-weight:bold;'>----------Nothing follows----------</td>
			</tr>
			
			<tr>
				<th class='text-right'>Previous Balance : </th>
				<td class='text-bold text-center total_number' style='font-weight:bold;' >&#8369; &nbsp;<?=number_format($prev_balance, 2, '.',' ');?></td>
				
				<th class='text-right'>Current Balance : </th>
				<td class='bold text-center total_number' style='font-weight:bold;' >&#8369; &nbsp;<?=$cur_obj ? number_format($cur_obj->balance, 2, '.',' ') : '0.00';?></td>
			</tr>
		</table>
		<input type='hidden' id='previous_balance' value='<?=$prev_balance?>' />
		<input type='hidden' id='current_balance' value='<?=$cur_obj->balance?>' />
	<?else:?>
		<?$enrollment_id = "";?>
		<h4>No unpaid enrollment for this student.</h4>
	<?endif;?>
<?endif;?>
<input type="hidden" id="enrollment_id" name="enrollment_id" value="<?=$enrollment_id?>" />
<input type="hidden" id="total_amount_balance" name="total_amount_balance" value="<?=$total_amount_balance?>" />