<script type='text/javascript'>
	$( document ).ready(function() {
		$('.ui-widget-overlay').live("click",function(){
            $("#message").dialog("close");
        });  
	});
  
	function validate()
	{
		run_pleasewait();
		
		if($('#date').val() == "")
		{
			close_pleasewait();
			custom_modal('Required','<h4>Date required</h4>');
			
			return false;
		}
		else
		{
			if($('#amount').val() == "")
			{
				close_pleasewait();
				custom_modal('Required','<h4>Amount required.</h4>');
			
				return false;
			}
			else
			{
				var xamount = $('#amount').val();
				if(xamount == '') { xamount = 0; }
				xamount = parseFloat(xamount);
				
				if(xamount <= 0)
				{
					close_pleasewait();
					custom_modal('Required','<h4>Amount should be greater than zero</h4>');
					
					return false;
				}
				
				var xexcess = $('#excess').val();
				if(xexcess == '') { xexcess = 0; }
				xexcess = parseFloat(xexcess);
				
				if((xexcess - xamount) < 0)
				{
					close_pleasewait();
					custom_modal('Required','<h4>Amount should be lesser or equal to the excess</h4>');
					
					return false;
				}
			}
		}
	}
	
	function amountchange(event, data)
	{
		var xamount = $(data).val().trim();
		if(xamount == '') { xamount = 0; }
		xamount = parseFloat(xamount);
		
		var xexcess = $('#excess').val();
		if(xexcess == '') { xexcess = 0; }
		xexcess = parseFloat(xexcess);
		
		var xbal = xexcess - xamount;
		
		if(xbal < 0)
		{
			$('#submit').attr('disabled', true);
			$('#lbl_excess').text(xexcess);
			return false;
		}
		else
		{
			$('#submit').attr('disabled', false);
			$('#lbl_excess').text(xbal);
		}
	}
	
</script>
<div id="right">
	<div id="right_bottom">
	
	<div id="parent">
	
	<?=form_open('','onsubmit="return validate()"')?>
		
		<div class="row">
			<div class="col-md-2"><label>Excess Amount</label><br /></div>
			<div class="col-md-4 total_number"><b>&nbsp;&#8369;&nbsp;<span id='lbl_excess'><?=number_format(abs($student_total->balance), 2, '.',' ');?></span></b></div>
			<input type='hidden' id='excess' value='<?=abs($student_total->balance);?>' />
		</div>
		<br/>
		<div class="row">
			<div class="col-md-2"><label for='date'>Date</label></div>
			<div class="col-md-4"><input type="text" class="date_pick not_blank" id="date" name="date" value="<?=set_value('date',date('Y-m-d'));?>"></div>
		</div>
		<br/>
		<br />
		<div class="row">
			<div class="col-md-2"><label for='amount'>Refund Amount</label><br /></div>
			<div class="col-md-4"><input class='amount currency not_blank total_number' type="text" id='amount' name="amount" value="<?=abs($student_total->balance);?>" onkeyup='amountchange(event, this)' placeHolder = "0.00" /></div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-2"><label>Remarks</label></div>
			<div class="col-md-8">
				<textarea name="remarks" class='form-control'> <?=set_value('remarks');?></textarea>
			</div>
		</div>
		<br/>
		<br/>
	  <input type="hidden" name="form_token" value="<?php echo $form_token;?>" />
	<?php echo form_submit('add_payment_record','Submit','id="submit"'); ?>
	
	<a class='btn btn-default btn-sm' href="<?php echo site_url('fees/view_fees').'/'.$enrollment_id; ?>">View Fees</a> 
	
	<a class='btn btn-warning' href="<?php echo site_url('fees/view_student_refund').'/'.$enrollment_id; ?>">Back To List</a>
		<?php echo form_close(); ?>
	</div>

	</div>
	
	<div class="clear"></div>
</div>
<div id='message' title='Required Fields' style='display:none;'>
	<div id='message_list' class='alert alert-info'>
		Validating ... 
	</div>
</div>
<div id='nt'>
</div>