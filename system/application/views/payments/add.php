<script type='text/javascript'>
	$( document ).ready(function() {
		$('.ui-widget-overlay').live("click",function(){
            $("#message").dialog("close");
        });  
	});
  
	function Validate()
	{
		run_pleasewait();
		
		var ctr = 0;
		var msg = "<ul>";
		var xfocus = "";
		$('.not_blank').css('border-color','gray');
		$('.not_blank').each(function(){
			var xval = $(this).val().trim();
			if(xval == ""){
				ctr++;
				$(this).css('border-color','red');
				if(ctr == 1) { xfocus = $(this); }
				var xid = $(this).attr('id');
				console.log(xid);
				var label = $('form').find("label[for='"+xid+"']").text();
				msg += "<li>"
				+label+" should not be blank</li>";
			}
		});
		
		msg += "<ul>";
		
		if(ctr > 0)
		{
			xfocus.focus();
			close_pleasewait();
			custom_modal('Required', msg);
			return false;
		}
		else
		{
			
			if($('#enrollment_id').val() == "" || $('#enrollment_id') == "")
			{
				custom_modal('Error','<h4>An error has occured. Please enter student id again.</h4>');
				
				close_pleasewait();
				return false;
			}
			
			//CHECK DUPLICATE OR NO
			if(check_dublicate_key())
			{
				close_pleasewait();
				return false;
			}
		}
	}
	
	function amount_change()
	{
		var total_amount_balance = parseFloat($('#total_amount_balance').val());
		var payment_amount = parseFloat($('#amount').val());
		
		if(payment_amount > 0 && payment_amount != "")
		{
			if(payment_amount > total_amount_balance){
				close_pleasewait();
				custom_modal('Warning','<h4>Amount is greater than the balance. Excess payment is expected</h4>');
			}
		}
	}
	
	function check_dublicate_key()
	{
		var controller = 'ajax';
		var base_url = '<?php echo site_url(); ?>'; 
		var xtbl_name = 'studentpayments'; 
		var xor_no = $('#receipt_number').val().trim();
		var xret = true;
		
		if(xor_no != "")
		{
			$.ajax({
				'url' : base_url + '' + controller + '/check_dublicate_key/'+xtbl_name+'/'+xor_no,
				'type' : 'POST', 
				'async': false,
				'data' : {},
				'dataType' : 'json',
				'success' : function(data){ 
					
					xret = data.is_duplicate;
					if(data.is_duplicate)
					{
						custom_modal('Warning','<h4>OR. NO. is already in used.</h4>');
						$('#receipt_number').val('');
						$('#receipt_number').focus();
						
					}
				}
			});
		}
		
		return xret;
	}
	
	function studid_keyup(e, data)
	{
		var code = (e.keyCode ? e.keyCode : e.which);
		if(e.keyCode == 13) { //Enter keycode
			view_payment();
			e.preventDefault();
		}
	}
	
	function view_payment()
	{
		var studid = $('#studid').val().trim();
		if(studid == ""){ return; }
		
		var controller = 'ajax';
		var base_url = '<?php echo site_url(); ?>'; 
		var semester_id = '<?php echo $this->open_semester->id; ?>'; 
		var year_from = '<?php echo $this->open_semester->year_from; ?>'; 
		var year_to = '<?php echo $this->open_semester->year_to; ?>'; 
		
		$.ajax({
			'url' : base_url + '' + controller + '/get_student_accounts',
			'type' : 'POST', 
			'async': false,
			'data' : {
				'studid' : studid,
				'year_from' : year_from,
				'year_to' : year_to,
				'semester_id' : semester_id
			},
			'dataType' : 'html',
			'success' : function(data){ 
				//JSON OBJECT RETURN
				$('#div_account').html(data);
				var prev_bal = $('#div_account').find('#previous_balance').attr("value");
				var cur_bal = $('#div_account').find('#current_balance').attr("value");
				var tot_bal = $('#div_account').find('#total_amount_balance').attr("value");
				var total_pay = parseFloat(prev_bal) + parseFloat(cur_bal);
				$('#amount').val(isNaN(total_pay) ? 0 : total_pay);
				
				// console.log(tot_bal);
				if(parseFloat(tot_bal) > 0){
					// console.log(tot_bal);
					$('#submit').attr('disabled', false);
				}
			}
		});
	}
	
	function CloseBlock(){
		$.unblockUI();
	}
</script>
<style type="text/css">
	fieldset {
		background-color:#ddd;
		padding: 5px 5px;
		border-radius: 5px;
	}

	fieldset legend {
		background: #1F497D;
		color: #fff;
		padding: 2px 2px ;
		font-size: 12px;
		border-radius: 5px;
		box-shadow: 0 0 0 5px #ddd;
		
	}
</style>
<div id="right">
	<div id="right_bottom">
	
	<div id="parent">
	
	<?=form_open('','onsubmit="return Validate()"')?>
		
		<div class="row">
			<div class="col-md-2"><label for='date_of_payment'>Date of payment</label></div>
			<div class="col-md-4"><input type="text" class="date_pick not_blank" id="date_of_payment" name="date_of_payment" value="<?=set_value('date_of_payment',date('Y-m-d'));?>"></div>
		</div>
		<br/>
		<div class="row">
			<div class="col-md-2"><label for="studid">Student ID NO.</label></div>
			<div class="col-md-4"><b><?=$profile->studid?></b><input class='' type="hidden" name="studid" id="studid" value="<?=$profile->studid;?>" placeHolder = "Student ID"/></div>
		</div>
		<br/>
		<div class="col-md-12">
			<fieldset>
				<legend><b><span class='glyphicon glyphicon-record' ></span>&nbsp; Account Details</b></legend>
					<div id='div_account'>
						
						<?$enrollment_id = "";?>
						<?$total_amount_balance = 0;?>
						<?if($profile == false):?>
						<h4>No student match for this ID number.</h4>
						<?else:?>
							<?
								if($student_total){
									$total_amount_balance = $student_total->balance;
								}
							?>
							<?if($profile):?>
								<?$enrollment_id = $profile->id;?>
								<table border = '1' width='100%' style='border-collapse:collapse;'>
									<tr>
										<th>Name : </th>
										<td><?=$profile->full_name;?></td>
										
										<th>Course & Year: </th>
										<td><?=$profile->course;?>&nbsp;|&nbsp;<?=$profile->year;?></td>
									</tr>
									<tr>
										<th>Current Grading Period : </th>
										<td>
											<?=$current_period->grading_period;?>
										</td>
										
										<th>Payment Plan : </th>
										<td><?=$profile->payment_plan;?></td>
									</tr>
									
									<tr>
										<td colspan=4 style='text-align:center;font-weight:bold;'>Payment Distribution</td>
									</tr>
									
									<tr>
										<td colspan=4>
											<?if($payment_details):?>
												<table width='100%'>
													<?$ctr = 1;?>
													<tr>
														<th>#</th>
														<th>Amount</th>
														<th>Paid Amount</th>
														<th>Balance</th>
														<th>Is Paid?</th>
														<th>Has Promisory?</th>
														<th>Remarks</th>
													</tr>
													<?php
														$cur_obj = false; 
														$cur_ctr = false;
														$pass = false;
														$xstyle = "";
														$pointer = "";
														
														$obj_array = array(); //SAVE ALL DETAILS HERE WITH THE CTR AS INDEX
														$prev_balance = 0;
													?>
													<?foreach($payment_details as $obj):?>
														<?php
															$obj_array[$ctr] = $obj;
															
															$xstyle = "";
															$pointer = "";
															
															if(count($payment_details) == 1) //FULLTIME PAYMENT
															{
																$cur_obj = $obj;
																$cur_ctr = $ctr;
																
																$xstyle = "style='color:#3b5998;font-weight:bold;font-size:12pt;'";
																$pointer = "<span class='glyphicon glyphicon-hand-right'></span>";
																$pass = true;
															}
															else
															{
																//GET FIRST DETAIL WITH NO PAYMENT AND NO PROMISORY & COLOR THE ROW
																if(!$pass)
																{
																	if(floatval($obj->balance) > 0 && $obj->is_promisory == 0){
																		$xstyle = "style='color:#3b5998;font-weight:bold;font-size:12pt;'";
																		$pointer = "<span class='glyphicon glyphicon-hand-right'></span>";
																		$cur_obj = $obj;
																		$cur_ctr = $ctr;
																		$pass = true;
																	}
																}
															}
														?>
														<tr <?=$xstyle?> >
															<td style='text-align:center'><?=$ctr?>. &nbsp;</td>
															<td><?=$pointer;?>&nbsp; &#8369; &nbsp;<?=number_format($obj->amount, 2, '.',' ')?></td>
															<td>&#8369; &nbsp;<?=number_format($obj->amount_paid, 2, '.',' ')?></td>
															<td>&#8369; &nbsp;<?=number_format($obj->balance, 2, '.',' ')?></td>
															<td><?=$obj->is_paid == 1 ? 'YES' : 'NO' ?></td>
															<td>
																<?
																	if($obj->is_promisory == 1)
																	{
																		echo "YES ";
																	}
																	else
																	{
																		echo 'NO';
																	}
																?>
															</td>
															<td>
																<?
																	if($obj->is_downpayment == 1)	{
																		echo "DOWNPAYMENT";
																	}
																	else{
																		echo $obj->grading_period;
																	}
																?>
															</td>
														</tr>
													<?$ctr++;?>
													<?endforeach;?>
													<?php
														//GET PREVIOUS BALANCE
														for($i = $cur_ctr-1; $i > 0; $i--)
														{
															if(($obj_array[$i]))
															{
																$prev_balance += $obj_array[$i]->balance;
															}
														}
														
													?>
													
													<!--TOTALS-->
													<tr>
														<td><span class='glyphicon glyphicon-chevron-right' ></span></td>
														<td class='text-bold text-center total_number' style='font-weight:bold;' >&#8369; &nbsp;<?=number_format($student_total->total_amount_due - $student_total->less_deduction, 2, '.',' ');?></td>
														
														<td class='text-bold text-center total_number' style='font-weight:bold;' >&#8369; &nbsp;<?=number_format($student_total->total_payment, 2, '.',' ');?></td>
														
														
														<td class='bold text-center total_number' style='font-weight:bold;' >&#8369; &nbsp;<?=number_format($student_total->balance, 2, '.',' ');?></td>
													</tr>
												</table>
											<?endif;?>
										</td>
									</tr>
									
									<tr>
										<td colspan=4 style='text-align:center;font-weight:bold;'>----------Nothing follows----------</td>
									</tr>
									
									<tr>
										<th class='text-right'>Previous Balance : </th>
										<td class='text-bold text-center total_number' style='font-weight:bold;' >&#8369; &nbsp;<?=number_format($prev_balance, 2, '.',' ');?></td>
										
										<th class='text-right'>Current Balance : </th>
										<td class='bold text-center total_number' style='font-weight:bold;' >&#8369; &nbsp;<?=$cur_obj ? number_format($cur_obj->balance, 2, '.',' ') : '0.00';?></td>
									</tr>
								</table>
								<input type='hidden' id='previous_balance' value='<?=$prev_balance?>' />
								<input type='hidden' id='current_balance' value='<?=$cur_obj->balance?>' />
							<?else:?>
								<?$enrollment_id = "";?>
								<h4>No unpaid enrollment for this student.</h4>
							<?endif;?>
						<?endif;?>
						<input type="hidden" id="enrollment_id" name="enrollment_id" value="<?=$enrollment_id?>" />
						<input type="hidden" id="total_amount_balance" name="total_amount_balance" value="<?=$total_amount_balance?>" />
						
					</div>
			</fieldset>
		</div>
		
		<br />
		<div class="row">
			<div class="col-md-2"><label for="receipt_number">OR NO.</label><br /></div>
			<div class="col-md-4"><input class='not_blank' type="text" id="receipt_number" name="receipt_number" value="<?=set_value('receipt_number');?>" placeHolder = "Required" onblur="check_dublicate_key()" /></div>
		</div>
		
		<br />
		<div class="row">
			<div class="col-md-2"><label for="group_or_no">GROUP OR NO.</label><br /></div>
			<div class="col-md-4"><input type="text" name="group_or_no" value="<?=set_value('group_or_no');?>"></div>
		</div>
		
		<br />
		<div class="row">
			<div class="col-md-2"><label for='amount'>Payment Amount</label><br /></div>
			<div class="col-md-4"><input class='amount currency not_blank total_number' type="text" id='amount' name="amount" value="<?=$prev_balance + $cur_obj->balance;?>" placeHolder = "0.00" onblur='amount_change()' /></div>
		</div>
		
	  <!--p>
		<label>Old Account</label><br />
		<input class='amount currency ' type="text" name="old_account" value="<?=set_value('old_account');?>" placeHolder = "0.00" />
	  </p>
	   <p>
		<label>Other</label><br />
		<input class='amount currency ' type="text" name="other" value="<?=set_value('other');?>" placeHolder = "0.00" />
	  </p-->
	  <br />
	  <p>
		<label>Remarks</label><br />
		<textarea name="remarks" style="min-width:500px;max-width:700px;min-height:100px;max-height:300px;"><?=set_value('remarks');?></textarea>
	  </p>
	  <input type="hidden" name="form_token" value="<?php echo $form_token;?>" />
	<?php echo form_submit('add_payment_record','Submit','id="submit"'); ?>
	
	<a class='btn btn-default btn-sm' href="<?php echo site_url('fees/view_fees').'/'.$enrollment_id; ?>">View Fees</a> 
	
	<a class='btn btn-warning' href="<?php echo site_url('payments/view_payment_record').'/'.$enrollment_id; ?>">Back To List</a>
		<?php echo form_close(); ?>
	</div>

	</div>
	
	<div class="clear"></div>
</div>
<div id='message' title='Required Fields' style='display:none;'>
	<div id='message_list' class='alert alert-info'>
		Validating ... 
	</div>
</div>
<div id='nt'>
</div>