<div id="right">
	
	<div>
		<?if($grading_periods):?>
		<?echo form_open('','onsubmit = "return validate()"');?>
		<?$this->load->view('payment_plan/_form')?>
		
			<?
				echo form_submit('','Save Changes');
				?>
				| <a href="<?php echo base_url(); ?>payment_plan" rel="facebox" class='btn btn-default btn-sm' ><span class='glyphicon glyphicon-backward'></span>&nbsp;  Back to List</a>
				<?
				echo form_close();
			?>
		<?else:?>
			<div class="well">
				<h4>No grading period created. To add "Payment Plan" you must first create <a href="<?=base_url()?>grading_periods">grading periods</a>.</h4>
			</div>
		<?endif;?>
	</div>
	<div class="clear"></div>
	<br/>
</div>
	
	<div class="clear"></div>
</div>