<?if($payment_plan):?>
	
	<?
		$division = floatval($payment_plan->division);
	?>

<script type="text/javascript">
  function select_change(el)
  {
	var xval = $(el).val();
	var xid = $(el).attr('xid');
	var xlast = $(el).attr('xlast');
	var xdivision = parseFloat($('#division').val());
	var xnext_select = $(el).parent().parent().next().find('select');
	$('#hd_'+xid).val(xval);
	
	$(el).attr("done", 1);
	
	$(el).parent().parent().find('td').css(
		{'background-color' : 'white',
		'color' : 'black'}
	);
	$(el).attr('disabled', true);
	
	$(xnext_select).attr('disabled', false);
	$(xnext_select).parent().parent().find('td').css(
		{'background-color' : 'green',
		'color' : 'white'}
	);

	if(xval == "0"){return true;}
	
	if(parseFloat(xval) == xdivision)
	{
		growl('<i class="fa fa-check"></i>Notice','<p>You are now ready to save.</p>');
		$('#btn_submit').attr('disabled', false);
	}

	$('select').each(function(){
		var xdis = $(this);
		if($(xdis).attr('done') == "0") //NOT SELECTED
		{
			var xsel = $(xdis).find("option[value='"+xval+"']");
			var xsel_val = parseFloat($(xsel).val());
			while(xsel_val != 0)
			{
				$(xdis).find("option[value='"+xsel_val+"']").remove();
				xsel_val = xsel_val - 1;
			}
		}
	});
	
	//MAKE CAPTION LIKE (2, 3, ,4 ,5)
	var xf_val = false;
	var xl_val = false;
	var xcaption = '';
	$(el).find('option').each(function(){
		if($(this).val() != "")
		{
			xf_val = parseFloat($(this).val());
			return false;
		}
	});

	$(el).find('option').each(function(){
		if($(this).val() != "")
		{
			xl_val = parseFloat($(this).val());
		}
	});
	
	if(xf_val <= xval){
		for(var i = xf_val; i <= xval; i++){
			
			xcaption += i + ", ";
		}
	}
	
	$('#period_con_'+xid).text(xcaption);	
	
  }
</script>
	
	<input type="hidden" id='division' name='division' value='<?=$division?>' />
	
	<table>
			<tr>
				<th>Plan</th>
				<td><?=$payment_plan->name?></td>
				<th>Division</th>
				<td><?=$payment_plan->division?></td>
			</tr>
	</table>

	<?if($division > 0):?>

		<table>
			<tr>
				<th>Grading Period</th>
				<th>
					Cut Off Period
					<a class='btn btn-default btn-sm confirm' title='Clear/Reset ?' style='float:right' href='<?=current_url();?>'><i class="fa fa-refresh"></i></span>&nbsp;  Clear/Reset</a>
				</th>
			</tr>
		
		<?if($grading_periods):?>
			
			<?if($division == 1):?>
				
				<!--IF FULL PAYMENT--->
				
			<?else:?>
			
			<?
				$arr_div[''] = 'Please select';
				for($i = 2; $i <= $division; $i++):
					$nth = num_to_th($i);
					$arr_div[$i] = $nth.' Payment';
				endfor;
				if(count($grading_periods) > $division){
					$arr_div['0'] = 'NONE';
				}
			?>
			
			<!--FIRST SHOULD BE DOWNPAYMENT-->
			<tr>
					<td><b>DOWNPAYMENT (FIXED)</b></td>
					<td>
						1st Payment
						<input type='hidden' name='cut_off[DOWNPAYMENT]' value='1' />
					</td>
			</tr>
			<?$ctr = 2?>
			<?$ctr2 = 1?>
			<?foreach($grading_periods as $obj):?>
				
				<?
					$def = '';
					$xenabled = $ctr == 2 ? '' : 'disabled';
					$xstyle = $ctr == 2 ? "background-color:green;color:white;" : '';
					$xlast = $ctr2 == count($grading_periods) ? 1 : 0;

					//DURING EDIT
					$edit_id = array();
					if(isset($clear) && $clear == "clear"){
						if($grading_period_req){
						foreach ($grading_period_req as $obj2) {
								if($obj->id == $obj2->grading_period_id){
									$edit_id[$obj->id] = $obj2->id;
									break;
								}
							}
						}
					}
				?>
				
				<tr>
					<td style="<?=$xstyle?>" >
						<b><?=$obj->grading_period?></b>
						<br>
						<strong>
						<i>
						<div id='period_con_<?=$obj->id?>' >
							
						</div>
						</i>
						</strong>
					</td>
					<td style="<?=$xstyle?>" >
						<?=form_dropdown('period_'.$obj->id,$arr_div,$def,'id=period_'.$obj->id.' class="" '.$xenabled.' onchange = "select_change(this)" xid="'.$obj->id.'" xlast="'.$xlast.'" done ="0" ');?>
						<input type='hidden' id='hd_<?=$obj->id?>' name='cut_off[<?=$obj->id?>]' value='' />
						<input type='hidden' id='ids_<?=$obj->id?>' name='cut_off_id[<?=$obj->id?>]' value='<?=$edit_id != false ? $edit_id[$obj->id] : ''?>' />
					</td>
				</tr>
				
				<?$ctr++;?>
				<?$ctr2++;?>
				
			<?endforeach;?>	
			
			<?endif;?>
			
		<?endif;?>
		
		</table>
		
	<?endif;?>
	
<?else:?>
	<div class='alert alert-danger'><p><span class='glyphicon glyphicon-remove'></span>&nbsp;  Please go back and select again</p></div>
<?endif;?>