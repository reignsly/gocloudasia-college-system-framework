<div class='alert alert-success'>
	<p><i class="fa fa-info-circle fa-2x"></i> Notice : Arrange the cut off period from lowest to highest.
	Please note that this payment plan will only be available for the next enrollees and cannot be applied from the past enrollment.
	</p>
</div>

<?echo form_open('','id="mysubmit" ');?>
	<?$this->load->view('payment_plan/_form2')?>
<?
echo form_submit('submit','Save Changes','disabled id="btn_submit"');
?>
<!--a title='Are you sure?' href="<?=base_url();?>payment_plan" rel="facebox" class='btn btn-default btn-sm confirm' ><span class='glyphicon glyphicon-backward'></span>&nbsp;  Back to Payment Plan List</a-->
<? echo form_close(); ?>