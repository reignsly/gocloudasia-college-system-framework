<a href="<?php echo base_url(); ?>payment_plan/create" rel="facebox" class='btn btn-default btn-sm btn-sm' ><span class = 'glyphicon glyphicon-plus'></span>&nbsp;  New Payment Plan</a>
<br/>
<br/>
<table id="table">

	<tr>
    <th>Name</th>
    <th>Payment Division</th>
    <th>Action</th>
  </tr>
<tbody>
	
  <?php if($payment_plan){ ?>
  <?php foreach( $payment_plan as $payment_plan): ?>
    <tr>
      <td><?php echo $payment_plan->name ; ?></td>
      <td align='center'><?php echo $payment_plan->division ; ?></td>
      <td>
		  <a href="<?php echo base_url()."payment_plan/edit/".$payment_plan->id; ?>" rel="facebox" class="actionlink"><span class='glyphicon glyphicon-pencil' ></span>&nbsp;  Edit</a> |
		  <a  title="Are you sure to delete this record? Deleting this plan is permanent." href="<?php echo base_url()."payment_plan/destroy/".$payment_plan->id; ?>" rel="facebox" class="actionlink confirm"><span class='glyphicon glyphicon-trash' ></span>&nbsp; Destroy</a>
		  <!-- |
		  <a href="<?php echo base_url()."payment_plan/edit_payment_plan_cut_off_period/".$payment_plan->id; ?>" rel="facebox" class="actionlink confirm"><span class='glyphicon glyphicon-list-alt'></span>&nbsp; Grading Period Cut Off Requirement</a> -->
		</td>
    </tr>
  <?php endforeach; ?>
  <?php } ?>
  
</tbody>
	<tr>
    <th>Name</th>
    <th>Payment Division</th>
    <th>Action</th>
  </tr>
</table>
<a href="<?php echo base_url(); ?>payment_plan/create" rel="facebox" class='btn btn-default btn-sm btn-sm' ><span class = 'glyphicon glyphicon-plus'></span>&nbsp;  New Payment Plan</a>