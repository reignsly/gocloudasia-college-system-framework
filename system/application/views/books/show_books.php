<div class="well">
	<form action="<?=site_url('search/search_employee');?>" method="POST" class=''>
		<p>
			Name: <br />
			<input id="search_name_like" name="name" size="30" type="text" />
		</p>
		<p>
				Login: <br />
				<input id="employee_search" name="login" size="30" type="text" />
		</p>
		<p>
			<input id="search_submit" name="search_employee" type="submit" value="Search" />
		</p>
	</form>
</div>
<table>
	  <tr>
		<th>Title</th>
		<th>Author</th>
		<th>Year</th>
		<th>Bar Code</th>
		<th>Action</th>
	  </tr>
	<? $num = 0 ?>
	<?php if(empty($libraries) == FALSE):?>
		<? foreach($libraries as $obj): ?>
			<tr>
			  
				<td><?= $obj->title;?></td>
				<td><?= $obj->author;?></td>
				<td><?= $obj->year ?></td>
				<td><?= $obj->bar_code ?></td>
				<td>
					<?= badge_link('books/create_library_card/'.$obj->id, "primary", 'Create Library Cards'); ?>
					<?= badge_link('books/view_library_card/'.$obj->id, "primary", 'View Library Cards'); ?>
					<?= badge_link('books/edit/'.$obj->id, "primary", 'Edit'); ?>
					<?= badge_link('books/destroy/'.$obj->id, "primary confirm", 'Destroy','title="Are you sure to delete this record?"'); ?>
				</td>
			</tr>
		<? endforeach;?>
	<?php else:?>
		<tr>
			<td colspan=5 class="text-center">----NO data to show---</td>
		</tr>	
	<?php endif;?>
</table>
<?= $links;?>