
<h4><div class='label label-default'>Bar Code</div>:<div class='label label-info'><?=$book->bar_code;?></div>
<?
	switch(strtoupper($book->status)){
		case "AVAILABLE":
			$style = "success";
		break;
		case "LOST":
			$style="danger";
		break;
		case "BORROWED":
			$style="warning";
		break;
		default:
			$style='default';
		break;
	}
?>
| <div class='label label-default'>Status</div>:<div class='label label-<?=$style?>'><?=$book->status;?></div>
</h4>
<?if(strtoupper($book->status) == 'AVAILABLE'):?>
<a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>books/create_borrower/<?=$id?>/<?=$library_id?>">Add Borrower</a><br/><br/>
<?endif;?>
<table>
	<tr>
		<th colspan='5' style='text-align: center;' >Borrowers</th>
	</tr>
	<tr>
		<th>Name</th>
		<th>Date Borrowed</th>
		<th>Date to be Returned</th>
		<th>Date Returned</th>
		<th>Action</th>
	</tr>
	<?if($borrowers):?>
	<?foreach($borrowers as $obj):?>
		<tr>
			<td><?=$obj->name;?></td>
			<td><?=$obj->date_borrowed;?></td>
			<td><?=$obj->date_to_be_returned;?></td>
			<td><?=$obj->date_return;?></td>
			<td><?=badge_link('books/edit_borrower/'.$id.'/'.$library_id.'/'.$obj->id, 'success', 'Edit')?>
			<?=badge_link('books/destroy_borrower/'.$id.'/'.$library_id.'/'.$obj->id, 'success', 'Destroy','', true)?></td>
		</tr>
	<?endforeach;?>
	<?endif;?>
</table>