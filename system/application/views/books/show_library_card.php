<h4><div class='label label-default'>Bar Code</div>:<div class='label label-info'><?=$book->bar_code;?></div>
<?
	switch(strtoupper($book->status)){
		case "AVAILABLE":
			$style = "success";
		break;
		case "LOST":
			$style="danger";
		break;
		case "BORROWED":
			$style="warning";
		break;
		default:
			$style='default';
		break;
	}
?>
| <div class='label label-default'>Status</div>:<div class='label label-<?=$style?>'><?=$book->status;?></div>
</h4>
<div class="well">
	<label><b>Title : <?=$book->title;?></b></label><br/>
	<label><b>Author : <?=$book->author;?></b></label><br/>
	<label><b>Year Publish : <?=$book->year;?></b></label><br/>
	<label><b>Section : <?=$book->section;?></b></label><br/>
</div>
<a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>books/create_library_card/<?=$id?>">Create Library Card</a>
| <a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>books">Back to List of Books</a><br/><br/>
<table>
	<tr>
		<th>Date</th>
		<th>Action</th>
	</tr>
	<?if($librarycard):?>
	<?foreach($librarycard as $obj):?>
		<tr>
			<td><?=date('m-d-Y',strtotime($obj->date))?></td>
			<td><?=badge_link('books/view_borrowers/'.$id.'/'.$obj->id, 'success', 'View Borrowers')?>
			<?if($book->status == "AVAILABLE"):?>
			<?=badge_link('books/create_borrower/'.$id.'/'.$obj->id, 'success', 'Add Borrowers')?>
			<?endif;?>
			</td>
		</tr>
	<?endforeach;?>
	<?endif;?>
</table>