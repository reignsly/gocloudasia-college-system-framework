<html>
<head>
 <link href="<?php echo base_url('assets/css/view_fees_pdf.css'); ?>" rel="stylesheet" type="text/css"  />
</head>
<body>
<div id="main">
<div>
 <div id="logo">
  <img src="<?= base_url($this->setting->logo); ?>"   width=100 height=100>	
 </div>
 <div id="school_info">
  <?= $this->setting->school_name;  ?><br />
  <?= $this->setting->school_address; ?><br />
  Tel No. <?= $this->setting->school_telephone; ?><br />
  Email: <?= $this->setting->email; ?><br /> 
  <h3>
  REGISTRATION FORM
  </h3>
 </div>
</div>
<div class="clear"></div>
<div>
<table>
 <tr>
  <td>enrollment no: <?= $enrollment->studid; ?></td>
  <td>course:<?= $enrollment->course; ?></td>
 </tr>
 <tr>
  <td><?= $enrollment->semestername; ?>, s.y. <?= $enrollment->sy_from; ?>-<?= $enrollment->sy_to; ?></td>
  <td>major</td>
 </tr>
 <tr>
  <td>student id: <?= $enrollment->studid; ?></td>
  <td>year level: <?= $enrollment->year; ?></td>
 </tr>
 <tr>
  <td>student name: <?= $enrollment->full_name; ?></td>
  <td>&nbsp;</td>
 </tr>
</table>
</div>

<div>
<table id="student_subjects">
 <tr>
  <th>SECTION</th>
  <th>CODE</th>
  <th>DESCRIPTION</th>
  <th>SCHEDULE</th>
  <th>ROOM</th>
  <th>UNIT</th>
  <th>LAB</th>
  <th>LEC</th>
 </tr>
<?php if(!empty($studentsubjects)) :?>
<?php foreach($studentsubjects as $ss): ?>
 <tr>
  <td><?= $ss->sc_id ;?></td>
  <td><?= $ss->code ;?></td>
  <td><?= $ss->subject; ?></td>
  <td><?= $ss->day; ?> <?= $ss->time; ?></td>
  <td><?= $ss->room; ?></td>
  <td><?= $ss->units; ?></td>
  <td><?= $ss->lab; ?></td>
  <td><?= $ss->lec; ?></td>
 </tr>
<?php endforeach; ?>
<?php endif; ?>
 <tr id="total_units">
   <td colspan=5 align=right>TOTAL UNITS</td>
   <td><?php if(!empty($subject_units))echo number_format($subject_units['total_units'],1,'.',' '); ?></td>
   <td colspan=2 ><?php if(!empty($subject_units))echo number_format($subject_units['total_lab'],1,'.',' '); ?></td>
 </tr>
</table>
</div>

<div id="fee_section" >
<div>
<table id="misc_table" >
<?php if(!empty($misc_fees)) :?>
<?php $count = 1; ?>
<tr>
<?php foreach($misc_fees as $sf): ?>
   <td><?= $sf->name ?></td>
   <td><?= $sf->value ?></td>
<?php if($count % 3 == 0) :?>
</tr>
<tr>
<?php endif ;?>
<? $count ++; ?>
<?php endforeach ;?>
</tr>
<?php endif ;?>
</table>
</div>

<table class="small-fonts">
 <tr>
  <td class="underline" colspan=2>ASSESSMENT OF FEES</td>
  <td class="underline" colspan=2>PAYMENT DETAILS</td>
  <td colspan=2>AMOUNT DUE</td>
  <td>DUE DATE</td>
 </tr>
 <tr>
  <td>TUITION FEE(<?php if(!empty($subject_units))echo number_format($subject_units['total_units'],1,'.',' '); ?>) </td>
  <th align=right><?= number_format($total_tuition_fee,2,'.',', '); ?></th>
  <td>OLD ACCOUNTS/(ADVANCE PAYMENT)</td>
  <td align=right><?= number_format($previous_account,2,'.',', '); ?></td>
  <td>Prelim</td>
  <td><?= number_format($payment_division,2,'.',',  '); ?></td>
  <td>________________</td>
 </tr>
 <tr>
  <td>LABORATORY FEE(<?php if(!empty($subject_units))echo number_format($subject_units['total_lab'],1,'.',' '); ?>)</td>
  <th align=right><?= number_format($total_lab_fee,2,'.',', '); ?></th>
  <td>PAYMENT MODE</td>
  <td align=right><?= $payment_category; ?></td>
  <td>Midterm</td>
  <td><?= number_format($payment_division,2,'.',',  '); ?></td>
  <td>________________</td>
 </tr>
 <tr>
  <td>MISCELANEOUS FEES</td>
  <th align=right><?= number_format($total_misc_fee,2,'.',', '); ?></th>
  <td>AMOUNT PAID</td>
  <td align=right><?= number_format($total_student_payment,2,'.', ', '); ?> </td>
  <td>Finals</td>
  <td><?= number_format($payment_division,2,'.',',  '); ?></td>
  <td>________________</td>
 </tr>
 <tr>
  <td>OTHER FEES</td>
  <th align=right><?= number_format($total_other_fee,2,'.',', '); ?> </th>
  <td>DATE PAID</td>
  <td align=right><?= !empty($payment_date) ? date("m/d/Y",strtotime($payment_date)) : ""; ?></td>
  <td> &nbsp; </td>
  <td> &nbsp; </td>
 </tr>
 <tr>
  <td class="underline" colspan=2>ADDITIONAL CHARGES</td>
  <td>PAYMENT TYPE</td>
  <td> &nbsp; </td>
  <td> &nbsp; </td>
 </tr>
 <tr>
  <th>TOTAL ASSESSMENT</th>
  <th  align=right> Php<?= number_format($total_charge,2,'.',', '); ?></th>
  <td>REFERENCE NUMBER</td>
  <td align=right><?= $or_no; ?></td>
  <td> &nbsp; </td>
 </tr>
 <tr>
  <td>REQUIRED DOWNPAYMENT</td>
  <td align=right>3, 200.00</td>
  <td>RECEIPT ISSUED BY</td>
  <td>&nbsp;</td>
  <td> &nbsp; </td>
 </tr>
</table>
</div>
<div id="line_division">
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
</div>
<div id="page2">
<div>
 <div id="logo">
  <img src="<?= base_url($this->setting->logo); ?>" width=100 height=100>	
 </div>
 <div id="school_info">
  <?= $this->setting->school_name;  ?><br />
  <?= $this->setting->school_address; ?><br />
  Tel No. <?= $this->setting->school_telephone; ?><br />
  Email: <?= $this->setting->email; ?><br /> 
  <h3>
  REGISTRATION FORM
  </h3>
 </div>
</div>
<div class="clear"></div>

<div>
<table>
 <tr>
   <td>TERM:</td>
   <td><?= $enrollment->semestername; ?></td>
   <td>S.Y.</td>
   <td colspan=4><?= $enrollment->sy_from; ?> - <?= $enrollment->sy_to; ?></td>
 </tr>
 <tr>
   <td>STUD. ID NO:</td>
   <td><?= $enrollment->studid; ?></td>
   <td>COURSE:</td>
   <td><?= $enrollment->course; ?></td>
   <td>YEAR:</td>
   <td><?= $enrollment->year; ?></td>
   <td>DEPARTMENT:</td>
   <td>&nbsp;</td>
 </tr>
 <tr>
   <td>STUDENT NAME:</td>
   <td colspan=7><?= $enrollment->full_name; ?></td>
 </tr>
 <tr>
   <td>DATE OF BIRTH:</td>
   <td><?= $enrollment->date_of_birth; ?></td>
   <td>PLACE OF BIRTH:</td>
   <td colspan=5><?= $enrollment->place_of_birth; ?></td>
 </tr>
 <tr>
   <td>GENDER:</td>
   <td><?= $enrollment->sex; ?></td>
   <td>CIVIL STATUS: </td>
   <td colspan=5><?= $enrollment->civil_status; ?></td>
 </tr>
 <tr>
   <td>NATIONALITY:</td>
   <td><?= $enrollment->nationality; ?></td>
   <td>RELIGIOUS AFFILIATION:</td>
   <td colspan=5><?= $enrollment->religion; ?></td>
 </tr>
 <tr>
   <td>CELLPHONE/ TEL NO:</td>
   <td colspan=7><?= $enrollment->mobile; ?></td>
 </tr>
 <tr>
   <td>NAME OF FATHER:</td>
   <td colspan=3><?= $enrollment->father_name; ?></td>
   <td>OCCUPATION:</td>
   <td colspan=3><?= $enrollment->father_occupation; ?></td>
 </tr>
 <tr>
   <td>NAME OF MOTHER:</td>
   <td colspan=3><?= $enrollment->mother_name; ?></td>
   <td>OCCUPATION:</td>
   <td colspan=3><?= $enrollment->mother_occupation; ?></td>
 </tr>
 <tr>
   <td>GUARDIAN:</td>
   <td colspan=3><?= $enrollment->guardian; ?></td>
   <td>CONTACT NO:</td>
   <td colspan=3><?= $enrollment->guardian_contact; ?></td>
 </tr>
 <tr>
   <td>LAST SCHOOL ATTENDED:</td>
   <td colspan=3><?= $enrollment->guardian; ?></td>
   <td>TERM:</td>
   <td colspan=3><?= $enrollment->guardian_contact; ?></td>
   <td>SY:</td>
   <td>&nbsp;</td>
 </tr>
 <tr>
   <td>PRESENT ADDRESS:</td>
   <td colspan=3><?= $enrollment->present_address; ?></td>
   <td>PROVINCIAL ADDRESS:</td>
   <td colspan=3><?= $enrollment->provincial_address; ?></td>
 </tr>
 <tr>
   <td>EDUCATIONAL DATA:</td>
   <td colspan=4>
   Elementary: <?= $enrollment->primary; ?><br />
   Secondary: <?= $enrollment->secondary; ?>
   </td>
   <td colspan=4>
   Year: <?= $enrollment->primary_date; ?><br />
   Year: <?= $enrollment->secondary_date; ?>
   </td>
 </tr>
</table>
</div>
<div>
<table id="student_subjects">
 <tr>
  <th>SECTION</th>
  <th>CODE</th>
  <th>DESCRIPTION</th>
  <th>SCHEDULE</th>
  <th>ROOM</th>
  <th>UNIT</th>
  <th>LAB</th>
  <th>LEC</th>
 </tr>
<?php if(!empty($studentsubjects)) :?>
<?php foreach($studentsubjects as $ss): ?>
 <tr>
  <td><?= $ss->sc_id ;?></td>
  <td><?= $ss->code ;?></td>
  <td><?= $ss->subject; ?></td>
  <td><?= $ss->day; ?> <?= $ss->time; ?></td>
  <td><?= $ss->room; ?></td>
  <td><?= $ss->units; ?></td>
  <td><?= $ss->lab; ?></td>
  <td><?= $ss->lec; ?></td>
 </tr>
<?php endforeach; ?>
<?php endif; ?>
 <tr id="total_units">
   <td colspan=5 align=right>TOTAL UNITS</td>
   <td><?php if(!empty($subject_units))echo number_format($subject_units['total_units'],1,'.',' '); ?></td>
   <td colspan=2 ><?php if(!empty($subject_units))echo number_format($subject_units['total_lab'],1,'.',' '); ?></td>
 </tr>
</table>
</div>
</div>

</div>
</body>
</html>
