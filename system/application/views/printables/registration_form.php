<html>
  <head>
    <link href="<?php echo base_url('assets/css/overwrite.css'); ?>" rel="stylesheet" type="text/css"  />
    <style>
      .arial{
        font-family: arial;
      }
      .border-bottom{
        border-bottom: 1px solid gray;
      }

      td.line,span.line{
        padding-bottom: 2px;
        border-bottom: 1px solid gray;
      }

      .collapse{
        border-collapse: collapse;
      }
    </style>
  </head>
  <body>
  <?php
    $copy = "";
    if($user == "registrar"){
      $copy = "Registrar";
    }elseif ($user == "finance") {
      $copy = "Accounting";
    }else{
      $copy = "Student";
    }

    $parent_guardian = $profile->father_name;
    $parent_guardian = trim($profile->mother_name) ? $parent_guardian .", ".$profile->mother_name : $parent_guardian;
    $parent_guardian = trim($profile->guardian_name) ? $parent_guardian .", ".$profile->guardian_name : $parent_guardian;
  ?>  
  <div class="">
    <table border="0" width="100%" style="border-collapse:collapse">
      <tr>
        <td valign="top" >(<?php echo ucwords($copy); ?>'s Copy)</td>
        <td>
          <center>   
            <table border="0" width="100%" style="border-collapse:collapse">
              <tr>
                <td align='right' ><img src="<?= base_url($setting->logo); ?>"   width='65' height='65'></td>
                <td>
                  <br>
                  <center>  
                  <h1 style='font-family:cooperstdblack'><?= ucwords(strtolower($setting->school_name)); ?></h1>
                  <h3 style='font-family:arial black; font-size:15pt'><b><?=$title?></b></h3>
                  <p style="font-family:arial; font-weight:bold" ><?php echo ucwords(strtolower($profile->name)) ?>, School Year <?=$profile->sy_from?>-<?=$profile->sy_to?></p>
                  </center>
                </td>
              </tr>
            </table>
          </center>
        </td>
        <td valign="top">
          <center>
            <div style="border-bottom:1px solid black;" ><b><?=$profile->studid?></b></div>
            <?php echo $cat == "temp" ? "Ref. Number" : "Student Number" ?><br>
            <div>( √ ) &nbsp; <?=ucwords($profile->status)?></div>
          </center>
        </td>
      </tr>
    </table>
  </div>
  <br>
  <div class='arial'>
    <table border="0" width="100%" style="border-collapse:collapse" padding = "2">
      <tr>
        <td width="5%">Name:</td>
        <td align="center"  style="border-bottom:1px solid black" ><?php echo ucwords($profile->lastname) ?></td>
        <td align="center"  style="border-bottom:1px solid black" ><?php echo ucwords($profile->firstname) ?></td>
        <td align="center"  style="border-bottom:1px solid black" ><?php echo ucwords($profile->middle) ?></td>
        <td width="5%">Sex:</td>
        <td align="center"  style="border-bottom:1px solid black" ><?php echo ucwords($profile->sex) ?></td>
      </tr>
      <tr>
        <td width="5%"></td>
        <td align="center" >(Surname)</td>
        <td align="center" >(First Name)</td>
        <td align="center" >(Middle Name)</td>
      </tr>
      <tr>
        <td width="5%">Course:</td>
        <td align="center" colspan="2"  style="border-bottom:1px solid black" ><?php echo ucwords($profile->course) ?></td>
        <td width="10%">Major/Specialization:</td>
        <td align="center" colspan="2"  style="border-bottom:1px solid black" ><?php echo ucwords($profile->specialization) ?></td>
      </tr>
    </table>
  </div>
  <br>
  <div class='arial'>
    <table border="1" width="100%" style="border-collapse:collapse" padding = "2">
      <tr>
        <td align="center">Course No.</td>
        <td align="center">Descriptive Title</td>
        <td align="center">Unit</td>
        <td align="center">Time</td>
        <td align="center">Days</td>
        <td align="center">Room No.</td>
        <td align="center">Professor</td>
      </tr>
      <?if($subjects): $u = 0;?>
        <?foreach ($subjects as $k => $v): $u += $v->units ?>
          <tr>
            <td align="center"><?=$v->code;?></td>
            <td><?=ucwords($v->subject);?></td>
            <td align="right"><?=($v->units);?></td>
            <td><?php echo $v->is_open_time === "0" ? _convert_to_12($v->time) : $v->time;?></td>
            <td><?=($v->day);?></td>
            <td><?=($v->room);?></td>
            <td><?=($v->instructor);?></td>
          </tr>
        <?endforeach;?>
        <tr>
          <td colspan="2" align="right">TOTAL</td>
          <td align="right"><?=($u);?></td>
          <td colspan="4" ></td>
        </tr>
      <?endif;?>
    </table>
  </div>
  <br>
  <br>
  <div class="arial">
     <table width="100%" padding = "2">
      <tr>
        <tr>
          <td width="25%" style="border-bottom:1px solid black" ></td>
          <td></td>
          <td width="25%" style="border-bottom:1px solid black" ></td>
        </tr>
        <tr>
          <td width="25%"><center>Signature of Student</center></td>
          <td></td>
          <td width="25%"><center>Registrar</center></td>
        </tr>
      </tr>
    </table>
  </div>
  <hr>
  <br>
  <br>
  <br>
  
  <div class="arial">
  <?if($user == "registrar"):?>
    <div class="arial">  
      <table border='0' class='collapse' width="100%">
        <tr>
          <td width="22%" >Address:</td>
          <td class='line' >
            <?= $profile->street_no?> &nbsp;
            <?= ucwords(strtolower($profile->barangay));?> &nbsp;
            <?= ucwords(strtolower($profile->municipal));?>, &nbsp;
            <?= ucwords(strtolower($profile->province));?> &nbsp;
          </td>
          <td width="10%" >P' Birth:</td>
          <td class='line' ><?= ucwords(strtolower($profile->place_of_birth));?></td>
        </tr>

        <tr>
          <td>Parent's/Guardian's Name :</td>
          <td colspan="3" class='line' ><?= ucwords(strtolower($parent_guardian));?></td>
        </tr>

        <tr>
          <td>Date of Birth :</td>
          <td colspan="3" class='line' ><?= date('m-d-Y', strtotime($profile->date_of_birth));?></td>
        </tr>

        <tr>
          <td>Civil Status :</td>
          <td colspan="3" class='line' ><?= ucwords(strtolower($profile->civil_status));?></td>
        </tr>

        <tr>
          <td>Contact No :</td>
          <td colspan="3" class='line' ><?= ucwords(strtolower($profile->mobile));?></td>
        </tr>
      </table>
      <br>
    </div>

    <br>
    <div class="arial">
      
      <table border='1' class='collapse' width="100%">
        <tr>
          <td width="25%" ><center>PRELIMINARY EDUCATION</center></td>
          <td><center>Name of School</center></td>
          <td><center>Date of Graduation</center></td>
        </tr>

        <tr>
          <td><center>Elementary School</center></td>
          <td><center><?=$profile->elementary?></center></td>
          <td><center><?=$profile->elementary_date?></center></td>
        </tr>

        <tr>
          <td><center>High School</center></td>
          <td><center><?=$profile->secondary?></center></td>
          <td><center><?=$profile->secondary_date?></center></td>
        </tr>
      </table>

      <br>
    </div>  
  <?elseif($user == "finance"):?>

  <?else:?>
    <table border="0" align="center" style="font-size:11pt">
      <tr>
        <td align="center" ><?php echo ucwords(strtolower($profile->name)) ?>, S.Y. <?=$profile->sy_from?>-<?=$profile->sy_to?></td>
      </tr>
      <tr>
        <td align="center">Number of Units enrolled : &nbsp; <span class="line"><?=$fees->tuition_fee['units']?></span></td>
      </tr>
    </table>
    <br>
    <h4 align="center">FEES</h4>
    <br>
    <table width="50%" border="0" align="center" style="font-size:10pt">
      <?php
        $registration = is_array($plan_modes) ? util::array_first($plan_modes)->value : '0';
      ?>
      <tr>
        <td>Registration:</td>
        <td align="center" width="5%">:</td>
        <td width="50%" align="right" class="line"><?php echo number_format($registration,2) ?></td>
      </tr>
      <tr>
        <td>Tuition:</td>
        <td align="center" width="5%">:</td>
        <td width="50%" align="right" class="line"><?php echo number_format($fees->tuition_fee['amount'],2) ?></td>
      </tr>
      
      <!-- Laboratory Fees -->
      <?if($fees->lab_fee):?>
        <?
          $_lab_fees = (object)$fees->lab_fee; 
          $_lab_amount = 0;
        ?>
        <?foreach ($_lab_fees as $key => $value):?>
          <?php
            $value = (object)$value;
            $_lab_amount += $value->amount;
          ?>
        <?endforeach;?>
        <tr>
          <td>Lab Fee:</td>
          <td align="center" width="5%">:</td>
          <td width="50%" align="right" class="line"><?php echo number_format($_lab_amount,2) ?></td>
        </tr>
      <?endif;?>

      <!-- Misc Fees -->
      <?if($fees->misc_fee):?>
        <?$_misc_fees = (object)$fees->misc_fee;?>
        <tr>
          <td>Miscellaneous Fee:</td>
          <td align="center" width="5%">:</td>
          <td width="50%" align="right" class="line"><?php echo number_format($_misc_fees->total,2) ?></td>
        </tr>
      <?endif;?>

      <!-- Other School Fees -->
      <?if($fees->other_school_fee):?>
        <? $_other_school_fees = (object)$fees->other_school_fee ?>
        <tr>
          <td>Other School Fee:</td>
          <td align="center" width="5%">:</td>
          <td width="50%" align="right" class="line"><?php echo number_format($_other_school_fees->total,2) ?></td>
        </tr>
      <?endif;?>

      <!-- NSTP Fees -->
      <?if($fees->nstp_fee):?>
        <? $_nstp_fees = (object)$fees->nstp_fee; ?>
        <tr>
          <td>NSTP Fee:</td>
          <td align="center" width="5%">:</td>
          <td width="50%" align="right" class="line"><?php echo number_format($_nstp_fees->total,2) ?></td>
        </tr>
      <?endif;?>

      <!-- Other/Clearance Fees -->
      <?if($fees->other_fee):?>
        <? $_other_fees = (object)$fees->other_fee ?>
        <tr>
          <td>Other/Clearance Fee:</td>
          <td align="center" width="5%">:</td>
          <td width="50%" align="right" class="line"><?php echo number_format($_other_fees->total,2) ?></td>
        </tr>
      <?endif;?> 

      <!-- Old Account -->
      <?if($fees->old_accounts):?>

        <tr>
          <td>Old Account:</td>
          <td align="center" width="5%">:</td>
          <td width="50%" align="right" class="line"><?php echo number_format($payment_total->total_previous_amount,2) ?></td>
        </tr>
      <?endif;?>
      
      <!-- Total -->
      <tr>
        <td>Total:</td>
        <td align="center" width="5%">:</td>
        <td width="50%" align="right" class="line"><?php echo number_format($payment_total->total_amount_due,2) ?></td>
      </tr>
    </table>
    <br>
    <br>
    <br>
    <div class="arial">
       <table width="100%" padding = "2">
        <tr>
          <tr>
            <td width="25%" style="border-bottom:1px solid black" ><center><?php echo strtoupper($school->president) ?></center></td>
            <td></td>
            <td width="25%" style="border-bottom:1px solid black" ></td>
          </tr>
          <tr>
            <td width="25%"><center>President</center></td>
            <td></td>
            <td width="25%"><center>Account Section</center></td>
          </tr>
        </tr>
      </table>
    </div>
  <?endif;?>
  </div>

</body>
</html>