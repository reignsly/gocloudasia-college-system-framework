	 <link href="<?php echo base_url('assets/css/printables.css'); ?>" rel="stylesheet" type="text/css"  />
  <div id="certificate_of_enrollment">
   <div id="header-center">
       <table align=center>
         <tr>
		 <td> <img src="<?= base_url($setting->logo); ?>"   width=100 height=100></td>	
		 <td>
		    <h3><?= $setting->school_name; ?></h3><br />
		    <?= $setting->school_address; ?><br />
		    <?= $setting->school_telephone; ?><br />
		    <?= $setting->email; ?><br />
		 </td>
         </tr>
       </table> 
    </div>
    
<table id="student_subjects">
  <tr>
    <th>Subject Code</th>
    <th>Section Code</th>
    <th>Description</th>
    <th>Finals</th>
    <th>Remarks</th>
  </tr>
  <? $num=0 ?>
  
  <? if(!empty($student_grades)) :?>
  <? foreach($student_grades as $key => $sg) :?>
  <tr>
    <td colspan=5 class="transcript-title"><?= $sg['year'] ?> | <?= $sg['semester'] ?> | <?= $sg['course'] ?></td>
  </tr>
  <? foreach($sg['student_grades'] as $sgg ) :?>
  <tr>
    <td><?= $sgg['sc_id']; ?></td>
    <td><?= $sgg['code']; ?></td>
    <td><?= $sgg['description']; ?></td>
    <td><?= $sgg['value']; ?></td>
    <td><?= $sgg['remarks']; ?></td>
  </tr>
  <? endforeach ;?>
  <? endforeach ;?>
  <? else :?>
  <tr>
	  <td colspan=5 align=center>
		  Transcript Empty
	  </td>
  </tr>
  <? endif ;?>
</table>


<div id="header-center">
<h4>TRANSCRIPT CLOSED</h4>
</div>
