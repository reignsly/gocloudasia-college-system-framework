
<p><a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>remaining_balance_percentages/create" rel="facebox">New Required Payment Percentage</a></li></p>

<table id="table">
	<thead>
		<tr>
		<th>Percentage</th>
		<th>Semester</th>
		<th>Action</th>
	  </tr>
	</thead>
	<tfoot>
		<tr>
		  <th>Percentage</th>
		<th>Semester</th>
		<th>Action</th>
	  </tr>
	</tfoot>
	<tbody>
		
	  <?php if($remaining_balance_percentages){ ?>
	  <?php foreach( $remaining_balance_percentages as $remaining_balance_percentage): ?>
		<tr>
		  <td><?php echo $remaining_balance_percentage->percentage ; ?></td>
		  <td><?php echo $remaining_balance_percentage->name ; ?></td>
		  
		  <td><a href="<?php echo base_url()."remaining_balance_percentages/edit/".$remaining_balance_percentage->id; ?>" rel="facebox" class="actionlink">Edit</a>
		  | <a href="<?php echo base_url()."remaining_balance_percentages/destroy/".$remaining_balance_percentage->id; ?>" rel="facebox" class="actionlink confirm">Destroy</a></td>
		  
		</td>
		</tr>
	  <?php endforeach; ?>
	  <?php } ?>
	  
	</tbody>
</table>

<p><a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>remaining_balance_percentages/create" rel="facebox">New Required Payment Percentage</a></li></p>

