<?php
	$sem = array();
	if($semesters){
		foreach($semesters as $obj){
			$sem[$obj->id] = $obj->name;
		}
	}
?>
 
  <p>
    <label for="remaining_balance_percentages">Percentage</label><br />
	<?php 
		$data = array(
              'name'        => 'remaining_balance_percentages[percentage]',
              'value'       => isset($remaining_balance_percentages->percentage) ? $remaining_balance_percentages->percentage : '0',
              'maxlength'   => '100',
              'size'        => '50',
              'style'       => 'width:100px',
            );

		echo form_input($data);
		echo " %";
	?>
  </p>

  <p>
		<label for="semesters">Semesters : </label><br />
		<?=form_dropdown('remaining_balance_percentages[semester_id]',$sem, isset($remaining_balance_percentages) ? $remaining_balance_percentages->semester_id : '');?>
	</p>
  <div class="clear"></div>