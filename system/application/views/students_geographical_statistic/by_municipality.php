<?$this->load->view('students_geographical_statistic/_tab')?>
<br/><br/>
<?if($result):?>
	<table>
		<tr>
			<th>Municipality</th>
			<th></th>
		</tr>
	<?php
		
		
		$total_male = 0;
		$total_female = 0;
		$total_rec = 0;
		
		foreach($result as $obj): 
		//COUNT MALE AND FEMALE
		$record = $obj['data'];
		$male = 0;
		$female = 0;
		$total = 0;
		if($record):
			foreach($record as $obj2):
				if(strtoupper($obj2->sex) == "MALE"){
					$male++;
					$total++;
					$total_male++;
					$total_rec++;
				}
				if(strtoupper($obj2->sex) == "FEMALE"){
					$female++;
					$total++;
					$total_female++;
					$total_rec++;
				}
			endforeach;
		endif;
	?>
		<tr>
			<td><?=strtoupper($obj['municipal'])?></td>
			<td>
				<ul class="nav nav-pills">
					<li>
					<a href="<?=site_url('students_geographical_statistic/view_all/').'/municipal/'.$obj['municipal'];?>">
					View <span class="badge"><?=$total;?></span>
					</a>
					</li>
					<li>
					<a href="<?=site_url('students_geographical_statistic/view_student_by_gender/').'/municipal/'.$obj['municipal'];?>/Male">
					  Male <span class="badge"><?=$male;?></span>
					</a>
					</li>
					<li>
					<a href="<?=site_url('students_geographical_statistic/view_student_by_gender/').'/municipal/'.$obj['municipal'];?>/Female">
					  Female <span class="badge"><?=$female;?></span>
					</a>
					</a>
					</li>
				</ul>
			</td>
		</tr>
	<?php		
		endforeach;
	?>
		<tr>
			<td></td>
			<td>
				<ul class="nav nav-pills">
					<li>
					<a href="#">
					Total Student <span class="badge"><?=$total_rec;?></span>
					</a>
					</li>
					<li>
					<a href="#">
					  Total Male <span class="badge"><?=$total_male;?></span>
					</a>
					</li>
					<li>
					<a href="#">
					 Total Female <span class="badge"><?=$total_female;?></span>
					</a>
					</a>
					</li>
				</ul>
			</td>
		</tr>
	</table>
<?endif;?>