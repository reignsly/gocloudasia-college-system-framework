<div id="right">

	<div id="right_top" >
		  <p id="right_title">Summary Of Credits</p>
	</div>
	<div id="right_bottom">

	<div>
	
<table>
  <tr>
    <th>Course</th>
  </tr>
  <?php foreach( $summary_of_credits as $summary_of_credit ): ?>
    <tr>
      <td><?php echo $summary_of_credit->course; ?></td>
      <td>
		<a href="/summary_of_credits/show/<?php echo $summary_of_credit->id; ?>">Show</a><br />
		<a href="/summary_of_credits/edit/<?php echo $summary_of_credit->id; ?>">Edit</a><br />
		<a href="/summary_of_credits/destroy/<?php echo $summary_of_credit->id; ?>" class="confirm" title="destroy subject <?php echo $summary_of_credit->course; ?>?">Destroy</a>
	  </td>
      <td>
		<a href="/credits/show/<?php echo $summary_of_credit->id; ?>">View Subject Categories</a><br />
		<a href="/credits/create/<?php echo $summary_of_credit->id; ?>">Add Subject Category</a>
	  </td>
    </tr>
  <?php endforeach; ?>
</table>

<p><a href="/summary_of_credits/create">New Summary Of Credit</a></p>
	
	</div>
	<div class="clear"></div>

	</div>
	
	<div class="clear"></div>
	
</div>