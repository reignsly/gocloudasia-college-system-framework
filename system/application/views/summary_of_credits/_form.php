  <p>
    <label for="course_id">Course</label><br />
	<?php
		foreach($courses as $course):
            $options[''.$course->id.''] = $course->course;
		endforeach;
		
		if($summary_of_credit)
		{
			echo form_dropdown('course_id', $options, $summary_of_credit->course_id);
		}
		else
		{
			echo form_dropdown('course_id', $options);
		}
		
	?>
  </p>
