<?php 
		$f_title = array(
              'name'        => 'lessons[title]',
              'value'       => isset($lessons->title) ? $lessons->title : '',
              'maxlength'   => '100',
              'size'        => '50',
              'style'       => 'width:50%',
            );
            
    $f_remarks = array(
              'name'        => 'lessons[remarks]',
              'value'       => isset($lessons->remarks) ? $lessons->remarks : '',
              'maxlength'   => '100',
              'size'        => '50',
              'style'       => 'width:50%',
            );
			
		
?>
	<p>
		<label for="title">Title</label><br />
		<?=form_input($f_title);?>
	</p>
	
	<p>
		<label for="remarks">Remarks</label><br />
		<?=form_input($f_remarks);?>
	</p>
