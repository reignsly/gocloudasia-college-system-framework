<div id="right">
	<div id="right_bottom">

	<div>
	  
	 <table>
	   <tr>
	    <th>Title</th>
	    <th>Remarks</th>
	    <th>Action</th>
	   </tr>
	   <?php if($lessons) :?> 
	   <?php foreach ($lessons as $lesson) :?>
	   <tr>
	    <td><?= $lesson->title ?></td>
	    <td><?= $lesson->remarks ?></td>
	    <td><a href="<?=site_url('lessons/edit');?>/<?= $lesson->id ?>">Edit</a> | <a href="<?=site_url('lessons/destroy');?>/<?= $lesson->id ?>" class="confirm">Delete</a></td>
	   </tr>
	   <?php endforeach; ?>
	   <?php else :?>
	   <tr>
	    <td align="center" colspan=3>No Lessons Created</td>
	   </tr>
	   <?php endif ;?>
	 </table>
	 <br />
	 
	 <a href="<?=site_url('lessons/create');?>" class="btn btn-default btn-sm btn-sm">Create Lesson</a><br>
		</div>
		
	 <br>
	<div class="clear"></div>
</div>
	
	<div class="clear"></div>
</div>
