<div class="row">

	<div id="no-more-tables">
	  <table class="col-md-12 table-bordered table-striped table-condensed cf">
			<thead class="cf">
				<tr>
					<th>Course No.</th>
					<th>Description</th>
					<th>Time</th>
					<th>Day</th>
					<th>Room</th>
					<th>Instructor</th>
				</tr>
			</thead>
			<tbody>
				<?if($subject_profile):?>
						
							<tr>
								<td data-title="Course No." class='bold txt-facebook'  > <?=$subject_profile->code;?></td>
								<td data-title="Subject" class='bold txt-facebook'><?=$subject_profile->subject;?></td>
								<td data-title="Time" class='bold txt-facebook'><?=$subject_profile->time;?></td>
								<td data-title="Day" class='bold txt-facebook'><?=$subject_profile->day;?></td>
								<td data-title="Room" class='bold txt-facebook'><?=$subject_profile->room;?></td>
								<td data-title="Instructor" class='bold txt-facebook'><?=$subject_profile->instructor;?></td>
							</tr>
				<?else:?>
					<tr>
						<td colspan="7"><div class="notice">No data Available</div></td>
					</tr>
				<?endif;?>
			</tbody>
		</table>
	</div>
	
	<script>
		$(document).ready(function(){
			$('#d_subject').on('change', function(){
				window.location = "<?=site_url('subject_grades/view_subject_grades')?>/"+$(this).val();
			});
		});
	</script>

	<div class="well">
		<div class="row">
			<div class="col-md-1 col-xs-2"><a href="<?=site_url('subject_grades')?>" class="btn btn-sm btn-dropbox"><i class="fa fa-hand-o-left"></i>&nbsp; Back</a></div>
			<div class="col-md-11 col-xs-8"><?=form_dropdown('subject', $subjects, $id,'id="d_subject"');?></div>
		</div>
	</div>

	<div class="class-responsive">
		<table class="table table-sortable table-condensed table-stripped">
			<thead>
				<tr class='gray'>
					<th>Student ID</th>
					<th>Student Name</th>
					<th>Course</th>
					<th>Year</th>
					<?if($grading_periods):?>
						<? foreach($grading_periods as $gp):?>
							<th><?=$gp->grading_period;?></th>
						<?endforeach;?>
					<?endif;?>
					<th>Remarks</th>
				</tr>
			</thead>
			<tbody>
				<?if(!empty($grades)):?>
					<? foreach($grades as $grade_val):?>
						<?php
							$profile = $grade_val['profile'];
							$grade = $grade_val['grade'];
						?>
						<tr>
							<td class='bold' ><?=$profile->studid?></td>
							<td class='bold' ><?=ucwords($profile->fullname)?></td>
							<td class='bold' ><?=ucwords($profile->course)?></td>
							<td class='bold' ><?=ucwords($profile->year)?></td>

							<?if($grading_periods):?>
								<? foreach($grading_periods as $gp):?>
									<td><span class="label label-as-badge label-default"><?=isset($grade[$gp->id]) ? $grade[$gp->id] : "";?></span></td>
								<?endforeach;?>
							<?endif;?>
							<td><?=$profile->remarks?></td>
						</tr>
					<?endforeach;?>
				<?else:?>
					<tr>
						<td colspan="7"><div class="notice">No data Available</div></td>
					</tr>
				<?endif;?>
			</tbody>
		</table>
	</div>

</div>