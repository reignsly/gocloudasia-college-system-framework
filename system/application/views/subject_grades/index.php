<form action="<?=site_url('subject_grades');?>/index/0" method="GET" class="" id="search_form" >
	<?php
		$course_id = isset($course_id) ? $course_id : "";
		$course_no = isset($_GET['code'])?$_GET['code']:'';
		$subject = isset($_GET['subject'])?$_GET['subject']:'';
		$major = isset($_GET['major'])?$_GET['major']:'';
	?>

	<div class="row">

		<div class="col-md-3 col-xs-4">
			<label>Course</label><br>
			<?php echo form_dropdown('course_id',$course_list, $course_id) ?>
		</div>

		<div class="col-md-3 col-xs-4">
			<label>Major</label>
			<input class='form-control' id="search_major_id" name="major" size="30" type="text"  value="<?=$major?>" placeHolder="Major" />
		</div>

		<div class="col-md-3 col-xs-4">
			<label>Course No.</label>
			<input class='form-control' id="search_code_eq" name="code" size="30" type="text"  value="<?=$course_no?>" placeHolder="Course No" />
		</div>

		<div class="col-md-3 col-xs-4">
			<label>Description</label>
			<input class='form-control' id="search_subject_eq" name="subject" size="30" type="text" value="<?=$subject?>" placeHolder="Description" />
		</div>

	</div><p></p>

	<div class='row'>

		<div class="col-md-3 col-xs-4">
			<label>Semester</label><br>
			<?=semester_dropdown('semester_id',isset($semester_id)?$semester_id:$this->cos->user->semester_id,"")?>
		</div>

		<div class="col-md-3 col-xs-4">
			<label>Year Level</label><br>
			<?=year_dropdown('year_id',isset($year_id)?$year_id:'',"",'All')?>
		</div>

		<div class="col-md-3 col-xs-4">
			<label>School Year</label><br>
			<?=school_year_dropdown('academic_year_id',isset($academic_year_id)?$academic_year_id:$this->cos->user->academic_year_id,"")?>
		</div>

		<div class="col-md-3 col-xs-6">
			<label></label><br>
			<button id="search_submit" name="search_subjects" type="submit" value="Search" class="btn btn-sm btn-success" ><span class="fa fa-search" ></span>&nbsp; Search</button>
			<button class="btn btn-sm btn-default clear-form" form-id="search_form" type="button" ><span class="fa fa-refresh" ></span></button>
		</div>
	</div>
	
</form>
<p></p>

<?php if(!empty($results)):?>
  <?= $links ;?>	
<?php endif;?>
<br/>
<div class="table-responsive">
	<table class="table table-bordered table-striped table-sortable">
		<thead>
		  <tr class='gray' >		  
		    <th>Course Code</th>
		    <th>Major</th>
		    <th>Year</th>
		    <th>Course No.</th>
		    <th>Description</th>
		    <th>Unit</th>
		    <th>Time</th>
		    <th>Day</th>
		    <th>Room</th>
		    <th>Instructor</th>
		    <th>Action</th>
		  </tr>
	  </thead>
	    
			<?php if(!empty($results)):?>
				<?php foreach($results as $s):?>
					<tr>
						  <td><?php echo $s->course_code;?></td>
						  <td><?php echo $s->major;?></td>
						  <td><?php echo $s->year;?></td>
						  <td><?php echo $s->code;?></td>
						  <td><?php echo $s->subject;?></td>
						  <td><?php echo $s->units;?></td>
						  <td><?php echo $s->time;?></td>
						  <td><?php echo $s->day;?></td>
						  <td><?php echo $s->room;?></td>
						  <td><?php echo $s->instructor;?></td>
						  <td>
							<a href="<?=base_url('subject_grades/view_subject_grades/'.$s->id);?>"><span class='glyphicon glyphicon-folder-open' title='Open <?=$s->subject?>' ></span>&nbsp; View</a>
						</td>
					</tr>
				<?php endforeach;?>
			<?php endif;?>
	</table>	
</div>
<?php if(!empty($results)):?>
  <?= $links ;?>	
<?php endif;?>
	</div>
	
</div>


