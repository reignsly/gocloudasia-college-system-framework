<div class="panel panel-primary">
  <div class="panel-heading">
  	 <h3 class="panel-title">
  	 	Search Media/Book 
  	 	<span class="badge"><?=$total_rows?></span>
        <a href="<?=site_url('library/add_media');?>" class="btn btn-success btn-sm" > <i class="glyphicon glyphicon-plus"></i> Create Media/Item &nbsp;</a>
  	 </h3> 
  </div>

  <?=form_open("library/search_media/0",'method="GET" role="search"')?>
  <div class="panel-body" style="padding : 3px !important;">
    <div class="table-responsive">
    	<table class="table">
    		<tr class="primary">
    			<td>
    				<div class="large-12 columns">
			        	<label>Type</label>
			        	<?=form_dropdown('item_type', $media_types, isset($item_type)?$item_type:'');?>
			      	</div>
    			</td>
    			<td>
    				<div class="large-12 columns">
			        	<label>Subject</label>
			        	<?=form_dropdown('item_category', $book_category, isset($item_category)?$item_category:'');?>
			      	</div>
    			</td>
				<td>
    				<div class="large-12 columns">
			        	<label>Acc. No.</label>
			        	<input type="text" name="accession_number" placeholder="Accession Number" value="<?=isset($accession_number)?$accession_number:''?>" >
			      	</div>
    			</td>
				<td>
    				<div class="large-12 columns">
			        	<label>Call No.</label>
			        	<input type="text" name="call_number" placeholder="Call Number" value="<?=isset($call_number)?$call_number:''?>" >
			      	</div>
    			</td>
    			<td>
    				<div class="large-12 columns">
			        	<label>Title</label>
			        	<input type="text" name="item_name" placeholder="Media or Book Name" value="<?=isset($item_name)?$item_name:''?>" >
			      	</div>
			    </td>
				<!--
    			<td>
    				<div class="large-12 columns">
			        	<label>Description</label>
			        	<input type="text" name="item_description" placeholder="Description" value="<?=isset($item_description)?$item_description:''?>" >
			      	</div>
    			</td>
				-->
    			<td>
	    				<div class="large-12 columns">
				        	<label>Barcode</label>
				        	<input type="text" name="item_barcode" placeholder="Bar Code" value="<?=isset($item_barcode)?$item_barcode:''?>" >
				      	</div>
	    			</td>
    			<td>
    				<div class="large-12 columns">
			        	<label>Author</label>
			        	<input type="text" name="item_author" placeholder="Author" value="<?=isset($item_author)?$item_author:''?>" >
			      	</div>
    			</td>
    			<td>
    				<div class="large-12 columns">
			        	<label>ISBN</label>
			        	<input type="text" name="item_isbn" placeholder="ISBN" value="<?=isset($item_isbn)?$item_isbn:''?>" >
			      	</div>
    			</td>
    			<td>
    				<div class="large-12 columns">
			        	<label>No. of Copies</label>
			      	</div>
    			</td>
    			<td>
    				<div class="large-12 columns">
			        	<label>Available Copy</label>
			      	</div>
    			</td>
    			<td>
    				<button type="submit" class='btn btn-default' ><i class="glyphicon glyphicon-search"></i>&nbsp;</button>
    				<a href="<?=current_url();?>" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i>&nbsp;</a>
    			</td>
    		</tr>
    		<?if(isset($search) && $search):?>
    			<?foreach ($search as $key => $v):?>
    				<tr class="<?=($v->available) <= 0 ? 'danger' : ''?>">
    					<td><?=$v->media_type?></td>
    					<td><?=$v->category?></td>
						<td><?=$v->accession_number?></td>
						<td><?=$v->call_number?></td>
    					<td><?=$v->book_name?></td>
    					<!--<td><?//=$v->book_desc?></td>-->
    					<td><?=$v->book_barcode?></td>
    					<td><?=$v->book_author?></td>
    					<td><?=$v->book_isbn?></td>
    					<td><?=$v->book_copies?></td>
    					<td><?=$v->available?></td>
    					<td>
    						<a href="<?=site_url('library/update_media/'.$v->id);?>" class="btn"><i class="glyphicon glyphicon-edit"></i> Edit</a>
    					</td>
    				</tr>
    			<?endforeach;?>
			<?else:?>
				<tr>
					<td colspan="8" >
						<p>No record to show. </p>
					</td>
				</tr>
			<?endif;?>
    	</table>
    	<?=$links?>
    </div>
  </div>
  </form>
</div>