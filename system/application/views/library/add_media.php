<div class="row">
	<fieldset>
	<?=form_open('');?>
		<div class="input-block">
			<label>Acc No.:</label>
			<input type="text" name="book[accession_number]" value="<?=set_value('book[accession_number]');?>" required />
		</div>
		<div class="input-block">
			<label>Call Number:</label>
			<input type="text" name="book[call_number]" value="<?=set_value('book[call_number]');?>" required />
		</div>
		<div class="input-block">
			<label>Author:</label>
			<input type="text" name="book[book_author]" value="<?=set_value('book[book_author]');?>" required />
		</div>
		<div class="input-block">
			<label>Title:</label>
			<input type="text" name="book[book_name]" value="<?=set_value('book[book_name]');?>" required />
		</div>
		<div class="input-block">
			<label>Item/Media Type:</label>
			<?=form_dropdown('book[media_type_id]',$media_types, set_value('book[media_type_id]'),'required');?>
		</div>
		<div class="input-block">
			<label>Item subject:</label>
			<?=form_dropdown('book[book_category]',$categories, set_value('book[book_category]'),'required');?>
		</div>
		<div class="input-block">
			<label>Item Description:</label>
			<textarea class="form-control"  required name="book[book_desc]"><?=set_value('book[book_desc]');?></textarea>
		</div>
		<div class="input-block">
			<label>ISBN:</label>
			<input type="text" name="book[book_isbn]" value="<?=set_value('book[book_isbn]');?>" required />
		</div>
		<div class="input-block">
			<label>Bar Code:</label>
			<input type="text" name="book[book_barcode]" value="<?=set_value('book[book_barcode]');?>" class="">
		</div>
		<div class="input-block">
			<label>Publisher:</label>
			<input type="text" name="book[book_publisher]" value="<?=set_value('book[book_publisher]');?>" required />
		</div>
		<div class="input-block">
			<label>Year Published:</label>
			<input type="text" name="book[book_dop]" value="<?=set_value('book[book_dop]');?>" class="datepicker_books" required />
		</div>
		<div class="input-block">
			<label>Number Of Copies:</label>
			<input class="form-control" type="number" min = "1" name="book[book_copies]" value="<?=set_value('book[book_copies]');?>" required />
		</div>
		<br>
		<div class="input-block">
			<input type="submit" class="btn btn-primary" name="add_book" value="Save Media">
			<a href="<?=site_url('library/search_media')?>" class="btn btn-default">Cancel</a>
		</div>
	</form>
	</fieldset>
</div>