<script>
  	function show_remark (xkiss) {
  		custom_modal('Remarks', $('#remarks_'+xkiss).html());
  	}
</script>
<?@$this->load->view('library/circulation_head');?>

<div class="panel panel-primary">
  <div class="panel-heading">
  	 <h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i>&nbsp; Active Transactions</h3>
  </div>

  <div class="panel-body" style="padding : 3px !important;">
  	<div class="">
  		<?if(!empty($transactions)):?>
			<!-- REFERENCE  http://bootsnipp.com/snippets/featured/accordion-list-group-menu -->
			<div class="panel-group" id="xaccordion">
			<?foreach ($transactions as $key => $data):?>
				
			  		<div class="panel panel-default">
		              <div class="panel-heading">
		                <p class="panel-title">
		                  <a data-toggle="collapse" data-parent="#xaccordion" href="#collapse_<?=$key?>"><span class="glyphicon glyphicon-book">&nbsp;
		                    </span>
		                    Date : <?=date('M, d Y g:h a', strtotime($data->header->trndte))?>
		                  </a>
		
		                  <a class="pull-right" href="javascript:;" onclick="show_remark('<?=$key?>')" title="Remarks" >
		                  	&nbsp;<span><i class="glyphicon glyphicon-comment"></i></span>&nbsp;
		                  </a>
						 <?if(!has_return_item($data->header->id)):?>
		                  <a href="<?=site_url('library/delete_transaction/'.$usertype.'/'.$id.'/'.$data->header->id)?>" class="confirm pull-right">
		                  	&nbsp;<span><i class="glyphicon glyphicon-trash"></i></span>&nbsp;
		                  </a>
		                  <?endif;?>
		                </p>
		              </div>
					  
					  <!-- Remarks to be show as modal -->
					  <div id="remarks_<?=$key?>" class="hidden" >
					  	<p><?=$data->header->remarks;?></p>
					  </div>

		              <div id="collapse_<?=$key?>" class="panel-collapse collapse">
						<?if(!empty($data->details)):?>
							<?=form_open('');?>
							<div class="table-responsive">
								<table class="table">
									<tr class="info" >
										<td></td>
										<td>Name</td>
										<td>Description</td>
										<td>Days</td>
										<td>Exp. Ret. Date</td>
										<td>Status</td>
										<td>Ret. Date</td>
									</tr>
									<?foreach ($data->details as $key => $item):?>
										<?php

										?>
										<tr class="<?=is_item_late($item->id)?'danger':'';?>" >
											<td>
												<?if($item->cir_status!="RETURN"):?> 
													<input type="checkbox" name="chk_return[]" value="<?=$item->id?>" id="">
												<?endif;?>
											</td>
											<td><?=ucwords($item->book_name)?></td>
											<td><?=ucwords($item->book_desc)?></td>
											<td><?=($item->day)?></td>
											<td><?=date('m/d/Y',strtotime($item->trndte) + (24*3600*$item->day));?></td>
											<td><?=($item->cir_status)?></td>
											<td><?=$item->retdte ? date('m/d/Y', strtotime($item->retdte)) : ''?></td>
										</tr>
									<?endforeach;?>
								</table>
								<button type="submit" name="return_selected" value="return_selected" class="btn btn-primary" >Return Selected Items</button>
								<button type="submit" name="remove_selected" value="remove_selected" class="btn btn-danger" >Remove Selected Items</button>
							</div>
							</form>
						<?else:?>
							<p>No Record</p>
						<?endif?>
		              </div>
		            </div>
			  	
			<?endforeach;?>
			</div>
			
  	 	<?else:?>
  	 		<br/>
  	 		
  	 		<div class="alert alert-danger"><p><i class="fa fa-warning"></i>&nbsp; No active transactions.</p></div>
  	 	<?endif;?>
  	 </div> 
  </div>
</div>
