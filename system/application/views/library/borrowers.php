<div class="row">
	
	<h3>Search <small>Borrower</small></h3>
	<?if(isset($system_message))echo $system_message;
	echo validation_errors() !==''?'<div class="error">'.validation_errors().'</div>' : NULL;
	?>	

	<?@$this->load->view('library/_borrower_tab');?>

	<!-- Tab panes -->
	<div class="tab-content">
	  <div class="tab-pane active" id="home">
			<?if(!empty($search)):?>
				<table class="table table-striped table-hover">
				<thead>
				<tr>
					<th>IDNO</th>
					<th>Student Name</th>
					<th>Grade Level</th>
					<th>Block</th>
					<th>Section</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>
				<?foreach($search as $k => $student):?>
					<tr>
						<td><?=$student->student_id?></td>
						<td><?=ucwords(strtolower($student->fullname));?></td>
						<td><?=ucwords($student->level_desc);?></td>
						<td><?=$student->block_name;?></td>
						<td><?=$student->section_name;?></td>
						<td>
							<a href="<?=site_url('circulation/profile/student/'.$student->enrollment_id)?>" class="btn btn-sm btn-success"><i class="fa fa-book"></i>&nbsp; Profile</a>
						</td>
					</tr>
				<?endforeach;?>
				</tbody>
				</table>
			<?else:?>
				<div class="alert-box">
					<i class="icon-exclamation-sign icon-white"></i> Nothing found!
				</div>
			<?endif;?>
			<?=$links;?>
			
	  </div>

	  <div class="tab-pane" id="profile"></div>
	</div>

</div>