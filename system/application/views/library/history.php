<?@$this->load->view('library/circulation_head');?>

<?@panel_head('Transaction History');?>

<?if(!empty($search)):?>
	<div class="tabl-responsive">
		<table class="table">
			<tr class="info">
				<td>Date</td>
				<td>Title</td>
				<!--<td>Description</td>-->
				<td>Acc. No.</td>
				<td>Call No.</td>
				<td>Barcode</td>
				<td>Status</td>
				<td>Day</td>
				<td>Exp. Ret. Date</td>
				<td>Ret. Date</td>
				<td>Remarks</td>
			</tr>
			<?foreach ($search as $key => $item):?>
			
			<tr class="<?=$item->is_late?'danger':''?>" >
				<td><?=date('m/d/Y', strtotime($item->trndte))?></td>
				<td><?=$item->book_name?></td>
				<!--<td><?//=$item->book_desc?></td>-->
				<td><?=$item->accession_number?></td>
				<td><?=$item->call_number?></td>
				<td><?=$item->book_barcode?></td>
				<td><?=$item->cir_status?></td>
				<td><?=$item->day?></td>
				<td><?=date('m/d/Y',strtotime($item->trndte) + (24*3600*$item->day));?></td>
				<td><?=date('m/d/Y', strtotime($item->retdte))?></td>
				<td><?=$item->remarks?></td>
			</tr>

			<?endforeach?>
		</table>
		<?=$links;?>
	</div>
	
<?else:?>
	<p>No history record.</p>
<?endif;?>

<?@panel_tail();?>