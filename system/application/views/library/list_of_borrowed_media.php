<style>
	select{
		padding:4px 4px !important;
	}
	.large-12{
		padding-left: 0px !important;
		padding-right: 0px !important;
	}
</style>
<!-- THE REASON THIS HERE BECAUSE PUTTING TO THE MAIN LAYOUT BROKES THE FOUNDATION JS -->
<?@panel_head('<i class="fa fa-book"></i>&nbsp; List of Borrowed Media');?>

	<?=form_open("library/list_of_borrowed_media/0",'method="GET" role="search"')?>
	  <div class="panel-body" style="padding : 3px !important;">
	    <div class="table-responsive">
	    	<table class="table">
	    		<tr class="primary">
	    			<td>
	    				<div class="large-12 columns">
				        	<label>Item Name</label>
				        	<input type="text" name="item_name" placeholder="Media or Book Name" value="<?=isset($item_name)?$item_name:''?>" >
				      	</div>
				    </td>
	    			<td>
	    				<div class="large-12 columns">
				        	<label>Description</label>
				        	<input type="text" name="item_description" placeholder="Description" value="<?=isset($item_description)?$item_description:''?>" >
				      	</div>
	    			</td>
	    			<td>
	    				<div class="large-12 columns">
				        	<label>Barcode</label>
				        	<input type="text" name="item_barcode" placeholder="Bar Code" value="<?=isset($item_barcode)?$item_barcode:''?>" >
				      	</div>
	    			</td>
	    			<td>
	    				<div class="large-12 columns">
				        	<label>Author</label>
				        	<input type="text" name="item_author" placeholder="Author" value="<?=isset($item_author)?$item_author:''?>" >
				      	</div>
	    			</td>
	    			<td>
	    				<div class="large-12 columns">
				        	<label>ISBN</label>
				        	<input type="text" name="item_isbn" placeholder="ISBN" value="<?=isset($item_isbn)?$item_isbn:''?>" >
				      	</div>
	    			</td>
	    			<td>
	    				<div class="large-12 columns">
				        	<label>Type</label>
				        	<?=form_dropdown('item_type', $media_types, isset($item_type)?$item_type:'');?>
				      	</div>
	    			</td>
	    			<td>
	    				<div class="large-12 columns">
				        	<label>Category</label>
				        	<?=form_dropdown('item_category', $book_category, isset($item_category)?$item_category:'');?>
				      	</div>
	    			</td>
	    			<td>
	    				<div class="large-12 columns">
				        	<label>Available Copy</label>
				      	</div>
	    			</td>

	    			<td>
	    				<div class="large-12 columns">
				        	<label>Borrowed Copy</label>
				      	</div>
	    			</td>
	    			<td><button type="submit" class='btn btn-primary' ><i class="fa fa-search"></i>&nbsp;</button></td>
	    		</tr>
	    		<?if($search):?>
	    			<?foreach ($search as $key => $v):?>
	    				<tr class="<?=($v->available) <= 0 ? 'danger' : ''?>">
	    					<td><?=$v->book_name?></td>
	    					<td><?=$v->book_desc?></td>
	    					<td><?=$v->book_barcode?></td>
	    					<td><?=$v->book_author?></td>
	    					<td><?=$v->book_isbn?></td>
	    					<td><?=$v->media_type?></td>
	    					<td><?=$v->category?></td>
	    					<td><?=$v->available?></td>
	    					<td><?=$v->book_borrowed?></td>
	    					<td>
	    						<a href="javascript:;" onclick="show('<?=$v->id?>')" class="btn" title="View Borrowers" ><i class="glyphicon glyphicon-folder-open"></i>&nbsp;</a>
	    					</td>
	    					<td class="hidden" >
	    						<div id="borrowers_<?=$v->id?>" >
	    							<?if(!empty($borrowers[$v->id])):?>
	    								<div class="table-responsive">
	    									<table class="table">
	    										<tr class="info" >
	    											<td>Borrowers Name</td>
	    											<td>User Type</td>
	    											<td>Number of Copy</td>
	    											<td>Action</td>
	    										</tr>

	    										<?foreach($borrowers[$v->id] as $key => $x ):?>
	    											<tr>
	    												<td>
	    													<?php
	    														if($x->usertype === "student"){
	    															echo ucwords($x->profile->full_name);
	    														}else{
	    															// vp($x->profile);
	    															echo ucwords($x->profile->fullname);
	    														}
	    													?>
	    												</td>
	    												<td><?=ucwords($x->usertype);?></td>
	    												<td><?=($x->copy);?></td>
	    												<td><a target="_blank" href="<?=site_url('library/circulation/'.$x->usertype.'/'.$x->borrower_id)?>" class="confirm btn btn"><i class="fa fa-book"></i>&nbsp; Go to Profile</a></td>
	    											</tr>
	    										<?endforeach;?>

	    									</table>
	    								</div>
	    							<?else:?>
	    								<p> No borrower Found </p>
	    							<?endif;?>
	    						</div>	
	    					</td>
	    				</tr>
	    			<?endforeach;?>
				<?else:?>
					<tr>
						<td colspan="11" >
							No Item Found.
						</td>
					</tr>
				<?endif;?>
	    	</table>
	    	<?=$links?>
	    </div>
	  </div>
	</form>

<?@panel_tail();?>
<script>
	function show (xid) {
		custom_modal('Borrowers',$('#borrowers_'+xid).html());
	}
</script>