<?if($usertype === "student"):?>
<div class="panel panel-primary">
  <div class="panel-heading">
  	 <h3 class="panel-title">
  	 	<i class="fa fa-tag"></i>&nbsp; Student Library Profile
  	 	<div class="btn-group">
  	 		<a href="<?=site_url('library/borrow/'.$usertype.'/'.$id)?>" class="btn btn-xs btn-default"><i class="fa fa-book"></i>&nbsp; Borrow Media</a>
  	 		<a href="<?=site_url('library/circulation/'.$usertype.'/'.$id)?>" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-tower"></i>&nbsp; Profile</a>
  	 		<a href="<?=site_url('library/history/'.$usertype.'/'.$id)?>" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-align-justify"></i>&nbsp; Transaction History</a>
  	 		<a href="<?=site_url('library/students')?>" class="btn btn-default btn-xs"><i class="fa fa-search"></i>&nbsp; Go to Search</a>
			</div>
  	 </h3>
  	 <!-- <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span> -->
  </div>

  <div class="panel-body">
    <div class="row">
    	<div class="col col-md-1"></div>
    	<div class="col col-md-6">
    		<h3><?=ucwords(strtolower($student->full_name))?></h3>
	      <p><strong><span class="glyphicon glyphicon-tag"></span>&nbsp; Course : &nbsp; </strong> <?=$student->course?> </p>
	      <p><strong><span class="glyphicon glyphicon-tag"></span>&nbsp; Year : &nbsp; </strong> <?=$student->year?> </p>
    	</div>
    </div>
	</div>
  </div>
</div>
<?else:?>
<div class="panel panel-primary">
  <div class="panel-heading">
  	 <h3 class="panel-title">
  	 	<i class="fa fa-tag"></i>&nbsp; Employee Library Profile
  	 	<div class="btn-group">
  	 		<a href="<?=site_url('library/borrow/'.$usertype.'/'.$id)?>" class="btn btn-xs btn-default"><i class="fa fa-book"></i>&nbsp; Borrow Media</a>
  	 		<a href="<?=site_url('library/circulation/'.$usertype.'/'.$id)?>" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-tower"></i>&nbsp; Profile</a>
  	 		<a href="<?=site_url('library/history/'.$usertype.'/'.$id)?>" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-align-justify"></i>&nbsp; Transaction History</a>
  	 		<a href="<?=site_url('library/students')?>" class="btn btn-default btn-xs"><i class="fa fa-search"></i>&nbsp; Go to Search</a>
			</div>
  	 </h3>
  	 <!-- <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span> -->
  </div>

  <div class="panel-body" style="padding : 3px !important;">
   <div class="row">
    	<div class="col col-md-1"></div>
    	<div class="col col-md-6">
    		<h3><?=ucwords(strtolower($employee->fullname))?></h3>
	      <p><strong><span class="glyphicon glyphicon-tag"></span>&nbsp; Department : &nbsp; </strong> <?=ucwords($employee->department)?> </p>
    	</div>
    </div>
  </div>
</div>
<?endif?>