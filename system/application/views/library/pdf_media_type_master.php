<!DOCTYPE html>
<html>
<head>
	<title>Media Master List by Type Report</title>
</head>
<body>

<p><strong>Report : Media Master List by Type</strong></p>
<hr/>
<style>
	.hdr{
		border-bottom: 2px solid gray;
		/*border-top: 2px solid gray;*/
	}
	table{
		width:100% !important;
	}
</style>
<div class="table-responsive">
	<?if(($type)):?>

	<table>
		<?php
			$m_total = 0;
			$c_total = 0;
		?>
		<?foreach ($type as $kt => $t):?>
			<?php
				$head = $t['head']; // type
				$tail = $t['tail']; // books
				$count = $t['tail'] ? count($t['tail']) : 0; // books
				$m_total += $count;
			?>
			<tr>
				<td width="100%" colspan="2"><strong>Media Type : &nbsp; <?=$head->media_type?></strong>&nbsp; (<?=$count?>)</td>
			</tr>
			<tr>
				<td colspan="2" >
					<?if($tail):?>
						<table style="width:100%; border: 1px solid black;">
							<tr style="background: gray;">
				    			<td class="hdr" >Category</td>
				    			<td class="hdr"  >Item Name</td>
				    			<td class="hdr"  >Description</td>
				    			<td class="hdr"  >Barcode</td>
				    			<td class="hdr"  >Author</td>
				    			<td class="hdr"  >ISBN</td>
				    			<td class="hdr"  >Publisher</td>
				    			<td class="hdr"  >Date Pub.</td>
				    			<td class="hdr"  >Qty</td>
				    			<!-- <td class="hdr"  >Available Copy</td> -->
			    			</tr>
			    			<?php
			    				$total_copy = 0;
			    			?>
							<?foreach ($tail as $key => $v):?>
								<?php
									$total_copy += $v->book_copies;
									$c_total += $v->book_copies;
								?>
			    				<tr>
			    					<td><?=$v->category?></td>
			    					<td><?=$v->book_name?></td>
			    					<td><?=$v->book_desc?></td>
			    					<td><?=$v->book_barcode?></td>
			    					<td><?=$v->book_author?></td>
			    					<td><?=$v->book_isbn?></td>
			    					<td><?=$v->book_publisher?></td>
			    					<td><?= $v->book_dop && $v->book_dop !== "1970-01-01" ? date('m/d/Y',strtotime($v->book_dop)) : '';?></td>
			    					<td><?=$v->book_copies?></td>
			    					<!-- <td><?=$v->available?></td> -->
			    				</tr>
			    			<?endforeach;?>
			    			<tr>
			    				<td colspan="9" >&nbsp; <br></td>
			    			</tr>
			    			<tr>
		    					<td colspan="9" ><strong>Total Copy : &nbsp; <?=$total_copy?></strong></td>
			    			</tr>
						</table>
					<?else:?>
					<p>----------No Media for <?=$head->media_type?>------------</p>
					<?endif;?>
				</td>
			</tr>
			<tr>
				<td>&nbsp; <br/></td>
			</tr>
			
		<?endforeach;?>
	</table>

	<?if( count($type) > 1):?>
		<p><strong>Total Type : <?=count($type);?></strong></p>
		<p><strong>Total Media : <?=$m_total;?></strong></p>
		<p><strong>Total Copies : <?=$c_total;?></strong></p>
	<?endif;?>
	<?else:?>
		<p>No record Found</p>
	<?endif;?>
</div>
</body>
</html>