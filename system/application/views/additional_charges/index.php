<table >
	<tr>
	<th>Remarks</th>
    <th>Value</th>
    <th>Action</th>
	</tr>
  <? $total_amount = 0; ?>	
  <?php if($additional_charges){ ?>
  <?php foreach( $additional_charges as $additional_charge): ?>
    <tr>
      <td><?php echo $additional_charge->remarks ; ?></td>
	  <td><?php echo $additional_charge->value ; ?></td>
	  <?$total_amount += $additional_charge->value;?>
      <td>
      <?= badge_link("additional_charges/destroy/".$additional_charge->id .'/'.$enrollment_id, 'danger confirm', 'delete'); ?>
   	</td>
    </tr>
  <?php endforeach; ?>
	<tr>
		<td colspan=1 class="text-right">Total Charges</td>
		<td><span class='badge'><?echo number_format($total_amount, 2, '.',' ');?></span></td>
	</tr>
  <?php }else{ ?>
    <tr><td colspan=3 class="text-center">No Additional Charges</td></tr>
  <?php } ?>
</table>

<p><a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>additional_charges/create/<?= $enrollment_id ?>" >New Additional Charge</a>
<a href="<?php echo base_url(); ?>fees/view_fees/<?= $enrollment_id ?>" class="btn btn-default btn-sm"> Back to Student Fees</a>
</p>

