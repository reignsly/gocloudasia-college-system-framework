
	<script type="text/javascript">
		
	  function validate()
	  {
		run_pleasewait();
		
		if($('#value').val().trim() == "")
		{
			close_pleasewait();
			notify_modal('Required','Value must not be blank.');
			return false;
		}
		else
		{
			return true;
		}
	  }
	  
	function typeChange(data)
	{
		var type = $(data).val();
		
		if(type == "manual")
		{
			$('.additional_charge').removeAttr('readonly');
		}
		else
		{
			$('.additional_charge').attr('readonly', true);
			
			distribute_charge_equally();
		}
	}
	
	function typeChange2()
	{
		if($('#type_all').attr('checked'))
		{
			
			$('.additional_charge').attr('readonly', true);
			
			distribute_charge_equally();
		}
		else
		{
			$('.additional_charge').removeAttr('readonly');
		}
	}
	
	function distribute_charge_equally()
	{
		var additional_charge = $('#value').val().trim();
		var payment_division = $('#payment_division').val();
		
		if(additional_charge != "" && payment_division != "" ){
			console.log(additional_charge);
			console.log(payment_division);
			additional_charge = parseFloat(additional_charge);
			payment_division = parseFloat(payment_division);
			
			if(additional_charge > 0 && payment_division > 0)
			{
				var division = additional_charge / payment_division;
				
				if(division > 0){
					$('.additional_charge').val($.number( division, 2,'.', '' ));
				}
				else
				{ $('.additional_charge').val(0); }
			}
			else
			{ $('.additional_charge').val(0); }
		}
		else
		{ $('.additional_charge').val(0); }
	}
	
	function valueChange(data)
	{
		var amount = $(data).val().trim();
		
		$('#lbl_add_amount').text($.number( amount, 2,'.', ' ' ));
		
		if(amount == "")
		{
			$('.additional_charge').val(0);
			$('#submit').attr('disabled', true);
		}
		else
		{
			amount = parseFloat(amount);
			
			if(amount > 0) { 
				
				typeChange2();
				
				$('#submit').attr('disabled', false); 
			}
			else{ $('#submit').attr('disabled', true); $('.additional_charge').val(0); }
		}
	}
	  
	</script>
	
		<?echo form_open('','onsubmit="return validate()"');?>
			
			<fieldset>
				
				<legend>Charges</legend>
				
				<?$this->load->view('additional_charges/_form')?>
			
			</fieldset>
			
			<br/>
				<?
					echo form_hidden("additional_charge[enrollment_id]",$enrollment_id );
					echo form_submit('','Save Changes','disabled id="submit"');
				?>
				<a href="<?php echo base_url(); ?>fees/view_fees/<?= $enrollment_id ?>" class="btn btn-default btn-sm"> Back to Student Fees</a>

				<a href="<?php echo base_url(); ?>additional_charges/index/<?= $enrollment_id ?>" class="btn btn-warning"> Back to Additional Charges</a>
			
			<? echo form_close(); ?>
