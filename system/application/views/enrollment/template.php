<doctype html>
<html>
		<head>
			<title><?=$this->title;?></title>
			<link rel="stylesheet" href="<?=base_url('assets/css/e/e.css');?>">
			<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" media="screen" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url('assets/css/custom-theme/jquery-ui-1.8.18.custom.css'); ?>" rel="stylesheet" type="text/css" media="all" />
			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>
			<style>
				div.error {border: 1px solid maroon !important; color: black; background: pink; margin:0 0  10px; padding:10px; }
				div.success {background: #c2ffcc; border: 1px solid #5cde72; color: #114019; margin: 0 0 10px 0; padding: 10px;}
				div.notice {color: black; background-color: #EBF8A4; margin:0 0  10px 0; padding:10px; border:1px solid #A2D246;}
			</style>
			<link href="<?php echo base_url('assets/css/custom.css'); ?>" rel="stylesheet" type="text/css" media="all" />
		</head>
		<body>
		<div id="head2">
			<?=ucwords($this->settings->school_name);?><br />
			Academic Year: <?=$this->header->academic_year;?> | Semester: <?=$this->header->name;?> <br />
			<?=date('F d, Y',time());?>
		</div>
		<div id="head3">
			<p>Note:</p>
			<p>- You only have 2 days to pay your required downpayment. Failure to do so will mean cancellation of your enrollment.</p>
			<p>- Having a problem entering your enrollment? Please call for assistance.</p> 
		</div>

	<div class="colmask fullpage">
		<div class="col1">
			<div id="container">
				<?php echo $yield; ?>
			</div>
		</div>
	</div>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?=base_url('assets/js/jquery-ui-1.8.18.custom.min.js');?>" type="text/javascript"></script>
	<script  src="<?=base_url('assets/js/e/corner.js');?>"></script>
	<script  src="<?=base_url('assets/js/e/application.js');?>"></script>
	<script src="<?=base_url('assets/js/myjs.js');?>" type="text/javascript"></script>
	</body>
</html>