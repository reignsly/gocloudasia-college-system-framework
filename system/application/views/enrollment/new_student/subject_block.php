<div id="enroll1">
	<?=isset($system_message) ? $system_message : NULL;?>
	<div class="boxed" >
		Student ID: <strong><?=ucwords($enrollee_profile->login);?></strong><br>
		Name: <strong><?=$enrollee_profile->stud_name;?></strong><br>
		Year: <strong><?=$enrollee_profile->year;?></strong><br>
		Semester: <strong><?=$enrollee_profile->sem_name;?></strong><br>
		Course: <strong><?=$enrollee_profile->course;?></strong><br>
		Academic <strong><?=$this->header->academic_year;?></strong>
	</div>
	<div class="clear"></div>
	<div class="boxed" >
	<?if(!empty($available_sections)):?>
		<form action="<?=site_url('enrollment/new_student/subject_block');?>" method="post">

			<?foreach($available_sections as $a):?>
				<input name="section" type="radio" value="<?=$a->section_id;?>" /><?=$a->section_name;?><br>
			<?endforeach;?>
		<p><input class="submit" name="select_section" type="submit" value="Submit"></p>
		</form>
	<?else:?>
	<?endif;?>
	</div>
	<div class="clear"></div>
</div>