<div id="enroll1">
<? echo  isset ($system_message) ? $system_message :  NULL ;?>
<form action="<?=site_url('enrollment/main');?>" method="POST">
		Please choose your Enrollment Status and click Submit:<br>

<table id="hor-minimalist-a" >
        <tr>
                <th width="100">Status</th>
                <th>Description</th>
        </tr>
               
	       
        <tr>
			<td width="20%">
				<input class="radio_button" id="status_new" name="status" type="radio" value="New" />
				<b>NEW</b>
			</td>
	        <td>
				-High school graduates who had never been enrolled to any colleges/universities
			</td>
		</tr>
		<tr>
			<td width="20%">
				<input class="radio_button" id="status_cross-enrollee" name="status" type="radio" value="cross" />
				<b>CROSS-ENROLLEE</b>
			</td>
	        <td>
				-Students who are officially enrolled in another college/university and is permitted to take some subjects in in this institution. (Should have submitted a Permit to Cross-enroll to the Registrar)
			</td>
		</tr>
        <tr>
			<td width="20%">
				<input class="radio_button" id="status_transferee" name="status" type="radio" value="Transferee" />
				<b>TRANSFEREE</b>
			</td>
	        <td>
			-Students who were enrolled in another college/university in the previous semester.
			</td>
        </tr>
        <tr>
               <td width="20%">
		<input class="radio_button" id="status_returnee" name="status" type="radio" value="Returnee" />
		<b>RETURNEE</b>
	        </td>
	        <td>
	         -Students in this institution who did not enroll in the previous semester. (Not Transferee)
	        </td>
        </tr>
        <tr>
            <td width="20%">
               <input class="radio_button" id="old_btn" name="status" type="radio" value="Old" />
	       <b>OLD</b>
		
		<div id ="old_id">
				ID No.<br />
				<input id="studid" name="studid" type="text" /><br />
				<p id="warn">"Refer to your Registration Form or Exam Permit only for your ID No."</p>
    	</div>
	        </td>
	        <td>
	        -Students in this institution who are enrolled in the previous semester.
	        </td>
        </tr>
        <tr>
				<input type="hidden" name="form_token" value="<?=$form_token;?>">
                <td><input class="submit" type="submit" name="start_enrollment" value="Submit" /></td>
        </tr>
</table>


</form>
</div>
<script type="text/javascript" language="javascript" charset="utf-8">
// <![CDATA[	
	$(document).ready(function(){
		//Hide div w/id extra
	   $("#old_id").hide();
		// Add onclick handler to checkbox w/id checkme
	   $(".radio_button").change(function(){
		// If checked
		if ($(this).val() == "Old")
		{
			//show the hidden div
			$("#old_id").show("fast");
		}
		else
		{
			//otherwise, hide it
			$("#old_id").hide("fast");
		}
	  });
     	//Hide div w/id extra
	   $("#continue_id").hide();

		// Add onclick handler to checkbox w/id checkme
	   $(".radio_button").change(function(){

		// If checked

		if ($(this).val() == "Continue-Enrollment")
		{
			//show the hidden div
			$("#continue_id").show("fast");
		}
		else
		{
			//otherwise, hide it
			$("#continue_id").hide("fast");
		}
	  });

	});
// ]]>

$('form').submit(function() { 
   var form_id = $(this).attr('id');
   if(form_id != 'new_user'){
    $('.default-value').each(function(){
      var defaultVal = $(this).attr('title');
      if ($(this).val() == defaultVal){
        $(this).val('');
      }
    }); 
    $(":submit").removeAttr("disabled"); 
    } 
}); 
</script>