<?php
$link = sha1('new');
$search_message = '<div class="alert alert-success"><strong>Searched Keywords:</strong><i> '.htmlentities($search_strings).'</i>';
$search_message .='<br><a href="'.site_url("finance/student_payments/?new=".$link).'">Clear Search History</a></div>';
?>

<div id="right">
	<div id="right_top" >
	<p id="right_title">Student Payments / Search</p>
		  <!--<p id="back"></p>-->
	</div>
	<div id="right_bottom">
		<div class="notice">
			Checks for all student payments from students, if you want to have the data from a specific date to another
			use <a href="<?=site_url('finance/all_payment_report');?>"><strong>Payment Report</strong></a>.
		</div>
			<?= validation_errors() == '' ? NULL : '<div class="alert alert-danger">'.validation_errors().'</div>';?>
			<?= isset($system_message) ? $system_message : NULL;?>
			
			<form action="<?=site_url('finance/student_payments');?>" method="post">
				<p>
					<label for="group_payment_start_date">NAME</label>
					<input name="name" size="30" type="text" /><br>
					<label for="group_payment_end_date">Student ID</label>
					<input name="student_id" size="30" type="text" /><br>
					<label for="group_payment_end_date">Original Reciept Number</label>
					<input name="orno" size="30" type="text" /><br>
					<input name="form_token" type="hidden" value="<?=$form_token;?>"/>
					<input id="group_payment_submit" name="search" type="submit" value="Search" />
				</p>
			</form>
		<div>
		<?=isset($search_strings) ? $search_message : NULL;?>
			<table>
				<thead>
					<tr>
						<td>Fullname</td>
						<td>Date of Payment</td>
						<td>Amount Recieved</td>
						<td>Old Accounts</td>
						<td>Total</td>
						<td>Reciept Number</td>
					</tr>
				</thead>
				<tbody>
					<? if(!empty($results)):?>
						<?foreach($results as $r):?>
						
						<tr>
							<td><?=$r->fullname;?></td>
							<td><?=$r->date_of_payment;?></td>
							<td><?=$r->account_recivable_student;?></td>
							<td><?=$r->old_account;?></td>
							<td><?=$r->total;?></td>
							<td><?=$r->or_no;?></td>
						</tr>
						<?endforeach;?>
					<? else:?>
						<div class="notice">No data found</div>
					<?endif;?>
				</tbody>
			</table>
		</div>
		<?=isset($pagination_links) ? $pagination_links : NULL;?>
	</div>
</div>