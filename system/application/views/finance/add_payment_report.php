<div id="right">
	<div id="right_top" >
	<p id="right_title">Add New Group Report</p>
		  <!--<p id="back"></p>-->
	</div>
	
	<div id="right_bottom">
			<?= validation_errors() == '' ? NULL : '<div class="alert alert-danger">'.validation_errors().'</div>';?>
			<?= isset($system_message) ? $system_message : NULL;?>
			<form action="<?=site_url('finance/add_payment_report');?>" method="post">
			  <p>
				<label for="group_payment_start_date">Start date</label><br />
				<input class="dateM" id="group_payment_start_date" name="group_payment_start_date" size="30" type="text" />
			  </p>
			  <p>
				<label for="group_payment_end_date">End date</label><br />
				<input class="dateM" id="group_payment_end_date" name="group_payment_end_date" size="30" type="text" />
			  </p>
			  <p><input id="group_payment_submit" name="commit" type="submit" value="Save changes" /></p>
			</form>
	</div>
</div>