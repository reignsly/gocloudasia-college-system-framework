<?php
$link = sha1('new');
$search_message = '<div class="alert alert-success"><i> '.htmlentities($search_strings).'</i>';
$search_message .='<br><a href="'.site_url("finance/remaining_balance_report/?new=".$link).'">Clear Search History</a></div>';
?>

<div id="right">
	<div id="right_top" >
	<p id="right_title">Remaining Balance Report / Search</p>
		  <!--<p id="back"></p>-->
	</div>
	<div id="right_bottom">
			<?= validation_errors() == '' ? NULL : '<div class="alert alert-danger">'.validation_errors().'</div>';?>
			<?= isset($system_message) ? $system_message : NULL;?>
			
			<form action="<?=site_url('finance/remaining_balance_report');?>" method="post">
				<p>
					<label for="group_payment_start_date">NAME</label>
					<input name="name" size="30" type="text" />
					<label for="group_payment_end_date">Student ID</label>
					<input name="student_id" size="30" type="text" />
					<input name="form_token" type="hidden" value="<?=$form_token;?>"/>
					<input id="group_payment_submit" name="search" type="submit" value="Search" />
				</p>
			</form>
		<div>
		<?=isset($search_strings) ? $search_message : NULL;?>
			<table>
				<thead>
					<tr>
						<td>Fullname</td>
						<td>Remaining Balance</td>
					</tr>
				</thead>
				<tbody>
					<? if(!empty($results)):?>
						<?foreach($results as $r):?>
						
						<tr>
							<td><?=ucwords($r->fullname);?></td>
							<td><?=$r->remaining_balance;?></td>
						</tr>
						<?endforeach;?>
					<? else:?>
						<div class="notice">No data found</div>
					<?endif;?>
				</tbody>
			</table>
		</div>
		<?=isset($pagination_links) ? $pagination_links : NULL;?>
	</div>
</div>