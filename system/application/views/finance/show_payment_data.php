<div id="right">
	<div id="right_top" >
		  <p id="right_title">Payment Report</p>
	</div>
	<div id="right_bottom">
	<?if(!empty($results)):?>
		<? $l = _se($id)?>
		<p><a href="<?=site_url('finance/generate_payment_report/?id='.$l->link.'&di='.$l->hash);?>" class="confirm" title="Export to Excel?">Export To Excel</a></p>
		<p>Start Date: <?=date('M d, Y',strtotime($results[0]->start_date));?></p>
		<p>End Date: <?=date('M d, Y',strtotime($results[0]->end_date));?></p>
		<table>
			<thead>
				<tr>
					<td>Fullname</td>
					<td>Remarks</td>
					<td>OR #</td>
					<td>&nbsp;</td>
					<td>date Paid</td>
					<td>Payments</td>
					<td>Old Accounts</td>

				</tr>
			</thead>
			<tbody>
					<? foreach($results as $r):?>
						<tr>
							<td><?=ucwords($r->first_name.' '.$r->last_name);?></td>
							<td><?=ucfirst($r->remarks);?></td>
							<td><?=ucfirst($r->or_no);?></td>
							<td>&nbsp;</td>
							<td><?=date('M d, Y',strtotime($r->stud_payment_date));?></td>
							<td class="money"><?=m($r->payments);?></td>
							<td class="money"><?=m($r->old_account);?></td>
						</tr>
						<? $payment_subtotal[] = $r->payments; ?>
						<? $oldacct_subtotal[] = $r->old_account; ?>
					<?endforeach;?>
			</tbody>
			<tfoot>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Sub Total:</td>
					<? ?>
					<td class="money"><?= m(array_sum($payment_subtotal),1);?></td>
					<td class="money"><?= m(array_sum($oldacct_subtotal),1);?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Total:</td>
					<td class="money">----</td>
					<td class="money">----</td>
					
				</tr>
			</tfoot>
		</table>
		<?else:?>
			<div class="notice">No Data Available</div>
		<?endif;?>
	
	<?=$links;?>
	
	<div class="clear"></div>
</div>

	<div class="clear"></div>
</div>
