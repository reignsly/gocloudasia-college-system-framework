<?
/*
$count = 0;
foreach($subject_student_grades as $value)
{
	if($subject_student_grades[$count]->user_id == $value->user_id)
	{
		$test[$count]['fullname'] = $value->fullname;
		$test[$count]['studid'] = $value->studid; 
		$test[$count]['user_id'] = $value->user_id; 
		$test[$count][$value->category] = $value->value;	
	}
	else
	{
		$count++;
		continue;
	}
}
echo '<pre>';
print_r($subject_student_grades);
echo '</pre>';
echo '<hr>';
*/
?>
<div id="right">

	<div id="right_top" >
		  <p id="right_title">View Subject Grades</p>
	</div>
	<div id="right_bottom">
	
	<table>
		<thead>
				<th>Subject Code</th>
				<th>Section Code</th>
				<th>Subject</th>
				<th>TIME</th>
				<th>Day</th>
				<th>Room</th>
		</thead>
		
		<tbody>
			<?if(!empty($subject_profile)):?>
				<?foreach($subject_profile as $sp):?>
					<tr>
						<td><?=$sp->sc_id;?></td>
						<td><?=$sp->code;?></td>
						<td><?=$sp->subject;?></td>
						<td><?=$sp->time;?></td>
						<td><?=$sp->day;?></td>
						<td><?=$sp->room;?></td>
					</tr>
				<?endforeach;?>
			<?else:?>
					<tr>
						<td colspan="6"> <div class="notice">No data Available</div></td>
					</tr>				
			<?endif;?>
		</tbody>
	</table>
	<br><hr><br>
	
	<table>
		<tbody>
			<tr>
				<th>Student Name</th>
				<th>Student ID</th>
				<th>Grade Category : Grade Value</th>
				<th>Remarks</th>
			</tr>
			<?if(!empty($subject_student_grades)):?>
				<? foreach($subject_student_grades as $ssg):?>
					<tr>
						<td><?=$ssg->fullname;?></td>
						<td><?=$ssg->studid;?></td>
						<td><?=$ssg->category;?>:<?=$ssg->value;?> <br></td>
						<td><?=$ssg->remarks;?><br></td>
					</tr>
				<?endforeach;?>
			<?else:?>
				<tr>
					<td colspan="7"><div class="notice">No data Available</div></td>
				</tr>
			<?endif;?>
		</tbody>
	</table>
	</div>

</div>