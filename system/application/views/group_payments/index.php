<div id="right">
		  
<div id="right_bottom">

<a href="<?php echo base_url(); ?>group_payments/create" rel="facebox" class='btn btn-default btn-sm btn-sm' ><span class = 'glyphicon glyphicon-plus'></span>&nbsp;  New Group Payments</a>
<br/>
<br/>

<!--div class="well">
	<form class="form-inline" role="form" name="myform" action="<?=base_url()?>group_payments" method="post">
		 <div class="form-group">
			<label class="sr-only" for="start_date">Start Date</label>
			<input type="text" class="form-control date_pick" id="start_date" name='end_date' placeholder="Start Date">
		  </div>
		  <div class="form-group">
			<label class="sr-only" for="end_date">End Date</label>
			<input type="text" class="form-control date_pick" id="end_date" name='end_date' placeholder="End Date">
		  </div>
		  <button type="submit" class="btn btn-default btn-sm">Search</button>
	</form>
</div-->

<table id="table">

	<tr>
    <th>Start Date</th>
    <th>End Date</th>
    <th>Action</th>
  </tr>
<tbody>
	
  <?php if($group_payments){ ?>
  <?php foreach( $group_payments as $val): ?>
    <tr>
      <td><?php echo date('Y-m-d', strtotime($val->start_date)) ; ?></td>
      <td><?php echo date('Y-m-d', strtotime($val->end_date)) ; ?></td>
      <td>
		  <a href="<?php echo base_url()."group_payments/display/".$val->id; ?>" rel="facebox" class="actionlink">Show Payments</a></li> |
		  <a href="<?php echo base_url()."group_payments/edit/".$val->id; ?>" rel="facebox" class="actionlink">Edit</a></li> |
		  <a href="<?php echo base_url()."group_payments/destroy/".$val->id; ?>" rel="facebox" class="actionlink confirm">Destroy</a>
		</td>
    </tr>
  <?php endforeach; ?>
  <?php } ?>
  
</tbody>
	<tr>
     <th>Start Date</th>
    <th>End Date</th>
    <th>Action</th>
  </tr>
</table>

<a href="<?php echo base_url(); ?>group_payments/create" rel="facebox" class='btn btn-default btn-sm btn-sm'><span class = 'glyphicon glyphicon-plus'></span>&nbsp;  New Group Payments</a>

</div>

</div>
