<div class="well">
	<a href="javascript:;" class='btn btn-xs btn-default'><b>Start Date : </b><?=date('m-d-Y',strtotime($group_payments->start_date));?></a> - <a href="javascript:;" class='btn btn-xs btn-default'><b>End Date : </b><?=date('m-d-Y',strtotime($group_payments->end_date));?></a>
</div>
<div class="well">
<table>
		<tr>
			<th>#</th>
			<th>Student Name</th>
			<th>Receipt Number</th>
			<th>Date Of Payment</th>
			<th>Mode Of Payment</th>
			<th>Remarks</th>
			<th>Amount Paid</th>
		</tr>
<?if($studentpayments):?>
<?
	$ctr=0;
	$tot_account_recivable_student = 0;
?>		
<?foreach($studentpayments as $obj):?>
		<?
			$ctr++;
			$tot_account_recivable_student += $obj->spr_ammt_paid;
		?>
		<tr>
			<td><?=$ctr?>.</td>
			<td><?=$obj->name;?></td>
			<td><?=$obj->spr_or_no;?></td>
			<td><?=date('m-d-Y', strtotime($obj->spr_payment_date));?></td>
			<td><?=$obj->spr_mode_of_payment;?></td>
			<td><?=$obj->spr_remarks;?></td>
			<td><?=number_format($obj->spr_ammt_paid, 2);?></td>

		</tr>
<?endforeach;?>
		<tr>
			<th colspan = '6' style='text-align:right'>Total</th>
			<th><div class="badge"><?=number_format($tot_account_recivable_student,2);?></div></th>
		</tr>
<?else:?>
		<tr>
			<td colspan= "7" style='text-align:center'>No record found.</td>
		</tr>
<?endif;?>
</table>
<?if($studentpayments):?>
<a target="_blank" href="<?=site_url('group_payments/print_show/'.$id.'/account_recivable_student')?>" class="btn btn-primary btn-sm"><i class="fa fa-print"></i>&nbsp; Print</a>
<?endif;?>
</div>
