<!-- <p>
  <label for="issue_comment">Comment</label><br />
  <textarea  id="comment" name="issue_comment" class="form-control" ><?= isset($issues->comment) ? $issues->comment : '' ?></textarea>
</p>
<p>
  <label for="resolved">Resolved</label><br />
  <input id="issue_resolved_yes" name="issue_resolved" type="radio" value="Yes" <?= (isset($issues) && $issues->resolved == 'Yes') ? 'checked' : ''?> />Yes
  <input id="issue_resolved_no" name="issue_resolved" type="radio" value="No" <?= isset($issues->resolved) && $issues->resolved == 'No' ? 'checked' : '' ?> />No
</p> -->

<div class="row">

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-2 control-label" for="textarea">Comment</label>
  <div class="col-md-10">                     
    <textarea class="form-control" id="comment" name="issue_comment" required ><?= isset($issues->comment) ? $issues->comment : '' ?></textarea>
  </div>
</div>

<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-2 control-label" for="radios">Is Resolved ?</label>
  <div class="col-md-10">
  <div class="radio">
    <label for="radios-0">
      <input type="radio" name="issue_resolved" id="issue_resolved_yes" value="Yes" <?= (isset($issues) && $issues->resolved == 'Yes') ? 'checked' : ''?>>
      Yes
    </label>
	</div>
  <div class="radio">
    <label for="radios-1">
      <input type="radio" name="issue_resolved" id="issue_resolved_no" value="No" <?= isset($issues->resolved) && $issues->resolved == 'No' ? 'checked' : '' ?>>
      No
    </label>
	</div>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-2 control-label" for="textarea">Remark if resolved</label>
  <div class="col-md-10">                     
    <textarea class="form-control" id="textarea" name="resolve_remark"><?= isset($issues->resolved_remark) ? $issues->resolved_remark : '' ?></textarea>
  </div>
</div>

</div>

