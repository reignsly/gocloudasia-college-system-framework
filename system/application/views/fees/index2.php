<script type="text/javascript">
  function Set_tuition_fee(data)
  {
	if($(data).is(':checked'))
	{
		var xurl = "<?=base_url()?>ajax/set_tuition_fee";
		$.ajax({
			url : xurl,
			type : 'POST',
			dataType : 'json',
			data : { id : $(data).val()},
			success : function(data){
				if(data.status)
				{
					notify_modal('Success','Record was successfully updated.');
				}
			}
		});
	}
  }
</script>
		
<?php 
	$formAttrib = array('id' => 'search_enrollee', 'class' => 'form-inline', 'method' => 'GET');
	echo form_open('fees/index/0', $formAttrib);
?>

<div class="form-group">
	<input id="lastname" class="form-control" type="text" value="<?=isset($name)?$name:''?>" name="name" placeholder="Fee Name">
</div>

<div class="form-group">
	<?php echo form_submit('submit', 'Search'); ?>
</div>

<div class="form-group">
	<a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>fees/create" rel="facebox"><span class = 'glyphicon glyphicon-plus'></span>&nbsp;  New Fee</a>
</div>

<?echo form_close();?>
<br/>
<?echo isset($links) ? $links : NULL;?>
<table id="table">
<thead>
	<tr>
		<th>Name</th>
		<th>Deduction</th>
		<th>Active</th>
		<th>Not Added to Old Students</th>
		<!--th>Is Tuition Fee per Unit</th-->
		<th colspan='2' >Action</th>
	</tr>
</thead>
<tfoot>
	<tr>
		<th>Name</th>
		<th>Deduction</th>
		<th>Active</th>
		<th>Not Added to Old Students</th>
		<!--th>Is Tuition Fee per Unit</th-->
		<th colspan='2' >Action</th>
  </tr>
</tfoot>
<tbody>
	
  <?php if($fees){ ?>
  <?php foreach( $fees as $fee): ?>
	<tr>
	  <td><?php echo $fee->name ; ?></td>
	  <td><?php $ded = ($fee->is_deduction) == 1 ? "Yes" :  "No"; echo $ded; ?></td>
	  <td><?php $pay = ($fee->is_active) == 1 ? "Yes" :  "No"; echo $pay; ?></td>
	  <td><?php $pay = ($fee->donot_show_in_old) == 1 ? "Yes" :  "No"; echo $pay; ?></td>
	  <!--td style='text-align:center'>
		<input onchange='Set_tuition_fee(this)' type='radio' name='tuition_fee' value='<?=$fee->id?>' <?=$fee->is_tuition_fee == 1 ? 'checked' : ''?>  >
	  </td-->
	  <td><a href="<?php echo base_url()."fees/edit/".$fee->id; ?>" rel="facebox" class="actionlink"><span class='glyphicon glyphicon-pencil' ></span>&nbsp;  Edit</a></td>
	  <td><a href="<?php echo base_url()."fees/destroy/".$fee->id; ?>" rel="facebox" class="actionlink confirm"><span class='glyphicon glyphicon-trash' ></span>&nbsp; Destroy</a></td>
	</td>
	</tr>
  <?php endforeach; ?>
  <?php } ?>
  
</tbody>
</table>
<?echo isset($links) ? $links : NULL;?>
