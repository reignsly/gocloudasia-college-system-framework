<ul class="nav nav-tabs">
	<li <?= ($this->router->class == "fees" && $this->router->method == 'index' && ($this->uri->segment(4) == false OR $this->uri->segment(4) == "TUITION")) ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('fees/index/0/TUITION'); ?>">Tuition</a></li>

	<li <?= ($this->router->class == "fees" && $this->router->method == 'index' && ($this->uri->segment(4) == "MISC")) ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('fees/index/0/MISC'); ?>">Miscellaneous</a></li>
	  
	<li <?= ($this->router->class == "fees" && $this->router->method == 'index' && ($this->uri->segment(4) == "LAB")) ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('fees/index/0/LAB'); ?>">Lab</a></li>
	
	<li <?= ($this->router->class == "fees" && $this->router->method == 'index' && ($this->uri->segment(4) == "NSTP")) ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('fees/index/0/NSTP'); ?>">NSTP</a></li>

	<li <?= ($this->router->class == "fees" && $this->router->method == 'index' && ($this->uri->segment(4) == "OTHER_SCHOOL")) ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('fees/index/0/OTHER_SCHOOL'); ?>">Other School Fees</a></li>

	<li <?= ($this->router->class == "fees" && $this->router->method == 'index' && ($this->uri->segment(4) == "OTHER")) ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('fees/index/0/OTHER'); ?>">Others</a></li>
</ul>
<br/>