<?=form_open('')?>
<div class="table-responsive">

<li class="list-group-item">
	<?if($student_scholarship):?>
	<?=form_submit('add_scholarship','Add Selected Scholarship')?>
<?endif;?>
	<a class="btn btn-default btn-sm" href="<?=site_url('fees/view_fees/'.$enrollment_id)?>"><i class="fa fa-arrow-left"></i>&nbsp; Cancel</a>
</li>

<table class="table">
	<tr>
		<th></th>
		<th>Name</th>
		<th>Type</th>
		<th>Percentage</th>
		<th>Applied To (Percentage Type)</th>
		<th>Amount (Cash Type)</th>
	</tr>
<?if($student_scholarship):?>
	
	<?foreach ($student_scholarship as $key => $value):?>
		
		<tr>
			<td><input type="checkbox" name='scholarship[]' value='<?=$value->id?>'></td>
			<td><?=$value->name?></td>
			<td class='info' ><?=$value->type?></td>
			<td class='<?= $value->type == "PERCENTAGE" ? 'info' : '' ;?>'><?= $value->type == "PERCENTAGE" ? $value->percentage : '' ;?></td>
			<td class='<?= $value->type == "PERCENTAGE" ? 'info' : '' ;?>'><?=$value->type == "PERCENTAGE" ? $value->applied_to: '' ?></td>
			<td class='<?= $value->type == "CASH" ? 'info' : '' ;?>'><?= $value->type == "CASH" ? m($value->cash_amount) : '' ;?></td>
		</tr>

	<?endforeach;?>
	<?=form_close();?>
<?else:?>
	<li class="list-group-item">
		No Scholarship Available.
	</li>
	<li class="list-group-item">
		<a class="btn btn-default btn-sm" href="<?=site_url('fees/view_fees/'.$enrollment_id)?>"><i class="fa fa-arrow-left"></i>&nbsp; Cancel</a>
	</li>
<?endif;?>
</table>
</div>
