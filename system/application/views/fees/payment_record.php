<div class="row">
	<?@$this->load->view('fees/student_profile_head');?>
	<div class="section-container auto" data-section data-options="deep_linking: true">
		<table class='table'>
			<tr>
				<th>Receipt Number</th>
				<th>Amount Paid</th>
				<th>Date Of Payment</th>
				<th>Mode Of Payment</th>
				<th>Check #(Checks)</th>
				<th>Remarks</th>
				<th>&nbsp;</th>
			</tr>
		<?if($payments_made):?>
			<?foreach($payments_made as $key => $pm):?>
			<tr>
				<td><?=$pm->spr_or_no;?></td>
				<td>&#8369; <?=money($pm->spr_ammt_paid);?></td>
				<td><?=date('m-d-Y', strtotime($pm->date));?></td>
				<td><?=$pm->spr_mode_of_payment;?></td>
				<td><?=$pm->spr_mode_of_payment == "CHECK" ? $pm->check_number : '';?></td>
				<td><?=$pm->spr_remarks;?></td>
				<td>
					<?if($mydepartment->stud_add_fees === "1"):?>
						<div class="btn-group btn-group-sm">
						<a href="<?=site_url('fees/show_payment/'.$enrollment_id.'/'.$pm->spr_id)?>" class="btn btn-xs btn-success">Show Payment</a>
						<a href="<?=site_url('fees/delete_payment/'.$enrollment_id.'/'.$pm->spr_id)?>" class="btn btn-danger btn-xs confirm" title="Are You sure you want to delete payment Record?">Delete</a>
						</div>
					<?else:?>
					-- No Access --
					<?endif;?>
				</td>
			</tr>
			<?endforeach;?>
		<?else:?>
			<tr>
				<td colspan='5' ><p>No Payment Record yet.</p></td>
			</tr>
		<?endif;?>
		</table>
	</div>
</div>
<?if(isset($payments_made_as_old) && $payments_made_as_old ):?>
	<h5>Payment Made in Other Accounts</h5>
	<div class="row">
		<div class="section-container auto" data-section data-options="deep_linking: true">
			<table class='table'>
				<tr>
					<th>Receipt Number</th>
					<th>Amount Paid</th>
					<th>Date Of Payment</th>
					<th>Mode Of Payment</th>
					<th>Check #(Checks)</th>
					<th>Remarks</th>
				</tr>
			<?if($payments_made_as_old):?>
				<?foreach($payments_made_as_old as $key => $pm):?>
				<tr>
					<td><?=$pm->spr_or_no;?></td>
					<td>&#8369; <?=money($pm->amount);?></td>
					<td><?=date('m-d-Y', strtotime($pm->spr_payment_date));?></td>
					<td><?=$pm->spr_mode_of_payment;?></td>
					<td><?=$pm->spr_mode_of_payment == "CHECK" ? $pm->check_number : '';?></td>
					<td><?=$pm->spr_remarks;?></td>
				</tr>
				<?endforeach;?>
			<?else:?>
				<tr>
					<td colspan='5' ><p>No Payment Record yet.</p></td>
				</tr>
			<?endif;?>
			</table>
		</div>
	</div>
<?endif;?>