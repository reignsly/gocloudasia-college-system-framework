<ul class="list-group">
	<a href="#" class="list-group-item active">
		<span class="badge">Rates</span>
	    Other Fee Name
	</a>
	<?if($other_fees):?>
		<?=form_open('')?>
		<?foreach ($other_fees as $key => $value):?>

			<li class="list-group-item">
				<span class="badge"><?=($value->value)?></span>

					<input type='checkbox' name='other_fee[]' value='<?=$value->id?>'>
					<i class="fa fa-hand-o-right"></i>&nbsp;
					<strong><?=$value->name?></strong>
			</li>
		<?endforeach;?>

		<li class="list-group-item">
			<?=form_submit('add_other_fee','Add')?>
			<a class="btn btn-default btn-sm" href="<?=site_url('fees/view_fees/'.$enrollment_id)?>"><i class="fa fa-arrow-left"></i>&nbsp; Cancel</a>
		</li>
		<?=form_close();?>
	<?else:?>
		<li class="list-group-item">
			No Fees Available.
		</li>
		<li class="list-group-item">
			<a class="btn btn-default btn-sm" href="<?=site_url('fees/view_fees/'.$enrollment_id)?>"><i class="fa fa-arrow-left"></i>&nbsp; Cancel</a>
		</li>
	<?endif;?>
</ul>