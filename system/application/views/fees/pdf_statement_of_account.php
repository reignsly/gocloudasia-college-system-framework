<style type="text/css">
	.text-center{
		text-align: center;
	}

	.text-right{
		text-align: right;
	}

	.bold{
		font-weight: bold;
	}
</style>
<table border='0' width="100%" >
	<tr>
		<td colspan="2" class="text-center bold" ><?=strtoupper($this->setting->school_name);?></td>
	</tr>
	<tr>
		<td colspan="2" class="text-center bold" >STATEMENT OF ACCOUNTS</td>
	</tr>
	<tr>
		<td colspan="2" class="text-center bold" ><br/></td>
	</tr>
	<tr>
		<td colspan="2" class="bold" ><?=$_student->profile->name?>&nbsp;<?=$_student->profile->sy_from?>-<?=$_student->profile->sy_to?></td>
	</tr>
	<tr>
		<td colspan="2" class="bold" ><?=$_student->profile->full_name?></td>
	</tr>
	<tr>
		<td class="bold" >ID NO:<?=$_student->profile->studid?></td>
		<td class="bold" >COURSE:<?=$_student->profile->course?></td>
	</tr>
	<tr>
		<td colspan="2" class="text-center bold" style="border-bottom:1px dotted gray;" ></td>
	</tr>

	<tr>
		<td class="bold info" >Previous Balance</td>
		<td class="text-right info" ><?=money($_student->student_payment_totals->total_previous_amount)?></td>
	</tr>

	<tr>
		<td class="bold info">Amount Due</td>
		<td class="text-right info" ><strong><?=money($_student->student_payment_totals->total_amount_due)?></strong></td>
	</tr>

	
	<?if($student_of_account->tuition_fee):?>
	<?$tuition_fee = (object)$student_of_account->tuition_fee;?>
	<tr>
		<td class="bold" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=$tuition_fee->name?> (<?=money($_student->profile->tuition_fee_multiplier);?> * <?=money($_student->profile->total_units);?>)</td>
		<td class="text-right" ><?=money($_student->profile->tuition_fee)?>&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<?endif;?>

	
	<tr>
		<td class="bold" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Lab Fee</td>
		<td class="text-right bold" ><?=money($_student->profile->lab_fee)?>&nbsp;&nbsp;&nbsp;</td>
	</tr>

	
	<?if(isset($student_of_account->lab_fee) && $student_of_account->lab_fee):?>
		
		<?foreach ($student_of_account->lab_fee as $key => $value):?>
		<?if(strtolower($key) === "total"){ continue; }?>
		<tr>
			<?$value = (object)$value;?>
			<td class="italic" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Lab Fee<?=$value->name?> (<?=money($value->rate);?> * <?=money($value->units);?>)</td>
			<td class="text-right" ><?=money($value->amount)?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<?endforeach;?>
		
	<?endif;?>

	
	<tr>
		<td class="bold" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Miscellaneous Fee</td>
		<td class="text-right bold" ><?=money($_student->profile->misc_fee)?>&nbsp;&nbsp;&nbsp;</td>
	</tr>
	
	<?if(isset($student_of_account->misc_fee) && $student_of_account->misc_fee):?>
		
		<?foreach ($student_of_account->misc_fee as $key => $value):?>
		<?if(strtolower($key) === "total"){ continue; }?>
		<tr>
			<?$value = (object)$value;?>
			<td class="italic" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=$value->name?></td>
			<td class="text-right" ><?=money($value->amount)?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<?endforeach;?>
		
	<?endif;?>
	
	<tr>
		<td class="bold info" >Discounts / Adjustments</td>
		<td class="text-right bold info" ><?=money($_student->student_payment_totals->total_deduction+$_student->student_payment_totals->total_scholarship+$_student->student_payment_totals->total_excess_amount)?></td>
	</tr>

	
	<tr>
		<td class="bold" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Scholarship <?=$student_of_account->scholarships?'':' (None) '?></td>
		<td class="text-right bold" ><?=money($_student->student_payment_totals->total_scholarship)?>&nbsp;&nbsp;&nbsp;</td>
	</tr>
	
	<?if(isset($student_of_account->scholarships) && $student_of_account->scholarships):?>
		
		<?foreach ($student_of_account->scholarships as $key => $value):?>
		<?if(strtolower($key) === "total"){ continue; }?>
		<tr>
			<td class="italic" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=$value->scholarship_name?></td>
			<td class="text-right" ><?=money($value->scho_amount)?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<?endforeach;?>
		
	<?endif;?>

	
	<tr>
		<td class="bold" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Deduction <?=$student_of_account->deductions?'':' (None) '?></td>
		<td class="text-right bold" ><?=money($_student->student_payment_totals->total_deduction)?>&nbsp;&nbsp;&nbsp;</td>
	</tr>
	
	<?if(isset($student_of_account->deductions) && $student_of_account->deductions):?>
		
		<?foreach ($student_of_account->deductions as $key => $value):?>
		<?if(strtolower($key) === "total"){ continue; }?>
		<tr>
			<td class="italic" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=$value->deduction_name?></td>
			<td class="text-right" ><?=money($value->amount)?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<?endforeach;?>
		
	<?endif;?>

	
	<tr>
		<td class="bold" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Advanced Payments <?=$student_of_account->excess_payments?'':' (None) '?></td>
		<td class="text-right bold" ><?=money($_student->student_payment_totals->total_deduction)?>&nbsp;&nbsp;&nbsp;</td>
	</tr>
	
	<?if(isset($student_of_account->excess_payments) && $student_of_account->excess_payments):?>
		
		<?foreach ($student_of_account->excess_payments as $key => $value):?>
		<?if(strtolower($key) === "total"){ continue; }?>
		<tr>
			<td class="italic" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=$value->year?> (<?=$value->sy_from?>-<?=$value->sy_to?>)</td>
			<td class="text-right" ><?=money($value->amount)?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<?endforeach;?>
		
	<?endif;?>
	
	<tr>
		<td class="bold info" >Clearance / Other fees</td>
		<td class="text-right bold info" ><?=money($_student->profile->other_fee)?></td>
	</tr>
	
	<?if(isset($student_of_account->other_fee) && $student_of_account->other_fee):?>
		
		<?foreach ($student_of_account->other_fee as $key => $value):?>
		<?if(strtolower($key) === "total"){ continue; }?>
		<?$value = (object)$value;?>
		<tr>
			<td class="" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=$value->name?></td>
		<td class="text-right" ><?=money($value->amount)?>&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<?endforeach;?>
		
	<?endif;?>
	
	<tr>
		<td class="bold info" >Total Amount Paid </td>
		<td class="text-right bold info" ><?=money($_student->student_payment_totals->total_amount_paid)?></td>
	</tr>
	
	<?if(isset($student_of_account->payment_record) && $student_of_account->payment_record):?>
		
		<?$ctr = 1;?>
		<?foreach ($student_of_account->payment_record as $key => $value):?>
		<? $value = (object)$value; ?>
		<tr>
			<td class="" >
				<div class="row">
					<div class="col-md-3" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=$ctr++;?>. &nbsp; <?=$value->spr_or_no?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
					<div class="col-md-3" ><?=date('m-d-Y',strtotime($value->spr_payment_date))?></div>
				</div>
			</td>
		<td class="text-right" ><?=money($value->spr_ammt_paid)?>&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<?endforeach;?>
		
	<?endif;?>
	
	<tr>
		<td class="bold info" >Current Balance </td>
		<td class="text-right bold info" ><?=money($_student->student_payment_totals->total_balance)?></td>
	</tr>
	
	<tr>
		<td class="bold info" >Required for '<?=$this->current_grading_period->grading_period;?>'</td>
		<td class="text-right bold info" ><?=money($req_payment)?></td>
	</tr>
	
	<tr>
		<td colspan="2" class="text-center bold" >In-charge - Accounting</td>
	</tr>
</table>