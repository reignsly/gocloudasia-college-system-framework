<script type="text/javascript">
	$(document).ready(function(){
		$('.clear_forms').submit(function(event){
			var xtitle = $(this).attr('title');
			var ans = confirm('Are you sure?');
			if(ans){

			}else{
				event.preventDefault();
			}
		})
	});
</script>
<div class="row">
	<?@$this->load->view('fees/student_profile_head');?>
	<div class="section-container auto" data-section data-options="deep_linking: true">
		<table class='table'>
			<tr>
					<th>Check Date</th>
					<th>Check Number</th>
					<th>Amount</th>
					<th>Remarks</th>
					<th>Action</th>
			</tr>
		<?if($pending_checks):?>	
			<?foreach ($pending_checks as $key => $value): ?>
				<tr>
						<td><?=date('d-m-Y',strtotime($value->check_date))?></td>
						<td><?=$value->check_number?></td>
						<td><strong><?=money($value->spr_ammt_paid);?> </strong></td>
						<?=form_open('fees/clear_check/'.$enrollment_id.'/'.$value->spr_id, 'name=form_'.$value->spr_id.' class="clear_forms"');?>
						<td><input type='text' name='remarks' value='<?=$value->check_remarks?>' style='width:100%;'></td>
						<td>
								<?=form_submit('clear_check', 'Clear Check & Apply','class="btn btn-success" title="Are you sure to Clear and Apply this check to student fees?"');?>
								<?=form_submit('decline_check', 'Decline Check','class="btn btn-danger" title="Are you sure to decline/reject this check?"');?>
						</td>
						<?=form_close();?>
				</tr>
			<?endforeach;?>
		<?else:?>
			<tr>
				<td colspan='5' ><p>No pending checks available</p></td>
			</tr>
		<?endif;?>
		</table>
	</div>
</div>