<?=form_open('','class="form-horizontal"')?>
<fieldset>

<!-- Prepended text-->
<div class="form-group">
  <div class="col-md-8">
    <div class="input-group">
      <span class="input-group-addon">Deduction Name</span>
      <input id="deduction_name" name="deduction_name" class="form-control" placeholder="Required" type="text" value="<?=set_value('deduction_name')?>" required>
    </div>
  </div>
</div>

<!-- Prepended text-->
<div class="form-group">
  <div class="col-md-8">
    <div class="input-group">
      <span class="input-group-addon">Remarks</span>
      <input id="remarks" name="remarks" class="form-control" placeholder="Required" type="text" value="<?=set_value('remarks')?>" required>
    </div>
  </div>
</div>
<!-- Prepended text-->
<div class="form-group">
  <div class="col-md-8">
    <div class="input-group">
      <span class="input-group-addon">Amount</span>
      <input id="amount" name="amount" class="form-control currency" placeholder="0.00" type="text" value="<?=set_value('amount')?>" required>
    </div>
    <p class="help-block">Deduction Amount must be numeric</p>
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <div class="col-md-8">
    <?=form_submit('add_deduction','Save and Exit','class="btn btn-default btn-sm"')?>
    <?=form_submit('add_deduction_add','Save and Add Another','class="btn btn-default btn-sm"')?>
    <a class="btn btn-default btn-sm" href="<?=site_url('fees/view_fees/'.$enrollment_id)?>"><i class="fa fa-arrow-left"></i>&nbsp; Cancel </a>
  </div>
</div>

</fieldset>
</form>
