<div class="row">
	<?@$this->load->view('fees/student_profile_head');?>
	<?=form_open();?>
	<div class="section-container auto" data-section data-options="deep_linking: true">
		<table class='table'>
			<tr>
					<th>Select</th>
					<th>Academic Year</th>
					<th>Year Level</th>
					<th>Total Amount Due</th>
					<th>Total Deduction</th>
					<th>Total Amount Paid</th>
					<th>Excess Amount</th>
			</tr>
		<?if($account_with_excess):?>	
			<?foreach ($account_with_excess as $key => $value): ?>
				<tr>
					<td><input type="checkbox" class="select_check" name="excess[<?=$value->id?>]" value="<?=$value->id?>"></td>
					<td><?=$value->sy_from;?>-<?=$value->sy_to;?></td>
					<td><?=$value->year;?></td>
					<td><?=money($value->total_amount_due,1);?></td>
					<td><?=money(($value->total_deduction+$value->total_scholarship+$value->total_excess_amount),1);?></td>
					<td><?=money($value->total_amount_paid,1);?></td>
					<td><strong><?=money(($value->total_balance*-1),1);?></strong></td>
				</tr>
			<?endforeach;?>
		<?else:?>
			<tr>
				<td colspan='7' ><p>No Excess Payments available</p></td>
			</tr>
		<?endif;?>
		</table>
		<?if($account_with_excess):?>	
		<input type="submit" name="add_excess_accounts" id="add_excess_accounts" value="Add Selected As Payments" disabled />
		<?endif;?>
		<a class="btn btn-sm btn-default" href="<?=site_url('fees/view_fees/'.$_student->enrollment_id)?>">Go Back to Fees</a>
	</div>
	<?=form_close();?>
</div>
<script type="text/javascript">
	$('.select_check').on('change', function(){
		var x = $('.select_check:checked').size();
		if(x > 0){
			$('#add_excess_accounts').attr('disabled', false);
		}else{
			$('#add_excess_accounts').attr('disabled', true);
		}
	})
</script>