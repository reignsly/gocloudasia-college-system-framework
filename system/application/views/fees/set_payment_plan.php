<script type="text/javascript">
	$(document).ready(function(){
		$('#myTab a:first').tab('show')
	})
</script>
<div class="row">
	<?@$this->load->view('fees/student_profile_head');?>
</div>

<div class="row">
	<a href="<?=site_url('fees/view_fees/'.$enrollment_id)?>" class="btn btn-sm btn-default"><span class="entypo-cancel"></span>&nbsp; Cancel</a>	
</div><p></p>

<div class="row">
	<?if($payment_plans):?>

		<ul class="nav nav-tabs" id="myTab">
			<?$ctr=0;?>
			<?foreach ($payment_plans as $key => $value):?>	
				<? $xactive = ++$ctr == 1 ? 'active' : ''; ?>
				<li><a class="<?=$xactive;?>" href="#tab_<?=$value->id?>" data-toggle="tab"><?=$value->name?></a></li>
			<?endforeach;?>
		</ul>
		<br/>
		<div class="tab-content">
			<?$ctr=0;?>
			<?foreach ($payment_plans as $key => $value):?>
				<?if(isset($payment_division[$value->id]) && $payment_division[$value->id]):?>
					<? $xactive = ++$ctr == 1 ? 'active' : ''; ?>
					<div class="tab-pane <?=$xactive;?>" id="tab_<?=$value->id?>">
						<table>
							<tr>
								<th>Name</th>
								<th>Amount</th>
							</tr>
							<?foreach ($payment_division[$value->id] as $key => $div):?>
								<tr>	
									<td><?=$div->name?></td>
									<td><strong><?=peso();?><?=money($div->value)?></strong></td>
								</tr>	
							<?endforeach;?>
						</table>
						<?=form_open('')?>
							<input type="submit" name="set_payment_plan" value="Choose this as Payment Plan" />
							<input type="hidden" value="<?=$value->id;?>" name="payment_plan_id" />
						<?=form_close();?>
					</div>
				<?endif;?>
			<?endforeach;?>
		</div>
	<?endif;?>
</div>