<?@$this->load->view('fees/student_profile_head') ?>

<div class="row">
	<div class="well">
		<div class="btn-group">
			<a href="<?=site_url('fees/view_fees/'.$_student->enrollment_id)?>" class="btn btn-sm btn-bitbucket"><i class="glyphicon glyphicon-asterisk"></i>&nbsp; View Fees</a>	
			<a target="_blank" href="<?=site_url('pdf/student_ledger/'.$_student->enrollment_id)?>" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-print"></span>&nbsp; Print Ledger</a>	
			<a target="_blank" href="<?=site_url('excels/student_ledger/'.$_student->enrollment_id)?>" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-download-alt"></span>&nbsp; Download (XLS)</a>	
		</div>

	</div>
</div>
<div class="row">
	<div class="table-responsive">
		<table class="table table-condensed table-hover table-padless">
			<thead>
			<tr>
				<th>School Year</th>
				<th>Year - Semester</th>
				<th>Date</th>
				<th>Or. No.</th>
				<th>Description</th>
				<th>Amount</th>
			</tr>
			</thead>

			<?if($student_ledger):?>
				<?$outstanding_balance = 0;?>
				<?foreach ($student_ledger as $k => $val):?>
					<?php
						$sy = $val['sy'];
						$en = $val['enrollments'];
					?>
					<!-- <tr>
						<td><strong><?=$sy->sy_from?>-<?=$sy->sy_to?></strong></td>
						<td colspan="5"></td>
					</tr> -->

					<?if($en):?>
						<?foreach ($en as $e => $eval):?>
								<?php
									$combined = isset($eval['combined'])?$eval['combined']:false;
									$profile = $eval['profile'];
									$totals = $eval['totals'];
									$outstanding_balance += $totals->total_balance;
								?>
								<tr>
									<td class="bold" ><?=$sy->sy_from?>-<?=$sy->sy_to?></td>
									<td>
										<a href="#"><?=$profile->year?> - <?=$profile->semester?></a> &nbsp;
										<a href="<?=site_url('fees/statement_of_account/'.$profile->id)?>" class="xconfirm" title="View Statement of Account?"><span class="glyphicon glyphicon-log-in"></span></a> &nbsp;
										<a target="_blank" href="<?=site_url('fees/print_statement_of_account/'.$profile->id)?>" class="xconfirm" title="Print Statement of Account?"><span class="glyphicon glyphicon-print"></span></a>
									</td>
									<td class="bold">Total Tuition and Fees</td>
									<td></td>
									<td></td>
									<td class="text-right" ><span class="badge"> <?=peso().m($profile->total_plan_due)?></span></td>
								</tr>
								<?if(isset($combined) && $combined):?>
									<?foreach ($combined as $c => $cval):?>
										<?php
											$date = date('m/d/Y', strtotime($cval['date']));
											$value = $cval['value'];
											$type = $cval['type'];
											$amount = 0;
											$remarks = 0;
											$or_no = "";

											switch ($type) {
												case 'payment':
													$or_no = $value->spr_or_no;
													$remarks = $value->spr_remarks;
													$amount = $value->spr_ammt_paid;
													break;
												case 'scholarship':
													$or_no = "";
													$remarks = $value->scholarship_name;
													$amount = $value->scho_amount;
													break;
												case 'deduction':
													$or_no = "";
													$remarks = $value->deduction_name;
													$amount = $value->amount;
													break;
											}
										?>
										<tr>
											<td></td>
											<td></td>
											<td><?=$date?></td>	
											<td><?=$or_no?></td>	
											<td><?=$remarks?></td>			
											<td class ="text-right" >- &nbsp; <?=m($amount)?></td>			
										</tr>
									<?endforeach;?>
								<?endif;?>
								
								<tr class="">
									<td colspan="5" class="bold text-right">Sub-Total</td>
									<td class="text-right" ><span class="bold"> <?=peso().m($totals->total_deduction_amount + $totals->total_amount_paid)?></span></td>
								</tr>

								<?if($totals->total_balance):?>
									<tr class="danger">
										<td colspan="5" class="bold text-right">Previous Balance</td>
										<td class="text-right" ><span class="badge alert-danger"> <?=peso().m($totals->total_balance)?></span></td>
									</tr>
								<?endif;?>

						<?endforeach;?>
					<?else:?>
						<td colspan="6" class="text-center bold" >No Record for this Year</td>
					<?endif;?>

				<?endforeach;?>
				<tfoot>
					<tr>
						<th colspan="5" class="bold text-center"><h5>OUTSTANDING BALANCE as of <?=date('F, d Y')?> </h5></th>
						<th class="text-right bold"><span class="badge alert-danger bold"><h5><?=peso().m($outstanding_balance)?></span></h5></th>
					</tr>
				</tfoot>	
			<?else:?>
				<tr>
					<td colspan="6" class="text-center bold" >No History Found</td>
				</tr>
			<?endif;?>
		</table>
	</div>
</div>