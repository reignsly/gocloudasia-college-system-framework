<script type="text/javascript">
  function Set_tuition_fee(data)
  {
	if($(data).is(':checked'))
	{
		var xurl = "<?=base_url()?>ajax/set_tuition_fee";
		$.ajax({
			url : xurl,
			type : 'POST',
			dataType : 'json',
			data : { id : $(data).val()},
			success : function(data){
				if(data.status)
				{
					notify_modal('Success','Record was successfully updated.');
				}
			}
		});
	}
  }
</script>

<?@$this->load->view('fees/_tab')?>
		
<?php 
	$formAttrib = array('id' => 'search_enrollee', 'class' => 'form-inline', 'method' => 'GET');
	echo form_open('fees/index/0', $formAttrib);
?>

<div class="form-group">
	<input id="lastname" class="form-control" type="text" value="<?=isset($name)?$name:''?>" name="name" placeholder="Fee Name">
</div>

<div class="form-group">
	<?php echo form_submit('submit', 'Search'); ?>
</div>

<div class="form-group">
	<a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>fees/create/<?=$fee_type?>" rel="facebox"><span class = 'glyphicon glyphicon-plus'></span>&nbsp;  New Fee</a>
</div>

<?echo form_close();?>
<br/>
<?echo isset($links) ? $links : NULL;?>
<div class="table-responsive">
<table id="table" class='table' >
<thead>
	<tr>
		<th>Name</th>
		<th>Default Value</th>
		<th>Active</th>
		<th colspan='2' >Action</th>
	</tr>
</thead>
<tfoot>
	<tr>
		<th>Name</th>
		<th>Default Value</th>
		<th>Active</th>
		<th>Action</th>
  </tr>
</tfoot>
<tbody>
	
  <?php if($fees){ ?>
  <?php foreach( $fees as $fee): ?>
	<tr>
		<td><?php echo $fee->name ; ?></td>
		<td><?php echo m($fee->value) ; ?></td>
		<td><?php $pay = ($fee->is_active) == 1 ? "Yes" :  "No"; echo $pay; ?></td>
		<td>
			<div class="btn-group">
				<a class="btn btn-xs btn-default" href="<?php echo base_url()."fees/edit/".$fee->id."/".$fee_type; ?>" rel="facebox" ><span class='glyphicon glyphicon-edit' ></span>&nbsp;  Edit</a>
				<a class="btn btn-xs btn-danger confirm" href="<?php echo base_url()."fees/destroy/".$fee->id."/".$fee_type; ?>" rel="facebox" ><span class='glyphicon glyphicon-remove' ></span>&nbsp; Destroy</a>	
			</div>

		</td>
	</tr>
  <?php endforeach; ?>
  <?php } ?>
  
</tbody>
</table>
</div>
<?echo isset($links) ? $links : NULL;?>
