<style>
	input#amount_money
	{
		padding:10px 20px;
		font-size:30px;
		font-family:DigitaldreamFatNarrowRegular;
		text-align:right;
	}
	span#amount_money_span
	{
		position:absolute;
		font-size:30px;
		margin-top:12px;
		margin-left:12px;
	}
</style>

<div class="row">
	<?@$this->load->view('fees/student_profile_head');?>
	<div class="section-container auto" data-section data-options="deep_linking: true">
		<ul class="nav nav-tabs">
		  <li class="active"><a href="#">Fees</a></li>
		</ul>
		<hr/>
		<div>
			<?@$this->load->view('fees/apr_structured_edit')?>
		</div>
	</div>
</div>