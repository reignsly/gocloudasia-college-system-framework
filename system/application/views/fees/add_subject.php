<?php
	$subjects = $_student->student_subjects;
	$total_units = $_student->total_units;
	$total_lab = $_student->total_lab;
	$total_lec = $_student->total_lec;
?>
<?@$this->load->view('layouts/_student_data'); ?>

<div class="tab-content">
	<?@$this->load->view('layouts/student_data/_student_subjects');?>

	<p></p>
	<br>
	
	<?if(isset($mydepartment) && $mydepartment->stud_edit_subject === "1"):?>
		<?=panel_head2('Search Subject','','default');?>
			<div class="table-responsive">
				<form action="<?=site_url('fees/add_subject/');?>/<?=$_student->enrollment_id;?>/0/<?=$search_type?>" method="GET" class="form-inline" id="search_form" >
				<div class="row">
		    	<div class="col-md-12">
						
						<div class="form-group">
							<input class='form-control' id="search_code_eq" name="code" size="30" type="text"  value="<?=isset($_GET['code'])?$_GET['code']:''?>" placeHolder="Course No" />
						</div>
						
						<div class="form-group">
							<input class='form-control' id="search_subject_eq" name="subject" size="30" type="text" value="<?=isset($_GET['subject'])?$_GET['subject']:''?>" placeHolder="Description" />
						</div>

						<div class="form-group">
							<input class='form-control' id="search_instructor" name="instructor" size="30" type="text" value="<?=isset($_GET['instructor'])?$_GET['instructor']:''?>" placeHolder="Instuctor" />
						</div>

		    	</div>
		    </div>
				<input type="hidden" value="" name="subject_ay_id" id="subject_ay_id" />
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<input class='form-control' id="search_time" name="time" size="30" type="text" value="<?=isset($_GET['time'])?$_GET['time']:''?>" placeHolder="Time" />
						</div>
						
						<div class="form-group">
							<input class='form-control' id="search_day" name="day" size="30" type="text"  value="<?=isset($_GET['day'])?$_GET['day']:''?>" placeHolder="Day" />
						</div>

						<div class="form-group">
							<input id="search_submit" name="search_subjects" type="submit" value="Search" />
						</div>

						<div class="form-group">
							<button type='button' href="" class="btn btn-sm btn-default custom-bs-modal" modal-title='Curriculum List' modal-id ='modalCurriculum' modal-size = 'wide'>View Curriculum</a>
						</div>

						<div class="form-group">
								<a href="javascript:;" id="clear_btn" class='btn btn-sm btn-default' >Clear</a>
								<script type="text/javascript">
									$('#clear_btn').on('click',function(e){
										e.preventDefault();
										$('#search_form input[type="text"]').val('');
									})
								</script>
						</div>
					</div>
				</div>
				</form>
				<?=$links?>
				<div class="table-responsive">
					<table class="table">
					  <tr class="gray bold" >
					    <td>Course No.</td>
					    <td>Description</td>
					    <td>Unit</td>
					    <td>Lec</td>
					    <td>Lab</td>
					    <td>Time</td>
					    <td>Day</td>
					    <td>Room</td>
					    <td>Load</td>
					    <td>Instructor</td>
					    <td>Action</td>
					  </tr>  
						<?php if(!empty($results)):?>
							<?php foreach($results as $s):?>
								<?php $xtyle = $s->subject_taken >= $s->original_load ?  "danger" : '';	?>
								<tr class="<?=$xtyle?>">
									  <td><?php echo $s->code;?></td>
									  <td><?php echo $s->subject;?></td>
									  <td><?php echo $s->units;?></td>
									  <td><?php echo $s->lec;?></td>
									  <td><?php echo $s->lab;?></td>
									  <td><span><?=_convert_to_12($s->time, $s->is_open_time);?></span></td>
									  <td><?php echo $s->day;?></td>
									  <td><?php echo $s->room;?></td>
									  <td class="<?php echo $s->subject_taken < $s->original_load ? '' : 'text-danger' ?>" ><span><?=$s->subject_taken;?>/<?=$s->original_load;?></span></td>
									  <td><?php echo $s->instructor;?></td>
									  <td>
									  	<?if($s->subject_taken >= $s->original_load):?>
									  		<!-- <input type="submit" onclick="$('#subject_ay_id').val($(this).attr('xid').val())" xid="<?=$s->id;?>" name="add_subject" value="Add" /> -->
									  		<a class="btn btn-sm btn-default confirm" title="This subject is already full, do you still want to add and override the subject load?" href="<?=site_url('fees/save_student_subject/'.$_student->enrollment_id."/".$s->id."/".$page)?>"><div style="color:red;"><i class="fa fa-plus"></i>&nbsp; Add</div></a>
										<?else:?>
											<!-- <input type="submit" onclick="$('#subject_ay_id').val($(this).attr('xid').val())" xid="<?=$s->id;?>" name="add_subject" value="Add" /> -->
											<a class="btn btn-sm btn-default confirm" href="<?=site_url('fees/save_student_subject/'.$_student->enrollment_id."/".$s->id."/".$page)?>"><i class="fa fa-plus"></i>&nbsp; Add</a>
										<?endif;?>
										
									  </td>
								</tr>
							<?php endforeach;?>
						<?php else:?>
							<tr>
								<td colspan="11" ><span class="iconicfill-x"></span>&nbsp; No Subject found. Edit filter and click 'search'</td>
							</tr>
						<?php endif;?>
					</table>	
				</div>
			</div>
		<?=panel_tail2();?>
	<?endif;?>
</div>
<?@$this->load->view('layouts/student_data/_curriculum');?>	