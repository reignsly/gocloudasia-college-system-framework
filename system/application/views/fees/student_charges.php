<!--
<?php echo form_open('') ?>
	<div class="row">
		<div class="col-lg-2">
			<label for="studid">Student ID</label>
			<input type="text" name="studid">
		</div>
		<div class="col-lg-3">
			<label for="fullname">Name</label>
			<input type="text" name="fullname">
		</div>

		<div class="col-lg-3">
			<label for="fullname">Course</label>
			<?php echo course_dropdown('course_id','','',' - All - '); ?>
		</div>

		<div class="col-lg-3">
			<label for="year_id">Year</label>
			<?php echo year_dropdown('year_id','','',' - All - '); ?>
		</div>

		<div class="col-lg-1">
			<label for="search"></label>
			<input type="submit" value="Search" name='search'>
		</div>
	</div>
<?php echo form_close(); ?>
<p></p>
-->
<div class="table-responsive">
	<table class="table table-condensed table-bordered">
		<thead>
			<tr>
				<th rowspan="2" >Student ID</th>
				<th rowspan="2" >Name</th>
				<th rowspan="2" >Course Code</th>
				<th rowspan="2" >Year Level</th>
				<th colspan="4" style='text-align:center' >Fees</th>
				<th rowspan="2" >Other / Clearance Fee</th>
				<th rowspan="2" >Old Accounts</th>
				<th colspan="3" style='text-align:center' >Deductions</th>
				<th colspan="3" style='text-align:center' >Totals</th>
			</tr>
			<tr>
				<th>Tuition</th>
				<th>Lab</th>
				<th>Misc</th>
				<th>Other School Fee</th>
				<th>Deduction</th>
				<th>Scholarship</th>
				<th>Excess Payment</th>
				<th>Total Payable</th>
				<th>Total Deduction</th>
				<th>Amount Due</th>
			</tr>
		</thead>
		<?if($enrollments):?>
			<tbody>
				<?foreach ($enrollments as $k => $v):?>
					<?php
						$profile = $v['profile'];
						$fee = $_student_fees = $v['fees'];
						$fee_totals = $fee->totals;
						$total = $v['total'];
						// vp($total);

						$tuition_fee = $fee_totals['tuition_fee'];
				    $lab_fee = $fee_totals['lab_fee'];
				    $misc_fee = $fee_totals['misc_fee'];
				    $other_school_fee = $fee_totals['other_school_fee'];
				    $other_fee = $fee_totals['other_fee'];
				    $nstp_fee = $fee_totals['nstp_fee'];
				    $old_accounts = $fee_totals['old_accounts'];
				    $deductions = $fee_totals['deductions'];
				    $scholarships = $fee_totals['scholarships'];
				    $excess = $fee_totals['excess'];
					?>
					<tr>
						<td class='bold' ><?php echo $profile->studid ?></td>
						<td class='bold' ><?php echo ucwords($profile->fullname) ?></td>
						<td><?php echo $profile->course_code ?></td>
						<td><?php echo $profile->year ?></td>
						<td align="right" class='bold' ><?php echo num_format(floatval($tuition_fee));?></td>
						<td align="right" class='bold' ><?php echo num_format(floatval($lab_fee));?></td>
						<td align="right" class='bold' ><?php echo num_format(floatval($misc_fee));?></td>
						<td align="right" class='bold' ><?php echo num_format(floatval($other_school_fee));?></td>
						<td align="right" class='bold' ><?php echo num_format(floatval($other_fee));?></td>
						<td align="right" class='bold' ><?php echo num_format(floatval($old_accounts));?></td>
						<td align="right" class='bold' ><?php echo num_format(floatval($deductions));?></td>
						<td align="right" class='bold' ><?php echo num_format(floatval($scholarships));?></td>
						<td align="right" class='bold' ><?php echo num_format(floatval($excess));?></td>
						<td align="right" class='bold' ><?php echo num_format(floatval($total->total_amount_due));?></td>
						<td align="right" class='bold' ><?php echo num_format(floatval($total->total_deduction_amount));?></td>
						<td align="right" class='bold' ><?php echo num_format(floatval($total->total_amount_due - $total->total_deduction_amount));?></td>
					</tr>
				<?endforeach;?>
			</tbody>
		<?endif;?>
	</table>

	<?php if ($enrollments): ?>
		<p class='bold' >Total Payable Amount: &nbsp; <span class='badge'><?php echo num_format(floatval($total_payable));?></span></p>
		<p class='bold' >Total Deduction Amount: &nbsp; <span class='badge'><?php echo num_format(floatval($total_deduction));?><span></p>
		<p class='bold' >Total Amount Due : &nbsp; <span class='badge'><?php echo num_format(floatval($total_amount_due));?><span></p>
	<?php endif ?>
</div>