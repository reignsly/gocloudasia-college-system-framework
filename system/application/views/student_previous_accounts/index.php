<table >
	<tr>
    <th>Previous Balance</th>
    <th>Action</th>
	</tr>
	
  <?php if($student_previous_accounts){ ?>
  <?php foreach( $student_previous_accounts as $previous_account): ?>
    <tr>
      <td><b><?php echo number_format($previous_account->value, 2, '.', '') ; ?></b></td>
      <td>
      <?//= badge_link("student_previous_accounts/destroy/".$previous_account->id .'/'.$enrollment_id, 'danger confirm', 'delete'); ?>
	  
	  <?= badge_link("student_previous_accounts/view_previous_balance/".$previous_account->id .'/'.$enrollment_id, 'success', 'View Enrollment Profile'); ?>
   	</td>
    </tr>
  <?php endforeach; ?>
  <?php }else{ ?>
    <tr><td colspan=3 class="text-center">No Previous Account</td></tr>
  <?php } ?>
</table>

<p>
<a href="<?php echo base_url(); ?>fees/view_fees/<?= $enrollment_id ?>" class="btn btn-default btn-sm"> Back to Student Fees</a>
</p>

