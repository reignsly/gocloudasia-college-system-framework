<ul class="nav nav-tabs">
	<li <?= ($this->router->class == "student_master_list" && $this->router->method == 'index') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('student_master_list/index'); ?>">ALL</a>
	</li>
	
	<li <?= ($this->router->class == "student_master_list" && $this->router->method == 'year') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('student_master_list/year'); ?>">By Year</a>
	</li>
	
	<li <?= ($this->router->class == "student_master_list" && $this->router->method == 'course') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('student_master_list/course'); ?>">By Course</a>
	</li>
</ul>
<br>
