<table>
  <tr>
    <th>#</th>
    <th>Student</th>
    <th>Preliminary</th>
    <th>Midterm</th>
    <th>Finals</th>
    <th>Remarks</th>
  </tr>
  <? $num=0 ?>
  <?if($students):?>
  <? foreach($student_grades as $sg ) :?>
  <tr>
    <td><?= $num += 1 ?></td>
    <td><?= $sg['name'] ?><br /> <?= $sg['studid'] ?></td>
    <? foreach($sg['grades'] as $sgg ) :?>
    <td><?= $sgg ?></td>
    <? endforeach ;?>
    <td><?= $sg['remarks'] ?></td>
  </tr>
  <? endforeach ;?>
  <?endif;?>
</table>

