<?$this->load->view('layouts/menu_creator/_header');?>
<?=form_open();?>
<table>
	<tr>
		<th>Department</th>
		<td><?=$department?></td>
	</tr>
	<tr>
		<th>Header Menu Caption</th>
		<td><input type='text' name='menu[caption]' class='form-control' ></td>
	</tr>
	<tr>
		<th>Menu Group</th>
		<td><input type='text' name='menu[menu_grp]' class='form-control' ></td>
	</tr>
	<tr>
		<th>Menu Group of it's submenu</th>
		<td><input type='text' name='menu[menu_sub]' class='form-control' ></td>
	</tr>
	<tr>
		<th>Menu Order</th>
		<td><input type='number' name='menu[menu_num]' class='form-control numeric' ></td>
	</tr>
	<tr>
		<th></th>
		<td>
			<?=form_submit('submit','Save')?>
			<a class='btn btn-default btn-sm' href='<?=base_url()?>/menu_creator'>Bact to Menus</a>
		</td>
	</tr>
</table>
<?=form_close();?>
<?$this->load->view('layouts/menu_creator/_footer');?>