<?echo form_open('','class="form-horizontal"');?>
    <div class="row">
      <div class="table-responsive">
        <table class="table">
          <tr>
            <td>
              <div class="row">
                <div class="col col-md-2">
                  <strong>Main Menu Source : </strong>
                </div>
                <div class="col col-md-5">
                  <?=form_dropdown('selected_id', $main_menus, ($selected)?$selected->id:'','required');?>
                </div>
                <div class="col col-md-2">
                  <button class="btn btn-sm btn-primary" type="submit" value="select_source" name="select_source">Submit</button>
                  <a href="<?=site_url('menu_creator')?>" class="btn btn-default">Cancel</a>
                </div>
              </div>
            </td>
          </tr>
        </table>
      </div>
<?=form_close();?>
<?echo form_open('','class="form-horizontal"');?>
      <div class="table-responsive">
        <table class="table">
          <tr>
            <td>
              <?if($selected):?>
                <fieldset>

                    <!-- Text input-->
                    <div class="form-group">
                      <label class="col-md-2 control-label" for="textinput">Controller</label>  
                      <div class="col-md-6">
                        <input type="text" name="clone[controller]" value="<?=$selected->controller?>" id="controller" placeholder="Required" maxlength="100" required="" class="form-control input-md" style="border: 1px dotted red;">
                        <span class="help-block">Name of the Controller to be used.</span>  
                      </div>
                    </div>
                  
                    <!-- Text input-->
                    <div class="form-group">
                      <label class="col-md-2 control-label" for="textinput">Caption</label>  
                      <div class="col-md-6">
                        <input type="text" name="clone[caption]" value="<?=$selected->caption?>" id="caption" placeholder="Required" maxlength="100" required="" class="form-control input-md" style="border: 1px dotted red;">
                      </div>
                    </div>
                  
                    <!-- Text input-->
                    <div class="form-group">
                      <label class="col-md-2 control-label" for="textinput">Menu Group</label>  
                      <div class="col-md-6">
                        <input type="text" name="clone[menu_grp]" value="<?=$selected->menu_grp?>" id="menu_grp" placeholder="Required" maxlength="100" required="" class="form-control input-md" style="border: 1px dotted red;">
                      </div>
                    </div> 
                  
                    <!-- Text input-->
                    <div class="form-group">
                      <label class="col-md-2 control-label" for="textinput">Menu Number</label>  
                      <div class="col-md-6">
                        <input type="text" name="clone[menu_num]" value="<?=$selected->menu_num?>" id="menu_num" placeholder="Required" maxlength="100" class="numeric form-control input-md" required="" style="border: 1px dotted red;">
                      </div>
                    </div>

                    <!-- SELECT OPTION-->
                    <div class="form-group">
                      <label class="col-md-2 control-label" for="textinput">Menu Level</label>  
                      <div class="col-md-6">
                        <select name="menu_lvl" id="menu_lvl" placeholder="Required" maxlength="100" required="" class="form-control input-md">
                          <option value="2">Second Level</option>
                        </select>
                      </div>
                    </div>
                  
                    <!-- Text input-->
                    <div class="form-group">
                      <label class="col-md-2 control-label" for="textinput">Remarks</label>  
                      <div class="col-md-6">
                        <input type="text" name="clone[remarks]" value="<?=$selected->remarks?>" id="caption" placeholder="" maxlength="255" class="form-control input-md">
                      </div>
                    </div>
                </fieldset>
              <?else:?>
                <div class="alert alert-info">No source selected.</div>
              <?endif;?>
            </td>
          </tr>
        </table>
      </div>
    </div>

    <?if($selected):?>
      <p></p>
      <div class="row">
        <p>
          <button type="submit" name="clone_menu" value="clone_menu" class="btn btn-sm btn-success" ><span class="entypo-install"></span>&nbsp; Clone Menu</button>
        </p>
      </div>
    <?endif;?>
<?=form_close();?>