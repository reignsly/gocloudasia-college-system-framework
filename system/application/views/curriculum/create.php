<script type='text/javascript'>
  $(document).ready(function(){
    $('#course_id').on('change',function(){
      var xparams = {course_id : $(this).val()};
      var xurl = $(this).attr("url");
      var xcon = $('#div_major');
      $.ajax({
        type: "POST",
        url: xurl,
        data: xparams,
        dataType : 'html',
      }).done(function(output){
        xcon.html(output);
      }).fail(function(jqXHR, textStatus, errorThrown){
          xcon.html(xerror_msg);
      });
    })
  });
</script>
<?php
	$input_name = array(
			'id' => 'name',
			'name' => 'name',
			'placeHolder' => 'Name',
			'value' => set_value('name',''),
			'require' => true
		);
?>

<?=form_open('','class="form-horizontal"');?>

	<!-- Text input-->
    <div class="form-group">
      <label class="col-md-2 control-label" for="">Curriculum Name</label>  
      <div class="col-md-6">
      
      <?=form_input($input_name);?>

      </div>
    </div>

    <!-- COurse input-->
    <div class="form-group">
      <label class="col-md-2 control-label" for="">Course</label>  
      <div class="col-md-6">
      
      <?=course_dropdown('course_id',set_value('course_id',''),'id="course_id" url="'.site_url('ajax/get_major').'"','Choose Course')?>

      </div>
    </div>

    <div class="form-group">
      <label class="col-md-2 control-label" for="">Major / Specialization</label>  
      <div class="col-md-6">
      
      <div id="div_major">
        <p><i>Select Course first.</i></p>
      </div>

      </div>
    </div>

    <!-- Button (Double) -->
    <div class="form-group">
      <label class="col-md-2 control-label" for="button1id">Actions</label>
      <div class="col-md-6">
        <!-- <input type="submit" name='save' id='save' value="Save" class="btn btn-sm btn-default btn-sm" > -->
        <label for="mySubmit" class="btn btn-sm btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp; Save & Continue to Add Subjects</label>
        <input id="mySubmit" type="submit" value="Save & Continue to Create Subjects" name="save_and_continue" class="hidden" />
        <a href="<?=site_url('curriculum')?>" class="btn btn-default">Cancel</a>
      </div>

    </div>
<?=form_close();?>