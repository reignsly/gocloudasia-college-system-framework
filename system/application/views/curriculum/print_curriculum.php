<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style>
	.text-center{
	text-align: center;
	}

	.text-right{
		text-align: right;
	}

	.bold{
		font-weight: bold;
	}

	.italic{
		font-style: italic;
	}

	.fsize-8{
		font-size: 8pt;
	}

	.fsize-9{
		font-size: 9pt;
	}

	.fsize-10{
		font-size: 10pt;
	}

	.fsize-11{
		font-size: 11pt;
	}

	.fsize-12{
		font-size: 12pt;
	}

	.fsize-13{
		font-size: 13pt;
	}

	.fsize-14{
		font-size: 14pt;
	}

	.table-subjects{
		border-collapse: collapse;
		border-color: #000;
		font-size: 8pt;
	}

	tr.info_head td{
		background-color:#CCC;
	}

	td{
		vertical-align: top;
	}
	
	body{
		font-family: arial;
	}

	table{
		border-collapse: collapse;
	}

</style>
</head>
<body>
<table border='0' width="100%" >
	<tr>
		<td colspan="2" align="center" class="text-center bold" ><?=strtoupper($this->setting->school_name);?></td>
	</tr>
	<tr>
		<td colspan="2" align="center" class="text-center italic fsize-9" ><?=$this->setting->school_address;?></td>
	</tr>
	<tr>
		<td colspan="2" align="center" class="text-center fsize-12" ><?=$curriculum->name?>&nbsp; | &nbsp;<?=$curriculum->course?></td>
	</tr>
</table>
<hr/>
<?if($curriculum_subjects):?>
		
	<?$ctr = 1; $semester = false;?>

	<?foreach ($curriculum_subjects as $year_id => $year_data):?>
		
		<?
			$years = isset($year_data['data']) ? $year_data['data'] : false;
			if($years === false){ continue; }
			$semester = isset($year_data['semester']) ? $year_data['semester'] : false;
			
			$tot_units = 0;
			$tot_lec = 0;
			$tot_lab = 0;
		?>

		<?if($semester):?>
		
			<?$ctr = 1;?>

			<?foreach ($semester as $y_id => $sem):?>

				<!-- LOOP SEMESTERS -->
				
					
					<?php
						$sex = isset($sem['data']) && $sem['data'] ? $sem['data'] : false;
						$subjects = isset($sem['subjects']) && $sem['subjects'] ? $sem['subjects'] : false;
						if($sex === false){ continue; }
						if($subjects === false){ continue; }
						$has_record = false;
					?>

					<?if($subjects):?>				

						<h5><?=$years->year;?>-<?echo $sex->name;?></h5>
						
						<div class="table-responsive">
							<table border="1" class="table-subjects" width="100%" >
								<tr class="info_head">
									<td colspan="6" class="text-center bold" >Subject Info</td>
									<td colspan="4" class="text-center bold">Prerequisite</td>
								</tr>
								<tr class="info_head">
									<td width="2%" >#</td>
									<td width="10%" >Course No.</td>
									<td width="33%">Description</td>
									<td width="5%">Unit</td>
									<td width="5%">Lec</td>
									<td width="5%">Lab</td>
									<td width="5%">Req. Year Level</td>
									<td width="5%">Req. Units</td>
									<td width="5%" >Req. En. Units</td>
									<td width="15%">Subject Prerequisite</td>
								</tr>
								<?php
									$ln_ctr = 1;
									$tt_units = 0;
									$tt_lec = 0;
									$tt_lab = 0;
								?>
								<?foreach ($subjects as $s_k => $sub_val):?>
									
									<!-- use closur function to get the subject prerequisit -->
									<?php
										$has_record = true;
										$sub_pre = $get_pre_requisite($curriculum_id, $sub_val->cur_sub_id);
										$sub_pre_str = "";
										if($sub_pre){
											
											foreach ($sub_pre as $k => $v) {
												$sub_pre_str .= "<div modal_id = pre_sub_$v->id class='custom-modal fsize-8 btn btn-xxs btn-twitter'>".$v->code."</div>&nbsp;";
											}
										}

										$tt_units += $sub_val->units;
										$tt_lec += $sub_val->lec;
										$tt_lab += $sub_val->lab;

										$tot_units += $sub_val->units;
										$tot_lec += $sub_val->lec;
										$tot_lab += $sub_val->lab;
									?>
										
									</div>

									<tr>
										  <td><?php echo $ln_ctr++;?>.</td>
										  <td><?php echo $sub_val->code;?></td>
										  <td><?php echo $sub_val->subject;?></td>
										  <td><?php echo $sub_val->units;?></td>
										  <td><?php echo $sub_val->lec;?></td>
										  <td><?php echo $sub_val->lab;?></td>
										  
										  <td><?php echo $sub_val->req_year ? $sub_val->req_year : 'None';?></td>
										  <td><?php echo $sub_val->total_units_req && $sub_val->total_units_req > 0 ? $sub_val->total_units_req : "None" ;?></td>
										  <td><?php echo $sub_val->enrollment_units_req && $sub_val->enrollment_units_req > 0 ? $sub_val->enrollment_units_req : "None" ;?></td>
										  <td><?php echo $sub_pre_str ? $sub_pre_str : 'None';?></td>
									</tr>
								<?endforeach;?>
								<tr>
									<td colspan="3" ></td>
									<td><?=$tt_units?></td>
									<td><?=$tt_lec?></td>
									<td><?=$tt_lab?></td>
									<td colspan="4" ></td>
								</tr>
							</table>
						</div>

					<?endif;?>

			<?endforeach;?>
			<?if($subjects):?>
			<br/>
			<table border="0" class="table-subjects" width="100%" >
				<tr>
					<td width="40%" colspan="3" align="right" >Total Units &nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td width="5%"><?=$tot_units?></td>
					<td width="5%"><?=$tot_lec?></td>
					<td width="5%"><?=$tot_lab?></td>
					<td width="5%"></td>
					<td width="5%"></td>
					<td width="5%" ></td>
					<td width="15%"></td>
				</tr>
			</table>
			<?endif;?>
		<?endif;?>

	<?endforeach;?>
	<br/>
	<table border="0" class="table-subjects" width="100%" >
		<tr>
			<td width="40%" colspan="3" align="right" ><strong>Grand Total of Units &nbsp;&nbsp;</strong></td>
			<td width="5%"><?=$curriculum->units?></td>
			<td width="5%"><?=$curriculum->lec?></td>
			<td width="5%"><?=$curriculum->lab?></td>
			<td width="5%"></td>
			<td width="5%"></td>
			<td width="5%" ></td>
			<td width="15%"></td>
		</tr>
	</table>
<?else:?>

	<div class="alert-alert-danger"><h5>No Subject added to this curriculum. &nbsp; 
		<a href="<?=site_url('curriculum/add_subjects/'.$curriculum_id)?>" class="btn btn-success btn-sm"></a>
	</h5></div>

<?endif;?>
</body>
</html>