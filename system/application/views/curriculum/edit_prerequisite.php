<!-- Subject/Curriculum Profile PROFILE HERE -->
<?php

	unset($panel_config);
	$panel_config['panel_title'] = "Curriculum & Subject Profile";

	$panel_config['panel_btn'][1]['class'] = "myprint btn btn-default";
	$panel_config['panel_btn'][1]['tooltip'] = "Return to Edit";
	$panel_config['panel_btn'][1]['attr'] = " ' ";
	$panel_config['panel_btn'][1]['url'] = site_url('curriculum/edit/'.$curriculum_id.'/'.$tab);
	$panel_config['panel_btn'][1]['title'] = '<i class="fa fa-arrow-up"></i>&nbsp; Return';

	panel_head($panel_config);
?>
	
	<div class="table-responsive">
	<table class="table">
		<tr>
			<td class="bold">Curriculum Name : &nbsp; <?=$curriculum->name;?></td>
			<td class="bold">Course : &nbsp; <?=$curriculum->course;?></div>
			<td class="bold">Status : &nbsp; <?=$curriculum->is_deleted == 0?'Active':'In-active';?></td>
		</tr>

		<tr>
			<td class="bold">Subject Code : &nbsp; <?=$subject->sc_id;?></td>
			<td class="bold">Subject Description : &nbsp; <?=$subject->subject;?></td>
			<td></td>
		</tr>
	</table>
	</div>

<?=panel_foot();?>

<!-- FORM STARTS HERE -->
<?=form_open('','class="single_subject_form form-horizontal" ')?>

<fieldset>

	<!-- Form Name -->
	<legend><small>Subject Prerequisite (Optional)</small></legend>

	<div class="form-group">
	  <label class="col-md-3 control-label" for="req_year_id">Required Year Level (Optional)</label>  
	  <div class="col-md-8">
	  <!-- <input id="textinput" name="textinput" type="text" placeholder="placeholder" > -->
	  <?=year_dropdown('req_year_id',isset($subject)?$subject->req_year_id:'','class="form-control input-md" id="req_year_id"','None')?>
	  <span class="help-block">Required Year Level for the subject selected above.</span>  
	  </div>
	</div>

	<div class="form-group">
	  <label class="col-md-3 control-label" for="total_units_req">Required Units (Optional)</label>  
	  <div class="col-md-8">
	  <input id="total_units_req" value="<?=isset($subject)?$subject->total_units_req:'';?>" name="total_units_req" class="form-control input-md" type="number" step="any" placeholder="0" min = "0" max = "1000" />
	  <span class="help-block">Required units taken by the students.</span>  
	  </div>
	</div>

	<div class="form-group">
	  <label class="col-md-3 control-label" for="enrollment_units_req">Required Enrollment Units (Optional)</label>  
	  <div class="col-md-8">
	  <input id="enrollment_units_req" value="<?=isset($subject)?$subject->enrollment_units_req:'';?>" name="enrollment_units_req" class="form-control input-md" type="number" step="any" placeholder="0" min = "0" max = "1000" />
	  <span class="help-block">Required units taken by the students during enrollment.</span>  
	  </div>
	</div>
	
	<?if($subject_prereq):?>
	<div class="form-group">
		<label class="col-md-3 control-label" for="enrollment_units_req">Current Subject Prerequisite</label>  
		<div class="col-md-8">
			<div class="table-responsive">
				<table class="table">
					<tr class="info" >
						<td>Subject Code</td>
						<td>Section Code</td>
						<td>Description</td>
						<td>Action</td>
						<?foreach ($subject_prereq as $sp_k => $spv):?>
							<tr>
								<td><?=$spv->sc_id?></td>
								<td><?=$spv->code?></td>
								<td><?=$spv->subject?></td>
								<td><a class="btn btn-xxs btn-google confirm" href="<?=site_url('curriculum/remove_prerequisite2/'.$spv->id.'/'.$curriculum_id.'/'.$curriculum_sub_id.'/'.$tab)?>"><i class="glyphicon glyphicon-remove"></i>&nbsp; Remove</a></td>
							</tr>
						<?endforeach;?>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<?endif;?>

	<div class="form-group">
	  <label class="col-md-3 control-label" for="semester_id">Selecet Prerequisite Subject/s (Optional)</label>  
	  <div class="col-md-8">
	  <?for($i = 1; $i <= 7; $i++):?>
	  		<?=form_dropdown('subject_pre['.$i.']', $subject_dropdown, '', 'class="form-control input-md" id="subject_id" class="form-control"' );?>
	  <?endfor;?>
	  <span class="help-block">Duplicate Subjects will not be added.</span>  
	  </div>
	</div>
</fieldset>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-2 control-label" for="semester_id"></label>  
  <div class="col-md-6">
    
    <div class="btn-group">
    	<label for="mySubmit1" class="btn btn-primary btn-small"><i class="fa fa-save"></i>&nbsp; Save Subject</label>
    	<input id="mySubmit1" type="submit" value="Save Subject" name="" class="hidden" />
    	<a href="<?=site_url('curriculum/edit/'.$curriculum_id.'/'.$tab)?>" id="button2id" name="button2id" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i>&nbsp; Go Back To List </a>
    	<input type="hidden" name="event_action" value="save_single" />
    </div>

  </div>
</div>
</form>