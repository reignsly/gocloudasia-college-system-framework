<div class="row">
	<form action="<?=site_url('curriculum');?>/index/0" method="GET" >
		
		<div class="row">
			<div class="col-md-2">
				<input class='form-control' id="name" name="name" size="30" type="text" value="<?=isset($_GET['name'])?$_GET['name']:''?>" placeHolder="Curriculum" />	
			</div>

			<div class="col-md-3">
				<input class='form-control' id="course" name="course" size="30" type="text" value="<?=isset($_GET['course'])?$_GET['course']:''?>" placeHolder="Course" />	
			</div>

			<div class="col-md-3">
				<!-- <input id="search_submit" name="search_subjects" type="submit" value="Search" class="btn btn-sm btn-success" /> -->
				<label for="mySubmit" class="btn btn-sm btn-success"><i class="fa fa-search"></i>&nbsp; Search</label>
				<input id="mySubmit" type="submit" value="Search" name="" class="hidden" />
				<a class='btn btn-success btn-sm' href="<?=site_url('curriculum/create');?>" ><i class="fa fa-plus"></i>&nbsp; New Record</a>
			</div>

		</div>
		
		<input type="hidden" name="back_url" value="<?=$back_url;?>">

	</form>
</div>

<p></p>

<!-- <div class="row">
	<div class="btn-group btn-group-xs">
		<button class="btn-colap btn btn-default btn-xs"><i class="fa fa-angle-double-down"></i>&nbsp;</button>
		<button class="btn-uncolap btn btn-default btn-xs"><i class="fa fa-angle-double-up"></i>&nbsp;</button>
	</div>
</div> -->

<script>
	$('.btn-colap').on('click', function(){
		$('.panel-collapse').addClass('in');
	})
	$('.btn-uncolap').on('click', function(){
		$('.panel-collapse').removeClass('in');
	})
</script>	

<p></p>
<div class="row">
	<?if($group_result):?>
		<div class="panel-group" id="accordion">
			<?foreach ($group_result as $k => $v):?>
				<?php 
					$the_course = $v['course'];
					$the_content = $v['course_major'];
				?>
				<div class="panel panel-default">
			    <div class="panel-heading">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?=$the_course->id?>">
			          <span class="glyphicon glyphicon-chevron-down"></span>&nbsp; <?=strtoupper($the_course->course_code)?>-<?=ucwords(strtolower($the_course->course))?>
			        </a>
			      </h4>
			    </div>

			    <div id="collapse_<?=$the_course->id?>" class="panel-collapse collapse">
			    	<div class="panel-body">
			    		<?if($the_content):?>
			    			<?foreach ($the_content as $k2 => $v2):?>
			    				<?php 
			    					$head = $v2['head'];
			    					$tail = $v2['tail'];
			    				?>
			    				
			    				<?if($tail):?>
			    					<div class="table-responsive">
			    						<table class="table table-bordered table-sortable table-stripped">
			    							<thead>
			    								<tr>
			    									<th colspan="4" ><?php echo $head->course ?> &nbsp; - &nbsp;<?php echo $head->major ?></th>
			    								</tr>
			    								<tr class='gray' >
			    									<th>Curriculum Name</th>
						    		 				<th>School Year Created</th>
						    		 				<th>Status</th>
						    		 				<th>Action</th>
			    								</tr>
			    							</thead>
					    					<?foreach ($tail as $k3 => $v3):?>
					    						<tr>
					    							<td class='bold txt-facebook' ><?php echo $v3->name ?></td>
					    							<td class='bold'><?=$v3->year_from?>-<?=$v3->year_to?></td>
					    							<td>
															<?if($v3->is_active == 1):?>
																<a href="<?=site_url('curriculum/deactivate/'.__link($v3->id))?>" class="btn btn-xs btn-default confirm" title="Are you sure to de-activate this curriculum?"><i class="fa fa-check-square-o"></i>&nbsp; Current</a>
															<?else:?>
																<a href="<?=site_url('curriculum/activate/'.__link($v3->id))?>" class="btn btn-xs btn-default confirm"><i class="fa fa-square-o"></i>&nbsp; Click to set as Current</a>
															<?endif;?>
														</td>
														<td>
															<div class="btn-group btn-group-sm">
																	<a class="btn btn-xs btn-default tp" href="<?=site_url('curriculum/add_subjects/'.__link($v3->id))?>"  data-toggle="tooltip" data-placement="top" title="Add subjects"><span class="entypo-plus"></span></a>
																	<a class="btn btn-xs btn-default tp" href="<?=site_url('curriculum/add_subjects/'.__link($v3->id).'/subjects')?>"  data-toggle="tooltip" data-placement="top" title="View Subjects"><i class="fa fa-book"></i></i></a>
																	<a class="btn btn-xs btn-default tp" href="<?=site_url('curriculum/edit/'.__link($v3->id))?>"  data-toggle="tooltip" data-placement="top" title="Edit Curicullum"><i class="glyphicon glyphicon-edit"></i></a>
																	<a class="btn btn-xs btn-default tp" target="_blank" href="<?=site_url('curriculum/print_curriculum/'.__link($v3->id))?>"  data-toggle="tooltip" data-placement="top" title="Print Curicullum"><i class="fa fa-print"></i></a>
															</div>
														</td>
					    						</tr>
					    					<?endforeach;?>
				    					</table>
			    					</div>
			    				<?endif;?>
			    			<?endforeach;?>
			    		<?endif;?>
			    	</div>
			    </div>
			  </div>
			<?endforeach;?>
		</div>
	<?endif;?>
</div>

<!--div class="row">
	<?php if(!empty($results)):?>
		
		<div class="panel-group" id="accordion">

		<?foreach ($results as $k => $v):?>
			
			<div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?=$v->course_id?>">
		          <span class="glyphicon glyphicon-chevron-down"></span>&nbsp; <?=strtoupper($v->course_code)?>-<?=ucwords(strtolower($v->course))?>
		        </a>
		      </h4>
		    </div>
		    <div id="collapse_<?=$v->course_id?>" class="panel-collapse collapse in">
		      <div class="panel-body">
		    		<div class="table-responsive">
		    		 	<table class="table table-hover table-stripped table-bordered">
		    		 		<thead>
		    		 			<tr class='gray' >
		    		 				<th>Curriculum</th>
		    		 				<th>Course</th>
		    		 				<th>Major</th>
		    		 				<th>School Year Created</th>
		    		 				<th>Status</th>
		    		 				<th>Action</th>
		    		 			</tr>
		    		 		</thead>
		    		 		<?if(isset($each_results[$v->course_id]) && $each_results[$v->course_id]):?>

		    		 			<?foreach ($each_results[$v->course_id] as $i => $c):?>
		    		 				<tr>
		    		 					<td class='bold txt-facebook' ><?=$c->name?></td>
		    		 					<td class='bold' ><?=$c->course_code?></td>
		    		 					<td class='bold' ><?=$c->major?></td>
											<td class='bold'><?=$c->year_from?>-<?=$c->year_to?></td>
											<td>
												<?if($c->is_active == 1):?>
													<i class="fa fa-check-square-o"></i>&nbsp; Current
												<?else:?>
													<a href="<?=site_url('curriculum/activate/'.$v->course_id.'/'.$c->id.'/'.$v->major_id)?>" class="btn btn-xs btn-default confirm"><i class="fa fa-square-o"></i>&nbsp; Click to set as Current</a>
												<?endif;?>
											</td>
											<td>
												<div class="btn-group btn-group-sm">
														<a class="btn btn-xs btn-default tp" href="<?=site_url('curriculum/add_subjects/'.__link($c->id))?>"  data-toggle="tooltip" data-placement="top" title="Add subjects"><span class="entypo-plus"></span></a>
														<a class="btn btn-xs btn-default tp" href="<?=site_url('curriculum/add_subjects/'.__link($c->id).'/subjects')?>"  data-toggle="tooltip" data-placement="top" title="View Subjects"><i class="fa fa-book"></i></i></a>
														<a class="btn btn-xs btn-default tp" href="<?=site_url('curriculum/edit/'.__link($c->id))?>"  data-toggle="tooltip" data-placement="top" title="Edit Curicullum"><i class="glyphicon glyphicon-edit"></i></a>
														<a class="btn btn-xs btn-default tp" target="_blank" href="<?=site_url('curriculum/print_curriculum/'.__link($c->id))?>"  data-toggle="tooltip" data-placement="top" title="Print Curicullum"><i class="fa fa-print"></i></a>
													</ul>
												</div>
											</td>
		    		 				</tr>	
		    		 			<?endforeach;?>

		    		 		<?endif;?>
		    		 	</table>
		    		 </div> 
		      </div>
		    </div>
		  </div>

		<?endforeach;?>

		</div>

		<?= $links ;?>

	<?else:?>
		<div class="alert alert-info"><h5>No Record Found.</h5></div>
	<?endif;?>
</div-->