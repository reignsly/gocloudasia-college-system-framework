<? if($curriculum_subjects):?>
		
	<?$ctr = 1; $semester = false;?>

	<?foreach ($curriculum_subjects as $year_id => $year_data):?>
		
		<?
			$years = isset($year_data['data']) ? $year_data['data'] : false;
			if($years === false){ continue; }
			$semester = isset($year_data['semester']) ? $year_data['semester'] : false;
			
			$tot_units = 0;
			$tot_lec = 0;
			$tot_lab = 0;
		?>

		<?if($semester):?>
			
			<?$ctr = 1;?>

			<?foreach ($semester as $y_id => $sem):?>

				<!-- LOOP SEMESTERS -->
				
					
					<?php
						$sex = isset($sem['data']) && $sem['data'] ? $sem['data'] : false;
						$subjects = isset($sem['subjects']) && $sem['subjects'] ? $sem['subjects'] : false;
						
						if($sex === false){ continue; }
						if($subjects === false){ continue; }
						$has_record = false;
					?>

					<?if($subjects):?>				

						<h4><?=$years->year;?>-<?echo $sex->name;?></h4>
						
						<div class="table-responsive">
							<table border="0" class="table-subjects table-bordered" width="100%" >
								<tr class="info_head success bold">
									<td colspan="7" class="text-center bold" >Subject Info</td>
									<td colspan="5" class="text-center bold">Prerequisite</td>
								</tr>
								<tr class="info_head success bold">
									<td width="2%" >#</td>
									<td width="10%">Course No.</td>
									<td width="33%">Description</td>
									<td width="5%">Unit</td>
									<td width="5%">Lec</td>
									<td width="5%">Lab</td>
									<td width="5%"><div class="tp"  data-toggle="tooltip" data-placement="top" title="Required Student Year Level" >Req. Year Level</div></td>
									<td width="5%"><div class="tp"  data-toggle="tooltip" data-placement="top" title="Required Units Totally Finished by the enrollee" >Req. Units</div></td>
									<td width="5%" ><div class="tp"  data-toggle="tooltip" data-placement="top" title="Required Units taken during the current registration" >Req. En. Units</div></td>
									<td width="15%">Subject Prerequisite</td>
									<?if(isset($with_action)):?>
									<td width="5%">Action</td>
									<?endif;?>
								</tr>
								<?php
									$ln_ctr = 1;
									$tt_units = 0;
									$tt_lec = 0;
									$tt_lab = 0;
									// vp($subjects);
								?>
								<?foreach ($subjects as $s_k => $sub_val):?>
									
									<!-- use closur function to get the subject prerequisit -->
									<?php
										$has_record = true;
										$sub_pre = $get_pre_requisite($curriculum_id, $sub_val->cur_sub_id);
										$sub_pre_str = "";
										if($sub_pre){
											
											foreach ($sub_pre as $k => $v) {
												$sub_pre_str .= "
													<div modal_id = 'pre_sub_$v->id' class='custom-modal fsize-8 btn btn-xxs btn-twitter'>".$v->code."</div>&nbsp;

													<div id = 'pre_sub_$v->id' style='display:none;'>
														<table class='table table-hover'>
															<thead>
																<tr>
																	<td>Code</td>
																	<td>Subject</td>
																	<td>unit</td>
																	<td>Lec</td>
																	<td>Lab</td>
																</tr>
															</thead>
															<tr>
																<td>".iset($v->code)."</td>
																<td>".iset($v->subject)."</td>
																<td>".iset($v->units)."</td>
																<td>".iset($v->lec)."</td>
																<td>".iset($v->lab)."</td>
															</tr>
														</table>
													</div>
												";
											}
										}

										$tt_units += $sub_val->units;
										$tt_lec += $sub_val->lec;
										$tt_lab += $sub_val->lab;

										$tot_units += $sub_val->units;
										$tot_lec += $sub_val->lec;
										$tot_lab += $sub_val->lab;
									?>
										
									</div>

									<tr>
										  <td><?php echo $ln_ctr++;?></td>
										  <td><?php echo $sub_val->code;?></td>
										  <td><?php echo $sub_val->subject;?></td>
										  <td><?php echo $sub_val->units;?></td>
										  <td><?php echo $sub_val->lec;?></td>
										  <td><?php echo $sub_val->lab;?></td>
										  
										  <td><?php echo $sub_val->req_year ? $sub_val->req_year : 'None';?></td>
										  <td><?php echo $sub_val->total_units_req && $sub_val->total_units_req > 0 ? $sub_val->total_units_req : "None" ;?></td>
										  <td><?php echo $sub_val->enrollment_units_req && $sub_val->enrollment_units_req > 0 ? $sub_val->enrollment_units_req : "None" ;?></td>
										  <td><?php echo $sub_pre_str ? $sub_pre_str : 'None';?></td>
										  <?if(isset($with_action)):?>
										  <td>
										  	<div class="btn-group">
												  <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
												    <span class="caret"></span>
												    <span class="sr-only">Toggle Dropdown</span>
												  </button>
												  <ul class="dropdown-menu pull-right" role="menu">
												    <li><a href="<?=site_url('curriculum/add_subjects/'.__link($curriculum_id).'/edit_subject/'.__link($sub_val->cur_sub_id))?>" class=""><span class="glyphicon glyphicon-edit"></span> Edit</a></li>
										  			<li><a href="<?=site_url('curriculum/remove_subject/'.__link($curriculum_id).'/'.__link($sub_val->cur_sub_id).'/subjects')?>" class="confirm" title="Are sure you want to remove <?=$sub_val->subject?>?"><span class="glyphicon glyphicon-remove"></span> Remove</a></li>
												  </ul>
												</div>
										  </td>
										  <?endif;?>
									</tr>
								<?endforeach;?>
								<tr>
									<td colspan="3" ></td>
									<td><?=$tt_units?></td>
									<td><?=$tt_lec?></td>
									<td><?=$tt_lab?></td>
									<td colspan="4" ></td>
								</tr>
							</table>
						</div>

					<?endif;?>

			<?endforeach;?>
			<?if($tot_units):?>
			<br/>
			<table border="0" class="table-subjects" width="100%" >
				<tr>
					<td width="40%" colspan="3" align="right" >Year Total Units</td>
					<td width="5%"><span class="badge" ><?=$tot_units?></span></td>
					<td width="5%"><span class="badge" ><?=$tot_lec?></span></td>
					<td width="5%"><span class="badge" ><?=$tot_lab?></span></td>
					<td width="5%"></td>
					<td width="5%"></td>
					<td width="5%" ></td>
					<td width="15%"></td>
				</tr>
			</table>
			<?endif;?>
		<?endif;?>

	<?endforeach;?>
	<br/>
	<table border="0" class="table table-subjects" width="100%" >
		<tr>
			<td width="40%" colspan="3" align="right" ><strong>Grand Total of Units</strong></td>
			<td width="5%"><span class="badge badge-success" > <?=$curriculum->units?></span></td>
			<td width="5%"><span class="badge " ><?=$curriculum->lec?></span></td>
			<td width="5%"><span class="badge " ><?=$curriculum->lab?></span></td>
			<td width="5%"></td>
			<td width="5%"></td>
			<td width="5%" ></td>
			<td width="15%"></td>
		</tr>
	</table>
<?else:?>

	<div class="alert-alert-danger"><h5>No Subject added to this curriculum. &nbsp; 
		<a href="<?=site_url('curriculum/add_subjects/'.$curriculum_id)?>" class="btn btn-success btn-sm"></a>
	</h5></div>

<?endif;?>