<div id="right">

<div id="right_top" >
	<p id="right_title">Employee Attendance : Edit</p>
</div>
		  
<div id="right_bottom">
	<div id="header">| Update attendance for <?=$date?> | </div>
	<?  ?>
		<div class="alert alert-danger">* Check Employees will be considered absent</div>
		<?echo form_open('');?>
		<table id="table">
			<thead>
				<tr>
				<th>Absent</th>
				<th>Name</th>
				<th>Time-IN</th>
				<th>Time-OUT</th>
			  </tr>
			</thead>
			<tfoot>
				<tr>
				<th>Absent</th>
				<th>Name</th>
				<th>Time-IN</th>
				<th>Time-OUT</th>
			  </tr>
			</tfoot>
			<tbody>
			</tbody>
			<?php
				
				foreach($employeeattendances as $value)
				{
				?>
					<tr>
						<td>
							<input type='hidden' name='attendance[<?=$value->employee_id;?>][id]'  value='<?=$value->id;?>' />
							<input type='checkbox' name='attendance[<?=$value->employee_id?>][reason]' <?=strtolower($value->reason) == 'absent' ? 'checked' : '';?> /></td>
						<th><?=$value->name;?></th>
						<td><input type='text' name='attendance[<?=$value->employee_id?>][timein]' maxLength='25' size='15' value='<?=$value->timein;?>' /></td>
						<td><input type='text' name='attendance[<?=$value->employee_id?>][timeout]' maxLength='25' size='15' value='<?=$value->timeout;?>' /></td>
					</tr>
				<?php
				}
			?>
		</table>
		<?
			echo form_submit('','Save Changes');
			echo form_close();
		?>
		
		<br/>
		<p>
			<a href="<?php echo base_url(); ?>employee_attendance/calendar/<?=$year;?>/<?=$month;?>" rel="facebox" class="actionlink">Back to calendar</a>
		</p>
</div>
	

</div>
