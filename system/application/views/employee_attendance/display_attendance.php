<div id="right">
		  
<div id="right_bottom">
	<p><a class="navbar-link" href='#'>Show attendance for <?=$date?></a></p>
	
	<?  ?>
		<table id="table">
			<tbody>
				<tr>
					<th colspan = '3' style='text-align:center'><strong>Present</strong></th>
				</tr>
				<tr>
					<th>Name</th>
					<th>Time-IN</th>
					<th>Time-OUT</th>
				</tr>
			</tbody>
			<tfoot>
				<tr>
				<th>Name</th>
				<th>Time-IN</th>
				<th>Time-OUT</th>
			  </tr>
			</tfoot>
			<?php
				$absent = array();
				if($employeeattendances){
				foreach($employeeattendances as $value)
				{
					if(strtolower($value->reason) != 'absent')
					{
					?>
						<tr>
						<td><?=$value->name;?></td>
						<td><?=$value->timein;?></td>
						<td><?=$value->timeout;?></td>
						</tr>
					<?php	
					}
					else
					{
						$absent[count($absent)] = $value;
					}
				}
				}
			?>
		</table>
		<br/>
		<table id="table">
			<tbody>
				<tr>
					<th colspan = '3' style='text-align:center'><strong>Absent</strong></th>
				</tr>
				<tr>
				<th>Name</th>
				<th>Time-IN</th>
				<th>Time-OUT</th>
			  </tr>
			</tbody>
			<tfoot>
				<tr>
				<th>Name</th>
				<th>Time-IN</th>
				<th>Time-OUT</th>
			  </tr>
			</tfoot>
			<tbody>
			
			<?php
				
				foreach($absent as $value)
				{
					?>
						<tr>
						<td><?=$value->name;?></td>
						<td><?=$value->timein;?></td>
						<td><?=$value->timeout;?></td>
						</tr>
					<?php	
				}
			?>
			</tbody>
		</table>
		
		<br/>
		<p>
			<a href="<?php echo base_url(); ?>employee_attendance/calendar/<?=$year;?>/<?=$month;?>" rel="facebox" class="btn btn-default btn-sm">Back to calendar</a>
		</p>
</div>
	

</div>
