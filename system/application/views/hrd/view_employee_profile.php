<div id="right">
	<div id="right_top" >
		  <p id="right_title">View Employee Profile</p>
	</div>
	<div id="right_bottom">
	<?php echo isset($system_message) == NULL ? NULL : $system_message;?>
		<a href="<?= site_url('hrd/edit_employee_profile/' . $employee_profile->id );?>">Edit Profile</a>
		<br />
		<table>
		<tr><th>Employee ID:</th><td><?= $employee_profile->employeeid;?></td></tr>
		<tr><th>First Name:</th><td><?= $employee_profile->first_name;?></td></tr>
		<tr><th>Middle Name:</th><td><?= $employee_profile->middle_name;?></td></tr>
		<tr><th>Last Name:</th><td><?= $employee_profile->last_name;?></td></tr>
		<tr><th>Email:</th><td><?= $employee_profile->email;?></td></tr>
		<tr><th>Date of Birth:</th><td><?= $employee_profile->dob;?></td></tr>
		<tr><th>Gender:</th><td><?= $employee_profile->gender;?></td></tr>
		<tr><th>Join Date:</th><td><?= $employee_profile->joining_date;?></td></tr>
		<tr><th>Role:</th><td><?= $employee_profile->role;?></td></tr>
		<tr><th>Martial Status:</th><td><?= $employee_profile->martial_status;?></td></tr>
		<tr><th>Spouse Name:</th><td><?= $employee_profile->martial_status;?></td></tr>
		<tr><th>Number of Children:</th><td><?= $employee_profile->no_children;?></td></tr>
		<tr><th>Father Name:</th><td><?= $employee_profile->father_name;?></td></tr>
		<tr><th>Mother Name:</th><td><?= $employee_profile->mother_name;?></td></tr>
		</table>
		<br />
		
		
	</div>
</div>
