<div id="right">
	<div id="right_top" >
		  <p id="right_title">Edit Employee Profile</p>
	</div>
	<div id="right_bottom">
	<a href="<?= site_url('hrd/view_employee_profile/' . $employee_profile->id );?>">View Profile</a><br />
	<?php echo isset($system_message) == NULL ? NULL : $system_message;?>
		<form action="<?php echo site_url('hrd/update_employee_profile').'/'.$employee_profile->id;?>" method="post" accept-charset="utf-8" class="registrar-form">
    <table>
    <tr>
      <th>Employee ID:</th><td><?= $employee_profile->employeeid;?></td>
    </tr>
    <tr>
      <th>First Name</th><td>
      <input name="first_name" size="30" type="text" value="<?= $employee_profile->first_name;?>" /></td>
    </tr>
    <tr>
      <th>Middle Name</th><td>
      <input name="middle_name" size="30" type="text" value="<?= $employee_profile->middle_name;?>" /></td>
    </tr>
    <tr>
      <th>Last Name</th><td>
      <input name="last_name" size="30" type="text" value="<?= $employee_profile->last_name;?>" /></td>
    </tr>
    <tr>
      <th>Email</th><td>
      <input name="email" size="30" type="text" value="<?= $employee_profile->email;?>" /></td>
    </tr>
    <tr>
      <th>Joining Date</th><td>
      <input class="date_pick" name="joining_date" size="30" type="text" value="<?= $employee_profile->joining_date;?>" /></td>
    </tr>
    <tr>
      <th>Gender</th><td>
      <? foreach($genders as $row):?>
        <? if($row == $employee_profile->gender):?>
        <input name='gender' checked="checked" type='radio' value="<?= $row;?>" /><?= $row;?>
        <? else :?>
        <input name='gender' type='radio' value="<?= $row;?>" /><?= $row;?>
        <? endif ;?>
      <? endforeach;?>
      </td>
    </tr>
    <tr>
      <th>Date of Birth</th><td>
      <input class="date_pick" name="dob" size="30" type="text" value="<?= $employee_profile->dob;?>" /></td>
    </tr>
    <tr>
      <th>Role</th><td>
      <? foreach($roles as $row):?>
        <? if($row == $employee_profile->role):?>
        <input name='role' checked="checked" type='radio' value="<?= $row;?>" /><?= $row;?>
        <? else :?>
        <input name='role' type='radio' value="<?= $row;?>" /><?= $row;?>
        <? endif ;?>
      <? endforeach;?>
      </td>
    </tr>
    <tr>
      <th>Martial Status</th>
      <td>
      <select name="martial_status">
      <? foreach($martial_status as $row):?>
      <? if($row == $employee_profile->martial_status) :?>
      <option value="<?= $row;?>" selected="selected"><?= $row;?></option>
      <? else: ?>
      <option value="<?= $row;?>"><?= $row ;?></option>
      <? endif;?>
      <? endforeach;?>
      </select>
      </td>
    </tr>
    <tr>
    <th>Number of Children</th>
	  <td><input name="no_children" size="30" type="text" value="<?= $employee_profile->no_children;?>"/></td>
	  </tr>
    <tr>
    <th>Father Name</th>
	  <td><input name="father_name" size="30" type="text" value="<?= $employee_profile->father_name;?>" /></td>
	  </tr>
    <tr>
    <th>Mother Name</th>
	  <td><input  name="mother_name" size="30" type="text" value="<?= $employee_profile->mother_name;?>" /></td>
	  </tr>
    <tr>
    <th>Spouse Name</th>
    <td><input  name="spouse_name" size="30" type="text" value="<?= $employee_profile->spouse_name;?>" /></td>
    </tr>
    </table>
    <table>
      <tr>
        <th>&nbsp;</th>
        <td><input type="submit" value="Submit" /></td>
      </tr>
    </table>
</form>
  <br>
  
	</div>
</div>
