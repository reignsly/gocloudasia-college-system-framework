<?php
echo link_tag('assets/stylesheets/nl.css');
?>
<title><?php echo $title; ?></title>

<table id="ched_table">
  <tr>
    <th colspan=2>Student Name</th>
	<?php for($i=1;$i<=$no_subjects;$i++){ ?>
    <th>Subject Code</th>
    <th>Earned Units</th>
	<?php } ?>
    <th>Total Earned Units</th>
  </tr>
<?php 
$num=1;
if($students){
foreach($students as $student): 
$show = 0; 
	foreach($studentsubjects as $studentsubject): 
		if($student->id == $studentsubject->enrollmentid && $studentsubject->code != '' && $studentsubject->units != ''){ 
			$show = 1; 
		} 
	endforeach; 
	if($show == 1){ ?>
  <tr>
    <td><?php echo $num++; ?></td>
    <td><?php echo $student->name;  $total_units = 0; $ctr = 0; ?></td>
	<?php 
	foreach($studentsubjects as $studentsubject):
		if($student->id == $studentsubject->enrollmentid){ 
	?>
	<td><?php echo $studentsubject->sc_id; ?></td>
    <td><?php echo $studentsubject->units; $total_units = $total_units + $studentsubject->units; ?></td>
	<?php 
		$ctr++; 
		}
	endforeach;
	for($i=$ctr+1;$i<=$no_subjects;$i++){ 
	?>
	<td>&nbsp;</td>
    <td>&nbsp;</td>
	<?php } ?>
	<td><?php echo $total_units; ?></td>
  </tr>
<?php 
	} 
endforeach; 
}else{
?>
<tr>
<td colspan="3">
no record to display
</td>
<?php
}
?>
</table>
<br />
<?php
if($students){
?>
<a href="<?php echo base_url(); ?>ched/print_show/<?php echo $year_id."/".$sem_id."/".$course_id; ?>" target="_blank">Print</a>
<?php
}
?>