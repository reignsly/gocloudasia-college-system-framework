<div class="row">
	<div class="col-md-12">
		<div class="alert alert-github">
			<div class="row">
				<div class="col-md-2 bold">Announcement for : </div>
				<div class="col-md-2">
					  
				    <input type="checkbox" name="to_whom[]" value="all" 
						<?php if($announcement)
						{
						echo $announcement->to_all=='' || $announcement->to_all=='0' ? "":"checked"; 
						}
						?> 
						/>
					  <label for="to_all">To All</label>
				</div>
				<div class="col-md-2">
					<p>
					<input type="checkbox" name="to_whom[]" value="employee" 
					<?php if($announcement)
					{
					echo $announcement->to_employee=='' || $announcement->to_employee=='0' ? "":"checked"; 
					}
					?> 
					/>
				    <label for="to_employee">To Employees</label>
				  </p>
				</div>
				<div class="col-md-2">
					  <p>
					    <input type="checkbox" name="to_whom[]" value="student" 
						<?php if($announcement)
						{
						echo $announcement->to_student=='' || $announcement->to_student=='0' ? "":"checked"; 
						}
						?>
						/>
					    <label for="to_student">To Students</label>
					  </p>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					 	<p>
					 		<?php
					 			if($announcement)
								{
									$title = $announcement->title;
								}else{
									$title = '';
								}
								$t_att = array('name'=>'title','value'=>$title, 'class'=>'form-control','required' => true);
					 		?>
				    	<label for="date">Title</label><br />
							<?php echo form_input($t_att); ?>
				  	</p>

				  	<p>
					 		<?php
					 			if($announcement)
								{
									$date_val = date('m/d/Y',strtotime($announcement->date));
								}else{
									$date_val = '';
								}
								$d_att = array('name'=>'date','value'=>$date_val, 'class'=>'', 'size'=>'20x20' ,'required' => true);
					 		?>
				    	<label for="date">Date</label><br />
							<?php echo form_input($d_att); ?>
				  	</p>
				</div>
			</div>
		</div>
	</div>
</div>
<p></p>

<div class="row">
	<div class="col-md-12">
		<?=panel_head2('Content',false,'primary')?>
			<p></p>
			<?=html_entity_decode($announcement->message,ENT_COMPAT);?>
		<?=panel_tail2()?>
	</div>
</div>

<a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>announcements"><span class='glyphicon glyphicon-backward'></span>&nbsp;  Cancel</a>

<script type="text/javascript">
	$('input').attr('disabled',true);
</script>