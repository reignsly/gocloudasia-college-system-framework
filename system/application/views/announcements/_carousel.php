<style>
	.carousel-indicators .active {
	  background: #31708f;
	}

	.content {
	  margin-top: 20px;
	}

	.adjust1 {
	  float: left;
	  width: 100%;
	  margin-bottom: 0;
	}

	.adjust2 {
	  margin: 0;
	}

	.carousel-indicators li {
	  border: 1px solid #ccc;
	}

	.carousel-control {
	  color: #31708f;
	  width: 5%;
	}

	.carousel-control:hover, .carousel-control:focus {
	  color: #31708f;
	}

	.carousel-control.left, .carousel-control.right {
	  background-image: none;
	}

	.media-object {
	  margin: auto;
	  margin-top: 15%;
	}

	/*@media screen and (max-width: 768px) {
	  .media-object {
	    margin-top: 0;
	  }
	}*/
</style>
<?if(isset($announcements) && $announcements):?>
   <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      <!-- Indicators --> 
      <ol class="carousel-indicators">
         <?foreach ($announcements as $k => $v):?>
            <li data-target="#carousel-example-generic" data-slide-to="<?=$k?>" class="<?=$k==0?'active':''?>"></li>
         <?endforeach?>
      </ol>
      <!-- Wrapper for slides --> 
      <div class="carousel-inner">
         
         <?foreach ($announcements as $k => $v):?>
            <div class="item <?=$k==0?'active':''?>">
               <div class="row">
                  <div class="col-xs-12">
                     <div class="thumbnail adjust1">
                        
                        <div class="col-md-10 col-sm-10 col-xs-12">
                           <div class="caption">
                              <p class="text-info lead adjust2"><?=$v->title?> <small>(<?=date('m-d-Y', strtotime($v->date))?>)</small></p>
                              <?=html_entity_decode($v->message,ENT_COMPAT);?>
                              <blockquote class="adjust2">
                                 <small><cite title="Source Title"><i class="glyphicon glyphicon-globe"></i> <?=$v->department?></cite></small> 
                              </blockquote>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>   
         <?endforeach;?>
      </div>
      <!-- Controls --> <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> </a> <a class="right carousel-control" href="#carousel-example-generic" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> </a> 
   </div>
<?endif;?>