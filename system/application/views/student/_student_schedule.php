
<table>
	<tr>
		<th>#</th>
		<th>Subject Code</th>
		<th>Section Code</th>
		<th>Description</th>
		<th>Units</th>
		<th>Lab</th>
		<th>Lec</th>
    <th>Time</th>
    <th>Day</th>
    <th>Block</th>
	</tr>
<?php if($subjects):?>
<? 
	$ctr = 0; 
	$total_lab = 0;
	$total_lec = 0;
	$total_units = 0;
?>

<?php foreach($subjects as $subject): ?>
	<?
		$total_lab += $subject->lab;
		$total_lec += $subject->lec;
		$total_units += $subject->units;
	?>
	<tr>
    <td><?php echo ++$ctr; ?></td>
    <td><?php echo $subject->sc_id; ?></td>
	  <td><?php echo $subject->code; ?></td>
	  <td><?php echo $subject->subject; ?></td>
	  <td><?php echo $subject->units; ?></td>
	  <td><?php echo $subject->lab; ?></td>
	  <td><?php echo $subject->lec; ?></td>
    <td><?php echo $subject->time; ?></td>
    <td><?php echo $subject->day; ?></td>
    <td><?php echo $subject->block_name; ?></td>
  </tr>
	<?php endforeach;endif; ?>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>Total</td>
		<td id="total_units"><?php if(isset($total_units))echo $total_units; ?></td>
		<td id="total_labs"><?php if(isset($total_lab))echo $total_lab; ?></td>
		<td id="total_lec"><?php if(isset($total_lec))echo $total_lec; ?></td>
		<td>&nbsp;</td>
	</tr>
	</table>			
</form>
	
