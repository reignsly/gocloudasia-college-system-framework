<div id="right">
	<?php 
		$formAttrib = array('id' => 'search_enrollee', 'class' => 'form-inline', 'method' => 'GET');
		echo form_open('search/search_student/0', $formAttrib);
	?>
    
 <div class="form-group">
    <input id="lastname" class="form-control" type="text" value="<?=isset($lastname)?$lastname:''?>" name="lastname" placeholder="Last Name">
  </div>
   
  <div class="form-group">
    <input id="fname" class="form-control" type="text" value="<?=isset($fname)?$fname:''?>" name="fname" placeholder="First Name">
  </div>
   
  <div class="form-group">
    <input id="studid" class="form-control" type="text" value="<?=isset($studid)?$studid:''?>" name="studid" placeholder="Student ID">
  </div>
  <div class="form-group">
    <?=year_dropdown('year_id',isset($year_id)?$year_id:'',"", '')?>
  </div>
   
	<?php
	echo form_hidden('semester_id', $this->open_semester->id);
	echo form_hidden('sy_from', $this->open_semester->year_from);
	echo form_hidden('sy_to', $this->open_semester->year_to);
	echo form_hidden('is_paid', 1);
	?>
   		<?php echo form_submit('submit', 'Search','class="btn btn-default btn-sm"'); ?>
		<?echo form_close();?>
<br>
<?echo isset($links) ? $links : NULL;?>
	<?php
	if(isset($search)){
	?>
	<br />
	<table class="table table-hover table-sortable" >
		<thead>
			<tr class='gray' >
			  <th>Student ID</th>
			  <th>Name</th>
			  <th>Year</th>
			  <th>Course</th>
			  <th>Date of Birth</th>
			  <th>View</th>
			</tr>
		</thead>
		<tbody>
		<?php
		if(!empty($search))
		{
			foreach($search as $student):
			?>
			<tr>
			<td class='bold txt-facebook' ><?php echo $student->studid; ?></td>
			<td class='bold txt-facebook' ><?php echo ucfirst($student->name); ?></td>
			<td><?php echo $student->year; ?></td>
			<td><?php echo $student->course; ?></td>
			<td><?php echo $student->dob !== "0000-00-00" ? date('m-d-Y', strtotime($student->dob)) : ' - Not Set -'; ?></td>
			<td>
				
				<div class="btn-group btn-group-sm">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-caret-square-o-down"></i> <span class="caret"></span>
					</button>
					<ul class="dropdown-menu pull-right" role="menu">
					
					<?
						$mctr=0;
						// vd($mydepartment);
					?>
					
					<?if($mydepartment->stud_profile == 1):?>
					<li><a href="<?=base_url('profile/view/'.$student->id.'/official')?>"><i class="fa fa-file-text"></i>&nbsp;  Profile</a></li>
					<?$mctr++;?>
					<?endif;?>
					
					<?if($mydepartment->stud_grade == 1):?>
					<li><a href="<?=base_url('grade/view/'.$student->id.'/official')?>"><i class="fa fa-th"></i>&nbsp;  Grades</a></li>
					<?$mctr++;?>
					<?endif;?>
					
					<?if($mydepartment->stud_fees == 1):?>
					<li><a href="<?=base_url('fees/view_fees/'.$student->id.'/official')?>"><i class="fa fa-list-alt"></i>&nbsp;  Fees</a></li>
					<?$mctr++;?>
					<?endif;?>
					
					<?if($mydepartment->stud_otr == 1):?>
					<li><a href="<?=base_url('transcript/view/'.$student->id.'/official')?>"><i class="fa fa-credit-card"></i> &nbsp;  Transcript</a></li>
					<?$mctr++;?>
					<?endif;?>
					
					<?if($mydepartment->stud_issues == 1):?>
					<li><a href="<?=base_url('issues/view/'.$student->id.'/official')?>"><i class="fa fa-thumbs-down"></i>&nbsp;  Issues</a></li>
					<?$mctr++;?>
					<?endif;?>
					
					<?if($mctr == 0):?>
					<li><a href="#"><i class="fa fa-times-circle"></i>&nbsp;  No action available.</a></li>
					<?endif;?>
					
				 </ul>
				</div>
				
			</td>
			</tr>
			<?php 
			endforeach; ?>
			</tbody>
			<tr>
				<td colspan='5' align='right'><b>Total Records</b></td>
				<td><span class='badge'><?=($total_rows) ? $total_rows : 0;?></span></td>
			</tr>
			<?php
		}
		else
		{
		?>
		<tr>
		<td colspan="5">
			Student Not Found
		</td>
		</tr>
		<?php
		}
		?>
	</table>
	<?echo isset($links) ? $links : NULL;?>
	<?php
	}
	?>
	</div>
	
	<div class="clear"></div>
</div>