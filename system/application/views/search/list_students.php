<div id="right">
	<?php 
		$formAttrib = array('id' => 'search_enrollee', 'class' => 'form-inline', 'method' => 'GET');
		echo form_open('', $formAttrib);
	?>
    
 <div class="form-group">
    <input id="lastname" class="form-control" type="text" value="<?=isset($lastname)?$lastname:''?>" name="lastname" placeholder="Last Name">
  </div>
   
  <div class="form-group">
    <input id="fname" class="form-control" type="text" value="<?=isset($fname)?$fname:''?>" name="fname" placeholder="First Name">
  </div>
   
  <div class="form-group">
    <input id="studid" class="form-control" type="text" value="<?=isset($studid)?$studid:''?>" name="studid" placeholder="Student ID">
  </div>
  <div class="form-group">
    <?=year_dropdown('year_id',isset($year_id)?$year_id:'',"", '')?>
  </div>
   
	<?php
	echo form_hidden('semester_id', $this->open_semester->id);
	echo form_hidden('sy_from', $this->open_semester->year_from);
	echo form_hidden('sy_to', $this->open_semester->year_to);
	echo form_hidden('is_paid', 1);
	?>
   		<?php echo form_submit('submit', 'Search'); ?>
		<?echo form_close();?>
<br>
<?echo isset($links) ? $links : NULL;?>
	<?php
	if(isset($search)){
	?>
	<br />
	<table>
		<tr>
		  <th>Student ID</th>
		  <th>Name</th>
		  <th>Year</th>
		  <th>Course</th>
		  <th>View</th>
		</tr>
		<tbody>
		<?php
		if(!empty($search))
		{
			foreach($search as $student):
			$l = _se($student->id);
			?>
			<tr>
			<td><?php echo $student->studid; ?></td>
			<td><?php echo ucfirst($student->name); ?></td>
			<td><?php echo $student->year; ?></td>
			<td><?php echo $student->course; ?></td>
			<td>
				<div class="btn-group btn-group-sm">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-caret-square-o-down"></i> <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
					
					<?
						$mctr=0;	
					?>
					
					<?if($mydepartment->stud_profile == 1):?>
					<li><a href="<?=base_url('profile/view/'.$student->id.'/'.$search_type)?>"><i class="fa fa-file-text"></i>&nbsp;  Profile</a></li>
					<?$mctr++;?>
					<?endif;?>
					
					<?if($mydepartment->stud_grade == 1):?>
					<li><a href="<?=base_url('grade/view/'.$student->id.'/'.$search_type)?>"><i class="fa fa-th"></i>&nbsp;  Grades</a></li>
					<?$mctr++;?>
					<?endif;?>
					
					<?if($mydepartment->stud_fees == 1):?>
					<li><a href="<?=base_url('fees/view_fees/'.$student->id.'/'.$search_type)?>"><i class="fa fa-list-alt"></i>&nbsp;  Fees</a></li>
					<?$mctr++;?>
					<?endif;?>
					
					<?if($mydepartment->stud_otr == 1):?>
					<li><a href="<?=base_url('transcript/view/'.$student->id.'/'.$search_type)?>"><i class="fa fa-credit-card"></i> &nbsp;  Transcript</a></li>
					<?$mctr++;?>
					<?endif;?>
					
					<?if($mydepartment->stud_issues == 1):?>
					<li><a href="<?=base_url('issues/view/'.$student->id.'/'.$search_type)?>"><i class="fa fa-thumbs-down"></i>&nbsp;  Issues</a></li>
					<?$mctr++;?>
					<?endif;?>
					
					<?if($mctr == 0):?>
					<li><a href="#"><i class="fa fa-times-circle"></i>&nbsp;  No action available.</a></li>
					<?endif;?>
					
				 </ul>
				</div>
			</td>
			</tr>
			<?php 
			endforeach; 
		}
		else
		{
		?>
		<tr>
		<td colspan="5">
			Student Not Found
		</td>
		</tr>
		<?php
		}
		?>
		</tbody>
		<tr>
		  <th>Student ID</th>
		  <th>Name</th>
		  <th>Year</th>
		  <th>Course</th>
		  <th>Action</th>
		</tr>
	</table>
	<?echo isset($links) ? $links : NULL;?>
	<?php
	}
	?>
	</div>
	
	<div class="clear"></div>
</div>
