<div id="right">
	<?php 
		$formAttrib = array('id' => 'search_employee', 'class' => 'form-inline', 'method' => 'GET');
		echo form_open('search/search_employee/index/0', $formAttrib);
	?>
    
 <div class="form-group">
    <input id="name" class="form-control" type="text" value="<?=isset($name)?$name:''?>" name="name" placeholder="Name">
  </div>
   
  <div class="form-group">
    <input id="login" class="form-control" type="text" value="<?=isset($login)?$login:''?>" name="login" placeholder="Employee ID / Login ID">
  </div>

  <div class="form-group">
    <b>Role : </b>&nbsp;
  </div>

  <div class="form-group">
	<?
    echo departments_dropdown('role',isset($role) ? $role : '','','All');
	?>
	
  </div>
   
	<?php
	echo form_hidden('semester_id', $this->open_semester->id);
	echo form_hidden('sy_from', $this->open_semester->year_from);
	echo form_hidden('sy_to', $this->open_semester->year_to);
	echo form_hidden('is_paid', 1);
	?>
   		<?php echo form_submit('submit', 'Search'); ?>
		<?echo form_close();?>
<br>
<?echo isset($links) ? $links : NULL;?>
	<?php
	if(isset($search)){
	?>
	<br />
	<table>
		<tr>
			<th>Name</th>
			<th>Login</th>
			<th>Role</th>
			<th>Action</th>
		</tr>
		<tbody>
		<?php
		if(!empty($search))
		{
			foreach($search as $s):
			$l = _se($s->id);
			?>
			<tr>
				<td>
					<?=strtoupper($s->fullname);?>
				</td>
				<td>
					
				<?=$s->employeeid;?>
				</td>
				<td>
					<?=$s->role;?>
				</td>
				<td>
					<? if(strtolower($this->session->userdata['userType']) == "hrd") :?>
					<a href="<?= site_url('employees/edit/' . $s->id );?>"><span class='glyphicon glyphicon-edit' ></span>&nbsp;Edit Profile</a><br/>
					
					<a href="<?= site_url('employees/display/' . $s->id );?>"><span class='glyphicon glyphicon-search' ></span>&nbsp;View Profile</a><br/>
						<? if(strtolower($s->role) == "teacher") :?>
					<a href="<?= site_url('hrd/add_assignsubject/' . $s->id );?>"><span class='glyphicon glyphicon-list' ></span>&nbsp;Assign Subject</a><br/>
					<? endif;?>
					<a href="<?= site_url('hrd/add_assign_course/' . $s->id );?>"><span class='glyphicon glyphicon-list' ></span>&nbsp;Assign Course</a><br/>
					<? endif;?>
					<a href="<?= site_url('change_password/reset/' . $s->employeeid );?>"><span class='glyphicon glyphicon-refresh' ></span>&nbsp;Change Password</a><br/>
					
				</td>
			</tr>
			<?php 
			endforeach; ?>
			<tr>
				<td colspan='1' align='right'><b>Total Records</b></td>
				<td><span class='badge'><?=($total_rows) ? $total_rows : 0;?></span></td>
			</tr>
			<?php
		}
		else
		{
		?>
		<tr>
		<td colspan="1">
			Student Not Found
		</td>
		</tr>
		<?php
		}
		?>
		</tbody>
		<tr>
			<th>Name</th>
			<th>Login</th>
			<th>Role</th>
			<th>Action</th>
		</tr>
	</table>
	<?echo isset($links) ? $links : NULL;?>
	<?php
	}
	?>
	</div>
	
	<div class="clear"></div>
</div>