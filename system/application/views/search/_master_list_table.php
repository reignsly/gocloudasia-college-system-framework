    <table>
			<tr>
			  <th>#</th>
			  <th>ID</th>
				<th>Name</th>
				<th>Year</th>
				<th>Action</th>
			</tr>
			<? $num = 0 ?>
			<?php if(empty($search_results) == FALSE):?>
				<? foreach($search_results as $s): ?>
					<? $l= _se($s->enrollment_id);?>
					<tr>
					  <td><?= $num += 1 ?></td>
						<td><?= $s->studid ?></td>
						<td><?=strtoupper($s->name);?></td>
						<td><?= $s->year ?></td>
						<!--<td><a href="<?#=site_url('profile/view?id='.$l->link.'&di='.$l->hash);?>">view profile</a></td>-->
						<td>
							<div class="btn-group btn-group-sm">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-caret-square-o-down"></i> <span class="caret"></span>
								</button>
								<ul class="dropdown-menu" role="menu">
								
								<?
									$mctr=0;
								?>
							
								<?if($mydepartment->stud_profile == 1):?>
								<li><a href="<?=base_url('profile/view/'.$s->enrollment_id)?>"><i class="fa fa-file-text"></i>&nbsp;  Profile</a></li>
								<?$mctr++;?>
								<?endif;?>
								
								<?if($mydepartment->stud_grade == 1):?>
								<li><a href="<?=base_url('grade/view/'.$s->enrollment_id.'/'.$s->user_id)?>"><i class="fa fa-th"></i>&nbsp;  Grades</a></li>
								<?$mctr++;?>
								<?endif;?>
								
								<?if($mydepartment->stud_fees == 1):?>
								<li><a href="<?=base_url('fees/view_fees/'.$s->enrollment_id)?>"><i class="fa fa-list-alt"></i>&nbsp;  Fees</a></li>
								<?$mctr++;?>
								<?endif;?>
								
								<?if($mydepartment->stud_otr == 1):?>
								<li><a href="<?=base_url('transcript/view/'.$s->enrollment_id.'/'.$s->user_id)?>"><i class="fa fa-credit-card"></i> &nbsp;  Transcript</a></li>
								<?$mctr++;?>
								<?endif;?>
								
								<?if($mydepartment->stud_issues == 1):?>
								<li><a href="<?=base_url('issues/view/'.$s->enrollment_id.'/'.$s->user_id)?>"><i class="fa fa-thumbs-down"></i>&nbsp;  Issues</a></li>
								<?$mctr++;?>
								<?endif;?>
								
								<?if($mctr == 0):?>
								<li><a href="#"><i class="fa fa-times-circle"></i>&nbsp;  No action available.</a></li>
								<?endif;?>
								
							 </ul>
							</div>
						</td>
					</tr>
				<? endforeach;?>
			<?php else:?>
				<tr>
					<td colspan=5 class="text-center">----NO data to show---</td>
				</tr>	
			<?php endif;?>
			  <tr>
				  <td colspan=4>TOTAL:</td>
				  <td>TOTAL:<div class='badge'><? echo $total_rows;?></div></td>
				</tr>
		</table>
		<?= $links;?>
