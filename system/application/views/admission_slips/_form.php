	
 <script>
  
  function validate()
	{
		var ctr = 0;
		var msg = "<ul>";
		var xfocus = "";
		$('.not_blank').css('border-color','gray');
		$('.not_blank').each(function(){
			var xval = $(this).val().trim();
			var xid = $(this).attr('id');
			if(xval == ""){
				ctr++;
				$(this).css('border-color','red');
				if(ctr == 1) { xfocus = $(this); }
				var label = $('label[for='+xid+']').text();
				
				msg += "<li>"
				+label+" should not be blank</li>";
			}
		});
		
		msg += "<ul>";
		
		if(ctr > 0)
		{
			xfocus.focus();
			$('#alertModal_Label').text('Required Fields'); //SET MODAL TITLE
			$('#alertModal_Body').html(msg); //SET MODAL CONTENT
			$('#alertModal').modal('show'); //SHOW MODAL
			
			return false;
		}
		else
		{
			return true; 
		}
	}
  </script>	
 <p>
    <label for="admission_slip_date">Date</label><br />
    <input id="admission_slip_date" name="admission_slip[date]" size="30" type="text" class='not_blank date_pick' value="<?=isset($admission_slip->date)?$admission_slip->date:date('Y-m-d')?>" />
  </p>
  <p>
    <label for="enrollment_id">Name of student</label>
	<input id="name" name="name" size="30" type="text" disabled value='<?=$student->full_name;?>' />
  </p>
  <p>
    <label for="course_and_year">Course and year</label><br />
    <input id="course_and_year" name="admission_slip[course_and_year]" size="30" type="text" disabled value='<?=$student->course;?>,<?=$student->year;?>' />
  </p>
  <p>
    <label for="inclusive_dates_of_absences">Inclusive dates of absences</label><br />
    <input id="inclusive_dates_of_absences" name="admission_slip[inclusive_dates_of_absences]" size="30" type="text" class='not_blank' value="<?=isset($admission_slip->inclusive_dates_of_absences)?$admission_slip->inclusive_dates_of_absences:''?>" />
  </p>
  <p>
    <label for="admission_slip_subject">Subject</label><br />
    <input id="admission_slip_subject" name="admission_slip[subject]" size="30" type="text" class='not_blank' value="<?=isset($admission_slip->subject)?$admission_slip->subject:''?>" />
  </p>
  <p>
    <label for="admission_slip_reason">Reason</label><br />
    <textarea cols="40" id="admission_slip_reason" name="admission_slip[reason]" rows="20" class='not_blank'  >
	<?=isset($admission_slip->reason)?$admission_slip->reason:''?>
	</textarea>
  </p>
  <p>
    <label for="admission_slip_category">Category</label><br />
	<?
		$category = array(
			"Excuse" => 'Excuse',
			"Unexcuse" => 'Unexcuse',
			"Warning" => 'Warning',
		);
		
		echo form_dropdown('admission_slip[category]', $category, isset($admission_slip->category)?$admission_slip->category:'','id="admission_slip_category"');
	?>
<option value="Excuse">Excuse</option>
<option value="Unexcuse">Unexcuse</option>
<option value="Warning">Warning</option></select>
  </p>
  <p>
    <label for="admission_slip_sao_remark">Sao remark</label><br />
    <textarea cols="40" id="admission_slip_sao_remark" name="admission_slip[sao_remark]" rows="20">
	<?=isset($admission_slip->sao_remark)?$admission_slip->sao_remark:''?>
	</textarea>
  </p>
  
  

