<div class='alert alert-info'>
	<center><b>
	<?=$departments->department?> |
	<?=$departments->description?>
	</b></center>
</div>

<ul class="nav nav-tabs">
	<li <?= ($this->router->class == "departments" && $this->router->method == 'edit') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('departments/edit/'.$id); ?>">Profile</a></li>
	  
	<li <?= ($this->router->class == "departments" && $this->router->method == 'menus') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('departments/menus/'.$id); ?>">Menus</a></li>
	
	<li <?= ($this->router->class == "departments" && $this->router->method == 'edit_menu_group') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('departments/edit_menu_group/'.$id); ?>">Edit Menu Group</a></li>
	
	<li <?= ($this->router->class == "departments" && $this->router->method == 'menu_list') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('departments/menu_list/'.$id); ?>">Edit Menu List</a></li>
	
	<li <?= ($this->router->class == "departments" && $this->router->method == 'add_menu') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('departments/add_menu/'.$id); ?>">Add Menu</a></li>
	
	<li <?= ($this->router->class == "departments" && $this->router->method == 'student_profile_access') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('departments/student_profile_access/'.$id); ?>">Student Profile Access</a></li>
	  
	  <li>
	  <a href="<?php echo base_url('departments'); ?>"><span class = 'glyphicon glyphicon-user'></span>&nbsp;  Go Back To Departments</a></li>
	
</ul>

<br/>