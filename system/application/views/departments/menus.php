<script type="text/javascript">
	$( document ).ready(function(){
		
		$('.menu_checkbox').change(function(event){
		
			var xchk = $(this).is(':checked');
			var xid = $(this).val();
				
			$.ajax({
				url : '<?=site_url("departments/ajax_set_menu")?>',
				type : 'POST',
				dataType : 'json',
				async	: false,
				data : {
					'access' : xchk,
					'id' : xid
				},
				success : function(data){}
			}).done(function(data){
				growl(data.title, data.msg);
			});
		})

		$('.method_checkbox').change(function(event){
		
			var xchk = $(this).is(':checked');
			var xid = $(this).val();
				
			$.ajax({
				url : '<?=site_url("departments/ajax_set_method")?>',
				type : 'POST',
				dataType : 'json',
				async	: false,
				data : {
					'access' : xchk,
					'id' : xid
				},
				success : function(data){}
			}).done(function(data){
				growl(data.title, data.msg);
			});
		})
	});

	function show_access(event,el){
		event.preventDefault();
		var xid = $(el).attr('xid');
		if($('#access_'+xid).is(':visible'))
		{
			$(el).text('Functions');
			$('#access_'+xid).hide();
		}
		else
		{
			$(el).text('Hide Functions');
			$('#access_'+xid).show();	
		}
	}
</script>

<?$this->load->view('departments/_tab');?>

<?echo isset($links) ? $links : NULL;?>

<?if($menus):?>
	<table>
		<tr>
			<th>Menu Group</th>
			<th>Order</th>
			<th>Visible</th>
			<th>Menu List</th>
			<th>Action</th>
		</tr>
	<?foreach($menus as $content):?>
		<?
			$head = $content['header'];
			$list = $content['menu_list'];
			$method = isset($content['menu_method'])?$content['menu_method']:false;
			
		?>
		
		<?if($head):?>
		
		<tr>
			<td></i><?=$head->caption?></td>
			<td><?=$head->menu_num?></td>
			<td><?=$head->visible == 1 ? 'YES' : 'NO'?></td>
			<td>
				<?if($list):?>
				<ul class="list-group">
					<?
						foreach($list as $menu):
						$xcheckbox = $menu->visible == 1 ? 'checked' : ''; 
						$xstyle = $menu->visible == 1 ? 'info' : 'danger';
						$xicon = $menu->visible == 1 ? 'glyphicon glyphicon-ok' : 'glyphicon glyphicon-remove';
					?>
					<li class="list-group-item list-group-item-<?=$xstyle?>">
						<span class="badge" style='cursor:pointer;' ><a class='confirm' style='color:white' href='<?=base_url('departments/delete_menu_list/'.$id.'/'.$menu->id)?>'><span class='glyphicon glyphicon-trash' ></span>&nbsp;</a></span>
						<!-- <span class="badge" style='cursor:pointer;' xid = "<?=$menu->id?>" onclick="show_access(event,this)" >Functions</span> -->
						
						<input class='menu_checkbox' xid = '<?=$menu->id?>' type='checkbox' name='cb_<?=$menu->id;?>' id='cb_<?=$menu->id;?>' value='<?=$menu->id?>' <?=$xcheckbox;?> />
						<?=$menu->caption?>
						<div class='alert alert-success' id ='access_<?=$menu->id?>' style='display:none;'>
							<?if(isset($method[$menu->id]) && $method[$menu->id]):?>
								<?foreach ($method[$menu->id] as $key => $md):?>
									<input class='method_checkbox' xid = '<?=$md->id?>' type='checkbox' name='md_<?=$md->id?>' id='md_<?=$md->id;?>' value='<?=$md->id?>' <?=$md->access == 1 ? 'checked' : ''?> />
									<?=$md->method?>
								<?endforeach?>
							<?endif;?>
						</div>
					</li>
					<?endforeach;?>
				</ul>
				<?endif;?>
			</td>
			<td>
				<a href='<?=base_url('departments/edit_menu_group/'.$id.'/'.$head->id)?>'><span class='glyphicon glyphicon-pencil' ></span>&nbsp; Edit Menu Group</a>
				
				<br/> 
				
				<a href='<?=base_url('departments/menu_list/'.$id.'/'.$head->id)?>'><span class='glyphicon glyphicon-list' ></span>&nbsp; Edit Menu List</a>
				
				<br/> 
				
				<a href='<?=base_url('departments/add_menu/'.$id.'/'.$head->id)?>'><span class='glyphicon glyphicon-plus' ></span>&nbsp; Add Menu</a>
				
				<br/> 
				
				<a class='confirm' href='<?=base_url('departments/delete_menu_head/'.$id.'/'.$head->id)?>'><span class='glyphicon glyphicon-trash' ></span>&nbsp; Delete</a>
			</td>
		</tr>
		
		<?endif;?>
		
	<?endforeach;?>
	</table>
<?else:?>
	<div class='alert alert-info'>No menus for this department. &nbsp; <a class='btn btn-default btn-sm' href='<?=base_url('departments/add_menu/'.$id)?>'><span class='glyphicon glyphicon-plus' ></span>&nbsp; Add Menu</a> </div>
<?endif;?>

<a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>departments" rel="facebox" class="actionlink"><span class='glyphicon glyphicon-backward'></span>&nbsp;  Back to list</a>