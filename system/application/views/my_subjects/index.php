<form action="<?=site_url('my_subjects');?>/index/0" method="GET" class="form-inline" >
	
	<div class="form-group">
		<input class='form-control' id="search_sc_id_eq" name="sc_id" size="30" type="text" value="<?=isset($_GET['sc_id'])?$_GET['sc_id']:''?>" placeHolder="Subject Code" />	
	</div>

	<div class="form-group">
		<input class='form-control' id="search_code_eq" name="code" size="30" type="text"  value="<?=isset($_GET['code'])?$_GET['code']:''?>" placeHolder="Section Code" />
	</div>

	<div class="form-group">
		<input class='form-control' id="search_subject_eq" name="subject" size="30" type="text" value="<?=isset($_GET['subject'])?$_GET['subject']:''?>" placeHolder="Description" />
	</div>

	<div class="form-group">
		<input id="search_submit" name="search_subjects" type="submit" value="Search" class="btn btn-sm btn-success" />
	</div>

</form>

<?php if(!empty($results)):?>
  <?= $links ;?>	
<?php endif;?>
<br/>
<div class="table-responsive">
<table class="paginated table table-hover table-sortable">
	<thead>
	  <tr class='gray'>
	    <th>Course No.</th>
	    <th>Description</th>
	    <th>Unit</th>
	    <th>Lec</th>
	    <th>Lab</th>
	    <th>Time</th>
	    <th>Day</th>
	    <th>Room</th>
	    <th>Action</th>
	  </tr>
  </thead>
  
    
		<?php if(!empty($results)):?>
			<?php foreach($results as $s):?>
				<tr>
					  <td><?php echo $s->code;?></td>
					  <td><?php echo $s->subject;?></td>
					  <td><?php echo $s->units;?></td>
					  <td><?php echo $s->lec;?></td>
					  <td><?php echo $s->lab;?></td>
					  <td><?php echo $s->time;?></td>
					  <td><?php echo $s->day;?></td>
					  <td><?php echo $s->room;?></td>
					  <td>
					  	
					  	<div class="btn-group">
					  		<?if($mydepartment->stud_grade === "1"):?>
					  			<a href="<?=site_url('my_subjects/grading/'.$s->ref_id);?>" class="btn btn-xs btn-primary"><i class="fa fa-star-half-o"></i>&nbsp; Grading</a>
					  			<a target="_blank" href="<?=site_url('my_subjects/print_grading/'.$s->id);?>" class="btn btn-xs btn-default"><span class="entypo-print"></span>&nbsp;</a>
					  		<?else:?>
					  			-- No Access --
					  		<?endif;?>
					  	</div>
					  </td>
				</tr>
			<?php endforeach;?>
		<?php endif;?>
</table>	
<?php if(!empty($results)):?>
  <?= $links ;?>	
<?php endif;?>
</div>

