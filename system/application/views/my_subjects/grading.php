<script type="text/javascript">
  $(document).ready(function(){
    $("#unlock_grade").click(function(event) {
      event.preventDefault();
      if($(this).text().trim() == "Click to Edit Grades"){
        $('.grade_tables input[type="text"]').removeClass('hidden');  
        $('.grade_tables input[type="number"]').removeClass('hidden');  
        $('.txt-remarks').removeClass('hidden');  
        $('.grade-badge').addClass('hidden');  
        $('.span-remarks').addClass('hidden');  
        $('#udpate_grade').attr('disabled', false);
        $(this).text("Lock Grades");
      }else{
        $('.grade_tables input[type="text"]').addClass('hidden');  
        $('.grade_tables input[type="number"]').addClass('hidden');  
        $('.txt-remarks').addClass('hidden');  
        $('.grade-badge').removeClass('hidden');  
        $('.span-remarks').removeClass('hidden');  
        $(this).text("Click to Edit Grades");
        $('#udpate_grade').attr('disabled', true);
      }
    })

    $("#sform").validate({
      submitHandler: function(form) {
        form.submit();
      }
    });
  })
</script>
<div class="row">
	<div class="table-responsive">
			<table class="table table-condensed table-bordered">
				<tr class="gray bold">
					<th>Course No.</th>
					<th>Description</th>
					<th>Room</th>
					<th>Time</th>
					<th>Day</th>
					<th>Current Grading Period</th>
				</tr>
				<tr>
					<td class='bold'><?php echo $subject->code ?></td>
					<td class='bold'><?php echo $subject->subject ?></td>
					<td><?php echo $subject->room ?></td>
					<td><?php echo $subject->time ?></td>
					<td><?php echo $subject->day ?></td>
					<td class='text-info' ><strong><?php echo $current_gp->grading_period ?></strong></td>
				</tr>
			</table>
		</div>
</div>

<div class="row">
	<div class="xwell">
		<a href="<?=site_url('my_subjects')?>" class="btn btn-sm btn-default"><i class="fa fa-hand-o-left"></i>&nbsp; Cancel</a>
		<div class="btn-group btn-group-sm">
		  <button type="button" class="btn btn-success">Select to Change Subject</button>
		  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
		    <span class="caret"></span>
		    <span class="sr-only">Toggle Dropdown</span>
		  </button>
		  <ul class="dropdown-menu" role="menu">
		    <?if($subjects): foreach ($subjects as $id => $v):?>
		    	<li><a href="<?=site_url('my_subjects/grading/'.$id)?>"><?=ucwords($v);?></a></li>
		    <?endforeach; endif;?>
		  </ul>
		</div>
		<a target="_blank" href="<?=site_url('my_subjects/print_grading/'.$subject_id)?>" class="btn btn-sm btn-tumblr"><span class="entypo-print"></span> &nbsp; Print (PDF)</a>
	</div>
</div>
<p></p>

<div class="row">
	<?=panel_head2('Grades',false,'default')?>
		<p></p>
		<?=form_open('','id="sform"')?>

			<?if($mydepartment->stud_edit_grade === "1" && isset($students) && $students):?>
	      <div class='btn-group'>
	        <button class="btn btn-sm btn-success" type="submit" value="update_grade" name="update_grade" id="udpate_grade" disabled ><i class="fa fa-edit"></i>&nbsp; Save Grade</button>
	        <button  data-toggle="tooltip" data-placement="top" title="Unlock/Lock the grades input fields" class='tp btn btn-danger btn-sm' id='unlock_grade' > Click to Edit Grades</button>
	      </div>
	    <?endif;?>
	    <a href="#" class="btn btn-sm btn-default"><span class="badge"><?=count($students);?></span></a>

	    <div class="btn-group pull-right">
	    	<?if($recalculate_grade):?>
	        <a href="<?php echo site_url('my_subjects/recalculate/'.($ref_id)) ?>" class='btn btn-facebook btn-sm confirm' title='Student grade conversion will be recompute based on the grading system table. Do you want to continue?' ><i class="fa fa-refresh"></i>&nbsp; Recompute</a>
	        <button type='button' modal-id = "myModal_grading_system" modal-title="Grading System Table" modal-size ="wide" class='tp btn btn-default btn-sm button-modal' id='unlock_grade' > Grading Table</button>
	      <?endif;?>
	    </div>
			<p></p>

			<div class="table-responsive">
				<?if($students):?> 

					<table class="table-hover table-stripped table-condensed grade_tables">
						<thead>
							<tr class='gray bold' >
								<th>ID</th>
								<th>Fullname</th>
								<th>Course</th>
								<th>Year</th>

								<?if($grading_period): foreach ($grading_period as $k => $gp):?>
									<th width="10%"><?=$gp->grading_period?></th>	
								<?endforeach; endif;?>
								<th>Remarks</th>
							</tr>
						</thead>

						<?foreach ($students as $k => $s): $profile = $s['profile']; $_grade = $s['grades'];?>
							<tr>
								<td class='bold' ><?=$profile->studid?></td>
								<td class='bold' ><?=$profile->fullname?></td>
								<td class='bold' ><?=$profile->course_code?></td>
								<td><?=$profile->year?></td>
								<?php
									$subject_profile = $_grade['subject_profile'];
									$grade = $_grade['subject_grade'];
								?>
								<?if($grading_period): foreach ($grading_period as $k => $gp):?>
									
									<td class="bold text-center">
										
										<?php
											$xgrade = "";
											$xgf_id = 0;

											if($grade): foreach ($grade as $gk => $g):

												if($g->gp_id === $gp->id){
													$xgrade = $g->value;
													$xgf_id = $g->id;
													break;
												}
											endforeach; endif;
										?>

											<?if($edit_grade && $mydepartment->stud_edit_grade === "1"):?>

												<?if($edit_all_period):?>
														<span class='label label-as-badge label-default grade-badge' ><?php echo ($g->converted); ?></span>
														<input class='grid hidden form-control' type='text' name="studentsubject[<?=$xgf_id?>]" id="" value="<?=number_format(floatval($xgrade), 2, '.', '');?>" max="<?php echo $max_grade ?>" min="<?php echo $min_grade ?>" step="any" placeHolder="0.00"/>

												<?else:?>
													
													<?if($gp->id == $current_gp->id):?>
														<span class='label label-as-badge label-default grade-badge' ><?php echo ($g->converted); ?></span>
														<input class='grid hidden form-control' type='text' name="studentsubject[<?=$xgf_id?>]" id="" value="<?=number_format(floatval($xgrade), 2, '.', '');?>" max="<?php echo $max_grade ?>" min="<?php echo $min_grade ?>" step="any" placeHolder="0.00"/>
													<?else:?>
														<span class='label label-as-badge label-default' ><?php echo ($g->converted); ?></span>
													<?endif;?>

													
												<?endif;?>
											<?else:?>
											
												<span class='label label-as-badge label-default grade-badge' ><?php echo ($g->converted); ?></span>

											<?endif;?>

									</td>	
								<?endforeach; endif;?>

								<td><input type="text" name="finalgrade[<?=$subject_profile->id?>][remarks]" id="" value="<?=$subject_profile->remarks?>" placeHolder="Remarks" class=""/></td>

							</tr>
						<?endforeach;?>
					</table>

				<?else:?>
					<p></p>
					<div class="alert alert-danger"><p><span class="iconicfill-info"></span> &nbsp; No Students Found for this Subject.</p></div>

				<?endif;?>	
			</div>
	
		<?=form_close();?>	

	<?=panel_tail2();?>
</div>

<div style="display:none;">
  <?php @$this->load->view('layouts/_grading_system'); ?>
</div>