<?php $this->load->view('layouts/_student_data'); ?>
<div class="tab-content">
  <div class="xrow">
    <a target='_blank' href="<?= base_url('pdf/transcript/'. $enrollment_id); ?>" class="btn btn-default btn-sm btn-sm"><span class="glyphicon glyphicon-print"></span> Print Transcript</a>
    <a target='_blank' href="<?= base_url('excels/collegiate_record/'. $enrollment_id); ?>" class="btn btn-default btn-sm"><i class="fa fa-download"></i>&nbsp; Download Collegiate Record (XLS)</a>
    <!-- <a target='_blank' href="<?= base_url('excels/otr/'. $enrollment_id); ?>" class="btn btn-default btn-sm btn-sm"><span class="glyphicon glyphicon-print"></span> Print OTR</a> -->
  </div>
  <p></p>

  <?//=panel_head2('Transcript',false,'default')?>
    <div class="table-responsive">

      <?if($transcript): foreach ($transcript as $k => $tr):  $profile = $tr['profile']; $subjects = $tr['subjects'];  ?>
        <table class="table table-hover table-striped">
          <thead>
            <tr>
              <td colspan="5" class="bold text-center success" >
                <div class="btn-group">
                  <?=$profile->year?> &nbsp; <i class="fa fa-asterisk"></i> &nbsp;
                  <?=$profile->name?> &nbsp; <i class="fa fa-asterisk"></i> &nbsp;
                  <?=$profile->course_code?>
                </div>
              </td>
            </tr>

            <tr class='gray' >
              <th>Course No</th>
              <th>Description</th>
              <th>Units</th>
              <th class="text-center">Grade</th>
            </tr>      
          </thead>
          
          <?if($subjects): foreach ($subjects as $k => $s):?>
            <tr>
              <td class='bold txt-facebook' ><?=$s->code?></td>
              <td class='bold txt-facebook' ><?=ucwords($s->subject)?></td>
              <td class='bold' ><?php echo $s->units ?></td>
              <td class="text-center" ><span class="badge"><?=$get_final_grade($enrollment_id,$s->id)?></span></td>
            </tr>
          <?endforeach; endif;?>    

        </table>      
      <?endforeach; endif;?>
    </div>
  <?//=panel_tail2();?>
</div>