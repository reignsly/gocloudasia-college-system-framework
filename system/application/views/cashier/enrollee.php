<div id="left">
	<?php $this->load->view('layouts/_cashier_menu'); ?>
</div>

<div id="right">
	<div id="right_top" >
		  <p id="right_title">Enrollee</p>
	</div>
	<div id="right_bottom">
	
	<table>
		<tr>
		  <th>ID No.</th>
		</tr>
		<?php
		if(!empty($search))
		{
			foreach($search as $student):
			?>
			<tr>
			<td>
			  <?php echo ucfirst($student->last_name).', '.ucfirst($student->first_name); ?><br />
			  <?php echo $student->studid; ?><br />
			  <?php echo $student->year." Year | ".$student->name." Semester"; ?><br />
			  <?php echo $student->course; ?>
			</td>
			<td>
			<div id="action_links2">
			<ul>
           	<li>
           		<ul class="actionlink">
				<li>
					<a href="<?php echo base_url(); ?>fees/view_fees/<?php echo $student->id; ?>" rel="facebox" class="actionlink">View Fees</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>profile/view/<?php echo $student->id; ?>" rel="facebox" class="actionlink">View Profile</a>
				</li>
				<li>
					<!--<%= link_to "Destroy Enrollment", enrollment, :method => :delete, :confirm => "Are you sure to delete enrollment?", :class => 'actionlink'%>-->
				</li>
				<li>
					<!--<%= link_to "Destroy Account", enrollment.user, :method => :delete, :confirm => "Are you sure to delete enrollment?", :class => 'actionlink'%>-->
				</li>
				</ul>
           	</li>
			</ul>
			</div>
			</td>
			</tr>
			<?php 
			endforeach; 
		}
		else
		{
		?>
		<tr>
		<td>
			No Enrollees
		</td>
		</tr>
		<?php
		}
		?>
	</table>

	</div>
	
	<div class="clear"></div>
</div>