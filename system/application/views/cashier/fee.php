<div id="left">
	<?php $this->load->view('layouts/_dynamic_menu'); ?>
</div>

<div id="right">
	<div id="right_top" >
		  <p id="right_title">Student Fee</p>
	</div>
	<div id="right_bottom">
	
	
	<?php $this->load->view('layouts/cashier_tab_menu'); ?>
	<div id="parent">

	<div id="header">| View Fees | <br> <?php $this->load->view('layouts/_student_data'); ?></div>

  <?php if($student->block_viewing_of_grade == 0){?>
    <a href="<?php echo base_url(); ?>cashier/block_grade/<?php echo $enrollment_id; ?>/1">Block View of Grade</a>
  <?php } ?>

  <?php if($student->block_viewing_of_grade == 1){?>
    <a href="<?php echo base_url(); ?>cashier/block_grade/<?php echo $enrollment_id; ?>/0">Allow View of Grade</a>
  <?php } ?>
<br />

  <a href="<?php echo base_url(); ?>cashier/reasses/<?php echo $enrollment_id; ?>">Re-assess Fees</a>
<br />
<br />

<?php if(!empty($student_total)){ ?>

<?php if(!empty($student_finance)){ ?>
<table class="table-custom">
	<tr>
		<td>Payment Category:
		<?php echo $student_finance->category2; ?>
		</td>
		<td>NSTP: <?php echo $student_total->less_nstp > 0 ? "Yes" : "No"; ?></td>
	</tr>
</table>
 
<br />

<table class="table-custom">
	<tr>
		<th>Fee</th>
		<th>Amount</th>
	</tr>
	<tr>
		<td align="left">Tuition Fee: <?php echo number_format($student_total->tuition_fee_per_unit,2,'.',' ')." x ".number_format($student_total->total_units,2,'.',' '); ?></td>
		<td align="right">
		<?php echo number_format($student_total->tuition_fee,2,'.',' '); ?>
		</td>
	</tr>
	<tr>
		<td align="left">Laboratory Fee: <?php echo number_format($student_total->lab_fee_per_unit,2,'.',' ')." x ". number_format($student_total->total_lab,2,'.',' '); ?></td>
		<td align="right">
		<?php echo number_format($student_total->lab_fee,2,'.',' '); ?>
		</td>
	</tr>

	<tr>
	  <td align="left">Miscellaneous Fees</td>
	  <td align="right">&nbsp;</td>
	</tr>
	<?php foreach($studentfees as $sf): ?>
	<?php if ($sf->is_misc == 1){ ?>
	<tr>
		<?php if(strtolower($sf->name) != "tuition fee per unit" || strtolower($sf->name) != "laboratory fee per unit" || strtolower($sf->name) != "rle"){ ?>
			<td align="left"><?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$sf->name ?>:</td>
			<td align="right"><?php echo number_format($sf->value,2,'.',' '); ?></td>
			<td><a href="#" class="confirm"
							title="Are you sure you want to delete fee from record? Changes cannot be reverted. continue?">
							delete
			</a></td>
		<?php } ?>
	</tr>
	<?php } ?>
	<?php endforeach; ?>
 

	<tr>
		<td align="left">Other Fees</td>
		<td align="right">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>	
        
	<?php foreach($studentfees as $sf): ?>
    <?php if($sf->is_other == 1){ ?>
	<tr>
		<?php if(strtolower($sf->name) != "tuition fee per unit" || strtolower($sf->name) != "laboratory fee per unit" || strtolower($sf->name) != "rle"){ ?>
			<td align="left"><?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$sf->name ?>:</td>
			<td align="right"><?php echo number_format($sf->value,2,'.',' '); ?></td>
			<td><a href="#" class="confirm"
						title="Are you sure you want to delete fee from record? Changes cannot be reverted. continue?">
						delete
			</a></td>
		<?php } ?>
	</tr>
	<?php } ?>
	<?php endforeach; ?>

		
	<tr>
		<th colspan=2>Related Experience Learning</th>
                  
		<th align="right">
			<?php echo number_format($student_total->total_rle,2,'.',' '); ?>
		</th>
	</tr>
	 
	<tr>
	<th>Total Current Charges:</th>
	<th align="right">
	</th>
	</tr>
	
	<tr>
		<th>Previous Account:</th>
		<th align="right">
		<?php if(!empty($previous)){ ?>
  		<a href="#" class="del confirm"
			title="Are you sure you want to delete fee from record? Changes cannot be reverted. continue?">
			Delete
		</a>
		<?php }else{ ?>
		<a href="#">Add</a>
		<?php } ?>
		<?php echo number_format($student_total->previous_account,2,'.',' '); ?>
		</th>
	</tr>
	<tr>
		<th>TOTAL AMOUNT DUE:</th>
		<th align="right">
		<?php echo number_format($student_total->total_amount,2,'.',' '); ?>
	</th>
	<tr>
	   <td>&nbsp;</td>
	   <td align="right" colspan=2><a href="#">Add Fee</a><!--<%= link_to 'Add Fee', new_studentfinance_studentfee_path(@studentfinance.id)  %>--></td>
	</tr>
	<tr>
	   <th align="right" colspan=3><a href="<?php echo base_url(); ?>payment_record/add/<?php echo $enrollment_id; ?>">Add Payment Record</a><!--<%= link_to 'Add Payment Record', new_enrollment_payment_record_path(@enrollment.id) %>--></th>
	</tr>
</table>
<br />
<div class="clear"></div>


	<br />
	
	<a href="#">Edit</a><!--<%= link_to "Edit", fee_edit_path(@studentfinance.id), :class => 'edit_feeval' %>--> |

	<a href="<?php echo base_url(); ?>cashier/print_fee_pdf/<?php echo $enrollment_id; ?>" target="_blank">Print</a><!--<%= link_to "Print", student_fee_path(@studentfinance.id, @enrollment.id, :format=>"pdf"), :target => '_blank' %>--><br>
	

<table class="table-custom">
	<tr>
		<th colspan=3>Deductions</th>
	</tr>
	<tr>
		<td>Less Deduction:</td>
		<td align="right">
			
			  <a href="#">Add</a><!--<%= link_to "Add", calcu_enrollment_student_deduction_path(@studentfinance.enrollmentid,@enrollment.student_total.tuition_fee.to_s.gsub(".","-"),@enrollment.student_total.lab_fee.to_s.gsub(".","-"),@enrollment.student_total.total_misc_fee.to_s.gsub(".","-"),@enrollment.student_total.total_other_fee.to_f.to_s.gsub(".","-"),@enrollment.student_total.total_amount.to_s.gsub(".","-")) %>--> |
			  <a href="#">View</a><!--<%= link_to "View", enrollment_student_deductions_path(@studentfinance.enrollmentid), :class => 'edit_feeval'  %>-->


			<?php echo number_format($student_total->total_deduction,2,'.',' '); ?>
		</td>
	</tr>
	<tr>
		<td>Less Payment:</td>
		<td align="right">
			
			<a href="#">Add</a><!--<%= link_to "Add", payment_create_path(@studentfinance.id, @enrollment.id), :class => 'edit_feeval' %>--> |
			<a href="#">View</a><!--<%= link_to "View", payments_path(@studentfinance.id, @enrollment.id), :class => 'edit_feeval'  %>-->
			
			
			<?php echo number_format($student_total->total_payment,2,'.',' '); ?>
		</td>
	</tr>
	<tr>
		<td>TOTAL DEDUCTIONS:</td>
		<td align="right">
		<?php echo number_format($student_total->total_payment + $student_total->total_deduction,2,'.',' '); ?>
		</td>
	</tr>
	<tr>
		<td>Remaining Balance:</td>
		<td align="right">
			<?php echo number_format($student_total->remaining_balance,2,'.',' '); ?>
  		</td>
	</tr>
	<tr>
		<td>Status:</td>
		<td align="right">
	 	<?php if($student->is_paid == 1){ ?>
	 	  Paid
	 	<?php }else{ ?>
	 	  Unpaid
	 	<?php } ?>
		</td>
	</tr>
</table>

<br />
<?php 
		$formAttrib = array('id' => 'add_promissory_note', 'class' => 'registrar-form', 'method' => 'POST');
		
		$preliminaryAttrib = array('name' => 'preliminary', 'id' => 'preliminary', 'maxlength' => '8');
		$midtermAttrib = array('name' => 'midterm', 'id' => 'midterm', 'maxlength' => '8');
		$semi_finalsAttrib = array('name' => 'semi_finals', 'id' => 'semi_finals', 'maxlength' => '8');
		$finalsAttrib = array('name' => 'finals', 'id' => 'finals', 'maxlength' => '8');
		echo form_open('cashier/add_promissory_note', $formAttrib);
		
	?>

	<table  class="table-custom">
	<tr>
    <th>Divison</th>
    <th>Current Value</th>
    <th>Change Value</th>
	</tr>
	<tr>
    <th>Preliminary</th>
    <th><?php echo number_format($student_total->preliminary,2,'.',' '); ?></th>
    <td><?php echo form_input($preliminaryAttrib); ?></td>
	</tr>
	<tr>
    <th>Midterm</th>
    <th><?php echo number_format($student_total->midterm,2,'.',' '); ?></th>
    <td><?php echo form_input($midtermAttrib); ?></td>
	</tr>
	<tr>
    <th>Semi-Finals</th>
    <th><?php echo number_format($student_total->semi_finals,2,'.',' '); ?></th>
    <td><?php echo form_input($semi_finalsAttrib); ?></td>
	</tr>
	<tr>
    <th>Finals</th>
    <th><?php echo number_format($student_total->finals,2,'.',' '); ?></th>
    <td><?php echo form_input($finalsAttrib); ?></td>
	</tr>
	<tr>
    <th>&nbsp;</th>
    <th>&nbsp;</th>
    <td>
	<?php
	echo form_hidden('enrollment_id', $enrollment_id);
	?>
   		<?php echo form_submit('submit', 'Save'); ?>
	</td>
	</tr>
	</table>
	
<br />
<?php if(!empty($promissory_notes)){ ?>
  <table>
    <tr>
      <th>Value</th>
      <th>Date</th>
      <th>Action</th>
    </tr>
    <?php foreach($promissory_notes as $promissory_note):  ?>
      <tr>
        <td><?php echo number_format($promissory_note->value,2,'.',' '); ?></td>
        <td><?php echo date("F d, Y", strtotime($promissory_note->date)); ?></td>
        <td>
        <a href="#" class="del confirm"
						title="Are you sure you want to delete fee from record? Changes cannot be reverted. continue?">
						Remove
		</a><!--<%= link_to 'Remove', promissory_note, :method => :delete, :confirm => "Are you sure?"  %>--> | 
        <a href="#">Print</a><!--<%= link_to 'Print', promissory_note_path(promissory_note, :format=>"pdf") %>-->
        </td>
      </tr>
    <?php endforeach; ?>
  </table>
<?php } ?>


<br />
<a href="#">Add Promissory</a><!--<%= link_to 'Add Promissory', new_enrollment_promissory_note_path(@enrollment.id) %>-->
<br />
<br />
			<table id="table">
				<thead>
				<tr>
					<th colspan=8>Subjects</th>
				</tr>
				<tr>
					<th>Subject Code</th>
					<th>Section Code</th>
					<th>Description</th>
					<th>Units</th>
					<th>Lab</th>
					<th>Lec</th>
          <th>Time</th>
          <th>Day</th>
				</tr>
				</thead>
				<tbody>

	<?php if(!empty($student_subjects)){ ?>
	<?php foreach($student_subjects as $s): ?>
	<tr>
        <td><?php echo $s->sc_id; ?></td>
	    <td><?php echo $s->code; ?></td>
	    <td><?php echo $s->subject; ?></td>
	    <td><?php echo $s->units; ?></td>
	    <td><?php echo $s->lab; ?></td>
	    <td><?php echo $s->lec; ?></td>
        <td><?php echo $s->time; ?></td>
        <td><?php echo $s->day; ?></td>
        <td><a href="#" class="confirm"
						title="Are you sure you want to delete fee from record? Changes cannot be reverted. continue?">
						Delete
			</a></td>
	</tr>
	<?php endforeach ?>
	<?php } ?>

				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Total</td>
					<td><?php echo number_format($student_total->total_units,2,'.',' '); ?></td>
					<td><?php echo number_format($student_total->total_lab,2,'.',' '); ?></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				
				<tr>
				    <a href="<?=site_url('subjects/add_subject').'/'.$course.'/'.$year.'/'.$semester.'/'.$enrollment_id;?>">Add Subject</a>
				</tr>
				
				</tbody>
			</table>
<?php } ?>
<?php } ?>

	</div>


	
	
	</div>
	
	<div class="clear"></div>
</div>