<?$this->load->view('departments_cpanel/_tab');?>

<div class='alert alert-success' >
	<p><i class="fa fa-info-circle fa-2x"></i>&nbsp;  This feature can only be applied to those departments that can access to students menu like "Student Search".</p>
</div>
<?echo form_open('');?>

<?
	$stud_profile = (isset($spa->stud_profile) && $spa->stud_profile == 1) ? 'checked' : '';
	$stud_grade = (isset($spa->stud_grade) && $spa->stud_grade == 1) ? 'checked' : '';
	$stud_fees = (isset($spa->stud_fees) && $spa->stud_fees == 1) ? 'checked' : '';
	$stud_otr = (isset($spa->stud_otr) && $spa->stud_otr == 1) ? 'checked' : '';
	$stud_issues = (isset($spa->stud_issues) && $spa->stud_issues == 1) ? 'checked' : '';

	$stud_edit_profile = (isset($spa->stud_edit_profile) && $spa->stud_edit_profile == 1) ? 'checked' : '';
	$stud_edit_grade = (isset($spa->stud_edit_grade) && $spa->stud_edit_grade == 1) ? 'checked' : '';
	$stud_add_fees = (isset($spa->stud_add_fees) && $spa->stud_add_fees == 1) ? 'checked' : '';
	$stud_add_issues = (isset($spa->stud_add_issues) && $spa->stud_add_issues == 1) ? 'checked' : '';
	$stud_edit_subject = (isset($spa->stud_edit_subject) && $spa->stud_edit_subject == 1) ? 'checked' : '';

	$stud_portal_activation = (isset($spa->stud_portal_activation) && $spa->stud_portal_activation == 1) ? 'checked' : '';

	$options = array(
		'stud_profile' => array('value' => (isset($spa->stud_profile) && $spa->stud_profile == 1) ? 'checked' : '', 'caption' => 'View Profile' ),
		'stud_edit_profile' => array('value' => (isset($spa->stud_edit_profile) && $spa->stud_edit_profile == 1) ? 'checked' : '', 'caption' => 'Update Profile' ),
		'stud_subject' => array('value' => (isset($spa->stud_subject) && $spa->stud_subject == 1) ? 'checked' : '', 'caption' => 'View Subjects' ),
		'stud_edit_subject' => array('value' => (isset($spa->stud_edit_subject) && $spa->stud_edit_subject == 1) ? 'checked' : '', 'caption' => 'Add / Delete Subject' ),
		'stud_grade' => array('value' => (isset($spa->stud_grade) && $spa->stud_grade == 1) ? 'checked' : '', 'caption' => 'View Grade' ),
		'stud_edit_grade' => array('value' => (isset($spa->stud_edit_grade) && $spa->stud_edit_grade == 1) ? 'checked' : '', 'caption' => 'Edit Grade' ),
		'stud_fees' => array('value' => (isset($spa->stud_fees) && $spa->stud_fees == 1) ? 'checked' : '', 'caption' => 'View Fees' ),
		'stud_add_fees' => array('value' => (isset($spa->stud_add_fees) && $spa->stud_add_fees == 1) ? 'checked' : '', 'caption' => 'Add Payment' ),
		'stud_otr' => array('value' => (isset($spa->stud_otr) && $spa->stud_otr == 1) ? 'checked' : '', 'caption' => 'View OTR' ),
		'stud_issues' => array('value' => (isset($spa->stud_issues) && $spa->stud_issues == 1) ? 'checked' : '', 'caption' => 'View Issues' ),
		'stud_add_issues' => array('value' => (isset($spa->stud_add_issues) && $spa->stud_add_issues == 1) ? 'checked' : '', 'caption' => 'Add / Delete Issues' ),
		'stud_portal_activation' => array('value' => (isset($spa->stud_portal_activation) && $spa->stud_portal_activation == 1) ? 'checked' : '', 'caption' => 'Student Portal Activation' ),
	);
	
?>

<?=panel_head2('Departments Access To Student Profile',false,'primary')?>
	<ul class="list-group">
		<?foreach ($options as $k => $v):?>
			<li class="list-group-item">
				<span style="color:<?=$v['value']==="checked" ? "" : "red"; ?>" class="glyphicon glyphicon-<?=$v['value']==="checked" ? "ok" : "remove"; ?>"></span> &nbsp;
				<?=$v['caption']?>
				<span class="badge pull-right"><input type="checkbox" id="<?=$k?>" name="<?=$k?>" <?=$v['value']?>></span>
			</li>
		<?endforeach;?>
	</ul>
<?=panel_tail2();?>

<?
	$others = (isset($spa->edit_announcement) && $spa->edit_announcement == 1) ? 'checked' : '';

	$options = array(
		'edit_announcement' => array('value' => (isset($spa->edit_announcement) && $spa->edit_announcement == 1) ? 'checked' : '', 'caption' => 'Add, update, delete Announcements' ),
		'edit_calendar' => array('value' => (isset($spa->edit_calendar) && $spa->edit_calendar == 1) ? 'checked' : '', 'caption' => 'Add, update, delete Calendar/Events' ),
	);
	
?>

<?=panel_head2('Others',false,'primary')?>
	<ul class="list-group">
		<?foreach ($options as $k => $v):?>
			<li class="list-group-item">
				<span style="color:<?=$v['value']==="checked" ? "" : "red"; ?>" class="glyphicon glyphicon-<?=$v['value']==="checked" ? "ok" : "remove"; ?>"></span> &nbsp;
				<?=$v['caption']?>
				<span class="badge pull-right"><input type="checkbox" id="<?=$k?>" name="<?=$k?>" <?=$v['value']?>></span>
			</li>
		<?endforeach;?>
	</ul>
<?=panel_tail2();?>
<?echo form_submit('submit','Save Changes');?>
	<a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>gopanel/departments" rel="facebox" class="actionlink"><i class="fa fa-arrow-left"></i>&nbsp;  Back to list</a>	
<?
	echo form_close();
?>