<?$this->load->view('departments_cpanel/_tab');?>

<script type="text/javascript">
	function validate()
	{
		run_pleasewait();
		var ctr = 0;
		var msg = "<ul>";
		var xfocus = "";
		$('.not_blank').css('border-color','gray');
		$('.not_blank').each(function(){
			var xval = $(this).val().trim();
			if(xval == ""){
				ctr++;
				$(this).css('border-color','red');
				if(ctr == 1) { xfocus = $(this); }
				var label = $(this).parent().prev().text();
				
				msg += "<li>"
				+label+" should not be blank</li>";
			}
		});
		
		msg += "<ul>";
		
		if(ctr > 0)
		{
			close_pleasewait();
			xfocus.focus();
			$('#alertModal_Label').text('Required Fields'); //SET MODAL TITLE
			$('#alertModal_Body').html(msg); //SET MODAL CONTENT
			$('#alertModal').modal('show'); //SHOW MODAL
			
			return false;
		}
		else
		{
			var xorder = $('#menu_num').val();
			if(isNumber(xorder) == false)
			{
				close_pleasewait();
				custom_modal('Required','Order should be interger');
			}
		}
	}
</script>

<?if($menu_group):?>
	
	<?echo form_open('','onsubmit="return validate()"');?>
	
	<div class="row">
	  <div class="col-md-3"><label for="caption">Caption</label></div>
	  <div class="col-md-6">
		<?
			$data = array(
				  'name'        => 'menu_group[caption]',
				  'id'   	     => 'caption',
				  'class'		=> 'not_blank',
				  'value'       => isset($menu_group->caption) ? $menu_group->caption : '',
				  'maxLength'   => 30,
				  'placeHolder'	=> 'Required',
				);
	
			echo form_input($data);
		?>
	  </div>
	</div>
	
	<div class="row">
	  <div class="col-md-3"><label for="visible">Visible ?</label></div>
	  <div class="col-md-6">
		<?
			$data = array(
				  'name'        => 'menu_group[visible]',
				  'id'        => 'visible',
				  isset($menu_group->visible) ? 'checked' : '' => isset($menu_group->visible) ? $menu_group->visible : FALSE,
				);

			echo form_checkbox($data);
		?>
	  </div>
	</div>
	
	<div class="row">
	  <div class="col-md-3"><label for="menu_num">Order (Numeric)</label></div>
	  <div class="col-md-6">
		<?
			$data = array(
				  'name'        => 'menu_group[menu_num]',
				  'id'        	=> 'menu_num',
				  'class'       => 'numeric form-control not_blank',
				  'maxLength'   => 3,
				  'value' 	=> isset($menu_group->menu_num) ? $menu_group->menu_num : '',
				  'placeHolder'	=> '0'
				);

			echo form_input($data);
		?>
	  </div>
	</div>
	
	<?=form_submit('submit','Save Changes')?>
	
	<?=form_close();?>
	
<?else:?>
	
	<div class='alert alert-danger'>
		
		<b>
			<span class='glyphicon glyphicon-remove-circle'></span> &nbsp;
			No Menu Group Selected
		</b>
		
	</div>
	
<?endif;?>