<?$this->load->view('departments_cpanel/_tab');?>
<script type="text/javascript">
  function radioChange(data)
  {
	if($(data).val() == 'selection')
	{
		$('#create_menu_group').attr('disabled', true);
		$('#menu_selection').attr('disabled', false);
	}
	else
	{
		$('#create_menu_group').attr('disabled', false);
		$('#menu_selection').attr('disabled', true);
	}
  }
</script>
	
	<?if($menu_list):?>
	
	<?echo form_open('','onsubmit="return validate()"');?>

	<div class="well">
		<div class="row">
		  <div class="col-md-3"><label for="create_menu_group">Menu Group</label></div>
		  <div class="col-md-1">(Select)&nbsp; <input type='radio' checked name='r_menu_grp' id='r_select' value='selection' onchange='radioChange(this)' /></div>
		  <div class="col-md-3"><?=menu_group_dropdown('menu_group',($head) ? $head->id : '','','id="menu_selection"');?></div>
		  <div class="col-md-2">(Create New Group) &nbsp; <input type='radio' name='r_menu_grp' id='r_create' value='create' onchange='radioChange(this)' /></div>
		  <div class="col-md-3">
				<?
					$data = array(
						  'name'        => 'create_menu_group',
						  'id'   	     => 'create_menu_group',
						  'value'       => isset($departments->department) ? $departments->department : '',
						  'maxLength'   => 30,
						  'placeHolder'	=> 'New Menu Head/Group',
						  'disabled'			=> true
						);
			
					echo form_input($data);
				?>
		  </div>
	  </div>
	</div>
	
	<div class="row">
	<?$l =1; foreach($menu_list as $obj):?>
		
			<?=$l++;?>.
			&nbsp;
			<input type='checkbox' name='menus[]' value='<?=$obj->id?>' />
			&nbsp;
			
			<span class='badge'><?=$obj->caption;?></span>
			
			<?if($obj->remarks):?>
			<i><span class='glyphicon glyphicon-hand-right'></span>&nbsp;  <?=$obj->remarks?></i>
			<?endif;?>
			
			
		<br/>
		
	<?endforeach;?>
	<br/>
	
	
	
	<?=form_submit('submit','Save Changes')?>
	
	<?=form_close();?>
	
	<?else:?>
		
		<div class='alert alert-danger'>
		
			<b>
				<span class='glyphicon glyphicon-remove-circle'></span> &nbsp;
				There's seems no menu record to be selected. Kindly call your school administrator.
			</b>
			
		</div>
		
	<?endif;?>
	</div>