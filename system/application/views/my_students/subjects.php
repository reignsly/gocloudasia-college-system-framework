<script type="text/javascript">
  $(document).ready(function(){
    $("#unlock_grade").click(function(event) {
      event.preventDefault();
      if($(this).text().trim() == "Click to Edit Grades"){
        $('.grade_tables input[type="text"]').removeClass('hidden');  
        $('.grade_tables input[type="number"]').removeClass('hidden');  
        $('.txt-remarks').removeClass('hidden');  
        $('.grade-badge').addClass('hidden');  
        $('.span-remarks').addClass('hidden');  
        $('#udpate_grade').attr('disabled', false);
        $(this).text("Lock Grades");
      }else{
        $('.grade_tables input[type="text"]').addClass('hidden');  
        $('.grade_tables input[type="number"]').addClass('hidden');  
        $('.txt-remarks').addClass('hidden');  
        $('.grade-badge').removeClass('hidden');  
        $('.span-remarks').removeClass('hidden');  
        $(this).text("Click to Edit Grades");
        $('#udpate_grade').attr('disabled', true);
      }      
    })
    $("#sform").validate({
      submitHandler: function(form) {
        form.submit();
      }
    });
  })
</script>
<div class="row">
	<?=panel_head2('Student Profile',false,'primary')?>
	
	<div class="table-responsive">	  	
		<table class="table table-bordered">
			<tr>
				<td><span class="bold">Student Name:</span></td>
				<td  colspan="4" class="student-name">
					<strong><?php echo ucwords($profile->full_name);?></strong>
					&nbsp;&nbsp;
					<?if(isset($show_tri_menu) && $show_tri_menu ):?>
						<?if($mydepartment->stud_profile == 1):?>
						<a class="btn btn-sm btn-primary" href="<?=base_url('profile/view/'.$_student->enrollment_id.$search)?>"><i class="fa fa-file-text"></i>&nbsp;  Profile</a>
						<?endif;?>
						
						<?if($mydepartment->stud_fees == 1):?>
						<a class="btn btn-sm btn-primary" href="<?=base_url('fees/view_fees/'.$_student->enrollment_id.$search)?>"><i class="fa fa-list-alt"></i>&nbsp;  Fees</a>
						<?endif;?>

						<?if(isset($show_payment_btn) && $show_payment_btn):?>
							<?if($this->router->method == "show_payment" || $this->router->method == "add_student_payment" && $mydepartment->stud_fees == 1):?>
							<a class="btn btn-sm btn-primary" href="<?=base_url('fees/payment_record/'.$_student->enrollment_id.$search)?>"><i class="fa fa-list-alt"></i>&nbsp;  Payment Records</a>
							<?endif;?>
							<?if($this->router->method == "payment_record" && $mydepartment->stud_add_fees === "1"):?>
							<a class="btn btn-sm btn-success confirm" href="<?=base_url('fees/add_student_payment/'.$_student->enrollment_id.$search)?>"><i class="fa fa-list-alt"></i>&nbsp;  Add Payment</a>
							<?endif;?>

						<?endif;?>
						
					<?endif;?>
				</td>
			</tr>
			<tr>
				<td class="student-name">
					<span class="bold">ID Number</span>
				</td>
				<td class="student-name">	
					<span class="bold">Course</span>
				</td>
				<td class="student-name">
					<span class="bold">Year</span>
				</td>
				<td class="student-name">
					<span class="bold">Semester</span>
				</td>
				<td class="student-name">
					<span class="bold">School Year</span>
				</td>
				<td class="student-name">
					<span class="bold">Grading Period</span>
				</td>
			</tr>
			<tr>
				<td class="student-name">
					<span class="label label-default label-lg"><?php echo ucwords($profile->studid);?></span>
				</td>
				<td class="student-name">
					<span class="label label-default label-lg"><?php echo ucwords($profile->course);?></span>	
					<?if($profile->specialization):?>
						<p></p>
						<span class="label label-default label-lg"><?php echo ucwords($profile->specialization);?></span>
					<?endif;?>
				</td>
				<td class="student-name">
					<span class="label label-default label-lg"><?php echo $profile->year;?></span>
				</td>
				<td class="student-name">
					<span class="label label-default label-lg"><?php echo $profile->name;?></span>
				</td>
				<td class="student-name">
					<span class="label label-default label-lg"><?php echo $profile->sy_from;?>-<?php echo $profile->sy_to;?></span>
				</td>
				<td class="student-name">
					<span class="label label-default label-lg"><?=$current_gp->grading_period?></span>
				</td>
			</tr>
			</tr>
		</table>
	</div>	
	<?=panel_tail2();?>
</div>

<div class="row">
	<div class="well">
		<a href="<?=site_url('my_students')?>" class="btn btn-sm btn-default"><i class="fa fa-hand-o-left"></i>&nbsp; Cancel</a>
		<div class="btn-group btn-group-sm">
		  <button type="button" class="btn btn-success">Select to Change Student</button>
		  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
		    <span class="caret"></span>
		    <span class="sr-only">Toggle Dropdown</span>
		  </button>
		  <ul class="dropdown-menu" role="menu">
		    <?if($students): foreach ($students as $id => $v):?>
		    	<li><a href="<?=site_url('my_students/subjects/'.$id)?>"><?=ucwords($v);?></a></li>
		    <?endforeach; endif;?>
		  </ul>
		</div>
		<a target="_blank" href="<?=site_url('my_students/print_subjects/'.$enrollment_id)?>" class="btn btn-sm btn-tumblr"><span class="entypo-print"></span> &nbsp; Print (PDF)</a>
	</div>
</div>

<div class="row">
	<?=panel_head2('Student Subjects',false,'default')?>
		
		<?=form_open('','id="sform"')?>
		<?if($mydepartment->stud_edit_grade === "1" && isset($students) && $students):?>
	      <div class='btn-group'>
	        <button class="btn btn-sm btn-success" type="submit" value="update_grade" name="update_grade" id="udpate_grade" disabled ><i class="fa fa-edit"></i>&nbsp; Update Grade</button>
	        <button  data-toggle="tooltip" data-placement="top" title="Unlock/Lock the grades input fields" class='tp btn btn-danger btn-sm' id='unlock_grade' > Click to Edit Grades</button>
	      </div>
	    <?endif;?>
    <a href="#" class="btn btn-sm btn-default">Total Subjects &nbsp; <span class="badge"><?=isset($grades->subjects)?count($grades->subjects):0?></span></a>  
		<div class="btn-group pull-right">
    	<?if($recalculate_grade):?>
        <a href="<?php echo site_url('my_students/recalculate/'.__link($enrollment_id)) ?>" class='btn btn-facebook btn-sm confirm' title='Student grade conversion will recompute based on the grading system table. Do you want to continue?' ><i class="fa fa-refresh"></i>&nbsp; Recompute</a>
        <button type='button' modal-id = "myModal_grading_system" modal-title="Grading System Table" modal-size ="wide" class='tp btn btn-default btn-sm button-modal' id='unlock_grade' > Grading Table</button>
      <?endif;?>
    </div>
		<p></p>
		
		<div class="table-responsive">
  
			  <table class="table grade_tables table-condensed table-sortable">
			    <thead>
			      <tr class='gray' >
			        <th>Course No.</th>
			        <th class="hidden-xs" >Description</th>
			        <th class="visible-xs" >Description</th>
			        <?if(isset($grades->periods) && $grades->periods):?>
			          <?foreach ($grades->periods as $gp_id => $p):?>
			            <th width="10%" ><?=$p?></th>
			          <?endforeach;?>
			        <?endif;?>
			        <th >Remarks</th>
			      </tr>
			    </thead>

			    <?if(isset($grades->subjects) && $grades->subjects):?>
			      <?foreach ($grades->subjects as $k => $subjects):?>

			        <? $grade = $subjects['grade']; ?>
			        <? $subject = $subjects['subject']; ?>
			        
			        <?if($subject):?>
			          <tr>
			            <td class='bold' ><?= $subject->code; ?></td>
			            <td class="hidden-xs bold" ><?= ucwords(strtolower($subject->subject)); ?></td>
			            <td class="visible-xs bold" ><?= ucwords(character_limiter(strtolower($subject->subject), 10)); ?></td>
			            
			            <?if($grade):?>
			              <?foreach ($grade as $g_k => $g):?>

			                <td>
												
												<?if($edit_grade && $mydepartment->stud_edit_grade === "1"):?>
													
													<?if($edit_all_period):?>
														
														<span class='label label-as-badge label-default grade-badge' ><?php echo ($g->converted); ?></span>
														<input class='grid hidden form-control' type='text' name="studentsubject[<?=$g->id?>]" id="" value="<?=$g->value;?>" max="<?php echo $max_grade ?>" min="<?php echo $min_grade ?>" step="any" placeHolder="0.00"/>

													<?else:?>

														<?if($g->gp_id == $current_gp->id):?>
															<span class='label label-as-badge label-default grade-badge' ><?php echo ($g->converted); ?></span>
														<input class='grid hidden form-control' type='text' name="studentsubject[<?=$g->id?>]" id="" value="<?=$g->value;?>" max="<?php echo $max_grade ?>" min="<?php echo $min_grade ?>" step="any" placeHolder="0.00"/>
														<?else:?>

															<span class='label label-as-badge label-default' ><?php echo ($g->converted); ?></span>

														<?endif;?>

													<?endif;?>

												<?else:?>
													<span class='label label-as-badge label-default' ><?php echo ($g->converted); ?></span>
												<?endif;?>

			                </td>

			              <?endforeach;?>
			            <?endif;?>
			            <td><input type='text' name='final[<?=$subject->id?>][remarks]' value='<?=$subject->remarks?>'  placeHolder="Remarks"></td>

			          </tr>
			        <?endif;?>

			      <?endforeach;?>
			    <?else:?>
			      <tr><td colspan="3" ><h4>&nbsp;<i class="fa fa-warning"></i>&nbsp; No Grades Available</h4></td></tr>
			    <?endif;?>

			  </table>
		</div>
	
		<?=form_close();?>	

	<?=panel_tail2();?>
</div>

<div style="display:none;">
  <?php @$this->load->view('layouts/_grading_system'); ?>
</div>