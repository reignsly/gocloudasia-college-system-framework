<div id="right">
	<?php 
		$formAttrib = array('id' => 'search_enrollee', 'class' => 'form-inline', 'method' => 'GET');
		echo form_open('main_menus/index/0', $formAttrib);
	?>
    
 <div class="form-group">
    <input id="controller" class="form-control" type="text" value="<?=isset($controller)?$controller:''?>" name="controller" placeholder="Controller">
  </div>
   
  <div class="form-group">
    <input id="caption" class="form-control" type="text" value="<?=isset($caption)?$caption:''?>" name="caption" placeholder="Caption">
  </div>

  <div class="form-group">
    <?php echo form_submit('submit', 'Search'); ?>
  </div>

  <div class="form-group">
    <a class='btn btn-default btn-sm' href='<?=base_url("main_menus/create")?>'><i class="fa fa-plus"></i>&nbsp; Create New</a>
  </div>
	
	<?echo form_close();?>

<br>
<?echo isset($links) ? $links : NULL;?>
	<?php
	if(isset($search)){
	?>
	<br />
	<table>
		<tr>
		  <th>Caption</th>
		  <th>Controller</th>
		  <th>Action</th>
		</tr>
		<tbody>
		<?php
		if(!empty($search))
		{
			foreach($search as $obj):
			?>
			<tr>
			<td><?php echo $obj->caption; ?></td>
			<td><?php echo $obj->controller; ?></td>
			<td>
				
				<div class="btn-group btn-group-sm">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-caret-square-o-down"></i> <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
					
					
					<li><a href="<?=base_url('main_menus/edit/'.$obj->id)?>"></i><i class="fa fa-pencil"></i>&nbsp; Edit</a></li>
					<li><a class='confirm' href="<?=base_url('main_menus/delete/'.$obj->id)?>"><i class="fa fa-trash-o"></i>&nbsp;  Destroy</a></li>
					
				 </ul>
				</div>
				
			</td>
			</tr>
			<?php 
			endforeach; ?>
			<tr>
				<td colspan='2' align='right'><b>Total Records</b></td>
				<td><span class='badge'><?=($total_rows) ? $total_rows : 0;?></span></td>
			</tr>
			<?php
		}
		else
		{
		?>
		<tr>
		<td colspan="2">
			No record found.
		</td>
		</tr>
		<?php
		}
		?>
		</tbody>
	</table>
	<?echo isset($links) ? $links : NULL;?>
	<?php
	}
	?>
	</div>
	
	<div class="clear"></div>
</div>