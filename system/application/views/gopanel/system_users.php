<?=form_open('gopanel/system_users/0','method="GET"');?>

  <div class="row">
    <div class="col-md-2">
      <label for="username">Username</label>
      <input type="text" name="username" id="username" class="form-control" value="<?=isset($username)?$username:""?>" placeHolder="Username">
    </div>
    <div class="col-md-2">
      <label for="email">Email</label>
      <input type="text" name="email" id="email" class="form-control" value="<?=isset($email)?$email:""?>" placeHolder="email">
    </div>
    <div class="col-md-2">
      <label for="name">Name</label>
      <input type="text" name="name" id="name" class="form-control" value="<?=isset($name)?$name:""?>" placeHolder="name">
    </div>
    <div class="col-md-2">
      <label for="name">Expiration (+)</label>
      <input type="text" name="expiration" id="expiration" class="form-control datepicker_ymd" value="<?=isset($expiration)?$expiration:""?>" placeHolder="expiration">
    </div>
    <div class="col-md-4">
      <label for="submit">&nbsp;</label><br>
      <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-search"></i>&nbsp;Search</button>
      <a href="<?=site_url('gopanel/create_system_user')?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp; Create New User</a>
    </div>
  </div>

<?=form_close();?>
<p></p>
<?if($search):?>
  
  <div class="table-responsive">
    <table class="table table-condensed table-bordered">
      <thead>
        <tr class="success">
          <td>Username</td>
          <td>Email</td>
          <td>Name</td>
          <td>Expiration</td>
          <td>Action</td>
        </tr>
      </thead>
      <?foreach ($search as $k => $v):?>
        <tr>
          <td><strong><?=$v->username?></strong></td>
          <td><?=$v->email?></td>
          <td><?=ucwords($v->name)?></td>
          <td><strong><?= $v->expiration ? date('M d, Y g:i a',strtotime($v->expiration)) : '<span class="badge">None</span>' ?></strong></td>
          <td>
            <a href="<?=site_url('gopanel/create_system_user/'.__link($v->id))?>" class="" title="Update?"><span class="glyphicon glyphicon-edit"></span></a>
            <a href="<?=site_url('gopanel/delete_system_user/'.__link($v->id))?>" class="" title="Delete?"><span class="glyphicon glyphicon-remove text-danger"></span></a>
          </td>
        </tr>
      <?endforeach;?>
    </table>
  </div>

<?else:?>
  
  <div class="alert alert-info">No record available.</div>

<?endif;?>