<?=form_open('', 'class="form-horizontal"');?>
  <fieldset>

    <?@$this->load->view('gopanel/_form_system_user')?>

    <!-- Button (Double) -->
    <div class="form-group">
      <label class="col-md-2 control-label" for="button1id"></label>
      <div class="col-md-8">
        <button type="submit" name="save" value="save" id="save" class="btn btn-success">Submit</button>
        <a href="<?=site_url('gopanel/system_users')?>" class="btn btn-default">Cancel</a>
      </div>
    </div>
  </fieldset>
</form>

<?if(isset($user) && $user):?>

<?=form_open('', 'class="form-horizontal"');?>
  <fieldset>

  <!-- Form Name -->
  <legend>Change Password</legend>

  
  <!-- Text input-->
  <div class="form-group">
    <label class="col-md-2 control-label" for="password">Password</label>  
    <div class="col-md-4">
    <input id="password" name="password" type="text" placeholder="Password" class="form-control input-md"  pattern=".{6,}" title="6 characters minimum" required>
    <span class="text-danger"><?=form_error('password')?> </span>
    <input id="verify_password" name="verify_password" type="text" placeholder="Verify Password" class="form-control input-md" pattern=".{6,}" title="6 characters minimum" required>
    <span class="text-danger"><?=form_error('verify_password')?>  </span>
    </div>
  </div>

  <!-- Button (Double) -->
  <div class="form-group">
      <label class="col-md-2 control-label" for="button1id"></label>
      <div class="col-md-8">
        <button type="submit" name="sukat_password" value="sukat_password" class="btn btn-success">Change Password</button>
      </div>
    </div>
  </fieldset>
</form>

<?endif;?>