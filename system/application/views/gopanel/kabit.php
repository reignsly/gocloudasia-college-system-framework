<?php
	echo panel_head(array(
			"panel_title" => "Database Connection"
		));
?>
	<div class="table-responsive">
		<?=form_open('')?>
		<table class="table">
			<tr class="info">
				<td>Host</td>
				<td>Username</td>
				<td>Password</td>
				<td>Database</td>
			</tr>

			<tr>
				<td><input type="text" name="host" id="host" required maxlength="30" value="<?=$host?>" placeHolder="Hostname" ></td>
				<td><input type="text" name="user" id="user" required maxlength="30" value="<?=$user?>" placeHolder="Username" ></td>
				<td><input type="text" name="password" id="password" maxlength="50" value="<?=$password?>" placeHolder="Password" ></td>
				<td><input type="text" name="database" id="database" required maxlength="50" value="<?=$database?>" placeHolder="Database" ></td>
			</tr>

			<tr>
				<td colspan="4" align = "right" >
					<input type="hidden" name="form_token" value="<?=$form_token?>" />
					<input type="submit" value="Create And Download File" name="download_database_connection">
					<input type="submit" value="Update Database Connection" name="update_database_connection">
				</td>
			</tr>
		</table>
		</form>
	</div>
<? echo panel_tail(); ?>