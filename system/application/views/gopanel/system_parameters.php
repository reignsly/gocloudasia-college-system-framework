<?=form_open('','class="form-horizontal"')?>
<fieldset>

<!-- Form Name -->
<legend>Form Name</legend>

  <div class="form-group">
    <label class="col-md-3 control-label bold" for="tmp_studid_series">Temporary Student ID Series (Enrollment Reference ID)</label>
    <div class="col-md-9">
    <input value="<?=isset($syspar)?$syspar->tmp_studid_series:''?>" id="tmp_studid_series" name="syspar[tmp_studid_series]" type="text" placeholder="tmp_studid_series" class="input-xlarge" pattern=".{8,8}" maxlength = '8' oninvalid="setCustomValidity('This field must be 8 characters only. ')" >
    <span class="help-block">Used By Enrollment as temporary studid or the reference id. (Should be 8 characters)</span>  
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-3 control-label bold" for="studid_series">Student ID Series</label>
    <div class="col-md-9">
    <input value="<?=isset($syspar)?$syspar->studid_series:''?>" id="studid_series" name="syspar[studid_series]" type="text" placeholder="studid_series" class="input-xlarge" pattern=".{8,8}" maxlength = '8' oninvalid="setCustomValidity('This field must be 8 characters only. ')" >
    <span class="help-block">Used By Enrollment : auto-generated student id when saving the enrollment process. (Should be 8 characters)</span>  
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-3 control-label bold" for="subj_refid_series">Subject Ref. ID Series</label>
    <div class="col-md-9">
    <input value="<?=isset($syspar)?$syspar->subj_refid_series:''?>" id="subj_refid_series" name="syspar[subj_refid_series]" type="text" placeholder="subj_refid_series" class="input-xlarge" pattern=".{10,10}" maxlength = '10' oninvalid="setCustomValidity('This field must be 10 characters only. ')" >
    <span class="help-block">Auto-generated subject reference id when adding new subject. (Should be 10 characters)</span>  
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-3 control-label bold" for="is_auto_id">Is Student ID auto-generated?</label>
    <div class="col-md-9">
     <?php
        $xoption[0] = 'No';
        $xoption[1] = 'Yes';
      ?>
      <?=form_dropdown('syspar[is_auto_id]',$xoption,isset($syspar)?$syspar->is_auto_id:'', 'class="input-xlarge"')?>
    <span class="help-block">Used in enrollment if student id is computer generated or manual. (Manual Input not yet develop. Please maintain it as auto.)</span>  
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-3 control-label bold" for="is_auto_id">Bootrap Theme</label>
    <div class="col-md-9">
      <?php
        $xboot['CURELEAN'] = 'CURELEAN';
        $xboot['DEFAULT'] = 'DEFAULT';
        $xboot['FLATLY'] = 'FLATLY';
        $xboot['JOURNAL'] = 'JOURNAL';
        $xboot['LUMEN'] = 'LUMEN';
        $xboot['READABLE'] = 'READABLE';
        $xboot['SIMPLEX'] = 'SIMPLEX';
        $xboot['SLATE'] = 'SLATE';
        $xboot['SPACELAB'] = 'SPACELAB';
        $xboot['SUPERHERO'] = 'SUPERHERO';
        $xboot['UNITED'] = 'UNITED';
        $xboot['YETI'] = 'YETI';
      ?>
      <?=form_dropdown('syspar[bootstrap_theme]',$xboot,isset($syspar)?$syspar->bootstrap_theme:'', 'class="input-xlarge"')?>
    <span class="help-block">Note : The system was designed to fit in "Yeti" theme.</span>  
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-3 control-label bold" for="reg_success_msg">Registration Success Message</label>
    <div class="col-md-9">
    <input value="<?=isset($syspar)?$syspar->reg_success_msg:''?>" id="reg_success_msg" name="syspar[reg_success_msg]" type="text" placeholder="subj_refid_series" class="input-xlarge" >
    <span class="help-block">This is a message being displayed after a student finishes his/her registration</span>  
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-3 control-label bold" for="is_auto_id">Is Menu Accordion?</label>
    <div class="col-md-9">
     <?php
        $xacc[0] = 'No';
        $xacc[1] = 'Yes';
      ?>
      <?=form_dropdown('syspar[is_menu_accordion]',$xacc,isset($syspar)?$syspar->is_menu_accordion:'', 'class="input-xlarge"')?>
    <span class="help-block">If Yes, menu will display as accordion and no if collapsible.</span>  
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-3 control-label bold" for="is_auto_id">Show Menu?</label>
    <div class="col-md-9">
     <?php
        $xacc[0] = 'No';
        $xacc[1] = 'Yes';
      ?>
      <?=form_dropdown('syspar[show_menu]',$xacc,isset($syspar)?$syspar->show_menu:'', 'class="input-xlarge"')?>
    <span class="help-block">Whether to show or hide the side bar menu</span>  
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-3 control-label bold" for="is_auto_id">Show Menu Tool Tip</label>
    <div class="col-md-9">
     <?php
        $xacc[0] = 'No';
        $xacc[1] = 'Yes';
      ?>
      <?=form_dropdown('syspar[show_menu_tip]',$xacc,isset($syspar)?$syspar->show_menu_tip:'', 'class="input-xlarge"')?>
    <span class="help-block">Whether to show or hide the side bar menu tool tip</span>  
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-3 control-label bold" for="application_layout">Main System Layout</label>
    <div class="col-md-9">
     <?php
        $xapplication_layout['application'] = 'Default - Logo in Text Form';
        $xapplication_layout['application2'] = 'With Logo';
        $xapplication_layout['application_sbadmin'] = 'SB Admin - Logo in Text Form';
      ?>
      <?=form_dropdown('syspar[application_layout]',$xapplication_layout,isset($syspar)?$syspar->application_layout:'', 'class="input-xlarge"')?>
    <span class="help-block">Main Layout for the system.</span>  
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-3 control-label bold" for="application_layout">Welcome Layout or Login Layout</label>
    <div class="col-md-9">
      <?php
        $xwelcome_layout['welcome'] = 'Default - School Name in Text Form';
        $xwelcome_layout['welcome2'] = 'With Logo';
      ?>
      <?=form_dropdown('syspar[welcome_layout]',$xwelcome_layout,isset($syspar)?$syspar->welcome_layout:'', 'class="input-xlarge"')?>
    <span class="help-block">Layout for the welcome and login screen.</span>  
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-3 control-label bold" for="grading_system">Grading System</label>
    <div class="col-md-9">
      <?php
        $grading_system['numeric'] = 'Numeric';
        $grading_system['input'] = 'Input - Raw Grade (No Conversion)';
      ?>
      <?=form_dropdown('syspar[grading_system]',$grading_system,isset($syspar)?$syspar->grading_system:'', 'class="input-xlarge"')?>
    <span class="help-block">Grading System to be used by system.</span>  
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-3 control-label bold" for="button1id"></label>
    <div class="col-md-9">
      <button id="button1id" name="button1id" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp; Update</button>
    </div>
  </div>

</fieldset>
</form>
