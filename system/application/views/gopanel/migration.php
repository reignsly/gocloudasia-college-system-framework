<?php
	echo panel_head(array(
			"panel_title" => isset($migration) && $migration ? "Migrations  : ".$migration->version.' / '.$migration_count : ''
		));
?>
	<?if($migration):?>
		
		<div class="table-responsive">
		<table class="table table-stripped" >
			<tr class="warning">
				<td><h3>Information</h3></td>
				<td><h3>Action</h3></td>
			</tr>
			
			<?if($migration->version == 0):?>
			<tr class="danger" >
				<td><b>Latest migration is zero, it is reccomended to run first migration version</b></td>
				<td><a class="btn btn-success btn-sm confirm" title = "Are you sure to run the first migration on your system? <br/> This may take a while." href="<?=base_url('gopanel/run_first_migration')?>"><i class="fa fa-info"></i>&nbsp; Run First Migration</a></td>
			</tr>
			<?endif;?>

			<tr>
				<td><b>Run the latest migration file inside migration folder</b></td>
				<td><a class="btn btn-primary btn-sm confirm" title = "Are you sure to run the latest migration on your system? <br/> This may take a while." href="<?=base_url('gopanel/migration/latest')?>"><i class="fa fa-arrows"></i>&nbsp; Run Latest</a></td>
			</tr>
			
			<?if($migration->version >= 1):?>
			<tr>
				<td><b>Rollback your current version. It will run the migration which is under the latest version</b></td>
				<td><a class="btn btn-danger btn-sm confirm" title = "Are you sure to rollback your migration? <br/> This may take a while." href="<?=base_url('gopanel/migration/rollback')?>"><i class="fa fa-arrow-left"></i>&nbsp; Rollback</a></td>
			</tr>
			<?endif;?>
			
			<?=form_open('')?>
			<tr>
				<td>
					<b>
						Run migration of specific version. Used this function when you know what you are doing.
						<input class='form-control' type="number" min = "0" name="version_no" id="" required value="<?=$migration->version;?>" placeHolder="<?=$migration->version;?>" >
					</b>
					</td>
				<td>
					<input type="submit" value="Run Version" name="run_version" class="btn btn-danger btn-sm" />
				</td>
			</tr>
			<?=form_close();?>

		</table>
		</div>

	<?else:?>
		<a class="btn btn-danger btn-sm confirm" href="<?=base_url('gopanel/migration/run_first_migration')?>"><i class="fa fa-arrow-left"></i>&nbsp; Run</a> &nbsp; your first migration.
	<?endif;?>
<? echo panel_tail(); ?>