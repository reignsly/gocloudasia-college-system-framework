<?panel_head2('Backup Database');?>

	<div class="table-responsive">
		<table class="table table-hovered">
			<tr>
				<td><strong> Dump Database and Download as SQL file.</strong></td>
				<td><a class="btn btn-xs btn-primary confirm" target="_blank" href="<?=site_url('gopanel/backup/download')?>"><i class="fa fa-download"></i>&nbsp; Download as SQL File</a></td>
			</tr>
		</table>
	</div>

<?panel_tail2();?>