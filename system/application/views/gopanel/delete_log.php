<script type="text/javascript" >
	$(document).ready(function(){
	    $("#txtFromDate").datepicker({
	        numberOfMonths: 2,
	        onSelect: function(selected) {
	          $("#txtToDate").datepicker("option","minDate", selected)
	        }
	    });
	    $("#txtToDate").datepicker({ 
	        numberOfMonths: 2,
	        onSelect: function(selected) {
	           $("#txtFromDate").datepicker("option","maxDate", selected)
	        }
	    });  
	});
</script>

<div class="panel panel-primary">
  <div class="panel-heading"><strong>Select date range to clear activity log</strong></div>
  <div class="panel-body">
  	<?=form_open()?>
	    <table>
	    	<tr>
	    		<td><strong>Date From</strong></td>
	    		<td><strong>Date To</strong></td>
	    		<td></td>
	    	</tr>
	    	<tr>
	    		<td><input type="text" name="from" id="txtFromDate" required></td>
	    		<td><input type="text" name="to" id="txtToDate" required></td>
	    		<td>
	    			<button type="submit" class="btn btn-primary" name="clear_log" value="clear_log" ><i class="fa fa-refresh"></i>&nbsp; Delete Log</button>
	    			<a href="<?=site_url('gopanel/activity_log')?>" class="btn btn-default" >Cancel</a>
	    		</td>
	    	</tr>
	    </table>
	   <?=form_close();?>
  </div>
</div>