<!-- HIDE FOR SMALL DEVICES - Phones (<768px) -->
<div class='hidden-xs user_container'>
<?if($levels):?>
	<?foreach($levels as $lvl):?>
		<?if(isset($departments[$lvl->level]) && is_array($departments[$lvl->level])):?>
			<?	
				switch($lvl->level)
				{
					case 1:?>
						<?foreach($departments[$lvl->level] as $dep):?>
							<div class="row">
								<div class="col-md-12">
										<p class="text-center">
											<a href="<?php echo base_url(); ?><?=$dep->controller?>/<?=$dep->department?>"><img class="welcome_icon" alt="<?=$dep->department?>" height="100" src="<?php echo base_url(); ?><?=$dep->icon?>" width="100"   /><br/><span class='label label-login text-center' style='font-size:8pt' ><?=$dep->description?></span></a>
										</p>
								</div>
							</div>
						<?endforeach;?>
					<?break;
					
					
					case 2:
						$ctr = 0;
						foreach($departments[$lvl->level] as $dep)
						{
							if($ctr == 0){
								echo "<div class = 'row'>";
							}
							$ctr++;
						?>
						
							<div class="col-md-6 col-xs-6">
									<p class="text-center">
										<a href="<?php echo base_url(); ?><?=$dep->controller?>/<?=$dep->department?>"><img class="welcome_icon" alt="<?=$dep->department?>" height="100" src="<?php echo base_url(); ?><?=$dep->icon?>" width="100"   /><br><span class='label label-login text-center' style='font-size:8pt' ><?=$dep->description?></span></a>
									</p>
							</div>
						<?
							if($ctr == 2){
								echo "</div>";
								$ctr = 0;
							}
						}
						
						if($ctr != 2)
						{
							echo "</div>";
						}
					break;
					
					case 3:
						$ctr = 0;
						foreach($departments[$lvl->level] as $dep)
						{
							if($ctr == 0){
								echo "<div class = 'row'>";
							}
							$ctr++;
						?>
						
							<div class="col-md-4 col-xs-4">
									<p class="text-center">
										<a href="<?php echo base_url(); ?><?=$dep->controller?>/<?=$dep->department?>"><img class="welcome_icon" alt="<?=$dep->department?>" height="100" src="<?php echo base_url(); ?><?=$dep->icon?>" width="100"   /><br><span class='label label-login text-center' style='font-size:8pt' ><?=$dep->description?></span></a>
									</p>
							</div>
						<?
							if($ctr == 3){
								echo "</div>";
								$ctr = 0;
							}
						}
						
						if($ctr != 3)
						{
							echo "</div>";
						}
					break;
					
					case 4:
						$ctr = 0;
						foreach($departments[$lvl->level] as $dep)
						{
							if($ctr == 0){
								echo "<div class = 'row'>";
							}
							$ctr++;
						?>
						
							<div class="col-md-3 col-xs-3">
									<p class="text-center">
										<a href="<?php echo base_url(); ?><?=$dep->controller?>/<?=$dep->department?>"><img class="welcome_icon" alt="<?=$dep->department?>" height="100" src="<?php echo base_url(); ?><?=$dep->icon?>" width="100"   /><br><span class='label text-center label-login' style='' ><?=$dep->description?></span></a>
									</p>
							</div>
						<?
							if($ctr == 4){
								echo "</div>";
								$ctr = 0;
							}
						}
						if($ctr != 4)
						{
							echo "</div>";
						}
					break;?>
				<?}?>
			
		<?endif;?>
	<?endforeach;?>
<?endif;?>
</div>
<!-- HIDE FOR SMALL DEVICES - Phones (<768px) -->
<div class='visible-xs user_container'>
<?if($levels):?>
	<?foreach($levels as $lvl):?>
		<?if(isset($departments[$lvl->level]) && is_array($departments[$lvl->level])):?>
				
			<?foreach($departments[$lvl->level] as $dep):?>
				<div class="row">
					<div class="col-md-2">
						<p class="text-center">
							<a class="thumbnails" href="<?php echo base_url(); ?><?=$dep->controller?>/<?=$dep->department?>"><img class="welcome_icon" alt="<?=$dep->department?>" height="100" src="<?php echo base_url(); ?><?=$dep->icon?>" width="100"   /><br/><span class='label label-login text-center' style='font-size:8pt' ><?=$dep->description?></span></a>
						</p>
					</div>
				</div>
			<?endforeach;?>
			
		<?endif;?>
	<?endforeach;?>
<?endif;?>

</div>



