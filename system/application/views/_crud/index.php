<?
	if(isset($disable_system_message) && $disable_system_message == false)
	{
		if(isset($system_message) && $system_message)
		{
			echo $system_message;
			echo validation_errors();
		}
	}
?>
<!-- FILTER IF AVAILABLE -->
<?if($search):?>
<div class="row">
	<div class="col-md-12">
		<?=form_open($form_url,'class="form-vertical" method="GET"')?>
			<!-- Prepended text-->
			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-1 control-label" for="selectbasic">Filter By</label>
			  <div class="col-md-2">
			  	<?=form_dropdown('filter_by',$filter_list,isset($filter_by)?$filter_by:'')?>
			  </div>
			  <div class="col-md-2">
			    <input id="keyword" name="keyword" class="form-control" placeholder="Keyword" type="text" value="<?=isset($keyword)?$keyword:''?>" >
			  </div>

			  <div class="col-md-7">
			    <button class="btn btn-primary btn-sm" type="submit" value="Search" name="search" ><i class="fa fa-search"></i>&nbsp; Search</button>
			    <a class="btn btn-default btn-sm btn-sm" href="<?=site_url($controller_method)?>">Clear</a>
			    <?endif;?>
			    
				    <?if($add_button):?>
				    	<a class="btn btn-default btn-sm btn-sm" href="<?=site_url($controller_method).'/create'?>">Create New Record</a>
				    <?endif;?>
				    <?if($additional_button && is_array($additional_button)):?>
				    	<?foreach ($additional_button as $k => $v): $v = (object)$v;?>
				    		<a class="<?=$v->class?>" href="<?=$v->url?>"><?=$v->title?></a>
				    	<?endforeach;?>
				    <?endif;?>

			    <?if($search):?>
			  </div>
			</div>
		</form>
	</div>
</div><hr>
<?endif;?>

<?if($result):?>
<div class="row">
	<div class="col-md-12">
		<?=isset($links) ? $links : ''?>
		<div class="table-responsive">
			<table class="table table-hover table-sortable table-striped">
				<thead>
					<tr class='gray' >
						<?if(isset($column_name) && $column_name):?>		
					
							<?foreach ($column_name as $key => $value):?>
								<?if($key==$uniq_id && $hide_id):?>
								<?else:?>
								<th><?=ucwords($value)?></th>
								<?endif;?>
							<?endforeach;?>

						<?endif?>

						<?if($action_button):?>
							<th>Actions</th>
						<?endif;?>
					</tr>
				</thead>
				
				<?foreach ($result as $key => $value):?>
					
					<tr>
						
						<?if(isset($column_name) && $column_name):?>
							<?foreach ($column_name as $col_k => $col_v):?>
								<?if($col_k==$uniq_id && $hide_id):?>
								<?else:?>
								<td class='bold' ><?=isset($value->$col_k) ? $value->$col_k : "Missing Field" ?></td>
								<?endif;?>
							<?endforeach;?>
						<?endif;?>
					
						<?if($action_button):?>
							<td>
								<div class="btn-group btn-group-sm">
									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-caret-square-o-down"></i> <span class="caret"></span>
									</button>
									<ul class="dropdown-menu pull-right" role="menu">
										<!-- CHECK THE OVERRIDE VIEW BUTTON -->
										<?if($view_button && !is_string($view_button)):?>
											<?php
												$view_button = (object)$view_button;
											?>
											<li><a href="<?=site_url($view_button->controller.'/'.$view_button->method.'/'.get_segments($value,$view_button->field))?>"><i class="<?=isset($delete_button->icon_class)?$delete_button->icon_class:'glyphicon glyphicon-eye-open'?>"></i>&nbsp;  View</a></li>
										<?elseif($view_button === false):?>
										<?else:?>
											<li><a href="<?=site_url($controller_method.'/view/'.$value->id)?>"><i class="glyphicon glyphicon-eye-open"></i>&nbsp;  View</a></li>
										<?endif;?>
										
										<!-- CHECK THE OVERRIDE EDIT BUTTON -->
										<?if($edit_button && !is_string($edit_button)):?>
											<?php
												$edit_button = (object)$edit_button;
											?>
											<li><a href="<?=site_url($edit_button->controller.'/'.$edit_button->method.'/'.get_segments($value,$edit_button->field))?>"><i class="<?=isset($delete_button->icon_class)?$delete_button->icon_class:'glyphicon glyphicon-edit'?>"></i>&nbsp;  Edit</a></li>
										<?elseif($edit_button === false):?>
										<?else:?>
											<li><a href="<?=site_url($controller_method.'/edit/'.$value->id)?>"><i class="glyphicon glyphicon-edit"></i>&nbsp;  Edit</a></li>
										<?endif;?>

										<!-- CHECK THE OVERRIDE DELETE BUTTON -->
										<?if($delete_button && !is_string($delete_button)):?>
											<?php
												$delete_button = (object)$delete_button;
											?>
											<li><a class="confirm"  href="<?=site_url($delete_button->controller.'/'.$delete_button->method.'/'.get_segments($value,$delete_button->field))?>"><i class="<?=isset($delete_button->icon_class)?$delete_button->icon_class:'glyphicon glyphicon-remove'?> confirm"></i>&nbsp;  Delete</a></li>
										<?elseif($delete_button === false):?>
										<?else:?>
											<li><a class="confirm" href="<?=site_url($controller_method.'/delete/'.$value->id)?>"><i class="glyphicon glyphicon-remove confirm"></i>&nbsp;  Delete</a></li>
										<?endif;?>

										<!-- CHECK ADDITIONAL ACTION BUTTONS -->
										<?if($add_action_button):?>
											<?foreach ($add_action_button as $k_ab => $ab):?>
												<?php
													$ab = (object)$ab;
													$ab_field = "";
													if(is_string($ab->field)){
														$xxx = $ab->field;
														$ab_field = $value->$xxx;
													}elseif(is_array($ab->field) || is_object($ab->field)){
														$abf = array();
														foreach ($ab->field as $k_abf => $v_abf) {
															$abf[] = $value->$v_abf;
														}
														$ab_field = implode("/", $abf);
													}else{}

													$ab_controller = isset($ab->controller) ? $ab->controller : "No_Controller_Set";
													$ab_method = isset($ab->method) ? $ab->method : "No_Method_Set";
													$ab_caption = isset($ab->caption) ? $ab->caption : "No_Caption_Set";
													$ab_icon_class = isset($ab->icon_class) ? $ab->icon_class : "No_Icon_Class_Set";
												?>
												<li><a class="" href="<?=site_url($ab_controller.'/'.$ab_method.'/'.$ab_field)?>"><i class="<?=$ab_icon_class?>"></i>&nbsp;  <?=$ab_caption?></a></li>
											<?endforeach;?>
										<?endif;?>
									</ul>
								</div>
							</td>
						<?endif;?>
					</tr>

				<?endforeach;?>
			</table>
			<hr class="hr">
			<h4>Total Record/s  <span class="badge"><?=$total_rows?></span></h4>
		</div>
	</div>
</div>
<?else:?>
	<hr>
	<div class="alert alert-danger"><strong><p>No Record Found.</p></strong></div>
<?endif?>