<?$has_others = false;?>
<div class="row" >
	<div class="col-md-12">
		<?if($subject_master):?>
			<ul class="nav nav-pills nav-pills-xs" id="subTab">
			<?foreach ($subject_master as $key => $value):?>
				<?
					if($key == "others"){
						$has_others = true;
						continue;
					}
				?>
				<li><a class="btn btn-xs" href="#<?=$key?>" data-toggle="tab"><?=strtoupper($key)?></a></li>
					
			<?endforeach;?>
			<?if($has_others):?>
				<li><a class="btn btn-xs" href="#others" data-toggle="tab">Others</a></li>
			<?endif;?>
			</ul>
			<p></p>
			<!-- Tab panes -->
			<div class="tab-content">
				<?foreach ($subject_master as $key => $value):?>
					<div class="tab-pane" id="<?=$key?>">

						<div class="table-responsive">
							<table class="table table-condensed table-sortable table-bordered">
								<thead>
								  <tr class="bold" >
								    <th>Check</th>
								    <th>Course No.</th>
								    <th>Description</th>
								    <th>Unit</th>
								    <th>Lec</th>
								    <th>Lab</th>
								    <th>Time</th>
								    <th>Day</th>
								    <th>Room</th>
								    <th>Load</th>
								    <th>Teacher</th>
								  </tr>
							  </thead>
							  <tbody>
								  <?if($value):?>
											<?foreach($value as $subject):?>
												<tr>
													<td>
														<input type='checkbox' name='subject[]' value='<?=$subject->id;?>'/>
													</td>
													<td class="bold"><?=$subject->code;?></td>
													<td class="bold"><?=$subject->subject;?></td>
													<td><?=$subject->units;?></td>
													<td><?=$subject->lec;?></td>
													<td><?=$subject->lab;?></td>
													<td><?=_convert_to_12($subject->time,$subject->is_open_time);?></td>
													<td><?=$subject->day;?></td>
													<td><?=$subject->room;?></td>
													<td><?=$subject->subject_taken;?>/<?=$subject->original_load;?></td>
													<td><?=$subject->teacher;?></td>
												</tr>
											<?endforeach;?>
									<?else:?>
										<tr>
											<td colspan="11" >No Subjects Available</td>
										</tr>
									<?endif;?>
								</tbody>
							</table>
						</div>	

					</div>
			  	<?endforeach;?>
			</div>

		<?else:?>
			<div class="alert alert-danger"><strong>No subject</strong> available or all was already added.</div>
		<?endif?>
	</div>
</div>

<script>
  $(function () {
    $('#subTab a:first').tab('show');
  })
</script>

