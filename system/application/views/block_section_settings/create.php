<?echo form_open('');?>
	<p>
    <label for="year">Name</label>
	<?php 
		$data= array(
              'name'        => 'block_section_settings[name]',
              'value'       => isset($block_section_settings->name) ? $block_section_settings->name : '',
              'maxlength'   => '100',
              'required'    => true
            );

		echo form_input($data);
  ?>
  </p>

  <p>
    <?php
      $def_sem = isset($this->cos->enrollment)&&$this->cos->enrollment?$this->cos->enrollment->semester_id:$this->cos->user->semester_id;
    ?>
    <label for="semester_id">Semester</label>
    <?echo semester_dropdown('semester_id',isset($block_section_settings)?$block_section_settings->semester_id:$def_sem,"required");?>
  </p>
  


	<?php
		echo panel_head2('Select Subjects');
		@$this->load->view('block_section_settings/subject_list');
		echo panel_tail2();
	?>
	
	<div class="btn-group">
		<button type="submit" name="save_block" value="save_block" class="btn btn-sm btn-primary" >Save Block</button>
		<button type="submit" name="save_block_n_add_schedule" value="save_block_n_add_schedule" class="btn btn-sm btn-success" >Save Block & Add More Schedule</button>
	</div>

	<a class='btn btn-default btn-sm' href='<?=base_url()?>block_section_settings'>Cancel</a>
	
<?echo form_close();?>