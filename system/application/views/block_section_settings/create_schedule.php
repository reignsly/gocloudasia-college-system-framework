<? echo panel_head2('Bock Section Profile','','primary');?>
	<div class="row">
		<div class="col-lg-12">
			<div class="table-responsive">
				<table class="table table-bordered" >
					<tr>
						<td><h5 class='txt-facebook bold' ><i class="fa fa-tag"></i>&nbsp; Block Name : &nbsp; <?php echo $block_section_settings->name ?></h5></td>
						<td><h5 class='txt-facebook bold'><i class="fa fa-tag"></i>&nbsp; For Semester : &nbsp; <?php echo $block_section_settings->semester ?></h5></td>
					</tr>
				</table>
			</div>

			<h4>Schedule Created</h4>
			<div class="table-responsive">
				<table class="table table-condensed table-bordered table-sortable" >
				  <thead>
					  <tr class="gray" >
							<th>Course No.</th>
							<th>Description</th>
							<th>Unit</th>
							<th>Lec</th>
							<th>Lab</th>
							<th>Time</th>
							<th>Day</th>
							<th>Room</th>
							<th>Load</th>
							<th>Teacher</th>
							<th>Action</th>
					  </tr>
					 </thead>
				  <?php if(is_array($block_subjects)):?>
						<?foreach($block_subjects as $block_subject):?>
							<tr>
								<td class="bold" ><?=$block_subject->code;?></td>
								<td class="bold"><?=$block_subject->subject;?></td>
								<td><?=$block_subject->units;?></td>
								<td><?=$block_subject->lec;?></td>
								<td><?=$block_subject->lab;?></td>
								<td><?=_convert_to_12($block_subject->time);?></td>
								<td><?=$block_subject->day;?></td>
								<td><?=$block_subject->room;?></td>
								<td><?=$block_subject->subject_taken;?>/<?=$block_subject->original_load;?></td>
								<td><?=ucwords(strtolower($block_subject->teacher));?></td>
								<td>
									<a class="btn btn-xs btn-danger confirm" href='<?=base_url()?>block_section_settings/destroy_block_subject/<?=__link($block_section_settings->id);?>/destroy/<?=__link($block_subject->id."/create_schedule")?>'><span class="entypo-trash"></span>&nbsp;</a>
								</td>
							</tr>
						<?endforeach;?>
					<?else:?>
						<tr>
							<td colspan="11" >No schedule added yet.</td>
						</tr>
					<?endif;?>
				</table>
			</div>
		</div>
	</div>
<?php echo panel_tail2(); ?>

<?php echo form_open('','class="confirm" title="Please confirm that all information you have entered is correct and continue."') ?>
	<?php @$this->load->view('subjects/_form') ?>
	<button class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-save"></span> &nbsp; Create Schedule and Add to Block</button>
	<a href="<?=site_url('block_section_settings/edit/'.$hash)?>" class="btn btn-sm btn-default">Cancel</a>
<?php echo form_close(); ?>
