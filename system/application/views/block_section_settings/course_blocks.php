<script type='text/javascript' >
	$(function(){
		$('#course_id').on('change',function(){
      var xparams = {course_id : $(this).val()};
      var xurl = $(this).attr("url");
      var xcon = $('#div_major');
      $.ajax({
        type: "POST",
        url: xurl,
        data: xparams,
        dataType : 'html',
      }).done(function(output){
        xcon.html(output);
      }).fail(function(jqXHR, textStatus, errorThrown){
          xcon.html(xerror_msg);
      });
    })
	})
</script>
<?php
	$f_years = array();
	$f_courses[''] = "Select Course";
	$ctr = 0;
	if(($years)){
		$ctr++;
		foreach($years as $obj){
			$f_years[$obj->id] = $obj->year;
		}
	}
	if(($courses)){
		$ctr++;
		foreach($courses as $obj){
			$f_courses[$obj->id] = $obj->course_code.'-'.$obj->course;
		}
	}
?>
<div id="right">
	
	<div id="right_bottom">
	
	<?  
		echo validation_errors('<div class="alert alert-danger">', '</div>');
		
		echo form_open('');
		
		echo form_hidden('courses[block_system_setting_id]',$block_system_settings->id);
		
		echo form_hidden('courses[academic_year_id]',$block_system_settings->academic_year_id);
		
		echo form_hidden('courses[semester_id]',$block_system_settings->semester_id);
	?>
	
	<br/>
	
	<p>
		<?if($years):?>
		<label for="year">Year</label><br />
			<?php echo form_dropdown('courses[year_id]', $f_years,'','required'); ?>
		<?else:?>
		<div class='alert alert-danger'>No years available.</div>
		<?endif;?>
	</p>
	
	<p>
		<?if($courses):?>
		<label for="year">Course</label><br />
			<?php echo form_dropdown('courses[course_id]', $f_courses,'',' id="course_id" required  url="'.site_url('ajax/get_major').'"'); ?>
		<?else:?>
		<div class='alert alert-danger'>No courses available</div>
		<?endif;?>
	</p>
	<p>
		<label for="">Major</label><br />
		<div id="div_major"><i>Select Course first.</i></div>
	</p>		
		<?
		if($ctr == 2){
				echo form_submit('','Save Changes');
			}
		?>
		<a class='btn btn-default btn-sm' href="<?=base_url('block_section_settings/edit/'.$hash)?>" >Cancel</a>
		<a class='btn btn-default btn-sm' href='<?=base_url()?>block_section_settings'><span class='glyphicon glyphicon-backward'></span>&nbsp;  Back to List of Block Settings</a>
		<?
		echo form_close();
	?>
		
	</div>
	<div class="clear"></div>

</div>
	
	<div class="clear"></div>
</div>