<div class="table-responsive">
	<table class="table table-bordered">
		<tr class="info bold">
			<td>Block Name</td>
			<td>Semester</td>
		</tr>
		<tr>
			<td><span class="underline bold"><?php echo $block_section_settings->name ?></span></td>
			<td><span class="underline bold"><?php echo $block_section_settings->semester ?></span></td>
		</tr>
	</table>
</div>

<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="#sub_sched" data-toggle="tab">Subject Schedule</a></li>
  <li><a href="#ass_cour" data-toggle="tab">Assigned Course</a></li>
  <li><a href="#update_block" data-toggle="tab">Update Block</a></li>
</ul>
<!-- Tab panes -->
<div class="tab-content">

  <div class="tab-pane active" id="sub_sched">
  	<p></p>
  	<a href="<?php echo site_url('block_section_settings/create_schedule/'.$hash); ?>" class="btn btn-sm btn-primary confirm" ><span class="glyphicon glyphicon-plus" ></span>&nbsp; Create Schedule</a>
  	<a class='btn btn-default btn-sm' href='<?=base_url()?>block_section_settings'><span class='glyphicon glyphicon-backward'></span>&nbsp;  Cancel</a>
		<p></p>
		<div class="table-responsive">
			<table class="table table-condensed table-bordered table-sortable" >
			  <thead>
				  <tr class="gray" >
						<th>Course No.</th>
						<th>Description</th>
						<th>Unit</th>
						<th>Lec</th>
						<th>Lab</th>
						<th>Time</th>
						<th>Day</th>
						<th>Room</th>
						<th>Load</th>
						<th>Teacher</th>
						<th>Action</th>
				  </tr>
			  </thead>
			  <?php
				if(is_array($block_subjects)){
				
				foreach($block_subjects as $block_subject){?>
					<tr>
						<td class="bold" ><?=$block_subject->code;?></td>
						<td class="bold"><?=$block_subject->subject;?></td>
						<td><?=$block_subject->units;?></td>
						<td><?=$block_subject->lec;?></td>
						<td><?=$block_subject->lab;?></td>
						<td><?=_convert_to_12($block_subject->time,$block_subject->is_open_time);?></td>
						<td><?=$block_subject->day;?></td>
						<td><?=$block_subject->room;?></td>
						<td><?=$block_subject->subject_taken;?>/<?=$block_subject->original_load;?></td>
						<td><?=ucwords(strtolower($block_subject->teacher));?></td>
						<td>
							<a class="btn btn-xs btn-danger confirm" href='<?=base_url()?>block_section_settings/destroy_block_subject/<?=__link($block_section_settings->id);?>/destroy/<?=__link($block_subject->id)?>'><span class="entypo-trash"></span>&nbsp;</a>
						</td>
					</tr>
				<?php
				}
				}
			  ?>
			
			</table>
		</div>
  </div>

  <div class="tab-pane" id="ass_cour">
  	<p></p>
  	<a href="<?php echo site_url('block_section_settings/course_blocks/'.$hash); ?>" class="btn btn-sm btn-primary" ><span class="glyphicon glyphicon-plus" ></span>&nbsp;Add Course</span></a>
  	<a class='btn btn-default btn-sm' href='<?=base_url()?>block_section_settings'><span class='glyphicon glyphicon-backward'></span>&nbsp;  Cancel</a>
		<p></p>
		<div class="table-responsive">
			<table class="table table-bordered table-sortable">
				<thead>
			  <tr class="gray" >
					<th>Year</th>
					<th>Code</th>
					<th>Course</th>
					<th>Major</th>
					<th>Action</th>
			  </tr>
			  </thead>	
				  <?php if(($course_blocks)):?>
						<?php foreach($course_blocks as $course_block):?>
							<tr>
								<td class="bold"><?=$course_block->year;?></td>
								<td class="bold"><?=$course_block->course_code;?></td>
								<td class='bold' ><?=$course_block->course;?></td>
								<td class="bold"><?=$course_block->major;?></td>
								<td>
									<a class="btn btn-xs btn-danger confirm" href='<?=base_url()?>block_section_settings/destroy_course_blocks/<?=__link($block_section_settings->id);?>/destroy/<?=__link($course_block->id)?>'><span class="entypo-trash"></span></a>
								</td>
							</tr>
						<?php endforeach; ?>
					<?php endif;?>
			</table>
		</div>	
	</div>

	<div class="tab-pane" id="update_block">
		<p></p>
		<?echo form_open('');?>
			<?@$this->load->view('block_section_settings/_form')?>
			<p></p>
			<div class="page-header">
			  <h4>Select subject to add <small>Choose below (Duplicate schedule will not be added)</small></h4>
			</div>

			<?@$this->load->view('block_section_settings/subject_list');?>	
				
			<?echo form_hidden('id', $block_section_settings->id);?>
			<?echo form_submit('','Save Changes');?>
			<a class='btn btn-default btn-sm' href='<?=base_url()?>block_section_settings'><span class='glyphicon glyphicon-backward'></span>&nbsp;  Cancel</a>
		<?echo form_close();?>
	</div>

</div>