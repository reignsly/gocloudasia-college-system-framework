<div id="right">
	<?php 
		$formAttrib = array('id' => 'search_enrollee', 'class' => 'form-inline', 'method' => 'GET');
		echo form_open('', $formAttrib);
	?>
    
	<div class="form-group">
		Date : &nbsp;
	</div>
	
	<div class="form-group">
		<?php 
		$data = array(
              'name'        => 'date',
			  'class'		=> 'date_pick',
              'value'       => isset($created_at) ? $created_at : date('Y-m-d'),
              'maxlength'   => '15',
              'size'        => '50',
			  'style'		=> 'width:100px;',
            );
		?>
		<?= form_input($data);  ?> 
	</div>
   
  <div class="form-group">
    <input id="name" class="form-control" type="text" value="<?=isset($name)?$name:''?>" name="name" placeholder="Name">
  </div>
   
  <div class="form-group">
    <input id="studid" class="form-control" type="text" value="<?=isset($studid)?$studid:''?>" name="studid" placeholder="Student ID">
  </div>
  <div class="form-group">
    <?=year_dropdown('year_id',isset($year_id)?$year_id:'',"", '')?>
  </div>
   
	<?php
	echo form_hidden('semester_id', $this->open_semester->id);
	echo form_hidden('sy_from', $this->open_semester->year_from);
	echo form_hidden('sy_to', $this->open_semester->year_to);
	echo form_hidden('is_paid', 1);
	?>
   		<?php echo form_submit('submit', 'Search'); ?>
		<?echo form_close();?>
<br>
<?echo isset($links) ? $links : NULL;?>
	<?php
	if(isset($search)){
	?>
	<br />
	<table>
		<tr>	
		  <th>Student ID</th>
		  <th>Name</th>
		  <th>Year</th>
		  <th>Course</th>
		  <th>Action</th>
		</tr>
		<tbody>
		<?php
		if(!empty($search))
		{
			foreach($search as $student):
			$l = _se($student->id);
			?>
			<tr>
			<td><?php echo $student->studid; ?></td>
			<td><?php echo ucfirst($student->name); ?></td>
			<td><?php echo $student->year; ?></td>
			<td><?php echo $student->course; ?></td>
			<td>
				<div class="btn-group btn-group-sm">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-caret-square-o-down"></i> <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
					
					<?
						$mctr=0;	
					?>
					
					<?if($mydepartment->stud_profile == 1):?>
					<li><a href="<?=base_url('profile/view/'.$student->id)?>"><i class="fa fa-file-text"></i>&nbsp;  Profile</a></li>
					<?$mctr++;?>
					<?endif;?>
					
					<?if($mydepartment->stud_grade == 1):?>
					<li><a href="<?=base_url('grade/view/'.$student->id.'/'.$student->user_id)?>"><i class="fa fa-th"></i>&nbsp;  Grades</a></li>
					<?$mctr++;?>
					<?endif;?>
					
					<?if($mydepartment->stud_fees == 1):?>
					<li><a href="<?=base_url('fees/view_fees/'.$student->id)?>"><i class="fa fa-list-alt"></i>&nbsp;  Fees</a></li>
					<?$mctr++;?>
					<?endif;?>
					
					<?if($mydepartment->stud_otr == 1):?>
					<li><a href="<?=base_url('transcript/view/'.$student->id.'/'.$student->user_id)?>"><i class="fa fa-credit-card"></i> &nbsp;  Transcript</a></li>
					<?$mctr++;?>
					<?endif;?>
					
					<?if($mydepartment->stud_issues == 1):?>
					<li><a href="<?=base_url('issues/view/'.$student->id.'/'.$student->user_id)?>"><i class="fa fa-thumbs-down"></i>&nbsp;  Issues</a></li>
					<?$mctr++;?>
					<?endif;?>
					
					<?if($mctr == 0):?>
					<li><a href="#"><i class="fa fa-times-circle"></i>&nbsp;  No action available.</a></li>
					<?endif;?>
					
				 </ul>
				</div>
				
			</td>
			</tr>
			<?php 
			endforeach; ?>
			<tr>
				<td colspan='4' align='right'><b>Total Records</b></td>
				<td><span class='badge'><?=($total_rows) ? $total_rows : 0;?></span></td>
			</tr>
			<?php
		}
		else
		{
		?>
		<tr>
		<td colspan="5">
			Student Not Found
		</td>
		</tr>
		<?php
		}
		?>
		</tbody>
		<tr>
		  <th>Student ID</th>
		  <th>Name</th>
		  <th>Year</th>
		  <th>Course</th>
		  <th>Action</th>
		</tr>
	</table>
	<?echo isset($links) ? $links : NULL;?>
	<?php
	}
	?>
	</div>
	
	<div class="clear"></div>
</div>