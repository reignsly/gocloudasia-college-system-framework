<!-- <div class="btn-group btn-group-sm">
  <a class='btn btn-success btn-sm' href="<?=site_url();?>subjects/create" ><i class="fa fa-plus"></i>&nbsp; New Subject</a>
  <a class='btn btn-default btn-sm' href="<?=site_url();?>subjects/print_subjects/<?=$get_url;?>" class="confirm" title="Download subject in excel form ?"><i class="fa fa-download"></i>&nbsp; Download All Subjects in Excel</a>
  <a class='btn btn-default btn-sm' href="<?=site_url();?>subjects/print_all_in_pdf/<?=$get_url;?>" ><i class="fa fa-print"></i>&nbsp; View All Subjects (PDF)</a>
</div>

<br/>
<br/> -->

<form action="<?=site_url('master_subjects');?>/index/0" method="GET" >
	
	<div class="row">

		<div class="col-md-2">
			<input class='form-control' id="search_code_eq" name="code" size="30" type="text"  value="<?=isset($_GET['code'])?$_GET['code']:''?>" placeHolder="Course No." />
		</div>

		<div class="col-md-2">
			<input class='form-control' id="search_subject_eq" name="subject" size="30" type="text" value="<?=isset($_GET['subject'])?$_GET['subject']:''?>" placeHolder="Description" />
		</div>

		<div class="col-md-5">
			<input id="search_submit" name="search_subjects" type="submit" value="Search" class="btn btn-sm btn-success" />
			<a class='btn btn-primary btn-sm' href="<?=site_url();?>master_subjects/create" ><i class="fa fa-plus"></i>&nbsp; New Subject</a>
			<a class='btn btn-primary btn-sm' href="<?=site_url();?>master_subjects/upload_csv" ><i class="fa fa-upload"></i>&nbsp; Upload by CSV</a>
		</div>
	</div>

</form>

<?php if(!empty($results)):?>
  <?= $links ;?>	
<?php endif;?>
<br/>
<div class="table-responsive">
<table class="table-sortable table"> 
  <thead>
	  <tr>
	    <th>Course No.</th>
	    <th>Description</th>
	    <th>Unit</th>
	    <th>Lec</th>
	    <th>Lab</th>
	    <th>Year Created</th>
	    <th>Action</th>
	  </tr>
  </thead>
  <tbody>
		<?php if(!empty($results)):?>
			<?php foreach($results as $s):?>
				<tr>
					  <td><strong><?php echo $s->code;?></strong></td>
					  <td><strong><?php echo $s->subject;?></strong></td>
					  <td><?php echo $s->units;?></td>
					  <td><?php echo $s->lec;?></td>
					  <td><?php echo $s->lab;?></td>
					  <td><?php echo $s->year_from.' - '.$s->year_to;?></td>
					  <td>
					  	<div class="btn-group btn-group-xs">
							  <a href="<?php echo site_url('master_subjects/edit/'.__link($s->id)) ?> " class="btn btn-xs btn-primary" title="Edit" ><span class="glyphicon glyphicon-edit" ></span></a>
					  		<a href="<?php echo site_url('master_subjects/destroy/'.__link($s->id)) ?> " class="confirm btn btn-danger" title="Are you sure to delete  <?php echo $s->subject ?> ?" ><span class="glyphicon glyphicon-remove" ></span></a>
							</div>
					  </td>
					</td>
				</tr>
			<?php endforeach;?>
		<?php endif;?>
	</tbody>
</table>	
<?php if(!empty($results)):?>
  <?= $links ;?>	
<?php endif;?>
</div>