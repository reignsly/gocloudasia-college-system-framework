
 
  <p>
    <label for="year">Grading Period</label><br />
	<?php 
		$data= array(
              'name'        => 'grading_periods[grading_period]',
              'value'       => isset($grading_periods->grading_period) ? $grading_periods->grading_period : '',
              'maxlength'   => '100',
              'size'        => '50',
              'required'        => 'true',
              'style'       => 'width:200px',
            );

		echo form_input($data);
	?>
  </p>
	<p>
		<label for="year">Order</label><br />
		<?
		$data= array(
              'name'        => 'grading_periods[orders]',
              'value'       => isset($grading_periods->orders) ? $grading_periods->orders : '',
              'maxlength'   => '2',
              'size'        => '50',
              'required'        => 'true',
              'style'       => 'width:200px',
			  'class'		=> 'numeric form-control',
			  'type'		=> 'number'
            );

		echo form_input($data);
		?>
	</p>
  <div class="clear"></div>

