<p></p>
<div class="row">
	<?=form_open('','onsubmit="return validate_addmenu()"')?>
	<div class="table-responsive">
		<table class="table">
			<tr>
				<td>Select Department</td>
				<td>
					  <div class="btn-group">
						    <button class="btn btn-default"><?=$selected_department ? $selected_department->description : 'Choose'?></button>
						    <button class="btn dropdown-toggle" data-toggle="dropdown">
						    <span class="caret"></span>
						    </button>
						    <ul class="dropdown-menu">
						    	<?if($departments_dd):?>
						    		<?foreach ($departments_dd as $k => $v):?>
						    			<li><a href="<?=site_url("packages/profile/add_menu/$p_id/$department_id/$k")?>"><?=$v;?></a></li>
						    		<?endforeach;?>
						    	<?endif;?>
						    </ul>
						</div>
						<input type="hidden" name="department_id" value="<?=$selected_department ? $selected_department->id : '0'?>">
				</td>
			</tr>
			<?if($selected_department):?>
			<tr>
				<td><input onchange="radioChange(this)" type="radio" name="menu_group_type" checked value="selection">&nbsp; Select Menu Group</td>
				<td><?=form_dropdown('menugroup_id', $menugroup_dd,'','id="select_menugroup"');?></td>
			</tr>
			<tr>
				<td><input onchange="radioChange(this)" type="radio" name="menu_group_type" value="create_new">&nbsp; Create New Menu Group</td>
				<td><input type="text" name="menu_group" id="menu_group" disabled></td>
			</tr>
			<tr>
				<td>Select From the following Menu List :</td>
				<td>
					<p><button type="submit" name="add_menu" value="add_menu" class="btn btn-sm btn-dropbox"><span class="entypo-install"></span>&nbsp; Save Menu</button></p>
					<hr>
					<?if(isset($menu_list)&&$menu_list): $l = 1;?>
						<ul id="ul_sel_menu" style=" list-style-type: none;">
							<?foreach($menu_list as $k => $ml):?>
								<li><?=$l++?>. &nbsp;
									<input type="checkbox" name="menu_list[]" id="" value="<?=$ml->id?>" > &nbsp;
									<span class="badge" ><?=uc_words($ml->caption);?></span>  
									<i><?=($ml->remarks) ? '<i class="fa fa-hand-o-right"></i>&nbsp;'.$ml->remarks : '' ;?> </i></li>
							<?endforeach;?>
						</ul>
					<?else:?>
						<p>No available menu.</p>
					<?endif;?>
					<hr>
					<p><p><button type="submit" name="add_menu" value="add_menu" class="btn btn-sm btn-dropbox"><span class="entypo-install"></span>&nbsp; Save Menu</button></p></p>
				</td>
			</tr>
			<?endif?>
		</table>
	</div>
	<?=form_close();?>
</div>

<script type="text/javascript">
	function validate_addmenu() {
		if($('#ul_sel_menu li :input[type="checkbox"]:checked').length <= 0){
        custom_modal('Error','No Menu Selected.');
        return false;
    }else{
        return true;            
    }
	}
  function radioChange(data)
  {
		if($(data).val() == 'selection')
		{
			$('#menu_group').attr('disabled', true);
			$('#select_menugroup').attr('disabled', false);
		}
		else
		{
			$('#menu_group').attr('disabled', false);
			$('#select_menugroup').attr('disabled', true);
		}
  }
</script>