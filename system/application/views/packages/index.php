<div class="row">
    <a href="<?=site_url('packages/create')?>" class="btn btn-sm btn-dropbox"><span class="entypo entypo-plus-circled"></span>&nbsp; Create  New Package</a>  
    <a href="<?=site_url('packages/clone_package')?>" class="btn btn-sm btn-github"><span class="entypo-google-circles"></span>&nbsp; Clone Package</a>  

    <!-- Split button -->
    <div class="btn-group">
      <button type="button" class="btn btn-default"><span class="entypo-print"></span> Print (PDF)</button>
      <button type="button" class="btn btn-dropbox dropdown-toggle" data-toggle="dropdown">
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
      </button>
      <ul class="dropdown-menu" role="menu">
        <li><a target="_blank" href="<?=site_url('packages/print_summary')?>">Summary</a></li>
        <li><a target="_blank" href="<?=site_url('packages/print_detailed')?>">Detailed</a></li>
      </ul>
    </div>
</div>
<p></p>
<div class="row">
    <div class="table-responsive">
        <?if(isset($packages) && $packages):?>
        <table class="table table-condensed">
            <tr>
                <th>Package Name</th>
                <th>Description</th>
                <th>Level</th>
            </tr>
            <?foreach ($packages as $k => $p):?>
                <tr style="background: <?=$p->color_code?$p->color_code:''?>" >
                    <td class="bold" ><?=$p->package?></td>
                    <td class="italic" ><?=$p->remarks?></td>
                    <td class="" ><?=$p->level?></td>
                    <td>
                        <div class="btn-group">
                            <a href="<?=site_url('packages/profile/departments/'.$p->id)?>" class="btn btn-sm btn-default tp" data-toggle="tooltip" data-placement="top" title="Package Departments"><span class="entypo-users"></span></i></a>
                            <a href="<?=site_url('packages/profile/edit/'.$p->id)?>" class="btn btn-sm btn-default tp" data-toggle="tooltip" data-placement="top" title="Edit Package Profile"><span class="iconicfill-pen"></span></a>
                            <a target="_blank" href="<?=site_url('packages/print_detailed/'.$p->id)?>" class="btn btn-sm btn-default tp" data-toggle="tooltip" data-placement="top" title="Print Package"><span class="entypo-print"></span></a>
                            <a href="<?=site_url('packages/destroy/'.$p->id)?>" class="btn btn-sm btn-danger confirm" title="Are you sure you want to remove this package and all its content?"><span class="entypo-trash"></span></a>
                        </div>
                    </td>
                </tr>
            <?endforeach;?>
        </table>
        <?else:?>
            <div class="alert alert-info">No available package.</div>
        <?endif;?>
    </div>
</div>