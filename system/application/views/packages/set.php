<style>
    .toggle-header{
        padding:20px 0;
        margin:20px 0;
        background-color:black;
        color:white;
    }
    .txt-green{
        color:green;
    }
    .txt-red{
        color:red;
    }

    .bronze{
        background: #474111;
    }
</style>
<div class="table-responsice">
    <table class="table">
        <tr>
            <th class="bold">Departments</th>
             <?if($packages):?>
                <?foreach ($packages as $k => $p):?>
                    <td style="background: <?=$p->color_code?$p->color_code:''?>" class="text-center bold" ><span class="badge tp" data-toggle="tooltip" data-placement="top" title="<?=$p->remarks?>"><?=uc_words($p->package);?></span></td>
                <?endforeach;?>
            <?else:?>
                <td>No Package Available</td>
            <?endif;?>
        </tr>
        <?if($departments):?>
            <?foreach ($departments as $k => $d):?>   
                <tr>
                    <td class="bold" ><?=uc_words($d->department);?></td>
                    <?if($packages):?>
                        <?foreach ($packages as $k => $p):?>
                            
                            <?php
                                $xicon = $is_dep_set($p->id, $d->id) ? "glyphicon glyphicon-ok" : "glyphicon glyphicon-remove";
                                $xcolor = $is_dep_set($p->id, $d->id) ? "txt-green" : "txt-red";
                            ?>
                            <td class="text-center" style="background: <?=$p->color_code?$p->color_code:''?>">
                                <i class="<?=$xicon?> <?=$xcolor?>"></i>
                            </td>
                        
                        <?endforeach;?>
                    <?endif;?>
                </tr>
            <?endforeach;?>
        <?endif;?>
        
        <?if($packages):?>
            <tr>
                <td></td>
                <?foreach ($packages as $k => $p):?>
                    <td style="background: <?=$p->color_code?$p->color_code:''?>" class="text-center bold txt-green" >
                        <?if($p->is_set == 1):?>
                            <span class="glyphicon glyphicon-check"></span>&nbsp; Current Package <br>
                            <a href="<?=site_url('packages/change_package/'.$p->id)?>" class="btn btn-success btn-sm confirm" title="Are you sure to reset this package?" >Re-set</a>
                        <?else:?>
                            <a href="<?=site_url('packages/change_package/'.$p->id)?>" class="btn btn-success btn-sm confirm" title="Are you sure ta change package to <?=uc_words($p->package)?>?" >Set Plan</a>
                        <?endif;?>
                    </td>
                <?endforeach;?>
            </tr>
        <?endif;?>
    </table>
</div>