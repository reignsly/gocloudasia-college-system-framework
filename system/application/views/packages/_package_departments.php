<p></p>
<div class="row">
    <div class="table-responsive">
        <?if(isset($departments) && $departments):?>
        <table class="table table-condensed">
            <tr>
                <th>Department</th>
                <th>Description</th>
                <th>Student Profile Access</th>
                <th></th>
            </tr>
            <?foreach ($departments as $k => $p):?>
                <?
                    $stud_profile = (isset($p->stud_profile) && $p->stud_profile == 1) ? 'checked' : '';
                    $stud_grade = (isset($p->stud_grade) && $p->stud_grade == 1) ? 'checked' : '';
                    $stud_fees = (isset($p->stud_fees) && $p->stud_fees == 1) ? 'checked' : '';
                    $stud_otr = (isset($p->stud_otr) && $p->stud_otr == 1) ? 'checked' : '';
                    $stud_issues = (isset($p->stud_issues) && $p->stud_issues == 1) ? 'checked' : '';

                    $stud_edit_profile = (isset($p->stud_edit_profile) && $p->stud_edit_profile == 1) ? 'checked' : '';
                    $stud_edit_grade = (isset($p->stud_edit_grade) && $p->stud_edit_grade == 1) ? 'checked' : '';
                    $stud_add_fees = (isset($p->stud_add_fees) && $p->stud_add_fees == 1) ? 'checked' : '';
                    $stud_add_issues = (isset($p->stud_add_issues) && $p->stud_add_issues == 1) ? 'checked' : '';
                    $stud_edit_subject = (isset($p->stud_edit_subject) && $p->stud_edit_subject == 1) ? 'checked' : '';

                    $stud_portal_activation = (isset($p->stud_portal_activation) && $p->stud_portal_activation == 1) ? 'checked' : '';

                    $spa = $p;
                    $options = array(
                        'stud_profile' => array('value' => (isset($spa->stud_profile) && $spa->stud_profile == 1) ? 'checked' : '', 'caption' => 'View Profile' ),
                        'stud_edit_profile' => array('value' => (isset($spa->stud_edit_profile) && $spa->stud_edit_profile == 1) ? 'checked' : '', 'caption' => 'Update Profile' ),
                        'stud_subject' => array('value' => (isset($spa->stud_subject) && $spa->stud_subject == 1) ? 'checked' : '', 'caption' => 'View Subjects' ),
                        'stud_edit_subject' => array('value' => (isset($spa->stud_edit_subject) && $spa->stud_edit_subject == 1) ? 'checked' : '', 'caption' => 'Add / Delete Subject' ),
                        'stud_grade' => array('value' => (isset($spa->stud_grade) && $spa->stud_grade == 1) ? 'checked' : '', 'caption' => 'View Grade' ),
                        'stud_edit_grade' => array('value' => (isset($spa->stud_edit_grade) && $spa->stud_edit_grade == 1) ? 'checked' : '', 'caption' => 'Edit Grade' ),
                        'stud_fees' => array('value' => (isset($spa->stud_fees) && $spa->stud_fees == 1) ? 'checked' : '', 'caption' => 'View Fees' ),
                        'stud_add_fees' => array('value' => (isset($spa->stud_add_fees) && $spa->stud_add_fees == 1) ? 'checked' : '', 'caption' => 'Add Payment' ),
                        'stud_otr' => array('value' => (isset($spa->stud_otr) && $spa->stud_otr == 1) ? 'checked' : '', 'caption' => 'View OTR' ),
                        'stud_issues' => array('value' => (isset($spa->stud_issues) && $spa->stud_issues == 1) ? 'checked' : '', 'caption' => 'View Issues' ),
                        'stud_add_issues' => array('value' => (isset($spa->stud_add_issues) && $spa->stud_add_issues == 1) ? 'checked' : '', 'caption' => 'Add / Delete Issues' ),
                        'stud_portal_activation' => array('value' => (isset($spa->stud_portal_activation) && $spa->stud_portal_activation == 1) ? 'checked' : '', 'caption' => 'Student Portal Activation' ),
                    );
                    
                ?>
                <tr>
                    <td class="bold"><?=ucwords($p->department)?></td>
                    <td class="italic" ><?=ucwords($p->description)?></td>
                    <td>
                        <?if($p->department !== "student"):?>
                            <?=form_open('');?>
                                
                                <?=panel_head2('Update')?>

                                <ul class="list-group">
                                    <?foreach ($options as $k => $v):?>
                                        <li class="list-group-item">
                                            <span style="color:<?=$v['value']==="checked" ? "" : "red"; ?>" class="glyphicon glyphicon-<?=$v['value']==="checked" ? "ok" : "remove"; ?>"></span> &nbsp;
                                            <?=$v['caption']?>
                                            <span class="badge pull-right"><input type="checkbox" id="<?=$k?>" name="<?=$k?>" <?=$v['value']?> ></span>
                                        </li>
                                    <?endforeach;?>
                                </ul>
                                
                                <input type="hidden" name="pd_dep_id" value="<?=$p->id?>">
                                <button type="submit" name="update_stud_access" value="update_stud_access" class="btn btn-xs btn-dropbox pull-right"><span class="entypo-floppy"></span>&nbsp; Save</button>
                                <?=panel_tail2();?>
                            <?=form_close();?>
                        <?endif;?>
                    </td>
                    <td>
                        <div class="btn-group">
                            <a href="<?=site_url("packages/destroy_package_department/$package->id/".$p->id)?>" class="btn btn-sm btn-danger confirm" title="Are you sure to remove this department? All menus associated with it wil also be removed?"><span class="entypo-trash"></span>&nbsp; Remove</a>
                        </div>
                    </td>
                </tr>
            <?endforeach;?>
        </table>
        <?else:?>
            <div class="alert alert-info">No department added. Add below.</div>
        <?endif;?>
    </div>
</div>

<!-- Add Department Form -->
<div class="row" id="id_select_dep">
    <h4>Add Department</h4>
    <hr>
    <?if(isset($departments_list) && $departments_list):?>
        <?=form_open('','onsubmit="return Validate();"')?>
        <ul style=" list-style-type: none;" >
            <?foreach ($departments_list as $k => $d):?>
                <li class="bold" >
                    <input type="checkbox" name="add_dep[]" id="add_dep" value="<?=$d->id?>">
                    <?=ucwords($d->description)?>
                </li>
            <?endforeach;?>
        
            <li>
                <button type="submit" class="btn btn-sm btn-dropbox" name="add_department" value="add_department" ><span class="entypo-install"></span>&nbsp; Add Selected Department</button>
                <button type="button" class="btn btn-xs btn-default tp" onclick="checkall()" data-toggle="tooltip" data-placement="top" title="Check All"><i class="fa fa-check-square-o"></i>&nbsp;</button>
                <button type="button" class="btn btn-xs btn-default tp" onclick="uncheckall()" data-toggle="tooltip" data-placement="top" title="Uncheck All"><i class="fa fa-square-o"></i>&nbsp;</button>
            </li>
        </ul>
        <?=form_close();?>
    <?else:?>
        <div class="alert alert-info"><p>No available department to be added.</p></div>
    <?endif;?>
</div>

<script type="text/javascript">
    function Validate(){

        if($('#id_select_dep li :input[type="checkbox"]:checked').length <= 0){
            custom_modal('Error','No Department Selected.');
            return false;
        }else{
            return true;            
        }
    }

    function checkall () {
        $('#id_select_dep li :input[type="checkbox"]').attr('checked', true);
    }

    function uncheckall () {
        $('#id_select_dep li :input[type="checkbox"]').removeAttr("checked");
    }
</script>
