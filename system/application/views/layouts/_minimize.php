<?
	$show = isset($body) ? $body.'_hide' : 'walakangnilagaynaparameter1';
	$hide = isset($body) ? $body.'_show' : 'walakangnilagaynaparameter2';
	$body = isset($body) ? $body : 'walakangnilagaynabodyparameter';
	
?>
<div  style="float:right">
	<span class="glyphicon glyphicon-eye-close xtooltip <?=$hide?>" data-toggle="tooltip" data-placement="top" title="Hide Panel" ></span>&nbsp;
	<span class="glyphicon glyphicon-eye-open xtooltip <?=$show?>" data-toggle="tooltip" data-placement="top" title="Show Panel" ></span>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.xtooltip').tooltip();
		$('.xtooltip').css('cursor','pointer');

		$('.<?=$hide?>').click(function(){
			$('.<?=$body?>').hide('slow');
		})

		$('.<?=$show?>').click(function(){
			$('.<?=$body?>').show('slow');
		})
	})
</script>