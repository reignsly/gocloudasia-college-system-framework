<?if($this->syspar->is_menu_accordion == 1):?>
	<!-- ACCORDION TYPE OF MENU -->
	<div id="DepMainMenu">
	  <div class="list-group panel">
	    <a href="#depmenu_1" class="list-group-item active-menus strong" data-toggle="collapse" data-parent="#DepMainMenu"><i class="fa fa-gear"></i>&nbsp; Account<i class="fa fa-caret-down float-right"></i></a>
	    <div class="collapse" id="depmenu_1">
	    	<a href="<?=site_url()?>home"  class="list-group-item"><i class="fa fa-home"></i>&nbsp; Home</a>
			<a href="<?=site_url()?>messages/inbox"  class="list-group-item"><i class="fa fa-inbox"></i>&nbsp; Messages
			<span class='badge' id='menu_unread_badge' ></span>
			</a>
			<a href="<?=site_url()?>change_password/index/<?php echo $this->session->userdata('userid'); ?>"  class="list-group-item"><i class="fa fa-unlock-alt"></i>&nbsp;Change Password</a>
			<a href="<?php echo site_url(); ?>news_and_events"  class="list-group-item"> Bulletin Board</a>
			<a href="<?php echo site_url(); ?>auth/logout"  class="list-group-item"><i class="fa fa-power-off"></i>&nbsp; Logout</a>
	    </div>
	  </div>
	</div>
<?else:?>
	<!-- COLLAPSIBLE TYPE OF MENU -->
	<div id="DepMainMenu">
	  <div class="list-group panel">
	    <a href="javascript:;" onclick="collapsemenu(this)" menu_key = "sly1" colap = 0 class="list-group-item active-menus strong" data-parent="#DepMainMenu"><i class="fa fa-gear"></i>&nbsp; Account<i class="fa fa-caret-down float-right"></i></a>
	    <div class="" id="menu_sly1">
	    	<a href="<?=site_url()?>home"  class="list-group-item"><i class="fa fa-home"></i>&nbsp; Home</a>
			<a href="<?=site_url()?>messages/inbox"  class="list-group-item"><i class="fa fa-inbox"></i>&nbsp; Messages
			<span class='badge' id='menu_unread_badge' ></span>
			</a>
			<a href="<?=site_url()?>change_password/index/<?php echo $this->session->userdata('userid'); ?>"  class="list-group-item"><i class="fa fa-unlock-alt"></i>&nbsp;Change Password</a>
			<a href="<?php echo site_url(); ?>news_and_events"  class="list-group-item"> Bulletin Board</a>
			<a href="<?php echo site_url(); ?>auth/logout"  class="list-group-item"><i class="fa fa-power-off"></i>&nbsp; Logout</a>
	    </div>
	  </div>
	</div>
<?endif;?>