<script>
	$(function() {
		var xinput = $( "#student_search_data" );
		var xurl = xinput.attr('url');
		xurl = xurl+"?type=<?=$type?>";
		xinput.autocomplete({
				minLength: 2,
				source: xurl,
				search: function( event, ui ) {
					$('#search_go').attr('disabled', true);
					$('#search_enrollment_id').val('');
				},
				focus: function( event, ui ) {
					if(ui.item.id != "--"){
						xinput.val( ui.item.value );
						$('#search_go').attr('disabled', true);
					}
					return false;
				},
				select: function( event, ui ) {
						if(ui.item.id != "--"){
							//Create a link in search_go link button and enabled
							$('#search_go').attr('disabled', false);
							$('#search_enrollment_id').val(ui.item.id);
						}
						return false;
					}
				})
				.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
					return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a><strong>" + item.studid + " - " + item.value + "</strong><br/><i>" + item.course + " - " + item.year + "</i></a>" )
            .appendTo( ul );
				};
	});
</script>
<?=form_open('fees/redirect_search')?>
<div class="well">
	<div class="row">
		<div class="col-md-10 col-xs-8 ">
			<input type="hidden" name="search_current_url" id="search_current_url" value="<?=$this->router->uri->uri_string;?>">
			<input type="hidden" name="search_enrollment_id" id="search_enrollment_id">
			<input type="text" name="student_search_data" id="student_search_data" autocomplete="off" placeholder="Search Student :  Type Student ID / Last Name (2 chars)" class="searchInput" url="<?=site_url('ajax/finance_search_student');?>" autofocus>
		</div>
		<div class="col-md-2 col-xs-2 "><button type="submit" name="search_student" value="search_student" id="search_go" disabled class="btn btn-dropbox btn-sm"><i class="fa fa-search"></i>&nbsp; Go</button></div>
	</div>
</div>
<?=form_close();?>