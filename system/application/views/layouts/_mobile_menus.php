<div class="navbar navbar-default">
	<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="#"><?=capital_fwords($this->session->userdata['username']);?></a>
  </div>
  <div class="navbar-collapse collapse navbar-responsive-collapse">
    <ul class="nav navbar-nav">

      <?if($user_menus):?>
		
		<?
			$ctr == 0;
			$k = "D";
			$menu_dropdown = array();

			foreach($user_menus as $key => $obj)
			{
				if($obj->controller == "#")
				{
					$k = $key;
					$menu_dropdown[$k]['head'] = $obj->caption;
				}

				$menu_dropdown[$k]['menus'][] = $obj;

			}
		?>

		<?foreach ($menu_dropdown as $key => $value):?>
			<li class="dropdown">
		        <a href="#" class="dropdown-toggle mobile-active" data-toggle="dropdown"><?=$value['head']?> <b class="caret"></b></a>
		        <ul class="dropdown-menu li_account_menu2">
		        	<?foreach ($value['menus'] as $key => $m):?>
		        		<li class="" ><a href="<?=site_url().$m->controller?>" class="menu_tp" data-toggle="tooltip" data-placement="bottom" title="<?=isset($this->syspar->show_menu_tip) && $this->syspar->show_menu_tip == 1 ? $m->remarks : ""?>" ><?=$m->caption?></a></li>
		        	<?endforeach;?>
		        </ul>
		      </li>
		<?endforeach;?>
	<?endif;?>
    </ul>
   <ul class="nav navbar-nav navbar-right">

	  <li class="dropdown">
	    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Account Menus <b class="caret"></b></a>
	    <ul class="dropdown-menu li_account_menu">
	      <?if($this->session->userdata['userType']==='admin'):?>
	      <li><a href="<?=base_url()?>departments"  class=""><i class="fa fa-users"></i>&nbsp; Departments</a></li>
	      <li class="divider"></li>
	      <?endif;?>
	      <li><a href="<?=base_url()?>home"  class="">Home</a></li>
	      <li><a href="<?=base_url()?>messages/inbox"  class="">Messages<span class='badge' id='menu_unread_badge' ></span></a></li>
	      <li><a href="<?php echo base_url(); ?>news_and_events"  class="">Bulletin Board</a></li>
	      <li><a href="<?=base_url()?>change_password/index/<?php echo $this->session->userdata('userid'); ?>">Change Password</a></li>
	      <li class="divider"></li>
	      <li><a href="<?=base_url()?>open_semester_employee_settings/edit/<?php echo $this->session->userdata('userid'); ?>"  class="">Open Semester</a></li>
	      <li><a href="<?php echo base_url(); ?>auth/logout">Logout</a></li>
	    </ul>
	  </li>
	</ul>
  </div>
</div>