<!-- Modal -->
<div id="myModal_grading_system">
  <?if(isset($grading_system) && $grading_system != "input"):?>
    
    <?if(isset($grading_table) && $grading_table != "input"):?>
      <div class="table-responsive">
        <table class="table table-condensed table-bordered">
          <thead>
            <tr class="gray">
              <th>From</th>
              <th>To</th>
              <th>Grade</th>
              <th>Description</th>
            </tr>
          </thead>
          <?foreach ($grading_table as $k => $v):?>
            <tr>
              <td align="center" ><strong><?php echo $v->range_start?></strong></td>
              <td align="center" ><strong><?php echo $v->range_end?></strong></td>
              <td align="center" ><span class="badge badge-success"><?php echo $v->value ?></span></td>
              <td align="center" ><?php echo $v->desc?></td>
            </tr>
          <?endforeach;?> 
        </table>
      </div>
    <?else:?>
    <?endif;?>

  <?else:?>
    <p>The <strong>Grading System</strong> is set as <code>default</code>, meaning the grade does not have any Conversion.</p>
    <p>Any value entered by the teacher or users will be remain as is.</p>
  <?endif;?>

</div>