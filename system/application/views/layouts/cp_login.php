<?@$this->load->view('layouts/_cp_header')?>
<style>
	  
  body {
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background-color: black;
  }
  .form-signin {
    max-width: 280px;
    padding: 15px;
    margin: 0 auto;
      margin-top:50px;
  }
  .form-signin .form-signin-heading, .form-signin {
    margin-bottom: 10px;
  }
  .form-signin .form-control {
    position: relative;
    font-size: 16px;
    height: auto;
    padding: 10px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
  }
  .form-signin .form-control:focus {
    z-index: 2;
  }
  .form-signin input[type="text"] {
    margin-bottom: -1px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    border-top-style: solid;
    border-right-style: solid;
    border-bottom-style: none;
    border-left-style: solid;
    border-color: #000;
  }
  .form-signin input[type="password"] {
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    border-top-style: none;
    border-right-style: solid;
    border-bottom-style: solid;
    border-left-style: solid;
    border-color: rgb(0,0,0);
    border-top:1px solid rgba(0,0,0,0.08);
  }
  .form-signin-heading {
    color: #fff;
    text-align: center;
    text-shadow: 0 2px 2px rgba(0,0,0,0.5);
  }
</style>
<div id="fullscreen_bg" class="fullscreen_bg"/>

<div class="container">

	<?=form_open('gopanel/login', 'class="form-signin animated pulse"');?>
    <h1 class="form-signin-heading text-muted"><span class="entypo-tools"></span> Go Panel</h1>
		<?=isset($msg)?$msg:'';?>
		<?=validation_errors();?>
		<input type="text" class="form-control" placeholder="User" name="username" required="" autocomplete="Off" autofocus="">
		<input type="password" class="form-control" placeholder="Password" name="password" autocomplete="Off" required="">
		<input id="captcha" name="captcha" class="form-control" placeholder="Captcha" autocomplete="Off" value="<?=set_value('captcha')?>" type="text" autocomplete="off" required>
		<div class="input-group form-control">
	     	<?echo $cap_image;?>
	     	<script>
	     		$('img').addClass('img-responsive');
	     		$('img').addClass('required');
	     	</script>
	    </div>
      <p></p>
		<button class="btn btn-lg btn-primary btn-block" type="submit">
      <i class="fa fa-unlock"></i>&nbsp;
      Sign In
    </button>
    <p></p>
    <a class="pull-right" href="<?=site_url()?>"><i class="fa fa-power-off"></i>&nbsp; Exit</a>
	</form>

</div>
<?@$this->load->view('layouts/_footer')?>