<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
		<link REL="SHORTCUT ICON" HREF="<?=base_url('assets/images/favicon.ico');?>">
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	    <title><?php echo $this->setting->school_name; ?><?=$this->router->class!= "" ? " | ".ucwords(str_replace('_', ' ', $this->router->class)) : ''?></title>
	    <link href="<?php echo base_url('assets/css/bootstrap.default.min.css'); ?>" rel="stylesheet" type="text/css" media="screen" />
	    <link href="<?php echo base_url('assets/css/glyphicons.css'); ?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/font-awesome-4.0.3/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" media="screen" />
	    <link href="<?php echo base_url('assets/css/entypo/entypo.css'); ?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/iconicfill/iconicfill.css'); ?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/pace.css'); ?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/custom-theme/jquery-ui-1.8.18.custom.css'); ?>" rel="stylesheet" type="text/css" media="all" />
	    <link href="<?php echo base_url('assets/js/jnotify/jNotify.jquery.css'); ?>" rel="stylesheet" type="text/css" media="all" />
	    <link href="<?php echo base_url('assets/css/animate.css'); ?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/overwrite.css'); ?>" rel="stylesheet" type="text/css" />
		  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.7.1.min.js"></script>
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui-1.8.18.custom.min.js"></script>
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.number.min.js"></script>
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jnotify/jNotify.jquery.min.js"></script>

    </head>
    <body>	
   <?php @$this->load->view('layouts/_alert'); ?>
  <!-- Wrap all page content here -->
    <div class="wrap">
			<?=$yield?>
    </div>

    <div id="footer">
      <div class="container">
        <p class="text-muted credit">&copy; Copyright Gocloud Asia 2013</p>
      </div>
    </div>
    
  
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
   
    <link href="<?php echo base_url('assets/css/bootstrap-switch.css'); ?>" rel="stylesheet" type="text/css" />
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pace.js"></script>
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom.js"></script>
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-switch.js"></script>
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/myjs.js"></script>
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/blockui.js"></script>
	
    <script>
      $('.radio1').on('switch-change', function () {
        $('.radio1').bootstrapSwitch('toggleRadioStateAllowUncheck', true);
      });
    </script>
	
	<!--PLEASE WAIT-->
	<div id='please_wait' style='display:none;'>
		<img src="<?=base_url()?>assets/images/loading.gif" ></img>
		&nbsp;
		Just a moment please.
	</div>
	
    </body>
</html>
