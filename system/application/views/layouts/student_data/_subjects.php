<div class="table-responsive">
	<table class="table table-striped table-hover table-bordered">
		<thead>
			<tr>
				<th>#</th>
				<th>Course No.</th>
				<th>Description</th>
				<th>Units</th>
				<th>Lab</th>
				<th>Lec</th>
				<th>Time</th>
				<th>Day</th>
				<th>Room</th>
				<th>Instructor</th>
			</tr>
		</thead>
		<?if(isset($subjects) && $subjects): $l = 1;?>
			<?php
					$total_units = 0;
					$total_lec = 0;
					$total_lab = 0;
			?>
			<?foreach ($subjects as $k => $s):?>
				<?php
						$total_units += $s->units;
						$total_lec += $s->lec;
						$total_lab += $s->lab;
				?>
				<tr>
					<td><?php echo $l; $l++; ?></td>
					<td><strong><?php echo $s->code ?></strong></td>
					<td><strong><?php echo ucwords(strtolower($s->subject)) ?></strong></td>
					<td><?php echo $s->units; ?></td>
					<td><?php echo $s->lec; ?></td>
					<td><?php echo $s->lab; ?></td>
					<td><?php echo $s->time; ?></td>
					<td><?php echo $s->day; ?></td>
					<td><?php echo $s->room; ?></td>
					<td><?php echo $s->instructor; ?></td>
				</tr>
			<?endforeach;?>
			<tr>
				<td colspan="3" align="right" class='bold' >Total</td>
				<td id="total_units"><span class="badge badge-success"><?php echo number_format($total_units,2,'.',' '); ?></span></td>
				<td id="total_labs"><span class="badge"><?php echo number_format($total_lab,2,'.',' '); ?></span></td>
				<td id="total_labs"><span class="badge"><?php echo number_format($total_lec,2,'.',' '); ?></span></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		<?else:?>
			<tr>
				<td colspan="10" >No subject or schedule available</td>
			</tr>
		<?endif;?>
	</table>
</div>