<?php
	$subjects = $_student->student_subjects;
	$drop_subjects = $_student->dropped_subjects;
	$total_units = $_student->total_units;
	$total_lab = $_student->total_lab;
	$total_lec = $_student->total_lec;
	$is_redirect = (isset($is_redirect) && $is_redirect && $is_redirect == 'yes') ? "/yes" : '';
	$redirect_attr = $is_redirect ? "is_redirect = 'yes' " : '';
?>

<?=panel_head2('Subject Schedule',false,'default')?>
	<div class="table-responsive">
		<table class="table table-condensed table-hover table-stripped" >
			<thead>
			<tr class = "bold gray" >
				<td>Course No.</td>
				<td>Description</td>
				<td>Units</td>
				<td>Lab</td>
				<td>Lec</td>
				<td>Time</td>
				<td>Day</td>
				<td>Room</td>
				<td>Professor</td>
				<td>Action</td>
			</tr>
			</thead>
		<?php if(!empty($subjects)):?>
		<?
			$total_units = 0;
			$total_lec = 0;
			$total_lab = 0;
		?>
		<?php foreach($subjects as $subject):?>
			<?php #exclude dropped subjects
				if($subject->is_drop == 1){ continue; }
			?>
			<?

				$total_units += floatval($subject->units);
				$total_lec += floatval($subject->lec);
				$total_lab += floatval($subject->lab);
			?>
			<tr>
		    <td><?php echo $subject->code; ?></td>
		    <td><?php echo $subject->subject; ?></td>
		    <td><?php echo $subject->units; ?></td>
		    <td><?php echo $subject->lab; ?></td>
		    <td><?php echo $subject->lec; ?></td>
				<td><?php echo $subject->time; ?></td>
				<td><?php echo $subject->day; ?></td>
				<td><?php echo $subject->room; ?></td>
				<td><?php echo $subject->instructor; ?></td>
				<td>
					<?if(isset($mydepartment) && $mydepartment->stud_edit_subject === "1"):?>
					<div class="btn-group btn-group-sm">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Actions</i> <span class="caret"></span></button>
						<ul class="dropdown-menu" role="menu">
						<li><a  class="confirm " title="Are you sure to delete this subject? This action cannot be reverted back. Fees will automatically adjust." click_function="delete_student_subject" <?=$redirect_attr;?> href="<?=site_url('fees/ajax_delete_subject/'.$_student->enrollment_id."/".$subject->id)?><?=$is_redirect;?>"><i class="fa fa-trash-o"></i>&nbsp; Delete</a></li>
						<li><a  class="confirm " title="Are you sure to delete this subject? This action cannot be reverted back. Fees will automatically adjust." click_function="delete_student_subject" <?=$redirect_attr;?> href="<?=site_url('fees/ajax_drop_subject/'.$_student->enrollment_id."/".$subject->id)?><?=$is_redirect;?>"><i class="fa fa-arrow-down"></i>&nbsp; Drop</a></li>
					 </ul>
					</div>
					<!--JAVASCRIPT ACTION IN assets/js/myjs.js (delete_subject CLASS)-->
					<?else:?>
						-- No Access --
					<?endif;?>
				</td>
		  </tr>
			<?php endforeach; ?>
			<?php endif; ?>
			<tr id="user_subjects_total">
				<td>&nbsp;</td>
				<td class='bold'>Total</td>
				<td id="total_units"><span class="badge badge-success"><?php echo $total_units; ?></span></td>
				<td id="total_labs"><span class="badge"><?php echo $total_lab; ?></span></td>
				<td id="total_labs"><span class="badge"><?php echo $total_lec; ?></span></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</table>
	</div>

	<?if($drop_subjects):?>
	<?
		$total_units = 0;
		$total_lec = 0;
		$total_lab = 0;
	?>
	
	<h5 class="hr-text-left"><span>Dropped Subjects</span></h5>
	<div class="table-responsive">
		<table class="table" >
			<tr class="bold success" >
				<td>Course No.</td>
				<td>Description</td>
				<td>Units</td>
				<td>Lab</td>
				<td>Lec</td>
				<td>Time</td>
				<td>Day</td>
				<td>Room</td>
			</tr>
		<?php if(!empty($drop_subjects)):?>
		<?php foreach($drop_subjects as $subject):?>
			<?
				$total_units += floatval($subject->units);
				$total_lec += floatval($subject->lec);
				$total_lab += floatval($subject->lab);
			?>
			<tr>
			    <td><?php echo $subject->code; ?></td>
			    <td><?php echo $subject->subject; ?></td>
			    <td><?php echo $subject->units; ?></td>
			    <td><?php echo $subject->lab; ?></td>
			    <td><?php echo $subject->lec; ?></td>
				<td><?php echo $subject->time; ?></td>
				<td><?php echo $subject->day; ?></td>
				<td><?php echo $subject->room; ?></td>
		  </tr>
			<?php endforeach; ?>
			<?php endif; ?>
			<tr id="user_subjects_total">
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>Total</td>
				<td id="total_units"><?php echo $total_units; ?></td>
				<td id="total_labs"><?php echo $total_lab; ?></td>
				<td id="total_labs"><?php echo $total_lec; ?></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</table>
	</div>
<?endif;?>
<?=panel_tail2()?>

<script type="text/javascript">

	// THIS IS called in myjs.js - dynamic name of function according the attribute name "click_function=?" with class = "confirm"
	function delete_student_subject(url, xelement)
	{
		$.ajax({
			url : url,
			type: "POST",
			dataType: "json",
			success: function (data) {
				close_mymodal();
				
				if(data.status == 1){
					$(xelement).parents('tr').hide('slow');
					notify_modal('System Message',data.msg);
				}else{
					growl('system_message',data.msg);
					notify_modal('System Message',data.msg);
				}
			},
			error: function (xhr, status) {

				alert("Sorry, there was a problem in ajax! Refresh the page and try again.");
				close_pleasewait();
				close_mymodal();
			}
		})
	}
</script>