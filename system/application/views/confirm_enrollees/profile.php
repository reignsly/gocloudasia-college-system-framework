<div class="row">
	<?php echo panel_head2('Enrollment Summary',false,'primary') ?>
		<p></p>

		<div class="xwell">
			<?if($enroll->confirm_status === "PENDING"):?>
				<a class='btn btn-success btn-sm' href='<?=base_url('confirm_enrollees/confirm/'.__link($enroll->id)); ?>'>
					<span class="glyphicon glyphicon-thumbs-up"></span>&nbsp; Confirm Registration
				</a>
				<a class='btn btn-danger btn-sm confirm' title="Deleting this record will be permanent, do you still want to continue?" href='<?=base_url('confirm_enrollees/delete/'.__link($enroll->id)); ?>'>
					<span class="entypo-trash"></span>&nbsp; Delete
				</a>
			<?endif;?>

			<?if($enroll->confirm_status === "PENDING"):?>
				<a href="<?= base_url('confirm_enrollees'); ?>" class="btn btn-default btn-sm">Cancel</a>
			<?else:?>
				<a href="<?= base_url('confirm_enrollees/lists'); ?>" class="btn btn-default btn-sm">Back to List</a>
			<?endif;?>
		</div><p></p>

		<div class="table-responsive">
				<table class="table table-bordered">
					<tr class="bold" >
						<td><span>Enrollment Ref. # : <span class='label label-default label-lg'><?=$profile->studid?></span></span></td>
						<td>Name : <span class="label label-default label-lg"><?=ucwords(strtolower($profile->full_name))?></span></td>
		                  <td>
		                      <?php echo isset($profile->payment_plan) && $profile->payment_plan ? "Payment Plan : <span class='label label-default label-lg'>".ucwords($profile->payment_plan)."</span>" : ""; ?>
		                  </td>
					</tr>
					<tr class="bold" >
						<td>
		                      Course : 
		                      <span class='label label-lg label-default' ><?=$profile->course?> (<?=$profile->course_code?>)</span>
		                  </td>
						<td>Year Level : <span class='label label-default label-lg'><?=$profile->year?></span></td>
						<td>Status : <button class="btn btn-xs btn-default label-lg"><?=$profile->status?></button></td>
					</tr>
					<?if($profile->major):?>
						<tr class="bold" >
							<td>
                  Major : 
                  <span class='label label-lg label-default' ><?=$profile->major?></span>
			        </td>
						</tr>
					<?endif;?>
				</table>
		</div>
	<?php echo panel_tail2(); ?>
</div>

<div class="row">
	<?php @$this->load->view('layouts/student_data/_mavc_enrollment_form'); ?>
	<br>
	<br>
	<br>
	<fieldset class="scheduler-border">
		<legend class="scheduler-border">Schedule</legend>		
		<?php @$this->load->view('layouts/student_data/_subjects'); ?>
	</fieldset>
</div>

<script type="text/javascript" charset="utf-8">
 $(function(){
   
    $('input').attr('disabled', 'disabled');
    $('select').attr('disabled', 'disabled');
   
 });
</script>