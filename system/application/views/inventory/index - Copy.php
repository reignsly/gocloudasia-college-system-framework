<div id="right">
<div id="right_bottom">
	<div class="alerts">
    	<strong>Notice:</strong><br>
    		<ul id="notice">
    			<?php foreach( $items as $item): ?>
				<?php foreach( $products as $product): ?>
					<?php if($product->id == $item->product_id){ ?>
    				<?php if($item->unit < $product->min) { ?>
    				<li><?php echo $product->product.": ".$item->unit." left"; ?><?php if($this->session->userdata('userType') == 'admin'){ ?> <a href="product/edit/<?php echo $product->id;?>">Re-stock</a><?php } ?></li>
    				<?php }else if($item->unit > $product->max) { ?>
    				<li><?php echo $product->product.": ".$item->unit." reached maximum unit"; ?><?php if($this->session->userdata('userType') == 'custodian'){ ?> <a href="product/edit/<?php echo $product->id;?>">Deduct</a><?php } ?></li>
    				<?php } ?>
					<?php } ?>
				<?php endforeach; ?>
    			<?php endforeach; ?>
    		</ul>
    	</div>
<div id="tabled-info">
	<strong>Note:</strong><br>
	<p>
	<em>Live Search</em> helps you view records dynamically as you type the keywords.<br>
	</p>
</div>

<div class="search">
<form action="" method="post">
<input id="search" name="search" size="60x10" type="text" />
<input type="submit" value="Live Search" />
</form>
</div>


<div class="clear"></div>
<ul id="legend">
	<li> <strong>Legend:</strong> </li>
	<li> <span id='stable1'></span> <span id='stable2'></span>- stable </li>
	<li> <span id='restock'></span> - needs re-stocking </li>
	<li> <span id='maximum'></span> - reached maximum unit</li>
</ul>
<table id="tabled_inv">
<thead>
	<tr>
		<th class="header">Serial</th>
		<th class="header">Category</th>
		<th class="header">Brand</th>
		<th class="header">Kind</th>
		<th class="header">Measure</th>
		<th class="header">Unit</th>
		<th class="header">Purchase Price</th>
		<th class="header">Minimum</th>
		<th class="header">Maximum</th>
		<th class="header">Action</th>
	</tr>
</thead>
<tbody>
	<?php foreach ($items as $item): ?>
	<?php foreach( $products as $product): ?>
		<?php if($product->id == $item->product_id){ ?>
		<?php if($item->unit < $product->min) { ?>
	<tr class="inventory_warning">
		<?php }else if($item->unit > $product->max) { ?>
	<tr class='inventory_notice'>
		<?php }else{ ?>
	<tr class="<?php echo cycle('strip_1', 'strip_2'); ?>">
		<?php } ?>
		<td><?php echo $item->serial; ?></td>
		<td><?php echo $item->category; ?></td>
		<td><?php echo $item->brand; ?></td>
		<td><?php echo $item->kind; ?></td>
		<td><?php echo $item->size; ?></td>
		<td><?php echo $item->unit; ?></td>
		<td><?php echo $item->purchase_price; ?></td>
		<td><?php echo $product->min; ?></td>
		<td><?php echo $product->max; ?></td>
		<td>
		<a href="<?php echo site_url('inventory/edit').'/'.$item->product_id; ?>">Edit</a>
		<br/>
		<a href="<?php echo base_url()."inventory/destroy/".$product->id; ?>" rel="facebox" class="confirm-auto" title="Are you sure ?">Remove</a>
		</td>
	</tr>
		<?php } ?>
	<?php endforeach; ?>
	<?php endforeach; ?>
</tbody>
</table>
<br/>
<p><a href="<?php echo base_url(); ?>inventory/create" rel="facebox">New Item</a></li></p>
</div>
</div>
</div>