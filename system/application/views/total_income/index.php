<div id="right">
	<?php 
		$formAttrib = array('id' => 'search_enrollee', 'class' => 'form-inline', 'method' => 'GET');
		echo form_open('', $formAttrib);
	?>
  <div class="form-group">
    <?=course_dropdown('course_id',isset($course_id)?$course_id:'',"", 'All Courses')?>
  </div>
  <div class="form-group">
    <?=year_dropdown('year_id',isset($year_id)?$year_id:'',"", 'All Years')?>
  </div>
   
	<?php
	echo form_hidden('semester_id', $this->open_semester->id);
	echo form_hidden('sy_from', $this->open_semester->year_from);
	echo form_hidden('sy_to', $this->open_semester->year_to);
	echo form_hidden('is_paid', 1);
	?>
   		<?php echo form_submit('submit', 'Search'); ?>
   		<?php echo form_submit('submit', 'Print'); ?>
		<?echo form_close();?>
<br>
<?echo isset($links) ? $links : NULL;?>
	<?php
	if(isset($search)){
	?>
	<br />
	<table>
		<tr>
		  <th>OR. #</th>
		  <th>Date of Payment</th>
		  <th>Student ID</th>
		  <th>Name</th>
		  <th>Year</th>
		  <th>Course</th>
		  <th>Payment Amount</th>
		</tr>
		<tbody>
		<?php
		if($search)
		{
			foreach($search as $student):
			$l = _se($student->id);
			?>
			<tr>
			<td><?php echo $student->or_no; ?></td>
			<td><?php echo date('m-d-Y', strtotime($student->date)); ?></td>
			<td><?php echo $student->studid; ?></td>
			<td><?php echo ucfirst($student->name); ?></td>
			<td><?php echo $student->year; ?></td>
			<td><?php echo $student->course; ?></td>
			<td><?php echo number_format($student->total, 2, '.', ''); ?></td>
			</tr>
			<?php 
			endforeach; 
		}
		else
		{
		?>
		<tr>
		<td colspan="5">
			Student Not Found
		</td>
		</tr>
		<?php
		}
		?>
		</tbody>
		<tr>
		  <th>OR. #</th>
		  <th>Date of Payment</th>
		  <th>Student ID</th>
		  <th>Name</th>
		  <th>Year</th>
		  <th>Course</th>
		  <th>Payment Amount</th>
		</tr>
	</table>
	
	<?php
	}
	?>
	</div>
	
	<div class='well'>
		<div class="row">
		  <div class="col-md-10" style='text-align:right'><b>Total Income</b></div>
		  <div class="col-md-2"><b><?=number_format($total_income, 2, '.',' ')?></b></div>
		</div>
	</div>
	
</div>
