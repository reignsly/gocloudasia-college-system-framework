<div class="row">

	<!-- NEWS -->
  <div class="col-md-6">
		<ul class="nav nav-pills nav-stacked">
		  <li class="active">
			<a href="<?=base_url()?>announcements">
			  <span class="badge pull-right">Read All</span>
			  News
			</a>
		  </li>
		  <?if($news):?>
			<?foreach($news as $obj):?>
				<li>
					
					<div class='well'>
						<a class='label label-instagram label-lg' href="#"><?=$obj->title;?></a>
						<p></p>
					  <?=html_entity_decode($obj->message,ENT_COMPAT);?>
					  <br/>
					   <span class="badge pull-right">By : <?=$obj->department;?></span><br/>
					</div>
					
					
				 </li>
			<?endforeach;?>
		  <?endif;?>
		  
		</ul>
  </div>
  
  <!-- EVENTS -->
  <div class="col-md-6">
		<ul class="nav nav-pills nav-stacked">
		  <li class="active">
			<a href="<?=base_url()?>events">
			  <span class="badge pull-right">Read All</span>
			  Events
			</a>
		  </li>
		   <?if($events):?>
			<?foreach($events as $obj):?>
				<li>
					
					<div class='well'>
						<a class='label label-instagram label-lg' href="#"><?=$obj->title;?></a>
						<?if($obj->is_holiday == 1):?>
							<span class="badge pull-right">Holiday</span><br/>
					  <?endif;?>
						<p></p>
					  <?=html_entity_decode($obj->description,ENT_COMPAT);?>
					  <br/>
					   <span class="badge pull-right">From : <?=date('M d, Y', strtotime($obj->start_date));?>&nbsp;to&nbsp;<?=date('M d, Y',strtotime($obj->end_date));?></span><br/>
					</div>
					
					
				 </li>
			<?endforeach;?>
		  <?endif;?>
		</ul>
  </div>
</div>