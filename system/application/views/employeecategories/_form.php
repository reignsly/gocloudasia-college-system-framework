
 <script type="text/javascript">
	
	function validate()
	{
		var id = $('#category');
		
		if(id.val() == "")
		{
			custom_modal('Required','<h4>Required Category Name.</h4>');
			return false;
		}
	}
	
 </script>
  <p>
    <label for="category">Category</label><br />
	<?php 
		$data = array(
              'name'        => 'employeecategories[category]',
              'id'        => 'category',
              'value'       => isset($employeecategories) ? $employeecategories->category : '',
              'maxlength'   => '100',
              'size'        => '50',
              'style'       => 'width:50%',
            );

		echo form_input($data);
	?>
  </p>
  <div class="clear"></div>

