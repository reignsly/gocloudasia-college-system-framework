<a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>employeecategories/create" rel="facebox"><span class = 'glyphicon glyphicon-plus'></span>&nbsp;  New Employee Category</a>
<br/>
<br/>
<table id="table">
<thead>
	<tr>
    <th>Category</th>
    <th>Action</th>
  </tr>
</thead>
<tfoot>
	<tr>
    <th>Category</th>
	<th>Action</th>
  </tr>
</tfoot>
<tbody>
	
  <?php if($employeecategories){ ?>
  <?php foreach( $employeecategories as $employeecategory): ?>
    <tr>
      <td><?php echo $employeecategory->category ; ?></td>
      <td><a href="<?php echo base_url()."employeecategories/edit/".$employeecategory->id; ?>" rel="facebox" class="actionlink"><span class='glyphicon glyphicon-pencil' ></span>&nbsp;  Edit</a>
      | <a href="<?php echo base_url()."employeecategories/destroy/".$employeecategory->id; ?>" rel="facebox" class="actionlink confirm"><span class='glyphicon glyphicon-trash' ></span>&nbsp; Destroy</a></td>
   	</td>
    </tr>
  <?php endforeach; ?>
  <?php } ?>
  
</tbody>
</table>

<p><a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>employeecategories/create" rel="facebox"><span class = 'glyphicon glyphicon-plus'></span>&nbsp;  New Employee Category</a>