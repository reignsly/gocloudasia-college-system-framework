
 
  <p>
    <label for="year">Academic Year</label><br />
	<?php 
		$year_from= array(
              'name'        => 'academic_years[year_from]',
              'value'       => isset($academic_years->year_from) ? $academic_years->year_from : '',
              'maxlength'   => '4',
              'size'        => '50',
              'style'       => 'width:100px',
              'required'    => true
            );
		$year_to= array(
              'name'        => 'academic_years[year_to]',
              'value'       => isset($academic_years->year_to) ? $academic_years->year_to : '',
              'maxlength'   => '4',
              'size'        => '50',
              'style'       => 'width:100px',
              'required'    => true
            );

		echo form_input($year_from) .' - '.form_input($year_to);
	?>
  </p>
  <div class="clear"></div>
