<p><a class='btn-add' href="<?php echo base_url(); ?>academic_years/create" rel="facebox"><span class = 'glyphicon glyphicon-plus'></span>&nbsp;  New Academic Year</a></li></p>
<div class="table-responsive">
  <table id="table" class="table">
    <tr>
      <th>Academic Year</th>
      <th>Action</th>
    </tr>
    <?php if($academic_years):?>
      <?php foreach( $academic_years as $academic_year): ?>
        <tr>
          <td><?php echo $academic_year->year_from.'-'.$academic_year->year_to ; ?></td>
          <td>
          
            <div class="btn-group">
                <a class="btn btn-default btn-xs" href="<?php echo base_url()."academic_years/edit/".$academic_year->id; ?>"  ><span class='glyphicon glyphicon-edit' ></span>&nbsp;  Edit</a>
                <a class="btn btn-danger btn-xs confirm" href="<?php echo base_url()."academic_years/destroy/".$academic_year->id; ?>" ><span class='glyphicon glyphicon-remove' ></span>&nbsp; Destroy</a>    
            </div>
        </td>
        </tr>
      <?php endforeach; ?>
    <?endif;?>
  </table>
</div>