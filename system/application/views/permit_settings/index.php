<div id="right">

<div id="right_top" >
	<p id="right_title">Years : Index</p>
</div>
		  
<div id="right_bottom">
  

<p><a href="<?php echo base_url(); ?>years/create" rel="facebox">New Year</a></li></p>

<table id="table">
<thead>
	<tr>
    <th>Year</th>
    <th>Action</th>
  </tr>
</thead>
<tfoot>
	<tr>
    <th>Year</th>
    <th>Action</th>
  </tr>
</tfoot>
<tbody>
	
  <?php if($years){ ?>
  <?php foreach( $years as $year): ?>
    <tr>
      <td><?php echo $year->year ; ?></td>
      <td>
      <ul class="actionlink">
      <li><a href="<?php echo base_url()."years/edit/".$year->id; ?>" rel="facebox" class="actionlink">Edit</a></li>
      <li><a href="<?php echo base_url()."years/destroy/".$year->id; ?>" rel="facebox" class="actionlink confirm">Destroy</a></li>
       </ul>
       </li></ul>
   	</td>
    </tr>
  <?php endforeach; ?>
  <?php } ?>
  
</tbody>
</table>

<p><a href="<?php echo base_url(); ?>years/create" rel="facebox">New Year</a></li></p>

</div>

</div>
