	<div class="well">
		<?echo form_open('','class="form-inline" role="form"');?>
		<div class="form-group">
			Search In
		</div>
		 <div class="form-group">
			<?
				$xfields = array(
					"demands.borrow_no" => "Borrow No.",
					"items.item" => "Item",
					"users.name" => "Borrower Name",
					"demands.date" => "Date",
				);
				echo form_dropdown('fields', $xfields, isset($fields)?$fields:'serial');
			?>
		</div>
		<div class="form-group">
			<label class="sr-only" for="author">Keyword</label>
			<input type='text' name='keyword' value="<?=isset($keyword)?$keyword:''?>" placeHolder="Keyword" />
		</div>
		<div class="form-group">
			Status
		</div>
		<div class="form-group">
			<?
				$xstatus = array(
					"" => "ALL",
					"UNRETURN" => "UNRETURNED",
					"RETURNED" => "RETURNED",
					"RETURNED W/ LOST/DAMAGE" => "RETURNED W/ LOST/DAMAGE",
					"PARTIAL RETURNED" => "PARTIAL RETURNED",
					"PARTIAL RETURNED W/ LOST/DAMAGE" => "PARTIAL RETURNED W/ LOST/DAMAGE",
					"LOST/DAMAGE" => "LOST/DAMAGE",
				);
				echo form_dropdown('status', $xstatus, isset($status)?$status:'');
			?>
		</div>
		<?
			echo form_submit('submit','Search');		?>
			<?echo form_close();?>
	</div>

	<table id="tabled_inv">
		<thead>
			<tr>
				<th class="header">Date</th>
				<th class="header">Borrow No.</th>
				<th class="header">Item</th>
				<th class="header">Borrower Name</th>
				<th class="header">Unit Borrowed</th>
				<th class="header">Unit Return</th>
				<th class="header">Unit Lost/Damage Return</th>
				<th class="header">Unit Unreturn</th>
				<th class="header">Status</th>
				<th class="header">Action</th>
			</tr>
		</thead>
		<tbody>
			<?if($items):?>
			<?php foreach ($items as $item): ?>
				<td><?php echo $item->date?></td>
				<td><?php echo $item->borrow_no; ?></td>
				<td><?php echo $item->item; ?></td>
				<td><?php echo $item->name; ?></td>
				<td><?php echo $item->unit; ?></td>
				<td><?php echo $item->unit_return; ?></td>
				<td><?php echo $item->unit_lost_return; ?></td>
				<td><?php echo $item->unit_unreturn; ?></td>
				<?	
					$style = "";
					$status = "";
					switch(strtoupper($item->status)){
						case "RETURNED":
							$style='success';
							$status = "Returned";
						break;
						case "PARTIAL RETURNED":
							$style='warning';
							$status = "Partial Returned";
						break;
						case "UNRETURN":
							$style='danger';
							$status = "Unreturned";
						break;
						default:
							$style = "primary";
							$status = "Unreturned";
						break;
					}
				?>
				<td><div style="font-size:9pt" class='label label-<?=$style?>'><?php echo $status; ?></div></td>
				<td>
				<!--ALLOW DELETE FEATURES ONLY WHEN THERE IS NO TRANSACTION MEANING THE STATUS IS STILL UNRETURNED-->
				<?if(strtoupper($item->unit_unreturn) > 0){ ?>
					<?//=badge_link('borrow_items/edit/'.$item->id, 'primary', 'Edit')?>
					<?=badge_link('borrow_items/return_item/'.$item->id, 'primary', 'return')?>
					<?=badge_link('borrow_items/return_lost_or_damage/'.$item->id, 'primary', 'return lost/damage')?><br/>
				<?}?>
				
				<?if(($item->unit_return + $item->unit_lost_return) == 0):?>
					<?=badge_link('borrow_items/destroy/'.$item->id, 'primary', 'Destroy', '', true)?>
				<?else:?>
					<?=badge_link('borrow_items/view_return/'.$item->id, 'primary', 'view returned')?><br/>
				<?endif;?>
				
				</td>
			</tr>
			<?php endforeach; ?>
			<tr><td colspan="9" style='text-align:right;'>No. of Record</td><td><div class='badge'><?=$total_rows;?></div></td></tr>
			<?else:?>
			<tr><td colspan="11">No record found.</td></tr>
			<?endif;?>
		</tbody>
	</table>
	
	<?=$links?>
