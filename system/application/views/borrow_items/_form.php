	
 <script>
  $(function() {
    $( "#advance_search" ).dialog({
      autoOpen: false,
      height: 300,
      width: 550,
      modal: true,
      title: "Advance Borrower Search",
    });
	$( "#items_search" ).dialog({
      autoOpen: false,
      height: 300,
      width: 550,
      modal: true,
      title: "Advance Item Search",
    });
  });
  
  function validate()
	{
		var ctr = 0;
		var msg = "<ul>";
		var xfocus = "";
		$('.not_blank').css('border-color','gray');
		$('.not_blank').each(function(){
			var xval = $(this).val().trim();
			var xid = $(this).attr('id');
			if(xval == ""){
				ctr++;
				$(this).css('border-color','red');
				if(ctr == 1) { xfocus = $(this); }
				var label = $('label[for='+xid+']').text();
				
				msg += "<li>"
				+label+" should not be blank</li>";
			}
		});
		
		msg += "<ul>";
		
		if(ctr > 0)
		{
			xfocus.focus();
			$('#alertModal_Label').text('Required Fields'); //SET MODAL TITLE
			$('#alertModal_Body').html(msg); //SET MODAL CONTENT
			$('#alertModal').modal('show'); //SHOW MODAL
			
			return false;
		}
		else
		{
			msg = "<ul>";
			var ctr2 = 0;
			$('.is_number').each(function(){
				var xval = $(this).val().trim();
				var isnum = isNumber(xval);
				var xid = $(this).attr('id');
				
				if(isnum == false)
				{
					ctr2++;
					$(this).css('border-color','red');
					$(this).focus();
					$(this).val('');
					var label = $('label[for='+xid+']').text();
					msg += "<li>"+label+" should be numeric.</li>";
			
				}
			});
			
			if(ctr2 > 0){
				msg += "</ul>";
				$('#alertModal_Label').text('Required Fields'); //SET MODAL TITLE
				$('#alertModal_Body').html(msg); //SET MODAL CONTENT
				$('#alertModal').modal('show'); //SHOW MODAL
				
				return false;
			}
		}
	}
	
	function Select(id)
	{
		$('#user_id').val(id);
		$('#advance_search').dialog('close');
	}
	
	function Select_item(id)
	{
		// cosole.log(id);
		$('#item_id').val(id);
		$('#items_search').dialog('close');
	}
	
	function CheckItem(data)
	{
		var xval = parseFloat($(data).val());
		if(!isNaN(xval)){
			var xitemid = $('#item_id').val();
			if(xitemid != ""){
				console.log('x');
					var controller = 'ajax';
					var base_url = '<?php echo base_url(); ?>'; 
					
					$.ajax({
						'url' : base_url + '' + controller + '/check_item',
						'type' : 'POST', 
						'async': false,
						'data' : { 'id' : xitemid, 'unit' : xval },
						'dataType' : 'json',
						'success' : function(data){ 
									if(data.validate === false)
									{
										$(data).focus();
										$('#unit').val('');
										$('#alertModal_Label').text('Please check'); //SET MODAL TITLE
										$('#alertModal_Body').html(data.msg); //SET MODAL CONTENT
										$('#alertModal').modal('show'); //SHOW MODAL
									}
								}
							})
						}
		}
	}
	
	function Search(page)
	{
		var page = page == null ? 0 : page;
		$.blockUI({ message: "" }); 
		var xreturn = false;
		var controller = 'ajax';
		var base_url = '<?php echo base_url(); ?>'; 
		var xdata = $('#advance_search *').serialize();
		
		$.ajax({
			'url' : base_url + '' + controller + '/search_users/'+page,
			'type' : 'POST', 
			'async': false,
			'data' : xdata,
			'dataType' : 'json',
			'success' : function(data){ 
				// console.log(data);
				$('#links').html(data.links);
				
				//PREVENT LINKS FROM REDIRECTING
				$('#links li a').click(function(event){
					event.preventDefault();
					var href = $(this).attr("href");
					next_page = (href.substr(href.lastIndexOf('/') + 1));
					Search(next_page);
				})
				$('#tbody').html('');
				
				var tr = "";
				
				if(data.total_rows > 0){
					for(var x = 0 ; x <= data.count; x++){
						if(data.users[x] != null){
						var xid = data.users[x].id;
						var xlogin = data.users[x].login;
						var xname = data.users[x].name;
						// console.log(xid);
						// console.log(data.users[x]);
						tr += "<tr>";
						tr += "<td>"+xlogin+"</td>";
						tr += "<td>"+xname+"</td>";
						tr += "<td><a href='javascript:;'><div class='badge' onclick=\"Select('"+xid+"')\">Select</div></a></td>";
						tr += "</tr>";
						}
					}
					tr += "<tr><td colspan='2'>No of Records</td><td><span class='badge'>"+data.total_rows+"</span></td></tr>";
					
				}else{
					tr = "<tr><td colspan='3'>No records found.</td></tr>";
				}
				
				$('#tbody').html(tr);
			}
		})
		.done(function() {
			  $.unblockUI(); 
		  });
		
	}
	
	function Search_item(page)
	{
		var page = page == null ? 0 : page;
		$.blockUI({ message: "" }); 
		var xreturn = false;
		var controller = 'ajax';
		var base_url = '<?php echo base_url(); ?>'; 
		var xdata = $('#items_search *').serialize();
		
		$.ajax({
			'url' : base_url + '' + controller + '/search_items/'+page,
			'type' : 'POST', 
			'async': false,
			'data' : xdata,
			'dataType' : 'json',
			'success' : function(data){ 
				// console.log(data);
				$('#search_links').html(data.links);
				
				//PREVENT LINKS FROM REDIRECTING
				$('#search_links li a').click(function(event){
					event.preventDefault();
					var href = $(this).attr("href");
					next_page = (href.substr(href.lastIndexOf('/') + 1));
					Search_item(next_page);
				})
				$('#tbody').html('');
				
				var tr = "";
				
				if(data.total_rows > 0){
					for(var x = 0 ; x <= data.count; x++){
						if(data.items[x] != null){
						var xid = data.items[x].id;
						var xserial = data.items[x].serial;
						var xitem = data.items[x].item;
						var xcategory = data.items[x].category;
						var xunit_left = data.items[x].unit_left;
						var xunit_price = data.items[x].purchase_price;
					
						tr += "<tr>";
						tr += "<td>"+xserial+"</td>";
						tr += "<td>"+xitem+"</td>";
						tr += "<td>"+xcategory+"</td>";
						tr += "<td>"+xunit_left+"</td>";
						tr += "<td>"+xunit_price+"</td>";
						tr += "<td><a href='javascript:;'><div class='badge' onclick=\"Select_item('"+xid+"')\">Select</div></a></td>";
						tr += "</tr>";
						}
					}
					tr += "<tr><td colspan='5'>No of Records</td><td><span class='badge'>"+data.total_rows+"</span></td></tr>";
					
				}else{
					tr = "<tr><td colspan='6'>No records found.</td></tr>";
				}
				
				$('#items_tbody').html(tr);
			}
		})
		.done(function() {
			  $.unblockUI(); 
		  });
		
	}
  </script>	
  <p>
    <label for="borrow_no">Borrow Item No. : <span class='label label-info'><strong><?=isset($borrow_items->borrow_no)?$borrow_items->borrow_no:"Computer Generated"?></strong></span>
	</label><br />
  </p>
  <p>
    <label for="date">Date</label><br />
    <input id="date" class="not_blank date_pick" name="borrow_items[date]" size="30" type="text" value='<?=isset($borrow_items->date)?$borrow_items->date:""?>' />
  </p>
  <p>
    <label for="user_id">Borrower * &nbsp;</label><button type="button" class="btn btn-default btn-sm btn-xs" onclick='$("#advance_search").dialog("open");'>Advance Search</button><br />
	<?=users_dropdown('borrow_items[user_id]',isset($borrow_items->user_id)?$borrow_items->user_id:'', 'Choose Borrowers','class="not_blank" id="user_id"');?>
  </p>
  <p>
    <label for="item_id">Items * &nbsp;</label><button type="button" class="btn btn-default btn-sm btn-xs" onclick='$("#items_search").dialog("open");'>Advance Search</button><br />
	<?=items_dropdown('borrow_items[item_id]',isset($borrow_items->item_id)?$borrow_items->item_id:'', 'id="item_id"','Choose Item');?>
  </p>
  <p>
    <label for="unit">Quantity *</label><br />
    <input id="unit" maximum_unit='' name="borrow_items[unit]" size="30" type="text" class='not_blank is_number' value='<?=isset($borrow_items->unit)?$borrow_items->unit:""?>' onchange='CheckItem(this)' />
  </p>
  
  
  	<div id='advance_search'>
		<table>
		  <tr>
			<td><input type="text" class="form-control" id="borrower_login" name="borrower_login" placeholder="ID/Login" style='width:150px'></td>
			<td><input type="text" class="form-control" id="borrower_name" name="borrower_name" placeholder="Name" style='width:150px'></td>
			<td><button type="button" class="btn btn-default btn-sm" onclick='Search()'>Search</button></td>
		  </tr>
		</table>
			<table id='results' style='font-size:7pt'>
			  <tr>
				<th>ID/Login</th>
				<th>NAME</th>
				<th>ACTION</th>
			  </tr>
				<tbody id='tbody'>
				</tbody>
			</table>
			<div id='links' style='font-size:7pt'></div>
	</div>
  	<div id='items_search'>
		<table>
		  <tr>
			<td> <div class="form-group">
				<?
					$xfields = array(
						"items.serial" => "Serial",
						"items.item" => "Item",
						"items.category" => "Category",
						"items.brand" => "Brand",
						"items.kind" => "Kind",
					);
					echo form_dropdown('fields', $xfields, isset($item_field)?$item_field:'serial','id="item_field"');
				?>
				</div>
			</td>
			<td>
				<div class="form-group">
					<label class="sr-only" for="author">Keyword</label>
					<input type='text' name='keyword' id='keyword' value="<?=isset($keyword)?$keyword:''?>" placeHolder="Keyword" />
				</div>
			</td>
			<td><button type="button" class="btn btn-default btn-sm" onclick='Search_item()'>Search</button></td>
		  </tr>
		</table>
			<table id='results' style='font-size:7pt'>
			  <tr>
				<th>Serial</th>
				<th>Items</th>
				<th>Category</th>
				<th>Unit Left</th>
				<th>Price</th>
				<th>ACTION</th>
			  </tr>
				<tbody id='items_tbody'>
				</tbody>
			</table>
			<div id='search_links' style='font-size:7pt'></div>
	</div>