<script type='text/javascript'>
	$( document ).ready(function() {
		$('.ui-widget-overlay').live("click",function(){
            $("#message").dialog("close");
        });  
	});
  
	function Validate()
	{
		var ctr = 0;
		var msg = "<ul>";
		var xfocus = "";
		$('.not_blank').css('border-color','gray');
		$('.not_blank').each(function(){
			var xval = $(this).val().trim();
			if(xval == ""){
				ctr++;
				$(this).css('border-color','red');
				if(ctr == 1) { xfocus = $(this); }
				var attr_name = $(this).attr('name');
				var label = $('label[for="'+attr_name+'"]').text();

				msg += "<li>"
				+label+" should not be blank</li>";
			}
		});
		
		msg += "<ul>";
		
		if(ctr > 0)
		{
			xfocus.focus();
			$('#message_list').html(msg);
			$( "#message" ).dialog({
			  height : 300,
			  modal : true,
			  buttons: {
				Close: function() {
				  $( this ).dialog( "close" );
				}
			  }
			});
			
			return false;
		}
		else
		{
			var ctr2 = 0;
			$('.amount').each(function(){
				var xval = $(this).val().trim();
				if(xval != "") { 
					ctr2++;
				}
			});
		}
	}
	
	function CloseBlock(){
		$.unblockUI();
	}
</script>
<div id="right">
	<div id="right_bottom">
	
	<div id="parent">
	
	<form action =<?=site_url('payment_record/add_deduction').'/'.$enrollment_id; ?> method ="POST" onsubmit='return Validate()'>
	   <p>
		<label for='date'>Date</label><br />
		<input type="text" class="date_pick not_blank" name="date" value="<?=set_value('date',date('Y-m-d'));?>">
	  </p>
	  <p>
		<label for='amount'>Amount</label><br />
		<input class='not_blank currency' type="text" name="amount" value="<?=set_value('amount');?>"  placeHolder="0.00">
	  </p>
	  <p>
		<label>Remarks</label><br />
		<textarea name="remarks" style="min-width:500px;max-width:700px;min-height:200;max-height:300px;"><?=set_value('remarks');?></textarea>
	  </p>
	  <input type="hidden" name="form_token" value="<?php echo $form_token;?>" />
	<?php 
		echo form_hidden('enrollment_id', $enrollment_id);
		echo form_submit('add_payment_record','Submit'); 
	?>
		| <a class='btn btn-default btn-sm' href="<?php echo site_url('fees/view_fees').'/'.$enrollment_id; ?>">View Fees</a> | 
		<a class='btn btn-default btn-sm' href="<?php echo site_url('fees/view_deducted_fees').'/'.$enrollment_id; ?>">Back To List</a>
	<?php echo form_close(); ?>
	
	</div>

	</div>
	
	<div class="clear"></div>
</div>
<div id='message' title='Required Fields' style='display:none;'>
	<div id='message_list' class='alert alert-info'>
		Validating ... 
	</div>
</div>