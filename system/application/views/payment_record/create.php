<script type='text/javascript'>
	$( document ).ready(function() {
		$('.ui-widget-overlay').live("click",function(){
            $("#message").dialog("close");
        });  
	});
  
	function Validate()
	{
		var ctr = 0;
		var msg = "<ul>";
		var xfocus = "";
		$('.not_blank').css('border-color','gray');
		$('.not_blank').each(function(){
			var xval = $(this).val().trim();
			if(xval == ""){
				ctr++;
				$(this).css('border-color','red');
				if(ctr == 1) { xfocus = $(this); }
				var label = $(this).parent().prev().text();
				
				msg += "<li>"
				+label+" should not be blank</li>";
			}
		});
		
		msg += "<ul>";
		
		if(ctr > 0)
		{
			xfocus.focus();
			$('#message_list').html(msg);
			$( "#message" ).dialog({
			  height : 300,
			  modal : true,
			  buttons: {
				Close: function() {
				  $( this ).dialog( "close" );
				}
			  }
			});
			
			return false;
		}
		else
		{
			var ctr2 = 0;
			$('.amount').each(function(){
				var xval = $(this).val().trim();
				if(xval != "") { 
					ctr2++;
				}
			});
			
			if(ctr2 == 0){
				$('.amount').css('border-color','red');
				$('#message_list').html('<h3">Please fill up atleast one of the amount fields.</h3>');
				$( "#message" ).dialog({
				  height : 300,
				  modal : true,
				  buttons: {
					Close: function() {
					  $( this ).dialog( "close" );
					}
				  }
				});
				return false;
			}
		}
	}
	
	function CloseBlock(){
		$.unblockUI();
	}
</script>
<div id="right">
	<div id="right_bottom">
	
	<div id="parent">
	
	<form action =<?=site_url('payment_record/create'); ?> method ="POST" onsubmit='return Validate()' >
	  <p>
		<label>Date of payment</label><br />
		<input type="test" class="date_pick not_blank" name="date_of_payment" value="<?=set_value('date_of_payment',date('Y-m-d'));?>">
	  </p>
	    <p>
		<label for="studid">Student ID NO.</label><br />
		<input class='not_blank' type="text" name="studid" value="<?=set_value('studid');?>" placeHolder = "Required" />
	  </p>
	  <p>
		<label for="receipt_number">OR NO.</label><br />
		<input class='not_blank' type="text" name="receipt_number" value="<?=set_value('receipt_number');?>" placeHolder = "Required" />
	  </p>
	   <p>
		<label for="receipt_number">GROUP OR NO.</label><br />
		<input type="text" name="group_or_no" value="<?=set_value('group_or_no');?>">
	  </p>
	  <p>
		<label>Account Receivable Student</label><br />
		<input class='amount' type="text" name="amount" value="<?=set_value('amount');?>" placeHolder = "0.00" />
	  </p>
	  <p>
		<label>Old Account</label><br />
		<input class='amount' type="text" name="old_account" value="<?=set_value('old_account');?>" placeHolder = "0.00" />
	  </p>
	   <p>
		<label>Other</label><br />
		<input class='amount' type="text" name="other" value="<?=set_value('other');?>" placeHolder = "0.00" />
	  </p>
	  <p>
		<label>Remarks</label><br />
		<textarea name="remarks" style="min-width:500px;max-width:700px;min-height:300px;max-height:300px;"><?=set_value('remarks');?></textarea>
	  </p>
	  <input type="hidden" name="form_token" value="<?php echo $form_token;?>" />
	<?php echo form_submit('add_payment_record','Submit'); ?>
	<?php echo form_close(); ?>
	
	</div>

	</div>
	
	<div class="clear"></div>
</div>
<div id='message' title='Required Fields' style='display:none;'>
	<div id='message_list' class='alert alert-info'>
		Validating ... 
	</div>
</div>