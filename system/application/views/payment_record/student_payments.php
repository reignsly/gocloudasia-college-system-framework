<div class="well">
<table >
	<tr>
		<th>Enrollment Id</th>
		<th>Name</th>
		<th>Course</th>
		<th>Year Level</th>
		<th>Payments</th>
	</tr>
	<?
	$ctr = 0;
	?>
	<?if($enrollments):?>
	<?foreach($enrollments as $obj):?>
		<tr>	
			<td><?=$obj->id;?></td>
			<td><?=$obj->name?></td>
			<td><?=$obj->course;?></td>
			<td><?=$obj->year;?></td>	
			<td colspan = 4>
				<table>
					<tr>
						<th>OR #</th>
						<th>Amount</th>
						<th>Remarks</th>
						<th>Date</th>
					
					</tr>
					<?if(isset($payments[$obj->id]) && $payments[$obj->id]):?>
						<?foreach($payments[$obj->id] as $val):?>
							<tr>
								<td><?=$val->or_no;?></td>
								<td><?=number_format($val->total,2);?></td>
								<td><?=$val->remarks;?></td>
								<td><?=date('m-d-Y', strtotime($val->date));?></td>	
							</tr>
						<?endforeach;?>
					<?endif;?>
				</table>
			</td>
		</tr>
	<?endforeach;?>
	<tr class=''>
		<td colspan = 8 style='text-align:right'>Total No. of Records</td>
		<td><div class='badge'><?=$total_rows?></div></td>
	</tr>
	<?else:?>
	<tr>
		<td colspan = '13'>No record found.</td>
	</tr>
	<?endif;?>
</table>
<?= $links;?>
</div>