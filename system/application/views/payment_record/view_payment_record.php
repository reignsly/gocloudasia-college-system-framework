<div id="right">
	<div id="right_bottom">
	  
	
	<div class="clear" style="margin-bottom:10px;"></div>
	
		<table>
			<tr>
				<th>Date</th>
				<th>OR #</th>
				<th>Account Receivables</th>
				<th>Old Account</th>
				<th>Others</th>
				<th>Total Amount</th>
				<th>Remarks</th>
				<th colspan=2>Action</th>
			</tr>
			<? $total_amount = 0; ?>
			<?php if(empty($payment_record) == FALSE):?>
				<? foreach($payment_record as $s): ?>
					<tr>
						<td><?=$s->created_at;?></td>
						<td><?=$s->or_no;?></td>
						<td>&#8369; <?=number_format($s->account_recivable_student,2,'.',' ');?></td>
						<td>&#8369; <?=number_format($s->old_account,2,'.',' ');?></td>
						<td>&#8369; <?=number_format($s->other,2,'.',' ');?></td>
						<td>&#8369; <?=number_format($s->total,2,'.',' ');?></td>
						<? $total_amount += $s->total; ?>
						<td><?=$s->remarks;?></td>
						<td>
							  <a href="<?=site_url('payment_record/show_payment/'.$s->id.'/'.$enrollment_id);?>">Show</a> | 
							  <a href="<?=site_url('payment_record/destroy/'.$s->id.'/'.$enrollment_id);?>" class="confirm">Delete</a>
					</tr>
				<? endforeach; ?>
				<tr>
					<td colspan ='5' style='text-align:right'>Total Payment</td>
					<td><span class='badge'>&#8369; <?=number_format($total_amount,2,'.',' ');?></span></td>
				</tr>
			<?php else:?>
				<tr>
					<td>&nbsp;</td>
					<td>----NO data to show---</td>
					<td>----NO data to show---</td>
				</tr>	
			<?php endif;?>
		</table>
		<p>
			<a class='btn btn-default btn-sm' href="<?=site_url('fees/view_fees/'.$enrollment_id);?>">Back To student fees</a> 
			<?if(($student_total) && $student_total->status != "PAID"):?>
			|<a class='btn btn-default btn-sm' href="<?=site_url('payment_record/add/'.$enrollment_id);?>">Add New Payment Record</a>
			<?endif;?>
		</p>
	</div>
</div>
