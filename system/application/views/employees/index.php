<?php 
	$formAttrib = array('id' => 'search_employee', 'class' => 'form-inline', 'method' => 'GET');
	echo form_open('employees/index/0', $formAttrib); ?>
  
	 <div class="form-group">
	    <input id="name" class="form-control" type="text" value="<?=isset($name)?$name:''?>" name="name" placeholder="Name">
	  </div>
	   
	  <div class="form-group">
	    <input id="login" class="form-control" type="text" value="<?=isset($login)?$login:''?>" name="login" placeholder="Employee ID / Login ID">
	  </div>

	  <div class="form-group">
	    <b>Role : </b>&nbsp;
	  </div>

	  <div class="form-group">
		<?
	    echo departments_dropdown('role',isset($role) ? $role : '','','All');
		?>
		
	  </div>
	  <?php echo form_submit('submit', 'Search'); ?>
	  <a href="<?=site_url('employees/create')?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp; Create Employee</a>
	  <a href="<?=site_url('employees/multiple')?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp; Batch Insert</a>
<?echo form_close();?>
<p></p>
<p><strong>Total Record</strong> <span class="badge" ><?php echo $total_rows; ?></span></p>
<p></p>
<?echo isset($links) ? $links : NULL;?>
	<?php
	if(isset($search)){
	?>
		<div class="table-responsive">
			<table class="table table-sortable table-striped table-bordered">
				<thead>
					<tr class='gray' >
						<th>Name</th>
						<th>Login</th>
						<th>Role</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if(!empty($search))
					{
						foreach($search as $s):
						$l = _se($s->id);
						?>
						<tr>
							<td><strong><?=ucwords($s->fullname);?></strong></td>
							<td><strong><?=$s->employeeid;?></strong></td>
							<td><?=ucwords($s->role);?></td>
							<td>
								<div class="btn-group btn-group-sm">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								
									<i class="fa fa-caret-square-o-down"></i> <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
									
									<? if(strtolower($this->session->userdata['userType']) == "hrd" || strtolower($this->session->userdata['userType']) == "admin") :?>
										<li><a href="<?= site_url('employees/edit/'.$s->id.'/'. $s->employeeid );?>"><span class='glyphicon glyphicon-edit' ></span>&nbsp;Edit Profile</a></li>
										<li><a href="<?= site_url('change_password/reset_employee/'.$s->id.'/'. $s->employeeid );?>"><span class='glyphicon glyphicon-refresh' ></span>&nbsp;Change Password</a></li>
									<? endif;?>

									<? if(strtolower($this->session->userdata['userType']) == "teacher") :?>
										<!-- <li><a href="<?= site_url('employees/add_assign_course/'.$s->id.'/'. $s->employeeid);?>"><i class="fa fa-list-alt"></i>&nbsp;Assign Course</a></li> -->
									<? endif;?>
									</ul>
								</div>
							</td>
						</tr>
						<?php 
						endforeach; ?>
						<?php
					}
					else
					{
					?>
					<tr>
					<td colspan="4">
						No empoyee record found.
					</td>
					</tr>
					<?php
					}
					?>
				</tbody>
			</table>
		</div>
	<?php
	}
	?>

<?echo isset($links) ? $links : NULL;?>