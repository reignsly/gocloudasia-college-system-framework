<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Alter_curriculum_add_specialization_id extends CI_Migration {

	public function up() 
	{
		if($this->db->table_exists('curriculum'))
		{
			if (!$this->db->field_exists('specialization_id', 'curriculum'))
			{
				$this->db->query("ALTER TABLE `curriculum` ADD COLUMN `specialization_id` VARCHAR(50)");
			}
		}	

		if($this->db->table_exists('subjects'))
		{
			if (!$this->db->field_exists('specialization_id', 'subjects'))
			{
				$this->db->query("ALTER TABLE `subjects` ADD COLUMN `specialization_id` VARCHAR(50)");
			}
		}	

		if($this->db->table_exists('course_blocks'))
		{
			if (!$this->db->field_exists('specialization_id', 'course_blocks'))
			{
				$this->db->query("ALTER TABLE `course_blocks` ADD COLUMN `specialization_id` VARCHAR(50)");
			}
		}	
	} 
	public function down() 
	{
		if ($this->db->field_exists('specialization_id', 'curriculum'))
		{
			$this->dbforge->drop_column('curriculum', 'specialization_id');
		}

		if ($this->db->field_exists('specialization_id', 'subjects'))
		{
			$this->dbforge->drop_column('subjects', 'specialization_id');
		}

		if ($this->db->field_exists('specialization_id', 'course_blocks'))
		{
			$this->dbforge->drop_column('course_blocks', 'specialization_id');
		}
	}
} 