<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_additional_menus_for_teacher extends CI_Migration {

	private $_table = "main_menus";
	private $_table2 = "menus";
	private $_menus = array(
				"my_students===My Students" => "insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `created_at`, `updated_at`, `remarks`) values('my_students','My Students','mystudents',NULL,'100','2',NULL,'1',NULL,'2014-09-29 16:36:00','2014-09-29 16:36:00','For Teachers, this are the list of all their students')",
				"my_subjects===My Subjects" => "insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `created_at`, `updated_at`, `remarks`) values('my_subjects','My Subjects','mysubject',NULL,'99','2',NULL,'1',NULL,'2014-09-29 16:34:16','2014-09-29 16:36:18','For Teachers, this are the list of all subjects assigned to them.')",
			);

	private $_menus2 = array(
				"my_students==My Students" => "insert into `menus` (`main_menu_id`, `department`, `controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `help`, `attr`, `created_at`, `updated_at`) values('98','teacher','my_students','My Students','41_SUB',NULL,'100','2','glyphicon glyphicon-globe','1',NULL,NULL,'2014-09-29 16:37:01','2014-09-29 16:37:01')",
				"my_subjects==My Subjects" => "insert into `menus` (`main_menu_id`, `department`, `controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `help`, `attr`, `created_at`, `updated_at`) values('97','teacher','my_subjects','My Subjects','41_SUB',NULL,'99','2','glyphicon glyphicon-globe','1',NULL,NULL,'2014-09-29 16:37:02','2014-09-29 16:37:02')",
			);

	private $_alter = array(
				"UPDATE menus SET visible = 0 WHERE controller = 'lessons' AND caption = 'Lessons'",
				"UPDATE menus SET visible = 0 WHERE controller = 'classes/classroom' AND caption = 'My Classes'",
			);

	public function up(){

		if($this->db->table_exists($this->_table)){	

			$this->load->model('M_core_model','m');
			$this->m->set_table($this->_table);

			foreach ($this->_menus as $key => $qry) {

				$key_ar = explode("===", $key);
				
				if(count($key_ar) == 2){
					$controller = trim($key_ar[0]);
					$caption = trim($key_ar[1]);

					/* CHECK MENUS IF EXISTED */
					unset($get);
					$get['where']['controller'] = $controller;
					$get['where']['caption'] = $caption;
					$get['single'] = true;
					$rs = $this->m->get_record(false, $get);
					if(!$rs){

						/* IF NONE - THEN INSERT OR RUN THE QRY*/

						$rs_qry = $this->db->query($qry);
					}
				}
			}
		}

		if($this->db->table_exists($this->_table2)){	

			$this->load->model('M_core_model','m');

			foreach ($this->_menus2 as $key => $qry) {

				$key_ar = explode("==", $key);

				if(count($key_ar) == 2){
					$controller = trim($key_ar[0]);
					$caption = trim($key_ar[1]);

					/* CHECK MENUS IF EXISTED IN MAIN MENUS TABLE */
					$this->m->set_table('main_menus');
					unset($get);
					$get['where']['controller'] = $controller;
					$get['where']['caption'] = $caption;
					$get['single'] = true;
					$rs = $this->m->get_record(false, $get);

					$this->m->set_table('menus');
					unset($get); // Check if menu already there
					$get['where']['controller'] = $controller;
					$get['where']['caption'] = $caption;
					$get['where']['department'] = "teacher";
					$get['single'] = true;
					$menu_ = $this->m->get_record(false, $get);

					if($rs && !$menu_){

						/* IF NONE - THEN INSERT OR RUN THE QRY*/
						// vd($qry);
						$this->m->set_table($this->_table2);
						$rs_qry = $this->db->query($qry);
					}
					else
					{
						// vd($qry);
						$rs_qry = $this->db->query($qry);
					}
				}
			}

			foreach ($this->_alter as $key => $qry) {
				$rs_qry = $this->db->query($qry);
			}
		}
	}

	public function down(){

		if($this->db->table_exists($this->_table)){	

			$this->load->model('M_core_model','m');
			$this->m->set_table($this->_table);

			foreach ($this->_menus as $key => $qry) {

				$key_ar = explode("===", $key);
				if(count($key_ar) == 2){
					$controller = $key_ar[0];
					$caption = $key_ar[1];

					/* CHECK MENUS IF EXISTED */
					unset($get);
					$get['where']['controller'] = $controller;
					$get['where']['caption'] = $caption;
					$get['single'] = true;
					$rs = $this->m->get_record(false, $get);
					if($rs){

						/* IF THERE WAS - THEN DELETE*/
						$rs_qry = $this->m->delete($rs->id);
					}
				}
			}
		}

		if($this->db->table_exists($this->_table2)){	

			$this->load->model('M_core_model','m');
			$this->m->set_table($this->_table2);

			foreach ($this->_menus2 as $key => $qry) {

				$key_ar = explode("==", $key);
				
				if(count($key_ar) == 2){
					$controller = trim($key_ar[0]);
					$caption = trim($key_ar[1]);

					/* CHECK MENUS IF EXISTED IN MENUS TABLE - IF TRUE DELETE */
					unset($get);
					$get['where']['controller'] = $controller;
					$get['where']['caption'] = $caption;
					$get['where']['department'] = "teacher";
					$rs = $this->m->get_record('menus', $get);
					
					if($rs){
						
						unset($get);
						$get['controller'] = $controller;
						$get['caption'] = $caption;
						$get['department'] = "teacher";

						$this->m->delete($get); 
					}
				}	
			}
			
		}
	}
}