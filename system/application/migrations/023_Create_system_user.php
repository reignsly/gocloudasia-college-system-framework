<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Migration_Create_system_user extends CI_Migration {

	public function up(){
		if(!$this->db->table_exists("system_users")){

			$this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
			$this->dbforge->add_field("`username` varchar(32) NOT NULL");
			$this->dbforge->add_field("`hashed_password` varchar(128) NOT NULL");
			$this->dbforge->add_field("`name` varchar(255) NOT NULL");
			$this->dbforge->add_field("`email` varchar(255) NOT NULL");
			$this->dbforge->add_field("`is_activated` tinyint(1) DEFAULT '1'");
			$this->dbforge->add_field("`date_activated` datetime DEFAULT NULL");
			$this->dbforge->add_field("`expiration` datetime DEFAULT NULL");
			$this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
			$this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
			$this->dbforge->add_field("`created_by` varchar(255) DEFAULT NULL");
			$this->dbforge->add_field("`remarks` varchar(255) DEFAULT NULL");
			$this->dbforge->add_field("`message` text");
			$this->dbforge->add_key('`id`', TRUE);
			$this->dbforge->create_table("system_users");
		}
	}
	public function down(){
		if($this->db->table_exists("system_users")){
			$this->dbforge->drop_table("system_users");
		}
	}
}