<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Alter_announcement_add_title extends CI_Migration {

	public function up() 
	{
		if($this->db->table_exists('announcements'))
		{
			if (!$this->db->field_exists('title', 'announcements'))
			{
				$this->db->query("ALTER TABLE `announcements` ADD COLUMN `title` VARCHAR(100)");
			}
		}	
	} 
	public function down() 
	{
		if ($this->db->field_exists('title', 'announcements'))
		{
			$this->dbforge->drop_column('announcements', 'title');
		}
	}
} 