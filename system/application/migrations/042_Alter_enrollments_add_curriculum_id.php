<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Alter_enrollments_add_curriculum_id extends CI_Migration {

	public function up() 
	{
		if($this->db->table_exists('enrollments'))
		{
			//remove unused fields
			if ($this->db->field_exists('c', 'enrollments'))
			{
				$this->db->query("ALTER TABLE `enrollments` DROP COLUMN `c`");
			}

			if ($this->db->field_exists('y', 'enrollments'))
			{
				$this->db->query("ALTER TABLE `enrollments` DROP COLUMN `y`");
			}

			if ($this->db->field_exists('s', 'enrollments'))
			{
				$this->db->query("ALTER TABLE `enrollments` DROP COLUMN `s`");
			}

			if ($this->db->field_exists('discount', 'enrollments'))
			{
				$this->db->query("ALTER TABLE `enrollments` DROP COLUMN `discount`");
			}

			if ($this->db->field_exists('less', 'enrollments'))
			{
				$this->db->query("ALTER TABLE `enrollments` DROP COLUMN `less`");
			}

			if ($this->db->field_exists('net', 'enrollments'))
			{
				$this->db->query("ALTER TABLE `enrollments` DROP COLUMN `net`");
			}

			if ($this->db->field_exists('semister', 'enrollments'))
			{
				$this->db->query("ALTER TABLE `enrollments` DROP COLUMN `semister`");
			}

			// add curriculum id
			if (!$this->db->field_exists('curriculum_id', 'enrollments'))
			{
				$this->db->query("ALTER TABLE `enrollments` ADD COLUMN `curriculum_id` VARCHAR(50)");
			}
		}	
	} 
	public function down() 
	{
		if ($this->db->field_exists('curriculum_id', 'enrollments'))
		{
			$this->dbforge->drop_column('enrollments', 'curriculum_id');
		}
	}
} 