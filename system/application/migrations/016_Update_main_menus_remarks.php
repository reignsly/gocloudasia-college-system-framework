<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_main_menus_remarks extends CI_Migration {

	public function up() 
	{
		if($this->db->table_exists('main_menus'))
		{
			if ($this->db->field_exists('remarks', 'main_menus'))
			{
				$this->db->query("ALTER TABLE `main_menus` CHANGE `remarks` `remarks` TEXT CHARSET latin1 COLLATE latin1_swedish_ci NULL");
			}
		}	
	} 
	public function down() 
	{
		return true;
	}
}