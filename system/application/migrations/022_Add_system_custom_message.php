<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_system_custom_message extends CI_Migration {
	
	public function up(){
		if(!$this->db->table_exists("system_custom_message")){
			$this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
			$this->dbforge->add_field("`type` varchar(100) NOT NULL");
			$this->dbforge->add_field("`message` text NOT NULL");
			$this->dbforge->add_field("`desc` varchar(255) DEFAULT NULL");
			$this->dbforge->add_key('`id`', TRUE);
			$this->dbforge->create_table("system_custom_message");
		}
	}
	public function down(){
		if($this->db->table_exists("system_custom_message")){
			$this->dbforge->drop_table("system_custom_message");
		}
	}
}