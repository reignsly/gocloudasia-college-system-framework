<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Insert_default_departments extends CI_Migration {

	/**
	 * Create a data
	 */

	private $_table = "departments";
	private $_departments = array(
			"president" =>	"insert into `departments` (`department`, `description`, `visible`, `ord`, `controller`, `icon`, `level`, `created_at`, `stud_profile`, `stud_fees`, `stud_grade`, `stud_issues`, `stud_otr`) values('president','President','1','1','login','assets/images/president.png','1',NULL,'0','0','0','0','0')",
			"admin" =>	"insert into `departments` (`department`, `description`, `visible`, `ord`, `controller`, `icon`, `level`, `created_at`, `stud_profile`, `stud_fees`, `stud_grade`, `stud_issues`, `stud_otr`) values('admin','System Administrator','1','3','login','assets/images/administrator.png','1',NULL,'0','0','0','0','0')",
			"dean" =>	"insert into `departments` (`department`, `description`, `visible`, `ord`, `controller`, `icon`, `level`, `created_at`, `stud_profile`, `stud_fees`, `stud_grade`, `stud_issues`, `stud_otr`) values('dean','Dean','1','4','login','assets/images/dean.png','4',NULL,'0','0','0','0','0')",
			"school_administrator" =>	"insert into `departments` (`department`, `description`, `visible`, `ord`, `controller`, `icon`, `level`, `created_at`, `stud_profile`, `stud_fees`, `stud_grade`, `stud_issues`, `stud_otr`) values('school_administrator','School Administrator','1','2','login','assets/images/president.png','1',NULL,'0','0','0','0','0')",
			"registrar" =>	"insert into `departments` (`department`, `description`, `visible`, `ord`, `controller`, `icon`, `level`, `created_at`, `stud_profile`, `stud_fees`, `stud_grade`, `stud_issues`, `stud_otr`) values('registrar','Registar','1','5','login','assets/images/registrar.png','4',NULL,'1','0','1','1','1')",
			"finance" =>	"insert into `departments` (`department`, `description`, `visible`, `ord`, `controller`, `icon`, `level`, `created_at`, `stud_profile`, `stud_fees`, `stud_grade`, `stud_issues`, `stud_otr`) values('finance','Finance','1','6','login','assets/images/financer.png','4',NULL,'1','1','0','1','0')",
			"hrd" =>	"insert into `departments` (`department`, `description`, `visible`, `ord`, `controller`, `icon`, `level`, `created_at`, `stud_profile`, `stud_fees`, `stud_grade`, `stud_issues`, `stud_otr`) values('hrd','Human Resource','0','7','login','assets/images/hrd.png','4',NULL,'0','0','0','0','0')",
			"teacher" =>	"insert into `departments` (`department`, `description`, `visible`, `ord`, `controller`, `icon`, `level`, `created_at`, `stud_profile`, `stud_fees`, `stud_grade`, `stud_issues`, `stud_otr`) values('teacher','Teacher','1','8','login','assets/images/teacher.png','4',NULL,'0','0','0','0','0')",
			"librarian" =>	"insert into `departments` (`department`, `description`, `visible`, `ord`, `controller`, `icon`, `level`, `created_at`, `stud_profile`, `stud_fees`, `stud_grade`, `stud_issues`, `stud_otr`) values('librarian','Library','1','9','login','assets/images/library.png','4',NULL,'0','0','0','0','0')",
			"custodian" =>	"insert into `departments` (`department`, `description`, `visible`, `ord`, `controller`, `icon`, `level`, `created_at`, `stud_profile`, `stud_fees`, `stud_grade`, `stud_issues`, `stud_otr`) values('custodian','Custodian','0','10','login','assets/images/custodian.jpg','4',NULL,'0','0','0','0','0')",
			"cashier" =>	"insert into `departments` (`department`, `description`, `visible`, `ord`, `controller`, `icon`, `level`, `created_at`, `stud_profile`, `stud_fees`, `stud_grade`, `stud_issues`, `stud_otr`) values('cashier','Cashier','1','11','login','assets/images/cashier.png','4',NULL,'1','1','0','1','0')",
			"guidance" =>	"insert into `departments` (`department`, `description`, `visible`, `ord`, `controller`, `icon`, `level`, `created_at`, `stud_profile`, `stud_fees`, `stud_grade`, `stud_issues`, `stud_otr`) values('guidance','Guidance','1','12','login','assets/images/guidance.png','4',NULL,'0','0','0','0','0')",
			"student" =>	"insert into `departments` (`department`, `description`, `visible`, `ord`, `controller`, `icon`, `level`, `created_at`, `stud_profile`, `stud_fees`, `stud_grade`, `stud_issues`, `stud_otr`) values('student','Student','0','13','login','assets/images/student.png',NULL,NULL,'0','0','0','0','0')",
			"student_affairs" =>	"insert into `departments` (`department`, `description`, `visible`, `ord`, `controller`, `icon`, `level`, `created_at`, `stud_profile`, `stud_fees`, `stud_grade`, `stud_issues`, `stud_otr`) values('student_affairs','Student Affairs','1','14','login','assets/images/hrd.png','4',NULL,'0','0','0','0','0')",
		);

	public function up(){

		if($this->db->table_exists($this->_table)){	

			$this->load->model('M_core_model','m');
			$this->m->set_table($this->_table);

			foreach ($this->_departments as $dep => $val) {

				unset($get);
				$get['where']['department'] = trim($dep);
				$get['single'] = true;

				$department = $this->m->get_record(false, $get);
				
				if(!$department){

					$this->db->query($val);
				}
			}
		}
	}

	public function down(){

		if($this->db->table_exists($this->_table)){	

			$this->load->model('M_core_model','m');
			$this->m->set_table($this->_table);

			foreach ($this->_departments as $dep => $val) {

				unset($get);
				$get['where']['department'] = trim($dep);
				$get['single'] = true;

				$department = $this->m->get_record(false, $get);
				
				if($department){

					$this->m->delete(array('department'=>$dep));
				}
			}
		}
	}
}