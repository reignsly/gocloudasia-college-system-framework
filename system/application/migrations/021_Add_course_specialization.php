<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_course_specialization extends CI_Migration {
	
	public function up(){
		if(!$this->db->table_exists("course_specialization")){
			$this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
			$this->dbforge->add_field("`course_id` bigint(20) NOT NULL");
			$this->dbforge->add_field("`specialization` varchar(255) DEFAULT NULL");
			$this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
			$this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
			$this->dbforge->add_key('`id`', TRUE);
			$this->dbforge->create_table("course_specialization");
		}
	}
	public function down(){
		if($this->db->table_exists("course_specialization")){
			$this->dbforge->drop_table("course_specialization");
		}
	}
}