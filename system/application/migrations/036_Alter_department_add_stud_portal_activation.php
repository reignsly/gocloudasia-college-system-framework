<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Alter_department_add_stud_portal_activation extends CI_Migration {

	public function up() 
	{
		if($this->db->table_exists('departments'))
		{
			if (!$this->db->field_exists('stud_portal_activation', 'departments'))
			{
				$this->db->query("ALTER TABLE `departments` ADD COLUMN `stud_portal_activation` TINYINT(1) DEFAULT 0 COMMENT 'Access To Student Portal'");
			}
		}	

		if($this->db->table_exists('package_departments'))
		{
			if (!$this->db->field_exists('stud_portal_activation', 'package_departments'))
			{
				$this->db->query("ALTER TABLE `package_departments` ADD COLUMN `stud_portal_activation` TINYINT(1) DEFAULT 0 COMMENT 'Access To Student Portal'");
			}
		}	
	} 
	public function down() 
	{
		if ($this->db->field_exists('stud_portal_activation', 'departments'))
		{
			$this->dbforge->drop_column('departments', 'stud_portal_activation');
		}

		if ($this->db->field_exists('stud_portal_activation', 'package_departments'))
		{
			$this->dbforge->drop_column('package_departments', 'stud_portal_activation');
		}
	}
} 