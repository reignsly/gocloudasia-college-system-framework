<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_enrollments_add_archived extends CI_Migration {

	public function up() 
	{
		if($this->db->table_exists('enrollments'))
		{
			if (!$this->db->field_exists('archived', 'enrollments'))
			{
				$this->db->query("ALTER TABLE `enrollments` ADD COLUMN `archived` tinyint(1) DEFAULT 0");
			}
		}	
	} 
	public function down() 
	{
		if ($this->db->field_exists('archived', 'enrollments'))
		{
			$this->dbforge->drop_column('enrollments', 'archived');
		}
	}
} 