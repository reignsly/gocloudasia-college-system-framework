<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_package_departments_table extends CI_Migration {
	private $_table = "package_departments";
	public function up(){
		if($this->db->table_exists("package_departments")){
			$fields = array(
        'stud_profile' => array(
					'type' => 'INT',
					'constraint' => '1',
					'default' => '0'
				),
			);
			if (!$this->db->field_exists('stud_profile', $this->_table)){ $this->dbforge->add_column($this->_table, $fields);}
			$fields = array(
        'stud_fees' => array(
					'type' => 'INT',
					'constraint' => '1',
					'default' => '0'
				),
			);
			if (!$this->db->field_exists('stud_fees', $this->_table)){ $this->dbforge->add_column($this->_table, $fields);}
			$fields = array(
        'stud_grade' => array(
					'type' => 'INT',
					'constraint' => '1',
					'default' => '0'
				),
			);
			if (!$this->db->field_exists('stud_grade', $this->_table)){ $this->dbforge->add_column($this->_table, $fields);}
			$fields = array(
        'stud_issues' => array(
					'type' => 'INT',
					'constraint' => '1',
					'default' => '0'
				),
			);
			if (!$this->db->field_exists('stud_issues', $this->_table)){ $this->dbforge->add_column($this->_table, $fields);}
			$fields = array(
        'stud_otr' => array(
					'type' => 'INT',
					'constraint' => '1',
					'default' => '0'
				),
			);
			if (!$this->db->field_exists('stud_otr', $this->_table)){ $this->dbforge->add_column($this->_table, $fields);}
		
		}
	}
	public function down(){
		if($this->db->table_exists($this->_table))
		{
			if ($this->db->field_exists('stud_profile', $this->_table))
			{
				$this->dbforge->drop_column($this->_table, 'stud_profile');
			}
			if ($this->db->field_exists('stud_fees', $this->_table))
			{
				$this->dbforge->drop_column($this->_table, 'stud_fees');
			}
			if ($this->db->field_exists('stud_grade', $this->_table))
			{
				$this->dbforge->drop_column($this->_table, 'stud_grade');
			}
			if ($this->db->field_exists('stud_issues', $this->_table))
			{
				$this->dbforge->drop_column($this->_table, 'stud_issues');
			}
			if ($this->db->field_exists('stud_otr', $this->_table))
			{
				$this->dbforge->drop_column($this->_table, 'stud_otr');
			}
		}	
	}
}