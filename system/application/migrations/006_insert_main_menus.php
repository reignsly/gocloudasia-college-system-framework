<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Insert_main_menus extends CI_Migration {

	/**
	 * Create a data
	 * Insert Values for Main Menus Necessary for the System Dynamic Menus
	 */
	private $_table = "main_menus";
	private $_menus = array(
				"configure/school===School Settings"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('configure/school','School Settings','0_SUB','','1','2',NULL,'1',NULL,'Includes school name, address, logo and others')",
				"events===Calendar"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('events','Calendar','0_SUB','','2','2',NULL,'1',NULL,'Master File of Calendar or Events')",
				"years===Year"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('years','Year','1_SUB','','5','2',NULL,'1',NULL,'Master File of Year')",
				"semesters===Semester"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('semesters','Semester','1_SUB','','6','2',NULL,'1',NULL,'Master File of Semester')",
				"courses===Course"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('courses','Course','1_SUB','','7','2',NULL,'1',NULL,'Master File of Course')",
				"academic_years===Academic Year"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('academic_years','Academic Year','1_SUB','','9','2',NULL,'1',NULL,'Master File of Academic Year')",
				"grading_periods===Grading Period"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('grading_periods','Grading Period','1_SUB','','10','2',NULL,'1',NULL,'Master File of Grading Period')",
				"open_semester===Open Semester"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('open_semester','Open Semester','1_SUB','','11','2',NULL,'1',NULL,'Sets the current semester for the system')",
				"block_section_settings===Block Section Settings"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('block_section_settings','Block Section Settings','1_SUB','','12','2',NULL,'1',NULL,'Master File of Block Sections')",
				"remarks===Remarks"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('remarks','Remarks','3_SUB','','17','2',NULL,'1',NULL,'Master File of Remarks')",
				"fees===Fee Creator"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('fees','Fee Creator','3_SUB','','18','2',NULL,'1',NULL,'Master File of Fees')",
				"coursefinances===Course Fee Management"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('coursefinances','Course Fee Management','3_SUB','','19','2',NULL,'1',NULL,'Settings of Fees, Course Finance Master File')",
				"remaining_balance_percentages===Required Payment Percentage"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('remaining_balance_percentages','Required Payment Percentage','3_SUB','','20','2',NULL,'1',NULL,'')",
				"permit_settings===Grade Slip"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('permit_settings','Grade Slip','4_SUB','','22','2',NULL,'1',NULL,'Settings of Grade Slip Report')",
				"employeecategories===Employee Category"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('employeecategories','Employee Category','5_SUB','','25','2',NULL,'1',NULL,'Master File for Employee Category')",
				"open_enrollment===Open Enrollment"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('open_enrollment','Open Enrollment','6_SUB','','27','2',NULL,'1',NULL,'Opens or Closes enrollment for the current Semester')",
				"search/search_student===Officially Enrolled"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('search/search_student','Officially Enrolled','8_SUB','','29','2',NULL,'1',NULL,'Student Search, contains actions like profile, fees and others')",
				"search/search_enrollee===Enrollees"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('search/search_enrollee','Enrollees','8_SUB','','30','2',NULL,'1',NULL,'Search for enrollees who is currently UNPAID')",
				"search/list_students/paid===List of Paid Students"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('search/list_students/paid','List of Paid Students','8_SUB','','32','2',NULL,'1',NULL,'Shows student with PAID Accounts')",
				"search/list_students/unpaid===Unpaid Accounts"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('search/list_students/unpaid','Unpaid Accounts','8_SUB','','33','2',NULL,'1',NULL,'Shows student with UNPAID Accounts')",
				"search/list_students/fullpaid===Fully Paid Students"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('search/list_students/fullpaid','Fully Paid Students','8_SUB','','34','2',NULL,'1',NULL,'Shows student with FULLY PAID Accounts')",
				"search/list_students/partialpaid===Partial Paid Students"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('search/list_students/partialpaid','Partial Paid Students','8_SUB','','35','2',NULL,'1',NULL,'Shows student with PARTIAL PAID Accounts')",
				"group_payments===Daily Collection"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('group_payments','Daily Collection','8_SUB','','36','2',NULL,'1',NULL,'Report for daily collection or payments')",
				"search/master_list_by_course===Master list by course"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('search/master_list_by_course','Master list by course','8_SUB','','37','2',NULL,'1',NULL,NULL)",
				"inventory===Inventory"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('inventory','Inventory','11_SUB','','50','2',NULL,'1',NULL,'')",
				"borrow_items/create===Borrow Items"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('borrow_items/create','Borrow Items','11_SUB','','51','2',NULL,'1',NULL,NULL)",
				"borrow_items/records===Records"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('borrow_items/records','Records','11_SUB','','52','2',NULL,'1',NULL,'Records of Borrowed Items (Library)')",
				"announcements===Announcements"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('announcements','Announcements','14_SUB','','56','2',NULL,'1',NULL,'Master File for Announcements')",
				"enrollees/daily_enrollees===Daily Enrollees"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('enrollees/daily_enrollees','Daily Enrollees','15_SUB','','61','2',NULL,'1',NULL,'Show daily enrolless')",
				"subjects===Subject"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('subjects','Subject','16_SUB','','63','2',NULL,'1',NULL,'Master File of Subject')",
				"fees/student_charges===Student Charges"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('fees/student_charges','Student Charges','18_SUB','','66','2',NULL,'1',NULL,'Show the record of student charges')",
				"excels/student_charges===Download Student Charges Excel"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('excels/student_charges','Download Student Charges Excel','18_SUB','','67','2',NULL,'1',NULL,'Download for student charges')",
				"payments/student_payments===Student Payments"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('payments/student_payments','Student Payments','18_SUB','','68','2',NULL,'1',NULL,'Show the record of student payments')",
				"excels/student_payments===Download Student Payments Excel"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('excels/student_payments','Download Student Payments Excel','18_SUB','','69','2',NULL,'1',NULL,'Download for Student payments')",
				"fees/student_deductions===Student Deductions"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('fees/student_deductions','Student Deductions','18_SUB','','70','2',NULL,'1',NULL,'')",
				"excels/student_deductions===Download Student Deductions"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('excels/student_deductions','Download Student Deductions','18_SUB','','71','2',NULL,'1',NULL,NULL)",
				"payments/remaining_balance_report===Remaining Balance Report"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('payments/remaining_balance_report','Remaining Balance Report','18_SUB','','72','2',NULL,'1',NULL,NULL)",
				"payments/download_remaining_balance_report===Download Remaining Balance Report"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('payments/download_remaining_balance_report','Download Remaining Balance Report','18_SUB','','73','2',NULL,'1',NULL,NULL)",
				"excels/download_all_student_profile===Download All Student Profile"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('excels/download_all_student_profile','Download All Student Profile','18_SUB','','74','2',NULL,'1',NULL,NULL)",
				"exam_permit===Generate Exam Permit"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('exam_permit','Generate Exam Permit','18_SUB','','75','2',NULL,'1',NULL,'Generation of Exam Permit')",
				"admission_slips===Issue Admission Slip"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('admission_slips','Issue Admission Slip','21_SUB','','90','2',NULL,'1',NULL,'')",
				"employees===Search Employee"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('employees','Search Employee','23_SUB','','94','2',NULL,'1',NULL,NULL)",
				"employees/create===Add Employee"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('employees/create','Add Employee','23_SUB','','95','2',NULL,'1',NULL,NULL)",
				"employee_attendance/calendar===Employee Attendance"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('employee_attendance/calendar','Employee Attendance','23_SUB','','96','2',NULL,'1',NULL,NULL)",
				"book_category===Category"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('book_category','Category','26_SUB','','101','2',NULL,'1',NULL,'Master file book category')",
				"books===Books"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('books','Books','26_SUB','','102','2',NULL,'1',NULL,'Master file of Books')",
				"total_income===Total Income"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('total_income','Total Income','29_SUB','','107','2',NULL,'1',NULL,'Report for total income')",
				"student_scholar===Total Scholars"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('student_scholar','Total Scholars','29_SUB','','108','2',NULL,'1',NULL,'Report for scholars')",
				"subject_grades===Subject Grades"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('subject_grades','Subject Grades','31_SUB','','116','2',NULL,'1',NULL,NULL)",
				"generate_grade_slip===Generate Grade Slip"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('generate_grade_slip','Generate Grade Slip','31_SUB','','117','2',NULL,'1',NULL,'Generation of grade slip')",
				"enrollees/dropped_students===Dropped Students"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('enrollees/dropped_students','Dropped Students','31_SUB','','119','2',NULL,'1',NULL,NULL)",
				"confirm_enrollees===Confirm Enrollees"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('confirm_enrollees','Confirm Enrollees','31_SUB','','120','2',NULL,'1',NULL,'Enrollment confirmation')",
				"ched===CHED"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('ched','CHED','32_SUB','','123','2',NULL,'1',NULL,'Report for CHED')",
				"tesda/tesda_by_course===TESDA by Course"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('tesda/tesda_by_course','TESDA by Course','33_SUB','','126','2',NULL,'1',NULL,'Report for TESDA')",
				"students_geographical_statistic===Students geographical statistics"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('students_geographical_statistic','Students geographical statistics','35_SUB','','132','2',NULL,'1',NULL,NULL)",
				"international_student===International Students"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('international_student','International Students','35_SUB','','133','2',NULL,'1',NULL,NULL)",
				"enrollees/nstp_students===NSTP Data List"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('enrollees/nstp_students','NSTP Data List','35_SUB','','134','2',NULL,'1',NULL,NULL)",
				"messages===Message"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('messages','Message','37_SUB','','137','2',NULL,'1',NULL,NULL)",
				"section_offering===Section Offering"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('section_offering','Section Offering','37_SUB','','138','2',NULL,'1',NULL,NULL)",
				"registration===Registration"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('registration','Registration','37_SUB','','139','2',NULL,'1',NULL,'Student Registration Approval')",
				"profile===Profile"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('profile','Profile','37_SUB','','140','2',NULL,'1',NULL,'Student Profile')",
				"schedule===Schedule"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('schedule','Schedule','37_SUB','','141','2',NULL,'1',NULL,NULL)",
				"grade===Grades"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('grade','Grades','37_SUB','','142','2',NULL,'1',NULL,NULL)",
				"student/search_schedule===Student Schedule"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('student/search_schedule','Student Schedule','39_SUB','','147','2',NULL,'1',NULL,NULL)",
				"student/soa===Generate SOA"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('student/soa','Generate SOA','39_SUB','','148','2',NULL,'1',NULL,'Generation of Student of Account')",
				"student/promisory===Student Promisory"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('student/promisory','Student Promisory','39_SUB','','150','2',NULL,'1',NULL,NULL)",
				"student_master_list===Student Master List"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('student_master_list','Student Master List','39_SUB','','151','2',NULL,'1',NULL,NULL)",
				"lessons===Lessons"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('lessons','Lessons','41_SUB','','153','2',NULL,'1',NULL,NULL)",
				"classes/classroom===My Classes"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('classes/classroom','My Classes','41_SUB','','154','2',NULL,'1',NULL,'Student Class (For Teachers)')",
				"payment_plan===Payment Plan"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('payment_plan','Payment Plan','41_SUB',NULL,'155','2',NULL,'1',NULL,'Payment plan ( Number of Division for student payments )')",
				"payment_type===Payment Type"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('payment_type','Payment Type','41_SUB',NULL,'156','2',NULL,'1',NULL,'Master File of Payment Type (Cash, Check etc)')",
				"users/activate_users===Activate/De-activate Employees"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('users/activate_users','Activate/De-activate Employees','user_activate',NULL,'0','2',NULL,'1',NULL,'Activates or deactivate employees in using the system.')",
				"scholarship===Scholarship Creator"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('scholarship','Scholarship Creator','42_SUB',NULL,'157','2',NULL,'1',NULL,'Scholarship Creator')",
				"rooms===View Rooms"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('rooms','View Rooms','room_00',NULL,'1','2',NULL,'1',NULL,'Create Read Update Delete of Rooms')",
				"student/reset_password===Reset Student Password"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('student/reset_password','Reset Student Password','student',NULL,'1','2',NULL,'1',NULL,'Reset Student Account Password')",
				"confirm_enrollees/lists===List of Confirm Enrollee"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('confirm_enrollees/lists','List of Confirm Enrollee','confirm_enrollee',NULL,'1','2',NULL,'1',NULL,'List of Confirm Enrollees currently enrolling')",
				"curriculum===Curriculum"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `remarks`) values('curriculum','Curriculum','curri',NULL,'1','2',NULL,'1',NULL,'Create, update and delete curicullum records.')",
			);

	public function up(){

		if($this->db->table_exists($this->_table)){	

			$this->load->model('M_core_model','m');
			$this->m->set_table($this->_table);

			foreach ($this->_menus as $key => $qry) {

				$key_ar = explode("===", $key);
				
				if(count($key_ar) == 2){
					$controller = trim($key_ar[0]);
					$caption = trim($key_ar[1]);

					/* CHECK MENUS IF EXISTED */
					unset($get);
					$get['where']['controller'] = $controller;
					$get['where']['caption'] = $caption;
					$get['single'] = true;
					$rs = $this->m->get_record(false, $get);
					if(!$rs){

						/* IF NONE - THEN INSERT OR RUN THE QRY*/

						$rs_qry = $this->db->query($qry);
					}
				}
			}
		}
	}

	public function down(){

		if($this->db->table_exists($this->_table)){	

			$this->load->model('M_core_model','m');
			$this->m->set_table($this->_table);

			foreach ($this->_menus as $key => $qry) {

				$key_ar = explode("===", $key);
				if(count($key_ar) == 2){
					$controller = $key_ar[0];
					$caption = $key_ar[1];

					/* CHECK MENUS IF EXISTED */
					unset($get);
					$get['where']['controller'] = $controller;
					$get['where']['caption'] = $caption;
					$get['single'] = true;
					$rs = $this->m->get_record(false, $get);
					if($rs){

						/* IF THERE WAS - THEN DELETE*/
						$rs_qry = $this->m->delete($rs->id);
					}
				}
			}
		}
	}
}