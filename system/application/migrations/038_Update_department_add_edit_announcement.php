<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_department_add_edit_announcement extends CI_Migration {

	public function up() 
	{
		if($this->db->table_exists('departments'))
		{
			if (!$this->db->field_exists('edit_announcement', 'departments'))
			{
				$this->db->query("ALTER TABLE `departments` ADD COLUMN `edit_announcement` TINYINT(1) DEFAULT 0");
			}
		}	

		if($this->db->table_exists('package_departments'))
		{
			if (!$this->db->field_exists('edit_announcement', 'package_departments'))
			{
				$this->db->query("ALTER TABLE `package_departments` ADD COLUMN `edit_announcement` INT(1) DEFAULT 0");
			}
		}	
	} 
	public function down() 
	{
		if ($this->db->field_exists('edit_announcement', 'departments'))
		{
			$this->dbforge->drop_column('departments', 'edit_announcement');
		}

		if ($this->db->field_exists('edit_announcement', 'package_departments'))
		{
			$this->dbforge->drop_column('package_departments', 'edit_announcement');
		}
	}
} 
