<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_grading_system extends CI_Migration {

	public function up(){

		if(!$this->db->table_exists("grading_system")){
			$this->dbforge->add_field("`id` int(10) unsigned NOT NULL AUTO_INCREMENT");
			$this->dbforge->add_field("`type` varchar(150) NOT NULL");
			$this->dbforge->add_field("`range_start` float NOT NULL");
			$this->dbforge->add_field("`range_end` float NOT NULL");
			$this->dbforge->add_field("`badge` varchar(30) NOT NULL DEFAULT ''");
			$this->dbforge->add_field("`value` varchar(30) DEFAULT NULL");
			$this->dbforge->add_field("`desc` varchar(150) DEFAULT NULL");
			$this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
			$this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
			$this->dbforge->add_field("`is_deleted` tinyint(1) DEFAULT '0'");
			$this->dbforge->add_field("`deleted_by` int(11) DEFAULT NULL");
			$this->dbforge->add_field("`deleted_date` datetime DEFAULT NULL");
			$this->dbforge->add_key('`id`', TRUE);
			$this->dbforge->create_table("grading_system");
		}
	}

	public function down(){
		
		if($this->db->table_exists("grading_system")){
			$this->dbforge->drop_table("grading_system");
		}
	}
} 