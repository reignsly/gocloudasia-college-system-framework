<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Alter_enrollment_add_major_name_ext extends CI_Migration {

	public function up(){
		
		if($this->db->table_exists("enrollments")){
			if (!$this->db->field_exists('course_specialization', 'enrollments')){
				$this->db->query("ALTER TABLE `enrollments` ADD COLUMN `course_specialization` VARCHAR(15) COMMENT 'ID of Major / Course Specialization'");
			}

			if (!$this->db->field_exists('name_ext', 'enrollments')){
				$this->db->query("ALTER TABLE `enrollments` ADD COLUMN `name_ext` VARCHAR(50) COMMENT 'Ex. Jr, I, II, III'");
			}

			if (!$this->db->field_exists('guardian_name', 'enrollments')){
				$this->db->query("ALTER TABLE `enrollments` ADD COLUMN `guardian_name` VARCHAR(150)");
			}
		}

		if($this->db->table_exists("temp_enrollments")){
			if (!$this->db->field_exists('course_specialization', 'temp_enrollments')){
				$this->db->query("ALTER TABLE `temp_enrollments` ADD COLUMN `course_specialization` VARCHAR(15) COMMENT 'ID of Major / Course Specialization'");
			}
			if (!$this->db->field_exists('name_ext', 'temp_enrollments')){
				$this->db->query("ALTER TABLE `temp_enrollments` ADD COLUMN `name_ext` VARCHAR(50) COMMENT 'Ex. Jr, I, II, III'");
			}

			if (!$this->db->field_exists('guardian_name', 'temp_enrollments')){
				$this->db->query("ALTER TABLE `temp_enrollments` ADD COLUMN `guardian_name` VARCHAR(50) COMMENT 'Ex. Jr, I, II, III'");
			}
		}
	}
	public function down(){

		if($this->db->table_exists("enrollments")){
			if ($this->db->field_exists('course_specialization', 'enrollments')){
				$this->dbforge->drop_column('enrollments', 'course_specialization');
			}
			if ($this->db->field_exists('name_ext', 'enrollments')){
				$this->dbforge->drop_column('enrollments', 'name_ext');
			}
			if ($this->db->field_exists('guardian_name', 'enrollments')){
				$this->dbforge->drop_column('enrollments', 'guardian_name');
			}
		}

		if($this->db->table_exists("temp_enrollments")){
			if ($this->db->field_exists('course_specialization', 'temp_enrollments')){
				$this->dbforge->drop_column('temp_enrollments', 'course_specialization');
			}
			if ($this->db->field_exists('name_ext', 'temp_enrollments')){
				$this->dbforge->drop_column('temp_enrollments', 'name_ext');
			}
			if ($this->db->field_exists('guardian_name', 'temp_enrollments')){
				$this->dbforge->drop_column('temp_enrollments', 'guardian_name');
			}
		}
	}
}  