<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Alter_grades_file_add_converted extends CI_Migration {

	public function up() 
	{
		if($this->db->table_exists('grades_file'))
		{
			if (!$this->db->field_exists('converted', 'grades_file'))
			{
				$this->db->query("ALTER TABLE `grades_file` ADD COLUMN `converted` VARCHAR(150) NULL COMMENT 'Converted Grade Depend on Grading System' AFTER `value`; ");
			}
		}	
	} 
	public function down() 
	{
		if ($this->db->field_exists('converted', 'grades_file'))
		{
			$this->dbforge->drop_column('grades_file', 'converted');
		}
	}
} 