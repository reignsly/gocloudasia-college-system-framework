<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_syspar_add_grading_system extends CI_Migration {

	public function up() 
	{
		if($this->db->table_exists('sys_par'))
		{
			if (!$this->db->field_exists('grading_system', 'sys_par'))
			{
				$this->db->query("ALTER TABLE `sys_par` ADD COLUMN `grading_system` VARCHAR(100) DEFAULT 'input'");
			}
		}	
	} 
	public function down() 
	{
		if ($this->db->field_exists('grading_system', 'sys_par'))
		{
			$this->dbforge->drop_column('sys_par', 'grading_system');
		}
	}
} 