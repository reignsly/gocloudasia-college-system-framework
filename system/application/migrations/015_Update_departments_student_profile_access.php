<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_departments_student_profile_access extends CI_Migration {
	private $_table = "departments";
	private $_table2 = "package_departments";
	public function up(){
		if($this->db->table_exists($this->_table)){
			$fields = array(
        'stud_edit_profile' => array(
					'type' => 'INT',
					'constraint' => '1',
					'default' => '0'
				),
			);
			if (!$this->db->field_exists('stud_edit_profile', $this->_table)){ $this->dbforge->add_column($this->_table, $fields);}
			$fields = array(
        'stud_add_fees' => array(
					'type' => 'INT',
					'constraint' => '1',
					'default' => '0'
				),
			);
			if (!$this->db->field_exists('stud_add_fees', $this->_table)){ $this->dbforge->add_column($this->_table, $fields);}
			$fields = array(
        'stud_edit_grade' => array(
					'type' => 'INT',
					'constraint' => '1',
					'default' => '0'
				),
			);
			if (!$this->db->field_exists('stud_edit_grade', $this->_table)){ $this->dbforge->add_column($this->_table, $fields);}
			$fields = array(
        'stud_add_issues' => array(
					'type' => 'INT',
					'constraint' => '1',
					'default' => '0'
				),
			);
			if (!$this->db->field_exists('stud_add_issues', $this->_table)){ $this->dbforge->add_column($this->_table, $fields);}
			$fields = array(
        'stud_edit_subject' => array(
					'type' => 'INT',
					'constraint' => '1',
					'default' => '0'
				),
			);
			if (!$this->db->field_exists('stud_edit_subject', $this->_table)){ $this->dbforge->add_column($this->_table, $fields);}
		}

		if($this->db->table_exists($this->_table2)){
			$fields = array(
        'stud_edit_profile' => array(
					'type' => 'INT',
					'constraint' => '1',
					'default' => '0'
				),
			);
			if (!$this->db->field_exists('stud_edit_profile', $this->_table2)){ $this->dbforge->add_column($this->_table2, $fields);}
			$fields = array(
        'stud_add_fees' => array(
					'type' => 'INT',
					'constraint' => '1',
					'default' => '0'
				),
			);
			if (!$this->db->field_exists('stud_add_fees', $this->_table2)){ $this->dbforge->add_column($this->_table2, $fields);}
			$fields = array(
        'stud_edit_grade' => array(
					'type' => 'INT',
					'constraint' => '1',
					'default' => '0'
				),
			);
			if (!$this->db->field_exists('stud_edit_grade', $this->_table2)){ $this->dbforge->add_column($this->_table2, $fields);}
			$fields = array(
        'stud_add_issues' => array(
					'type' => 'INT',
					'constraint' => '1',
					'default' => '0'
				),
			);
			if (!$this->db->field_exists('stud_add_issues', $this->_table2)){ $this->dbforge->add_column($this->_table2, $fields);}
			$fields = array(
        'stud_edit_subject' => array(
					'type' => 'INT',
					'constraint' => '1',
					'default' => '0'
				),
			);
			if (!$this->db->field_exists('stud_edit_subject', $this->_table2)){ $this->dbforge->add_column($this->_table2, $fields);}
		}
	}
	public function down(){
		if($this->db->table_exists($this->_table))
		{
			if ($this->db->field_exists('stud_edit_profile', $this->_table))
			{
				$this->dbforge->drop_column($this->_table, 'stud_edit_profile');
			}
			if ($this->db->field_exists('stud_add_fees', $this->_table))
			{
				$this->dbforge->drop_column($this->_table, 'stud_add_fees');
			}
			if ($this->db->field_exists('stud_edit_grade', $this->_table))
			{
				$this->dbforge->drop_column($this->_table, 'stud_edit_grade');
			}
			if ($this->db->field_exists('stud_add_issues', $this->_table))
			{
				$this->dbforge->drop_column($this->_table, 'stud_add_issues');
			}
			if ($this->db->field_exists('stud_edit_subject', $this->_table))
			{
				$this->dbforge->drop_column($this->_table, 'stud_edit_subject');
			}
		}	

		if($this->db->table_exists($this->_table2))
		{
			if ($this->db->field_exists('stud_edit_profile', $this->_table2))
			{
				$this->dbforge->drop_column($this->_table2, 'stud_edit_profile');
			}
			if ($this->db->field_exists('stud_add_fees', $this->_table2))
			{
				$this->dbforge->drop_column($this->_table2, 'stud_add_fees');
			}
			if ($this->db->field_exists('stud_edit_grade', $this->_table2))
			{
				$this->dbforge->drop_column($this->_table2, 'stud_edit_grade');
			}
			if ($this->db->field_exists('stud_add_issues', $this->_table2))
			{
				$this->dbforge->drop_column($this->_table2, 'stud_add_issues');
			}
			if ($this->db->field_exists('stud_edit_subject', $this->_table2))
			{
				$this->dbforge->drop_column($this->_table2, 'stud_edit_subject');
			}
		}	
	}
}