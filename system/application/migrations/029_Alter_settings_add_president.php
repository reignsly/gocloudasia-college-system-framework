<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Migration_Alter_settings_add_president extends CI_Migration {
	public function up(){
		
		if($this->db->table_exists('settings'))
		{
			if (!$this->db->field_exists('president', 'settings'))
			{
				$this->db->query("ALTER TABLE `settings` ADD COLUMN `president` VARCHAR(150) NULL"); 
			}
		}	
	}
	public function down(){
		if($this->db->table_exists("settings")){
			
			if ($this->db->field_exists('president', 'settings'))
			{
				$this->dbforge->drop_column('settings', 'president');
			}
		}
	}
}