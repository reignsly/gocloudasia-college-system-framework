<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_grades_files extends CI_Migration {

	public function up(){

		if(!$this->db->table_exists("grades_file")){
			$this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
			$this->dbforge->add_field("`enrollment_id` varchar(25) NOT NULL COMMENT 'Enrollment Id'");
			$this->dbforge->add_field("`ss_id` varchar(25) NOT NULL COMMENT 'Student Subject Id'");
			$this->dbforge->add_field("`gp_id` varchar(25) NOT NULL COMMENT 'Grading Period Id | if value 0'");
			$this->dbforge->add_field("`user_id` varchar(25) DEFAULT NULL COMMENT 'Teacher/User Id of The one who input'");
			$this->dbforge->add_field("`type` set('GRADE','TOTAL','REMARKS') DEFAULT NULL");
			$this->dbforge->add_field("`raw` decimal(11,2) DEFAULT NULL COMMENT 'Inputted Grade'");
			$this->dbforge->add_field("`value` decimal(11,2) DEFAULT NULL COMMENT 'Final Grade'");
			$this->dbforge->add_field("`position` int(11) DEFAULT NULL COMMENT 'Order when display'");
			$this->dbforge->add_field("`is_deleted` int(1) DEFAULT 0 COMMENT '1 when record is deleted'");
			$this->dbforge->add_field("`deleted_at` datetime DEFAULT NULL");
			$this->dbforge->add_field("`deleted_by` int(11) DEFAULT NULL COMMENT 'User Id of Users'");
			$this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
			$this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
			$this->dbforge->add_key('`id`', TRUE);
			$this->dbforge->create_table("grades_file");
			}
		}

		public function down(){
		if($this->db->table_exists("grades_file")){
			$this->dbforge->drop_table("grades_file");
		}
	}
}