<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_all_tables extends CI_Migration {

public function up(){
    if(!$this->db->table_exists("academic_years")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`year_from` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`year_to` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("academic_years");
    }
    if(!$this->db->table_exists("activity_log")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`user` varchar(50) DEFAULT NULL COMMENT 'USERID/STUDID/EMPLOYEEID'");
    $this->dbforge->add_field("`action` varchar(255) DEFAULT NULL COMMENT 'ADD/EDIT/DELETE/OTHERS'");
    $this->dbforge->add_field("`remarks` text COMMENT 'REMARKS PUT ID OF ADDED/DELETED/UPDATED/OTHER'");
    $this->dbforge->add_field("`url` varchar(255) DEFAULT NULL COMMENT 'FULL URL TO KNOW CONTROLLER & METHOD'");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("activity_log");
    }
    if(!$this->db->table_exists("additional_charges")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`value` float DEFAULT NULL");
    $this->dbforge->add_field("`remarks` text");
    $this->dbforge->add_field("`enrollment_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("additional_charges");
    }
    if(!$this->db->table_exists("admission_slips")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`inclusive_dates_of_absences` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`subject` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`reason` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`categoy` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`sao_remark` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`enrollment_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`course_and_year` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`category` varchar(255) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("admission_slips");
    }
    if(!$this->db->table_exists("announcements")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`message` text");
    $this->dbforge->add_field("`to_student` tinyint(1) DEFAULT NULL");
    $this->dbforge->add_field("`to_employee` tinyint(1) DEFAULT NULL");
    $this->dbforge->add_field("`to_all` tinyint(1) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`user_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`date` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("announcements");
    }
    if(!$this->db->table_exists("assesment_settings")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`title` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("assesment_settings");
    }
    if(!$this->db->table_exists("assign_coursefees")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`coursefinance_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`academic_year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`is_old` tinyint(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("assign_coursefees");
    }
    if(!$this->db->table_exists("assign_courses")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`employee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("assign_courses");
    }
    if(!$this->db->table_exists("assign_subjects")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`room` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`day` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`time_schedule_from` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`time_schedule_to` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`subject` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("assign_subjects");
    }
    if(!$this->db->table_exists("assign_teachers")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`room` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`day` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`time_schedule_from` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`time_schedule_to` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`teacher_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("assign_teachers");
    }
    if(!$this->db->table_exists("assignsts")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`assignsubject_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`employee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`subjecttimeschedule_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("assignsts");
    }
    if(!$this->db->table_exists("assignsubjects")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`subject_id` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`employee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`sts` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("assignsubjects");
    }
    if(!$this->db->table_exists("attendance_files")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`subject_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`employee_id` int(11) NOT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("attendance_files");
    }
    if(!$this->db->table_exists("attendances")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`enrollment_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`reason` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`attendance_file_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`employee_id` int(11) NOT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("attendances");
    }
    if(!$this->db->table_exists("block_subjects")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`subject_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`block_system_setting_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("block_subjects");
    }
    if(!$this->db->table_exists("block_system_settings")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`academic_year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("block_system_settings");
    }
    if(!$this->db->table_exists("borrowers")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`users_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`date_due` datetime DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`librarycard_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`date_return` datetime DEFAULT NULL");
    $this->dbforge->add_field("`date_borrowed` datetime DEFAULT NULL");
    $this->dbforge->add_field("`date_to_be_returned` date DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("borrowers");
    }
    if(!$this->db->table_exists("brands")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`ext` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("brands");
    }
    if(!$this->db->table_exists("captcha")){
    $this->dbforge->add_field("`captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`captcha_time` int(10) unsigned NOT NULL");
    $this->dbforge->add_field("`ip_address` varchar(16) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`word` varchar(20) NOT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`captcha_id`', TRUE);
    $this->dbforge->add_key('`word`');
    $this->dbforge->create_table("captcha");
    }
    if(!$this->db->table_exists("career_services")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`career` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`rating` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`date_of_examination` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`employee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("career_services");
    }
    if(!$this->db->table_exists("carrer_services")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`carrer` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`rating` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`date_of_examination` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("carrer_services");
    }
    if(!$this->db->table_exists("categories")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("categories");
    }
    if(!$this->db->table_exists("categoryfinances")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`employeecategory_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("categoryfinances");
    }
    if(!$this->db->table_exists("certificate_of_enrollment_settings")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`first_paragraph` text");
    $this->dbforge->add_field("`second_paragraph` text");
    $this->dbforge->add_field("`signatory` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`signatory_position` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`third_paragraph` text");
    $this->dbforge->add_field("`header` tinyint(1) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("certificate_of_enrollment_settings");
    }
    if(!$this->db->table_exists("ched_files")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`enrollment_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("ched_files");
    }
    if(!$this->db->table_exists("cheds")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`ched_file_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`subject_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`enrollment_id` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("cheds");
    }
    if(!$this->db->table_exists("conversation")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`user1_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`user2_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`deleted` smallint(1) DEFAULT '0'");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("conversation");
    }
    if(!$this->db->table_exists("course_blocks")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`academic_year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`block_system_setting_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("course_blocks");
    }
    if(!$this->db->table_exists("coursefees")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`value` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`coursefinance_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`fee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`position` int(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("coursefees");
    }
    if(!$this->db->table_exists("coursefinances")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`fees` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`category` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`category2` varchar(255) NOT NULL");
    $this->dbforge->add_field("`code` varchar(20) DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`academic_year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("coursefinances");
    }
    if(!$this->db->table_exists("courses")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`course_code` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`course` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`new_finance_category` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`old_finance_category` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`is_smaw` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`is_tesda_course` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("courses");
    }
    if(!$this->db->table_exists("credits")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`summary_of_credit_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`subject` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`value` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("credits");
    }
    if(!$this->db->table_exists("curriculum")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`name` varchar(255) NOT NULL");
    $this->dbforge->add_field("`course_id` bigint(20) NOT NULL");
    $this->dbforge->add_field("`academic_year_id` bigint(20) NOT NULL");
    $this->dbforge->add_field("`sy_from` int(11) NOT NULL");
    $this->dbforge->add_field("`sy_to` int(11) NOT NULL");
    $this->dbforge->add_field("`units` float DEFAULT NULL");
    $this->dbforge->add_field("`lec` float DEFAULT NULL");
    $this->dbforge->add_field("`lab` float DEFAULT NULL");
    $this->dbforge->add_field("`fee_amount` float DEFAULT NULL");
    $this->dbforge->add_field("`is_deleted` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`deleted_by` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`deleted_date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("curriculum");
    }
    if(!$this->db->table_exists("curriculum_subjects")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`curriculum_id` bigint(20) NOT NULL COMMENT 'Link to curriculum Table'");
    $this->dbforge->add_field("`subject_id` bigint(20) NOT NULL COMMENT 'Link to subjects table'");
    $this->dbforge->add_field("`subject_refid` varchar(25) DEFAULT NULL");
    $this->dbforge->add_field("`req_year_id` varchar(100) DEFAULT NULL COMMENT 'Student year Eq 1st Year 2nd Year'");
    $this->dbforge->add_field("`year_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` varchar(100) NOT NULL COMMENT 'Semester Eg 1st 2nd semester'");
    $this->dbforge->add_field("`total_units_req` float DEFAULT NULL COMMENT 'Required Finish Units of Students'");
    $this->dbforge->add_field("`enrollment_units_req` float DEFAULT NULL COMMENT 'Required Additional Subjects (Units)'");
    $this->dbforge->add_field("`is_deleted` tinyint(1) DEFAULT '0' COMMENT 'if deleted value is 1'");
    $this->dbforge->add_field("`deleted_by` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`deleted_date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("curriculum_subjects");
    }
    if(!$this->db->table_exists("delayed_jobs")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`priority` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`attempts` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`handler` text");
    $this->dbforge->add_field("`last_error` text");
    $this->dbforge->add_field("`run_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`locked_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`failed_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`locked_by` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("delayed_jobs");
    }
    if(!$this->db->table_exists("deleted_students")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`studid` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`year` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`semester` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`course` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`academic_year` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("deleted_students");
    }
    if(!$this->db->table_exists("demands")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`borrow_no` varchar(50) DEFAULT NULL");
    $this->dbforge->add_field("`date` date DEFAULT NULL");
    $this->dbforge->add_field("`user_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`item_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`unit` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`unit_return` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`unit_lost_return` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`unit_unreturn` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`price` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`amount` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`status` varchar(50) DEFAULT NULL COMMENT 'STATUS'");
    $this->dbforge->add_field("`deducted` int(11) DEFAULT '1'");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("demands");
    }
    if(!$this->db->table_exists("demands_return")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`demands_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`unit_return` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`date_return` datetime DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`remarks` varchar(255) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("demands_return");
    }
    if(!$this->db->table_exists("demands_return_lost")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`demands_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`unit_return` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`date_return` datetime DEFAULT NULL");
    $this->dbforge->add_field("`category` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`remarks` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("demands_return_lost");
    }
    if(!$this->db->table_exists("departments")){
    $this->dbforge->add_field("`id` int(10) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`department` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`description` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`visible` int(1) DEFAULT '1'");
    $this->dbforge->add_field("`ord` int(11) DEFAULT '1'");
    $this->dbforge->add_field("`controller` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`icon` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`level` smallint(2) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`stud_profile` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`stud_fees` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`stud_grade` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`stud_issues` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`stud_otr` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("departments");
    }
    if(!$this->db->table_exists("employeeattendances")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`forenoon` tinyint(1) NOT NULL DEFAULT '1'");
    $this->dbforge->add_field("`afternoon` tinyint(1) NOT NULL DEFAULT '1'");
    $this->dbforge->add_field("`reason` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`start_date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`end_date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`employee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`year` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`month` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`day` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`timein` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`timeout` varchar(255) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("employeeattendances");
    }
    if(!$this->db->table_exists("employeecategories")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`category` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("employeecategories");
    }
    if(!$this->db->table_exists("employees")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`employeeid` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`first_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`middle_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`last_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`joining_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`gender` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`dob` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`employeecategory_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`status` tinyint(1) NOT NULL DEFAULT '1'");
    $this->dbforge->add_field("`martial_status` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`no_children` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`father_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`mother_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`spouse_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`email` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`admin` tinyint(1) DEFAULT NULL");
    $this->dbforge->add_field("`teacher` tinyint(1) DEFAULT NULL");
    $this->dbforge->add_field("`registrar` tinyint(1) DEFAULT NULL");
    $this->dbforge->add_field("`finance` tinyint(1) DEFAULT NULL");
    $this->dbforge->add_field("`role` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`sss_id_no` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`place_of_birth` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`citizenship` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`religion` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`pagibig_no` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`philhealth_no` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`residential_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`permanent_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`driver_license_no` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`tin` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`cellphone` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`spouse_last_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`spouse_first_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`spouse_middle_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`spouse_occupation` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`spouse_employer` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`spouse_business_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`spouse_telephone` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`child1` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`child1bday` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`child2` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`child2bday` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`child3` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`child3bday` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`child4` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`child4bday` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`child5` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`child5bday` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`child6` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`child6bday` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`child7` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`child7bday` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`father_first_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`father_middle_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`father_last_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`mother_last_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`mother_first_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`mother_middle_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`elementary_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`elementary_degree` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`elementary_year` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`secondary_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`secondary_degree` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`secondary_year` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`vocational_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`vocational_degree` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`vocational_year` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`college_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`college_degree` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`college_year` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`graduate_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`graduate_degree` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`graduate_year` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`date_from1` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`date_to1` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`agency1` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`comunity_tax` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`issued_at` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`issued_on` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`community_tax` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`department` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`employee_status` varchar(255) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("employees");
    }
    if(!$this->db->table_exists("enrollment_notes")){
    $this->dbforge->add_field("`id` int(11) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`notes` text");
    $this->dbforge->add_field("`sort` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("enrollment_notes");
    }
    if(!$this->db->table_exists("enrollments")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`studid` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`fname` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`middle` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`firstname` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`lastname` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`date_of_birth` date DEFAULT NULL");
    $this->dbforge->add_field("`year` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`sy_from` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`sy_to` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`unit` varchar(255) NOT NULL");
    $this->dbforge->add_field("`semister` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`recent` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`sts` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`user_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`subject` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`status2` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`semester` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`age` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`is_drop` tinyint(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`course` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`telephone` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`disability` varchar(255) DEFAULT ''");
    $this->dbforge->add_field("`nationality` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`mobile` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`sex` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`discount` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`total` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`less` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`net` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`place_of_birth` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`civil_status` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`guardian` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`occupation` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`residence_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`business_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`education_parent` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`no_family` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`occupation_parent` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`income` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`primary` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`primary_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`intermediate` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`intermediate_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`secondary` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`secondary_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`college` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`college_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`course_completed` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`course_completed_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`fake_email` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`status` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`scholar` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`is_active` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`old_or_new` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`total_units` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`total_lab_units` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`total_lec_units` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`father_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`father_occupation` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`father_contact_no` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`mother_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`mother_occupation` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`mother_contact_no` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`parents_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`guardian_relation` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`guardian_contact_no` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`guardian_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`present_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`provincial_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`elementary` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`elementary_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`vocational` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`vocational_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`tertiary` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`tertiary_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`elementary_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`elementary_degree` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`secondary_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`secondary_degree` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`vocational_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`vocational_degree` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`tertiary_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`tertiary_degree` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`others_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`others_degree` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`others` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`others_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`studentid` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`is_activated` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`religion` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`is_paid` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`y` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`s` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`c` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`barangay` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`subject_load_deducted` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`date_of_admission` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`admission_credentials` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`date_of_graduation` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`is_scholar` tinyint(4) DEFAULT NULL");
    $this->dbforge->add_field("`province` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`municipal` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`living_with_parents` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`full_paid` int(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`partial_paid` int(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`is_graduating` tinyint(1) DEFAULT NULL");
    $this->dbforge->add_field("`is_deleted` tinyint(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`street_no` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`is_used` smallint(1) DEFAULT '0'");
    $this->dbforge->add_field("`block_system_setting_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`payment_plan_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`confirm_userid` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`confirm_date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`is_foreigner` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`coursefinance_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`tuition_fee_multiplier` float DEFAULT '0' COMMENT 'Tuition Value'");
    $this->dbforge->add_field("`other_fee` float DEFAULT '0' COMMENT 'Total Additional or Other Fee'");
    $this->dbforge->add_field("`tuition_fee` float DEFAULT '0' COMMENT 'Total Tuition Fee (tuition fee * total_units)'");
    $this->dbforge->add_field("`misc_fee` float DEFAULT '0' COMMENT 'Total Misc Fee'");
    $this->dbforge->add_field("`other_school_fee` float DEFAULT '0'");
    $this->dbforge->add_field("`lab_fee` float DEFAULT '0' COMMENT 'Total Lab Fee'");
    $this->dbforge->add_field("`nstp_fee` float DEFAULT '0' COMMENT 'Total Nstp Fee'");
    $this->dbforge->add_field("`total_plan_due` float DEFAULT '0' COMMENT 'Tuition + Misc + lab + nstp'");
    $this->dbforge->add_field("`total_deduction` float DEFAULT '0'");
    $this->dbforge->add_field("`total_scholarship` float DEFAULT '0'");
    $this->dbforge->add_field("`total_excess_amount` float DEFAULT '0' COMMENT 'Total Applied Excess Amount'");
    $this->dbforge->add_field("`total_previous_amount` float DEFAULT '0'");
    $this->dbforge->add_field("`total_amount_due` float DEFAULT '0' COMMENT 'Total of all due'");
    $this->dbforge->add_field("`total_amount_paid` float DEFAULT '0' COMMENT 'Total of amount paid'");
    $this->dbforge->add_field("`total_balance` float DEFAULT '0' COMMENT 'Current Balance'");
    $this->dbforge->add_field("`payment_status` set('UNPAID','FULLY PAID','PARTIAL PAID') DEFAULT 'UNPAID' COMMENT 'Status'");
    $this->dbforge->add_field("`recompute_by` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`recompute_date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`is_paid_as_previous` tinyint(1) DEFAULT '0' COMMENT 'When this enrollment is paid in other enrollment as student previous account'");
    $this->dbforge->add_field("`paid_by_eid` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`is_apply_as_excess` tinyint(1) DEFAULT '0' COMMENT 'When balance is negative and used as payment as excess in another enrollment'");
    $this->dbforge->add_field("`excess_payment_eid` varchar(100) DEFAULT NULL COMMENT 'Enrollment Id of the one who used the excess payment'");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("enrollments");
    }
    if(!$this->db->table_exists("events")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`start_date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`end_date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`title` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`description` text");
    $this->dbforge->add_field("`is_holiday` tinyint(1) DEFAULT NULL");
    $this->dbforge->add_field("`to_all` tinyint(1) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("events");
    }
    if(!$this->db->table_exists("examinations")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`start_date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`end_date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`user_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`employee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("examinations");
    }
    if(!$this->db->table_exists("examsubjects")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`examination_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`subject_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`min_mark` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`max_mark` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`start_time` datetime DEFAULT NULL");
    $this->dbforge->add_field("`end_time` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("examsubjects");
    }
    if(!$this->db->table_exists("fees")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`belongs_to` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`category_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`is_active` tinyint(1) DEFAULT '1'");
    $this->dbforge->add_field("`is_deduction` tinyint(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`position` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`show_in_new_student` tinyint(1) DEFAULT NULL");
    $this->dbforge->add_field("`donot_show_in_old` tinyint(1) DEFAULT NULL");
    $this->dbforge->add_field("`is_other` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`is_misc` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`is_nstp` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`is_package` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`value` decimal(13,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`is_tuition_fee` smallint(1) DEFAULT '0'");
    $this->dbforge->add_field("`is_lab` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`is_other_school` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("fees");
    }
    if(!$this->db->table_exists("feevals")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`value` float NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`fee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`enrollment_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`employeecategory_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`employee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`is_active` tinyint(1) NOT NULL DEFAULT '1'");
    $this->dbforge->add_field("`is_deduction` tinyint(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`position` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("feevals");
    }
    if(!$this->db->table_exists("gengrades")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`student_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`examination_id` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("gengrades");
    }
    if(!$this->db->table_exists("genpayslips")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`employee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`payslip` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_by` varchar(255) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("genpayslips");
    }
    if(!$this->db->table_exists("grades")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`grade` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`student_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`subject_id` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`value` decimal(10,2) DEFAULT NULL");
    $this->dbforge->add_field("`gengrade_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`examsubject_id` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("grades");
    }
    if(!$this->db->table_exists("grading_periods")){
    $this->dbforge->add_field("`id` bigint(20) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`grading_period` varchar(255) NOT NULL");
    $this->dbforge->add_field("`is_set` tinyint(4) DEFAULT NULL");
    $this->dbforge->add_field("`orders` smallint(6) DEFAULT '1'");
    $this->dbforge->add_field("`created_at` datetime NOT NULL");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("grading_periods");
    }
    if(!$this->db->table_exists("group_payments")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`start_date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`end_date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("group_payments");
    }
    if(!$this->db->table_exists("issues")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`user_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`comment` text");
    $this->dbforge->add_field("`issue_type` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`resolved` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`issued_by` varchar(255) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("issues");
    }
    if(!$this->db->table_exists("items")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`item` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL");
    $this->dbforge->add_field("`brand` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL");
    $this->dbforge->add_field("`product_id` int(11) NOT NULL");
    $this->dbforge->add_field("`kind` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL");
    $this->dbforge->add_field("`size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL");
    $this->dbforge->add_field("`unit` int(11) NOT NULL");
    $this->dbforge->add_field("`unit_left` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`price` float(11,2) DEFAULT '0.00'");
    $this->dbforge->add_field("`category` varchar(255) COLLATE utf8_unicode_ci NOT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`serial` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL");
    $this->dbforge->add_field("`purchase_price` float(11,2) DEFAULT '0.00'");
    $this->dbforge->add_field("`margin` decimal(10,0) DEFAULT NULL");
    $this->dbforge->add_field("`status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL");
    $this->dbforge->add_field("`min` float DEFAULT NULL");
    $this->dbforge->add_field("`max` float DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("items");
    }
    if(!$this->db->table_exists("jobs")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("jobs");
    }
    if(!$this->db->table_exists("kinds")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL");
    $this->dbforge->add_field("`brand_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("kinds");
    }
    if(!$this->db->table_exists("lessons")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`title` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`remarks` text");
    $this->dbforge->add_field("`subject_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`file_file_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`file_content_type` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`file_file_size` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`user_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`subject` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("lessons");
    }
    if(!$this->db->table_exists("libraries")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`title` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`author` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`date_due` datetime DEFAULT NULL");
    $this->dbforge->add_field("`borrower` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`year` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`bar_code` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`section` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`date_published` datetime DEFAULT NULL");
    $this->dbforge->add_field("`librarycategory_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`status` varchar(100) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("libraries");
    }
    if(!$this->db->table_exists("librarycards")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`library_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`date_to_be_returned` date DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("librarycards");
    }
    if(!$this->db->table_exists("librarycategory")){
    $this->dbforge->add_field("`id` int(10) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`category` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("librarycategory");
    }
    if(!$this->db->table_exists("librarysearches")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`title` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`author` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`year` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`bar_code` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("librarysearches");
    }
    if(!$this->db->table_exists("librarystatus")){
    $this->dbforge->add_field("`id` int(10) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`status` varchar(100) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("librarystatus");
    }
    if(!$this->db->table_exists("main_menus")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`controller` varchar(100) DEFAULT NULL COMMENT 'Name of Controller'");
    $this->dbforge->add_field("`caption` varchar(100) DEFAULT NULL COMMENT 'Title of Menu'");
    $this->dbforge->add_field("`menu_grp` varchar(30) DEFAULT NULL");
    $this->dbforge->add_field("`menu_sub` varchar(30) DEFAULT NULL");
    $this->dbforge->add_field("`menu_num` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`menu_lvl` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`menu_icon` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`visible` smallint(1) DEFAULT '1'");
    $this->dbforge->add_field("`attr` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`remarks` varchar(255) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("main_menus");
    }
    if(!$this->db->table_exists("main_menus_method")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`main_menu_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`method` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("main_menus_method");
    }
    if(!$this->db->table_exists("masterlists")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`year_from` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`year_to` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`count` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`count_male` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`count_female` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("masterlists");
    }
    if(!$this->db->table_exists("menus")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`main_menu_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`department` varchar(255) DEFAULT NULL COMMENT 'Name of Department'");
    $this->dbforge->add_field("`controller` varchar(100) DEFAULT NULL COMMENT 'Name of Controller'");
    $this->dbforge->add_field("`caption` varchar(100) DEFAULT NULL COMMENT 'Title of Menu'");
    $this->dbforge->add_field("`menu_grp` varchar(30) DEFAULT NULL");
    $this->dbforge->add_field("`menu_sub` varchar(30) DEFAULT NULL");
    $this->dbforge->add_field("`menu_num` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`menu_lvl` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`menu_icon` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`visible` smallint(1) DEFAULT '1'");
    $this->dbforge->add_field("`help` text");
    $this->dbforge->add_field("`attr` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("menus");
    }
    if(!$this->db->table_exists("menus_method")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`menu_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`method` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`access` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("menus_method");
    }
    if(!$this->db->table_exists("messages")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`conversation_id` bigint(11) DEFAULT NULL COMMENT 'CONVERSATION ID'");
    $this->dbforge->add_field("`sender_id` bigint(20) DEFAULT NULL COMMENT 'SENDER ID'");
    $this->dbforge->add_field("`recipient_id` bigint(20) DEFAULT NULL COMMENT 'RECIPIENT ID'");
    $this->dbforge->add_field("`message` text COMMENT 'MESSAGE CONTENT'");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`read` smallint(1) DEFAULT '0'");
    $this->dbforge->add_field("`deleted` smallint(1) DEFAULT '0'");
    $this->dbforge->add_field("`date_read` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("messages");
    }
    if(!$this->db->table_exists("migrate_student_subjects")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`tracking` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`sc_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("migrate_student_subjects");
    }
    if(!$this->db->table_exists("open_semester_employee_settings")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`user_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`employee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`open_semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("open_semester_employee_settings");
    }
    if(!$this->db->table_exists("open_semesters")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`academic_year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`use` int(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`open_enrollment` smallint(1) DEFAULT '0'");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("open_semesters");
    }
    if(!$this->db->table_exists("orders")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`sale_id` int(11) NOT NULL");
    $this->dbforge->add_field("`unit` int(255) DEFAULT NULL");
    $this->dbforge->add_field("`price` int(11) NOT NULL");
    $this->dbforge->add_field("`product` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL");
    $this->dbforge->add_field("`amount` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`deducted` int(11) NOT NULL DEFAULT '1'");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("orders");
    }
    if(!$this->db->table_exists("other_informations")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`recognition` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`organization` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`employee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("other_informations");
    }
    if(!$this->db->table_exists("otrs")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`special_order_no` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`series` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`issued_on` datetime DEFAULT NULL");
    $this->dbforge->add_field("`remarks` text");
    $this->dbforge->add_field("`date_issued` datetime DEFAULT NULL");
    $this->dbforge->add_field("`prepared_by` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`position` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`enrollment_id` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("otrs");
    }
    if(!$this->db->table_exists("payment_plan")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`name` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`division` smallint(2) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("payment_plan");
    }
    if(!$this->db->table_exists("payment_plan_req")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`payment_plan_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`number` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`grading_period_id` varchar(20) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("payment_plan_req");
    }
    if(!$this->db->table_exists("payment_type")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`payment_type` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("payment_type");
    }
    if(!$this->db->table_exists("payments_breakdown")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`enrollment_id` bigint(20) NOT NULL");
    $this->dbforge->add_field("`spr_id` bigint(20) NOT NULL");
    $this->dbforge->add_field("`table_id` varchar(100) NOT NULL COMMENT 'student_plan_modes id, student_enrollment_fees id , student_previous_account id'");
    $this->dbforge->add_field("`fee_id` varchar(100) DEFAULT NULL COMMENT 'if type is other fee'");
    $this->dbforge->add_field("`payment_type` varchar(100) NOT NULL");
    $this->dbforge->add_field("`fee_type` set('TUITION','OTHER','PREVIOUS') DEFAULT NULL");
    $this->dbforge->add_field("`amount` float DEFAULT NULL COMMENT 'Amount Applied in the Fee'");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`is_deleted` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`delete_remarks` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`delete_date` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("payments_breakdown");
    }
    if(!$this->db->table_exists("payslips")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`value` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`fee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`employee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`is_deduction` tinyint(1) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`genpayslip_id` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("payslips");
    }
    if(!$this->db->table_exists("permit_settings")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("permit_settings");
    }
    if(!$this->db->table_exists("photos")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`user_id` int(255) NOT NULL");
    $this->dbforge->add_field("`description` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`image_file_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`image_content_type` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`image_file_size` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`image_updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("photos");
    }
    if(!$this->db->table_exists("products")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`product` varchar(255) NOT NULL");
    $this->dbforge->add_field("`min` int(11) NOT NULL");
    $this->dbforge->add_field("`max` int(11) NOT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("products");
    }
    if(!$this->db->table_exists("profiles")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`enrollment_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`last_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`first_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`middle_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`full_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`gender` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`civil_status` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`date_of_birth` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`place_of_birth` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`age` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`disability` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`nationality` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`religion` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`mobile` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`email` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`present_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`father_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`father_occupation` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`father_contact_no` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`mother_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`mother_occupation` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`mother_contact_no` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`parents_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`guardian_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`guardian_relation` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`guardian_contact_no` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`guardian_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`elementary` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`elementary_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`elementary_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`secondary` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`secondary_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`secondary_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`vocational` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`vocational_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`vocational_degree` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`vocational_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`tertiary` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`tertiary_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`tertiary_degree` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`tertiary_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`others` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`others_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`others_degree` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`others_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`provincial_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`user_id` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("profiles");
    }
    if(!$this->db->table_exists("province")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`province` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("province");
    }
    if(!$this->db->table_exists("refunds")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`value` float DEFAULT NULL");
    $this->dbforge->add_field("`enrollment_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("refunds");
    }
    if(!$this->db->table_exists("registrars")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`login` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`name` varchar(100) DEFAULT ''");
    $this->dbforge->add_field("`email` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`crypted_password` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`salt` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`remember_token` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`remember_token_expires_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`activation_code` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`activated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("registrars");
    }
    if(!$this->db->table_exists("remaining_balance_percentages")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`percentage` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`float_percentage` float(11,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`semester_id` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("remaining_balance_percentages");
    }
    if(!$this->db->table_exists("remarks")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`value` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`is_deduction` tinyint(1) DEFAULT NULL");
    $this->dbforge->add_field("`is_payment` tinyint(1) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("remarks");
    }
    if(!$this->db->table_exists("roles")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("roles");
    }
    if(!$this->db->table_exists("roles_users")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`user_id` int(11) NOT NULL");
    $this->dbforge->add_field("`role_id` int(11) NOT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("roles_users");
    }
    if(!$this->db->table_exists("room_schedules")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`room_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`time_schedule_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("room_schedules");
    }
    if(!$this->db->table_exists("rooms")){
    $this->dbforge->add_field("`id` bigint(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`name` varchar(128) NOT NULL");
    $this->dbforge->add_field("`description` varchar(128) NOT NULL");
    $this->dbforge->add_field("`academic_year_id` varchar(50) DEFAULT NULL");
    $this->dbforge->add_field("`section_assigned` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("rooms");
    }
    if(!$this->db->table_exists("sales")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`from_warehouse` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`code` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`customer` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`cash` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`change` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`casher` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`total` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`grand_total` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("sales");
    }
    if(!$this->db->table_exists("schema_migrations")){
    $this->dbforge->add_field("`version` varchar(255) NOT NULL");
    $this->dbforge->create_table("schema_migrations");
    }
    if(!$this->db->table_exists("searchsubjects")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`subject` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("searchsubjects");
    }
    if(!$this->db->table_exists("semesters")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`level` int(5) DEFAULT '1'");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("semesters");
    }
    if(!$this->db->table_exists("semisters")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("semisters");
    }
    if(!$this->db->table_exists("serialnumbers")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`serial` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`generate_type` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("serialnumbers");
    }
    if(!$this->db->table_exists("settings")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`school_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`school_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`school_telephone` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`email` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`logo` varchar(255) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("settings");
    }
    if(!$this->db->table_exists("student_attendance")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`enrollment_id` bigint(20) NOT NULL");
    $this->dbforge->add_field("`number` int(11) DEFAULT '1'");
    $this->dbforge->add_field("`month` varchar(120) DEFAULT NULL");
    $this->dbforge->add_field("`total_school_days` float DEFAULT '0'");
    $this->dbforge->add_field("`total_school_day_present` float DEFAULT '0'");
    $this->dbforge->add_field("`total_times_tardy` float DEFAULT '0'");
    $this->dbforge->add_field("`user_id` varchar(20) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("student_attendance");
    }
    if(!$this->db->table_exists("student_deductions")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`category` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`amount` float DEFAULT NULL");
    $this->dbforge->add_field("`remarks` text");
    $this->dbforge->add_field("`enrollment_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("student_deductions");
    }
    if(!$this->db->table_exists("student_deductions_record")){
    $this->dbforge->add_field("`id` bigint(10) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`amount` decimal(10,0) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`remarks` varchar(255) NOT NULL DEFAULT ''");
    $this->dbforge->add_field("`enrollment_id` int(10) unsigned NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`user_trans_id` int(10) unsigned NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`schoolyear_id` int(10) unsigned NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`deduction_name` varchar(45) NOT NULL DEFAULT ''");
    $this->dbforge->add_field("`academic_year_id` int(11) NOT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("student_deductions_record");
    }
    if(!$this->db->table_exists("student_enrollment_fees")){
    $this->dbforge->add_field("`sef_id` int(10) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`sef_enrollment_id` int(10) unsigned NOT NULL");
    $this->dbforge->add_field("`sef_fee_name` varchar(255) NOT NULL");
    $this->dbforge->add_field("`sef_fee_rate` float NOT NULL");
    $this->dbforge->add_field("`sef_gperiod_id` int(10) unsigned NOT NULL");
    $this->dbforge->add_field("`sef_schoolyear_id` int(10) unsigned NOT NULL");
    $this->dbforge->add_field("`fee_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`is_tuition_fee` tinyint(1) unsigned NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`is_misc_fee` tinyint(1) unsigned NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`is_other_fee` tinyint(1) unsigned NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`is_other_school` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`is_lab_fee` tinyint(4) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`is_nstp_fee` tinyint(4) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`amount_paid` float DEFAULT '0'");
    $this->dbforge->add_field("`is_paid` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`spr_id` bigint(20) DEFAULT NULL COMMENT 'Student Payment Record ID'");
    $this->dbforge->add_field("`is_check` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`check_applied` float DEFAULT '0'");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`is_added` tinyint(1) DEFAULT '0' COMMENT 'If fee is added by Registrar/finance'");
    $this->dbforge->add_field("`added_by` varchar(100) DEFAULT NULL COMMENT 'User Id of register/finance who add this fee'");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`recompute_by` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`recompute_date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`is_deduction_paid` tinyint(1) DEFAULT '0' COMMENT 'If paid by deduction/scholarship/excess'");
    $this->dbforge->add_field("`is_deleted` tinyint(1) DEFAULT '0' COMMENT 'When deleted value is 1'");
    $this->dbforge->add_field("`deleted_by` bigint(20) DEFAULT NULL COMMENT 'User Id of the one who delete'");
    $this->dbforge->add_field("`delete_date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`delete_remarks` varchar(255) DEFAULT NULL COMMENT 'Remarks When deleted'");
    $this->dbforge->add_key('`sef_id`', TRUE);
    $this->dbforge->create_table("student_enrollment_fees");
    }
    if(!$this->db->table_exists("student_finance_categories")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`enrollment_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`coursefinance_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`nstp` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("student_finance_categories");
    }
    if(!$this->db->table_exists("student_grade_categories")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`student_grade_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`category` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`value` float DEFAULT NULL");
    $this->dbforge->add_field("`editable` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`user_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("student_grade_categories");
    }
    if(!$this->db->table_exists("student_grade_files")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`user_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`enrollment_id` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("student_grade_files");
    }
    if(!$this->db->table_exists("student_grades")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`student_grade_file_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`value` float DEFAULT NULL");
    $this->dbforge->add_field("`category` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`subject_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`subjectid` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`remarks` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`studentsubject_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`enrollment_id` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("student_grades");
    }
    if(!$this->db->table_exists("student_payment_breakdown")){
    $this->dbforge->add_field("`spb_id` int(10) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`spb_fee_name` varchar(45) NOT NULL DEFAULT ''");
    $this->dbforge->add_field("`spb_fee_rate` decimal(10,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`spb_amount_paid` decimal(10,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`spb_remaining_balance` decimal(10,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`spb_is_tuition_fee` tinyint(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`spb_payment_number` int(10) unsigned NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`spb_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'");
    $this->dbforge->add_field("`spb_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'");
    $this->dbforge->add_field("`spb_transacted_by` int(10) unsigned NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`spb_spr_id` int(10) unsigned NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`spb_stud_enrollment_id` int(10) unsigned NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`spb_is_misc_fee` tinyint(1) unsigned NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`spb_is_other_fee` tinyint(1) unsigned NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`spb_sef_id` int(10) unsigned NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`spb_is_elective_fee` tinyint(4) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`spb_is_uniform_fee` tinyint(4) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`spb_sy_id` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`spb_is_cca_fee` tinyint(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_key('`spb_id`', TRUE);
    $this->dbforge->create_table("student_payment_breakdown");
    }
    if(!$this->db->table_exists("student_payment_records")){
    $this->dbforge->add_field("`spr_id` int(10) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`spr_or_no` varchar(45) NOT NULL");
    $this->dbforge->add_field("`old_or_no` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`spr_remarks` text NOT NULL");
    $this->dbforge->add_field("`spr_payment_date` date NOT NULL");
    $this->dbforge->add_field("`spr_enrollment_id` int(10) unsigned NOT NULL");
    $this->dbforge->add_field("`spr_gperiod_id` int(10) unsigned NOT NULL");
    $this->dbforge->add_field("`spr_ammt_paid` decimal(10,2) NOT NULL");
    $this->dbforge->add_field("`spr_created` datetime NOT NULL");
    $this->dbforge->add_field("`spr_updated` datetime NOT NULL");
    $this->dbforge->add_field("`spr_user_trans_id` int(10) unsigned NOT NULL");
    $this->dbforge->add_field("`spr_user_trans_name` varchar(45) NOT NULL");
    $this->dbforge->add_field("`spr_schoolyear_id` int(10) unsigned NOT NULL");
    $this->dbforge->add_field("`spr_payment_number` int(10) unsigned NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`spr_is_deleted` tinyint(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`spr_date_deleted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'");
    $this->dbforge->add_field("`spr_deleted_by` int(10) unsigned NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`spr_for_the_month_of` varchar(140) DEFAULT NULL");
    $this->dbforge->add_field("`spr_mode_of_payment` varchar(150) DEFAULT NULL");
    $this->dbforge->add_field("`check_date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`check_number` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`check_confirm_date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`check_status` set('PENDING','CLEARED','DECLINED') DEFAULT 'PENDING'");
    $this->dbforge->add_field("`is_applied` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`check_confirm_userid` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`check_ids` varchar(255) DEFAULT NULL COMMENT 'HAS SUFFIX T - Tuition O - Others P - Previous'");
    $this->dbforge->add_field("`check_remarks` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`spr_id`', TRUE);
    $this->dbforge->create_table("student_payment_records");
    }
    if(!$this->db->table_exists("student_plan_modes")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`enrollment_id` bigint(20) NOT NULL");
    $this->dbforge->add_field("`payment_plan_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`number` int(2) DEFAULT NULL");
    $this->dbforge->add_field("`name` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`value` float DEFAULT NULL");
    $this->dbforge->add_field("`amount_paid` float DEFAULT NULL");
    $this->dbforge->add_field("`check_applied` float DEFAULT '0' COMMENT 'For Check Soon to Copy to Amount_Paid When Confirm'");
    $this->dbforge->add_field("`is_check` tinyint(1) DEFAULT '0' COMMENT 'When the applied Payment is Check'");
    $this->dbforge->add_field("`is_paid` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`spr_id` bigint(20) DEFAULT NULL COMMENT 'Student Payment Record ID'");
    $this->dbforge->add_field("`is_deleted` tinyint(1) DEFAULT '0' COMMENT 'When deleted value is 1'");
    $this->dbforge->add_field("`deleted_by` bigint(20) DEFAULT NULL COMMENT 'Delete by : User Id'");
    $this->dbforge->add_field("`deleted_date` datetime DEFAULT NULL COMMENT 'Date When Deleted'");
    $this->dbforge->add_field("`delete_remarks` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("student_plan_modes");
    }
    if(!$this->db->table_exists("student_previous_accounts")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`value` float DEFAULT NULL COMMENT 'Amount of the Plan'");
    $this->dbforge->add_field("`amount_paid` float DEFAULT NULL COMMENT 'Amount to be paid'");
    $this->dbforge->add_field("`is_paid` tinyint(1) DEFAULT '0' COMMENT 'Payment Status'");
    $this->dbforge->add_field("`spr_id` bigint(20) DEFAULT NULL COMMENT 'Student Payment Record ID'");
    $this->dbforge->add_field("`is_check` tinyint(1) DEFAULT NULL");
    $this->dbforge->add_field("`check_applied` float DEFAULT '0'");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`enrollment_id` int(11) DEFAULT NULL COMMENT 'Enrollment ID'");
    $this->dbforge->add_field("`prev_enrollment_eid` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`is_deduction_paid` tinyint(1) DEFAULT '0' COMMENT 'If paid by deduction/scholarship/excess'");
    $this->dbforge->add_field("`is_deleted` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`deleted_by` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`date_deleted` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("student_previous_accounts");
    }
    if(!$this->db->table_exists("student_refund")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`enrollment_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`amount` float DEFAULT NULL");
    $this->dbforge->add_field("`date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`remarks` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("student_refund");
    }
    if(!$this->db->table_exists("student_scholarship_record")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`enrollment_id` bigint(20) NOT NULL");
    $this->dbforge->add_field("`stud_e_id` int(11) NOT NULL");
    $this->dbforge->add_field("`scholarship_id` int(11) NOT NULL");
    $this->dbforge->add_field("`academic_year_id` int(11) NOT NULL");
    $this->dbforge->add_field("`scho_amount` float DEFAULT '0'");
    $this->dbforge->add_field("`tuit_per` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`misc_per` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`tuit_cash` decimal(10,2) NOT NULL");
    $this->dbforge->add_field("`misc_cash` decimal(10,2) NOT NULL");
    $this->dbforge->add_field("`scholarship_name` varchar(200) NOT NULL");
    $this->dbforge->add_field("`scholarship_desc` text NOT NULL");
    $this->dbforge->add_field("`original_tuit_rate` decimal(10,2) NOT NULL");
    $this->dbforge->add_field("`original_misc_rate` decimal(10,2) NOT NULL");
    $this->dbforge->add_field("`total_value_deducted` decimal(10,2) NOT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("student_scholarship_record");
    }
    if(!$this->db->table_exists("student_scholarships")){
    $this->dbforge->add_field("`id` int(10) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`name` varchar(45) NOT NULL DEFAULT ''");
    $this->dbforge->add_field("`desc` text NOT NULL");
    $this->dbforge->add_field("`type` set('CASH','PERCENTAGE') DEFAULT 'CASH'");
    $this->dbforge->add_field("`cash_amount` float DEFAULT '0'");
    $this->dbforge->add_field("`percentage` decimal(10,0) DEFAULT '0'");
    $this->dbforge->add_field("`applied_to` set('TUITION','MISC','TUITION & MISC') DEFAULT 'TUITION'");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("student_scholarships");
    }
    if(!$this->db->table_exists("student_subject_grades")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`enrollment_id` bigint(20) NOT NULL");
    $this->dbforge->add_field("`subject_id` bigint(20) NOT NULL");
    $this->dbforge->add_field("`gp_id` bigint(20) NOT NULL COMMENT 'Grading Period ID'");
    $this->dbforge->add_field("`value` float DEFAULT '0'");
    $this->dbforge->add_field("`letter` varchar(25) DEFAULT NULL");
    $this->dbforge->add_field("`remarks` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("student_subject_grades");
    }
    if(!$this->db->table_exists("student_total_file")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`enrollment_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`additional_charge` float DEFAULT '0'");
    $this->dbforge->add_field("`total_charge` float DEFAULT '0'");
    $this->dbforge->add_field("`prev_account` float DEFAULT '0'");
    $this->dbforge->add_field("`total_amount_due` float DEFAULT '0'");
    $this->dbforge->add_field("`total_deduction` float DEFAULT '0'");
    $this->dbforge->add_field("`less_deduction` float DEFAULT '0'");
    $this->dbforge->add_field("`total_payment` float DEFAULT '0'");
    $this->dbforge->add_field("`balance` float DEFAULT '0'");
    $this->dbforge->add_field("`status` varchar(30) DEFAULT NULL");
    $this->dbforge->add_field("`coursefinance_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`subject_units` float DEFAULT '0'");
    $this->dbforge->add_field("`total_lab` float DEFAULT '0'");
    $this->dbforge->add_field("`total_lec` float DEFAULT '0'");
    $this->dbforge->add_field("`tuition_fee` float DEFAULT '0'");
    $this->dbforge->add_field("`total_tuition_fee` float DEFAULT '0'");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("student_total_file");
    }
    if(!$this->db->table_exists("student_totals")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`enrollment_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`total_units` decimal(10,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`total_lab` decimal(10,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`tuition_fee_per_unit` decimal(8,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`lab_fee_per_unit` decimal(8,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`total_misc_fee` decimal(8,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`total_other_fee` decimal(8,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`tuition_fee` decimal(8,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`lab_fee` decimal(8,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`less_nstp` decimal(8,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`additional_charge` decimal(8,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`total_charge` decimal(8,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`previous_account` decimal(8,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`total_deduction` decimal(8,2) DEFAULT '0.00'");
    $this->dbforge->add_field("`total_amount` decimal(8,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`total_payment` decimal(8,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`remaining_balance` decimal(8,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`total_rle_units` decimal(10,1) NOT NULL DEFAULT '0.0'");
    $this->dbforge->add_field("`total_rle` decimal(10,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`rle_fee` decimal(8,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`preliminary` decimal(8,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`midterm` decimal(8,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`semi_finals` decimal(8,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`finals` decimal(8,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("student_totals");
    }
    if(!$this->db->table_exists("studentadvances")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`value` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`studentfinance_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`student_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("studentadvances");
    }
    if(!$this->db->table_exists("studentbalances")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`value` float DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("studentbalances");
    }
    if(!$this->db->table_exists("studentdownpayments")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`value` float(10,2) DEFAULT NULL");
    $this->dbforge->add_field("`studentfinance_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`student_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`date_of_payment` datetime DEFAULT NULL");
    $this->dbforge->add_field("`is_advance` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`remarks` text");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("studentdownpayments");
    }
    if(!$this->db->table_exists("studentexcess_prev_sem")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`enrollment_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`amount` float DEFAULT '0'");
    $this->dbforge->add_field("`prev_sem_eid` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`remarks` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`is_deleted` tinyint(1) DEFAULT '0' COMMENT 'when deleted value is 1'");
    $this->dbforge->add_field("`deleted_by` varchar(100) DEFAULT NULL COMMENT 'user id of logged user'");
    $this->dbforge->add_field("`deleted_date` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("studentexcess_prev_sem");
    }
    if(!$this->db->table_exists("studentfees")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`enrollment_id` bigint(20) NOT NULL");
    $this->dbforge->add_field("`student_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`studentfinance_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`fee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`value` float(10,2) DEFAULT NULL");
    $this->dbforge->add_field("`position` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("studentfees");
    }
    if(!$this->db->table_exists("studentfinances")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`coursefinance_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`student_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`discount` float(11,2) DEFAULT '0.00'");
    $this->dbforge->add_field("`total` float(11,2) DEFAULT '0.00'");
    $this->dbforge->add_field("`less` float(11,2) DEFAULT '0.00'");
    $this->dbforge->add_field("`net` float(11,2) DEFAULT '0.00'");
    $this->dbforge->add_field("`status` tinyint(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`total_unit_fee` float DEFAULT NULL");
    $this->dbforge->add_field("`total_lab_fee` float DEFAULT NULL");
    $this->dbforge->add_field("`account_recivable_student` float DEFAULT NULL");
    $this->dbforge->add_field("`old_account` float DEFAULT NULL");
    $this->dbforge->add_field("`other` float DEFAULT NULL");
    $this->dbforge->add_field("`enrollmentid` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`type` varchar(30) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("studentfinances");
    }
    if(!$this->db->table_exists("studentpayments")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`value` decimal(11,2) DEFAULT '0.00'");
    $this->dbforge->add_field("`date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`remarks` text");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`studentfinance_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`student_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`category` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`account_recivable_student` decimal(11,2) DEFAULT '0.00'");
    $this->dbforge->add_field("`old_account` decimal(11,2) DEFAULT '0.00'");
    $this->dbforge->add_field("`other` float(11,2) DEFAULT '0.00'");
    $this->dbforge->add_field("`total` decimal(11,2) DEFAULT '0.00'");
    $this->dbforge->add_field("`enrollmentid` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`or_no` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`group_or_no` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`is_excess_payment` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`excess_enrollment_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("studentpayments");
    }
    if(!$this->db->table_exists("studentpayments_details")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`enrollment_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`is_paid` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`amount` float DEFAULT NULL");
    $this->dbforge->add_field("`amount_paid` float DEFAULT NULL");
    $this->dbforge->add_field("`balance` float DEFAULT NULL");
    $this->dbforge->add_field("`is_promisory` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`studentpromisory_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`date_applied` datetime DEFAULT NULL");
    $this->dbforge->add_field("`is_current` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`is_downpayment` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`grading_period_id` varchar(20) DEFAULT NULL");
    $this->dbforge->add_field("`grant_userid` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`number` int(11) DEFAULT '0'");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("studentpayments_details");
    }
    if(!$this->db->table_exists("studentpromisory")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`studentpayment_details_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`enrollment_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`promisory` text");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("studentpromisory");
    }
    if(!$this->db->table_exists("studentregistrations")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`value` float(10,2) DEFAULT '0.00'");
    $this->dbforge->add_field("`date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`studentfinance_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`student_id` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("studentregistrations");
    }
    if(!$this->db->table_exists("students")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`login` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`name` varchar(100) DEFAULT ''");
    $this->dbforge->add_field("`email` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`crypted_password` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`salt` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`remember_token` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`remember_token_expires_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`activation_code` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`activated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`firstname` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`lastname` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`age` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`telephone` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`mobile` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`enrollment_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`sex` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`middle` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`date_of_birth` date DEFAULT NULL");
    $this->dbforge->add_field("`place_of_birth` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`civil_status` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`guardian` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`occupation` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`residence_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`business_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`education_parent` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`no_family` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`occupation_parent` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`income` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`student_number` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`primary` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`primary_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`intermediate` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`intermediate_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`secondary` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`secondary_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`college` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`college_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`course_completed` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`course_completed_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`studid` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`fake_email` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`old_or_new` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`scholar` varchar(255) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("students");
    }
    if(!$this->db->table_exists("studentsts")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`enrollment_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`subject_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`sts_id` int(11) NOT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`studentsubject_id` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("studentsts");
    }
    if(!$this->db->table_exists("studentsubjects")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`enrollment_id` int(11) DEFAULT NULL COMMENT 'USERS ID'");
    $this->dbforge->add_field("`subject_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`is_active` tinyint(1) DEFAULT NULL");
    $this->dbforge->add_field("`enrollmentid` int(11) DEFAULT NULL COMMENT 'ENROLLMENT ID'");
    $this->dbforge->add_field("`block_system_setting_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`is_deducted` smallint(6) DEFAULT NULL");
    $this->dbforge->add_field("`preliminary` decimal(11,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`midterm` decimal(11,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`semi_finals` decimal(11,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`finals` decimal(11,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`re_exam` decimal(11,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`remidial` decimal(11,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`remarks` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`date_dropped` date DEFAULT NULL");
    $this->dbforge->add_field("`rating` decimal(11,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`average` decimal(11,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`user_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`is_deleted` tinyint(1) DEFAULT '0' COMMENT 'When deleted value is 1'");
    $this->dbforge->add_field("`deleted_by` varchar(20) DEFAULT NULL COMMENT 'User Id of the one who delete'");
    $this->dbforge->add_field("`delete_date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`is_drop` tinyint(1) DEFAULT '0' COMMENT 'When dropped value is 1'");
    $this->dbforge->add_field("`dropped_by` varchar(20) DEFAULT NULL COMMENT 'User Id of the ony who dropped'");
    $this->dbforge->add_field("`dropped_date` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("studentsubjects");
    }
    if(!$this->db->table_exists("subject_pre_requisite")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`curriculum_id` bigint(20) DEFAULT NULL COMMENT 'Link to Table Curriculum'");
    $this->dbforge->add_field("`curriculum_sub_id` bigint(20) DEFAULT NULL COMMENT 'Link to Table Curriculum Subjects'");
    $this->dbforge->add_field("`subject_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`remarks` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`is_deleted` tinyint(1) DEFAULT '0' COMMENT 'if deleted value is 1'");
    $this->dbforge->add_field("`deleted_by` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`deleted_date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("subject_pre_requisite");
    }
    if(!$this->db->table_exists("subjects")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`ref_id` varchar(25) NOT NULL COMMENT 'Unique ID of Every Subject, To Determine Same subject in every school year'");
    $this->dbforge->add_field("`sc_id` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`code` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`subject` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`units` float(11,2) NOT NULL DEFAULT '0.00'");
    $this->dbforge->add_field("`time` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`day` text");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`room_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`room` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`lec` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`lab` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`lab_fee_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`original_load` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`load` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`subject_load` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`subject_taken` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`lab_kind` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`year_from` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`year_to` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`academic_year` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`employee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`is_nstp` smallint(1) DEFAULT '0'");
    $this->dbforge->add_field("`nstp_fee_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`class_start` time DEFAULT NULL");
    $this->dbforge->add_field("`class_end` time DEFAULT NULL");
    $this->dbforge->add_field("`hour` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`min` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`teacher_user_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("subjects");
    }
    if(!$this->db->table_exists("subjects_load_file")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`subject_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`enrollment_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("subjects_load_file");
    }
    if(!$this->db->table_exists("subjecttimeschedules")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`day` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`timef_h` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`timef_m` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`timef_state` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`timet_h` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`timet_m` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`timet_state` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`subject_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("subjecttimeschedules");
    }
    if(!$this->db->table_exists("summary_of_credits")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("summary_of_credits");
    }
    if(!$this->db->table_exists("sys_par")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`tmp_studid_series` varchar(50) DEFAULT '0'");
    $this->dbforge->add_field("`studid_series` varchar(50) DEFAULT '0' COMMENT 'Series Student ID'");
    $this->dbforge->add_field("`subj_refid_series` varchar(25) DEFAULT NULL COMMENT 'Series Reference Id for Subjects'");
    $this->dbforge->add_field("`is_auto_id` tinyint(1) DEFAULT '1'");
    $this->dbforge->add_field("`bootstrap_theme` set('AMELIA','CURELEAN','DEFAULT','FLATLY','JOURNAL','LUMEN','READABLE','SIMPLEX','SLATE','SPACELAB','SUPERHERO','UNITED','YETI','UB') DEFAULT 'DEFAULT'");
    $this->dbforge->add_field("`reg_success_msg` text");
    $this->dbforge->add_field("`is_menu_accordion` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`show_menu` tinyint(1) DEFAULT '1'");
    $this->dbforge->add_field("`application_layout` varchar(100) DEFAULT 'application' COMMENT 'Layout to be Used by the system (views/layouts/application_layout.php)'");
    $this->dbforge->add_field("`welcome_layout` varchar(100) DEFAULT 'welcome' COMMENT 'Layout to used in welcome screen and login'");
    $this->dbforge->add_field("`is_setup` tinyint(1) DEFAULT '0' COMMENT 'Is system was setup after fresh install'");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("sys_par");
    }
    if(!$this->db->table_exists("teachers")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`login` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`name` varchar(100) DEFAULT ''");
    $this->dbforge->add_field("`email` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`crypted_password` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`salt` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`remember_token` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`remember_token_expires_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`activation_code` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`activated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("teachers");
    }
    if(!$this->db->table_exists("temp_enrollments")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`temp_start_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`is_drop` tinyint(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`status2` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`enrollmentid` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`fname` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`year` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`course` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`subject` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`unit` varchar(255) NOT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`middle` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`semister` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`sy_from` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`sy_to` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`recent` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`sts` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`user_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`semester` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`firstname` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`lastname` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`age` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`telephone` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`disability` varchar(255) DEFAULT ''");
    $this->dbforge->add_field("`nationality` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`mobile` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`sex` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`discount` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`total` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`less` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`net` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`date_of_birth` date DEFAULT NULL");
    $this->dbforge->add_field("`place_of_birth` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`civil_status` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`guardian` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`occupation` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`residence_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`business_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`education_parent` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`no_family` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`occupation_parent` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`income` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`primary` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`primary_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`intermediate` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`intermediate_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`secondary` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`secondary_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`college` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`college_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`course_completed` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`course_completed_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`studid` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`fake_email` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`status` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`scholar` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`is_active` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`old_or_new` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`total_units` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`total_lab_units` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`total_units_fee` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`total_lab_units_fee` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`father_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`father_occupation` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`father_contact_no` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`mother_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`mother_occupation` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`mother_contact_no` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`parents_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`guardian_relation` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`guardian_contact_no` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`guardian_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`present_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`provincial_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`elementary` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`elementary_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`vocational` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`vocational_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`tertiary` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`tertiary_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`elementary_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`elementary_degree` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`secondary_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`secondary_degree` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`vocational_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`vocational_degree` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`tertiary_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`tertiary_degree` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`others_address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`others_degree` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`others` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`others_date` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`studentid` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`is_activated` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`religion` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`is_paid` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`y` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`s` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`c` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`total_student_payment` float NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`total_previous_account` float NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`total_student_deduction` float NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`total_additional_charges` float NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`barangay` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`subject_load_deducted` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`date_of_admission` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`admission_credentials` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`date_of_graduation` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`is_scholar` tinyint(4) DEFAULT NULL");
    $this->dbforge->add_field("`province` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`municipal` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`living_with_parents` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`full_paid` int(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`partial_paid` int(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`is_graduating` tinyint(1) DEFAULT NULL");
    $this->dbforge->add_field("`is_deleted` tinyint(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`street_no` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`is_used` smallint(1) DEFAULT '0'");
    $this->dbforge->add_field("`block_system_setting_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`studid_auto` varchar(50) DEFAULT NULL");
    $this->dbforge->add_field("`confirm_status` varchar(50) DEFAULT 'UNFINISH'");
    $this->dbforge->add_field("`confirm_userid` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`confirm_date` datetime DEFAULT NULL");
    $this->dbforge->add_field("`is_foreigner` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("temp_enrollments");
    }
    if(!$this->db->table_exists("temp_payment")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`temp_start_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`payment_plan_id` bigint(20) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("temp_payment");
    }
    if(!$this->db->table_exists("temp_start")){
    $this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`studid` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`type` varchar(50) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("temp_start");
    }
    if(!$this->db->table_exists("temp_studentsubjects")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`subject_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`block_system_setting_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`temp_start_id` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("temp_studentsubjects");
    }
    if(!$this->db->table_exists("time_schedules")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`from` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`to` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("time_schedules");
    }
    if(!$this->db->table_exists("time_slots")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`room` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`day` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`time_schedule_from` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`time_schedule_to` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`subject` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`teacher` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`room_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`room_name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`year` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`semester` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`course` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("time_slots");
    }
    if(!$this->db->table_exists("time_test")){
    $this->dbforge->add_field("`class_start` time DEFAULT NULL");
    $this->dbforge->add_field("`class_end` time DEFAULT NULL");
    $this->dbforge->create_table("time_test");
    }
    if(!$this->db->table_exists("training_programs")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`title` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`from` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`to` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`conducted` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`employee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("training_programs");
    }
    if(!$this->db->table_exists("transcripts")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`year_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`semester_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`course_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`enrollment_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`subject_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`final_grade` float(11,2) DEFAULT '0.00'");
    $this->dbforge->add_field("`earned_units` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("transcripts");
    }
    if(!$this->db->table_exists("user_tracks")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`url` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`user_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`track_controller` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`track_model` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`track_action` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("user_tracks");
    }
    if(!$this->db->table_exists("users")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`login` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`name` varchar(100) DEFAULT ''");
    $this->dbforge->add_field("`email` varchar(100) DEFAULT NULL");
    $this->dbforge->add_field("`crypted_password` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`salt` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`remember_token` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`remember_token_expires_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`activation_code` varchar(40) DEFAULT NULL");
    $this->dbforge->add_field("`activated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`is_activated` tinyint(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`admin` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`student` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`employee` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`employee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`registrar` tinyint(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`finance` tinyint(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`teacher` tinyint(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`custodian` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`dean` tinyint(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`hrd` tinyint(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`president` tinyint(1) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`librarian` tinyint(1) DEFAULT '0'");
    $this->dbforge->add_field("`cashier` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`guidance` int(11) NOT NULL DEFAULT '0'");
    $this->dbforge->add_field("`vp_assistant` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`assistant_to_the_president` int(11) DEFAULT '0'");
    $this->dbforge->add_field("`department` varchar(100) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("users");
    }
    if(!$this->db->table_exists("voluntary_works")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`name` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`address` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`employee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("voluntary_works");
    }
    if(!$this->db->table_exists("work_experiences")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`from` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`to` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`positioin` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`agency` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`employee_id` int(11) DEFAULT NULL");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`position` varchar(255) DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("work_experiences");
    }
    if(!$this->db->table_exists("years")){
    $this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
    $this->dbforge->add_field("`year` varchar(255) DEFAULT NULL");
    $this->dbforge->add_field("`level` int(2) DEFAULT '1'");
    $this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
    $this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
    $this->dbforge->add_key('`id`', TRUE);
    $this->dbforge->create_table("years");
    }
}

public function down(){
    $i_table = "academic_years"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "activity_log"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "additional_charges"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "admission_slips"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "announcements"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "assesment_settings"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "assign_coursefees"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "assign_courses"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "assign_subjects"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "assign_teachers"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "assignsts"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "assignsubjects"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "attendance_files"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "attendances"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "block_subjects"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "block_system_settings"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "borrowers"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "brands"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "captcha"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "career_services"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "carrer_services"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "categories"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "categoryfinances"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "certificate_of_enrollment_settings"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "ched_files"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "cheds"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "conversation"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "course_blocks"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "coursefees"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "coursefinances"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "courses"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "credits"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "curriculum"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "curriculum_subjects"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "delayed_jobs"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "deleted_students"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "demands"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "demands_return"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "demands_return_lost"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "departments"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "employeeattendances"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "employeecategories"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "employees"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "enrollment_notes"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "enrollments"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "events"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "examinations"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "examsubjects"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "fees"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "feevals"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "gengrades"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "genpayslips"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "grades"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "grading_periods"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "group_payments"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "issues"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "items"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "jobs"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "kinds"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "lessons"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "libraries"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "librarycards"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "librarycategory"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "librarysearches"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "librarystatus"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "main_menus"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "main_menus_method"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "masterlists"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "menus"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "menus_method"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "messages"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "migrate_student_subjects"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "open_semester_employee_settings"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "open_semesters"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "orders"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "other_informations"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "otrs"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "payment_plan"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "payment_plan_req"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "payment_type"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "payments_breakdown"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "payslips"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "permit_settings"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "photos"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "products"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "profiles"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "province"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "refunds"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "registrars"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "remaining_balance_percentages"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "remarks"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "roles"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "roles_users"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "room_schedules"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "rooms"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "sales"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "schema_migrations"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "searchsubjects"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "semesters"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "semisters"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "serialnumbers"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "settings"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "student_attendance"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "student_deductions"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "student_deductions_record"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "student_enrollment_fees"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "student_finance_categories"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "student_grade_categories"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "student_grade_files"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "student_grades"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "student_payment_breakdown"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "student_payment_records"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "student_plan_modes"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "student_previous_accounts"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "student_refund"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "student_scholarship_record"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "student_scholarships"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "student_subject_grades"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "student_total_file"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "student_totals"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "studentadvances"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "studentbalances"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "studentdownpayments"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "studentexcess_prev_sem"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "studentfees"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "studentfinances"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "studentpayments"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "studentpayments_details"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "studentpromisory"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "studentregistrations"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "students"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "studentsts"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "studentsubjects"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "subject_pre_requisite"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "subjects"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "subjects_load_file"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "subjecttimeschedules"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "summary_of_credits"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "sys_par"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "teachers"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "temp_enrollments"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "temp_payment"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "temp_start"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "temp_studentsubjects"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "time_schedules"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "time_slots"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "time_test"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "training_programs"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "transcripts"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "user_tracks"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "users"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "voluntary_works"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "work_experiences"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
    $i_table = "years"; if($this->db->table_exists($i_table)){ $this->dbforge->drop_table($i_table); } 
  }
}