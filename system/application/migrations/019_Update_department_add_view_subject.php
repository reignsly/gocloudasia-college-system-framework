<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_department_add_view_subject extends CI_Migration {

	public function up() 
	{
		if($this->db->table_exists('departments'))
		{
			if (!$this->db->field_exists('stud_subject', 'departments'))
			{
				$this->db->query("ALTER TABLE `departments` ADD COLUMN `stud_subject` TINYINT(1) DEFAULT 0 NULL AFTER `stud_issues`;");
			}
		}	

		if($this->db->table_exists('package_departments'))
		{
			if (!$this->db->field_exists('stud_subject', 'package_departments'))
			{
				$this->db->query("ALTER TABLE `package_departments` ADD COLUMN `stud_subject` INT(1) DEFAULT 0 NULL AFTER `stud_add_fees`;");
			}
		}	
	} 
	public function down() 
	{
		if ($this->db->field_exists('stud_subject', 'departments'))
		{
			$this->dbforge->drop_column('departments', 'stud_subject');
		}

		if ($this->db->field_exists('stud_subject', 'package_departments'))
		{
			$this->dbforge->drop_column('package_departments', 'stud_subject');
		}
	}
} 
