<?php
	
class M_student_subjects Extends MY_Model
{
	protected $_table = 'studentsubjects';
	protected $_uid = 'id';
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
  	
	public function get_lab_kind($data, $lab_kind)
	{
	  $value = 0.0;
	  if(is_array($data))
	  {
		  foreach ($data as $row) {
			if (strpos(strtolower($row->lab_kind),strtolower($lab_kind)) !== false) {
			  $value += $row->lab;
			}
		  }  
		  return $value;
	  }
	  return $value;
	}
	
	public function get_class_list_by_subject($subject_id, $year_from, $year_to,$semester_id)
	{
		$sql = "select enrollments.name AS student_name, enrollments.studid, enrollments.id, years.year, courses.course, semesters.name, enrollments.sex 
				FROM studentsubjects as ss
				LEFT JOIN subjects ON ss.subject_id = subjects.id
				LEFT JOIN enrollments ON ss.enrollmentid = enrollments.id
				LEFT JOIN years ON enrollments.year_id = years.id
				LEFT JOIN courses ON enrollments.course_id = courses.id
				LEFT JOIN semesters ON enrollments.semester_id = semesters.id
				WHERE ss.subject_id = ? 
				AND enrollments.sy_from = ?
				AND enrollments.sy_to =?
				AND enrollments.semester_id = ?
				AND enrollments.is_paid = 1
				AND ss.is_deleted = 0
				ORDER BY enrollments.name ASC";
		$query = $this->db->query($sql,array($subject_id, $year_from, $year_to,$semester_id));
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	
	
	
	public function get_student_subjects($data,$c = false,$y = false,$s = false)
	{
		$sql = "SELECT
						ss.id,subjects.sc_id, 
						subjects.code, 
						subjects.subject, 
						subjects.units, 
						subjects.lab, 
						subjects.lec, 
						subjects.time, 
						subjects.day, 
						ss.enrollmentid, 
						subjects.lab_kind
					FROM studentsubjects as ss
					LEFT JOIN subjects ON ss.subject_id = subjects.id
					WHERE ss.enrollment_id = ?
					AND ss.is_deleted = 0
					AND ss.is_drop = 0
				";
		$query = $this->db->query($sql,array($data,$c,$y,$s));
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function recalculate_load($data, $condition)
	{
	  $this->load->model(array('M_subjects'));
	  
	  
		$sql = "select s.subject_load, s.id
				FROM studentsubjects as ss
				LEFT JOIN subjects s ON ss.subject_id = s.id
				WHERE ss.enrollmentid = ? 
				";
		$query = $this->db->query($sql,array($data));
		$result = $query->num_rows() > 0 ? $query->result() : FALSE;
		
		if($result !== FALSE)
		{
			foreach ($result as $s) 
			{
				if ($condition == "add") 
				{
					$this->M_subjects->update_load($s->id, 'add', $data);
				}
			
				if ($condition == "remove") 
				{
					$this->M_subjects->update_load($s->id, 'remove', $data);
				}	   	      
			}
	  }
	}
	
	public function get_studentsubjects($data)
	{
		$sql = "select 
					ss.id,
					subjects.sc_id, 
					subjects.code, 
					subjects.subject, 
					subjects.units, 
					subjects.lab, 
					subjects.lec, 
					subjects.time, 
					subjects.day, 
					ss.enrollmentid, 
					ss.final_grade, 
					ss.remarks, 
					subjects.lab_kind, 
					subjects.subject_load, 
					subjects.id as subjectid, 
					rooms.name as room, 
					users.name as instructor,
					subjects.lec,
					subjects.is_nstp,
					b.name as block_name
				FROM studentsubjects as ss
				LEFT JOIN subjects ON ss.subject_id = subjects.id
				LEFT JOIN rooms ON rooms.id = subjects.room_id
				LEFT JOIN users on users.id = subjects.teacher_user_id
				LEFT JOIN block_system_settings b ON b.id = ss.block_system_setting_id
				WHERE ss.enrollment_id = ?
				AND ss.is_deleted = 0
				AND ss.is_drop = 0
				";
		$query = $this->db->query($sql,array($data));
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}


	/**
	 * Load Student Subjects
	 */
	public function load_subjects($e_id)
	{
		unset($get);
		$get['fields'] = array(
			'studentsubjects.id',
			'subjects.sc_id', 
			'subjects.code', 
			'subjects.subject', 
			'subjects.units', 
			'subjects.lab', 
			'subjects.lec', 
			'subjects.time', 
			'subjects.day', 
			'subjects.lab_fee_id', 
			'studentsubjects.enrollmentid', 
			'subjects.lab_kind', 
			'subjects.subject_load', 
			'subjects.subject_taken', 
			'subjects.id as subjectid', 
			'subjects.id as subject_id', 
			'CONCAT(rooms.name,"-",rooms.description) as room',
			'subjects.lec',
			'users.name as instructor',
			'subjects.is_nstp',
			'subjects.nstp_fee_id',
			'studentsubjects.final_grade',
			'studentsubjects.remarks',
			'studentsubjects.rating',
			'studentsubjects.average',
			'studentsubjects.is_drop',
			'block_system_settings.name as block_name'
		);
		$get['where']['studentsubjects.enrollment_id'] = $e_id;
		$get['where']['studentsubjects.is_deleted'] = 0;
		$get['where']['studentsubjects.is_drop'] = 0;

		$get['join'] = array(
				array(
				"table" => "subjects",
				"on"	=> "subjects.id = studentsubjects.subject_id",
				"type"  => "LEFT"
				),
				array(
				"table" => "users",
				"on"	=> "users.id = subjects.teacher_user_id",
				"type"  => "LEFT"
				),
				array(
				"table" => "block_system_settings",
				"on"	=> "block_system_settings.id = studentsubjects.block_system_setting_id",
				"type"  => "LEFT"
				),
				array(
				"table" => "rooms",
				"on"	=> "rooms.id = subjects.room_id",
				"type"  => "LEFT"
				)
			);
		$get['order'] = 'subjects.code';
		return $this->get_record("studentsubjects",$get);
	}
	
	public function get_studentsubjects_total($studentsubjects)
	{
	  $data["total_units"] = $data["total_lab"] = $data["total_lec"] = 0;
	  if(!empty($studentsubjects)){
		foreach ($studentsubjects as $s) {
		  $data["total_units"] += $s->units;
		  $data["total_lab"] += $s->lab;
		  $data["total_lec"] += $s->lec;
		}
		}
		
		return $data;
	}
	
	public function get_student_subjects_units($data,$c,$y,$s)
	{
		$sql = "select SUM(subjects.units) as total_units, SUM(subjects.lab) as total_labs,  ss.enrollmentid
				FROM studentsubjects as ss
				LEFT JOIN subjects ON ss.subject_id = subjects.id
				WHERE ss.enrollmentid = ? 
				AND ss.course_id = ? 
				AND ss.year_id = ? 
				AND ss.semester_id = ?
				AND ss.is_deleted = 0
				";
		$query = $this->db->query($sql,array($data,$c,$y,$s));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	public function get_units($eid)
	{
		$sql = "SELECT 
					SUM(subjects.units) as total_units, 
					SUM(subjects.lab) as total_labs,  ss.enrollmentid
				FROM studentsubjects as ss
				LEFT JOIN subjects ON ss.subject_id = subjects.id
				WHERE ss.enrollmentid = ? 
				AND ss.is_deleted = 0
				";
		$query = $this->db->query($sql,array($eid));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	public function add_student_subject($input,$user_cred)
	{	
		$insert_this[] = NULL;
		foreach($input as $key => $value)
		{
			$subject_log[] = $value;
			$insert_this[$key]['subject_id'] = $value;
			$insert_this[$key]['year_id'] = $user_cred['year'];
			$insert_this[$key]['semester_id'] = $user_cred['sems'];
			$insert_this[$key]['course_id'] = $user_cred['cors'];
			$insert_this[$key]['enrollmentid'] = $user_cred['enid'];
		}
			$subject = implode(',',$subject_log);
			$this->db->insert_batch($this->_table,$insert_this);
			return $this->db->affected_rows() > 0 ? array('status'=>'true','log'=>'Added Subject ID(s): '.$subject.'; ') : array('status'=>'false','log'=>'Unable to add Subject ID(s): '.$subject. '; ');
	}
	
	public function destroy_subject($where = false)
	{
		$this->db->where('id',$where)->delete($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function get_all_nstp_student()
	{
		$ci =& get_instance();
		
		$sql = "
			SELECT 
				ss.id,
				e.name,
				s.subject,
				s.code,
				c.course
				FROM 
				studentsubjects ss
				LEFT JOIN enrollments e ON e.id = ss.enrollmentid
				LEFT JOIN subjects s ON s.id = ss.subject_id
				LEFT JOIN courses c ON c.id = ss.course_id
				WHERE s.code LIKE '%NSTP%'
				GROUP BY ss.enrollmentid
				ORDER BY e.name	
		";
	}
	
	public function add_ajax($input = false)
	{
		$this->db->insert($this->_table,$input);
		return $this->db->affected_rows() > 0 ? array('status'=>'true', 'id' => $this->db->insert_id()) : array('status'=>'false');
	}

	public function fix_student_grade(){
		
		$ci =& get_instance();
		$semester_id = $ci->open_semester->id;
		$sy_from = $ci->open_semester->year_from;
		$sy_to = $ci->open_semester->year_to;

		$param[] = $semester_id;
		$param[] = $sy_from;
		$param[] = $sy_to;

		$sql = "SELECT 
				id, name, studid
				FROM enrollments 
				WHERE semester_id = ?
				and sy_from = ?
				and sy_to = ?
				LIMIT 1";
		$query = $this->db->query($sql, $param);
		$rs = $query->num_rows() > 0 ? $query->result() : false;

		if($rs){

			foreach ($rs as $key => $en) {

				echo $en->studid." | ".$en->name."<hr/><br/>";

				//Get student grade file
				$sql = "SELECT id from student_grade_files where user_id = ?";
				$query = $this->db->query($sql, array($en->id));
				$sgf = $query->num_rows() > 0 ? $query->row() : false;

				if($sgf){

				//Get student grade file && subjects
				$sql = "SELECT id from student_grades where student_grade_file_id = ?";
				$query = $this->db->query($sql, array($sgf->id));
				$sg = $query->num_rows() > 0 ? $query->result() : false;

					if($sg){

						unset($data);

						foreach ($sg as $key => $obj) {

							echo "Subject ID : ".$obj->id."<br/>";

							$sql = "SELECT * FROM student_grade_categories WHERE student_grade_id = ?";
							$query = $this->db->query($sql, array($obj->id));
							$sgc = $query->num_rows() > 0 ? $query->result() : false;
							if($sgc){

								$pass_ctr = 0;

								foreach ($sgc as $key => $cat) {
									echo $cat->category.' - '.$cat->value.' | ';
									switch ($cat->category) {
										case 'Finals':
											if($cat->value){
												$data['finals'] = $cat->value;
												$pass_ctr++;
											}
											break;
										case 'Semi-Finals':
											if($cat->value){
												$data['semi_finals'] = $cat->value;
												$pass_ctr++;
											}
											break;
										case 'Midterm':
											if($cat->value){
												$data['midterm'] = $cat->value;
												$pass_ctr++;
											}
											break;
										case 'Preliminary':
											if($cat->value){
												$data['preliminary'] = $cat->value;
												$pass_ctr++;
											}
											break;
									}
								}
								echo "<br/>";
							}
						}

						//UPDATE STUDENT SUBJECTS
						if($pass_ctr > 0){
							$data['remarks'] = $sg->remarks;
							// $data['date_dropped'] = $sg->date_dropped;
							// $data['rating'] = $sg->rating;
							// $data['average'] = $sg->average;
							vd($data);
							$rs = $this->update_studentsubjects($sg->subjectid, $data);

							if($rs['status'])
							{
								echo "Success";
							}
							else
							{
								echo "Failed";
							}
						}
						else
						{
							echo "Failed. No categories found.";
						}

						echo "<hr/>";
					}
				}
				echo "<hr/>";
			}

		}
	}

	public function update_studentsubjects($data = false, $where = false)
	{
		$this->db->set($data)->where('id',$where)->update($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}

	/**
	 * Get Students for updating subject grade in teachers
	 * @param $subject_id 
	 * @return array of student objects
	 */
	public function get_subjects_students($subject_id, $student_only = false)
	{
		#STEP 1 : Get All Students that enrolled the subject in studentsubjects table
		$sql = "
			SELECT
			e.id,
			e.studid,
			e.name
			FROM $this->_table as ss
			LEFT JOIN enrollments e ON e.id = ss.enrollment_id
			LEFT JOIN subjects s ON s.id = ss.subject_id
			WHERE ss.subject_id = ?
			AND ss.is_drop = 0
			AND ss.is_deleted = 0
			AND e.sy_from = ?
			AND e.sy_to = ?
			AND e.semester_id = ?
			ORDER BY e.lastname
		";

		$filter[] = $subject_id;
		$filter[] = $this->ci->cos->user->year_from;
		$filter[] = $this->ci->cos->user->year_to;
		$filter[] = $this->ci->cos->user->semester_id;

		$subjects = false;

		$this->load->model('M_grading_periods');
		$gps = $this->M_grading_periods->get_all();
		
		//load blank student library
		$this->load->library('_Student', array('enrollment_id'=>''));

		if($rs = $this->query($sql, $filter)): foreach ($rs as $k => $e):

			#STEP 2 : Get Enrollment Grades	
			$this->_student->enrollment_id = $e->id;
			$this->_student->load_default();
			$this->_student->load_profile();
			
			if(!$student_only){
				$this->_student->create_student_grades_file();

				
				$grades = $this->_student->get_subject_grades($subject_id);
				
				if($gps){}
					
				if($this->_student->profile){
					$subjects[] = array(
							'profile' => $this->_student->profile,
							'grades' => $grades,
						);
				}
			}else{
				$subjects[] = $this->_student->profile;
			}

		endforeach; endif;
		
		return $subjects;
	}

	/**
	 * Is Student or enrollment belongs to the teacher
	 * @param int $user_id Teacher ID
	 * @param int $enrollment_id Student Enrollment Id
	 * @return bool
	 */
	public function is_enrollment_belong_to_teacher($userid, $enrollment_id)
	{
		$sql = "
			SELECT
			ss.enrollment_id
			FROM $this->_table ss
			LEFT JOIN enrollments e ON e.id = ss.enrollment_id
			LEFT JOIN subjects s ON s.id = ss.subject_id
			WHERE ss.enrollment_id = ?
			AND s.teacher_user_id = ?
			AND e.is_drop = 0
			AND e.is_deleted = 0
		";

		$param[] = $enrollment_id;
		$param[] = $userid;

		$rs = $this->query($sql, $param);

		return $rs ? TRUE : FALSE;
	}

	/**
	 * Get Student Subjects under the teacher
	 * @param int $enrollment_id Enrollment ID
	 * @param int $userid Teacher User ID
	 * @return array
	 */
	public function get_student_subjects_under_teacher($enrollment_id, $userid)
	{
		$like = false;
		$filter = false;

		$filter['enrollments.sy_from'] = $this->ci->cos->user->year_from;
		$filter['enrollments.sy_to'] = $this->ci->cos->user->year_to;
		$filter['enrollments.semester_id'] = $this->ci->cos->user->semester_id;
		$filter['subjects.teacher_user_id'] = $userid;
		
		//CONFIGURATION
		$get['fields'] = array(
				'enrollments.id',
				'enrollments.name',
				'enrollments.studid',
				'years.year',
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			0 => array(
				"table" => "subjects",
				"on"	=> "subjects.id = studentsubjects.subject_id",
				"type"  => "LEFT"
			),
			1 => array(
				"table" => "enrollments",
				"on"	=> "enrollments.id = studentsubjects.enrollment_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "enrollments.lastname";
		$get['group'] = "studentsubjects.enrollment_id";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
		return $this->get_where_dd("studentsubjects", $get, false, array('id','studid','name'));
	}
}
