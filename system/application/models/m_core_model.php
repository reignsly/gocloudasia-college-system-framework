<?php
	
class M_core_model Extends MY_Model
{
	protected $_table;
	protected $_uid;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();

		$this->_uid = "id";
	}

	public function set_table($table="", $_uid = false)
	{
		$this->_table = $table;
		if($_uid){
			$this->_uid = $_uid;
		}else{
			$this->_uid = "id";
		}
	}
	
	public function run_sql($sql, $die = true)
	{
		//INTENDED FOR DEVELOPMENT ONLY
		//SHOULD BE REMOVE IN PRODUCTION
		
		$array_sql = explode(";", $sql);
		// if (ob_get_level() == 0) ob_start();
		echo "Please wait . . . .";
		
		foreach($array_sql as $qry)
		{
			if(trim($qry) != ""):
			$query = $this->db->query($qry);
		
			if(is_object($query) && $query->num_rows() > 0){
				echo "<hr/>";
				echo "Result<br/>";
				
				$result = $query->result();
				
				echo "<table border = 1 style='border-collapse:collapse;'>";
				echo "<tr>";
				echo "<th>#</th>";
				foreach($result as $obj)
				{
					foreach($obj as $key => $val)
					{
						echo "<th>".$key."</th>";
					}
					break;
				}
				echo "</tr>";
				
				$ctr = 1;
				
				foreach($result as $obj)
				{
					echo "<tr>";
					echo "<td>".$ctr."</td>";
					foreach($obj as $key => $val)
					{
						echo "<td>".$val."</td>";
					}
					echo "</tr>";
					$ctr++;
				}
				
				echo "</table>";
			}
			
			echo "<br/><hr/>Affected Rows<br/>";
			echo $this->db->affected_rows()." Affected Rows<br/>";
			echo "<br/><hr/> End of Query";
			// ob_flush();
        	flush();
			endif;
		}
		
		echo "<hr/>";
		echo "Finish.";
		ob_flush();
        flush();
		if($die){
			die();
		}
	}

	/**
	*Check table captcha if exist
	*else create the table
	*/
	public function check_captcha_table(){
		$this->load->dbforge();
		$exist = $this->db->table_exists('captcha');
		if($exist){
			return true;
		}else{
			$this->dbforge->add_field(array(
				'id' => array(
					'type' => 'BIGINT',
					'constraint' => 13,
					'unsigned' => TRUE,
					'auto_increment' => TRUE
				),
				'captcha_time' => array(
					'type' => 'INT',
					'constraint' => 10
				),
				'ip_address' => array(
					'type' => 'VARCHAR',
					'constraint' => '16',
				),
				'word' => array(
					'type' => 'VARCHAR',
					'constraint' => '20',
				),
				'created_at' => array(
					'type' => 'DATETIME'
				),
				'updated_at' => array(
					'type' => 'DATETIME'
				)
			));

			$this->dbforge->add_key('id');

			return $this->dbforge->create_table('captcha');
		}
	}

	/**
	*Check table if exits
	*returns true or false
	*/
	public function is_table_exist($table){
		return $this->db->table_exists($table);
	}
}