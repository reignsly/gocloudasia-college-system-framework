<?php

class M_menus Extends MY_Model
{
	protected $_table = 'menus';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function fix()
	{
		$sql = "
			SELECT
			*
			FROM menus
			WHERE menu_lvl = 2
		";
		
		$rs = $this->db->query($sql)->result();
		
		foreach($rs as $obj)
		{
			$sql2 = "
				SELECT * FROM main_menus where controller = ?
			";
			
			$rs2 = $this->db->query($sql2, array($obj->controller))->row();
			
			if($rs2)
			{
				unset($data);
				$data['main_menu_id'] = $rs2->id;
				$this->update($obj->id, $data);
			}
		}
	}
	
	/*
		FIX THE MENUS (menu_grp, menu_sub, menu_num)
		FOR Programming purpose only
		SLY
	*/
	
	public function fix_menu_grp()
	{
		$sql_dep = "SELECT department FROM menus GROUP BY department";
		$dep = $this->db->query($sql_dep)->result();
		
		$ctr = 0;
		$menu_ctr = 0;
		
		foreach($dep as $department):
		
		$sql = "SELECT * FROM menus WHERE menu_lvl = 1  and department = ?";
		$rs = $this->db->query($sql, array($department->department))->result();
		
		foreach($rs as $obj)
		{
			$sql2 = "SELECT * FROM menus WHERE menu_lvl = 2 AND menu_grp = ? AND department = ?";
			$rs2 = $this->db->query($sql2, array($obj->menu_sub, $obj->department))->result();
			{
				unset($data);
				$data['menu_grp'] = $ctr;
				$data['menu_sub'] = $ctr.'_SUB';
				$data['menu_num'] = $menu_ctr;
				$menu_ctr++;
				$this->update($obj->id, $data);
				
				foreach($rs2 as $obj2):
					unset($data);
					$data['menu_grp'] = $ctr.'_SUB';
					$data['menu_sub'] = '';
					$data['menu_num'] = $menu_ctr;
					$this->update($obj2->id, $data);
					$menu_ctr++;
				endforeach;
				
				$ctr++;
			}
		}
		
		$ctr++;
		
		endforeach;
	}
	
	public function get_all_menus($usertype='')
	{
		#REGION GET USER MENUS AND PUT IT TO SESSION
		$user_menus = false;
		//GET LEVEL ONE MENUS OR HEADER MENUS
		$sql = "SELECT
					id,
					main_menu_id,
					department,
					controller,
					caption,
					menu_grp,
					menu_sub,
					menu_num,
					menu_lvl,
					menu_icon,
					visible,
					help,
					attr
				FROM menus
				WHERE department = ?
				AND visible = ?
				AND menu_lvl = ?
				GROUP BY menu_grp
				ORDER BY menu_num
			";
		$this->db->cache_on();
		$query = $this->db->query($sql, array($usertype, 1, 1));
		if($query->num_rows() > 0){
			$rs =  $query->result();
			
			foreach($rs as $obj1){
				//GET LEVEL TWO MENUS
				$user_menus[] = $obj1;
				$sql2 = "SELECT
						m.id,
						m.main_menu_id,
						m.department,
						m.controller,
						m.caption,
						m.menu_grp,
						m.menu_sub,
						m.menu_num,
						m.menu_lvl,
						m.menu_icon,
						m.visible,
						m.help,
						m.attr,
						mm.remarks
					FROM menus m
					LEFT JOIN main_menus mm ON mm.id = m.main_menu_id
					WHERE m.department = ?
					AND m.visible = ?
					AND m.menu_lvl = ?
					AND m.menu_grp = ?
					ORDER BY m.menu_num";
				$this->db->cache_on();
				$query2 = $this->db->query($sql2, array($usertype, 1, 2, $obj1->menu_sub));
				if($query2->num_rows() > 0){
					
					$rs2 = $query2->result();
					foreach($rs2 as $obj2){
						//GET THE MENU
						
						$user_menus[] = $obj2;
					}
				}
			}
		}
		
		return $user_menus;
	}
	
	public function find_all($where = false)
	{
		if($where != false)
		{
			$query = $this->db->select('*')->where($where)->get($this->_table);
			return $query->num_rows() > 0 ? $query->result() : FALSE;
		}
		else
		{
			$sql = "select *
				FROM $this->_table
				ORDER BY id";
			$query = $this->db->query($sql);
			return $query->num_rows() > 0 ? $query->result() : FALSE;
		}
	}
	
	public function get_menus_group_by_department()
	{
		$sql = "SELECT 
				department
				FROM $this->_table
				GROUP BY department
				ORDER BY department";
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function get_header_menus_by_department($department)
	{
		$sql = "SELECT 
				*
				FROM $this->_table
				WHERE department = ?
				AND menu_lvl = 1
				ORDER BY menu_num";
		$query = $this->db->query($sql, array($department));
		return $query->num_rows() > 0 ? $query->result() : FALSE;

	}
	
	public function get_sub_menus_by_menu_sub($department, $menu_sub)
	{
		$sql = "SELECT 
				*
				FROM $this->_table
				WHERE department = ?
				AND menu_grp = ?
				AND menu_lvl = 2
				ORDER BY menu_num";
		$query = $this->db->query($sql, array($department, $menu_sub));
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function create_menus($input = false)
	{
		$this->db->insert($this->_table,$input);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function get_menus($id = false)
	{
		$sql = "select *
				FROM $this->_table
				WHERE id = ?
				ORDER BY id";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	public function update_menus($data = false, $where = false)
	{
		$this->db->set($data)->where('id',$where)->update($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function delete_menus($where = false)
	{
		$this->db->where('id',$where)->delete($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}

	public function delete_menus_by_menu_grp($department = false, $menu_grp = false)
	{
		$this->db->where('department', $department);
		$this->db->where('menu_grp', $menu_grp);
		$this->db->delete($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}

	public function payment_record_fix()
	{
		$sql = "SELECT
				*
				FROM menus
				WHERE controller LIKE '%payment_record%'
		";
		
		$query = $this->db->query($sql);
		
		if($query->num_rows() > 0)
		{
			$rs = $query->result();
			
			foreach($rs as $obj){
				
				$controller = $obj->controller;
				$controller = str_replace('payment_record','payments', $controller);
				
				$data['controller'] = $controller;
				
				$this->update_menus($data, $obj->id);
			}
		}	
	}
	
	public function fetch_all_menus($department_obj = false, $department)
	{
		$this->load->model('M_menus_method');

		$result = false;
		
		$men_grp = $department_obj;

		$ctr = 0;
		
		if($men_grp)
		{
			foreach($men_grp as $obj)
			{
				$result[$ctr]['header'] = $obj;
				$sql2 = "SELECT
						*
						FROM menus 
						WHERE department = ?
						AND menu_grp = ?
						AND menu_lvl = 2
				";
				$query = $this->db->query($sql2, array($department, $obj->menu_sub));
				$result[$ctr]['menu_list'] = $query->num_rows() > 0 ? $query->result() : false;
				
				#region GET MENU LIST METHODS

				$ml = $result[$ctr]['menu_list'];

				if($ml){
					foreach ($ml as $key => $m) {
						unset($config);
						$config['fields'] = array('id','method','access');
						$config['where'] = array(
							'menu_id' =>$m->id 
						);

						$rs_m = $this->M_menus_method->get_record('menus_method',$config);
						$result[$ctr]['menu_method'][$m->id] = $rs_m;
					}
				}

				#endregion

				$ctr++;
			}
		}
		
		return $result;
	}
	
	public function fetch_menu_list($department, $menu_sub)
	{
		$sql = "
			SELECT
			*
			FROM $this->_table
			WHERE department = ?
			AND menu_grp = ?
			ORDER BY menu_num DESC
		";
		
		$query = $this->db->query($sql,array($department, $menu_sub));
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function find_menu_group()
	{
		$sql = "
			SELECT
			*
			FROM $this->_table
			WHERE menu_lvl = 1
			ORDER BY menu_num
		";
		
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}

	/**
	 * Copy Menus from Package_menus
	 * @param int $p_id Package ID
	 * @return boolean true / false
	 */
	public function copy_menus_from_package($p_id)
	{
		$this->load->model('M_package_menus');
		$package_menus = $this->M_package_menus->get_menus($p_id, true);

		$this->query2("DELETE FROM menus"); // remove all menus except those for student page

		if($package_menus){
			foreach ($package_menus as $l => $m) {
				unset($m['id']);
				unset($m['package_id']);
				unset($m['department_id']);
				unset($m['create_at']);
				unset($m['updated_at']);
				$rs = $this->insert($m);
			}
		}

		return true;
	}

	/**
	 * Fix Main Menu ID - Loop for menus and get their main_menu_id and update
	 */
	public function fix_main_menu_id()
	{
		$sly = new stdClass;
		$sly->status = false;
		$sly->msg = "Transaction failed";

		$sql = "SELECT 
		 			id,
		 			main_menu_id,
		 			controller,
		 			caption
		 			FROM $this->_table
		 			WHERE menu_lvl = 2
		 ";
		$rs = $this->query($sql);
		$ctr = 0;
		
		if($rs){
		 	foreach ($rs as $key => $m) {
		 		
		 		unset($get);
		 		$get['fields'] = "id";
		 		$get['where']['controller'] = $m->controller;
		 		$get['single'] = true;

		 		$rs_main = $this->get_record('main_menus', $get);
		 		
		 		if($rs_main){

		 			unset($save);
		 			$save['main_menu_id'] = $rs_main->id;
		 			$rs_save = $this->update($m->id, $save);

		 			if($rs_save){
		 				$ctr++;
		 			}
		 		}
		 	}

		 	if($ctr>0){
		 		$sly->status = true;
		 		$sly->msg = "Menus were succcessfully fixed";
		 	}else{
		 		$sly->msg = "No menu were succcessfully fixed";
		 	}
		}

		 return $sly;
	}

	/**
	 * System patch to Udpate Main menu remarks
	 */
	public function fix_update_all_remarks()
	{
		$sly = new stdClass;
		$sly->status = false;
		$sly->msg = "Transaction failed";

		$remarks = array(
				'configure/school' => 'School settings, which include the name, address, contacts and other information.',
				'events' => 'School Calendar that will record events.',
				'years' => 'Enrollees year level.',
				'semesters' => 'The term or semester that divides the academic year.',
				'courses' => 'The entire program or academic course taken by students.',
				'academic_years' => 'School year.',
				'grading_periods' => 'Master File of Grading Period',
				'open_semester' => 'Current open semester of school that include academic year and semester.',
				'block_section_settings' => 'Group or block of subjects and schedule that enrollees can take as a whole.',
				'remarks' => 'Master File of Remarks',
				'fees' => 'Master File or creator of School fees.',
				'coursefinances' => 'A group of school fees that will be applied per academic course.',
				'remaining_balance_percentages' => '',
				'permit_settings' => 'Settings of Grade Slip Report.',
				'employeecategories' => 'Group of categorization of employees or users.',
				'open_enrollment' => 'Setup to open or close the student registration for the current open semester set (Both New and Old students).',
				'search/search_student' => 'Student search for officially enrolled or those students who already made their first payments.',
				'search/search_enrollee' => 'Student search for those who is enrolled but has not yet made its first payment.',
				'search/list_students/paid' => 'Student search for those who is currently enrolled and has made payment.',
				'search/list_students/unpaid' => 'Student search for those who is currently enrolled but no payment has been made yet.',
				'search/list_students/fullpaid' => 'Student search for those who is currently enrolled and already in full payment.',
				'search/list_students/partialpaid' => 'Student search for those who is currently enrolled and has made a partial payment.',
				'group_payments' => 'Creates a report of collection based on a given time.',
				'search/master_list_by_course' => 'Academic Course master list.',
				'inventory' => 'Master file (Create, Edit, Delete) for inventory.',
				'borrow_items/create' => 'Borrowing and returning of items in inventory.',
				'borrow_items/records' => 'Records of Borrowed Items (Inventory).',
				'announcements' => 'Creation of announcements.',
				'enrollees/daily_enrollees' => 'Enrollee search for the current day. Student who are enrolled but no payment has been made yet.',
				'subjects' => 'Subject and schedule master file (Per Academic year and Semester).',
				'fees/student_charges' => 'Show the record of student charges for current open semester.',
				'excels/student_charges' => 'Download excel format of student charges report.',
				'payments/student_payments' => 'List of student payments for the current open semester.',
				'excels/student_payments' => 'Download report of Student payments.',
				'fees/student_deductions' => 'List of student deductions for the current open semester.',
				'excels/student_deductions' => 'Download report for student deduction.',
				'payments/remaining_balance_report' => 'Remaining balance report of students for the current open semester.',
				'payments/download_remaining_balance_report' => 'Report for remaining balance report.',
				'excels/download_all_student_profile' => 'Download all profile information of all officially enrolled students for the current open semester.',
				'exam_permit' => 'Generation of Student exam permit.',
				'admission_slips' => 'Issuance of student admission slip.',
				'employees' => 'Search for users registered on the system.',
				'employees/create' => 'Creation of system users per department.',
				'employee_attendance/calendar' => 'Employee attendance.',
				'book_category' => 'Creation of item category for library.',
				'books' => 'Creation of items or book for library.',
				'total_income' => 'Total income report.',
				'student_scholar' => 'Report for student scholars in the current open semester.',
				'subject_grades' => 'View of student grade per subject during current open semester.',
				'generate_grade_slip' => 'Student grade slip generation.',
				'enrollees/dropped_students' => 'List of dropped or deleted enrollees.',
				'confirm_enrollees' => 'Confirmation of student registration during enrollment.',
				'ched' => 'CHED Report.',
				'tesda/tesda_by_course' => 'TESDA Report.',
				'students_geographical_statistic' => "",
				'international_student' => 'Search of foreign students for the current open semester.',
				'enrollees/nstp_students' => 'The student who is currently taking any nstp subject.',
				'messages' => 'Inbox.',
				'section_offering' => 'Section and Course offerings.',
				'registration' => 'Student Registration Approval',
				'profile' => 'Profile.',
				'schedule' => 'Subject Schedule.',
				'grade' => 'Grade.',
				'student/search_schedule' => 'Search of subject schedule.',
				'student/soa' => 'Viewing and printing of Student Statement of Accounts.',
				'student/promisory' => 'Promisory.',
				'student_master_list' => 'Student master list.',
				'lessons' => 'Lessons',
				'classes/classroom' => 'Teachers student class.',
				'payment_plan' => 'Payment plan (Enrollees total payables divided by payment division).',
				'payment_type' => 'Type of payment (Cash, Check).',
				'users/activate_users' => 'Activates or deactivate users from entering the system.',
				'scholarship' => 'Scholarship Creator.',
				'rooms' => 'Room master file.',
				'student/reset_password' => 'Resetting of student password in the student portal.',
				'confirm_enrollees/lists' => 'The list of confirmed enrollees during registration (Confirmed by the registrar and ready to continue for assessment).',
				'curriculum' => 'Course Curriculum.',
				'my_students' => 'Teachers student list.',
				'my_subjects' => 'Teachers subject list.'
			);

		$ctr = 0;

		$this->load->model('M_main_menus');

		foreach ($remarks as $c => $r) {
			//search for the main with controller
			$sql = "SELECT id FROM main_menus WHERE `controller` = ? LIMIT 1";
			
			$m = $this->query($sql, [$c], true);
			if($m){
				unset($save);
				$save['remarks'] = $r;
				$rs_save = $this->M_main_menus->update($m->id, $save);
				
				if($rs_save){
					$ctr++;
				}
			}
		}

		if($ctr>0){
			$sly->status = true;
			$sly->msg = "Main menu remarks was successfully updated";
		}else{
			$sly->msg = "No menus was updated";
		}

		return $sly;
	}

	/**
	 * System Patch to Add Library Menus
	 * @return Object
	 */
	public function add_library_main_menus()
	{
		$sly = new stdClass;
		$sly->status = false;
		$sly->msg = "Transaction failed";

		$usertype = "librarian";

		$this->load->model(['M_main_menus','M_menus','M_package_menus']);

		/** Remove this Menus no longer needed (Main_menus)*/
			$rem_controllers = array(
					'book_category',
					'book_category/index',
					'books',
					'books/index'
				);
			$rem_header_menu = array(
					'Library Section',
				);
			foreach ($rem_controllers as $c) {
				$get = array('controller'=>$c);
				$del_mm = $this->M_main_menus->delete($get);
				$del_m = $this->M_menus->delete($get);
				$del_mp = $this->M_package_menus->delete($get);
			}

			foreach ($rem_header_menu as $h) {
				$del = array(
						'controller' => "#",
						'department' => $usertype,
						'caption' => $h,
						'menu_lvl' => 1,
					);
				$del_m = $this->M_menus->delete($del);
				$del_mp = $this->M_package_menus->delete($del);
			}

		/** Create New Library Menus
		 * Shoould Manually add to Department Menus and Package Menus Afterwards
		 */
			$lib_menus = array(
					"library/search_media" => "insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `remarks`) values('library/search_media','Search Media','lib_sm',NULL,'200','2',NULL,'1','Media are the items being borrowed from the library (e.g Books).')",
					"library/media_types" => "insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `remarks`) values('library/media_types','Media Types','lib_mt',NULL,'2001','2',NULL,'1','Media Types are the categorization of the items or material being borrowed in the library.')",
					"library/subjects" => "insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `remarks`) values('library/subjects','Media Subjects','lib_ms',NULL,'202','2',NULL,'1','It is the category of item or material in the library in terms of Subject.')",
					"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `remarks`) values('library/upload_media','Media Upload (CSV)','lib_mucsv',NULL,'210','2',NULL,'1','Upload library items/media through CSV file.')",
					"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `remarks`) values('library/upload_media','Media Upload (CSV)','lib_mucsv',NULL,'210','2',NULL,'1','Upload library items/media through CSV file.')",
					"library/borrowers" => "insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `remarks`) values('library/borrowers','Borrowers','lib_brws',NULL,'204','2',NULL,'1','Student or teachers that can borrow items in the library.')",
					"library/list_of_borrowed_media" => "insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `remarks`) values('library/list_of_borrowed_media','List of Borrowed Media','lib_lbm',NULL,'205','2',NULL,'1','List of borrowed items in the library.')",
					"library/media_masterlist" => "insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `remarks`) values('library/media_masterlist','Media Master List','lib_mml',NULL,'206','2',NULL,'1','Master list of materials or items in the library.')",
					"library/circulation_report" => "insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `remarks`) values('library/circulation_report','Circulation Report','lib_cr',NULL,'207','2',NULL,'1','A report of media or items being borrowed or return in the library.')",
			);

			$ctr = 0;
			
			// for main menus
			foreach ($lib_menus as $c => $qry) {

				// check first if menu already there
				$main = $this->M_main_menus->pull(array('controller'=>$c), 'id'); if($main) { continue; }
				$rs_insert_main = $this->M_main_menus->query2($qry);

				if($rs_insert_main){
					$ctr++;
				}
			}
				
		if($ctr > 0){
			$sly->status = true;
			$sly->msg = "Library Main Menus was successfully added.";
		}

		$sly->msg = "No menus have been added. Either menu already exists or the process failed.";

		return $sly;
	}
}