<?php

class M_users extends MY_Model{

  protected $_table = "users";
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function update_email_n_name($id, $email,$name)
	{
		$user['email'] = $email;
		$user['name'] = $name;

		return $this->update($id, $user);
	}
	
	function get_login_by_id($id = false)
	{
	  $sql = "select id, login, crypted_password, salt
				FROM $this->_table
				WHERE id = ?
				ORDER BY id";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	function get_user_by_id($id)
	{
		$sql = "select *
				FROM $this->_table
				WHERE id = ?
				";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	function get_user_by_login($id = false)
	{
		$sql = "select id, login, crypted_password, salt
				FROM $this->_table
				WHERE login = ?
				ORDER BY id";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	function fix_users(){
		
		$sql = "select department
				FROM departments
				";
		$query = $this->db->query($sql);
		$rs = $query->result();
		
		foreach($rs as $obj){
			$sql1 = "update users set department = '$obj->department' where $obj->department = 1";
			$query1 = $this->db->query($sql1);
		}	
	}
	
	function get_profile($id){
		$q = $this->db->where('id', $id)->where('is_activated', 1)->get('users');
		if($q->num_rows() >= 1){
			$row = $q->row();

			//GET USER DATA AND PUT IT TO SESSION
			$userdata = array(
						'userid' => $row->id,
						'username' => $row->name,
						'userlogin' => $row->login,
						'userType' => $row->department,
						'logged_in' => TRUE
					);
			
			$this->session->set_userdata($userdata);
			
			return true;
		}else{
			return false;
		}
	}
	
	public function activate_user_account($array)
	{
		foreach ($array['user_ids'] as $key => $id)
		{
			$data[$key]['is_activated'] = 1;
			$data[$key]['activated_at'] = date('Y-m-d H:i:s');
			$data[$key]['activation_code'] = '';
			$data[$key]['id'] = $id;
		}
		$this->db->update_batch($this->_table, $data, 'id');
	}

	public function activate_by_enrollment_id($id,$access)
	{
		$this->load->model('M_enrollments');
		$e = $this->M_enrollments->pull($id, 'user_id');
		
		$data['is_activated'] = $access;
		$data['activated_at'] = NOW;
		
		$r = $this->update($e->user_id, $data);

		if($r){
			$ret['code'] = "s";
			$ret['msg'] = "Student portal was successfully opened.";
			$action = $access == 1 ? "Activate Student Portal" : "Deactivate Student Portal";
			activity_log($action,false,'');
		}else{
			$ret['code'] = "e";
			$ret['msg'] = "Student portal failed to be activated, please try again";
		}
		return (object)$ret;
	}
	
	public function create_users($input = false, $activate = 0)
	{
		if($activate === 1){
			$input['is_activated'] = $activate;
			$input['activated_at'] =  NOW;
		}

		$this->db->insert($this->_table,$input);
		return $this->db->affected_rows() > 0 ? array('status'=>'true','id'=>$this->db->insert_id()) : array('status'=>'false');
	}
	
	public function update_users($data = false, $where = false)
	{
		$this->db->set($data)->where('id',$where)->update($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function update_users_by_login($data = false, $where = false)
	{
		$this->db->set($data)->where('login',$where)->update($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function get_users_by($where, $single = false, $array = false)
	{
			$this->db->where($where);
		
			if(!$array)
			{
				if($single)
				{
					$query = $this->db->get($this->_table, $single);
					return $query->num_rows() > 0 ? $query->row() : FALSE;
				}
				else
				{
					$query = $this->db->get($this->_table, $single);
					return $query->num_rows() > 0 ? $query->result() : FALSE;
				}
			}
			else
			{
				if($single)
				{
					$query = $this->db->get($this->_table, $single);
					return $query->num_rows() > 0 ? $query->first_row('array') : FALSE;
				}
				else
				{
					$query = $this->db->get($this->_table, $single);
					return $query->num_rows() > 0 ? $query->result_array() : FALSE;
				}
			}
	}

	public function find_all($where = false)
	{
		if($where != false)
		{
			$this->db->select(array('id', 'login', 'name','department'));
			$this->db->where($where);
			$this->db->order_by("name", "ASC");
			$query = $this->db->get($this->_table);
			
			// vd($this->db->last_query());
			return $query->num_rows() > 0 ? $query->result() : FALSE;
		}
		else
		{
			$sql = "select id, login, name
				FROM $this->_table
				ORDER BY name";
			$query = $this->db->query($sql);
			return $query->num_rows() > 0 ? $query->result() : FALSE;
		}
	}
	
	public function find($start=0,$limit=100, $filter = false, $all = false, $ret_count = false){
		
		$start = $this->db->escape_str($start);
		$limit = $this->db->escape_str($limit);
		$ci =& get_instance();
		
		//GET Library FIELDS
		// $sql = "DESCRIBE $this->_table";
		// $query = $this->db->query($sql);
		// $fields = $query->result();
		// $fields_array = array();
		// foreach($fields as $val){
			
			// $fields_array[] = $this->_table.'.'.$val->Field;
		// }
		
		//ADD FILTERS
		$fields_array[] = 'users.id';
		$fields_array[] = 'users.login';
		$fields_array[] = 'users.name';
		
		$param = array();
		if($filter != false){
			//if filter is array
			if(is_array($filter)){
				foreach($filter as $key => $value){
		
					$param[$key] = $value;
				}
			}
			
		}
		
		$this->db->select($fields_array);
		$this->db->from($this->_table);
		$this->db->where($param);
		$this->db->order_by("users.name", "ASC"); 
		// $this->db->join('librarycategory', 'librarycategory.id = '.$this->_table.'.librarycategory_id','LEFT');
		
		
		if($all == false){
			$this->db->limit($limit, $start);
		}
		
		$query = $this->db->get();
		// vp($this->db->last_query());
		if($ret_count == false){
			return $query->num_rows() > 0 ? $query->result() : FALSE;
		}else{
			return $query->num_rows();
		}
	}
	
	public function get_usertype($id){
		
		$sql = "select id, login, crypted_password, salt
				FROM $this->_table
				WHERE id = ?
				ORDER BY id";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
		
	}

	public function profile($id)
	{
		$sql = "
			SELECT
				u.id,
				u.name,
				u.department,
				e.employeeid,
			  e.first_name,
			  e.middle_name,
			  e.last_name,
			  e.joining_date,
			  e.gender,
			  e.dob,
			  e.employeecategory_id,
			  e.status,
			  e.martial_status,
			  e.no_children,
			  e.father_name,
			  e.mother_name,
			  e.spouse_name,
			  e.email
			FROM $this->_table u 
			LEFT JOIN employees e ON u.employee_id = u.id
			WHERE u.id = ?
		";
		return $this->query($sql,[$id],true);
	}
	
	public function fix_users_table()
	{
		$department = array(
			'admin',
			'registrar',
			'finance',
			'teacher',
			'custodian',
			'dean',
			'hrd',
			'president',
			'librarian',
			'cashier',
			'guidance',
			'vp_assistant',
			'assistant_to_the_president',
			'department',
			'student'
			
		);
		
		$affected_rows = 0;
		
		foreach($department as $dep)
		{
			if($dep == "vp_assistant" || $dep == "assistant_to_the_president")
			{
				$sql = "UPDATE users 
						set department = ?
						WHERE vp_assistant = 1
						OR assistant_to_the_president = 1";
				$query = $this->db->query($sql, array($dep));
				
				$affected_rows += $this->db->affected_rows();
			}
			else
			{
				$sql = "UPDATE users 
						set department = ?
						WHERE $dep = ?";
				$query = $this->db->query($sql, array($dep, 1));
				
				$affected_rows += $this->db->affected_rows();
			}
		}
		
		return $affected_rows > 0 ? true : false;
	}

	/**
	 * FIx Users without employee record - if users has no employee record create
	 */
	public function fix_users_withno_employee_record()
	{
		$sly = new stdClass;
		$sly->status = false;
		$sly->msg = "Transaction failed";

		$this->load->model('M_employees');

		$sql = "SELECT login,name,activated_at,department,created_at FROM users where department <> 'student'";
		$rs = $this->query($sql);
		$ctr = 0;
		
		if($rs){
			foreach ($rs as $k => $v) {
				
				//Check employees record by login
				$sql = "SELECT id FROM employees WHERE employeeid = ?";
				$rs_emp = $this->query($sql,[$v->login]);
				if($rs_emp === false){

					$name = explode(" ", $v->name);

					//create record
					unset($sata);
					$sata['employeeid'] = $v->login;
					$sata['first_name'] = isset($name[0]) ? $name[0] : "";
					$sata['middle_name'] = "";
					$sata['last_name'] = isset($name[1]) ? $name[1] : "";
					$sata['joining_date'] = $v->created_at;
					$sata['gender'] = "Male";
					$sata['dob'] = $v->created_at;
					$sata['status'] = 1;
					$sata['role'] = $v->department;
					$sata['department'] = $v->department;

					$add = (object)$this->M_employees->insert($sata);

					if($add->status){
						$ctr++;
					}
				}
			}
		}

		if($ctr > 0){
			$sly->status = true;
			$sly->msg = "There are $ctr users successfully updated.";
			return $sly;
		}

		$sly->msg = "No users has been updated.";

		return $sly;
	}
}

?>
