<?php

class M_student_enrollment_fees Extends MY_Model
{
	protected $_table = 'student_enrollment_fees';
	protected $_uid = 'sef_id';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function fix_add_fee_id()
	{
		$fees = $this->fetch_all();

		if($fees){
			foreach ($fees as $key => $value) {
				$name = $value->sef_fee_name;
				$qry = $this->db->select('fee_id')->where('fee_name',$name)->get('fees');
				if($qry->num_rows() > 0){
					$fee_id = $qry->row()->fee_id;
					$rs = $this->update($value->sef_id,array('fee_id'=>$fee_id));
					
				}
			}
			return true;
		}
	}

	public function update_payment_status($id){
		$rs = $this->pull($id, array('sef_id','sef_fee_rate','amount_paid','is_paid'));
		if($rs){
			$balance = $rs->sef_fee_rate - $rs->amount_paid;
			unset($data);
			if($balance <= 0){
				$data['is_paid'] = 1;
			}else{
				$data['is_paid'] = 0;
			}

			if($rs->is_paid != $data['is_paid']){
				$this->update($rs->sef_id, $data);
			}
		}
	}

	public function get_other_additional_fees($enrollment_id){
		$this->db->select(array('sef_fee_name','sef_fee_rate'));
		$this->db->where('sef_enrollment_id',$enrollment_id);
		$this->db->where('is_other_fee',1);
		$this->db->where('is_deleted',0);
		$q = $this->db->get($this->_table);
		return $q->num_rows() > 0 ? $q->result() : false;
	}

	public function get_student_enrollment_fees($enrollment_id)
	{
		$this->db->select(array('sef_fee_name','sef_fee_rate'));
		$this->db->where('sef_enrollment_id',$enrollment_id);
		$this->db->where('is_deleted',0);
		$q = $this->db->get($this->_table);
		return $q->num_rows() > 0 ? $q->result() : false;
	}
}