<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_student_scholarship_record Extends MY_Model
{
	protected $_table = 'student_scholarship_record';
	protected $_uid = 'id';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}	
}