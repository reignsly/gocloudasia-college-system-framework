<?php
	
class M_course_specialization Extends MY_Model
{
	protected $_table = 'course_specialization';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Check if record dublicated
	 * @param string $specialty
	 * @param int $course_id
	 * @return bool true/false
	 */
	public function check_if_duplicate($specialty, $course_id, $id = false)
	{
		$get['specialization'] = $specialty;
		$get['course_id'] = $course_id;

		if($id !== false){
			$get['id <> '] = $id;
		}

		$rs = $this->pull($get, 'id');
		return $rs ? TRUE : FALSE;
	}

	/**
	 * Create Specialty from POST
	 * @param array $post
	 * @param int $id Course id
	 */
	public function create_specialty($post, $id)
	{
		$sly = new stdClass;
		$sly->code = "e";
		$sly->msg = "Transaction failed, please try again";

		unset($save);
		$save['specialization'] = $s = trim($post['specialization']);
		$save['course_id'] = $id;

		if($this->check_if_duplicate($s,$id)){
			$sly->msg = $s." was already created, please try a unique one.";
			return $sly;
		}

		$rs = (object)$this->insert($save);

		if($rs->status){

			$save['course_specialization_id'] = $rs->id;
			$sly->code = "s";
			$sly->msg = $s." was successfully created.";

			activity_log('Add Course specialization',false, arr_str($save));
		}

		return $sly;
	}

	public function get_all_specialties($id)
	{
		$get['where']['course_id'] = $id;
		$get['order'] = "specialization";
		return $this->get_record(false, $get, false);
	}

	/**
	 * update data
	 * @param array of data
	 * @param int $id
	 * @param bool true/false
	 */
	public function update_specialization($data, $id)
	{
		$r = $this->pull($id, 'id,course_id');
		
		if($r){
			//check if unique
			if(!$this->check_if_duplicate($data, $r->course_id, $id)){
				$save['specialization'] = trim($data);
				$rs = $this->update($id, $save);
				if($rs){
					activity_log('Update Course specialization',false, " Course ID : {$r->course_id} Course specialization Id : {$id} . Data : ".arr_str($save));
				}
				return $rs;
			}
		}

		return false;
	}

	/**
	 * delete data
	 * @param int $id
	 * @param bool true/false
	 */
	public function delete_specialization($id)
	{
		$r = $this->pull($id, 'id,course_id,specialization');

		$log['course_id'] = $r->course_id;
		$log['Course Specialization ID'] = $id;
		$log['Specialization'] = $r->specialization;
		
		if($r){
			$rs = $this->delete($id);
			if($rs){

				activity_log('Delete Course specialization',false, " Data : ".arr_str($log));				
				return true;
			}
		}

		return false;
	}

}
