<?php

class M_sys_par Extends MY_Model
{
	protected $_table = 'sys_par';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function create_sys_par($input = false)
	{
		$this->db->insert($this->_table,$input);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function get_sys_par()
	{
		$sql = "select *
				FROM $this->_table
				LIMIT 1";
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	public function get_tmp_studid_and_update_to_next_series()
	{
		$ret = "00000001";
		
		$rs = $this->get_sys_par();
		if($rs)
		{
			$ret = $rs->tmp_studid_series;
			$num = floatval($rs->tmp_studid_series);
			$num++;
			
			$data['tmp_studid_series'] = $new = str_pad($num, 8, "0", STR_PAD_LEFT);
			$this->update_sys_par($data);
			
			$ret = "ER-".$ret;
		}
		else
		{
			$data['tmp_studid_series'] = $ret;
		}
		
		return $ret;
	}
	
	public function get_studid_and_update_to_next_series()
	{
		$ret = "00000001";
		
		$rs = $this->get_sys_par();
		if($rs)
		{
			$ret = $rs->studid_series;
			$num = floatval($rs->studid_series);
			$num++;
			
			$data['studid_series'] = $new = str_pad($num, 8, "0", STR_PAD_LEFT);
			$this->update_sys_par($data);
		}
		else
		{
			$data['studid_series'] = $ret;
		}
		
		return $ret;
	}

	public function get_subj_refid_and_update_to_next_series()
	{
		$ret = "0000000001";
		
		$rs = $this->get_sys_par();
		if($rs)
		{
			$ret = $rs->subj_refid_series;
			$num = floatval($rs->subj_refid_series);
			$num++;
			
			$data['subj_refid_series'] = $new = str_pad($num, 10, "0", STR_PAD_LEFT);
			$this->update_sys_par($data);
		}
		else
		{
			$data['subj_refid_series'] = $ret;
		}

		#check if the ref_id exist if yes - run this function again
		$this->load->model('M_subjects');
		unset($get);
		$get['where']['ref_id'] = $ret;
		$get['single'] = true;
		$rs_sub = $this->M_subjects->get_record(false, $get);

		if($rs_sub){
			
			$ret = $this->get_subj_refid_and_update_to_next_series();
		}

		return $ret;
	}
	
	public function update_sys_par($data = false)
	{
		$this->db->set($data)->update($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}

	public function get_current_system_profile()
	{
		/*GET CURRENT ACADEMIC YEAR*/
	}
}