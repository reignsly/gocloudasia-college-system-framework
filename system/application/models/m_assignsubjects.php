<?php

  #doc
  #  classname:  M_assignsubjects
  #  scope:    PUBLIC
  #
  #/doc
  
  class M_assignsubjects extends CI_Model
  {
    #  internal variables
  
    #  Constructor
    private $_table = 'assignsubjects';
    function __construct ()
    {
		  parent::__construct();
		  $this->load->database();
    }
    ###
	
	public function create_assignsubjects($input = false)
	{
		$this->db->insert($this->_table,$input);
		return $this->db->affected_rows() > 0 ? array('status'=>'true','id'=>$this->db->insert_id()) : array('status'=>'false');
	}
    
    public function insert_to_db($array = false)
	{
		if(!$array && is_array($array))
		{
			foreach ($array['subject_ids'] as $key => $id)
			{
				$data[$key]['employee_id'] = $array['employee_id'];
				$data[$key]['subject_id'] = $id;
			}
			$this->db->insert_batch($this->_table, $data);
			}
		}

	public function delete_from_db($id)
	{
			$this->db->where('id', $id);
			$this->db->delete($this->_table); 
	}

	public function get_employee_assignsubjects($id)
	{
			$sql = "SELECT assignsubjects.id, subjects.sc_id,subjects.subject,subjects.time,subjects.day, subjects.code, subjects.room, assignsubjects.subject_id
					FROM assignsubjects
					LEFT JOIN subjects on subjects.id = assignsubjects.subject_id
					WHERE assignsubjects.employee_id = ?";
			$query = $this->db->query($sql, array($id));
			// vd($this->db->last_query());
			return $query->num_rows() > 0 ? $query->result() : FALSE;
	
	}
	
	public function get_subject_by_subject_id($employee_id, $subject_id)
	{
		$sql = "
			SELECT
			*
			FROM assignsubjects
			WHERE employee_id = ?
			AND subject_id = ?
		";
		
		$query = $this->db->query($sql, array($employee_id, $subject_id));
		
		return $query->num_rows() > 0 ? $query->row() : false;
	}
	
	public function get($id = false,$array = false,$where = false)
	{
		if($id == false)
		{
			if($array == false)
			{
				if($where == false)
				{
					$query = $this->db->get($this->_table);
					return $query->num_rows() > 0 ? $query->result() : FALSE;
				}else
				{
					$query = $this->db->where($where)->get($this->_table);
					return $query->num_rows() > 0 ? $query->result() : FALSE;
				}
			}else
			{
				if($where == false)
				{
					$query = $this->db->select($array)->get($this->_table);
					return $query->num_rows() > 0 ? $query->result() : FALSE;
				}else
				{
					$query = $this->db->select($array)->where($where)->get($this->_table);
					return $query->num_rows() > 0 ? $query->result() : FALSE;
				}
			}
		}else
		{
			if($array == false)
			{
				if($where == false)
				{
					$query = $this->db->where('id',$id)->get($this->_table);
					return $query->num_rows() > 0 ? $query->row() : FALSE;
				}else
				{
					$query = $this->db->where('id',$id)->where($where)->get($this->_table);
					return $query->num_rows() > 0 ? $query->row() : FALSE;
				}
			}else
			{	
				if($where == false)
				{
					$query = $this->db->select($array)->where('id',$id)->get($this->_table);
					return $query->num_rows() > 0 ? $query->row() : FALSE;
				}else
				{
					$query = $this->db->select($array)->where('id',$id)->where($where)->get($this->_table);
					return $query->num_rows() > 0 ? $query->row() : FALSE;
				
				}
			}
		}
	}
	
	public function delete_record($where = false)
	{
		$this->db->where('id',$where)->delete($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
  }
  ###

?>
