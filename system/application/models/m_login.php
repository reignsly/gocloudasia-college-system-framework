<?php



class M_login Extends CI_Model
{	
	public function get_login_credentials($login, $usertype="")
	{
		$sql = 'SELECT id,crypted_password,salt FROM users WHERE login = ? AND department = ? AND is_activated = ? limit 1';
		$query = $this->db->query($sql,array($login, $usertype, 1));
		
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	
	
}
