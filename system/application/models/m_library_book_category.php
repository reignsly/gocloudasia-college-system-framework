<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_library_book_category extends MY_Model {
	
	protected $_table = "library_book_category";
	protected $_timestamp = false;

	public function __construct(){
		parent::__construct();
	}	
}