<?php

class M_system_custom_message Extends MY_Model
{
	protected $_table = 'system_custom_message';
	protected $_timestamp = false;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_all_messages()
	{
		$get = array(
				'order' => 'id',
				'fields' => 'id,type,message,desc'
			);
		return $this->get_record(false,$get);
	}

	public function update_message($id, $post)
	{
		$r = new stdClass;
		$r->code = "e";
		$r->msg = "Transaction failed, please try again";

		unset($save);
		$save['message'] = htmlentities(trim($post['message_mes']),ENT_QUOTES);
		$rs = $this->update($id, $save);
		if($rs){
			$r->code = "s";
			$r->msg = "System Custom Message was successfully updated.";

			activity_log('Update System Custom Message',false, "ID : $id - ".arr_str($save));
		}

		return $r;
	}
}