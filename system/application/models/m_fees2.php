<?php

class M_fees2 Extends MY_Model
{
	protected $_table = 'fees';
	protected $_uid = 'id';
	
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_all_tuition_fees()
	{
		$sql = "select *
				FROM $this->_table
				ORDER BY id";
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}

	public function get_other_fees($eid)
	{
		$sql = "
			SELECT 
				id,
				name,
				value
			FROM fees
			WHERE is_other = 1
			AND is_active = 1
			AND id NOT IN(
					SELECT fee_id 
					FROM student_enrollment_fees
					WHERE sef_enrollment_id = ?
					AND is_other_fee = 1
					AND fee_id IS NOT NULL
					AND is_deleted = 0
				)
			ORDER BY name

		";

		$q = $this->db->query($sql, array($eid));
		return $q->num_rows() > 0 ? $q->result() : false;
	}
	
}