<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	
class M_student_payment_record Extends MY_Model{
	
	protected $_table = 'student_payment_records';
	protected $_uid = 'spr_id';
	private $enrollments_table = 'enrollments';
	private $last_input_id;
	public function __construct()
	{
		parent::__construct();
	}
	
	public function add_payment_record($data)
	{
		$sql = "select max(spr_payment_number)as current_payment_number 
				from student_payment_records 
				where spr_enrollment_id = ?
				and spr_is_deleted = 0 ";

		$query = $this->db->query($sql,array($data['spr_enrollment_id']));
		if($query->num_rows() >= 1)
		{
			$result = $query->row()->current_payment_number;
			if($result == null)
			{
				$data['spr_payment_number'] = 1;
				$this->db->insert($this->_table,$data);
				$this->set_last_input_id($data);
				$this->insert_journal_entry($data); // insert to accounting
				
				if($this->db->affected_rows() > 0)
				{
					$this->_insert_payment_to_student_payment_mode($data);
					return true;
				}else{
					return false;
				}
				
			}else
			{
				$data['spr_payment_number'] = ($result + 1);
				$this->db->insert($this->_table,$data);
				$this->set_last_input_id();
				$this->insert_journal_entry($data); // insert to accounting
				
				if($this->db->affected_rows() > 0)
				{
					$this->_insert_payment_to_student_payment_mode($data);
					return true;
				}else{
					return false;
				}
			}
		}
	}
	
	public function update_enrollment_record($e_id)
	{
		$query = $this->db->select('e_registered')
						  ->where('e_id',$e_id)
						  ->get($this->enrollments_table);
		if($query->num_rows()>=1)
		{
			if($query->row()->e_registered == 1)
			{
				return true;
			}else{
				$this->db->set('e_registered',1)
						 ->where('e_id',$e_id)
						 ->update($this->enrollments_table);
				return $this->db->affected_rows() > 0 ? true : false;
			}
		}
	}

	public function get_payment_date($sy_id)
	{
		$sql = "select distinct spr_payment_date 
				from student_payment_records 
				where spr_schoolyear_id = ?
				order by date(spr_payment_date)";
		$q = $this->db->query($sql,array($sy_id)); 
		return $q->num_rows() >= 1 ? $q->result() : false;
	}
	
	public function get_payment_list($data)
	{
		$sql = "select student_payment_records.spr_or_no, student_payment_records.spr_ammt_paid, student_payment_records.spr_remarks, student_payment_records.spr_enrollment_id, enrollments.e_profile_id, profiles.firstname, profiles.middlename, profiles.lastname 
				from (student_payment_records 
				left join enrollments on student_payment_records.spr_enrollment_id = enrollments.e_id) 
				left join profiles on enrollments.e_profile_id = profiles.profile_id 
				where date(student_payment_records.spr_payment_date) = ?
				and spr_is_deleted = 0
				";
		$q = $this->db->query($sql,array($data)); 
		return $q->num_rows() >= 1 ? $q->result() : false;
	}
	
	public function get_payment_record($id = false)
	{
		if($id !== false)
		{
			$sql = "select spr.spr_or_no,
						   spr.spr_remarks,
						   spr.spr_payment_date,
						   spr.spr_ammt_paid
					from {$this->_table} spr 
					where spr.spr_id = ? 
					and spr_is_deleted = 0
					limit 1";
			$query = $this->db->query($sql,array($id));
			return $query->num_rows() >=1 ? $query->row() : false;
		}else{
			$sql = "select spr.spr_or_no,
						   spr.spr_remarks,
						   spr.spr_payment_date,
						   spr.spr_ammt_paid
					from {$this->_table} spr
					and spr_is_deleted = 0
					";
			$query = $this->db->query($sql);
			return $query->num_rows() >=1 ? $query->result() : false;			
		}
	}
	
	public function get_payment_record_by_enrollment_id($id)
	{
		$id = intval($id);
		
		$sql = "select `spr_id`,
						`spr_or_no`,
						concat(date_format(`spr_payment_date`,'%m %d, %y'),' - ',date_format(`spr_created`,'%r') ) as spr_payment_date,
						`spr_payment_date` as date,
						`spr_ammt_paid`,
						`spr_created`,
						`spr_mode_of_payment`,
						`spr_remarks`,
						`check_number`,
						`check_status`
				from `student_payment_records` 
				where `spr_enrollment_id` = ? 
				and spr_schoolyear_id = ?
				and spr_is_deleted = 0
				order by spr_id asc";
		$query = $this->db->query($sql,array($id,$this->c_sy->id));
		// vd($this->db->last_query());
		
		return $query->num_rows() >=1 ? $query->result() : false;
	}
	
	public function set_last_input_id()
	{
		$this->last_input_id = $this->db->insert_id();
	}
	
	public function get_last_input_id()
	{
		return $this->last_input_id;
	}
	
	public function current_payment_record_id($e_id)
	{
		$sql = "select max(spr_id) as current_spr_id 
				from student_payment_records 
				where spr_enrollment_id = ?
				and spr_is_deleted = 0 ";
		$query = $this->db->query($sql,array($e_id));
		return $query->num_rows() > 0 ? $query->row()->current_spr_id : false;
	}
	
	public function current_payment_number($e_id,$spr_id)
	{
		$sql = "select max(spr_payment_number) as current_payment_number 
				from student_payment_records 
				where spr_enrollment_id = ? 
				and spr_is_deleted = 0
				and spr_id = ?";
		$query = $this->db->query($sql,array($e_id,$spr_id));
		return $query->num_rows() > 0 ? $query->row()->current_payment_number : false;
	}
	
	public function delete_student_payment_and_record($id)
	{
		$sql = "update student_payment_records 
				set spr_is_deleted = 1, 
				spr_date_deleted = ?, 
				spr_deleted_by = ? 
				where spr_id = ? ";
		$this->db->query($sql,array(now,$this->c_user->userid,$id));
		
		if($this->db->affected_rows() >= 1)
		{
			$sql2 = 'delete from student_payment_breakdown
					 where spb_spr_id = ?';
			$this->db->query($sql2,array($id));
			
			if($this->db->affected_rows() >= 1)
			{
				$this->_clear_payment_to_student_payment_mode($id);
				return true;
			}else{
				log_message('error','unable to delete student payment breakdown spb_spr_id = '.$id.'');
				return false;
			}
		}else{
			log_message('error','unable to update student payment record to spr_deleted to 1 spr_id= '.$id.'');
			return false;
		}
	}
	
	
	public function update_payment_record($data,$id)
	{
		$this->db->set($data)
				 ->where('spr_id',$id)
				 ->update($this->_table);
		return $this->db->affected_rows() >= 1 ? true : true;
	}
	
	public function update_fully_paid_status($id)
	{
		$check = "select e_fullypaid
				  from enrollments 
				  where e_id = ? 
				  and e_ay_id = ? ";
				  
		$do_update = "update enrollments set e_fullypaid = 1
				where e_id = ? 
				and e_ay_id = ? ";
		$check_query = $this->db->query($check,array($id,$this->c_sy->id));
		
		if($check_query->num_rows() >= 1)
		{
			if($check_query->row()->e_fullypaid == 1)
			{
				return true;
			}else{
				$this->db->query($do_update,array($id,$this->c_sy->id));
			}
		}
	}
	
	public function over_the_counter_payments($data,$e_id)
	{
		foreach($data['quantity'] as $k => $v)
		{
			$r = $this->_get_fee_rate_name($data['desc'][$k]);
		
			$input[$k]['quantity'] = $v;
			$input[$k]['remarks'] = $r->fee_desc;
			$input[$k]['name_of_fee'] = $r->fee_desc;
			$input[$k]['ammt_paid'] = $v * $r->fee_rate;
			$input[$k]['user_trans_id'] = $this->session->userdata('userid');
			$input[$k]['user_trans_name'] = $this->session->userdata('username');
			$input[$k]['enrollment_id'] = $e_id;
			$input[$k]['or_no'] = $this->input->post('spr_or_no');
			$input[$k]['payment_date'] =  datetime::createfromformat('m/d/y', $data['date_of_payment'])->format('y-m-d');
			$input[$k]['schoolyear_id'] = $this->current_school_year;// defined in mycontroller
			$input[$k]['gperiod_id'] = $this->c_gp->gp_id;
			$input[$k]['created'] = now;
		
		}
		
		// pd($input);
		
		
		$this->db->insert_batch('fees_over_the_counter',$input);
		return $this->db->affected_rows() >= 1 ? true : false;
	}
	
	private function _get_fee_rate_name($id)
	{
		$q = $this->db->select('fee_name,fee_rate,fee_desc')
				 ->where('fee_id',$id)
				 ->limit(1)
				 ->get('fees');
		
		return $q->num_rows() >= 1 ? (object)$q->row() : (object)array('fee_name'=>'unable to get fee','fee_rate'=>0);
	
	}
	
	private function _insert_payment_to_student_payment_mode($data = '')
	{
		
		// $value['student_paid']= $data['spr_ammt_paid'];
		// $stud_id = $data['spr_enrollment_id'];
		// $current_month = date('f');
		
		// $this->db->set($value)
				 // ->where('spm_student_id',$stud_id)
				 // ->where('spm_ay_id',$this->c_sy->id)
				 // ->where('month_name',$current_month)
				 // ->update('student_payment_modes');	 
				 
				 
		$d = (object)$data;
				 
		$sql = 'update student_payment_modes
				set student_paid = student_paid + ?
				where spm_student_id =?
				and spm_ay_id = ?
				and month_name = ?
				limit 1';
				
		$this->db->query($sql,array($d->spr_ammt_paid,$d->spr_enrollment_id,$this->c_sy->id,$d->spr_for_the_month_of));
		// pd($this->db->last_query());
	}
	
	private function _clear_payment_to_student_payment_mode($id)
	{
		$value['student_paid']= 0.00;
		$stud_id = $id;
		$current_month = date('f');
		
		$this->db->set($value)
				 ->where('spm_student_id',$stud_id)
				 ->where('spm_ay_id',$this->c_sy->id)
				 ->where('month_name',$current_month)
				 ->update('student_payment_modes');
	}
	

	public function insert_journal_entry($data)
	{
		if($this->db->table_exists('0_journal_entry'))
		{
			// $input['spr_user_trans_id'] = $this->session->userdata('userid');
			// $input['spr_user_trans_name'] = $this->session->userdata('username');
			// $input['spr_enrollment_id'] = $enrollment_id_post;
			// $input['spr_or_no'] = $this->input->post('spr_or_no');
			// $input['spr_remarks'] = $this->input->post('remarks');
			// $input['spr_payment_date'] =  date('y-m-d',strtotime($this->input->post('date_of_payment')));
			// $input['spr_schoolyear_id'] = $this->current_school_year;// defined in mycontroller
			// $input['spr_gperiod_id'] = $this->current_grading_period;// defined in mycontroller
			// $input['spr_ammt_paid'] = $this->input->post('ammount_paid');
			// $input['spr_created'] = now;

			$sql = 'select ';
		
		
		
		}
	}
	
	public function insert_reference()
	{
		
		if($this->db->table_exists('0_gl_trans'))
		{
			$sql = 'select max(type_no]) as max_num';
		
		
		
		
		
		
		
		}
	}

	/**
	 * Get Student Payment Record by Date Range
	 * Returns Object list of payment record
	*/
	public function get_student_payment_by_range($from, $to)
	{
		$sql = "
			SELECT 
			spr.*,
			en.name
			FROM $this->_table spr
			LEFT JOIN enrollments en ON en.id = spr.spr_enrollment_id
			WHERE spr.spr_is_deleted = 0
			AND spr.is_applied = 1
			AND DATE(spr.spr_payment_date)
			BETWEEN ? AND ?
			ORDER BY spr.spr_payment_date
		";

		$q = $this->db->query($sql, array($from, $to));

		return $q->num_rows() > 0 ? $q->result() : false;
	}

	/**
	 * Get payment record by enrollment_id and grading period
	 */
	public function get_record_by_period($e_id, $gperiod)
	{
		$sql = "select spr.spr_or_no,
			   spr.spr_remarks,
			   spr.spr_payment_date,
			   spr.spr_ammt_paid
			from {$this->_table} spr 
			where spr.spr_enrollment_id = ? 
			and spr_gperiod_id = ?
			and spr_is_deleted = 0";
		return $this->query($sql, [$e_id,$gperiod]);
	}
	
	/**
	 * Get Student Payment Record using enrollment id
	 * @param  int $id enrollment_id
	 * @return object     result
	 */		
	public function get_enrollee_payment_record($id)
	{
		$sql = "
			SELECT 
				spr.spr_id as id,
				spr.spr_or_no as or_no,
				spr.spr_ammt_paid as total,
				spr.spr_remarks as remarks,
				spr.spr_payment_date as date
			FROM $this->_table spr
			WHERE spr.spr_is_deleted = 0
			AND spr.is_applied = 1
			AND spr.spr_enrollment_id = ?
			ORDER BY spr.spr_payment_date
		";

		$q = $this->db->query($sql, array($id));

		return $q->num_rows() > 0 ? $q->result() : false;
	}

	/**
	 * Get Student Payments Records base from post filters
	 * To be use in excel report
	 * @param  array $post from Filter Form
	 * @return object       result
	 */	
	public function student_payment_record_list($post)
	{
		$sql = "
			SELECT
				spr.spr_id,
				spr.spr_or_no as or_no,
				spr.spr_ammt_paid as amount,
				spr.spr_payment_date as date,
				spr.spr_remarks as remarks,
				spr.spr_mode_of_payment as mode,
				spr.check_date,
				spr.check_number,
				CONCAT(e.lastname,', ',e.fname,' ',e.middle) as fullname,
				c.course,
			  c.course_code,
			  y.year, 
			  y.id as year_id	
			FROM $this->_table spr
			LEFT JOIN enrollments e ON e.id = spr.spr_enrollment_id
			LEFT JOIN courses c ON c.id = e.course_id
			LEFT JOIN years y ON y.id = e.year_id
			WHERE spr.spr_is_deleted = 0
			AND is_applied = 1
			AND e.semester_id = ?
			AND e.sy_from = ?
			AND e.sy_to = ?
		";
		$param[] = $this->ci->cos->user->semester_id;
		$param[] = $this->ci->cos->user->year_from;
		$param[] = $this->ci->cos->user->year_to;

		if(isset($post['year_id']) && $post['year_id']){
			$sql .= " AND e.year_id = ? ";
			$param[] = $post['year_id'];
		}

		if(isset($post['course_id']) && $post['course_id']){
			$sql .= " AND e.course_id = ? ";
			$param[] = $post['course_id'];
		}

		$sql .= " ORDER BY ".$post['report_order'];
		$rs = $this->query($sql,$param);
		return $rs;
	}

	/**
	 * Get Student Payments Records base from post filters - by Academic year
	 * To be use in excel report
	 * @param  array $post from Filter Form
	 * @return object       result
	 */	
	public function student_payment_record_ay_list($post)
	{
		$this->load->model('M_academic_years');
		$ay_id = $post['ay_id'];
		$ay = $this->M_academic_years->pull($ay_id,'year_from, year_to');

		$sql = "
			SELECT
				spr.spr_id,
				spr.spr_or_no as or_no,
				spr.spr_ammt_paid as amount,
				spr.spr_payment_date as date,
				spr.spr_remarks as remarks,
				spr.spr_mode_of_payment as mode,
				spr.check_date,
				spr.check_number,
				CONCAT(e.lastname,', ',e.fname,' ',e.middle) as fullname,
				c.course,
			  c.course_code,
			  y.year, 
			  y.id as year_id,
			  s.name as semester	
			FROM $this->_table spr
			LEFT JOIN enrollments e ON e.id = spr.spr_enrollment_id
			LEFT JOIN courses c ON c.id = e.course_id
			LEFT JOIN years y ON y.id = e.year_id
			LEFT JOIN semesters s ON s.id = e.semester_id
			WHERE spr.spr_is_deleted = 0
			AND is_applied = 1
			AND e.sy_from = ?
			AND e.sy_to = ?
		";
		
		$param[] = $ay->year_from;
		$param[] = $ay->year_to;

		if(isset($post['semester_id']) && $post['semester_id']){
			$sql .= " AND e.semester_id = ? ";
			$param[] = $post['semester_id'];
		}

		if(isset($post['year_id']) && $post['year_id']){
			$sql .= " AND e.year_id = ? ";
			$param[] = $post['year_id'];
		}

		if(isset($post['course_id']) && $post['course_id']){
			$sql .= " AND e.course_id = ? ";
			$param[] = $post['course_id'];
		}

		$sql .= " ORDER BY ".$post['report_order'];
		$rs = $this->query($sql,$param);
		return $rs;
	}

	/**
	 * Get Student Payments Records base from post filters and return amount by year level
	 * To be use in excel report
	 * @param  array $post from Filter Form
	 * @return object       result
	 */	
	public function student_payment_record_year($post)
	{
		$this->load->model('M_years');

		$get = false;
		if(isset($post['year_id']) && $post['year_id']){
			$get['id'] = $post['year_id'];
		}
		$years = $this->M_years->fetch_all($get, 'level');

		$rs = false;

		if($years){
			foreach ($years as $ky => $y) {
				
				$sql = "
					SELECT
						SUM(spr.spr_ammt_paid) as amount
					FROM $this->_table spr
					LEFT JOIN enrollments e ON e.id = spr.spr_enrollment_id
					WHERE spr.spr_is_deleted = 0
					AND spr.is_applied = 1
					AND e.semester_id = ?
					AND e.sy_from = ?
					AND e.sy_to = ?
					AND e.year_id = ?
				";
				unset($param);
				$param[] = $this->ci->cos->user->semester_id;
				$param[] = $this->ci->cos->user->year_from;
				$param[] = $this->ci->cos->user->year_to;
				$param[] = $y->id;

				if(isset($post['course_id']) && $post['course_id']){
					$sql .= " AND e.course_id = ? ";
					$param[] = $post['course_id'];
				}

				$a = $this->query($sql,$param,true);
				
				unset($data);
				$data['id'] = $y->id;
				$data['year'] = $y->year;
				$data['amount'] = $a->amount ? $a->amount : 0;

				$rs[] = (object)$data;
			}
		}
		return $rs;
	}

	/**
	 * Get Student Payments Records base from post filters and return amount by year level - annually
	 * To be use in excel report
	 * @param  array $post from Filter Form
	 * @return object       result
	 */	
	public function student_payment_record_ay_year($post)
	{
		$this->load->model('M_years');
		$this->load->model('M_academic_years');
		$ay_id = $post['ay_id'];
		$ay = $this->M_academic_years->pull($ay_id,'year_from, year_to');

		$get = false;
		if(isset($post['year_id']) && $post['year_id']){
			$get['id'] = $post['year_id'];
		}
		$years = $this->M_years->fetch_all($get, 'level');

		$rs = false;

		if($years){
			foreach ($years as $ky => $y) {
				
				$sql = "
					SELECT
						SUM(spr.spr_ammt_paid) as amount
					FROM $this->_table spr
					LEFT JOIN enrollments e ON e.id = spr.spr_enrollment_id
					WHERE spr.spr_is_deleted = 0
					AND spr.is_applied = 1
					AND e.sy_from = ?
					AND e.sy_to = ?
					AND e.year_id = ?
				";
				unset($param);
				$param[] = $ay->year_from;
				$param[] = $ay->year_to;
				$param[] = $y->id;

				if(isset($post['semester_id']) && $post['semester_id']){
					$sql .= " AND e.semester_id = ? ";
					$param[] = $post['semester_id'];
				}

				if(isset($post['course_id']) && $post['course_id']){
					$sql .= " AND e.course_id = ? ";
					$param[] = $post['course_id'];
				}

				$a = $this->query($sql,$param,true);

				unset($data);
				$data['id'] = $y->id;
				$data['year'] = $y->year;
				$data['amount'] = $a->amount ? $a->amount : 0;

				$rs[] = (object)$data;
			}
		}
		return $rs;
	}

	/**
	 * Get Student Payments Records base from post filters and return amount by course
	 * To be use in excel report
	 * @param  array $post from Filter Form
	 * @return object       result
	 */	
	public function student_payment_record_course($post)
	{
		$this->load->model('M_courses');

		$get = false;
		if(isset($post['course_id']) && $post['course_id']){
			$get['id'] = $post['course_id'];
		}
		$courses = $this->M_courses->fetch_all($get, 'course');
		$rs = false;

		if($courses){
			foreach ($courses as $ky => $c) {
				
				$sql = "
					SELECT
						SUM(spr.spr_ammt_paid) as amount
					FROM $this->_table spr
					LEFT JOIN enrollments e ON e.id = spr.spr_enrollment_id
					WHERE spr.spr_is_deleted = 0
					AND spr.is_applied = 1
					AND e.semester_id = ?
					AND e.sy_from = ?
					AND e.sy_to = ?
					AND e.course_id = ?
				";
				unset($param);
				$param[] = $this->ci->cos->user->semester_id;
				$param[] = $this->ci->cos->user->year_from;
				$param[] = $this->ci->cos->user->year_to;
				$param[] = $c->id;

				if(isset($post['year_id']) && $post['year_id']){
					$sql .= " AND e.year_id = ? ";
					$param[] = $post['year_id'];
				}

				$a = $this->query($sql,$param,true);
				
				unset($data);
				$data['id'] = $c->id;
				$data['code'] = $c->course_code;
				$data['course'] = $c->course;
				$data['amount'] = $a->amount ? $a->amount : 0;
				$rs[] = (object)$data;
			}
		}
		return $rs;
	}

	/**
	 * Get Student Payments Records base from post filters and return amount by course - annually
	 * To be use in excel report
	 * @param  array $post from Filter Form
	 * @return object       result
	 */	
	public function student_payment_record_ay_course($post)
	{
		$this->load->model('M_courses');
		$this->load->model('M_academic_years');
		$ay_id = $post['ay_id'];
		$ay = $this->M_academic_years->pull($ay_id,'year_from, year_to');

		$get = false;
		if(isset($post['course_id']) && $post['course_id']){
			$get['id'] = $post['course_id'];
		}
		$courses = $this->M_courses->fetch_all($get, 'course');
		$rs = false;

		if($courses){
			foreach ($courses as $ky => $c) {
				
				$sql = "
					SELECT
						SUM(spr.spr_ammt_paid) as amount
					FROM $this->_table spr
					LEFT JOIN enrollments e ON e.id = spr.spr_enrollment_id
					WHERE spr.spr_is_deleted = 0
					AND spr.is_applied = 1
					AND e.sy_from = ?
					AND e.sy_to = ?
					AND e.course_id = ?
				";
				unset($param);
				$param[] = $ay->year_from;
				$param[] = $ay->year_to;
				$param[] = $c->id;

				if(isset($post['semester_id']) && $post['semester_id']){
					$sql .= " AND e.semester_id = ? ";
					$param[] = $post['semester_id'];
				}

				if(isset($post['year_id']) && $post['year_id']){
					$sql .= " AND e.year_id = ? ";
					$param[] = $post['year_id'];
				}

				$a = $this->query($sql,$param,true);
				
				unset($data);
				$data['id'] = $c->id;
				$data['code'] = $c->course_code;
				$data['course'] = $c->course;
				$data['amount'] = $a->amount ? $a->amount : 0;
				$rs[] = (object)$data;
			}
		}
		return $rs;
	}

	/**
	 * Get Student Payments Records base from post filters and return amount by semester - annually
	 * To be use in excel report
	 * @param  array $post from Filter Form
	 * @return object       result
	 */	
	public function student_payment_record_ay_semester($post)
	{
		$this->load->model('M_semesters');
		$this->load->model('M_academic_years');
		$ay_id = $post['ay_id'];
		$ay = $this->M_academic_years->pull($ay_id,'year_from, year_to');

		$get = false;
		if(isset($post['semester_id']) && $post['semester_id']){
			$get['id'] = $post['semester_id'];
		}
		$semesters = $this->M_semesters->fetch_all($get, 'level');
		$rs = false;
		
		if($semesters){
			foreach ($semesters as $ky => $s) {
				
				$sql = "
					SELECT
						SUM(spr.spr_ammt_paid) as amount
					FROM $this->_table spr
					LEFT JOIN enrollments e ON e.id = spr.spr_enrollment_id
					WHERE spr.spr_is_deleted = 0
					AND spr.is_applied = 1
					AND e.sy_from = ?
					AND e.sy_to = ?
					AND e.semester_id = ?
				";
				unset($param);
				$param[] = $ay->year_from;
				$param[] = $ay->year_to;
				$param[] = $s->id;

				if(isset($post['course_id']) && $post['course_id']){
					$sql .= " AND e.course_id = ? ";
					$param[] = $post['course_id'];
				}

				if(isset($post['year_id']) && $post['year_id']){
					$sql .= " AND e.year_id = ? ";
					$param[] = $post['year_id'];
				}

				$a = $this->query($sql,$param,true);
				
				unset($data);
				$data['id'] = $s->id;
				$data['semester'] = $s->name;
				$data['amount'] = $a->amount ? $a->amount : 0;
				$rs[] = (object)$data;
			}
		}
		
		return $rs;
	}
}
?>