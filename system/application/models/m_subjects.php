<?php
class M_subjects Extends MY_Model
{

	protected $_table = 'subjects';
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Available Schedule for current open semester
	 * @param string $ex Exclude subjects
	 */
	public function available_schedules($ex=false)
	{
		unset($get);

		//fields
		$get['fields'] = array(
				'master_subjects.code',
				'master_subjects.subject',
				'master_subjects.units',
				'master_subjects.lec',
				'master_subjects.lab',
				'subjects.*',
				'rooms.name as room',
				'users.name as teacher'
			);

		//where
		$get['where']['subjects.year_from'] = $this->ci->cos->user->year_from;
		$get['where']['subjects.year_to'] = $this->ci->cos->user->year_to;
		$get['where']['subjects.semester_id'] = $this->ci->cos->user->semester_id;

		if($ex){
			$get['not_in'] = array(
					'field' => 'subjects.id',
					'data' => $ex
				);
		}

		//join
		$get['join'][] = array(
				'table' => 'master_subjects',
				'on' => 'master_subjects.ref_id = subjects.ref_id',
				'type' => 'left'
			);
		$get['join'][] = array(
				'table' => 'users',
				'on' => 'users.id = subjects.teacher_user_id',
				'type' => 'left'
			);
		$get['join'][] = array(
				'table' => 'rooms',
				'on' => 'rooms.id = subjects.room_id',
				'type' => 'left'
			);

		//order
		$get['order'] = "master_subjects.code";
		
		return $this->get_record('subjects', $get);
	}
	
	public function search_subjects($start=0,$limit=100,$data, $ret_count = false, $ret_all = false)
	{
		
		$start = $this->db->escape_str($start);
		$limit = $this->db->escape_str($limit);
		$filtered = array();
		unset($data['search_subjects']);
		foreach($data as $key => $value)
		{
			if($value && $value != "")
			{
				$filtered[trim($key)] = trim($value);
			}
		}
		
		if($ret_count)
		{
			$query = $this->db->select(array('id','sc_id','code','subject','units','lec','lab','time','day','room','subject_load','original_load','year_to','year_from'))->like($filtered)->get($this->_table);
		}
		else
		{
			if($ret_all){
				$query = $this->db->select(array('id','sc_id','code','subject','units','lec','lab','time','day','room','subject_load','original_load','year_to','year_from'))->like($filtered)->get($this->_table);
			}else{
				$query = $this->db->select(array('id','sc_id','code','subject','units','lec','lab','time','day','room','subject_load','original_load','year_to','year_from'))->like($filtered)->get($this->_table, $start, $limit);
			}
			
		}
		
		if($ret_count){
			return $query->num_rows();
		}else{
			return $query->num_rows() > 0 ? $query->result() : FALSE;
		}
	
	}

	public function get_subject($id)
	{
		$sql = "
			SELECT
			*,
			HOUR(class_start) as class_start_hr,
			HOUR(class_end) as class_end_hr,
			MINUTE(class_start) as class_start_min,
			MINUTE(class_end) as class_end_min
			FROM $this->_table
			WHERE id = ?
		";
		$q = $this->db->query($sql, array($id));
		return $q->num_rows() > 0 ? $q->row() : false;
	}

	/** Get Subject Profile
	 * @param $id Subject ID
	 * @return object
	 */
	public function get_profile($id)
	{
		$sql = "
			SELECT
			ms.ref_id,
			ms.code,
			ms.subject,
			ms.units,
			ms.lab,
			ms.lec,
			s.id,
			s.time,
			s.is_open_time,
			s.day,
			s.course_id,
			s.room_id,
			r.name as room,
			s.year_id,
			s.semester_id,
			s.lab_fee_id,
			s.original_load,
			s.subject_load,
			s.subject_taken,
			s.year_from,
			s.year_to,
			s.academic_year,
			s.teacher_user_id,
			s.class_start,
			s.class_end,
			s.hour,
			s.min,
			s.is_nstp,
			u.name as teacher
			FROM $this->_table s 
			LEFT JOIN master_subjects ms ON ms.ref_id = s.ref_id
			LEFT JOIN users u ON u.id = s.teacher_user_id
			LEFT JOIN rooms r ON r.id = s.room_id
			WHERE s.id = ?
		";
		return $this->query($sql, array($id), true);
	}

	/** Get Subject Profile by Ref ID
	 * @param $ref_id Subject Reference ID
	 * @return object
	 */
	public function get_profile_ref_id($ref_id)
	{
		$sql = "
			SELECT
			ms.ref_id,
			ms.code,
			ms.subject,
			ms.units,
			ms.lab,
			ms.lec,
			s.id,
			s.time,
			s.is_open_time,
			s.day,
			s.course_id,
			s.room_id,
			r.name as room,
			s.year_id,
			s.semester_id,
			s.lab_fee_id,
			s.original_load,
			s.subject_load,
			s.subject_taken,
			s.year_from,
			s.year_to,
			s.academic_year,
			s.teacher_user_id,
			s.class_start,
			s.class_end,
			s.hour,
			s.min,
			s.is_nstp,
			u.name as teacher
			FROM $this->_table s 
			LEFT JOIN master_subjects ms ON ms.ref_id = s.ref_id
			LEFT JOIN users u ON u.id = s.teacher_user_id
			LEFT JOIN rooms r ON r.id = s.room_id
			WHERE s.ref_id = ?
		";
		return $this->query($sql, array($ref_id), true);
	}
	
	public function reset_subject_load($id)
	{
		$this->db->set('subject_load',0)->where('id',$id)->update($this->_table);
		return $this->db->affected_rows() > 0 ? TRUE : FALSE;
	}
	
	public function update_table($data,$id)
	{
		foreach($data as $key => $value)
		{
			$columns_array[] = $key; 
		}
		$columns = implode(',',$columns_array);
		$query = $this->db->select($columns)->where($data)->get($this->_table);
		if($query->num_rows() > 0)
		{
			return true;
		}else
		{
			$this->db->set($data)->where('id',$id)->update($this->_table);
			return $this->db->affected_rows() > 0 ? TRUE : FALSE;
		}
	}
	
	public function update_load($id, $condition, $enrollment_id = false)
	{
		$this->load->model('M_subjects_load_file');
		if ($condition == "remove") 
		{
			$sql = "
				UPDATE subjects SET subjects.subject_load = subjects.subject_load - 1
				WHERE subjects.id = ?
					";
		}
	  
		if($condition == "add")
		{
			$sql = "
				UPDATE subjects SET subjects.subject_load = subjects.subject_load + 1
				WHERE subjects.id = ?
					";
		}
				
		$query = $this->db->query($sql,array($id));
		
		//UPDATE SUBJECTS LOAD FILE ADD OR REMOVE
		$this->M_subjects_load_file->update_subjects_load_file($id, $enrollment_id, $condition);
		
		return $this->db->affected_rows() >= 1 ? TRUE : FALSE;
	}
	
	public function subjects_for_selected_course($c,$y,$s,$yf,$yt,$eid)
	{
		$sql = "	SELECT s.id,s.subject,s.units,s.time,s.day,s.code,s.room
					WHERE s.year_from = ?
					AND s.year_to = ?
					AND s.course_id = ?
					AND s.year_id = ?
					AND s.semester_id = ?
					";
		$query = $this->db->query($sql,array($yf,$yt,$c,$y,$s,$eid));
		return $query->num_rows() > 0 ? $query->result() : FALSE; 
	}
	
	public function subjects_for_selected_semester($s,$yf,$yt)
	{
		$sql = "	SELECT s.id,s.subject,s.units,s.time,s.day,s.code,s.room, s.sc_id
					FROM subjects as s
					WHERE s.year_from = ?
					AND s.year_to = ?
					AND s.semester_id = ?
					ORDER BY s.code
					";
		$query = $this->db->query($sql,array($yf,$yt,$s));
		return $query->num_rows() > 0 ? $query->result() : FALSE; 
	}
	
	public function subjects_count($s,$yf,$yt)
	{
		$sql = "SELECT count(s.id) as total
					FROM subjects as s
					WHERE s.year_from = ?
					AND s.year_to = ?
					AND s.semester_id = ?
					ORDER BY s.code ASC
					";
		$query = $this->db->query($sql,array($yf,$yt,$s));
		return $query->num_rows() > 0 ? $query->row()->total : FALSE; 
	}
	
	//Pagination
	public function subjectlist($s, $yf, $yt, $eid, $start=0,$limit=100)
	{
			$start = $this->db->escape_str($start);
			$limit = $this->db->escape_str($limit);
			$sql = 	"SELECT s.id, s.code, s.subject, s.units, s.time, s.time, s.day, s.room, s.sc_id, s.subject_load, s.lab, s.lec
					  FROM subjects as s
					  WHERE s.year_from = ?
					  AND s.year_to = ?
					  AND s.semester_id = ?
					  AND s.id NOT IN (SELECT subject_id FROM studentsubjects WHERE studentsubjects.enrollmentid = ? )
					  ORDER BY s.code ASC
						LIMIT ".$limit.",".$start;
			$query = $this->db->query($sql,array($yf, $yt, $s, $eid));
			
		if ($query->num_rows() > 0) {
	            foreach ($query->result() as $row)
				{
	                $data[] = $row;
	            }
	            return $data;
	        }
	        return false;
	}
	
	// End pagination
	
	public function current_batch_subject($year_from, $year_to, $semester)
	{
		$sql = "SELECT subjects.sc_id, subjects.id,subjects.subject,subjects.units,subjects.time,subjects.day,subjects.code,subjects.room
					FROM subjects 
					WHERE subjects.year_from = ?
					AND subjects.year_to = ?
					AND subjects.semester_id = ?
					";
		$query = $this->db->query($sql,array($year_from, $year_to, $semester));
		
		return $query->num_rows() > 0 ? $query->result() : FALSE; 
	}
	
	public function create_subjects($data)
	{
		$this->db->insert($this->_table,$data);
		return $this->db->affected_rows() > 0 ? array('status'=>'true','id'=>$this->db->insert_id()) : array('status'=>'false');
	}

	/**
	 * Create Subjects From Post Data
	 * @param array $post POSTED from form
	 * @return object insert details
	 */
	public function create_subject_from_post($post, $id = false)
	{
		$data = $this->input->post('subjects');
		$data['subject_load'] = $data['original_load'];
		$data['is_nstp'] = 0;
		$data['specialization_id'] = $this->input->post('specialization_id');
		if(isset($_POST['is_nstp']))
		{
			$data['is_nstp'] = 1;
		}
		$data['is_open_time'] = 0;
		if(isset($_POST['is_open_time'])){
			$data['is_open_time'] = 1;
		}
		
		$this->load->model('M_academic_years');
		$year = $this->M_academic_years->get_academic_years($this->input->post('academic_year_id'));
		if($year){
			$data['year_from'] = $year->year_from;
			$data['year_to'] = $year->year_to;
		}
		
		if($id === false){ // ADD
			// vd($data);
			$rs = $this->insert($data);
			if($rs['status']){
				$subject_id = $rs['id'];
				activity_log('Subject Schedule Add',false,"Subjects Table Id:$subject_id Data : ".arr_str($data));
			}
		}else{ // EDIT
			// ud($data);
			$rs = $this->update($id, $data);
			activity_log('Subject Schedule Update',false,"Subjects Table Id:$id Data : ".arr_str($data));
		}

		return $rs;
	}
	
	public function update_subjects($data = false, $where = false)
	{
		$this->db->set($data)->where('id',$where)->update($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function delete_subjects($where = false)
	{
		$this->db->where('id',$where)->delete($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
    public function fetch_subjects($limit, $start) {
        $this->db->limit($limit, $start);
		$this->db->order_by('subject');
		
        $input = $this->session->userdata('search_subject');
		unset($input['search_subjects']);
		
        $query = $this->db->select(array('id','sc_id','code','subject','units','lec','lab','time','day','room','subject_load','original_load','year_to','year_from'))->or_like($input)->get('subjects');
		
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   
    public function fetch_subjects2($limit, $start) {
        $this->db->limit($limit, $start);
		
        $input = $this->session->userdata('search_subject');
		unset($input['search_subjects']);
		
		$filter = " WHERE TRUE ";
		
		foreach($input as $field => $value){
			if(trim($value) != ""){
				$filter .= " AND ".$field." LIKE '%".$value."%' ";
			}
		}
		
		
		$sql = "select 	
			    'id',
				'sc_id',
				'code',
				'subject',
				'units',
				'lec',
				'lab',
				'time',
				'day',
				'room',
				'subject_load',
				'original_load',
				'year_to',
				'year_from'
				FROM $this->_table
				$filter
				ORDER BY subject";
				var_dump($sql);
		$query = $this->db->query($sql);
		
		if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   
   	public function search_student_subject($data = false,$start = NULL,$limit = NULL,$search_count = false )
	{
		$ci =& get_instance();
		$year_f = $this->db->escape_str($ci->open_semester->year_from);
		$year_t = $this->db->escape_str($ci->open_semester->year_to);
		$sem_id = $this->db->escape_str($ci->open_semester->id);
		$start = $this->db->escape_str($start);
		$limit = $this->db->escape_str($limit);
		if($data == false AND $start !== false)
		{
			$sql = 'SELECT id,sc_id,subject,code,time,day,room,units,lec,lab,`load` FROM '.$this->_table.' 
					WHERE year_from = '.$year_f.' 
					AND year_to = '.$year_t. ' 
					AND semester_id = '.$sem_id;
			if($start == null OR $limit == null){
			}
			else{
				$sql .= ' LIMIT '.$limit.','.$start;
			}
			// vd($this->db->last_query());	
			return $this->db->query($sql)->result();
			
		}elseif($data !== false)
		{
			unset($data['search_subjects']);
			foreach($data as $k => $v)
			{
				if(!empty($v))
				{
					$where[$k] = '%'.$v.'%';
				}
			}
				
			$cols = implode(' like ? OR ',array_keys($where)).' like ? ';
			$vals = array_values($where);
			
			$sql = 'SELECT id,sc_id,subject,code,time,day,room FROM '.$this->_table.'
					WHERE year_from = '.$year_f.' 
					AND year_to = '.$year_t. ' 
					AND semester_id = '.$sem_id.'
					AND '.$cols.' LIMIT '.$limit.','.$start;
			
			return $this->db->query($sql,$vals)->result();
			
		}
	}
	
	public function count_subjects_for_current_open_semester($data = false)
	{
		$ci =& get_instance();
		$year_f = $this->db->escape_str($ci->open_semester->year_from);
		$year_t = $this->db->escape_str($ci->open_semester->year_to);
		$sem_id = $this->db->escape_str($ci->open_semester->id);
	
		if($data == false)
		{
			$sql = 'SELECT count(id) as totals FROM '.$this->_table.' 
					WHERE year_from = '.$year_f.' 
					AND year_to = '.$year_t. ' 
					AND semester_id = '.$sem_id;		
			return $this->db->query($sql)->row()->totals;			
		}else{
		
			unset($data['search_subjects']);
			foreach($data as $k => $v)
			{
				if(!empty($v))
				{
					$where[$k] = '%'.$v.'%';
				}
			}
			$cols = implode(' like ? OR ',array_keys($where)).' like ? ';
			$vals = array_values($where);
			
			$sql = 'SELECT count(id) as totals FROM '.$this->_table.'
					WHERE year_from = '.$year_f.' 
					AND year_to = '.$year_t. ' 
					AND semester_id = '.$sem_id.'
					 AND '. $cols;
			return $this->db->query($sql,$vals)->row()->totals;				
		}
	}
	
	public function recalculate_subject_load($data, $condition)
	{
	  $this->load->model(array('M_subjects'));
	  
		$sql = "select *
				FROM $this->_table as s
				WHERE s.id = ? 
				";
		$query = $this->db->query($sql,array($data));
		$s = $query->num_rows() > 0 ? $query->row() : FALSE;
		
		
	   $this->M_subjects->update_load($data, $condition);  
	}

	public function set_subject_ref_id()
	{
		unset($get);
		$get['where']['ref_id'] = '';
		$subjects = $this->get_record(false, $get);
		// vd($subjects);
		if($subjects){
			$this->load->model('M_sys_par');
			foreach ($subjects as $key => $value) {
				$ref_id = $this->M_sys_par->get_subj_refid_and_update_to_next_series();
				unset($data);
				$data['ref_id'] = $ref_id;
				$rs = $this->update($value->id, $data);
			}
		}
	}

	#CLONE SUBJECTS OF PREVIOUS ACADEMIC YEAR
	public function cloned_subjects_from_previous_sy($current_academic_year_id =  false, $prev_academic_year_id = false){

		#get the current academic year
			unset($get);
			$get['where']['id'] = $current_academic_year_id;
			$get['single'] = true;
			$c_ay = $this->get_record('academic_years', $get);
			if($c_ay === false){ return;}

		#check if there are already subjects for the current AY - if yes do not continue
			unset($get);
			$get['where']['year_from'] = $c_ay->year_from;
			$get['where']['year_to'] = $c_ay->year_from;
			$rm_chk = $this->get_record(false, $get);
			if($rm_chk){ return false; }


		#get previous academic year
			if($prev_academic_year_id){
				unset($get);
				$get['where']['id'] = $prev_academic_year_id;
				$get['single'] = true;
				$p_ay = $this->get_record('academic_years', $get);
			}else{
				unset($get);
				$get['where']['year_from < '] = $c_ay->year_from;
				$get['single'] = true;
				$p_ay = $this->get_record('academic_years', $get);
			}
			if($p_ay === false){ return;}

		#get subjects in the previous AY
			unset($get);
			$get['where']['year_from'] = $p_ay->year_from;
			$get['where']['year_to'] = $p_ay->year_to;
			$get['array'] = true;
			$prev_subjects = $this->get_record('subjects', $get);
			
			if($prev_subjects){
				foreach ($prev_subjects as $key => $value) {
					$data = $value;
					$value = (object)$value;
					unset($get);
					$get['year_from'] = $c_ay->year_from;
					$get['year_to'] = $c_ay->year_to;
					$get['sc_id'] = $value->sc_id;
					$get['code'] = $value->code;
					$get['course_id'] = $value->course_id;
					$get['year_id'] = $value->year_id;
					$get['semester_id'] = $value->semester_id;

					$exist = $this->pull($get);
					if($exist === false){

						#insert subjects
							unset($data['id']);
							unset($data['created_at']);
							unset($data['updated_at']);
							$data['year_from'] = $c_ay->year_from;
							$data['year_to'] = $c_ay->year_to;
							$rs = $this->insert($data);
							
					}

				}
				
			}
	}

	#CLONE SUBJECTS OF PREVIOUS SEMESTER
	public function cloned_subjects_from_previous_semester($current_academic_year_id =  false, $prev_academic_year_id = false, $current_semester_id = false, $prev_semester_id = false){

		#get the current academic year
			unset($get);
			$get['where']['id'] = $current_academic_year_id;
			$get['single'] = true;
			$c_ay = $this->get_record('academic_years', $get);
			if($c_ay === false){ return;}

		#check if there are already subjects for the current AY - if yes do not continue
			unset($get);
			$get['where']['year_from'] = $c_ay->year_from;
			$get['where']['year_to'] = $c_ay->year_from;
			$rm_chk = $this->get_record(false, $get);
			if($rm_chk){ return false; }


		#get previous academic year
			if($prev_academic_year_id){
				unset($get);
				$get['where']['id'] = $prev_academic_year_id;
				$get['single'] = true;
				$p_ay = $this->get_record('academic_years', $get);
			}else{
				unset($get);
				$get['where']['year_from < '] = $c_ay->year_from;
				$get['single'] = true;
				$p_ay = $this->get_record('academic_years', $get);
			}
			if($p_ay === false){ return;}

		#get prev semester id 
			unset($get);
			$get['field'] = array('id');
			$get['where']['id'] = $current_semester_id;
			$get['single'] = true;
			$current_semester = $this->get_record('semesters', $get);
			if($current_semester === false){ return false; }

		#get current semester id 
			unset($get);
			$get['field'] = array('id');
			$get['where']['id'] = $prev_semester_id;
			$get['single'] = true;
			$prev_semester = $this->get_record('semesters', $get);
			if($prev_semester === false){ return false; }

		#get subjects in the previous AY
			unset($get);
			$get['where']['year_from'] = $p_ay->year_from;
			$get['where']['year_to'] = $p_ay->year_to;
			$get['where']['semester_id'] = $prev_semester_id;
			$get['array'] = true;
			$prev_subjects = $this->get_record('subjects', $get);
			
			if($prev_subjects){
				foreach ($prev_subjects as $key => $value) {
					$data = $value;
					$value = (object)$value;
					unset($get);
					$get['year_from'] = $c_ay->year_from;
					$get['year_to'] = $c_ay->year_to;
					$get['sc_id'] = $value->sc_id;
					$get['code'] = $value->code;
					$get['course_id'] = $value->course_id;
					$get['year_id'] = $value->year_id;
					$get['semester_id'] = $current_semester_id;

					$exist = $this->pull($get);
					if($exist === false){

						#insert subjects
							unset($data['id']);
							unset($data['created_at']);
							unset($data['updated_at']);
							$data['year_from'] = $c_ay->year_from;
							$data['year_to'] = $c_ay->year_to;
							$data['semester_id'] = $current_semester_id;
							$rs = $this->insert($data);
							// vd($rs);
					}
				}
			}
	}

	/**
	 * Check if subject is under the teacher
	 * @param $teacher_id Teacher Used Id
	 * @param $subject_id Subject ID
	 * @return boolean
	 */
	public function check_subject_under_teacher($teacher_id, $subject_id)
	{
		$filter[] = $teacher_id;
		$filter[] = $subject_id;
		$filter[] = $this->ci->cos->user->year_from;
		$filter[] = $this->ci->cos->user->year_to;
		$filter[] = $this->ci->cos->user->semester_id;

		$sql = "
			SELECT 
			id
			FROM subjects
			WHERE teacher_user_id = ?
			AND id = ?
			AND year_from = ?
			AND year_to = ?
			AND semester_id = ?
		";

		return $this->query($sql, $filter) ? TRUE : FALSE;
	}

	/**
	 * Get teachers subject - ready for Dropdown
	 * @param $userid User ID
	 */
	public function get_teachers_subjects($userid)
	{
		$like = false;
		$filter = false;

		$filter['subjects.year_from'] = $this->ci->cos->user->year_from;
		$filter['subjects.year_to'] = $this->ci->cos->user->year_to;
		$filter['subjects.semester_id'] = $this->ci->cos->user->semester_id;
		$filter['subjects.teacher_user_id'] = $userid;
		
		//CONFIGURATION
		$get['fields'] = array(
				'subjects.id',
				'subjects.ref_id',
				'master_subjects.code',
				'master_subjects.subject',
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			0 => array(
				"table" => "master_subjects",
				"on"	=> "master_subjects.ref_id = subjects.ref_id",
				"type"  => "LEFT"
			),
			1 => array(
				"table" => "rooms",
				"on"	=> "rooms.id = subjects.room_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "users",
				"on"	=> "users.id = subjects.teacher_user_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "master_subjects.subject";
		$get['group'] = "master_subjects.ref_id";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
		$rs = $this->get_where_dd("subjects", $get, false, array('ref_id','code','subject'));
		return $rs;
	}
}
