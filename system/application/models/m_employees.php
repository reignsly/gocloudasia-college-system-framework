<?php

class M_employees extends MY_Model{

	protected $_table = "employees";
	protected $_timestamp = false;


  public function __construct()
  {
	parent::__construct();
	$this->load->database();
	$this->_table = "employees";  
	
  }
  
  public function load_employee($id)
  {
  
    $sql = "SELECT employees.id, employees.employeeid, employees.first_name, employees.middle_name, employees.last_name, employees.joining_date, employees.gender, employees.dob, employees.status, employees.martial_status, employees.no_children, employees.father_name, employees.mother_name, employees.spouse_name, employees.role, employees.email, employees.updated_at, employees.created_at
            FROM $this->_table
            Where id = ?
    ";
    
    $query = $this->db->query($sql, array($id));
    return $query->num_rows() > 0 ? $query->row() : FALSE;
  }
  
  public function update_employee($data, $id)
  {
    $this->db->where('id', $id);
    $this->db->update($this->_table, $data); 
    return $this->db->affected_rows() > 0 ? TRUE : FALSE;
  }
  
	public function create_employee($input = false)
	{
		$this->db->insert($this->_table,$input);
		return $this->db->affected_rows() > 0 ? array('status'=>'true','id'=>$this->db->insert_id()) : array('status'=>'false');
	}
	
	public function get_employee($id = false)
	{
		$sql = "select e.*, u.is_activated, u.id as user_id
				FROM $this->_table e
				LEFT JOIN users u on u.login = e.employeeid
				WHERE e.id = ?
				ORDER BY e.id";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	} 
	
	public function get_employee_by_login($login)
	{
		$sql = "select *
				FROM $this->_table
				WHERE employeeid = ?
				ORDER BY id";
		$query = $this->db->query($sql,array($login));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	} 
	
	public function get_employee_profile($id)
	{
		$ret = false;
		
		$pi = $this->get_employee($id);
		// vd($id);
		if($pi != false){
			$ret['personal_info'] = $pi;
			$ret['career_services'] = $this->get_employee_career_services($id);
			$ret['work_experiences'] = $this->get_employee_work_experiences($id);
			$ret['voluntary_works'] = $this->get_employee_voluntary_works($id);
			$ret['training_programs'] = $this->get_employee_training_programs($id);
			$ret['other_informations'] = $this->get_employee_other_informations($id);
		}
		
		return $ret;
	}
	
	public function get_employee_career_services($id)
	{
		$sql = "select *
				FROM `career_services`
				WHERE employee_id = ?
				ORDER BY id";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function get_employee_work_experiences($id)
	{
		$sql = "select *
				FROM `work_experiences`
				WHERE employee_id = ?
				ORDER BY id";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function get_employee_voluntary_works($id)
	{
		$sql = "select *
				FROM `voluntary_works`
				WHERE employee_id = ?
				ORDER BY id";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function get_employee_training_programs($id)
	{
		$sql = "select *
				FROM `training_programs`
				WHERE employee_id = ?
				ORDER BY id";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function get_employee_other_informations($id)
	{
		$sql = "select *
				FROM `other_informations`
				WHERE employee_id = ?
				ORDER BY id";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
  
  // Activate Employees
	public function load_unactivated_employees()
	{
		$sql = "SELECT users.id, employees.name, employees.role, employees.employeeid, users.is_activated, users.activated_at, users.activation_code
				FROM $this->_table
				LEFT JOIN users on users.login = employees.employeeid
				WHERE users.is_activated = 0
				AND users.employee = 1
		";
		
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
  
	public function load_active_employees()
	{
  
		$sql = "SELECT 
					employees.id, 
					CONCAT(employees.last_name,',',employees.first_name,' ',employees.middle_name) AS `name`,
					employees.role, 
					employees.employeeid
				FROM $this->_table
				LEFT JOIN users on users.login = employees.employeeid
				WHERE users.is_activated = 1
				ORDER BY employees.last_name
		";
		
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->result() : FALSE;
  
	}

	public function get_employees_for_dd($fields = "id, name")
	{
		$rs = $this->load_active_employees();

		$f_array = explode(",", $fields);
		$id = trim($f_array[0]);
		$val = trim($f_array[1]);
		
		$ret[""] = " - Select Employee - ";

		if($rs){
			foreach ($rs as $key => $v) {
				$ret[$v->$id] = ucwords(strtolower($v->$val));
			}
		}
		
		return $ret;
	}	
  
	public function load_active_employees_ids()
	{
  
		$sql = "SELECT employees.id
				FROM $this->_table
				LEFT JOIN users on users.login = employees.employeeid
				WHERE users.is_activated = 1
				AND users.employee = 1
		";
		
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->result_array() : FALSE;
  
	}
	
	public function get_current_user($id)
	{
		$sql = "SELECT users.id as userid, employees.id
				FROM $this->_table
				LEFT JOIN users on employees.employeeid = users.login
				WHERE employees.id = ?";
				
		$query = $this->db->query($sql, array($id));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	public function get_enrollment_by($where, $single = false, $array = false)
	{
			$this->db->where($where);
		
			if(!$array)
			{
				if($single)
				{
					$query = $this->db->get($this->_table, $single);
					return $query->num_rows() > 0 ? $query->row() : FALSE;
				}
				else
				{
					$query = $this->db->get($this->_table, $single);
					return $query->num_rows() > 0 ? $query->result() : FALSE;
				}
			}
			else
			{
				if($single)
				{
					$query = $this->db->get($this->_table, $single);
					return $query->num_rows() > 0 ? $query->first_row('array') : FALSE;
				}
				else
				{
					$query = $this->db->get($this->_table, $single);
					return $query->num_rows() > 0 ? $query->result_array() : FALSE;
				}
			}
	}

	/**
	 * Get Employee Profile
	 * @param int $id employee id
	 */
	public function get_profile($id)
	{
		$sql = "
			SELECT
				e.id,
				e.employeeid,
				e.first_name,
				e.middle_name,
				e.last_name,
				e.gender,
				e.dob,
				u.department,
				CONCAT(last_name, ', ', first_name, ' ',middle_name) as fullname
			FROM $this->_table e
			LEFT JOIN users u ON u.login = e.employeeid
			WHERE e.id = ?
			LIMIT 1
		";

		return $this->query($sql,[$id], true);
	}

	/**
	 * Insert Employee Multiple
	 * @param array $post Posted FORM
	 */
	public function add_by_batch($post)
	{
		$this->load->model('M_users');
		$ctr = 0;	
		foreach( $post['user'] as $k => $v){
			$data = $v['employees_attributes']['personal_info'];
			$o = (object)$data;

			if(
					trim($o->employeeid) &&
					trim($o->role) &&
					trim($o->last_name) &&
					trim($o->middle_name) &&
					trim($o->dob) &&
					trim($o->gender) &&
					trim($o->martial_status)
				)
				{
					//get if existing
					$e = $this->M_users->pull(array('login'=>$o->employeeid));
					if($e){	}
					else{
						
						$d = (array)$o;
						$add_to_emp = (object)$this->insert($d);
						if($add_to_emp->status){
							//add to users
							
							unset($d['dob']);
							unset($d['gender']);
							unset($d['martial_status']);

							$d['login'] = trim($d['employeeid']); unset($d['employeeid']);
							$d['employee_id'] = $add_to_emp->id;
							$d['name'] = trim($d['first_name']).' '.trim($d['middle_name']).' '.trim($d['last_name']);

							unset($d['last_name']);
							unset($d['first_name']);
							unset($d['middle_name']);

							$d['created_at'] = date('Y-m-d H:i:s');
							$d['updated_at'] = date('Y-m-d H:i:s');
							$d['is_activated'] = 1;
							$d['department'] = $d['role']; unset($d['role']);
							$this->login->_generate_password(trim($d['employeeid']));
							$d['crypted_password'] = $this->login->get_password();
							$d['salt'] = $this->login->get_salt();
							$add_u = $this->M_users->create_users($d);
							if($add_u){
								$ctr++;
							}
						}
					}
				}
		}

		if($ctr>0){
			$r['code'] = 's';
			$r['msg'] = 'Success';
		}else{
			$r['code'] = 'e';
			$r['msg'] = 'Failed';
		}
		return (object)$r;
	}
}