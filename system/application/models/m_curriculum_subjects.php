<?php
class M_curriculum_subjects Extends MY_Model
{
	protected $_table = 'curriculum_subjects';
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Current Curriculum of Course
	 * @param int $cours_id Course ID
	 * @return int Curriculum ID
	 */
	public function get_current_course_curriculum($course_id)
	{
		$sql = "SELECT id FROM curriculum WHERE course_id = ? AND is_active = ?";
		$rs = $this->query($sql,[$course_id,1],true);
		return $rs ? $rs->id : false;
	}

	/**
	 * Add Subject to Curriculum from post
	 * Used by Curriculum : asign single and create schedule
	 * @param string $ref_id Subject Ref_id
	 * @param array $post From POST FORM
	 * @param int $curriculum_id Curriculum ID
	 * @return object Save status
	 */
	public function add_curriculum_subject($ref_id, $post, $curriculum_id)
	{
		/** Return Variable */
		$sly = new StdClass;
		$sly->id = 0;
		$sly->msg = "";
		$sly->code = "e";
		$sly->status = false;

		$this->load->model('M_master_subjects');

		$year_id = $post['year_id'];
		$semester_id = $post['semester_id'];

		/** Get Subject*/
		$rs_subj =  $this->M_master_subjects->pull(array('is_deleted'=>0,'ref_id'=>$ref_id),'id,ref_id,subject');
		if($rs_subj==false){ return $sly; }

		/** Check subject if already added in the curriculum_subject */
		unset($get);
		$get['curriculum_id'] = $curriculum_id;
		$get['subject_refid'] = $ref_id;
		$get['year_id'] = $year_id;
		$get['semester_id'] = $semester_id;
		$get['is_deleted'] = 0;
		$exist = $this->pull($get);
		if($exist){ return $sly; }		

		/** Save data */
		unset($data);
		$data['curriculum_id'] = $curriculum_id;
		$data['subject_refid'] = $ref_id;
		$data['subject_id'] = $rs_subj->id;
		$data['year_id'] = $year_id;
		$data['semester_id'] = $semester_id;
		if($post['req_year_id']){
			$data['req_year_id'] = $post['req_year_id'];
		}
		if($post['total_units_req']){
			$data['total_units_req'] = floatval($post['total_units_req']);
		}
		if($post['enrollment_units_req']){
			$data['enrollment_units_req'] = floatval($post['enrollment_units_req']);
		}
		
		$rs = (object)$this->insert($data);

		if($rs->status){

			/** Save Pre-requisite subject if any */
			$subject_pre = $post['subject_pre'];
			$subject_pre_coincide = $post['subject_pre_coincide'];
			if($subject_pre){
				$this->load->model('M_subject_pre_requisite');
				foreach($subject_pre as $k => $subj_refid){
					$rs_subj_ref_id = $this->M_master_subjects->pull(array('ref_id'=>$subj_refid),'id,ref_id');
					
					if($rs_subj_ref_id){

						// check if already added
						unset($get);
						$get['where']['curriculum_id'] = $curriculum_id;
						$get['where']['curriculum_sub_id'] = $rs->id;
						$get['where']['subject_refid'] = $subj_refid;
						$get['where']['is_deleted'] = 0;
						$get['single'] = true;
						$exist = $this->M_subject_pre_requisite->get_record(false, $get);
						if($exist){ continue; }

						//Save to subject_pre_requisite table
						unset($data);
						$data['curriculum_id'] = $curriculum_id;
						$data['curriculum_sub_id'] = $rs->id;
						$data['subject_refid'] = $subj_refid;
						$data['subject_id'] = $rs_subj_ref_id->id;
						$data['taken_at_once'] = $subject_pre_coincide ? ( isset($subject_pre_coincide[$k]) ? 1 : 0 ) : 0 ;
						
						$rs_pre = (object)$this->M_subject_pre_requisite->insert($data);
						
						if($rs_pre->status){

							activity_log('create Subject prerequisite',false,'ID : ' .$rs_pre->id. 'Data : '. arr_str($data)); /*SAVE ACTIVITY*/
						}
					}
				}
			}

			activity_log('assign curriculum subject',false,'ID : ' .$rs->id. 'Data : '. arr_str($data)); /*SAVE ACTIVITY*/

			$sly->code = "s";
			$sly->status = true;
			$sly->msg = "Subject ".$rs_subj->subject." was successfully assigned to the curriculum.";
		}else{
			$sly->msg = "Subject failed to be assigned, please try again.";
		}

		return $sly;
	}

	/**
   * Get Subjects, arrange subjects yearly, semester
	 */ 
	public function get_curriculum_subjects($id,$s_id = false)
	{
		$ret = false;

		// get years
		unset($get);
		$get['order'] = 'level';
		$rs_yr = $this->get_record('years', $get);
		if($rs_yr){
			foreach ($rs_yr as $key => $value) {
				$ret[$value->id]['data'] = $value;
			}
		}

		//loop years
		if($ret){
			foreach ($ret as $year_id => $value) {

				// get semesters 		
				unset($get);
				$get['order'] = 'level';
				$rs_semester = $this->get_record('semesters', $get);
				if($rs_semester){
					foreach ($rs_semester as $sem_value) {
						
						$ret[$year_id]['semester'][$sem_value->id]['data'] = $sem_value;

						// get subjects
						unset($get);
						$get['fields'] = array(
								'master_subjects.*',
								'curriculum_subjects.id as cur_sub_id',
								'curriculum_subjects.total_units_req',
								'curriculum_subjects.enrollment_units_req',
								'years.year as req_year'
							);
						$get['where']['curriculum_subjects.curriculum_id'] = $id;
						$get['where']['curriculum_subjects.year_id'] = $year_id;
						$get['where']['curriculum_subjects.semester_id'] = $sem_value->id;
						$get['where']['curriculum_subjects.is_deleted'] = 0;
						$get['join'][] = array(
								'table' => 'master_subjects',
								'on' => 'master_subjects.ref_id = curriculum_subjects.subject_refid',
								'type' => 'left'
							);
						$get['join'][] = array(
								'table' => 'years',
								'on' => 'years.id = curriculum_subjects.req_year_id',
								'type' => 'left'
							);
						$rs_subjects = $this->get_record('curriculum_subjects', $get);
						if($rs_subjects){
							$ret[$year_id]['semester'][$sem_value->id]['subjects'] = $rs_subjects;
							$ret[$year_id]['semester'][$sem_value->id]['curriculum_sub_id'] = $rs_subjects;
						}
					}
				}
			}
		}

		return $ret;
	}

	/**
	 * Get Curriculum Subject for Dropdown
	 * @param $id Curriculum ID
	 * @return array
	 */
	public function get_curriculum_subject_for_dd($id)
	{
		$sql = "
			SELECT
				cs.id,
				s.sc_id,
				s.subject
			FROM $this->_table cs
			RIGHT JOIN subjects s ON s.id = cs.subject_id
			WHERE cs.curriculum_id = ?
			AND cs.is_deleted = 0
			ORDER BY s.sc_id
		";
		$rs = $this->get_qry_dd($sql, array($id), false, array('id','sc_id','subject'));
		return $rs;
	}

	// Get Subject by id
	public function get_profile($id)
	{
		unset($get);
		$get['fields'] = array(
				'curriculum_subjects.id',
				'curriculum_subjects.req_year_id',
				'curriculum_subjects.total_units_req',
				'curriculum_subjects.enrollment_units_req',
				'curriculum_subjects.year_id',
				'curriculum_subjects.semester_id',
				'years.year',
				'semesters.name as semester',
				'master_subjects.ref_id',
				'master_subjects.code',
				'master_subjects.subject',
				'master_subjects.units',
				'master_subjects.lec',
				'master_subjects.lab'
			);
		$get['where']['curriculum_subjects.id'] = $id;
		$get['where']['curriculum_subjects.is_deleted'] = 0;
		$get['join'][] = array(
				'table' => 'master_subjects',
				'on' => 'master_subjects.ref_id = curriculum_subjects.subject_refid',
				'type' => 'left'
			);
		$get['join'][] = array(
				'table' => 'years',
				'on' => 'years.id = curriculum_subjects.year_id',
				'type' => 'left'
			);
		$get['join'][] = array(
				'table' => 'semesters',
				'on' => 'semesters.id = curriculum_subjects.semester_id',
				'type' => 'left'
			);

		$get['single'] = true;
		return $this->get_record(false, $get);
	}

	// get total of unit lab and lec
	public function get_unit_totals($id)
	{
		$ret = false;

		$sql = "SELECT 
					SUM(master_subjects.units) as units,
					SUM(master_subjects.lec) as lec,
					SUM(master_subjects.lab) as lab
				FROM $this->_table
				LEFT JOIN master_subjects ON master_subjects.ref_id = $this->_table.subject_refid
				WHERE $this->_table.curriculum_id = ?
				AND $this->_table.is_deleted = 0
		";

		$q = $this->db->query($sql, array($id));

		if($q->num_rows() > 0){

			$ret = new StdClass;
			$rs = $q->row();
			$ret->units = $rs->units;
			$ret->lec = $rs->lec;
			$ret->lab = $rs->lab;
		}
		
		return $ret;
	}
}
