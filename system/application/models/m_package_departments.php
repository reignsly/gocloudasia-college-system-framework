<?php
class M_package_departments Extends MY_Model
{
	protected $_table = 'package_departments';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_departments($p_id)
	{
		$sql = "
			SELECT
				pd.*,
				d.department,
				d.description
			FROM $this->_table pd 
			LEFT JOIN departments d ON d.id = pd.department_id
			WHERE pd.package_id = ?
			ORDER BY d.department
		";
		return $this->query($sql, array($p_id));
	}

	public function get_departments_dd($p_id)
	{
		$sql = "
			SELECT
				pd.id,
				pd.package_id,
				pd.department_id,
				d.department,
				d.description
			FROM $this->_table pd 
			LEFT JOIN departments d ON d.id = pd.department_id
			WHERE pd.package_id = ?
			ORDER BY d.department
		";
		return $this->get_qry_dd($sql, array($p_id),false, array('department_id','department'));
	}

	public function get_departments_string($p_id)
	{
		$d = $this->get_departments($p_id);
		$r = false;
		if($d){
			foreach ($d as $k => $v) {
				$r[] = uc_words(str_replace('_', " ", $v->department));
			}
		}
		return implode(', ', $r);
	}

	/**
	 * Check if Package has department 
	 * @param int $p_id Package ID
	 * @param int $d_id Department ID
	 * @param bool $ret_obj if return the row or boolean
	 * @return boolean/object TRUE/FALSE
	 */
	public function has_department($p_id, $d_id, $ret_obj = false)
	{
		$sql = "
			SELECT *
			FROM $this->_table
			WHERE package_id = ?
			AND department_id = ?
		";

		if($ret_obj){
			return $this->query($sql, array($p_id, $d_id), true);
		}

		return $this->query($sql, array($p_id, $d_id)) ? TRUE : FALSE;
	}
}