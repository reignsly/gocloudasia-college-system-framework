<?php
class M_student_scholarship Extends MY_Model
{

	protected $_table = 'student_scholarships';
	protected $_uid = "id";
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_student_scholarship($eid)
	{
		$sql = "
			SELECT 
			*
			FROM $this->_table
			WHERE ID NOT IN(
				SELECT scholarship_id
				FROM student_scholarship_record
				WHERE enrollment_id = ?
			)
			ORDER BY `name`
		";
		$q = $this->db->query($sql, array($eid));
		return $q->num_rows() > 0 ? $q->result() : false;
	}
}
