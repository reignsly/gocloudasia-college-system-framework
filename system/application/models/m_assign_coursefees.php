<?php
	
class M_assign_coursefees Extends MY_Model
{
	protected $_table = 'assign_coursefees';
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_assign_course_fee($data)
	{
		$sql = "SELECT  
							coursefinance_id 
						FROM assign_coursefees 
						WHERE year_id = ?  
						AND course_id = ? 
						AND semester_id = ? 
						AND academic_year_id = ?
						LIMIT 1";
		$query = $this->db->query($sql,$data);
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	public function get($id = false,$array = false)
	{
		if($id == false)
		{
				if($array == false)
				{
					$query = $this->db->get($this->_table);
					return $query->num_rows() > 0 ? $query->result() : FALSE;
				}else
				{
					$query = $this->db->select($array)->get($this->_table);
					return $query->num_rows() > 0 ? $query->result() : FALSE;
				}
		}else
		{
			if($array == false)
			{
				$query = $this->db->where('id',$id)->get($this->_table);
				return $query->num_rows() > 0 ? $query->row() : FALSE;
			}else
			{
				$query = $this->db->select($array)->where('id',$id)->get($this->_table);
				return $query->num_rows() > 0 ? $query->row() : FALSE;
			}
		}
		
	}
	
	public function get_all()
	{
		//print_r($data);
		$sql = "select message, date
				FROM $this->_table
				ORDER BY created_at desc LIMIT 15";
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function get_all_by_coursefinance_id($id = false)
	{
		$sql = "select
				$this->_table.id,
				$this->_table.year_id,
				years.year,
				$this->_table.course_id,
				courses.course,
				courses.course_code,
				semesters.name as semester
				FROM $this->_table
				LEFT JOIN years ON years.id = $this->_table.year_id
				LEFT JOIN courses on courses.id = $this->_table.course_id
				LEFT JOIN semesters ON $this->_table.semester_id = semesters.id
				WHERE $this->_table.coursefinance_id = ?
				ORDER BY $this->_table.id";
				// vd($sql);
		$query = $this->db->query($sql, array($id));
		
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function find_all()
	{
		//print_r($data);
		$sql = "select *
				FROM $this->_table
				ORDER BY id";
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function create_assign_coursefees($input = false)
	{
		$this->db->insert($this->_table,$input);
		return $this->db->affected_rows() > 0 ? array('status'=>'true','id'=>$this->db->insert_id()) : array('status'=>'false');
	}
	
	public function get_assign_coursefees($id = false)
	{
		$sql = "select *
				FROM $this->_table
				WHERE id = ?
				ORDER BY id";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	public function update_assign_coursefees($data = false, $where = false)
	{
		$this->db->set($data)->where('id',$where)->update($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function delete_assign_coursefees($where = false)
	{
		$this->db->where('id',$where)->delete($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}

	/**
	 * Assign Course from Post
	 *
	 */
	public function assigned_course($id, $post)
	{
		$sly = new stdClass;
		$sly->code = "e";
		$sly->msg = "No Course was added, either all courses were already added or the transaction failed.";

		$create_all_years = isset($post['create_all_year']) ? TRUE : FALSE;
		$create_all_semesters = isset($post['create_all_semester']) ? TRUE : FALSE;

		$assign_coursefee = (object)$post['assign_coursefee'];
		$post = $post['assign_coursefee'];
		
		

		if($create_all_years){
			$year_ids = $this->get_record('years');
		}else{
			$r['id'] = $post['year_id'];
			$year_ids[] = (object)$r;
		}

		if($create_all_semesters){
			$semester_ids = $this->get_record('semesters');
		}else{
			$r['id'] = $post['semester_id'];
			$semester_ids[] = (object)$r;
		}

		$ctr = 0;
		
		foreach ($year_ids as $k => $y) {
			foreach ($semester_ids as $sk => $s) {
				
				//check if added
				unset($get);
				$get['where']['year_id'] = $y->id;
				$get['where']['semester_id'] = $s->id;
				$get['where']['course_id'] = $assign_coursefee->course_id;
				$get['where']['coursefinance_id'] = $id;
				$get['where']['academic_year_id'] = $assign_coursefee->academic_year_id;
				$rs_x = $this->get_record($this->_table,$get);

				if($rs_x == false){

					//insert
					unset($save);
					$save['coursefinance_id'] = $id;
					$save['academic_year_id'] = $assign_coursefee->academic_year_id;
					$save['semester_id'] = $s->id;
					$save['year_id'] = $y->id;
					$save['course_id'] = $assign_coursefee->course_id;
					
					$add = (object)$this->insert($save);
					if($add->status){

						activity_log('Assign Course to Course Finance', false, "$this->_table id : $add->id. Data : ".arr_str($save));

						$ctr++;
					}
				}
			}
		}

		if($ctr>0){
			$sly->code = "s";
			$sly->msg = "Course was successfully assigned to coursefinance.";
		}

		return $sly;
	}
}