<?php

class M_payment_plan_req Extends MY_Model
{
	protected $_table = 'payment_plan_req';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function delete_record_by_plan_id($id){
		$this->db->where('payment_plan_id', $id);
		$this->db->delete($this->_table);
		return $this->db->affected_rows() > 0 ? true : false;
	}

	public function get_record_by_plan_id($id){
		$sql ="
			SELECT
			*
			FROM $this->_table
			WHERE payment_plan_id = ?
			ORDER BY id";
		$query = $this->db->query($sql, array($id));
		return $query->num_rows() > 0 ? $query->result() : false;
	}
}