<?php

class M_grading_periods Extends MY_Model
{
	protected $_table = 'grading_periods';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function get($id = false,$array = false)
	{
		if($id == false)
		{
				if($array == false)
				{
					$query = $this->db->get($this->_table);
					return $query->num_rows() > 0 ? $query->result() : FALSE;
				}else
				{
					$query = $this->db->select($array)->get($this->_table);
					return $query->num_rows() > 0 ? $query->result() : FALSE;
				}
		}else
		{
			if($array == false)
			{
				$query = $this->db->where('id',$id)->get($this->_table);
				return $query->num_rows() > 0 ? $query->row() : FALSE;
			}else
			{
				$query = $this->db->select($array)->where('id',$id)->get($this->_table);
				return $query->num_rows() > 0 ? $query->row() : FALSE;
			}
		}
		
	}
	
	public function find_all()
	{
		$sql = "select 	
					id,
						grading_period,
						is_set,
						orders,
						created_at,
						updated_at
				FROM $this->_table
				WHERE is_deleted = 0
				ORDER BY orders";
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function create_grading_periods($input = false)
	{
		$this->db->insert($this->_table,$input);
		return $this->db->affected_rows() > 0 ? array('status'=>'true','id'=>$this->db->insert_id()) : array('status'=>'false');
	}
	
	public function get_grading_periods($id = false)
	{
		$sql = "select *
				FROM $this->_table
				WHERE id = ?
				ORDER BY orders";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	public function update_grading_periods($data = false, $where = false)
	{
		$this->db->set($data)->where('id',$where)->update($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function delete_grading_periods($where = false)
	{
		$u['is_deleted'] = 1;
		$u['deleted_by'] = $this->userid;
		$u['date_deleted'] = NOW;
		$this->db->where('id',$where)->update($this->_table, $u);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function reset_grading_period(){
		$sql = "UPDATE
				$this->_table
				SET is_set = ?";
		$query = $this->db->query($sql,array('0'));
	}
	
	public function get_current_grading_period()
	{
		$sql = "
			SELECT *
			FROM $this->_table
			WHERE is_set = ?
			AND is_deleted = 0
		";
		
		$query = $this->db->query($sql, array(1));
		
		return $query->num_rows() > 0 ? $query->row() : false;
	}
	
	public function get_next_period_according_to_order()
	{
		$ret = false;
		
		$current_period = $this->current_period();
		
		if($current_period)
		{
			$sql = "
				SELECT *
				FROM $this->_table
				WHERE orders >= ?
				AND id <> ?
				AND is_deleted = 0
			";
			
			$query = $this->db->query($sql, array($current_period->orders, $current_period->id));
			
			$ret = $query->num_rows() > 0 ? $query->row() : false;
		}
		
		return $ret;
	}
	
	public function get_all_array(){
		$sql = "select 	
					id,
						grading_period,
						is_set,
						orders,
						created_at,
						updated_at
				FROM $this->_table
				WHERE is_deleted = 0
				ORDER BY orders";
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->result_array() : FALSE;
	}

	public function get_all(){
		$sql = "select 	
						id,
						grading_period
				FROM $this->_table
				WHERE is_deleted = 0
				ORDER BY orders";
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}

	/**
	 * Is Last Grading Period - it means final grading period
	 * used order to sort
	 * @param  int $id
	 * @return boolean     true/false
	 */
	public function is_current_last_gp()
	{
		$rs = $this->get_current_grading_period();
		$last = $this->get_last_gp();
		if($rs->id == $last->id){
			return true;
		}
		return false;
	}

	public function get_last_gp()
	{
		$sql = "
			SELECT 
			*
			FROM $this->_table
			ORDER BY orders DESC
			LIMIT 1
		";
		return $this->query($sql,false,true);
	}
}