<?php
class M_curriculum Extends MY_Model
{
	protected $_table = 'curriculum';
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Current Curriculum of Course
	 * @param int $cours_id Course ID
	 * @return int Curriculum ID
	 */
	public function get_current_course_curriculum($course_id,$specialization_id=false)
	{
		$this->db->select('id')->where('course_id',$course_id);
		if($specialization_id){
			$this->db->where('specialization_id',$specialization_id);
		}
		$rs = $this->db->get($this->_table);
		return $rs->num_rows > 0 ? $rs->row()->id : false;
	}
	
	public function get_curriculum($id,$s_id = false){
		unset($get);
		$get['fields'] = array(
				'curriculum.id',
				'curriculum.name',
				'curriculum.units',
				'curriculum.lec',
				'curriculum.lab',
				'curriculum.is_deleted',
				'curriculum.course_id',
				'course_specialization.specialization',
				'curriculum.specialization_id as major_id',
				'courses.course',
				'academic_years.year_from',
				'academic_years.year_to',
			);
		$get['where']['curriculum.id'] = $id;
		if($s_id){
			$get['where']['curriculum.specialization_id'] = $s_id;
		}
		$get['join'][] = array(
				'table' => 'courses',
				'on' => 'courses.id = curriculum.course_id',
				'type' => 'left',
			);
		$get['join'][] = array(
				'table' => 'academic_years',
				'on' => 'academic_years.id = curriculum.academic_year_id',
				'type' => 'left',
			);
		$get['join'][] = array(
				'table' => 'course_specialization',
				'on' => 'course_specialization.id = curriculum.specialization_id',
				'type' => 'left',
			);
		$get['order'] = "curriculum.name";
		$get['single'] = true;
		return $this->get_record(false, $get);
	}

	public function update_curriculum_units($id)
	{
		// get curriculum
		$curriculum = $this->get_curriculum($id);
		if($curriculum === false){ return false; }

		// counts unit/lec/lab
		$this->load->model('M_curriculum_subjects');
		$c_obj = $this->M_curriculum_subjects->get_unit_totals($id);

		if($c_obj){
			unset($data);
			$data['units'] = $c_obj->units;
			$data['lec'] = $c_obj->lec;
			$data['lab'] = $c_obj->lab;
			$rs = $this->update($id, $data);
			return $rs;
		}
	}

	/**
	 * Re-calculate total units
	 * @param $id Curiculuum ID
	 */
	public function recalculate_units($id)
	{
		$c = $this->get($id);

		if($c){

			$sql = "
				SELECT 
				 SUM(s.units) AS units,
				 SUM(s.lec) AS lec,
				 SUM(s.lab) AS lab
				 FROM `curriculum_subjects` cs
				 LEFT JOIN master_subjects s ON cs.`subject_refid` = s.ref_id
				 WHERE cs.`curriculum_id` = ?
				 AND cs.is_deleted = 0;
			";

			$rs_sum = $this->query($sql, array($id), true);
			// vd($this->db->last_query());
			if($rs_sum){
				$data = false;
				if($c->units != $rs_sum->units){
					$data['units'] = $rs_sum->units;
				}if($c->lab != $rs_sum->lab){
					$data['lab'] = $rs_sum->lab;
				}if($c->lec != $rs_sum->lec){
					$data['lec'] = $rs_sum->lec;
				}
				if($data){
					return $this->update($id, $data);
				}
			}
			return true;
		}

		return false;
	}

	public function activate($id)
	{
		$c = $this->get_curriculum($id);
		if($c){
			
			// de-activate within its group (course_id, specialization_id)
			unset($data);
			$data['is_active'] = 0;
			$where = array(
					'course_id' => $c->course_id,
					'specialization_id' => $c->major_id
				);
			$this->update($where, $data);

			// then activate the record with the id
			$data['is_active'] = 1;
			$rs = $this->update($id, $data);
			if($rs){
				activity_log('Activate Curriculum',false, ' Curriculum ID : '.$id);
				return true;
			}
		}
		return false;
	}

	/**
	 * Activate curriculum if it does not have any other alike
	 * @param  int $id curriculum id
	 */
	public function activate_if_possible($id)
	{
		$c = $this->get_curriculum($id);

		//get if there are alike
		$get['where']['id <> '] = $id;
		$get['where']['course_id'] = $c->course_id;
		$get['where']['specialization_id'] = $c->major_id;
		$get['where']['is_deleted'] = 0;
		$alike = $this->get_record(false, $get);
		
		if($alike == false){
			unset($data);
			$data['is_active'] = 1;
			$rs = $this->update($id, $data);
			return $rs;
		}
		return false;
	}

	public function deactivate($id)
	{
		$c = $this->get_curriculum($id);
		if($c){
			
			unset($data);
			$data['is_active'] = 0;
			$rs = $this->update($id, $data);

			if($rs){
				activity_log('Deactivate Curriculum',false, ' Curriculum ID : '.$id);
				return true;
			}
		}
		return false;
	}
}
