<?php

class M_coursefinances Extends MY_Model
{
	protected $_table = 'coursefinances';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function find_all($array = false)
	{
		$method = 'result';
		if($array){ $method = 'result_array'; }
		$sql = "select *
				FROM $this->_table
				ORDER BY category2";
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->$method() : FALSE;
	}
	
	public function create_coursefinances($input = false)
	{
		$this->db->insert($this->_table,$input);
		return $this->db->affected_rows() > 0 ? array('status'=>'true','id'=>$this->db->insert_id()) : array('status'=>'false');
	}
	
	public function get_coursefinances($id = false)
	{
		$sql = "select *
				FROM $this->_table
				WHERE id = ?
				ORDER BY id";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	public function get_coursefinances_category($category = false)
	{
		$sql = "select category2
				FROM $this->_table
				WHERE category = ?
				LIMIT 1";
		$query = $this->db->query($sql,array($category));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
		
		
	}
	
	public function update_coursefinances($data = false, $where = false)
	{
		$this->db->set($data)->where('id',$where)->update($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function delete_coursefinances($where = false)
	{
		$this->db->where('id',$where)->delete($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}

	#CLONE COURSE FINANCE OF PREVIOUS ACADEMIC YEAR
		public function cloned_coursefinance_from_previous_sy($current_academic_year_id =  false, $prev_academic_year_id = false){

			#get the current academic year
				unset($get);
				$get['where']['id'] = $current_academic_year_id;
				$get['single'] = true;
				$c_ay = $this->get_record('academic_years', $get);
				if($c_ay === false){ return;}


			#get previous academic year
				if($prev_academic_year_id){
					unset($get);
					$get['where']['id'] = $prev_academic_year_id;
					$get['single'] = true;
					$p_ay = $this->get_record('academic_years', $get);
				}else{
					unset($get);
					$get['where']['year_from < '] = $c_ay->year_from;
					$get['single'] = true;
					$p_ay = $this->get_record('academic_years', $get);
				}
				if($p_ay === false){ return;}

			#get course finance in the previous AY
				unset($get);
				$get['where']['academic_year_id'] = $p_ay->id;
				$get['array'] = true;
				$prev_cf = $this->get_record(false, $get);
				// vd($c_ay);
				if($prev_cf){
					foreach ($prev_cf as $key => $value) {
						$data = $value;
						$value = (object)$value;

						#check if already in the current AY if not Create the record
						unset($get);
						$get['academic_year_id'] = $c_ay->id;
						$get['semester_id'] = $value->semester_id;
						$exist = $this->pull($get);
						if($exist === false){

							#insert new coursefinances
								unset($data['id']);
								unset($data['created_at']);
								unset($data['updated_at']);
								$data['academic_year_id'] = $c_ay->id;
								$rs = (object)$this->insert($data);
								if($rs->status === false){ return false; }

								#clone also the coursefees tagged in the previous AY
									unset($get);
									$get['where']['coursefinance_id'] = $value->id;
									$get['order'] = 'id';
									$get['not_in'] = array(
											'field' => 'fee_id',
											'data' => "SELECT fee_id FROM coursefees WHERE coursefinance_id = '$rs->id'"
										);
									$rs_course_fees = $this->get_record('coursefees', $get);
									
									if($rs_course_fees){
										$this->load->model('M_coursefees');
										foreach ($rs_course_fees as $cf_k => $cf_val) {
											unset($data);
											$data['coursefinance_id'] = $rs->id;
											$data['fee_id'] = $cf_val->fee_id;
											$data['value'] = $cf_val->value;
											$data['position'] = $cf_val->position;
											$this->M_coursefees->insert($data);
										}
									}

								#clone also the assign coursefees
									unset($get);
									$get['where']['coursefinance_id'] = $value->id;
									$rs_ass_cf = $this->get_record('assign_coursefees', $get);
									if($rs_ass_cf){
										$this->load->model('M_assign_coursefees');
										foreach ($rs_ass_cf as $ass_cf_k => $ass_cf_val) {
											unset($data);
											$data['coursefinance_id'] = $rs->id;
											$data['course_id'] = $ass_cf_val->course_id;
											$data['year_id'] = $ass_cf_val->year_id;
											$data['academic_year_id'] = $c_ay->id;
											$data['semester_id'] = $value->semester_id;
											$this->M_assign_coursefees->insert($data);
										}
									}
						}
					}
				}
		}

}