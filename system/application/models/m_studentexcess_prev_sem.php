<?php

class M_studentexcess_prev_sem Extends MY_Model
{
	protected $_table = 'studentexcess_prev_sem';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function get_studentexcess_by_eid($enrollment_id = false)
	{
		if(!$enrollment_id) { return false; }
		
		$sql = "
			SELECT
			se.*,
			e.sy_from,
			e.sy_to,
			y.year,
			c.course
			FROM $this->_table se
			LEFT JOIN enrollments e ON se.enrollment_id = e.id
			LEFT JOIN years y ON y.id = e.year_id
			LEFT JOIN courses c ON c.id = e.course_id
			WHERE se.enrollment_id = ?
		";
		
		$query = $this->db->query($sql, array($enrollment_id));
		
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	
	public function get_sum_studentexcess_by_eid($enrollment_id = false)
	{
		if(!$enrollment_id) { return 0; }
		
		$return = 0;
		
		$sql = "
			SELECT
			sum(amount) AS sum_amount
			FROM $this->_table
			WHERE enrollment_id = ?
		";
		
		$query = $this->db->query($sql, array($enrollment_id));
		
		$rs = $query->num_rows() > 0 ? $query->row() : false;
		
		if($rs):
		$return = ($rs->sum_amount) ? $rs->sum_amount : 0;
		endif;
		
		return $return;
		
	}
}