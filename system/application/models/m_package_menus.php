<?php
class M_package_menus Extends MY_Model
{
	protected $_table = 'package_menus';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Menu Group Dropdown of Package and department
	 * @param int $p_id Package ID
	 * @param int $d_id Department ID
	 * @return array
	 */
	public function get_menugroup_dd($p_id, $d_id)
	{
		$sql = "
			SELECT
			*
			FROM $this->_table pm
			WHERE pm.menu_lvl = ?
			AND pm.package_id = ?
			AND pm.department_id = ?
			ORDER BY pm.caption
		";

		$param[] = 1;
		$param[] = $p_id;
		$param[] = $d_id;

		return $this->get_qry_dd($sql, $param, '',array('id','caption'));
	}

	/**
	 * Get Menus by Package ID
	 * @param int $p_id Package ID
	 * @param boolean $array if result is object or array
	 * @return array of objects
	 */
	public function get_menus($p_id, $array = false)
	{
		$sql = "
			SELECT
			*
			FROM $this->_table
			WHERE package_id = ?
			ORDER BY id,menu_num
		";

		$rs = $this->db->query($sql, array($p_id));

		if($array){
			return $rs->num_rows() > 0 ? $rs->result_array() : false;
		}else{
			return $rs->num_rows() > 0 ? $rs->result() : FALSE;
		}
	}

	/**
	 * Get Package Menus by Department
	 * @param int $p_id Package ID
	 * @param int $dep_id Department ID
	 * @return array of object
	 */
	public function get_department_menus($p_id, $dep_id)
	{
		$menus = false;
		#step 1: Get All Header Menus (menu_lvl = 1)
		$sql = "
			SELECT
			*
			FROM $this->_table pm
			WHERE package_id = ?
			AND department_id = ?
			AND menu_lvl = 1
			ORDER BY menu_num
		";
		$menu_group = $this->query($sql, array($p_id, $dep_id));
		// vd($this->db->last_query());
		// vd($menu_group);
		#step 2: Loop Header Menus and Get Sub Menus (menu_lvl = 2)
		if($menu_group){
			foreach ($menu_group as $k => $v) {
				$sql = "
					SELECT
					*
					FROM $this->_table 
					WHERE package_id = ?
					AND department_id = ?
					AND menu_lvl = 2
					AND menu_grp = ?
					AND visible = 1
					ORDER BY menu_num
				";
				$sub_menus = $this->query($sql, array($p_id, $dep_id, $v->menu_sub));

				$menus[] = array(
					'head' => $v,
					'menus' => $sub_menus
					);
			}
		}
		// vd($menus);
		return $menus;
	}

	/**
	 * Get All Package Menus by Package ID and department id
	 * @param int $p_id Package ID
	 * @param int $d_id Department ID
	 * @param boolean $array if result is object or array
	 * @return array of objects
	 */
	public function get_package_menus($p_id, $d_id, $array = false)
	{
		$sql = "
			SELECT
			*
			FROM $this->_table
			WHERE package_id = ?
			AND department_id = ?
			ORDER BY menu_num
		";

		$rs = $this->db->query($sql, array($p_id, $d_id));

		if($array){
			return $rs->num_rows() > 0 ? $rs->result_array() : false;
		}else{
			return $rs->num_rows() > 0 ? $rs->result() : FALSE;
		}
	}
}