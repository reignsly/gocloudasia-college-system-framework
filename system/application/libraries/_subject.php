<?php

class _Subject
{
	private $ci;
	
	public $id;
	public $profile;

	public $current_semester;
	public $current_grading;
	public $current_sy;

	//GLOBAL VARIABLES
	public $userlogin;
	public $userid;
	
	public function __construct($config)
	{
		set_time_limit(0);
		$this->id = $config['id'];
		$this->ci =& get_instance();
		$this->ci->load->model('M_core_model','m');

		$this->load_default_function();
	}

	public function load_default_function()
	{
		$this->load_default();
		$this->load_current_school_profile();

		$this->load_profile();
	}

	private function load_default()
	{
		$this->profile = false;

		$this->current_semester = false;
		$this->current_grading = false;
		$this->current_sy = false;
	}
	
	private function load_profile()
	{
		unset($get);
		$get['fields'] = array(
			'subjects.*',
			'master_subjects.code',
			'master_subjects.subject',
			'master_subjects.units',
			'master_subjects.lab',
			'master_subjects.lec',
			'users.name as instructor'
			);
		$get['where']['subjects.id'] = $this->id;
		$get['single'] = true;
		$get['join'][] = array(
				'table' => 'master_subjects',
				'on' => 'master_subjects.ref_id = subjects.ref_id',
				'type' => 'left'
			);
		$get['join'][] = array(
				'table' => 'users',
				'on' => 'users.id = subjects.teacher_user_id',
				'type' => 'left'
			);
		$this->ci->m->set_table('subjects'); //RESET TO BE USED BY THE MODEL
		$this->profile = $this->ci->m->get_record('subjects', $get);
	}

	/*FUNCTION TO GET CURRENT SEMESTER"|GRADING|SY*/
	public function load_current_school_profile()
	{
		$this->current_semester = $this->get_current_semester();
		$this->current_grading = $this->get_current_grading();
		$this->current_sy = $this->get_current_sy();
	}

	public function get_current_semester()
	{
		unset($get);
		$get['fields'] = array(
				'open_semesters.id',
				'open_semesters.academic_year_id',
				'open_semesters.semester_id',
				'semesters.name as semester'
			);
		$get['where']['use'] = 1;
		$get['all'] = false;
		$get['single'] = true;
		$get['join'][] = array(
				'table' => 'semesters',
				'on' => 'open_semesters.semester_id = semesters.id',
				'type' => 'left'
			);
		return $this->ci->m->get_record('open_semesters',$get);
	} 

	public function get_current_grading()
	{
		unset($get);
		$get['fields'] = array(
				'id',
				'grading_period',
				'orders'
			);
		$get['where']['is_set'] = 1;
		$get['where']['is_deleted'] = 0;
		$get['single'] = true;
		return $this->ci->m->get_record('grading_periods',$get);
	}

	public function get_current_sy()
	{
		$open_semester = $this->get_current_semester();
		
		if($open_semester){
			unset($get);
			$get['fields'] = array(
					'id',
					'year_from',
					'year_to'
				);
			$get['where']['id'] = $open_semester->academic_year_id;
			$get['single'] = true;
			return $this->ci->m->get_record('academic_years',$get);
		}else{
			return false;
		}
	}
	/*END OF FUNCTION GET CURRENT SEMESTER"|GRADING|SY*/


	/* GET STUDENT GRADES FOR THIS SUBJECT */
	public function get_student_grades_for_subject(){

		
		unset($get);
		$get['fields'] = array(
			'studentsubjects.*',
			'enrollments.studid',
			'enrollments.name as fullname',
			'users.name as instructor'
			);
		$get['where']['studentsubjects.subject_id']	= $this->id;
		$get['where']['studentsubjects.is_deleted'] = 0;
		$get['where']['studentsubjects.is_drop'] = 0;
		$get['where']['enrollments.sy_from'] = $this->current_sy->year_from;
		$get['where']['enrollments.sy_to'] = $this->current_sy->year_to;
		$get['where']['enrollments.semester_id'] = $this->current_semester->semester_id;
		$get['join'][] = array(
				'table' => 'enrollments',
				'on' => 'enrollments.id = studentsubjects.enrollment_id',
				'type' => 'left'
			);
		$get['join'][] = array(
				'table' => 'subjects',
				'on' => 'subjects.id = studentsubjects.subject_id',
				'type' => 'left'
			);
		$get['join'][] = array(
				'table' => 'users',
				'on' => 'users.id = subjects.teacher_user_id',
				'type' => 'left'
			);

		$this->ci->m->set_table('studentsubjects');
		return $this->ci->m->get_record('studentsubjects', $get);
	}

	/**
	 * Get Subject Grades per Student by Subject ID
	 * @return object
	 */
	public function get_grades()
	{
		unset($get); // Get All enrollment match this the subject id
		$get['fields'] = array(
			'studentsubjects.id',
			'studentsubjects.subject_id',
			'studentsubjects.remarks',
			'enrollments.id as enrollment_id',
			'enrollments.studid',
			'enrollments.name as fullname',
			'years.year',
			'courses.course'
			);
		$get['where']['studentsubjects.subject_id']	= $this->id;
		$get['where']['studentsubjects.is_deleted'] = 0;
		$get['where']['studentsubjects.is_drop'] = 0;
		$get['where']['enrollments.sy_from'] = $this->current_sy->year_from;
		$get['where']['enrollments.sy_to'] = $this->current_sy->year_to;
		$get['where']['enrollments.semester_id'] = $this->current_semester->semester_id;
		$get['join'][] = array(
				'table' => 'enrollments',
				'on' => 'enrollments.id = studentsubjects.enrollment_id',
				'type' => 'left'
			);
		$get['join'][] = array(
				'table' => 'subjects',
				'on' => 'subjects.id = studentsubjects.subject_id',
				'type' => 'left'
			);
		$get['join'][] = array(
				'table' => 'years',
				'on' => 'years.id = enrollments.year_id',
				'type' => 'left'
			);
		$get['join'][] = array(
				'table' => 'courses',
				'on' => 'courses.id = enrollments.course_id',
				'type' => 'left'
			);
		if($rs = $this->ci->m->get_record('studentsubjects', $get)){

			$gps = $this->get_gp();
			$result = false;

			foreach ($rs as $k => $v) {

				// Get Grades for each enrollment
				$grade = false;
				if($gps){ foreach ($gps as $g => $p) {
						$get = array(
								'where' => array(
									'enrollment_id'=> $v->enrollment_id,
									'ss_id'=> $v->id,
									'gp_id'=> $p->id,
									'is_deleted'=> 0,
								),
								'fields' => "id,converted",
								'single' => true,
							);
						$grd = $this->ci->m->get_record('grades_file', $get);
						$grade[$p->id] = $grd ? $grd->converted : "";
					}
				}

				$result[$k]['profile'] = $v;
				$result[$k]['grade'] = $grade;
			}
			
			return $result;
		}
		
		return false;
	}

	/**  HELPER FUNCTION  **/

		/**
		 * Get Grading Periods
		 */
		private function get_gp($value='')
		{
			return $this->ci->m->get_record('grading_periods', array('where'=>array('is_deleted'=>0),'order'=>'orders','fields'=>'id, grading_period'));
		}
	/**  END HELPER FUNCTION  **/

}

?>