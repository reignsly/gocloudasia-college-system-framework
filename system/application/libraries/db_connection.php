<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

///////////////////////////////////
// SYLVER : DATABASE CONNECTION // 
/////////////////////////////////
/**
 * Saves the Database Connection in the config/himitsu.gocloud
 * @By SYLVER | 8/11/2014
 */
class Db_connection
{
	private $ci;

	public $host = "localhost";
	public $user = "root";
	public $password = "password";
	public $database = "database";

	private $key = "";
	private $dbconfigfile = "himitsu.gocloud";
	public $itspath = "";

	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->helper('file');
		$this->ci->load->library('encrypt');

		$this->_load_data();
	}

	public function _load_data()
	{
		$this->key = $this->ci->encrypt->hash(md5(sha1($this->ci->config->item('encryption_key'))));
		$this->itspath = APPPATH.'config/'.$this->dbconfigfile;

		if(file_exists($this->itspath)){

			/* Read the file */
			/* FORMAT
				0 - Database
				1 - User
				2 - Password
				3 - Host
			 */
			$string = trim(read_file($this->itspath));
			$arr_str = explode("\n", $string);

			if(count($arr_str) === 4){

				$this->database = isset($arr_str[0]) ? $this->ci->encrypt->decode(trim($arr_str[0]), $this->key) : $this->database;
				$this->user = isset($arr_str[1]) ? $this->ci->encrypt->decode(trim($arr_str[1]), $this->key) : $this->user;
				$this->password = isset($arr_str[2]) ? $this->ci->encrypt->decode(trim($arr_str[2]), $this->key) : $this->password;
				$this->host = isset($arr_str[3]) ? $this->ci->encrypt->decode(trim($arr_str[3]), $this->key) : $this->host;
			}
		}
	}


	public function update($download = false)
	{
		if($this->ci->input->post()){

			$string_arr = array();
			$string_arr[] = $db = $this->ci->encrypt->encode($this->ci->input->post('database'), $this->key);
			$string_arr[] = $user = $this->ci->encrypt->encode($this->ci->input->post('user'), $this->key);
			$string_arr[] = $pass = $this->ci->encrypt->encode($this->ci->input->post('password'), $this->key);
			$string_arr[] = $host = $this->ci->encrypt->encode($this->ci->input->post('host'), $this->key);

			$connection_string = implode("\n", $string_arr);

			/* TRY TO CONNECT TO THE DATABASE AND CHECK IF POSSIBLE */

			$db = false;
			$db['hostname'] = $this->ci->input->post('host');
			$db['username'] = $this->ci->input->post('user');
			$db['password'] = $this->ci->input->post('password');
			$db['database'] = $this->ci->input->post('database');
			$db['dbdriver'] = 'mysql';
			$db['dbprefix'] = '';
			$db['pconnect'] = TRUE;
			$db['db_debug'] = FALSE;
			$db['cache_on'] = FALSE;
			$db['cachedir'] = '';
			$db['char_set'] = 'utf8';
			$db['dbcollat'] = 'utf8_general_ci';
			$db['swap_pre'] = '';
			$db['autoinit'] = FALSE; // Disable Auto Initialize
			$db['stricton'] = FALSE;

			if(!$download){

				$db_obj = $this->ci->load->database($db, true);
	  		$connected = $db_obj->initialize();
	  		// vd($connected);
	  			
	  		// if($connected){
					
					if ( write_file($this->itspath, trim($connection_string))){
					    
					    return true;
					}

				// }
			}
			else
			{
				/* DOWNLOAD THE FILE */
				$this->ci->load->helper('download');
				force_download($this->dbconfigfile, trim($connection_string));
			}
		}

		return false;
	}

	public function check($return = false, $check_if_login = false)
	{
		if(isset($this->ci->session->userdata['logged_in']) && $this->ci->session->userdata['logged_in']){
			return true;
		}

		$db = false;
		$db['hostname'] = $this->host;
		$db['username'] = $this->user;
		$db['password'] = $this->password;
		$db['database'] = $this->database;
		// $db['hostname'] = 'localhost';
		// $db['username'] = 'root';
		// $db['password'] = '1234asdf';
		// $db['database'] = 'ub_online';
		$db['dbdriver'] = 'mysql';
		$db['dbprefix'] = '';
		$db['pconnect'] = TRUE;
		$db['db_debug'] = FALSE;
		$db['cache_on'] = FALSE;
		$db['cachedir'] = '';
		$db['char_set'] = 'utf8';
		$db['dbcollat'] = 'utf8_general_ci';
		$db['swap_pre'] = '';
		$db['autoinit'] = FALSE; // Disable Auto Initialize
		$db['stricton'] = FALSE;
		
		$db_obj = $this->ci->load->database($db, true);
		$connected = $db_obj->initialize();
		
		if($return){
			return $connected;
		}else{
			if(!$connected){
				//show_error('Database connection error, please consult your provider.');
			}
		}
	}
}