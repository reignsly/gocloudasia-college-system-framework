<?php
class Setup_Controller Extends CI_Controller
{
	protected $layout_view = 'application';
	protected $welcome_view = 'welcome';
	protected $content_view ='';
	protected $view_data = array();
	protected $user;
	protected $userlogin;
	protected $departments;
	protected $user_menus = array();
	protected $disable_layout = FALSE;
	protected $disable_menus = FALSE;
	protected $disable_views = FALSE; //DISABLE THE LOADING OF VIEW - FOR AJAX PURPOSE DEC 11 2013
	
	public function __construct()
	{
		parent::__construct();
		
		$this->output->enable_profiler(ENVIRONMENT == 'development' ? true : false);

		$this->session_checker->check_if_alive();

		$this->load->model(array('M_settings','M_menus','M_grading_periods','M_departments','M_sys_par','M_open_semesters'));
		$this->user = $this->session->userdata('userType');
		$this->userlogin = $this->session->userdata('userlogin');
		$this->userid = $this->session->userdata('userid');

		$this->check_system_settings();

		$this->setting 	= $this->M_settings->get_settings();
		vd($this->setting);
		$this->syspar 	= $this->M_sys_par->get_sys_par();
		$this->layout_view =  $this->syspar->application_layout ? $this->syspar->application_layout : 'application';
		$this->welcome_view = $this->syspar->welcome_layout ? $this->syspar->welcome_layout : 'welcome';
		$this->current_open_semester = $this->cos = $this->M_open_semesters->get_current_semesters();
		$this->departments 	= $this->M_departments->get_departments($this->user);	
		$this->user_menus = $this->M_menus->get_all_menus($this->user);
		$this->current_grading_period = $this->M_grading_periods->get_current_grading_period();	
		$this->view_data['system_message'] = $this->_msg = $this->session->flashdata('system_message');
		$this->view_data['user_menus'] = $this->user_menus;
		$this->view_data['mydepartment'] = $this->departments;
	}

	// CHECK SYSTEM SETTINGS AND SYSPAR
	private function check_system_settings(){

		if($this->db->table_exists('settings') == false){
			show_error('System settings is missing, please contact your provider');
		}

		if($this->db->table_exists('sys_par') == false){
			show_error('System Parameter is missing, please contact your provider');	
		}

		#load models
		$this->load->model(array('M_settings','M_menus','M_grading_periods','M_departments','M_sys_par','M_open_semesters'));

		if($this->M_sys_par->get_sys_par() == false){
			show_error('System Parameter is not properly configured, please contact your provider');	
		}
	}
	
	public function _output($output)
    {
		$this->view_data['user_menus'] = $this->user_menus;
		
         if($this->content_view !== FALSE && empty($this->content_view)){
			$this->content_view = $this->router->class . '/' . $this->router->method;
        }
		
        $yield = file_exists(APPPATH . 'views/' . $this->content_view . EXT) ? $this->load->view($this->content_view, $this->view_data, TRUE) : FALSE ;
       
		if($this->disable_layout == FALSE)
		{
			if($this->layout_view)
			{
				$this->view_data['yield'] = $yield;
				$this->view_data['disable_menus'] = $this->disable_menus;
				if($this->disable_views == false){
					echo $this->load->view('layouts/' . $this->layout_view, $this->view_data, TRUE);
				}
			}else{
				if($this->disable_views == false){
					
					echo $yield;
				}
			}
			
		}else{
			
			if($this->disable_views == false){
				echo $this->load->view($this->content_view, $this->view_data, TRUE);
			}
		}
		
		if($this->disable_views == false){
			//output profiler
			echo $output;
		}
    }

	function pagination_style()
	{
	    // Style
	  $config['full_tag_open'] = '<ul class="pagination">';
      $config['full_tag_close'] = '</ul>';
      $config['prev_link'] = '&lt; Prev';
      $config['prev_tag_open'] = '<li>';
      $config['prev_tag_close'] = '</li>';
      $config['next_link'] = 'Next &gt;';
      $config['next_tag_open'] = '<li>';
      $config['next_tag_close'] = '</li>';
      $config['cur_tag_open'] = '<li class="active"><a href="#">';
      $config['cur_tag_close'] = '</a></li>';
      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';
      $config['first_link'] = FALSE;
      $config['last_link'] = FALSE;
      
      return $config;
      
	}
}
?>
