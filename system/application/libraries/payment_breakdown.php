<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class M_payment_breakdown Extends MY_Model{
	
	protected $_table = 'payments_breakdown';
	protected $_uid = 'id';
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_id_of_fees($eid, $spr_id){
		$sql = "
			SELECT 
				table_id,
				fee_type
			FROM $this->_table
			WHERE enrollment_id = ?
			AND spr_id = ?
			GROUP BY table_id
			ORDER BY fee_type
		";
		$q = $this->db->query($sql, array($eid, $spr_id));
		$r = $q->num_rows() > 0 ? $q->result() : false;

		$ret['TUITION'] = array();
		$ret['OTHER'] = array();
		$ret['PREVIOUS'] = array();

		if($r){
			foreach ($r as $key => $value) {
				$ret[$value->fee_type][$key] = $value->table_id;
			}
		}

		return $ret;
	}

	public function get_amount($spr_id, $table_id){
		
		$ret = 0;

		$qry = $this->db->select('amount')
		->where('spr_id',$spr_id)
		->where('table_id',$table_id)
		->get($this->_table);

		$rs = $qry->num_rows() > 0  ? $ret = $qry->row()->amount : $ret = 0;

		return $ret;
	}

	public function insert_payment_breakdown($id,$values, $fee_type, $payment_type){

		unset($data);
		$data['fee_type'] = $fee_type;
		$data['amount'] = $values; //AMOUNT OF THE PAYMENT
		$data['payment_type'] = $payment_type; //CHECK or CASH etc

		switch ($fee_type) {
			case 'TUITION':
				$this->load->model('M_student_plan_modes','m_spm');
				$spm = $this->m_spm->get($id);
				if($spm){
					
					$data['enrollment_id'] = $spm->enrollment_eid;
					$data['spr_id'] = $spm->spr_id;
					$data['table_id'] = $spm->id;
					$this->insert($data);
				}

				break;
			case 'OTHER':
				$this->load->model('M_student_enrollment_fees','m_sef');
				$sef = $this->m_sef->get($id);
				if($sef){
					
					$data['enrollment_id'] = $sef->sef_enrollment_id;
					$data['spr_id'] = $sef->spr_id;
					$data['table_id'] = $sef->sef_id;
					$this->insert($data);
				}
				break;
			case 'PREVIOUS':
				$this->load->model('M_student_previous_accounts','m_spa');
				$spa = $this->m_spa->get($id);
				if($spa){
					$data['enrollment_id'] = $spa->enrollment_eid;
					$data['spr_id'] = $spa->spr_id;
					$data['table_id'] = $spa->id;
					$this->insert($data);
				}
				break;
		}

	}
}

?>