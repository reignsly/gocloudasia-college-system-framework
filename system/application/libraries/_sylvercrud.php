<?php
/*
	SLY
	THIS IS LIBRARY FOR create read update & delete function
*/
class _Sylvercrud
{
	private $ci;
	private $view_data;
	private $title;
	private $_sql;
	private $_crud;
	private $_crud_extension;

	public $_table;
	public $_config;
	public $_html;
	public $_uniq_id = "id";

	//PAGER
	private $_per_page;
	private $_num_links;
	private $_uri_segment;
	private $_page;
	private $_order;
	private $_group;
	private $_total_rows;
	private $_search_columns; //Columns that will be display in the search filter
	private $_show_columns; //Columns that will be display in the table , if null/false display all from the query
	private $_hide_id;

	private $_system_message;
	private $_disable_system_message;
	private $_selected_id;
	private $_controller_method;

	/**
	 * Override Buttons must be array
	 * array('controller'=>'', 'method'=>'', 'field'=>'field','caption' => '','icon_class' => '')
	 * false to disable
	 */
	private $_delete_button; // OVERRIDE DELETE BUTTON
	private $_view_button; // OVERRIDE VIEW BUTTON
	private $_edit_button; // OVERRIDE EDIT BUTTON

	private $_add_button; // true or false - enable/disable add button in search area
	private $_action_button; // true or false - enable/disable action button
	private $_additional_button; //Addition button in search area
	private $_add_action_button; // array('0'=>array('controller'=>'', 'method'=>'', 'field'=>'field','caption' => '','icon_class' => '')) - addition button in action button

	private $_enable_activity_log;
	private $_activity_log_table;
	
	public function __construct($config=false)
	{
		$this->ci =& get_instance();

		/*CONFIG PARAMETER MUST*/

		/*
		TYPE
			query = CRUD will depend on the given query
			custom = CRUD will depend on the custom config
		*/	


		if($config && is_array($config))
		{
			$this->load_config($config);


			if(isset($config['type']) && trim($config['type']) == "query" )
			{
				if(isset($_POST['save']) && $_POST['save'] == "Save" )
				{
					//SAVE THE FORM
					$this->save_create();
				}
				elseif(isset($_POST['update_save']) && $_POST['update_save'] == "Save" )
				{
					$this->save_edit();
				}
				else
				{
					if($this->ci->uri->segment($this->_uri_segment) == "create")
					{
						$this->create();
					}
					elseif($this->ci->uri->segment($this->_uri_segment) == "view")
					{
						$this->view();	
					}
					elseif($this->ci->uri->segment($this->_uri_segment) == "edit")
					{
						$this->edit();
					}
					elseif($this->ci->uri->segment($this->_uri_segment) == "delete")
					{
						$this->delete();
					}
					else
					{
						$this->load_query();
					}
				}
			}
			elseif (isset($config['type']) && trim($config['type']) == "custom" )
			{
				$this->load_custom();	
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	private function load_config($config)
	{

		$this->_config = $config = (object)$config;
		
		$this->_table = $this->view_data['table'] = isset($this->_config->table) ? $this->_config->table : '';
		$this->_crud = $this->view_data['crud'] = isset($this->_config->crud) ? $this->_config->crud : false;
		$this->_crud_extension = $this->view_data['crud_extension'] = isset($this->_config->crud_extension) ? $this->_config->crud_extension : false;
		$this->title = $this->view_data['title'] = isset($this->_config->title) ? $this->_config->title : '';
		
		$this->_sql = isset($this->_config->sql) ? $this->_config->sql : '';
		$this->_per_page = isset($this->_config->pager['per_page']) ? $this->_config->pager['per_page'] : 25;
		$this->_num_links = isset($this->_config->pager['num_links']) ? $this->_config->pager['num_links'] : 10;
		$this->_uri_segment = isset($this->_config->pager['uri_segment']) ? $this->_config->pager['uri_segment'] : 3;
		$this->_order = isset($this->_config->order) ? $this->_config->order : '';
		$this->_group = isset($this->_config->group) ? $this->_config->group : '';
		$this->_hide_id = $this->view_data['hide_id'] = isset($this->_config->hide_id) ? $this->_config->hide_id : true;
		$this->_uniq_id = $this->view_data['uniq_id'] = isset($this->_config->uniq_id) ? $this->_config->uniq_id : $this->_uniq_id;

		$this->_search_columns = $this->view_data['search_columns'] = isset($this->_config->search_columns) ? $this->_config->search_columns : false;
		$this->_show_columns = $this->view_data['show_columns'] = isset($this->_config->show_columns) ? $this->_config->show_columns : false;

		// BUTTON FUNCTION
		$this->_delete_button = $this->view_data['delete_button'] = isset($this->_config->delete_button) ? $this->_config->delete_button : 'default';
		$this->_view_button = $this->view_data['view_button'] = isset($this->_config->view_button) ? $this->_config->view_button : 'default';
		$this->_edit_button = $this->view_data['edit_button'] = isset($this->_config->edit_button) ? $this->_config->edit_button : 'default';

		$this->_add_button = $this->view_data['add_button'] = isset($this->_config->add_button) ? $this->_config->add_button : true;
		$this->_action_button = $this->view_data['action_button'] = isset($this->_config->action_button) ? $this->_config->action_button : true;
		$this->_add_action_button = $this->view_data['add_action_button'] = isset($this->_config->add_action_button) ? $this->_config->add_action_button : false;
		$this->_additional_button = $this->view_data['additional_button'] = isset($this->_config->additional_button) ? $this->_config->additional_button : false;

		$this->_page = $this->ci->uri->segment($this->_uri_segment) ? (intval($this->ci->uri->segment($this->_uri_segment)) ? $this->ci->uri->segment($this->_uri_segment) : 0) : 0 ;

		$this->_controller_method = $this->view_data['controller_method'] = $this->ci->router->class.'/'.$this->ci->router->method;

		$this->_disable_system_message = $this->view_data['disable_system_message'] = isset($this->_config->disable_system_message)?$this->_config->disable_system_message:false;
		$this->view_data['disable_title'] = isset($this->_config->disable_title)?$this->_config->disable_title:false;
		$this->_system_message = $this->view_data['system_message'] = $this->ci->session->flashdata('system_message');

		$this->_enable_activity_log = isset($this->_config->enable_activity_log)?$this->_config->enable_activity_log:TRUE;
		$this->_activity_log_table = isset($this->_config->activity_log_table)?$this->_config->activity_log_table:"activity_log";

		$this->ci->load->model('M_core_model','m');
		$this->ci->m->set_table($this->_table);

		//SET SELECTED ID
		if($this->ci->uri->segment($this->_uri_segment) 
			&& $this->ci->uri->segment($this->_uri_segment+1)
			&& intval($this->ci->uri->segment($this->_uri_segment+1)) )
		{
			$this->_selected_id = $this->ci->uri->segment($this->_uri_segment+1);
		}else
		{
			$this->_selected_id = false;
		}

		//additional 
		$this->view_data['form_url'] = $fu = $this->ci->router->class.'/'.$this->ci->router->method.'/0';
	}

	private function load_query()
	{
		$query = $this->ci->db->query($this->get_sql_query());
		$query_fields = $query->list_fields();

		//SEARCH TRUE/FALSE
		$this->view_data['search'] = $this->_config->search;

		unset($config);
		$this->ci->load->library("pagination");
		//$config = $this->pagination_style();
		$config["base_url"] = site_url() .$this->ci->router->class."/".$this->ci->router->method;
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 

		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['prev_link'] = '&lt; Prev';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'Next &gt;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;

		$this->view_data['total_rows'] = $config["total_rows"] = $this->get_total_rows();
		
		$config["per_page"] = $this->_per_page;
		$config['num_links'] = $this->_num_links;
		$config["uri_segment"] = $this->_uri_segment;

		// $config['use_page_numbers'] = TRUE;
		$this->ci->pagination->initialize($config);
		
		$this->view_data['result'] = $result = $query->num_rows() > 0 ? $query->result() : false;
		$this->view_data['links'] = $this->ci->pagination->create_links();

		//CHECK IF THERE IS TABLE COLUMN ALIAS
		if(isset($this->_show_columns) && is_array($this->_show_columns) && $this->_show_columns ) 
		{
			$this->view_data['column_name'] = $this->_show_columns;
		}
		else
		{
			$first_row = $result ? current($result) : array();
			$this->view_data['column_name'] = $this->_show_columns = $this->get_column_name($query_fields, $first_row);
		}
		// vd($this->_show_columns);
		// $this->view_data['filter_list'] = $this->get_filter_array_for_dd($result);
		$this->view_data['filter_list'] = $this->get_search_column_for_dd($query_fields);

		$this->_html = $this->ci->load->view('_crud/index',$this->view_data, true); //RETURN HTML CODES
	}

	private function create()
	{
		$this->_html = $this->ci->load->view('_crud/create',$this->view_data, true); //RETURN HTML CODES
	}

	private function view()
	{
		$this->view_data['edit_record'] = $er = $this->ci->m->get($this->_selected_id);
		$this->view_data['view_only'] = true;
		
		if($er){
			$this->_html = $this->ci->load->view('_crud/view',$this->view_data, true); //RETURN HTML CODES
		}else{
			show_404();
		}
	}

	private function edit()
	{
		$this->view_data['edit_record'] = $er = $this->ci->m->get($this->_selected_id);
		
		if($er){
			$this->_html = $this->ci->load->view('_crud/edit',$this->view_data, true); //RETURN HTML CODES
		}else{
			show_404();
		}
	}

	private function delete()
	{
		$er = $this->ci->m->get($this->_selected_id);

		if($er)
		{
			$result = $this->ci->m->delete($this->_selected_id);
			
			if($result){
				//Activity log
				if($this->_enable_activity_log){
					$this->ci->m->set_table($this->_activity_log_table);
					unset($log);
					$log['action'] = "Delete : ".$this->_table;
					$log['user'] = isset($this->ci->session->userdata['userlogin']) ? $this->ci->session->userdata['userlogin'] : '';
					$log['url'] = current_url();
					$log['remarks'] = "Data : ".arr_str($er); //from activty log helper
					$rs_log = $this->ci->m->insert($log);
				}

				$this->_msg('s','Record was successfully removed.',$this->_controller_method);
			}else
			{
				$this->_msg('e','Delete failed. Please try again',$this->_controller_method);
			}
		}
		else
		{
			show_404();
		}
	}

	private function get_sql_query($remove_limit = false)
	{
		$sql = $this->_config->sql;

		//GET VARIABLES FOR FILTER SEARCH
		if($this->_config->search){
			//WHERE QUERY IF THERE IS A WHERE STATEMENT
			$has_where = strpos(strtolower($sql), 'where') ? true : false;

			if(
				isset($_GET['keyword']) 
				&& trim($_GET['keyword']) != ""
				&& isset($_GET['filter_by']) 
				&& trim($_GET['filter_by']) != ""
			){
				$fb = $_GET['filter_by'];
				$kw = $this->ci->db->escape('%'.trim($_GET['keyword'].'%'));
				if($has_where){
					$sql .= " AND $fb LIKE $kw";
				}else{
					$sql .= " WHERE $fb LIKE $kw";
				}

				$this->view_data['filter_by'] = $_GET['filter_by'];
				$this->view_data['keyword'] = $_GET['keyword'];
			}
		}

		if($this->_group != "")
		{
			$sql .= " GROUP BY " . $this->_group;
		}

		if($this->_order != "")
		{
			$sql .= " ORDER BY " . $this->_order;
		}

		if($remove_limit == false)
		{
			$sql .= " LIMIT  $this->_page , $this->_per_page";
			// $sql .= " LIMIT  $this->_per_page , $this->_page";
		}
		// vd($sql);
		return $sql;
	}

	private function get_total_rows()
	{
		$query = $this->ci->db->query($this->get_sql_query(true));
		$ret = $query->num_rows();
		$this->_total_rows = $ret;
		return $ret;
	}

	/**
	 * Get Column Names from the query
	 * Ex select fname as first_name, mname as middle_name ....
	 * @return array ex array('fname'=>'first name','dname'=>'middle name')
	 */
	private function get_column_name($qryflds, $row)
	{
		$r = array();

		if($qryflds){
			foreach ($qryflds as $k => $v) {

				//change underscore to space
				$t = str_replace("_", " ", $v);
				$r[$v] = ucfirst($t);
			}
		}

		return $r;

		//experimental
		if($row){	
			$row_array = (array)$row;
			$row_array = array_keys($row_array);
			
			foreach ($row_array as $k => $v) {
				$t = isset($qryflds[$k]) ? $qryflds[$k] : $v;
				$t = str_replace("_", " ", $v);
				$r[$v] = ucfirst($t);
			}
		}else{

			if($qryflds){
				foreach ($qryflds as $k => $v) {

					//change underscore to space
					$t = str_replace("_", " ", $v);
					$r[$v] = ucfirst($t);
				}
			}
		}
		
		return $r;
	}

	private function get_filter_array_for_dd($result)
	{
		// vd($result);
		$ret = array();

		if(isset($this->_config->search_columns) ) 
		{
			if($this->_config->search)
			{
				$c = $this->_config->search_columns;

				if(is_array($c)){
					foreach ($c as $key => $name) {
						if($key==$this->_uniq_id && $this->_hide_id == true)
						{
							continue;
						}
						$ret[$key] = $name;
					}
				}else{
					$c = explode(",", $c);
					foreach ($c as $value) {
						if($value==$this->_uniq_id && $this->_hide_id == true)
						{
							continue;
						}
						$ret[$value] = $value;
					}
				}
			}
		}
		else
		{
			if($this->_config->search)
			{
				$c = current($result);
				foreach ($c as $key => $name) 
				{
					if($key==$this->_uniq_id && $this->_hide_id == true)
					{
						continue;
					}
					$ret[$key] = ucwords($key);
				}
			}
		}
		// vd($ret);
		return $ret;
	}

	private function get_search_column_for_dd($qryflds)
	{
		// vd($result);
		$ret = array();

		if(isset($this->_config->search_columns) ) 
		{
			if($this->_config->search)
			{
				$c = $this->_config->search_columns;

				if(is_array($c)){
					foreach ($c as $key => $name) {
						if($key==$this->_uniq_id && $this->_hide_id == true)
						{
							continue;
						}
						$ret[$key] = $name;
					}
				}else{
					$c = explode(",", $c);
					foreach ($c as $value) {
						if($value==$this->_uniq_id && $this->_hide_id == true)
						{
							continue;
						}
						$ret[$value] = $value;
					}
				}
			}
		}
		else
		{
			if($this->_config->search)
			{

				//Experimental - error if the query has aliases
				if($qryflds){
					foreach ($qryflds as $k => $v) {
						if($k==$this->_uniq_id && $this->_hide_id == true)
						{
							continue;
						}else{
							//change underscore to space
							$t = str_replace("_", " ", $v);
							$ret[$v] = ucfirst($t);
						}
					}
				}
			}
		}
		// vd($ret);
		return $ret;
	}

	/*SAVING OF DATA*/
	private function save_create()
	{
		$post = $this->ci->input->post();
		if($post)
		{
			//LOAD LIBRARY
			$this->ci->load->library('form_validation');

			unset($input);

			//SET RULES
			$has_rules = 0;
			$this->ci->form_validation->set_error_delimiters('<div class="alert alert-xs alert-danger">', '</div>'); 
			foreach ($this->_crud as $key => $value) {
				$value = (object)$value;
				if(isset($value->rules)){
					$this->ci->form_validation->set_rules($value->field, $value->label,$value->rules);		
					$has_rules++;
				}

				$attr = (object)$value->attr;				
				if( isset($_POST[$attr->name]) && trim($this->ci->input->post($attr->name)) )
				{
					switch (strtolower($value->type)) {
						case 'text':
						case 'select':
							$input[$value->field] = trim($this->ci->input->post($attr->name));
							break;
						
						default:
							# code...
							break;
					}
				}
			}

			//PUSH CRUD EXTENSION BEFORE ADDING
			//Additional data example date, user etc
			if($this->_crud_extension){
				foreach ($this->_crud_extension as $key => $value) {
					$input[$key] = $value;
				}
			}
			
			if($this->ci->form_validation->run())
			{
				$this->ci->m->set_table($this->_table);
				$result = (object)$this->ci->m->insert($input);
				if($result->status)
				{
					//SPECIAL RUN SAVING CREATE FINISH
					if(isset($this->_config->after_create)){
						// vd('x');
					}

					//Activity log
					if($this->_enable_activity_log){
						$this->ci->m->set_table($this->_activity_log_table);
						unset($log);
						$log['action'] = "Add : ".$this->title;
						$log['user'] = isset($this->ci->session->userdata['userlogin']) ? $this->ci->session->userdata['userlogin'] : '';
						$log['url'] = current_url();
						$log['remarks'] = "Insert ".$this->_table." ID : ".$result->id;
						$rs_log = $this->ci->m->insert($log);
					}

					$this->_msg('s','Data was successfully saved.',current_url());
				}
				else
				{
					$this->_msg('e','Saving Failed. Please try again.',current_url());
				}
			}
			else
			{
				$this->create();
			}
		}
		else
		{
			show_404();
		}
	}
	private function save_edit()
	{
		if($this->_selected_id)
		{
			$data_record = $this->ci->m->get($this->_selected_id);

			if($data_record)
			{
				$post = $this->ci->input->post();
				if($post)
				{
					//LOAD LIBRARY
					$this->ci->load->library('form_validation');

					unset($input);

					//SET RULES
					$has_rules = 0;
					$this->ci->form_validation->set_error_delimiters('<div class="alert alert-xs alert-danger">', '</div>'); 
					foreach ($this->_crud as $key => $value) {
						$value = (object)$value;
						if(isset($value->rules)){
							
							if(strpos($value->rules, "is_unique")){
								$xname = $value->attr['name'];
								$xcheck = $data_record->$xname;
								$value->rules = str_replace("]", ".$xcheck]", $value->rules);
							}
							
							$this->ci->form_validation->set_rules($value->field, $value->label,$value->rules);		
							$has_rules++;
						}

						$attr = (object)$value->attr;				
						if( isset($_POST[$attr->name]) && trim($this->ci->input->post($attr->name)) )
						{
							switch (strtolower($value->type)) {
								case 'text':
								case 'select':
								case 'date':
									$input[$value->field] = trim($this->ci->input->post($attr->name));
									break;
								
								default:
									# code...
									break;
							}
						}
					}
					
					if($this->ci->form_validation->run())
					{
						$this->ci->m->set_table($this->_table);
						$result = $this->ci->m->update($this->_selected_id,$input);
						if($result)
						{
							//Activity log
							if($this->_enable_activity_log){
								$this->ci->m->set_table($this->_activity_log_table);
								unset($log);
								$log['action'] = "Edit : ".$this->title;
								$log['user'] = isset($this->ci->session->userdata['userlogin']) ? $this->ci->session->userdata['userlogin'] : '';
								$log['url'] = current_url();
								$log['remarks'] = "Data ".arr_str($input); //from activty log helper
								$rs_log = $this->ci->m->insert($log);
							}

							$this->_msg('s','Data was successfully updated.',current_url());	
						}
						else
						{
							$this->_msg('e','Saving Failed. Please try again.',current_url());	
						}
					}
					else
					{
						$this->edit();
					}
				}
				else
				{
					$this->_msg('e','Saving Failed. Please try again.',current_url());	
				}
			}
			else
			{
				$this->_msg('e','Saving Failed. Please try again.',current_url());	
			}
		}
		else
		{
			show_404();
		}
	}
	/*End of saving data*/

	/*Form Validation of Unique while edit*/
	public function username_check($str)
	{
		$this->form_validation->set_message('username_check', 'The %s field can not be the word "test"');
		if ($str == 'test')
		{
			
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function _msg($type = FALSE,$message = FALSE,$redirect = FALSE,$var_name = 'system_message')
	{
		$template = "";
		$type = strtolower(trim($type));
		switch($type)
		{
		 	case $type == 'e':
				$template = "<div class='alert alert-danger'><i class='fa fa-exclamation'></i>&nbsp; ".$message."</div>";
				
			break;
			case $type == 's':
				$template = "<div class='alert alert-success'><i class='fa fa-check'></i>&nbsp; ".$message."</div>";
			break;
			case $type == 'n':
				$template = "<div class='alert alert-info'><i class='fa fa-check'></i>&nbsp; ".$message."</div>";
			break;
			case $type == 'p':
				$template = $message;
			break;
			case $type === FALSE;
				$template = $message;
			break;
		}

		if($type AND $message AND $redirect)
		{
			$this->ci->session->set_flashdata($var_name,$template);	
			redirect($redirect);
		}elseif($type AND $message AND $redirect == FALSE){
			return $template;
		}
		
		if($redirect == FALSE AND $message == FALSE AND $redirect == FALSE)
		{
			return $this->ci->session->flashdata($var_name);
		}
	}
}

/* HELPER FUNCTION STARTS HERE */
	
	// create segments like 1/2/3/4 according to the given param
	function get_segments($data, $field){
    // vd($data);
		$ret = "";

		if(is_string($field)){

			$ret = isset($data->$field) ? $data->$field : 'Missing Field';
		}else{
			foreach ($field as $key => $value) {
				if(isset($data->$value)){
					if($ret == ""){
						$ret = isset($data->$value) ? $data->$value : 'Missing Field';
					}else{
						$ret .= "/".isset($data->$value) ? $data->$value : 'Missing Field';
					}
				}
			}
		}

		return $ret;
	}
/* END OF HELPER FUNCTIONS */

/** Update Logs */

 ## Date Jan 1 2015 3:39 PM
 ## Fix Show Column - for table header name custom
 ## Change Column Names to Search Column
 ## Add Uniq_id - to specify the table unique id