<?php
class Dev_Controller Extends CI_Controller
{
	protected $layout_view = 'control_panel';
	protected $content_view ='';
	public $loggin = false;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','my_dropdown'));
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->output->enable_profiler(true);
		$this->view_data['system_message'] = $this->_msg = $this->session->flashdata('system_message');

		$this->loggin = $this->view_data['login'] = $this->check_if_logged_in(true) ? true : false;
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		/** If Database Connection is invalid redirect to edit connection ***/
		if($this->loggin && !$this->db_connection->check(true)){
			if($this->router->class == "gopanel" && $this->router->method != "kabit"){
				redirect('gopanel/kabit');
			}
		}
	}

	public function _output($output)
    {	
        if($this->content_view !== FALSE && empty($this->content_view)){
			$this->content_view = $this->router->class . '/' . $this->router->method;
        }
		
        $yield = file_exists(APPPATH . 'views/' . $this->content_view . EXT) ? $this->load->view($this->content_view, $this->view_data, TRUE) : FALSE ;
       
		if($this->layout_view)
		{
			$this->view_data['yield'] = $yield;
			echo $this->load->view('layouts/' . $this->layout_view, $this->view_data, TRUE);
		}else{
			echo $yield;
		}

		echo $output;
    }

  public function check_if_logged_in($check = false){
    	
		if($check == false)
		{
			if(!isset($this->session->userdata['username']) || empty($this->session->userdata['username']))
			{
				redirect('gopanel/login');
			}
			else
			{
				// check if userType cpanel
				if(isset($this->session->userdata['userType']) && $this->session->userdata['userType'] === 'cpanel'){}
				else {
					$this->session->sess_destroy();
					redirect('gopanel/login');
				}
			}
		}else{
			if(!isset($this->session->userdata['username']) || empty($this->session->userdata['username']))
			{
				return FALSE;
			}else{
				// check if userType cpanel
				if(isset($this->session->userdata['userType']) && $this->session->userdata['userType'] === 'cpanel'){
					return TRUE;
				}
				return FALSE;
			}
		}
	}

	public function _msg($type = FALSE,$message = FALSE,$redirect = FALSE,$var_name = 'system_message')
	{
		$template = "";
		$type = strtolower(trim($type));
		switch($type)
		{
		 	case $type == 'e':
				$template = "<div id='focusme' class='alert alert-danger'><i class='fa fa-exclamation'></i>&nbsp; ".$message."<span style='float:right;' class='glyphicon glyphicon-remove sys_m_close'></span></div>";
				
			break;
			case $type == 's':
				$template = "<div id='focusme' class='alert alert-success'><i class='fa fa-check'></i>&nbsp; ".$message."<span style='float:right;' class='glyphicon glyphicon-remove sys_m_close'></span></div>";
			break;
			case $type == 'n':
				$template = "<div id='focusme' class='alert alert-info'><i class='fa fa-check'></i>&nbsp; ".$message."<span style='float:right;' class='glyphicon glyphicon-remove sys_m_close'></span></div>";
			break;
			case $type == 'p':
				$template = $message;
			break;
			case $type === FALSE;
				$template = $message;
			break;
		}
		
		if($type AND $message AND $redirect)
		{
			$this->session->set_flashdata($var_name,$template);	
			redirect($redirect);
		}elseif($type AND $message AND $redirect == FALSE){
			return $template;
		}
		
		if($redirect == FALSE AND $message == FALSE AND $redirect == FALSE)
		{
			return $this->session->flashdata($var_name);
		}
	}

	function pagination_style()
	{
	    // Style
	  $config['full_tag_open'] = '<ul class="pagination">';
      $config['full_tag_close'] = '</ul>';
      $config['prev_link'] = '&lt; Prev';
      $config['prev_tag_open'] = '<li>';
      $config['prev_tag_close'] = '</li>';
      $config['next_link'] = 'Next &gt;';
      $config['next_tag_open'] = '<li>';
      $config['next_tag_close'] = '</li>';
      $config['cur_tag_open'] = '<li class="active"><a href="#">';
      $config['cur_tag_close'] = '</a></li>';
      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';
      $config['first_link'] = FALSE;
      $config['last_link'] = FALSE;
      
      return $config;
      
	}
}
?>
