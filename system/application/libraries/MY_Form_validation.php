<?php
	class MY_form_validation extends CI_Form_validation{
		
		private $ci;
		
		public function __construct($rules = array()) 
		{ 
		  parent::__construct($rules); 
		  $this->CI->lang->load('MY_form_validation'); 
		  $this->ci =& get_instance();
		}
		
		public function is_existing($value, $par = ""){
			
			$this->ci->load->model('M_enrollments');
			
			$res = $this->ci->M_enrollments->get_by(array('studid'=> $value), true);
			
			if($res)
			{
				return true;
			}
			else{
				return false;
			}
		}
		/**
		 * Match one field to another
		 * Added edit param example is_unique[user.username.edit]
		 * Edit means it will exclude it self
		 *
		 * @access	public
		 * @param	string
		 * @param	field
		 * @return	bool
		 */
		public function is_unique($str, $field)
		{
			$ex_array = explode('.', $field);
			$table = $ex_array[0];
			$field = $ex_array[1];

			$q = $this->CI->db->query("SHOW KEYS FROM $table WHERE Key_name = 'PRIMARY'")->row();
  		$primary_key = $q->Column_name;

			$where[$field] = $str;

			if(isset($ex_array[2]))
			{
				$where[$field.' <> '] = $ex_array[2];
			}

			if($this->CI->input->post($primary_key) > 0){
		   $query = $this->CI->db->limit(1)->get_where($table, array($field => $str,$primary_key.' !='=>$this->CI->input->post($primary_key)));
			}else{
				$query = $this->CI->db->limit(1)->get_where($table, $where);
			}

			// vd($this->ci->db->last_query());
			return $query->num_rows() === 0;
	  }

	    public function alpha_dash_space($str)
		{
			return ( ! preg_match("/^([-a-z0-9_ ])+$/i", $str)) ? FALSE : TRUE;
		}
		
		public function alpha_space($str)
		{
			return ( ! preg_match("/^([a-z ])+$/i", $str)) ? FALSE : TRUE;
		}
		
		public function alphanum_space($str)
		{
			return ( ! preg_match("/^([a-z0-9 ])+$/i", $str)) ? FALSE : TRUE;
		}
		
		public function text_all($str)
		{
			return ( ! preg_match("/^([-a-z0-9_#,.!& ])+$/i", $str)) ? FALSE : TRUE;
		}
		
		public function enumeration($str)
		{
			return ( ! preg_match("/^([a-z0-9, ])+$/i", $str)) ? FALSE : TRUE;
		}
		
		public function check_student_nationality()
		{
			if(strtolower($this->CI->input->post('child_nationality')) !== 'filipino')
			{
				if($this->CI->input->post('nationality_ssp_number') AND 
				   $this->CI->input->post('nationality_visa_status') AND
				   $this->CI->input->post('nationality_auth_stay') AND
				   $this->CI->input->post('nationality_passport_no') AND
				   $this->CI->input->post('nationality_icard_no') AND
				   $this->CI->input->post('nationality_date_issued')
				  )
				  {
					return TRUE;
				  }else{
					return FALSE;
				  }
			}else{
				return TRUE;
			}
		}
		
		function datechecker($date_from_input)
		{
			//Date From Input must be m/d/Y
		
			$date = DateTime::createFromFormat('m/d/Y', $date_from_input);
			if($date !== FALSE)
			{
				$d_e = explode('/',$date->format('m/d/Y'));

				if($date AND checkdate($d_e[0],$d_e[1],$d_e[2]))
				{
					return TRUE;
				}else{
					return FALSE;
				}
			}else{
				return FALSE;
			}
		}
		
		public function remove_space($string)
		{
			return trim(str_replace(' ','',$string));
		}
		
		public function remove_wspace($string)
		{
			return trim(preg_replace('/\s+/', '', $string));
		}

		public function is_unik_edit($str, $field)
		{
			$exclude = $_POST['validate_unique_data'];
			list($table, $field)=explode('.', $field);
			$this->CI->db->where($field.' <>',$exclude);
			$query = $this->CI->db->limit(1)->get_where($table, array($field => $str));
			return $query->num_rows() === 0;
		}
	}
?>