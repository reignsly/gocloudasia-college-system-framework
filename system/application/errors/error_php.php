
<?
	$config = get_config();
	$base_url = $config['base_url'];
?>
<link href="<?php echo $base_url.'assets/css/bootstrap.min.css'; ?>" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo $base_url.'assets/css/pace.css'; ?>" rel="stylesheet" type="text/css" />

<div id="container" class='container'>
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-info">
				<h4>A PHP Error was encountered</h4>
			</div>
			<div class="well">
				<p><strong>Severity: <?php echo $severity; ?></strong></p>
				<p><strong>Message:  <?php echo $message; ?></strong></p>
				<p><strong>Filename: <?php echo $filepath; ?></strong></p>
				<p><strong>Line Number: <span class="badge"><?php echo $line; ?></span></strong></p>
			</div>
		</div>
	</div>
</div>
