<!DOCTYPE html>
<html lang="en">
<head>
<title>404 Page Not Found</title>
<?
	$config = get_config();
	$base_url = $config['base_url'];
?>
<link href="<?php echo $base_url.'assets/css/bootstrap.min.css'; ?>" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo $base_url.'assets/css/pace.css'; ?>" rel="stylesheet" type="text/css" />
<style type="text/css">
	body{
		background-color:#269;
		background-image: linear-gradient(white 2px, transparent 2px),
		linear-gradient(90deg, white 2px, transparent 2px),
		linear-gradient(rgba(255,255,255,.3) 1px, transparent 1px),
		linear-gradient(90deg, rgba(255,255,255,.3) 1px, transparent 1px);
		background-size:100px 100px, 100px 100px, 20px 20px, 20px 20px;
		background-position:-2px -2px, -2px -2px, -1px -1px, -1px -1px
	}
</style>

</head>
<body>
	<p></p>
	<p></p>
	<p></p>
	<p></p>
	<div id="container" class='container well'>
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					<?php echo '<i class="glyphicon glyphicon-exclamation-sign" style="color:red" ></i>&nbsp; '.$heading; ?>
				</div>
				<div class="alert alert-success">
					<h4><?php echo $message; ?></h4>
				</div>
				<div class="alert alert-warning">
					<button class='btn btn-info' onclick="window.history.back()">
						BACK TO PREVIOUS PAGE
					</button>
				</div>
				<p></p>
				<p class="text-muted credit">Copy &copy; GoCloudAsia 2014</p>
			</div>
		</div>
	</div>
</body>
</html>