<?php

class MY_Controller Extends CI_Controller
{
	protected $layout_view = 'application';
	protected $welcome_view = 'welcome';
	protected $content_view ='';
	protected $view_data = array();
	protected $user;
	protected $userlogin;
	protected $departments;
	protected $user_menus = array();
	protected $disable_layout = FALSE;
	protected $disable_menus = FALSE;
	protected $disable_views = FALSE; //DISABLE THE LOADING OF VIEW - FOR AJAX PURPOSE DEC 11 2013
	
	public function __construct()
	{
		parent::__construct();
		
		$this->output->enable_profiler(ENVIRONMENT == 'development' ? false : false);
		if(ENVIRONMENT === 'development'){ $this->db_connection->check(false, true);}#check database connection if not log in

		/**
		 * Autoload Models
		 */

		$this->load->model(array(
			'M_settings',
			'M_menus',
			'M_grading_periods',
			'M_departments',
			'M_sys_par',
			'M_open_semesters',
			'M_core_model'
		));  
		
		$this->user = $this->session->userdata('userType');
		$this->userlogin = $this->session->userdata('userlogin');
		$this->userid = $this->session->userdata('userid');
		$this->islogin = $this->session->userdata("logged_in") ? TRUE : FALSE;
		$this->isadmin = strtolower($this->session->userdata('userType')) === "admin" ? TRUE : FALSE;
		$this->view_data['global_message'] = array();
		
		$this->check_system_settings(); // Check the necessary tables to run
		
		$this->setting 	= $this->M_settings->get_settings();
		$this->syspar 	= $this->M_sys_par->get_sys_par();
		$this->layout_view =  $this->syspar->application_layout ? $this->syspar->application_layout : 'application';
		$this->welcome_view = $this->syspar->welcome_layout ? $this->syspar->welcome_layout : 'welcome';

		$this->current_open_semester = $this->cos = $this->M_open_semesters->get_current_semesters();
		$this->set_open_semester(); // `````` set user open semester - used by old codes

		$this->departments 	= $this->M_departments->get_departments($this->user);
		$this->user_menus = $this->M_menus->get_all_menus($this->user);
		$this->current_grading_period = $this->M_grading_periods->get_current_grading_period();	
		$this->view_data['system_message'] = $this->_msg = $this->session->flashdata('system_message');
		$this->view_data['user_menus'] = $um = $this->user_menus;
		$this->view_data['mydepartment'] = $md = $this->departments;

		//Notification
		$this->get_system_notification();
	}
	
	public function _output($output)
    {
		$this->view_data['user_menus'] = $this->user_menus;
		$this->view_data['elapsed_time'] = $this->benchmark->elapsed_time('total_execution_time_start', 'total_execution_time_end');
		$this->view_data['memory_usage'] = ( ! function_exists('memory_get_usage')) ? '0' : round(memory_get_usage()/1024/1024, 2).'MB';
		
         if($this->content_view !== FALSE && empty($this->content_view)){
			$this->content_view = $this->router->class . '/' . $this->router->method;
        }
		
        $yield = file_exists(APPPATH . 'views/' . $this->content_view . EXT) ? $this->load->view($this->content_view, $this->view_data, TRUE) : FALSE ;
       
		if($this->disable_layout == FALSE)
		{
			if($this->layout_view)
			{
				$this->view_data['yield'] = $yield;
				$this->view_data['disable_menus'] = $this->disable_menus;
				if($this->disable_views == false){
					echo $this->load->view('layouts/' . $this->layout_view, $this->view_data, TRUE);
				}
			}else{
				if($this->disable_views == false){
					
					echo $yield;
				}
			}
			
		}else{
			
			if($this->disable_views == false){
				echo $this->load->view($this->content_view, $this->view_data, TRUE);
			}
		}
		
		if($this->disable_views == false){
			//output profiler
			echo $output;
		}
    }
	
	// CHECK SYSTEM SETTINGS AND SYSPAR
	private function check_system_settings(){

		if($this->db->table_exists('settings') == false){
			show_error('System settings is missing, please contact your provider');
		}

		if($this->db->table_exists('sys_par') == false){
			show_error('System Parameter is missing, please contact your provider');	
		}

		#load models
		$this->load->model(array('M_settings','M_menus','M_grading_periods','M_departments','M_sys_par','M_open_semesters','M_academic_years','M_semesters'));

		if($this->M_sys_par->get_sys_par() == false){
			show_error('System Parameter is not properly configured, please contact your provider');	
		}

		#check if usertype / department active - additional security
		if(isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']){
			$role = $this->M_departments->pull(array('department'=>$this->user),'visible');

			if($role && $role->visible == "1"){
				
			}else{
				$this->session->sess_destroy();
				redirect('auth/logout');
			}

			unset($get);
			$get['fields'] = "id";
			$get['single'] = true;
			
			//Check academic year
			if(!$this->M_academic_years->get_record(false, $get)){
				$this->view_data['global_message'][] = "No record for 'Academic Year or School Year' found.";
			}

			//Check M_semesters
			if(!$this->M_semesters->get_record(false, $get)){
				$this->view_data['global_message'][] = "No record for 'Semester' found.";
			}

			//Check M_grading_periods
			if(!$this->M_grading_periods->get_record(false, $get)){
				$this->view_data['global_message'][] = "No record for 'Grading Period' found.";
			}else{
				//check if there is current grading period set
				if(!$this->M_grading_periods->get_current_grading_period()){
					$this->view_data['global_message'][] = "No 'Current Grading Period' set.";
				}
			}

			//Check M_open_semesters
			if(!$this->M_open_semesters->get_record(false, $get)){
				$this->view_data['global_message'][] = "No record for 'Open Semester' found.";
			}else{
				//check if there is open semester set
				if(!$this->M_open_semesters->get_current_semesters()->system){
					$this->view_data['global_message'][] = "No current 'Open Semester Set'.";
				}
			}
		}
	}

	/**
	 * Get System Notifications
	 */	
	public function get_system_notification()
	{
		$n = false;

		// $n = array(
		// 		'title' => '',
		// 		'muted' => '',
		// 		'url' => '',
		// 		'msg' => ''
		// 	);

		//check if enrollment open - for admin only
		if($this->islogin && $this->isadmin){
			
			if($this->cos->enrollment){
				$e = $this->cos->enrollment;
				$n[] = array(
						'title' => "Enrollment",
						'muted' => "Reminder",
						'msg' 	=> "Enrollment is currently Open for <strong> School Year $e->school_year , $e->semester </strong>",
						'url' 	=> site_url('open_semester')
					);
			}
		}

		//check if user open semester is equal to the system open semester
		if($this->islogin){
			$s = $this->cos->system;
			$u = $this->cos->user;
			if($u && $s && ($u->semester_id !== $s->semester_id)){
				$n[] = array(
						'title' => "Open Semester",
						'muted' => "Warning",
						'url'		=> site_url('open_semester_employee_settings'),
						'msg' 	=> "Please note that <strong>your open semester</strong> is <i>different</i> from the <strong>system open semester</strong>."
					);
			}
		}

		$this->view_data['bar_notification'] = $n;
	}

	function issue_count($user_id)
	{
	  $count = 0;
		$this->load->model(array('M_issues'));
	  // Count Issues
	  $issues = $this->M_issues->count_all_by_user_id($user_id);
	  if (!empty($issues)) {
	    $count += intval($issues) ;
	  }
	  
	  $this->view_data['total_issues'] = $count;
	}
	
	function pagination_style()
	{
	    // Style
	  $config['full_tag_open'] = '<ul class="pagination">';
      $config['full_tag_close'] = '</ul>';
      $config['prev_link'] = '&lt; Prev';
      $config['prev_tag_open'] = '<li>';
      $config['prev_tag_close'] = '</li>';
      $config['next_link'] = 'Next &gt;';
      $config['next_tag_open'] = '<li>';
      $config['next_tag_close'] = '</li>';
      $config['cur_tag_open'] = '<li class="active"><a href="#">';
      $config['cur_tag_close'] = '</a></li>';
      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';
      $config['first_link'] = FALSE;
      $config['last_link'] = FALSE;
      
      return $config;
      
	}
	
	/*
		CHECK IF CURRENT URL IS INCLUDED IN THE MENU LIST ACCESIBLE BY THE DEPARTMENT
		OVER_RIDE (controller/method/param)  - ADD ACCESS IF THE GIVEN STRING OR ARRAY IS IN THE DEPARTMENT (controller) MENU LIST
	*/
	function menu_access_checker($over_ride = false)
	{
		// vd($this->uri);
		// vd($this->router->method);
		
		$pass = false;
		
		$this->session_checker->check_if_alive();
		
		if($this->user_menus)
		{
			$pass = $this->check_department_menu_list();
			
			if($pass)
			{				
				//PAGE IS ACCESIBLE
			}
			else
			{
				//CHECK OVER_RIDE
				if($over_ride)
				{
					$pass = $this->check_over_ride_menu_list($over_ride);
				}
			}
		}
		// vd($over_ride);
		if(!$pass)
		{
			show_error("<div class='' style='color:red'><h4><span class = 'glyphicon glyphicon-ban-circle'></span>&nbsp;  Sorry, you dont have access to this page.</h4></div>", 404);
		}
	}
	
	
	/*
		@uri_string - contains controll/method/param
	*/
	function check_over_ride_menu_list($over_ride = false)
	{
		$pass = false;
		
		if(!$over_ride) { return false; }
		
		if(is_array($over_ride)){
			foreach($this->user_menus as $menu){
				foreach($over_ride as $val){
					if($val == $menu->controller){
						$pass = true;
						break;
					}
				}
				if($pass){
					break;
				}
			}
		}
		else if(is_string($over_ride))
		{
			foreach($this->user_menus as $menu){				
				if($over_ride == $menu->controller){
					$pass = true;
					break;
				}
			}
		}
		
		return $pass;
	}
	
	function check_department_menu_list()
	{
		$pass = false;
		// vd($this->uri);
		// vd($this->router);
		foreach($this->user_menus as $menu)
		{
			if($menu->visible == 1 && $menu->menu_lvl == 2)
			{
				$arr_con = explode('/', $menu->controller);
				
				switch(count($arr_con))
				{
					case 1:
						//MEANS METHOD IS INDEX
						if($arr_con[0] == $this->router->class){ 
							$pass = true; 
						}
					break;
					case 2:
						if($arr_con[0] == $this->router->class
						&& $arr_con[1] == $this->router->method){
							$pass = true;
						}
					break;
					case 3:
						if($arr_con[0] == $this->router->class
						&& $arr_con[1] == $this->router->method
						&& $arr_con[2] == $this->uri->segment(3)){
							$pass = true;
						}
					break;
					
				}
				
				if($pass)
				{
					break;
				}
			}
		}
		
		return $pass;
	}


	function check_the_active_menu($controller = false)
	{
		if($controller)	
		{
			$pass = false;

			$arr_con = explode('/', $controller);
					
			switch(count($arr_con))
			{
				case 1:
					//MEANS METHOD IS INDEX
					if($arr_con[0] == $this->router->class){ 
						$pass = true; 
					}
				break;
				case 2:
					if($arr_con[0] == $this->router->class
					&& $arr_con[1] == $this->router->method){
						$pass = true;
					}
				break;
				case 3:
					if($arr_con[0] == $this->router->class
					&& $arr_con[1] == $this->router->method
					&& $arr_con[2] == $this->uri->segment(3)){
						$pass = true;
					}
				break;
				
			}
			
			if($pass)
			{
				break;
			}
		}
		
		return $pass;
	}
	
	/*
		SLY
		@PARAM - string LIKE profile, grade, 
		CHECKS WHETHER THE DEPARTMENT HAS ACCESS TO STUDENT DATA GIVEN IN THE PARAMETER
	*/
	function check_student_profile_access($par = false)
	{
		$xfld = "";
		$xaccess = 0;
		if($par)
		{
			switch($par)
			{
				case "profile":
					$xaccess = $this->departments->stud_profile;
				break;
				case "grade":
					$xaccess = $this->departments->stud_grade;
				break;
				case "fees":
					$xaccess = $this->departments->stud_fees;
				break;
				case "transcript":
				case "otr":
					$xaccess = $this->departments->stud_otr;
				break;
				case "issues":
					$xaccess = $this->departments->stud_issues;
				break;
				default:
					$xaccess = 0;
				break;
			}
		}
		
		if($xaccess == 0)
		{
			show_error("<div class='' style='color:red'><h4><span class = 'glyphicon glyphicon-ban-circle'></span>&nbsp;  Sorry, you dont have access to this page. Go back to <a class='btn btn-info' href='".base_url()."home'>HOME</a></h4></div>", 404);
		}
	}

	/**
	 * Check Department Access to Student Profile
	 * @param string $field Field in the database to check
	 */
	public function check_stud_access($field)
	{
		if(isset($this->departments->$field)){
			if($this->departments->$field !== "1"){
				show_error("<div class='' style='color:red'><h4><span class = 'glyphicon glyphicon-ban-circle'></span>&nbsp;  Sorry, you dont have access to this page. Go back to <a class='btn btn-info' href='".base_url()."home'>HOME</a></h4></div>", 404);
			}
		}else{
			show_404();
		}
	}

	/**
	 * Check User Access To Student Profile Menus (Profile, Grade, View Fees, Transcript, Issues, Subjects)
	 */
	public function check_access_studentprofile_menu()
	{
		$this->menu_access_checker(array(
			'search/search_student',
			'enrollees/daily_enrollees',
			'search/master_list_by_course',
			'search/search_enrollee',
			'search/list_students/paid',
			'search/list_students/unpaid',
			'search/list_students/fullpaid',
			'search/list_students/partialpaid',
			'fees/view_fees',
		));
	}

	/**
	 * Check or get id of a hash string usually a table id
	 * @param string $hash 
	 * @param bool $show_error - show_404 if false
	 */
	public function check_hash($hash, $show_error = true){
		$x = $this->hs->decrypt($hash);
		
		if($x){
			return $x[0];
		}
		else{
			if($show_error === true){
				show_404();
			}else{
				return false;
			}
		}
	}

	/**
	 * Check Record - usually used to validate if record or id passed in url has data
	 */
	public function check_rec($rec)
	{
		if($rec){}
		else{show_404();}
	}

	function _msg($type = FALSE,$message = FALSE,$redirect = FALSE,$var_name = 'system_message')
	{
		$template = "";
		$type = strtolower(trim($type));
		switch($type)
		{
		 	case $type == 'e':
				$template = "<div class='alert alert-danger'><i class='fa fa-exclamation'></i>&nbsp; ".$message."<span style='float:right;' class='glyphicon glyphicon-remove sys_m_close'></span></div>";
				
			break;
			case $type == 's':
				$template = "<div class='alert alert-success'><i class='fa fa-check'></i>&nbsp; ".$message."<span style='float:right;' class='glyphicon glyphicon-remove sys_m_close'></span></div>";
			break;
			case $type == 'n':
				$template = "<div class='alert alert-info'><i class='fa fa-check'></i>&nbsp; ".$message."<span style='float:right;' class='glyphicon glyphicon-remove sys_m_close'></span></div>";
			break;
			case $type == 'p':
				$template = $message;
			break;
			case $type === FALSE;
				$template = $message;
			break;
		}
		
		if($type AND $message AND $redirect)
		{
			$this->session->set_flashdata($var_name,$template);	
			redirect($redirect);
		}elseif($type AND $message AND $redirect == FALSE){
			return $template;
		}
		
		if($redirect == FALSE AND $message == FALSE AND $redirect == FALSE)
		{
			return $this->session->flashdata($var_name);
		}
	}

	/**
	 * Redirect with MSG
	 * Eliminate the if else
	 * @param boolean $status true/false - success or failed
	 * @param string $url - redirect to
	 * @param string $s_msg - success msg
	 * @param string $f_msg - fail message
	 * @param array $log - Save activity log
	 */	
	public function __msg($status, $url, $s_msg, $f_msg = false, $log = false)
	{
		$f_msg = $f_msg ? $f_msg : "Transaction failed, please try again";
		$msg = $status ? $s_msg : $f_msg;
		$code = $status ? "s" : 'e';

		//save activity log
		if($log){
			$log = (object)$log;
			activity_log($log->activity, false, $log->log);
		}

		$this->_msg($code, $msg, $url);
	}

	function check_if_already_logged_in()
	{
		if($this->islogin === TRUE)
		{
			redirect('home');
		}
	}

	/** 
		* CLONE SCHOOL YEAR RECORD - Copy and change the academic year id of the ff: subjects, room, block_sections
		* @new_sy_id = new academic_year_id
		* @prev_sy_id = copy from this academic_year_id, if false it will detect
	 	*/
	function clone_sy_data($new_sy_id = false, $prev_sy_id = false){
			$this->load->model(array('M_academic_years','M_rooms','M_subjects',"M_block_section_settings",'M_open_semesters','M_coursefinances'));

			$this->M_rooms->cloned_rooms_from_previous_sy($new_sy_id, $prev_sy_id);
			// $this->M_subjects->cloned_subjects_from_previous_sy($new_sy_id,$prev_sy_id);
			// $this->M_block_section_settings->cloned_blocks_from_previous_sy($new_sy_id, $prev_sy_id);
			$this->M_coursefinances->cloned_coursefinance_from_previous_sy($new_sy_id, $prev_sy_id);
	}

	/* ARRANGE RECORD TO ALPHABETICAL
		@record - data list to be arrange
		@field - what field will be arrange
		@order - desc or asc, default is asc
		result will look like this
		$result['0'][0]
		$result['1'][0]
		$result['a'][0]
		$result['b'][0]
		$result['c'][0] etc etc
	 */
	function sort_data_alpha($record = false, $field = false, $order = 'ASC'){

			$alphabet = array('0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
	
			$alphabet_array = array();
			//GENERATE AND ARRANGE ARRAY OF SUBJECTS
			if($record){
					foreach($record as $key => $value){
						if($value != null){
							$word = trim($value->$field);
							
						if($word != ""){
							$f_letter = strtolower($word[0]);
							$pass = 0;
							$pass2 = 0;
							if($order == "ASC"){
								foreach($alphabet as $letter)
								{
									$pass2++;
									if($letter == $f_letter)
									{
										$alphabet_array[$letter][$key] = $value;
										$pass++;
										break;
									}
								}
							}
							// if($order == "DESC"){

							// 	for($x = count($alphabet); $x >=0; $x--){
							// 		$pass2++;

							// 		if(isset($alphabet[$x])){

							// 			if($alphabet[$x] == $f_letter){

							// 				$alphabet_array[$alphabet[$x]][$key] = $value;
							// 				$pass++;
							// 				break;
							// 			}
							// 		}
							// 	}
							// }
							//NO Category
							if($pass == 0 && $pass2 > 0){
								$alphabet_array['others'][$key] = $value;
							}
						}
					}
				}
			}
			ksort($alphabet_array);
			// vd($alphabet_array);
			return $alphabet_array;
	}

	/**
	 * Set Open Semester Variable
	 * Used by old codes
	 */
	public function set_open_semester()
	{
		unset($os);
		$os['id'] = 0;
		$os['year_from'] = "";
		$os['year_to'] = "";

		if($this->cos->user){
			
			$os['id'] = $this->cos->user->open_semester_id;
			$os['academic_year_id'] = $this->cos->user->academic_year_id;
			$os['year_from'] = $this->cos->user->year_from;
			$os['year_to'] = $this->cos->user->year_to;			
			$os['name'] = $this->cos->user->semester;			
		}
		$this->open_semester = (object)$os;
	}
}

class Systemlogin_Controller Extends CI_Controller
{
	protected $layout_view = 'blank';
	protected $content_view ='';
	public $loggin = false;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','my_dropdown'));
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->output->enable_profiler(true);
		$this->view_data['system_message'] = $this->_msg = $this->session->flashdata('system_message');
		
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		/** If Database Connection is invalid redirect to edit connection ***/
		if($this->loggin && !$this->db_connection->check(true)){
			if($this->router->class == "gopanel" && $this->router->method != "kabit"){
				redirect('gopanel/kabit');
			}
		}
	}

	public function _output($output)
  {	
        if($this->content_view !== FALSE && empty($this->content_view)){
			$this->content_view = $this->router->class . '/' . $this->router->method;
        }
		
        $yield = file_exists(APPPATH . 'views/' . $this->content_view . EXT) ? $this->load->view($this->content_view, $this->view_data, TRUE) : FALSE ;
       
		if($this->layout_view)
		{
			$this->view_data['yield'] = $yield;
			echo $this->load->view('layouts/' . $this->layout_view, $this->view_data, TRUE);
		}else{
			echo $yield;
		}

		echo $output;
  }

	public function _msg($type = FALSE,$message = FALSE,$redirect = FALSE,$var_name = 'system_message')
	{
		$template = "";
		$type = strtolower(trim($type));
		switch($type)
		{
		 	case $type == 'e':
				$template = "<div id='focusme' class='alert alert-danger'><i class='fa fa-exclamation'></i>&nbsp; ".$message."<span style='float:right;' class='glyphicon glyphicon-remove sys_m_close'></span></div>";
				
			break;
			case $type == 's':
				$template = "<div id='focusme' class='alert alert-success'><i class='fa fa-check'></i>&nbsp; ".$message."<span style='float:right;' class='glyphicon glyphicon-remove sys_m_close'></span></div>";
			break;
			case $type == 'n':
				$template = "<div id='focusme' class='alert alert-info'><i class='fa fa-check'></i>&nbsp; ".$message."<span style='float:right;' class='glyphicon glyphicon-remove sys_m_close'></span></div>";
			break;
			case $type == 'p':
				$template = $message;
			break;
			case $type === FALSE;
				$template = $message;
			break;
		}
		
		if($type AND $message AND $redirect)
		{
			$this->session->set_flashdata($var_name,$template);	
			redirect($redirect);
		}elseif($type AND $message AND $redirect == FALSE){
			return $template;
		}
		
		if($redirect == FALSE AND $message == FALSE AND $redirect == FALSE)
		{
			return $this->session->flashdata($var_name);
		}
	}

	function pagination_style()
	{
	    // Style
	  $config['full_tag_open'] = '<ul class="pagination">';
      $config['full_tag_close'] = '</ul>';
      $config['prev_link'] = '&lt; Prev';
      $config['prev_tag_open'] = '<li>';
      $config['prev_tag_close'] = '</li>';
      $config['next_link'] = 'Next &gt;';
      $config['next_tag_open'] = '<li>';
      $config['next_tag_close'] = '</li>';
      $config['cur_tag_open'] = '<li class="active"><a href="#">';
      $config['cur_tag_close'] = '</a></li>';
      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';
      $config['first_link'] = FALSE;
      $config['last_link'] = FALSE;
      
      return $config;
      
	}
}

class Backend_Controller Extends CI_Controller
{
	protected $layout_view = 'bower_bootstrap_application';
	protected $content_view ='';
	public $loggin = false;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library(['form_validation','hs']);
		$this->load->helper(array('form','my_dropdown','url_encrypt'));
		$this->load->library('session');
		$this->output->enable_profiler(true);
		$this->view_data['system_message'] = $this->_msg = $this->session->flashdata('system_message');

		$this->load->model(['M_settings']);

		$this->loggin = $this->view_data['login'] = $this->session->userdata('gopanel_logged_in') == TRUE ? true : false;
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		$this->setting 	= $this->M_settings->get_settings();

		//check if logged in
		$this->secure_page();
		
		/** If Database Connection is invalid redirect to edit connection ***/
		if($this->loggin && !$this->db_connection->check(true)){
			if($this->router->class == "gopanel" && $this->router->method != "kabit"){
				// redirect('gopanel/kabit');
			}
		}
	}

	public function _output($output)
  {	
    if($this->content_view !== FALSE && empty($this->content_view)){
			$this->content_view = $this->router->class . '/' . $this->router->method;
    }
		
    $yield = file_exists(APPPATH . 'views/' . $this->content_view . EXT) ? $this->load->view($this->content_view, $this->view_data, TRUE) : FALSE ;
       
		if($this->layout_view)
		{
			$this->view_data['yield'] = $yield;
			echo $this->load->view('layouts/' . $this->layout_view, $this->view_data, TRUE);
		}else{
			echo $yield;
		}

		// echo $output;
  }

	public function _msg($type = FALSE,$message = FALSE,$redirect = FALSE,$var_name = 'system_message')
	{
		$template = "";
		$type = strtolower(trim($type));
		switch($type)
		{
		 	case $type == 'e':
				$template = "<div id='focusme' class='alert alert-danger'><i class='fa fa-exclamation'></i>&nbsp; ".$message."<span style='float:right;' class='glyphicon glyphicon-remove sys_m_close'></span></div>";
				
			break;
			case $type == 's':
				$template = "<div id='focusme' class='alert alert-success'><i class='fa fa-check'></i>&nbsp; ".$message."<span style='float:right;' class='glyphicon glyphicon-remove sys_m_close'></span></div>";
			break;
			case $type == 'n':
				$template = "<div id='focusme' class='alert alert-info'><i class='fa fa-check'></i>&nbsp; ".$message."<span style='float:right;' class='glyphicon glyphicon-remove sys_m_close'></span></div>";
			break;
			case $type == 'p':
				$template = $message;
			break;
			case $type === FALSE;
				$template = $message;
			break;
		}
		
		if($type AND $message AND $redirect)
		{
			$this->session->set_flashdata($var_name,$template);	
			redirect($redirect);
		}elseif($type AND $message AND $redirect == FALSE){
			return $template;
		}
		
		if($redirect == FALSE AND $message == FALSE AND $redirect == FALSE)
		{
			return $this->session->flashdata($var_name);
		}
	}

	public function pagination_style()
	{
	  $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['prev_link'] = '&lt; Prev';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['next_link'] = 'Next &gt;';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $config['first_link'] = FALSE;
    $config['last_link'] = FALSE;
    
    return $config;   
	}

	/**
	 * Secure GoPanel Page
	 */
	public function secure_page()
	{
		if($this->session->userdata('gopanel_logged_in') == TRUE)
		{
			
		}
		else{
			// vd($this->router->class);
			if($this->router->class != "gopanel_auth"){
				redirect('backend');
			}
		}
	}
}