<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Total_income extends MY_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','form','url_encrypt'));
		$this->load->library(array('form_validation'));
		$this->load->model(array('M_settings','M_enrollments','M_payment_records','M_student_total_file','M_grading_periods','M_studentpayments_details','M_student_refund','M_core_model'));
		$this->session_checker->open_semester();
		$this->session_checker->check_if_alive();
		$this->load->helper('my_dropdown');
	}
	
	public function index($page = 0){
		
		$this->view_data['custom_title'] = "Student Payments";	
	
/* 		SELECT 
			sp.`or_no`,
			e.name,
			yr.`year`,
			c.`course`,
			sp.`total`
		FROM studentpayments sp
		LEFT JOIN enrollments e ON e.id = sp.`enrollmentid`
		LEFT JOIN `years` yr ON yr.`id` = e.`year_id`
		LEFT JOIN courses c ON c.`id` = e.`course_id`;
 */		
		//PAGINATION
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		$like = false;
		$order_by = false;
		
		$filter['enrollments.semester_id'] = $this->open_semester->id;
		$filter['enrollments.sy_from'] = $this->open_semester->year_from;
		$filter['enrollments.sy_to'] = $this->open_semester->year_to;
		
		
		$arr_filters = array();
		$suffix = "";
		
		if($_GET)
		{
	/* 		if(isset($_GET['lastname'])){
				$this->view_data['lastname'] = $lastname = trim($_GET['lastname']);
				$like['enrollments.lastname'] = $lastname;
				$arr_filters['lastname'] = $lastname;
			}
			
			if(isset($_GET['fname'])){
				$this->view_data['fname'] = $fname = trim($_GET['fname']);
				$like['enrollments.fname'] = $fname;
				$arr_filters['fname'] = $fname;
				
			}
			
			if(isset($_GET['studid'])){
				$this->view_data['studid'] = $studid = trim($_GET['studid']);
				$like['enrollments.studid'] = $studid;
				$arr_filters['studid'] = $studid;
			} */
			
			if(isset($_GET['year_id']) && $_GET['year_id'] != ''){
				$this->view_data['year_id'] = $year_id = trim($_GET['year_id']);
				$filter['enrollments.year_id'] = $year_id;
				$arr_filters['year_id'] = $year_id;
			}
			
			if(isset($_GET['course_id']) && $_GET['course_id'] != ''){
				$this->view_data['course_id'] = $course_id = trim($_GET['course_id']);
				$filter['enrollments.course_id'] = $course_id;
				$arr_filters['course_id'] = $course_id;
			}
			
			$suffix = array_to_geturl($arr_filters);
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'studentpayments.id',
				'studentpayments.or_no',
				'studentpayments.date',
				'enrollments.studid',
				'enrollments.name',
				'years.year',
				'courses.course',
				'studentpayments.total'
		);
		
		$get['where'] = $filter;
		// $get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "enrollments",
				"on"	=> "enrollments.id = studentpayments.enrollmentid",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "courses",
				"on"	=> "courses.id = enrollments.course_id",
				"type"  => "LEFT"
			),
			3 => array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "enrollments.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."total_income/index";
		$config['suffix'] = '?'.http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("studentpayments", $get);
		
		$get['count'] = false;
		$this->view_data['all_rows'] = $all_rows = $this->M_core_model->get_record("studentpayments", $get);
		
		$config["per_page"] = 25;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->post('submit') == "Print")
		{
			$get['all'] = true;
		}
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("studentpayments", $get);
		$this->view_data['links'] = $this->pagination->create_links();
		
		$total_income = 0;
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("studentpayments", $get);
		
		if($all_rows):
			foreach($all_rows as $obj):
				$total_income += $obj->total;
			endforeach;
		endif;
		
		$this->view_data['total_income'] = $total_income;
		
		if($this->input->get('submit') == "Print")
		{
			$this->print_total_income($this->view_data);
		}
	}
	
	public function print_total_income($data = false)
	{
		if($data == false) {show_404();}
		
		if($data['total_rows'] <= 0) {	
			$this->view_data['system_message'] = '<div class="alert alert-danger">No record to be printed.</div>';
			return false;
		}
		
		$search = $data['all_rows'];
		$total_income = $data['total_income'];
		
		if($search)
		{
			$this->load->helper('print');
			
			$html = _html_total_income($search, $total_income); 

			$this->load->library('mpdf');
			
			$mpdf=new mPDF('','FOLIO','','',5,5,5,5,0,0); 

			$mpdf->AddPage('P');

			$mpdf->WriteHTML($html);

			$mpdf->Output();
		}
	}
}
