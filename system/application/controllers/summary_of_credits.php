<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Summary_of_credits extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
	}
	
	public function index()
	{
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_summary_of_credits'));
		
		$this->view_data['summary_of_credits'] = $this->M_summary_of_credits->get();
	}
	
	public function create()
	{
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_summary_of_credits'));
		
		$this->view_data['summary_of_credit'] = FALSE;
		$this->view_data['courses'] = $this->M_summary_of_credits->get_courses();
		
		if($_POST)
		{
			$data['course_id'] = $_POST['course_id'];
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_summary_of_credits->create_summary_of_credit($data);
			if($result['status'])
			{
				redirect('summary_of_credits');
			}
		}
	}
	
	public function show($id)
	{
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_summary_of_credits'));
		
		$this->view_data['summary_of_credit'] = $this->M_summary_of_credits->get($id);
	}
	
	public function edit($id)
	{
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_summary_of_credits','M_courses'));
		
		$this->view_data['summary_of_credit'] = $this->M_summary_of_credits->get($id);
		$this->view_data['courses'] = $this->M_courses->get(FALSE,array('id','course'));
		
		if($_POST)
		{
			$data['course_id'] = $_POST['course_id'];
			$data['updated_at'] = date('Y-m-d H:i:s');
			$id = $_POST['id'];
			
			$result = $this->M_summary_of_credits->update_summary_of_credit($data,$id);
			if($result['status'])
			{
				redirect('summary_of_credits');
			}
		}
	}
	
	public function destroy($id)
	{
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_summary_of_credits','M_credits'));
		
		$credits = $this->M_credits->get($id);
		foreach($credits as $credit):
			$this->M_credits->delete_credits($credit->id);
		endforeach;
		$result = $this->M_summary_of_credits->delete_summary_of_credits($id);
		redirect('summary_of_credits');
	}
}