<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Announcements extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->menu_access_checker('announcements');
		$this->load->model(array('M_announcements'));
	}

	// Create
	public function create()
	{
		$this->check_stud_access('edit_announcement');
		
		$this->view_data['announcement'] = FALSE;
		
		if($_POST)
		{
			if(isset($_POST['to_whom'])){
			while(list(,$val) = each($_POST['to_whom'])){
				if($val=='student'){$data['to_student']=1;}
				if($val=='employee'){$data['to_employee']=1;}
				if($val=='all'){$data['to_all']=1;}
			}
			}
			$data['message'] = $this->input->post('message');
			$data['title'] = $this->input->post('title');
			$data['date'] = date('Y-m-d H:i:s',strtotime($this->input->post('date').' '.date('h:i:s')));
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			$data['user_id'] = $this->session->userdata('userid');
			
			$result = $this->M_announcements->create_announcement($data);
			if($result['status'])
			{
				activity_log('create announcements',$this->userlogin,'Announcements created by: '.$this->user.'Success; announcement Id: '.$result['id']);
				
				redirect('announcements');
			}
		}
	}
	
	// Retrieve
	public function index($page = 0)
	{
		
		$get = false;
		if($_GET)
		{	
			if(isset($_GET['date']) && trim($_GET['date']) != ''){
				$this->view_data['date'] = $date = trim($_GET['date']);
				$get['where']['announcements.date'] = $date;
			}
			
			if(isset($_GET['title']) && trim($_GET['title']) != ''){
				$this->view_data['title'] = $title = trim($_GET['title']);
				$get['like']['announcements.title'] = $title;
				
			}
		}
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."announcements/index";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_announcements->paginate(0,0,$get,true);
		$this->view_data['announcements'] = $a = $this->M_announcements->paginate($page,$config["per_page"],$get);

		$this->pagination->initialize($config);
		$this->view_data['links'] = $this->pagination->create_links();
	}
	
	public function display($hash='')
	{
		$id = $this->check_hash($hash);
		$this->view_data['announcement'] = $this->M_announcements->get($id);
	}
	
	// Update
	public function edit($hash = '')
	{
		$id = $this->check_hash($hash);
		$this->check_stud_access('edit_announcement');
		$this->view_data['announcement'] = $this->M_announcements->get($id);
		
		if($_POST)
		{
			if($_POST)
			{
			while(list(,$val) = each($_POST['to_whom'])){
				if($val=='student'){$data['to_student']=1;}else{$data['to_student']=0;}
				if($val=='employee'){$data['to_employee']=1;}else{$data['to_employee']=0;}
				if($val=='all'){$data['to_all']=1;}else{$data['to_all']=0;}
			}
			$data['message'] = $_POST['message'];
			$data['title'] = $_POST['title'];
			$data['date'] = date('Y-m-d H:i:s',strtotime($this->input->post('date').' '.date('h:i:s')));
			$data['updated_at'] = date('Y-m-d H:i:s');
			$data['user_id'] = $this->session->userdata('userid');
			$id = $_POST['id'];
			
			$result = $this->M_announcements->update_announcement($data,$id);
			if($result['status'])
			{
				activity_log('update announcements',$this->userlogin,'Announcements updated by: '.$this->user.'Success; announcement Id: '.$id);
				
				redirect('announcements');
			}
			}
		}
	}

	// view
	public function view($hash = '')
	{
		$id = $this->check_hash($hash);
		$this->view_data['announcement'] = $this->M_announcements->get($id);
	}
	
	// Update
	public function destroy($hash = '')
	{
		$id = $this->check_hash($hash);
		
		$result = $this->M_announcements->delete_announcement($id);
		
		activity_log('delete announcements',$this->userlogin,'Announcements deleted by: '.$this->user.'Success; announcement Id: '.$id);
		
		redirect('announcements');
	}
}