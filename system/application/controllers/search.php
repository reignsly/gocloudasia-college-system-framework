<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search Extends MY_Controller
{
	public function __construct(){
		parent::__construct();
		$this->session_checker->open_semester();
		$this->load->model(array('M_enrollments'));
		$this->load->helper(array('url_encrypt','my_dropdown'));
		$this->load->model('M_core_model');
	}
		

	public function index()
	{
		$this->session_checker->check_if_alive();
	}
	
	public function search_student($page = 0)
	{
		$this->menu_access_checker();
		//PAGINATION
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->view_data['search_type'] = 'official';
		
		$filter = false;
		$like = false;
		$order_by = false;
		
		$filter['enrollments.semester_id'] = $this->cos->user->semester_id;
		$filter['enrollments.sy_from'] = $this->cos->user->year_from;
		$filter['enrollments.sy_to'] = $this->cos->user->year_to;
		$filter['enrollments.is_deleted'] = 0;
		$filter['enrollments.is_drop'] = 0;
		$filter['enrollments.is_paid'] = 1;
		
		
		$arr_filters = array();
		$suffix = "";
		
		if($_GET)
		{
			if(isset($_GET['submit']) && $_GET['submit'] == "Search")
			{
				$page = 0;
			}
			
			if(isset($_GET['lastname']) && trim($_GET['lastname']) != ''){
				$this->view_data['lastname'] = $lastname = trim($_GET['lastname']);
				$like['enrollments.lastname'] = $lastname;
				$arr_filters['lastname'] = $lastname;
			}
			
			if(isset($_GET['fname']) && trim($_GET['fname']) != ''){
				$this->view_data['fname'] = $fname = trim($_GET['fname']);
				$like['enrollments.fname'] = $fname;
				$arr_filters['fname'] = $fname;
				
			}
			
			if(isset($_GET['studid']) && trim($_GET['studid']) != ''){
				$this->view_data['studid'] = $studid = trim($_GET['studid']);
				$like['enrollments.studid'] = $studid;
				$arr_filters['studid'] = $studid;
			}
			
			if(isset($_GET['year_id']) && trim($_GET['year_id']) != ''){
				$this->view_data['year_id'] = $year_id = trim($_GET['year_id']);
				$filter['enrollments.year_id'] = $year_id;
				$arr_filters['year_id'] = $year_id;
			}
			
			$suffix = array_to_geturl($arr_filters);
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'enrollments.id',
				'enrollments.studid',
				'enrollments.name' ,
				'years.year',
				'courses.course',
				'enrollments.user_id',
				'enrollments.date_of_birth as dob',
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "courses",
				"on"	=> "courses.id = enrollments.course_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "enrollments.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."search/search_student";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("enrollments", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		// $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->post('submit') == "Print")
		{
			$get['all'] = true;
		}
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("enrollments", $get);
		$this->view_data['links'] = $this->pagination->create_links();
		
		if($this->input->post('submit') == "Print")
		{
			$this->print_nstp_students($this->view_data);
		}
	}	
	
	public function search_enrollee($page = 0)
	{	
		$this->menu_access_checker();
		//PAGINATION
		$this->view_data['system_message'] = $this->session->flashdata('system_message');

		//Top Search Parameter
		$this->view_data['search_type'] = 'enrollees';
		
		$filter = false;
		$like = false;
		$order_by = false;
		
		$filter['enrollments.semester_id'] = $this->cos->user->semester_id;
		$filter['enrollments.sy_from'] = $this->cos->user->year_from;
		$filter['enrollments.sy_to'] = $this->cos->user->year_to;
		$filter['enrollments.is_deleted'] = 0;
		$filter['enrollments.is_drop'] = 0;
		$filter['enrollments.is_paid'] = 0;
		
		
		$arr_filters = array();
		$suffix = "";
		
		if($_GET)
		{
			if(isset($_GET['lastname'])){
				$this->view_data['lastname'] = $lastname = trim($_GET['lastname']);
				$like['enrollments.lastname'] = $lastname;
				$arr_filters['lastname'] = $lastname;
			}
			
			if(isset($_GET['fname'])){
				$this->view_data['fname'] = $fname = trim($_GET['fname']);
				$like['enrollments.fname'] = $fname;
				$arr_filters['fname'] = $fname;
				
			}
			
			if(isset($_GET['studid'])){
				$this->view_data['studid'] = $studid = trim($_GET['studid']);
				$like['enrollments.studid'] = $studid;
				$arr_filters['studid'] = $studid;
			}
			
			if(isset($_GET['year_id'])){
				$this->view_data['year_id'] = $year_id = trim($_GET['year_id']);
				$filter['enrollments.year_id'] = $year_id;
				$arr_filters['year_id'] = $year_id;
			}
			
			$suffix = array_to_geturl($arr_filters);
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'enrollments.id',
				'enrollments.created_at',
				'enrollments.studid',
				'enrollments.name' ,
				'enrollments.date_of_birth as dob' ,
				'enrollments.user_id' ,
				'years.year',
				'courses.course' 
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "courses",
				"on"	=> "courses.id = enrollments.course_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "enrollments.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."search/search_enrollee";
		$config['suffix'] = $suffix;
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("enrollments", $get);
		
		$config["per_page"] = 20;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->post('submit') == "Print")
		{
			$get['all'] = true;
		}
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("enrollments", $get);
		$this->view_data['links'] = $this->pagination->create_links();
		
		if($this->input->post('submit') == "Print")
		{
			$this->print_nstp_students($this->view_data);
		}
	}

	public function search_employee($page = 0)
	{
		$this->menu_access_checker();
		
		//PAGINATION
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		// vp($_GET);
		$filter = false;
		$like = false;
		$or_like = false;
		$order_by = false;
		
		$arr_filters = array();
		$suffix = "";
		
		if($_GET)
		{
			if(isset($_GET['name']) && trim($_GET['name']) != ''){
				$this->view_data['name'] = $name = trim($_GET['name']);
				$or_like['employees.last_name'] = $name;
				$or_like['employees.middle_name'] = $name;
				$or_like['employees.first_name'] = $name;
				$arr_filters['name'] = $name;
			}
			
			if(isset($_GET['employeeid']) && trim($_GET['employeeid']) != ''){
				$this->view_data['employeeid'] = $employeeid = trim($_GET['employeeid']);
				$like['employees.employeeid'] = $employeeid;
				$arr_filters['employeeid'] = $employeeid;
				
			}
			
			if(isset($_GET['role']) && trim($_GET['role']) != ''){
				$this->view_data['role'] = $role = trim($_GET['role']);
				$filter['employees.role'] = $role;
				$arr_filters['role'] = $role;
			}
			
			$suffix = array_to_geturl($arr_filters);
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				"employees.*",
				"CONCAT(employees.last_name,',',employees.first_name,' ', employees.middle_name) as fullname"
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		$get['or_like'] = $or_like;
		
		/* $get['join'] = array(
			
			1 => array(
				"table" => "courses",
				"on"	=> "courses.id = enrollments.course_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			)
		); */
		$get['order'] = "employees.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."search/search_employee";
		$config['suffix'] = '?'.http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("employees", $get);
		
		$config["per_page"] = 25;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->post('submit') == "Print")
		{
			$get['all'] = true;
		}
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("employees", $get);
		$this->view_data['links'] = $this->pagination->create_links();
	}
	
	public function master_list_by_course()
	{
		$this->menu_access_checker();
		$this->session_checker->check_if_alive();
		$this->load->model('M_courses');
		$this->view_data['masterlist'] = $master_list = $this->M_courses->masterlist();
		$this->view_data['unassigned'] = $this->M_courses->unassigned();
	}
	
	public function master_list($course_id = false)
	{
		$this->menu_access_checker(array(
			'search/master_list_by_course',
		));
		
		if($course_id !== false AND ctype_digit($course_id))
		{
			$this->load->model('M_courses');
			$this->load->library("pagination");
			$config = $this->pagination_style();
			$config["base_url"] = base_url() ."search/master_list/$course_id/";
			$this->view_data['total_rows'] = $config["total_rows"] = $this->M_courses->masterlist_count($course_id);
			$config["per_page"] = 30;
			$config['num_links'] = 10;
			$config["uri_segment"] = 4;
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			$this->pagination->initialize($config);
			$this->view_data['course'] = $this->M_courses->get($course_id);
			$this->view_data['course_id'] = $course_id;
			$this->view_data['search_results'] = $this->M_courses->masterlist_course_enrollment_profile($course_id,$config["per_page"], $page);
			$this->view_data['links'] = $this->pagination->create_links();
		}else
		{
			show_404();
		}
	}
	
	public function master_list_gender($course_id = false, $gender = false)
	{
		if(!$course_id || !$gender) { show_404(); }
		
		if($gender == "male")
		{
			$this->view_data['custom_title'] = "Male Master List";
		}
		
		if($gender == "female")
		{
			$this->view_data['custom_title'] = "Female Master List";
		}
		
		$this->menu_access_checker(array('search/master_list_by_course'));
		if($course_id !== false AND ctype_digit($course_id))
		{
			$this->load->model('M_courses');
			$this->load->library("pagination");
			$config = $this->pagination_style();
			$config["base_url"] = base_url() ."search/master_list_gender/$course_id/$gender";
			$this->view_data['total_rows'] = $config["total_rows"] = $this->M_courses->get_gender_count($course_id, $gender);
			$config["per_page"] = 50;
			$config['num_links'] = 10;
			$config["uri_segment"] = 5;
			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
			$this->pagination->initialize($config);
			$this->view_data['course'] = $this->M_courses->get($course_id);
			$this->view_data['gender'] = $gender;
			$this->view_data['course_id'] = $course_id;
			$this->view_data['search_results'] = $this->M_courses->masterlist_course_enrollment_profile_gender($course_id, $gender, $config["per_page"], $page);
			$this->view_data['links'] = $this->pagination->create_links();
		}else
		{
			show_404();
		}
	}
	
	public function list_students($x = false, $page = 0)
	{		
		$this->menu_access_checker();
		if($x == false) { show_404(); }
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->view_data['search_type'] = $x;
		
		$filter = false;
		$like = false;
		$order_by = false;
		
		$filter['enrollments.semester_id'] = $this->cos->user->semester_id;
		$filter['enrollments.sy_from'] = $this->cos->user->year_from;
		$filter['enrollments.sy_to'] = $this->cos->user->year_to;
		$filter['enrollments.is_deleted'] = 0;
		$filter['enrollments.is_drop'] = 0;
		
		switch(strtolower($x)){
			case "paid":
				$filter['is_paid'] = 1;
				$this->view_data['custom_title'] = "List of Paid Students";
			break;
			case "unpaid":
				$filter['is_paid'] = 0;
				$this->view_data['custom_title'] = "List of Unpaid Students";
			break;
			case "fullpaid":
				$filter['full_paid'] = 1;
				$this->view_data['custom_title'] = "List of Fully paid Students";
			break;
			case "partialpaid":
				$filter['full_paid'] = 0;
				$filter['is_paid'] = 1;
				$this->view_data['custom_title'] = "List of Partial paid Students";
			break;
			default:
				show_404();
			break;
		}
		
		$arr_filters = array();
		$suffix = "";
		
		if($_GET)
		{
			if(isset($_GET['lastname'])){
				$this->view_data['lastname'] = $lastname = trim($_GET['lastname']);
				$like['enrollments.lastname'] = $lastname;
				$arr_filters['lastname'] = $lastname;
			}
			
			if(isset($_GET['fname'])){
				$this->view_data['fname'] = $fname = trim($_GET['fname']);
				$like['enrollments.fname'] = $fname;
				$arr_filters['fname'] = $fname;
				
			}
			
			if(isset($_GET['studid'])){
				$this->view_data['studid'] = $studid = trim($_GET['studid']);
				$like['enrollments.studid'] = $studid;
				$arr_filters['studid'] = $studid;
			}
			
			if(isset($_GET['year_id'])){
				$this->view_data['year_id'] = $year_id = trim($_GET['year_id']);
				$filter['enrollments.year_id'] = $year_id;
				$arr_filters['year_id'] = $year_id;
			}
			
			$suffix = array_to_geturl($arr_filters);
		}
		
		//CONFIGURATION
		
		//Fields to GET
		$get['fields'] = array(
				'enrollments.id',
				'enrollments.studid',
				'enrollments.name' ,
				'enrollments.user_id' ,
				'years.year',
				'courses.course' 
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "courses",
				"on"	=> "courses.id = enrollments.course_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "enrollments.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."search/list_students/".$x;
		$config["suffix"] = $suffix;
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("enrollments", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 4;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->post('submit') == "Print")
		{
			$get['all'] = true;
		}
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $results = $this->M_core_model->get_record("enrollments", $get);
		$this->view_data['links'] = $this->pagination->create_links();
	}
	
	public function deleted_accounts()
	{
		$this->menu_access_checker();
		$this->session_checker->check_if_alive();
		if($this->input->post('search_deleted_user'))
		{
			$lastname = $this->input->post('lastname',TRUE);
			$firstname = $this->input->post('firstname',TRUE);
			$idno = $this->input->post('idno',TRUE);
			$semester_id_eq = $this->input->post('semester_id_eq',TRUE);
			$sy_from_eq = $this->input->post('sy_from_eq',TRUE);
			$sy_to_eq = $this->input->post('sy_to_eq',TRUE);
			$is_paid_eq = $this->input->post('is_paid_eq',TRUE);
			
			$this->view_data['search'] = $this->M_enrollments->search_deleted_accounts($firstname, $lastname, $idno, $semester_id_eq, $sy_from_eq, $sy_to_eq);
		}
	}
	
	public function destroy_enrollment($id = false)
	{
		$jquery = $this->input->post('jquery');
		if($jquery == false)
		{
			if($this->M_enrollments->destroy_enrollment($id))
			{
				log_message('info','Destroy Enrollment By: '.$this->user. '; Id: '.$id.';');
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Student Deleted</div>');
				redirect(current_url());
			}else
			{
				log_message('error','Destroy Enrollment By: '.$this->user. 'FAILED  on dean/destroy_enrollment; Id: '.$id.';');
				$this->view_data['system_message'] = '<div class="alert alert-danger">An error was encountered while processing your request</div>';
			}
		}else{
			if($this->M_enrollments->destroy_enrollment($id))
			{
				log_message('info','Destroy Enrollment By: '.$this->user. '; Id: '.$id.';');
				echo 'true';
				die();//stops php from sending the views so that browser will only receive true
			}else
			{
				log_message('error','Destroy Enrollment By: '.$this->user. 'FAILED on dean/destroy_enrollment; Id: '.$id.';');
				echo 'false';
				die();//stops php from sending the views so that browser will only receive false
			}
		}
	}
	
	public function restore_enrollment($id = false)
	{
		$jquery = $this->input->post('jquery');
		if($jquery == false)
		{
			if($this->M_enrollments->restore_enrollment($id))
			{	
				log_message('info','Restore Enrollment By: '.$this->user. '; Id: '.$id.';');
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Student Restored</div>');
				redirect(current_url());
			}else
			{
				log_message('info','Restore Enrollment By: '.$this->user. 'Failed on dean/restore_enrollment; Id: '.$id.';');
				$this->view_data['system_message'] = '<div class="alert alert-danger">An error was encountered while processing your request</div>';
			}
		}else{
			if($this->M_enrollments->restore_enrollment($id))
			{
				log_message('info','Restore Enrollment By: '.$this->user. '; Id: '.$id.';');
				echo 'true';
				die();//stops php from sending the views so that browser will only receive true
			}else
			{
				log_message('error','Restore Enrollment By: '.$this->user. 'FAILED  on dean/restore_enrollment; Id: '.$id.';');
				echo 'false';
				die();//stops php from sending the views so that browser will only receive false
			}
		}
	}
	
	public function list_paid_students($page=0)
	{
		$this->load->model('M_search');
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."search/list_paid_students";
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_search->count_paid_students();
		$config["per_page"] = 100;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		// $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		// vp($page, true);
		$this->pagination->initialize($config);
		$this->view_data['search_results'] = $this->M_search->get_paid_students($config["per_page"], $page);
		$this->view_data['links'] = $this->pagination->create_links();
	}

}
