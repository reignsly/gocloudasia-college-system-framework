<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migrate extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
		
		$this->load->library('migration');
		$this->load->dbforge();
	}

	public function current(){


		if ( ! $this->migration->current())
		{
			show_error($this->migration->error_string());
			
		}else{
			
		}
	}
	
}
