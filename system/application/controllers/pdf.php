<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class Pdf Extends MY_Controller
{
	private $student_name;
	private $student_idno;
	private $student_level;
	
	private $watermark_image;
	private $watermark_text;

	private $my_data;
	private $school_name;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('mpdf'));
		$this->load->model(array('M_settings'));
		$settings = $this->M_settings->get_settings();
		
		$this->my_data['school_name'] = $this->school_name = $settings->school_name;
		$this->my_data['school_address'] = $settings->school_address;
		$this->my_data['school_telephone'] = $settings->school_telephone;
		$this->my_data['report_name'] = "";
	}

	private function reload_school_settings()
	{
		$settings = $this->M_settings->get_settings();
		
		$this->my_data['school_name'] = $settings->school_name;
		$this->my_data['school_address'] = $settings->school_address;
		$this->my_data['school_telephone'] = $settings->school_telephone;
		$this->my_data['report_name'] = "";
	}
	
	public function index()
	{
		show_404();
	}
	
	// class mPDF ([ string $mode [, mixed $format [, float $default_font_size [, string $default_font [, float $margin_left , float $margin_right , float $margin_top , float $margin_bottom , float $margin_header , float $margin_footer [, string $orientation ]]]]]])
	private function __create_pdf($html,$filename = FALSE,$footer = FALSE,$header = FALSE,$watermark = FALSE, $dimensions = FALSE, $orientation = "P")
	{
		set_time_limit(0);
		if($filename == FALSE)
		{
			$filename = str_replace(' ','_',trim($this->student_name)).'_'.trim($this->student_idno);
		}
		
		$now = date('F d, Y - h:i:s');
		
		if($dimensions == FALSE)
		{
			$mpdf=new mPDF('c','A4','','' , 10 , 10 , 10 , 10 , 5 , 5);
		}else{
			$s = $dimensions;

			if(is_array($s)){
				$size = isset($s[6]) ? [$s[6][0],$s[6][1]] :'A4';
				$mpdf=new mPDF('c',$size,'','' , $s[0] , $s[1] , $s[2] , $s[3] , $s[4] , $s[5], $orientation);
			}else{
				$size = $s;
				$mpdf=new mPDF('c',$size,'','');
			}
		}
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
			if($footer !== FALSE)
			{
				if(is_bool($footer) === TRUE)
				{
					$mpdf->SetFooter('Page {PAGENO} of {nbpg} -  Generated At '.$now);
				}else{
					$mpdf->SetFooter($footer);
				}
			}
			
			if($header !== FALSE)
			{
				if(is_bool($header) === TRUE)
				{
					$mpdf->SetHeader($this->school_name);
				}else{
					$mpdf->SetHeader($header);
				}
			}
			
			if($watermark !== FALSE)
			{
				if($watermark == 'image')
				{
					$text = $this->watermark_image;
					$mpdf->SetWatermarkImage(school_logo(),0.2);
					$mpdf->showWatermarkImage = true;
				
				}elseif($watermark == 'text')
				{
					$text = $this->watermark_text;
					$mpdf->SetWatermarkText('DRAFT');
					$mpdf->showWatermarkText = true;
					$mpdf->watermarkImageAlpha = 0.1;
				}
			}
			
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->WriteHTML($html);
		$mpdf->Output($filename,'I');
	}

	public function student_ledger($id = false)
	{
		$this->check_access_studentprofile_menu();
		
		set_time_limit(0);
		
		$this->my_data['report_name'] = "Student Ledger";
		#load student library
		$this->load->library('_Student',array('enrollment_id'=>$id));
		$this->my_data['_student'] = $this->_student;
		$this->my_data['student_ledger'] = $ledger = $this->_student->ledger();

		$html = $this->load->view('_pdf/student_ledger',$this->my_data, true );
		// echo $html;
		// exit(0);
		$this->__create_pdf($html,false, false, true, false,[10,10,10,10,15,15,[215.9,279.4]], "P");
		exit(0);
	}

	/** Student Transcript
	 * @param string $enrollment_id Enrollment id
	 */
	public function transcript($enrollment_id)
	{
		$this->check_student_profile_access('otr');
		$this->check_access_studentprofile_menu();

		$this->load->model(array("M_transcript","M_grades_file"));

		#load student library
		set_time_limit(0);
		$this->load->library('_student',array('enrollment_id'=>$enrollment_id));
		$this->my_data['_student'] = $this->_student; if($this->_student->valid === false){ show_404(); }
		$this->my_data['transcript'] = $tr = $this->M_transcript->get_transcript($this->_student->profile->studid);
		$this->my_data['get_final_grade'] = function($eid,$ss_id){ return $this->M_grades_file->get_final_grade($eid,$ss_id); };
		$this->my_data['enrollment_id'] = $enrollment_id;
		
		$html = $this->load->view('_pdf/transcript',$this->my_data, true );
		// echo $html;
		// exit(0);
		$this->__create_pdf($html,false, true, false, false,[10,10,10,10,15,15,[215.9,279.4]], "P");
		exit(0);
	}

	/** Print Grading , from teacher account my_subjects controller - method grading
	 * @param $data $this->view_data
	 */
	public function grading($data)
	{
		$this->load->helper('grade_helper');
		$this->my_data['record'] = $data;
		$html = $this->load->view('_pdf/grading',$this->my_data, true);
		// echo $html;
		// exit(0);
		$this->__create_pdf($html,false, true, false, false,[10,10,10,10,15,15,[215.9,279.4]], "P");
		exit(0);
	}

	public function subject_grade($data)
	{
		$this->load->helper('grade_helper');
		$this->my_data['record'] = $data;
		$html = $this->load->view('_pdf/subject_grade',$this->my_data, true);
		// echo $html;
		// exit(0);
		$this->__create_pdf($html,false, true, false, false,[10,10,10,10,15,15,[215.9,279.4]], "P");
		exit(0);
	}


	/***** LIBRARY REPORTS *****/

	public function media_type_master($id = false)
	{
		$this->menu_access_checker('library/media_masterlist');

		$this->load->model('M_library_books');

		set_time_limit(0);
		$this->my_data['type'] = $t = $this->M_library_books->get_master_list_by_type($id);
		
		$html = $this->load->view('library/pdf_media_type_master',$this->my_data,TRUE);
		// exit(0);
		$this->__create_pdf($html);
		exit(0);
	}

	public function media_category_master($id = false)
	{
		$this->menu_access_checker('library/media_masterlist');

		$this->load->model('M_library_books');

		set_time_limit(0);
		$this->my_data['type'] = $t = $this->M_library_books->get_master_list_by_category($id);
		
		$html = $this->load->view('library/pdf_media_category_master',$this->my_data,TRUE);
		// exit(0);
		$this->__create_pdf($html);
		exit(0);
	}

	public function all_library_media()
	{
		$this->menu_access_checker('library/media_masterlist');
		
		$this->load->model('M_library_books');

		set_time_limit(0);

		unset($get);
		$get['order'] = 'book_name';
		$get['fields'] = array(
				'library_books.*',
				'library_book_category.lbc_name as category',
				'book_copies - book_borrowed as available',
				'media_types.media_type',
		);

		$get['join'][] = array(
				'table' => 'media_types',
				'on' => 'library_books.media_type_id = media_types.id',
				'type' => 'left'
			);

		$get['join'][] = array(
				'table' => 'library_book_category',
				'on' => 'library_books.book_category = library_book_category.id',
				'type' => 'left'
			);
		
		$this->my_data['search'] = $this->M_library_books->get_record(false, $get);
		
		$html = $this->load->view('library/pdf_all_library_media',$this->my_data,TRUE);
		
		$this->__create_pdf($html);
		exit(0);
	}

	public function circulation_report()
	{
		$this->menu_access_checker('library/circulation_report');

		if(!$_POST){
			redirect('library/circulation_report');
		}
		set_time_limit(0);
		$this->load->model(array(
				'M_media_types',
				'M_library_books',
				'M_library_book_category',
				'M_employees'
			));
		$this->load->model('M_lib_circulationfile1','m_lib_c1');
		$this->load->model('M_lib_circulationfile2','m_lib_c2');

		//Load dropdowns
		$this->view_data['media_types'] = $this->M_media_types->get_for_dd(array('id','media_type'), false, 'All Types','media_type');
		$this->view_data['book_category'] = $this->M_library_book_category->get_for_dd(array('id','lbc_name'), false, 'All Categories','lbc_name');

		$tab = $_POST ? $this->input->post('tab') : 'date';

		/** GENERATE REPORT BY BORROW DATE **/
		if($this->input->post('print_by_date')){
			
			$trndte = $this->input->post('trndte');

			$this->my_data = $rs = $this->m_lib_c2->circulation_report_by_date($trndte);
			
			$this->my_data['report_type'] = 'trndte';
			$this->my_data['orientation'] = $o = $trndte['orientation'];
		}

		/** GENERATE REPORT BY RETURN DATE **/
		if($this->input->post('print_by_ret_date')){
			
			$retdte = $this->input->post('retdte');

			$this->my_data = $rs = $this->m_lib_c2->circulation_report_by_retdte($retdte);

			$this->my_data['report_type'] = 'retdte';
			$this->my_data['orientation'] = $o = $retdte['orientation'];
		}

		/** GENERATE REPORT BY BORROWER **/
		if($this->input->post('print_by_borrower')){
			
			$borrower = $this->input->post('borrower');

			$this->my_data = $rs = $this->m_lib_c2->circulation_report_by_borrower($borrower);

			$this->my_data['report_type'] = 'borrower';
			$this->my_data['orientation'] = $o = $borrower['orientation'];
			$this->my_data['usertype'] = $borrower['usertype'];

		}

		/** GENERATE REPORT BY MEDIA **/
		if($this->input->post('print_by_media')){
			
			$media = $this->input->post('media');

			$this->my_data = $rs = $this->m_lib_c2->circulation_report_by_media($media);

			$this->my_data['report_type'] = 'media';
			$this->my_data['orientation'] = $o = $media['orientation'];

		}

		$this->reload_school_settings();

		$html = $this->load->view('library/pdf_circulation_report',$this->my_data,TRUE);
		// echo $html;
		// exit(0);
		$this->__create_pdf($html,false, true, true, false,[10,10,20,20,15,15,[215.9,279.4]], $o);
		exit(0);
	}

	/***** END LIBRARY REPORTS *****/
}