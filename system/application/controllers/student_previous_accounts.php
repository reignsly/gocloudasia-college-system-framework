<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Student_previous_accounts extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		
		$this->load->model(array(
			'M_student_grades',
			'M_enrollments'
		));
		
		$this->menu_access_checker(array(
			'search/search_student',
			'enrollees/daily_enrollees',
			'search/master_list_by_course',
			'search/search_enrollee',
			'search/list_students/paid',
			'search/list_students/unpaid',
			'search/list_students/fullpaid',
			'search/list_students/partialpaid',
		));
	}
	
	// Create
	public function create($enrollment_id)
	{
		show_404();
		
		$this->load->model(array('M_student_previous_accounts'));
		
		$this->view_data['academic_year'] = FALSE;
		$this->view_data['enrollment_id'] = $enrollment_id;
		
		if($_POST)
		{
			$data = $this->input->post('student_previous_account');
			$data['enrollment_id'] = $enrollment_id;
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			
			$result = $this->M_student_previous_accounts->create($data);
			
			if($result['status'])
			{
				log_message('error','Previous Account Created by: '.$this->user.'Success; Previous Account Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Academic Year successfully added.</div>');
				redirect('student_previous_accounts/index/'. $enrollment_id);
			}
		}
	}
	
	// Retrieve
	public function index($enrollment_id)
	{
		$this->load->model(array('M_student_previous_accounts'));
		$this->view_data["enrollment_id"] = $enrollment_id;
		$this->view_data['student_previous_accounts'] = $this->M_student_previous_accounts->find_all($enrollment_id);
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
	}
	
	public function display($id)
	{
		$this->load->model(array('M_student_previous_accounts'));
		
		$this->view_data['student_previous_accounts'] = $this->M_student_previous_accounts->get($id);
	}
	
	// Update
	public function edit($id)
	{
		$this->load->model(array('M_student_previous_accounts'));
		
		$this->view_data['student_previous_accounts'] = $this->M_student_previous_accounts->get_student_previous_accounts($id);
		
		if($_POST)
		{
			$data = $this->input->post('student_previous_accounts');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_student_previous_accounts->update($data, $id);
			
			if($result['status'])
			{
				log_message('error','Previous Account Updated by: '.$this->user.'Success; Previous Account Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Academic Year successfully updated.</div>');
				redirect('student_previous_accounts');
			}
		}
	}
	
	// Update
	public function destroy($id = false, $enrollment_id = false)
	{
		if($id == false) { show_404(); }
		if($enrollment_id == false) { show_404(); }
		
		$this->load->model(array('M_student_previous_accounts'));
		
		$result = $this->M_student_previous_accounts->delete($id);
		log_message('error','Previous Account Deleted by: '.$this->user.'Success; Previous Account Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-success">Previous Account successfully deleted.</div>');
		redirect('student_previous_accounts/index/'.$enrollment_id);
	}
	
	public function view_previous_balance($id = false, $enrollment_id = false)
	{
		if($id == false) { show_404(); }
		if($enrollment_id == false) { show_404(); }
		
		$this->load->model(array('M_enrollments','M_student_subjects'));
		
		$this->load->helper('my_dropdown');
		
		$this->view_data['form_token'] = $this->token->get_token();
		$this->view_data['employeid'] = $enrollment_id;
		
		$this->view_data['student_profile'] = $this->view_data['student'] = $p = $this->M_enrollments->profile($enrollment_id); // student profile
		$this->view_data['subjects']	 = $subjects =  $this->M_student_subjects->get_studentsubjects($enrollment_id);
		
		$subject_units = 0;
		$subject_lab = 0;
		$subject_lec = 0;
		$has_nstp = 0;
		
		if($subjects){
			foreach($subjects as $obj){
				$subject_units += $obj->units;
				$subject_lab += $obj->lab;
				$subject_lec += $obj->lec;
				if($obj->is_nstp == 1)
				{
					$has_nstp++;
				}	
			}
		}
		
		$this->view_data['subject_units']['subject_units'] = $subject_units;
		$this->view_data['subject_units']['total_lab'] = $subject_lab;
		$this->view_data['subject_units']['total_lec'] = $subject_lec;
		$this->view_data['total_lec'] = $subject_lec;
		$this->view_data['total_lab'] = $subject_lab;
		$this->view_data['has_nstp'] = $has_nstp;
		
		// Count Issues
	    $this->issue_count($p->user_id);
		$this->view_data['course'] = $p->course_id;
		$this->view_data['year'] = $p->year_id;
		$this->view_data['semester'] = $p->sem_id;
		$this->view_data['enrollment_id'] = $p->id;
			
		// Ajax Subject Deletion
		$this->view_data['eid'] = $p->id;
		$this->view_data['y'] = $p->year_id;
		$this->view_data['c'] = $p->course_id;
		$this->view_data['s'] = $p->semester_id;
	}
	
	public function grade($id = false)
	{
		if($id == false) { show_404(); }
		
		$this->view_data['student'] = $p = $this->M_enrollments->profile($id);
		$this->view_data['student_grades'] = $this->M_student_grades->get_student_grades($id);
		  
		// Count Issues
	    $this->issue_count($p->user_id);
	}
	
	public function view_fees($enrollment_id = false)
	{
			if($enrollment_id == false){
				
				show_404();
			}
			
			$this->load->model(array('M_enrollments','M_student_finances','M_student_fees', 'M_student_previous_accounts', 'M_student_subjects', 'M_student_deductions','M_coursefees','M_assign_coursefees','M_studentpayments', 'M_additional_charges','M_student_total_file',
			'M_subjects'));
			
			$this->view_data['system_message'] = $this->session->flashdata('system_message');
			
#			Top header data of student
			$this->view_data['student'] = $p = $this->M_enrollments->profile($enrollment_id);
			$this->view_data['enrollment_id'] = $enrollment_id;
			
			$this->issue_count($p->user_id);
			
			//GET STUDENT FINANCE
			$this->view_data['student_finance'] = $studentfinances = $this->M_student_finances->get_student_finance($enrollment_id);
			
			//GET STUDENT TOTALS
			$this->view_data['student_total'] = $student_total = $this->M_student_total_file->get_student_total_file($enrollment_id);
			
			if($studentfinances)
			{
				//GET STUDENT FEES
				$stud_fees = $this->M_student_fees->get_student_fees($studentfinances->id);
			}
			
			//GET SUBJECTS
			$this->view_data['subjects'] = $subjects = $this->M_student_subjects->get_studentsubjects($enrollment_id);
			//LOOP SUBJECTS TO GET TOTAL NUMBER OF UNIT LAB AND LEC
			$subjects_units = 0;
			$total_lec = 0;
			$total_lab = 0;
			$has_nstp = 0;
			
			if($subjects)
			{
				foreach($subjects as $subject)
				{				
					$subjects_units += $subject->units;
					$total_lec += $subject->lec;
					$total_lab += $subject->lab;
					if($subject->is_nstp == 1)
					{
						$has_nstp++;
					}	
				}				
			}
			
			$this->view_data['subject_units']['subject_units'] = $subjects_units;
			$this->view_data['subject_units']['total_lab'] = $total_lec;
			$this->view_data['subject_units']['total_lec'] = $total_lab;
			$this->view_data['total_lec'] = $total_lec;
			$this->view_data['total_lab'] = $total_lab;
			$this->view_data['has_nstp'] = $has_nstp;
			
			//ARRANGE STUDENT FEES
			
			if($stud_fees)
			{
				$student_fees = array();
				
				foreach($stud_fees as $cf)
				{
					if($cf->is_tuition_fee == 1)
					{
						$student_fees['tuition_fee'][0] = $cf;
						// $total_tuition = $subjects_units * $cf->value;
						// $total_amount_due += $total_tuition;
					}
					else if($cf->is_misc == 1)
					{
						$student_fees['misc'][] = $cf;
						// $total_amount_due += $cf->value;
					}
					else if($cf->is_other == 1)
					{
						$student_fees['other'][] = $cf;
						// $total_amount_due += $cf->value;
					}
					else if($cf->is_nstp == 1)
					{
						$student_fees['nstp'][] = $cf;
						// $total_amount_due += $cf->value;
					}
					else{}
				}
				$this->view_data['student_fees'] = $student_fees;
			}

			$this->view_data['course'] = $p->course_id; 
			$this->view_data['year'] = $p->year_id;
			$this->view_data['semester'] = $p->sem_id;
			$this->view_data['enrollment_id'] = $p->id;
			
			
			//Ajax Subject
			$this->view_data['eid'] = $p->id;
			$this->view_data['y'] = $p->year_id;
			$this->view_data['c'] = $p->course_id;
			$this->view_data['s'] = $p->semester_id;
	}
}
