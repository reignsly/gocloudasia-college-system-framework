<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Classes Extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->menu_access_checker();
	}
	
	public function index()
	{
		// vp($this->session);
	}
	
	public function classroom()
	{
		$id = $this->session->userdata['userid'];
	  $this->load->model(array("M_assignsubjects", "M_employees", "M_users"));
	  
	  $user = $this->M_users->get_login_by_id($id);
		$employee = $this->M_employees->get_employee_by_login($user->login);
		
	  $this->view_data['assignsubjects'] = $this->M_assignsubjects->get_employee_assignsubjects($employee->id);
	  
	}
	
	public function class_list($subject_id)
	{
	  $this->load->model(array("M_student_subjects", "M_subjects"));
	  $this->view_data["subject"] = $this->M_subjects->get($subject_id);
	  $this->view_data["students"] = $this->M_student_subjects->get_class_list_by_subject($subject_id, $this->open_semester->year_from, $this->open_semester->year_to, $this->open_semester->id);
	}
	
	public function class_grades($subject_id)
	{
	  $this->load->model(array("M_student_grades"));
	  $this->view_data["student_grades"] = $this->M_student_grades->get_student_grade_subjects($subject_id);
	}
	
	public function lolwut()
	{
	
	}






}
