<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gopanel_auth extends Backend_Controller
{
	private $username = "gocloudasia";
	private $password = "booda1214";

	public function __construct()
	{
		parent::__construct();
		$this->layout_view = 'application_blank';
		// vd($this->session->userdata);
		if($this->session->userdata('gopanel_logged_in') == TRUE AND strtolower($this->uri->segment(2)) !== 'logout')
		{
			redirect('gopanel');
		}
		$this->load->library(array('form_validation','token'));
		$this->load->helper(array('url','form'));
		$this->token->set_token();
		session_start();
	}

	public function index()
	{
		redirect('backend');
	}
	
	private function _captcha()
	{
		$this->load->helper('captcha');
		$system_counted_hash = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
		$vals = array(
			'word'	 	 => str_replace('0','', substr(md5(rand(1,100).$system_counted_hash),3,rand(2,5))),
			'img_path'	 => 'assets/captcha/',
			'img_url'	 => base_url().'assets/captcha/',
			'font_path'  => 'assets/fonts/4.ttf',
			'img_width'	 => 300,
			'img_height' => 55,
			'expiration' => 7200
			);
		
		$cap = create_captcha($vals);
		$image['word'] = $cap['word'];
		$image['image'] ='<image src="'.base_url().'assets/captcha/'.$cap['time'].'.jpg" class="img-responsive"/>';
		$image['link'] = './captcha/'.$cap['time'].'.jpg';

		return (object)$image;
	}

	public function login()
	{
		$this->load->helper('file');//load helper file
		delete_files('./captcha');// delete all images in captcha folder
		$captcha = $this->_captcha(); //generate captcha
		$this->session->set_flashdata('image_url',$captcha->link); // save path of generated captcha to session
		$this->view_data['form_token'] = $this->token->get_token();//generate form token
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'username', 'required|trim|htmlspecialchars');
		$this->form_validation->set_rules('password', 'password', 'required|trim|htmlspecialchars');
		$this->form_validation->set_rules('fit', ' ', 'required|trim|htmlspecialchars');
		$this->form_validation->set_rules('captcha', 'captcha', 'required|trim|htmlspecialchars');
		
		$this->view_data['captcha_image'] = $captcha->image;
		$this->view_data['username'] = $username = isset($_SESSION['username']) ? $_SESSION['username'] : NULL;

		if($this->input->post('login_me'))
		{
			//get captcha link from session
			$captcha_image_link = $this->session->flashdata('image_url');
	
			// check if XSF or XXS filtering
			if($this->token->validate_token($this->input->post('fit',TRUE)))
			{
				//run form validation
				if($this->form_validation->run() !== FALSE)
				{
					//hash the word
					$captcha_word = md5(strtolower($this->session->userdata('word')));
					$sent_captcha_text = md5(strtolower($this->input->post('captcha')));
					
					if($captcha_word == $sent_captcha_text)
					{
						if($this->_verify_user($this->input->post()))
						{
							$this->token->destroy_token();//destroy token
							$this->session->set_userdata('word',''); //destroy captcha from session
							$msg = "<strong>Congratulations!</strong> You have successfully logged in";
							$this->_msg('s',$msg,'gopanel'); // redirect success
						}else
						{
							$this->token->destroy_token();//destroy token
							$this->session->set_userdata('word','');//destroy captcha from session
							// unlink($captcha_image_link);//delete captcha image from file
							$msg = "<strong>Incorrect Credentials!</strong> Please try again";
							$this->_msg('e', $msg, 'backend');
						}
					}else{
						$this->token->destroy_token();
						$this->session->set_userdata('word','');
						// unlink($captcha_image_link);
						$this->_msg('e','<strong>Wrong</strong> Captcha Code entered.','backend');
					}
				}
			}else{
				$this->token->destroy_token();
				$this->session->set_userdata('word','');
				// unlink($captcha_image_link);
				$this->_msg('e','Invalid username/password combination.','backend');
			}
		}else{
			$this->session->set_userdata('word',$captcha->word);
		}
	}

	private function _verify_user($post)
	{
		$p = (object)$post;

		if($this->username === $p->username AND $this->password === $p->password){

			$userdata = array(
				'system_username'  => $this->username,
				'gopanel_logged_in' => TRUE,
			);

			$this->session->set_userdata($userdata);

			return TRUE;
		}
		
		return FALSE;
	}
	
  function logout()
  {
		$message = $this->_msg();
		
		$userdata = array(
			'system_username'  => "",
			'gopanel_logged_in' => FALSE,
		);
		
		$this->session->set_userdata($userdata);

		if($message)
		{
			$this->_msg('s',$message,'backend');
		}else{
			$this->_msg('n','You have successfully <strong>logged out.</strong>','backend');
		}
  }
}