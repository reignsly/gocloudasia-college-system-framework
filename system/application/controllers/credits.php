<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Credits extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
	}
	
	public function create($id)
	{
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_credits'));
		
		$this->view_data['credit'] = FALSE;
		$this->view_data['credit_id'] = $id;
		
		if($_POST)
		{
			$data['subject'] = $_POST['subject'];
			$data['value'] = $_POST['value'];
			$data['summary_of_credit_id'] = $_POST['summary_of_credit_id'];
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_credits->create_credit($data);
			if($result['status'])
			{
				redirect('credits/show/'.$_POST['summary_of_credit_id']);
			}
		}
	}
	
	public function show($id)
	{
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_credits'));
		
		$this->view_data['credits'] = $this->M_credits->get($id);
		$this->view_data['credit_id'] = $id;
	}
	
	public function edit($id)
	{
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_credits'));
		
		$this->view_data['credit'] = $this->M_credits->get_credits($id);
		
		if($_POST)
		{
			if($_POST)
			{
				$data['subject'] = $_POST['subject'];
				$data['value'] = $_POST['value'];
				$summary_of_credit_id = $_POST['summary_of_credit_id'];
				$data['updated_at'] = date('Y-m-d H:i:s');
				$id = $_POST['id'];
				
				$result = $this->M_credits->update_credit($data,$id);
				if($result['status'])
				{
					redirect('credits/show/'.$summary_of_credit_id);
				}
			}
		}
	}
	
	public function destroy($id,$from)
	{
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_credits'));
		
		$result = $this->M_credits->delete_credits($id);
		redirect('credits/show/'.$from);
	}
}