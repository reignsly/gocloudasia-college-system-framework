<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_creator extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('M_menus','M_departments','M_users','M_main_menus'));
		$this->view_data['system_message'] = $this->_msg();
	}

	public function index(){ redirect('menu_creator/main_menus');}

	public function main_menus()
	{
		$this->view_data['system_message'] = $this->_msg();
		//LOAD CRUD LIBRARY
		unset($config);
		$config['title'] = 'Main Menus';
		$config['type'] = 'query';
		$config['sql'] = "SELECT id,controller,caption,menu_grp,menu_num,remarks FROM main_menus WHERE visible = 1";
		$config['table'] = "main_menus";
		$config['order'] = "id desc";
		$config['disable_system_message'] = true;
		$config['additional_button'] = array(
				'0' => array(
						'url' => site_url('menu_creator/clone_menu'),
						'title' => 'Clone Menu',
						'class' => 'btn btn-default btn-sm',
					),
			);
		// $config['add_action_button'] = array(
		// 		'0' => array(
		// 				'controller' => 'menu_creator',
		// 				'method' => 'add_to_department',
		// 				'caption' => 'Add to Department',
		// 				'field' => 'id',
		// 				'icon_class' => 'fa fa-tags'
		// 			),
		// 	);
		$config['pager'] = array(
			"per_page" => 10,
			"num_links" => 3,
			"uri_segment" => 3
		);
		$config['search'] = true;

		$config['crud'] = array(
			0 => array(					
					'field' => 'controller',
					'type'	=> 'text',
					'label' => 'Controller',
					'rules' => 'required|is_unique[main_menus.controller]',
					'help'  => 'Name of the Controller to be used.',
					'attr'  => array(
						'name'        	=> 'controller',
						'id'          	=> 'controller',
						'placeHolder'   => 'Required',
						'maxlength'   	=> '100',
						'required'		=> null
						)
				),
			1 => array(					
					'field' => 'caption',
					'type'	=> 'text',
					'label' => 'Caption',
					'rules' => 'required',
					'attr'  => array(
						'name'        	=> 'caption',
						'id'          	=> 'caption',
						'placeHolder'   => 'Required',
						'maxlength'   	=> '100',
						'required'		=> null
						)
				),
			2 => array(					
					'field' => 'menu_grp',
					'type'	=> 'text',
					'label' => 'Menu Group',
					'rules' => 'required',
					'attr'  => array(
						'name'        	=> 'menu_grp',
						'id'          	=> 'menu_grp',
						'placeHolder'   => 'Required',
						'maxlength'   	=> '100',
						'required'		=> null
						)
				),
			3 => array(					
					'field' => 'menu_num',
					'type'	=> 'text',
					'label' => 'Menu Number',
					'rules' => 'required|numeric',
					'attr'  => array(
						'name'        	=> 'menu_num',
						'id'          	=> 'menu_num',
						'placeHolder'   => 'Required',
						'maxlength'   	=> '100',
						'class'   		=> 'numeric',
						'required'		=> null
						)
				),
			4 => array(					
					'field' => 'menu_lvl',
					'type'	=> 'select',
					'label' => 'Menu Level',
					'rules' => 'required',
					'attr'  => array(
						'name'        	=> 'menu_lvl',
						'id'          	=> 'menu_lvl',
						'placeHolder'   => 'Required',
						'maxlength'   	=> '100',
						'required'		=> null
						),
					'select_option'		=> array(
						2				=> 'Second Level'
						)
				),
			5 => array(					
					'field' => 'remarks',
					'type'	=> 'text',
					'label' => 'Remarks',
					'attr'  => array(
						'name'        	=> 'remarks',
						'id'          	=> 'caption',
						'placeHolder'   => ''
						)
				),
			6 => array(					
					'field' => 'visible',
					'type'	=> 'select',
					'label' => 'Is Active',
					'rules' => 'required',
					'attr'  => array(
						'name'        	=> 'visible',
						'id'          	=> 'visible',
						'required'		=> null
						),
					'select_option'		=> array(
						1               => 'Yes',
						0				=> 'No'
						)
				),
			);

		$this->load->library('_sylvercrud',$config);

		$this->view_data['data_record'] = $this->_sylvercrud->_html;
	}

	public function clone_menu($id = false)
	{
		unset($get);
		$get['fields'] = 'id, controller, caption';
		$get['order'] = "caption";
		$this->view_data['main_menus'] = $mm = $this->M_main_menus->get_where_dd(false, $get, 'Select Source', array('id','caption'));
		$this->view_data['selected'] = $s = $this->M_main_menus->pull($id,"id,controller,caption,menu_grp, menu_num, remarks");
		
		//if select source submitted
		if($this->input->post('select_source')){
			redirect('menu_creator/clone_menu/'.$this->input->post('selected_id'));
		}

		if($this->input->post('clone_menu')){
			$clone = $this->input->post('clone');
			unset($s->id);
			if(($clone != (array)$s)){
				$clone['menu_lvl'] = 2;
				$clone['menu_icon'] = "entypo-globe";
				$rs_add = (object)$this->M_main_menus->insert($clone);
				if($rs_add->status){
					activity_log('Clone Menu',$this->userlogin, "Source Main_menu_id : ".$id." Added Main Menu ID : ".$rs_add->id);
					$this->_msg('s','Main menu was successfully added', 'menu_creator/main_menus');
				}
			}else{
				$this->_msg('s','Cloning unsuccessful, please try again', current_url());
			}
		}
	}

	public function index_old()
	{
		show_404();
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$this->layout_view = 'menu_creator/index';
		
		$this->view_data['departments'] = $department = $this->M_menus->get_menus_group_by_department();
		
		$user_menus = false;
		
		if($department){
			$headers = false;
			foreach($department as $obj){ //LOOP DEPARTMENT
				$header_menu = $this->M_menus->get_header_menus_by_department($obj->department);
				if($header_menu){
					foreach($header_menu as $head){ //LOOP MENU HEADER : LEVEL 1
						
						$headers[$obj->department][] = $head;
						
						$sub_menu = $this->M_menus->get_sub_menus_by_menu_sub($obj->department,$head->menu_sub);
						// vp($sub_menu);
						if($sub_menu){
							foreach($sub_menu as $key => $xmenus){ // LOOB SUB MENU : LEVEL 2
								$user_menus[$obj->department][$head->menu_sub][] = $xmenus;
								// vd($user_menus);
							}
						}
					}
				}
			}
			$this->view_data['headers'] = $headers;
			
		}
		
		$this->view_data['level2_menus'] = $user_menus;
		
		// vp($this->session->userdata);
	}
	
	public function create_header_menu($dep = false)
	{
		if($dep == false) { show_404(); }
		$this->layout_view = 'menu_creator/create_header_menu';
		
		$this->view_data['department'] = $dep;
		
		if($_POST)
		{
			$data = $this->input->post('menu');
			// $data['created_at'] = NOW;
			// $data['updated_at'] = NOW;
			$data['department'] = $dep;
			$data['controller'] = '#';
			$data['menu_lvl'] = 1;
			
			$rs = $this->M_menus->create_menus($data);
			
			if($rs['status']):
			
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">Header menu successfully added.</div>');
			redirect(current_url());
			
			endif;
		}
	}
	
	public function create_sub_menu($dep = false, $menu_sub = false, $caption = '')
	{
		if(!$dep) { show_404(); }
		if(!$menu_sub) { show_404(); }
		
		$this->layout_view = 'menu_creator/create_sub_menu';
		
		$this->view_data['department'] = $dep;
		$this->view_data['caption'] = $caption;
		
		if($_POST)
		{
			$data = $this->input->post('menu');
			// $data['created_at'] = NOW;
			// $data['updated_at'] = NOW;
			$data['department'] = $dep;
			$data['menu_grp'] = $menu_sub;
			$data['menu_lvl'] = 2;
			
			$rs = $this->M_menus->create_menus($data);
			
			if($rs['status']):
			
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">Header menu successfully added.</div>');
			redirect(current_url());
			
			endif;
		}
	}
	
	public function create_menu_by_html(){
		
		//I USED THIS TO SAVE USER MENUS FROM LIST OF A TAG
		
		$this->layout_view = 'menu_creator/create_menu_by_html';
	}
	
	public function show_menu($id = false)
	{
		if(!$id) { show_404(); }
		
		$data['visible'] = 1;
		
		$rs = $this->M_menus->update_menus($data, $id);
		
		if($rs)
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">Sub-Menu successfully showed.</div>');
			redirect(base_url('menu_creator'));
		}
	}
	
	public function hide_menu($id = false)
	{
		if(!$id) { show_404(); }
		
		$data['visible'] = 0;
		
		$rs = $this->M_menus->update_menus($data, $id);
		
		if($rs)
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">Sub-Menu successfully hide.</div>');
			redirect(base_url('menu_creator'));
		}
	}
	
	public function delete($id = false)
	{
		if(!$id) { show_404(); }
		
		$rs = $this->M_menus->delete_menus($id);
		
		if($rs)
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">Sub-Menu successfully deleted.</div>');
			redirect(base_url('menu_creator'));
		}
	}
	
	public function delete_header($dep = false, $menu_grp= false)
	{
		if(!$dep) { show_404(); }
		if(!$menu_grp) { show_404(); }
		
		$rs = $this->M_menus->delete_menus_by_menu_grp($dep, $menu_grp);
		
		if($rs)
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">Header Menu and its sub-menu successfully deleted.</div>');
			redirect(base_url('menu_creator'));
		}
	}
	
	public function create()
	{
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->view_data['departments'] = $dep = $this->M_departments->get_all_department_for_welcome_screen(1);
		$this->layout_view = 'menu_creator/create';
		
		if($_POST){
			$this->load->model(array('M_menus'));
			
			
			$department = $this->input->post('department');
				
			if(isset($department) && count($department) > 0){
				
				foreach($department as $dep)
				{
					$data = $this->input->post('menu');
					$data['department'] = $dep;
			
					$rs = $this->M_menus->create_menus($data);
			
					if($rs['status']){
						
					}
				}
				
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Menus successfully added.</div>');
				redirect(menu_creator);
			}else{
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">No department selected.</div>');
				redirect(menu_creator);
			}
		}
	}

	/**
	 * Add menu to department and package department
	 * @param int $id Main Menu ID
	 */
	public function add_to_department($id)
	{
		$this->view_data['menu'] = $m = $this->M_main_menus->pull($id);
		$this->view_data['departments'] = $d = $this->M_departments->get_all_department_for_welcome_screen(1);
		// vd($m);
		if($m){


		}else{ show_404(); }
	}
}
