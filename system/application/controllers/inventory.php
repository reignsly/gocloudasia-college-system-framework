<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventory extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		// $this->session_checker->secure_page('all');
		$this->menu_access_checker();
	}
	
	public function index($page = 0)
	{
		$this->load->model(array('M_products','M_items'));
		$this->load->helper(array('my_url'));
		
		//PAGINATION
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		$order_by = 'items.id ASC';
		
		if($_POST){
			if($this->input->post('submit') == "Search"){
				$this->view_data['fields'] = $field = $this->input->post('fields');
				$this->view_data['keyword'] = $keyword = $this->input->post('keyword');
				$this->view_data['status'] = $status = $this->input->post('status');
				if(trim($keyword) != ""){
					$filter[$field.' LIKE '] = "%".trim($keyword)."%";
				}
				if($status != ""){
					$filter['status = '] = strtoupper($status);
				}
				$order_by = $field.' ASC';
				$page = 0;
			}
		}
		
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."inventory/index";
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_items->fetch_record(0,0,$filter, $order_by,true, true);
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$this->view_data['items'] = $items = $this->M_items->fetch_record($page, $config["per_page"], $filter, $order_by);
		$this->view_data['links'] = $this->pagination->create_links();
		
	}
	
	// Create
	public function create()
	{
		$this->load->model(array('M_products','M_items'));
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($_POST)
		{
			if($this->input->post('submit') == "Save Changes")
			{
				$data = $this->input->post('item');
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['updated_at'] = date('Y-m-d H:i:s');
				$data['unit_left'] = $data['unit'];
				$data['status'] = 'STABLE';
				
				$result = $this->M_items->create_items($data);
				
				if($result['status'])
				{
					$id = $result['id'];
					log_message('success','Item Created by: '.$this->user.'Success; Items Id: '.$id);
					$this->session->set_flashdata('system_message', '<div class="alert alert-success">Items successfully added.</div>');
					redirect(current_url());
				}
			}
		}
	}
	
	// Update
	public function edit($id=false)
	{
		if($id == false) {show_404();}
		
		$this->load->model(array('M_products','M_items'));
		
		$this->view_data['items'] = $this->M_items->get_items($id);
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($_POST)
		{
			
			if($this->input->post('submit') == "Save Changes")
			{
				$data = $this->input->post('item');
				$data['updated_at'] = date('Y-m-d H:i:s');
				
				$result = $this->M_items->update_items($data, $id);
				
				if($result['status'])
				{
					log_message('success','Item Updated by: '.$this->user.'Success; Items Id: '.$id);
					$this->session->set_flashdata('system_message', '<div class="alert alert-success">Items successfully updated.</div>');
					redirect(current_url());
				}
			}
		}
	}
	
	public function destroy($id = false){
		if($id == false){ show_404(); }
	
		$this->load->model(array('M_items'));
		
		$result = $this->M_items->delete_items($id);
	
		if($result['status']){
			log_message('error','Items Deleted by: '.$this->user.'Success; Item Id: '.$id);
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">Items successfully deleted.</div>');
			redirect(base_url().'inventory');
		}		
	}
}