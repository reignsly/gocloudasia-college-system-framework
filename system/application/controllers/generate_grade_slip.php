<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Generate_grade_slip extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->menu_access_checker();
		$this->load->helper('my_dropdown');
	}

	public function index($page = 0, $tab = "all")
	{
		$this->session_checker->check_if_alive();
		$this->load->helper('url_encrypt');
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->load->model('M_core_model', 'm');
		$this->load->model('M_student_subjects', 's');
		$this->view_data['tab'] = $tab;
		
		if($_POST)
		{
			$this->view_data['type'] = $type = $this->input->post('type');
			$this->view_data['year_id'] = $year_id = $this->input->post('year_id');
			$this->view_data['semester_id'] = $semester_id = $this->input->post('semester_id');
			$this->view_data['course_id'] = $course_id = $this->input->post('course_id');
			$this->view_data['per_page'] = $per_page = $this->input->post('per_page');
			
			$fields = array(
				
				'enrollments.id',
				'enrollments.studid',
				'enrollments.name',
				'enrollments.status',
				'years.year',
				'enrollments.year_id',
				'enrollments.semester_id',
				'courses.course',
				'enrollments.course_id',
				'enrollments.sy_from',
				'enrollments.sy_to',
				'semesters.name as semester',
				'student_grade_files.id as student_grade_file_id'
				
			);
			
			//FILTERS
			$where['enrollments.is_paid'] = 1;
			$where['enrollments.is_deleted'] = 0;
			$where['enrollments.sy_from'] = $this->cos->user->year_from;
			$where['enrollments.sy_to'] = $this->cos->user->year_to;

			if($year_id != "")
			{
				$where['enrollments.year_id'] = $year_id;
			}
			
			if($semester_id != "")
			{
				$where['enrollments.semester_id'] = $semester_id;
			}
			
			if($course_id != "")
			{
				$where['enrollments.course_id'] = $course_id;
			}
			
			
			//CONFIGURATION
			$config['fields'] = $fields;
			$config['where'] = $where;
			$config['join'][] = array(
				"table" => "courses",
				"on"	=> "courses.id = enrollments.course_id",
				"type"  => "LEFT"
			);
			$config['join'][] = array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			);
			$config['join'][] = array(
				"table" => "semesters",
				"on"	=> "semesters.id = enrollments.semester_id",
				"type"  => "LEFT"
			);
			$config['join'][] = array(
				"table" => "student_grade_files",
				"on"	=> "student_grade_files.user_id = enrollments.id",
				"type"  => "LEFT"
			);
			$config['order'] = "enrollments.name ASC";
			
			$config['all'] = true;
			
			$this->view_data['students'] = $rs = $this->m->get_record("enrollments", $config);			
			
			if($rs)
			{
				$this->load->library('_Student',array('enrollment_id'=>0));
				foreach($rs as $student)
				{
					$this->_student->enrollment_id = $student->id;
        	$this->_student->load_default();
					$this->_student->load_profile();
					$this->_student->load_subjects();
					
					// $this->view_data['studentsubjects'][$student->id] = $g = $this->_student->get_student_subject_grades(false);
					$this->view_data['studentsubjects'][$student->id] = $g = $this->_student->student_subjects;
				}
			
				if($type == 'pdf')
				{
					$this->generate_pdf($this->view_data);
				}
				else if($type == 'excel')
				{
					$this->generate_excel($this->view_data);
				}
				else
				{
					show_404();
				}
			}
			else
			{
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">No record found.</div>');
			}
		}

		$filter = false;
		$like = false;
		$order_by = false;
		
		$filter['enrollments.semester_id'] = $this->cos->user->semester_id;
		$filter['enrollments.sy_from'] = $this->cos->user->year_from;
		$filter['enrollments.sy_to'] = $this->cos->user->year_to;
		$filter['enrollments.is_deleted'] = 0;
		$filter['enrollments.is_drop'] = 0;
		$filter['enrollments.is_paid'] = 1;
		
		$arr_filters = array();
		$suffix = "";
		
		if($_GET)
		{
			if(isset($_GET['submit']) && $_GET['submit'] == "Search")
			{
				$page = 0;
			}
			
			if(isset($_GET['lastname']) && trim($_GET['lastname']) != ''){
				$this->view_data['lastname'] = $lastname = trim($_GET['lastname']);
				$like['enrollments.lastname'] = $lastname;
				$arr_filters['lastname'] = $lastname;
			}
			
			if(isset($_GET['fname']) && trim($_GET['fname']) != ''){
				$this->view_data['fname'] = $fname = trim($_GET['fname']);
				$like['enrollments.fname'] = $fname;
				$arr_filters['fname'] = $fname;
				
			}
			
			if(isset($_GET['studid']) && trim($_GET['studid']) != ''){
				$this->view_data['studid'] = $studid = trim($_GET['studid']);
				$like['enrollments.studid'] = $studid;
				$arr_filters['studid'] = $studid;
			}
			
			if(isset($_GET['year_id']) && trim($_GET['year_id']) != ''){
				$this->view_data['year_id'] = $year_id = trim($_GET['year_id']);
				$filter['enrollments.year_id'] = $year_id;
				$arr_filters['year_id'] = $year_id;
			}
			
			$suffix = array_to_geturl($arr_filters);
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'enrollments.id',
				'enrollments.studid',
				'enrollments.name' ,
				'years.year',
				'courses.course',
				'enrollments.user_id',
				'enrollments.date_of_birth as dob',
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "courses",
				"on"	=> "courses.id = enrollments.course_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "enrollments.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."generate_grade_slip/index/0/perstudent";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->m->get_record("enrollments", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		// $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->m->get_record("enrollments", $get);
		$this->view_data['links'] = $this->pagination->create_links();
		
	}
	
	public function generate_grade_slip_per_student($id = false, $excel = false)
	{
		if($id == false) { show_404(); }
		$this->load->model('M_core_model', 'm');
		$this->load->model('M_student_subjects', 's');	
		
		$fields = array(
				
				'enrollments.id',
				'enrollments.studid',
				'enrollments.name',
				'enrollments.status',
				'years.year',
				'enrollments.year_id',
				'enrollments.semester_id',
				'courses.course',
				'enrollments.course_id',
				'enrollments.sy_from',
				'enrollments.sy_to',
				'semesters.name as semester',
				'student_grade_files.id as student_grade_file_id'
				
			);
		
		//FILTERS
		$where['enrollments.id'] = $id;
		$where['enrollments.is_paid'] = 1;
		$where['enrollments.is_deleted'] = 0;
		
		//CONFIGURATION
		$config['fields'] = $fields;
		$config['where'] = $where;
		$config['join'][] = array(
			"table" => "courses",
			"on"	=> "courses.id = enrollments.course_id",
			"type"  => "LEFT"
		);
		$config['join'][] = array(
			"table" => "years",
			"on"	=> "years.id = enrollments.year_id",
			"type"  => "LEFT"
		);
		$config['join'][] = array(
			"table" => "semesters",
			"on"	=> "semesters.id = enrollments.semester_id",
			"type"  => "LEFT"
		);
		$config['join'][] = array(
			"table" => "student_grade_files",
			"on"	=> "student_grade_files.user_id = enrollments.id",
			"type"  => "LEFT"
		);
		$config['order'] = "enrollments.name ASC";
		
		$config['all'] = true;
		
		$this->view_data['students'] = $rs = $this->m->get_record("enrollments", $config);			
		$this->view_data['per_page'] = 1;
		
		if($rs)
		{
			$this->load->library('_Student',array('enrollment_id'=>0));
			foreach($rs as $student)
			{
				$this->_student->enrollment_id = $student->id;
      	$this->_student->load_default();
				$this->_student->load_profile();
				$this->_student->load_subjects();
				
				$this->view_data['studentsubjects'][$student->id] = $this->_student->student_subjects;
			}
			
			if($excel === false)
			{
				$this->generate_pdf($this->view_data);
			}
			else
			{
				$this->generate_excel($this->view_data);
			}
		}
		else
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-danger">No record found.</div>');
		}
	}
	
	
	public function generate_pdf($data = false)
	{
		if($data == false) { show_404(); }
		
		$this->load->helper('print');
			
		$this->load->library('mpdf');

		$students = $data['students'];
		$studentsubjects = $data['studentsubjects'];
		
		$html .= _css(); //GET CSS
		$html .= _css_grade_slip($data['per_page']); //GET CSS
		
		if($students)
		{
			$html .= "";
			$ctr = 0;
			$per_page = $data['per_page'];
			$xpass = false;
			
			foreach($students as $stud)
			{
				$ctr++;
				
				$student[] = $stud;
				$subject[$stud->id] = $studentsubjects[$stud->id];
				// vd($subject[$stud->id]);
				if($ctr == $per_page)
				{
					if($xpass == false){
						$xpass = true;
					}else{
						$html .= "<pagebreak />"; // next page
					}
					
					if($student){
						$html .= _html_grade_slip($student, $subject, $per_page); //CREATES PER PAGE
						unset($student);
						unset($subject);
						$ctr = 0;
					}
				}
				else{
					if($student){
						$html .= _html_grade_slip($student, $subject, $per_page); //CREATES PER PAGE
						unset($student);
						unset($subject);
						$ctr++;
					}
				}
			}
			// vd($html);
			// exit(0);
			$mpdf=new mPDF('','FOLIO','','',10,10,5,5,0,0); 
			$mpdf->WriteHTML($html);
			$mpdf->Output();
		
		}
		else
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-danger">No record found.</div>');
			redirect('generate_grade_slip');
		}
	}
	
	public function generate_excel($data = false)
	{
		if($data == false) { show_404(); }
		
		$students = $data['students'];
		$studentsubjects = $data['studentsubjects'];
		
		if($students)
		{
			$this->load->library('../controllers/_the_downloadables');
			
			$this->_the_downloadables->_generate_student_gradeslip($data);
		}
		else
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-danger">No record found.</div>');
			redirect('generate_grade_slip');
		}
	}
}