<?php
class Master_subjects Extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->load->helper('my_dropdown');
		$this->load->library(array('form_validation'));

		// load models
		$this->load->model('M_master_subjects','m_ms');
	}
	
	public function index($page = 0)
	{	
		$this->menu_access_checker();
		//PAGINATION
		$this->view_data['system_message'] = $this->session->flashdata('system_message');

		$like = false;
		$filter = false;
		$filter['master_subjects.is_deleted'] = 0;
		
		if($_GET)
		{			
			if(isset($_GET['code']) && trim($_GET['code'])){
				$this->view_data['code'] = $code = trim($_GET['code']);
				$like['master_subjects.code'] = $code;
			}
			
			if(isset($_GET['subject']) && trim($_GET['subject'])){
				$this->view_data['subject'] = $subject = trim($_GET['subject']);
				$like['master_subjects.subject'] = $subject;
			}
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'master_subjects.id',
				'master_subjects.ref_id',
				'master_subjects.code',
				'master_subjects.subject',
				'master_subjects.units',
				'master_subjects.lec',
				'master_subjects.lab',
				'master_subjects.year_to',
				'master_subjects.year_from',
				'master_subjects.ay_id',
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['order'] = "master_subjects.code";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."master_subjects/index";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$this->view_data['get_url'] = $config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("master_subjects", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		// $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['results'] = $search = $this->M_core_model->get_record("master_subjects", $get);
		$this->view_data['links'] = $this->pagination->create_links();
	}
	
	public function create(){
		
		$this->menu_access_checker('master_subjects/index');
		
		$this->load->model(array(
			'M_subjects',
			'M_years',
			'M_courses',
			'M_semesters',
			'M_academic_years',
			'M_coursefinances',
			'M_fees',
			'M_rooms',
			'M_users2'
		));
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($_POST){
			
			if($this->form_validation->run('master_subjects') !== FALSE){
				
				$result = (object)$this->m_ms->save_subject($this->input->post());
			
				if($result->status)
				{
					$this->_msg('s',"Master Subject was successfully created", current_url());			
				}else{
					$this->_msg('e',"Transaction Failed, please try again", current_url());			
				}
			}
		}
	}
	
	public function edit($hash_id = false){
		
		$id = $this->check_hash($hash_id);
		$this->menu_access_checker('master_subjects/index');

		$this->view_data['subjects'] = $s = $this->m_ms->get($id); $this->check_rec($s);
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($_POST){

			if($this->form_validation->run('master_subjects') !== FALSE){
				
				$data = $this->input->post('subjects');
				$data['updated_at'] = NOW;
				$result = $this->m_ms->update($id, $data);
			
				if($result)
				{	
					activity_log('Master Subject Update', false, "Table master_subjects id : $id : Data - ".arr_str($data));
					$this->_msg('s',"Master Subject ".$data['subject']." was successfully updated", current_url());			
				}else{
					$this->_msg('e',"Transaction Failed, please try again", current_url());			
				}
				
			}
			
		}
	}
	
	// Delete
	public function destroy($hash_id = false)
	{
		$id = $this->check_hash($hash_id);
		$this->menu_access_checker('master_subjects/index');
		$subjects = $this->m_ms->get($id); $this->check_rec($subjects);

		$result = $this->m_ms->delete_master_subjects($id,$this->userid);
		if($result){
			$this->_msg('s',"Subject : '$subjects->subject' successfully deleted.", 'master_subjects');			
		}else{
			$this->_msg('e',"Transaction Failed, please try again", 'master_subjects');			
		}
	}
	
	public function add_subject_ajax($subject_id, $y, $c, $s, $eid)
	{
		$this->load->model(array('M_student_subjects', 'M_subjects', 'M_enrollments'));
		$data['enrollmentid'] = $eid;
		$data['subject_id'] = $subject_id;
		$data['year_id'] = $y;
		$data['semester_id'] = $s;
		$data['course_id'] = $c;
		
		//echo json_encode($data);
	}
	
	public function add_subject($y, $c, $s,$eid)
	{
		$this->menu_access_checker(array(
			'subjects',
			'search/search_student',
			'enrollees/daily_enrollees',
			'search/master_list_by_course',
			'search/search_enrollee',
			'search/list_students/paid',
			'search/list_students/unpaid',
			'search/list_students/fullpaid',
			'search/list_students/partialpaid',
		));
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->load->model(array('M_subjects', "M_student_subjects"));
		
		$this->token->set_token();
		$this->view_data['form_token'] = $this->token->get_token();
		$this->view_data['eid'] = $eid;
		$this->view_data['y'] = $y;
		$this->view_data['c'] = $c;
		$this->view_data['s'] = $s;
		
			$this->load->library("pagination");
			$config = $this->pagination_style();
			$config["base_url"] = base_url() ."subjects/add_subject/$y/$c/$s/$eid" ;
			$this->view_data['total_rows'] = $config["total_rows"] = $this->M_subjects->subjects_count($s,$this->open_semester->year_from,$this->open_semester->year_to);
			$config["per_page"] = 50;
			$config['num_links'] = 10;
			$config["uri_segment"] = 7;
			$page = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
			$this->pagination->initialize($config);
			$this->view_data['subject_list'] = $this->M_subjects->subjectlist
			($s,$this->open_semester->year_from,$this->open_semester->year_to , $eid,$config["per_page"], $page);
			$this->view_data['links'] = $this->pagination->create_links();
		

		
		
		$this->view_data['subjects']	 = $subjects=  $this->M_student_subjects->get_studentsubjects($eid);
		$this->view_data['subject_units'] =  $this->M_student_subjects->get_studentsubjects_total($subjects);
	}
	
	public function view_class_list($subject_id)
	{
		$this->menu_access_checker();
	  $this->load->model('m_subjects');
	  $this->load->model('m_enrollments');
	  $this->load->model('M_student_subjects');
	  $this->view_data['subject'] = $this->m_subjects->get($subject_id);
	  $this->view_data['enrollments'] = $e = $this->M_student_subjects->get_subjects_students($subject_id, true);
	  
	}

	public function print_subjects()
	{
		$this->menu_access_checker();
		// inlcude download controller
		$this->load->library('../controllers/_the_downloadables');
		
		$this->_the_downloadables->_print_all_subject();
	}

	public function print_all_in_pdf()
	{
		// $this->menu_access_checker();
		//PAGINATION
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$like = false;
		$filter = false;
		
		if($_GET)
		{
			if(isset($_GET['sc_id']) && trim($_GET['sc_id']) != ""){
				$this->view_data['sc_id'] = $sc_id = trim($_GET['sc_id']);
				$like['subjects.sc_id'] = $sc_id;
			}
			
			if(isset($_GET['code']) && trim($_GET['code'])){
				$this->view_data['code'] = $code = trim($_GET['code']);
				$like['subjects.code'] = $code;
			}
			
			if(isset($_GET['subject']) && trim($_GET['subject'])){
				$this->view_data['subject'] = $subject = trim($_GET['subject']);
				$like['subjects.subject'] = $subject;
			}

			if(isset($_GET['semester_id']) && trim($_GET['semester_id'])){
				$this->view_data['semester_id'] = $semester_id = trim($_GET['semester_id']);
				$filter['subjects.semester_id'] = $semester_id;
			}

			if(isset($_GET['academic_year_id']) && trim($_GET['academic_year_id'])){
				$this->view_data['academic_year_id'] = $academic_year_id = trim($_GET['academic_year_id']);

				$this->load->model('M_academic_years');
				$sy = $this->M_academic_years->get($academic_year_id);
				$filter['subjects.year_from'] = $sy->year_from;
				$filter['subjects.year_to'] = $sy->year_to;
			}
		}else{
			$filter['subjects.year_from'] = $this->cos->user->year_from;
			$filter['subjects.year_to'] = $this->cos->user->year_to;
			$filter['subjects.semester_id'] = $this->cos->user->semester_id;
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'subjects.id',
				'subjects.sc_id',
				'subjects.code',
				'subjects.subject',
				'subjects.units',
				'subjects.lec',
				'subjects.lab',
				'subjects.time',
				'subjects.day',
				'subjects.room_id',
				'subjects.subject_taken',
				'subjects.original_load',
				'subjects.year_to',
				'subjects.year_from',
				'subjects.teacher_user_id',
				'rooms.name AS room',
				'users.name AS instructor' 
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "rooms",
				"on"	=> "rooms.id = subjects.room_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "users",
				"on"	=> "users.id = subjects.teacher_user_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "subjects.subject";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
		
		//FOR PAGINATION
		$get['all'] = true;
		$get['count'] = false;
		
		$this->view_data['results'] = $search = $this->M_core_model->get_record("subjects", $get);
		
		if($search)
		{
			// $this->disable_menus = true;
			// $this->disable_views = true;

			$this->load->helper('print');
			
			$html = _html_subject_list($this->view_data); 
			// echo $html;
			$this->load->library('mpdf');
			
			$mpdf=new mPDF('','FOLIO','','',5,5,5,5,0,0); 

			$mpdf->AddPage('L');
			
			$mpdf->WriteHTML($html);
			
			$mpdf->Output();
		}
		else
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-danger"><span class="glyphicon glyphicon-remove"></span>&nbsp; No record to print.</div>');
			redirect('subjects');
		}
	}

	public function print_pdf_old()
	{
		show_404(); //BACK UP ONLY
		$this->menu_access_checker('subjects');

		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$arr_filters = array();
		$x = array();
		$suffix = "";
		
		if($_GET)
		{
			// if(isset($_GET['sc_id'])){
			// 	$arr_filters['sc_id'] = trim($_GET['sc_id']);
			// }
			
			// if(isset($_GET['code'])){
			// 	$arr_filters['code'] = trim($_GET['code']);
			// }
			
			// if(isset($_GET['subject'])){
			// 	$arr_filters['subject'] = trim($_GET['subject']);
			// }
			
			// $x = ($this->uri->assoc_to_uri($_GET));
			// $suffix = array_to_geturl($arr_filters);
		}		
		
		
		$this->load->model('M_subjects');

		// $page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
		$this->view_data["results"] = $results = $this->M_subjects->search_subjects(0, 0, $arr_filters, false, true);
		

		if($results)
		{
			// $this->disable_menus = true;
			// $this->disable_views = true;

			$this->load->helper('print');
			
			$html = _html_subject_list($this->view_data); 

			$this->load->library('mpdf');
			
			$mpdf=new mPDF('','FOLIO','','',5,5,5,5,0,0); 

			$mpdf->AddPage('L');
			
			$mpdf->WriteHTML($html);
			
			$mpdf->Output();
		}
		else
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-danger"><span class="glyphicon glyphicon-remove"></span>&nbsp; No record to print.</div>');
			redirect('subjects');
		}
	}

	/* For Developers */
	public function set_subjects_refid()
	{
		$this->M_subjects->set_subject_ref_id();
		$this->_msg('s','Subjects Reference I.D. successfully set.', 'subjects');
	}

	/**
	 *Clone Subjects of Previous Semester
	*/
	public function clone_subject_of_prev_semester($current_academic_year_id = false, $prev_academic_year_id = false, $current_semester_id = false, $prev_semester_id = false){

		$current_academic_year_id = 2;
		$prev_academic_year_id = 2;
		$current_semester_id = 2;
		$prev_semester_id = 1;

		$this->M_subjects->cloned_subjects_from_previous_semester($current_academic_year_id, $prev_academic_year_id, $current_semester_id, $prev_semester_id);
	}

	public function upload_csv()
	{
		$this->load->model('M_master_subjects','m_ms');

		$this->view_data['custom_title'] = "Upload via CSV";
		$this->token->set_token();
		$this->view_data['token'] = $this->token->get_token();

		if($this->input->post('upload_csv') && $_FILES['userfile']['name'] ){
			
				if($this->token->validate_token($this->input->post('form_token'))){

					set_time_limit(0);
					$config['upload_path'] = './assets/uploads/school';
					$config['allowed_types'] = 'csv';
					$config['max_size']	= '10000';
					$config['file_name'] = 'master_subject_csv_upload';
					$config['overwrite'] = TRUE;
					$config['width']	 = 188;
					$config['height'] = 187;
					$this->load->library('upload', $config);
					
					if ( ! $this->upload->do_upload()){

						$this->view_data['system_message'] = $this->_msg('e',' '.$this->upload->display_errors());
					}else{
						
						$data = array('upload_data' => $this->upload->data());
						//resize start
						$source_path = $data['upload_data']['full_path'];
						
						$up = $this->m_ms->upload_from_csv($source_path);

						if($up->status){

							$this->_msg('s', $up->msg, current_url());
						}else{
							$this->_msg('e',$up->msg,current_url());
						}	
					}
				}else{
					show_error('Upload form breach!');
				}
		}
	}

	public function download_master_subject_csv_format($hash='')
	{

		$hid = $this->check_hash($hash);
		if($hid === 143){
			$this->load->model('M_master_subjects','m_ms');
			$format = $this->m_ms->_master_subject_format('csv');
			$this->load->helper('csv');
			array_to_csv($format, 'master_subject_format.csv');
		}
	}
}
