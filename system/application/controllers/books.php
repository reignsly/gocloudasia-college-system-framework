<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Books extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->menu_access_checker();
		$this->load->helper('my_dropdown');
	}
	
	// Create
	public function create()
	{
		$this->load->model(array('M_libraries'));
		
		$this->load->helper('my_dropdown');
		
		$this->view_data['libraries'] = FALSE;
		
		if($_POST)
		{
			$data = $this->input->post('library');
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_libraries->create_libraries($data);
			
			if($result['status'])
			{
				activity_log('create book',$this->userlogin,'Book Created by: '.$this->user.'Success; Book Id: '.$result['id']);
				
				log_message('error','Book Created by: '.$this->user.'Success; Book Id: '.$result['id']);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Book successfully added.</div>');
				redirect('books/create');
			}
		}
	}
	
	// Retrieve
	public function index($page=0)
	{
		$this->load->model(array('M_libraries'));
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		
		if($_POST){
			if($this->input->post('submit') == "Search"){
				$this->view_data['fields'] = $field = $this->input->post('fields');
				$this->view_data['keyword'] = $keyword = $this->input->post('keyword');
				if(trim($keyword) != ""){
					$filter[$field.' LIKE '] = "%".trim($keyword)."%";
				}
			}
			
			$page = 0;
		}
		
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."books/index";
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_libraries->find_all(0,0,$filter, true, true);
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$this->view_data['libraries'] = $this->M_libraries->find_all($page, $config["per_page"], $filter);
		$this->view_data['links'] = $this->pagination->create_links();
	}
	
	public function create_library_card($id = false)
	{
		if($id == false){ show_404(); }
		
		$this->load->model(array('M_libraries', 'M_librarycards','M_borrowers'));
		
		$this->load->helper('my_dropdown');
		
		$this->view_data['book'] = $this->M_libraries->get_libraries($id);
		
		if($_POST){
			$data = $this->input->post('library');
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			$data['library_id'] = $id;
			// vp($data);
			$result = $this->M_librarycards->create_librarycards($data);
			
			if($result['status'])
			{
				activity_log('create Library Card',$this->userlogin,'Library Card Created by: '.$this->user.'Success; Library Card Id: '.$result['id']);
				
				log_message('error','Library Card Created by: '.$this->user.'Success; Library Card Id: '.$result['id']);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Library Card successfully added.</div>');
				// redirect(base_url().'books/show_library_card/'.$id.'/'.$result['id']);
				redirect(current_url());
			}
		}
	}
	
	public function show_library_card($id = false){
		
		if($id == false){ show_404(); }
		// if($library_id == false){ show_404(); }
		
		$this->load->model(array('M_libraries', 'M_librarycards','M_borrowers'));
		$this->load->helper('my_url');
		
		$this->view_data['book'] = $this->M_libraries->get_libraries($id);
		// vd($this->view_data['book']);
		$this->view_data['id'] = $id;
		
		$this->view_data['librarycard'] = $librarycard = $this->M_librarycards->get_librarycards_bookid($id);
	}
	
	// Update
	public function edit($id = false)
	{
		if($id == false){ show_404(); }
		
		$this->load->model(array('M_libraries'));
		
		$this->view_data['libraries'] = $this->M_libraries->get_libraries($id);
		
		if($_POST)
		{
			$data = $this->input->post('library');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_libraries->update_libraries($data, $id);
			
			if($result['status'])
			{
				activity_log('update book',$this->userlogin,'Book Updated by: '.$this->user.'Success; Book Id: '.$id);
				
				log_message('error','Book Updated by: '.$this->user.'Success; Book Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Book successfully updated.</div>');
				redirect(current_url());
			}
		}
	}
	
	// Update
	public function destroy($id = false)
	{
		if($id == false){ show_404(); }
		$this->load->model(array('M_libraries'));
		
		$result = $this->M_libraries->delete_libraries($id);
		
		activity_log('delete book',$this->userlogin,'Book Deleted by: '.$this->user.'Success; Book Id: '.$id);
		
		log_message('error','Book Deleted by: '.$this->user.'Success; Book Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-success">Book successfully deleted.</div>');
		redirect('books');
	}
	
	public function view_borrowers($id = false, $library_id = false){
		
		if($id == false){ show_404(); }
		if($library_id == false){ show_404(); }
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$this->load->model(array('M_libraries', 'M_librarycards','M_borrowers'));
		$this->load->helper('my_url');
		
		$this->view_data['book'] = $this->M_libraries->get_libraries($id);
		
		$this->view_data['librarycard'] = $this->M_librarycards->get_librarycards($library_id);
		
		$this->view_data['id'] = $id;
		
		$this->view_data['library_id'] = $library_id;
		
		$this->view_data['borrowers'] = $this->M_borrowers->get_borrowers_by_libcard_id($library_id);
		
	}
	
	public function destroy_borrower($bookid = false, $library_id =false, $id = false){
		if($id == false){ show_404(); }
		if($library_id == false){ show_404(); }
		if($bookid == false){ show_404(); }
		$this->load->model(array('M_borrowers','M_libraries'));
		
		$result = $this->M_borrowers->delete_borrowers($id);
	
		if($result['status']){
			//UPDATE BOOK TO UNAVAILABLE
			unset($data);
			$data['updated_at'] = NOW;
			$data['status'] = 'AVAILABLE';
			$u_rs = $this->M_libraries->update_libraries($data, $bookid);
			
			activity_log('delete borrower',$this->userlogin,'Borrower Deleted by: '.$this->user.'Success; Borrower Id: '.$id);
			
			log_message('error','Borrower Deleted by: '.$this->user.'Success; Borrower Id: '.$id);
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">Borrower successfully deleted.</div>');
			redirect(base_url().'books/view_borrowers/'.$bookid.'/'.$library_id);
		}
	}
	
	public function create_borrower($bookid = false, $library_id = false){
	
		if($library_id == false){ show_404(); }
		if($bookid == false){ show_404(); }
		
		$this->load->model(array('M_libraries', 'M_librarycards','M_borrowers'));
		
		// unset($data);
		// $data['updated_at'] = NOW;
		// $data['status'] = 'AVAILABLE';
		// $u_rs = $this->M_libraries->update_libraries($data, $bookid);
		
		$this->view_data['id'] = $bookid;
		
		$this->view_data['book'] = $this->M_libraries->get_libraries($bookid);
		
		if($_POST && $this->input->post('submit_borrower') == "Save Changes")
		{
		
			//DOUBLE CHECK IF BOOK AVAILABLE
			if($this->M_libraries->check_book_if_available($bookid))
			{
			
				$data = $this->input->post('borrower');
				$data['created_at'] = NOW;
				$data['updated_at'] = NOW;
				$data['updated_at'] = NOW;
				$data['librarycard_id'] = $library_id;
				
				//SAVE BORROWERS
				$rs = $this->M_borrowers->create_borrowers($data);
				
				//IF SAVING SUCCESSFULL
				if($rs['status'])
				{
					
					activity_log('added borrower',$this->userlogin,'Borrower Added by: '.$this->user.'Success; Borrower Id: '.$rs['id']);
					
					log_message('error','Borrower Added by: '.$this->user.'Success; Borrower Id: '.$rs['id']);
					
					//UPDATE BOOK TO UNAVAILABLE
					unset($data);
					$data['updated_at'] = NOW;
					$data['status'] = 'BORROWED';
					$u_rs = $this->M_libraries->update_libraries($data, $bookid);
					
					$this->session->set_flashdata('system_message', '<div class="alert alert-success">Book was successfully borrowed.</div>');
					redirect(base_url().'books/view_borrowers/'.$bookid.'/'.$library_id);
				}
			}
			else
			{
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">This book was already unavailable</div>');
				redirect(base_url().'books/view_borrowers/'.$bookid.'/'.$library_id);
			}
		}
		
	}
	
	public function edit_borrower($bookid = false, $library_id = false, $id = false)
	{
		if($id == false){ show_404(); }
		if($library_id == false){ show_404(); }
		if($bookid == false){ show_404(); }
		
		$this->load->model(array('M_libraries', 'M_librarycards','M_borrowers'));
		
		$this->view_data['book'] = $this->M_libraries->get_libraries($bookid);
		
		$this->view_data['librarycard'] = $this->M_librarycards->get_librarycards($library_id);
		
		$this->view_data['borrower'] = $this->M_borrowers->get_borrowers($id);
		// vp($this->view_data['borrower']);
		$this->view_data['id'] = $id;
		
		$this->view_data['library_id'] = $library_id;
		
		if($_POST && $this->input->post('submit_borrower') == "Save Changes")
		{
			$data = $this->input->post('borrower');
			$data['updated_at'] = NOW;
			
			//SAVE BORROWERS
			$rs = $this->M_borrowers->update_borrowers($data, $id);
			
			//IF SAVING SUCCESSFULL
			if($rs['status'])
			{
				activity_log('updated borrower',$this->userlogin,'Borrower Updated by: '.$this->user.'Success; Borrower Id: '.$id);
				
				log_message('success','Borrower Updated by: '.$this->user.'Success; Borrower Id: '.$id);
				
				//UPDATE BOOK TO UNAVAILABLE

				$data2['updated_at'] = NOW;
				$data2['status'] =  trim($data['date_return']) != '' ? 'AVAILABLE' : 'BORROWED';
				$u_rs = $this->M_libraries->update_libraries($data2, $bookid);
				
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Book was successfully updated.</div>');
				redirect(base_url().'books/view_borrowers/'.$bookid.'/'.$library_id);
			}
		}
	}
}