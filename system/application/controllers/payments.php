<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payments extends MY_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','form','url_encrypt'));
		$this->load->library(array('form_validation'));
		$this->load->model(array('M_settings','M_enrollments','M_payment_records','M_student_total_file','M_grading_periods','M_studentpayments_details','M_student_refund'));
		
		$this->menu_access_checker(array(
			'search/search_student',
			'enrollees/daily_enrollees',
			'search/master_list_by_course',
			'search/search_enrollee',
			'search/list_students/paid',
			'search/list_students/unpaid',
			'search/list_students/fullpaid',
			'search/list_students/partialpaid',
			'payments/create',
		));
		
		$this->session_checker->open_semester();
		$this->session_checker->check_if_alive();
		$this->check_grading_period_if_set();
	}
	
	public function index(){
		
	}
	
	public function check_grading_period_if_set()
	{
		$this->load->model(array('M_grading_periods'));
		
		$rs = $this->M_grading_periods->get_current_grading_period();
		
		if($rs)
		{}
		else{
			$this->session->set_flashdata('system_message' ,'<div class="alert alert-warning">No current grading period was set. Please contact your school admin.</div>');					
			redirect($this->session->userdata['userType']);
		}
	}
	
	public function create($enrollment_id = false){
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->token->set_token();
		$this->view_data['form_token'] = $this->token->get_token();
		
		$this->form_validation->set_message('is_unique', 'The %s Entered is already in Use');
		
		if($this->form_validation->run('record_payment2') == true)
		{	
			$form_token = $this->input->post('form_token');
			
			if($this->token->validate_token($form_token) == TRUE)
			{	
				$input['receipt_number'] 	= $this->input->post('receipt_number',TRUE);
				$input['group_or_no'] 		= $this->input->post('group_or_no',TRUE);
				$input['date_of_payment'] 	= $this->input->post('date_of_payment',TRUE);
				$input['amount'] = $amount 	= $this->input->post('amount',TRUE);
				$input['old_account'] 		= $old_account = $this->input->post('old_account',TRUE);
				$input['other'] = $other 	= $this->input->post('other',TRUE);
				$total_amount 				= floatval($amount) + floatval($old_account) + floatval($other);
				$input['remarks'] 			= $this->input->post('remarks',TRUE);
				
				$enrollment_id = $this->input->post('enrollment_id');
				
				//DOUBLE CHECK IF enrollment_id exist
				if(isset($enrollment_id) && $enrollment_id != ""){}
				else{
					$this->session->set_flashdata('system_message' ,'<div class="alert alert-warning">Something went wrong. Please try re-enter the student number.</div>');
					redirect(current_url());
				}
				
				//ADD TO STUDENT PAYMENT
				$result = $this->M_payment_records->add_payment_record($input,$enrollment_id);
				
				if($result['status'] == 'true')
				{
					//DEDUCT TOTAL AMOUNT TO STUDENT TOTAL BALANCE
					$this->M_student_total_file->deduct_student_payment($enrollment_id, $total_amount);
					
					//UPDATE ENROLLMENT IF PAID, FULL , PARTIAL AND UPDATE SUBJECT LOADS
					$this->M_enrollments->update_enrollments_status($enrollment_id); 
					
					//UPDATE STUDENT PAYMENT DETAILS
					$this->update_studentpayment_details($enrollment_id, $total_amount);
					
					activity_log('add',$this->user,'add payment record by: '.$this->user.' OR# '.$input['receipt_number'].'; Amount: '.$input['amount'].'; From student enrollment id: '.$enrollment_id);
					
					log_message('info','add payment record by: '.$this->user.' OR# '.$input['receipt_number'].'; Amount: '.$input['amount'].'; From student enrollment id: '.$enrollment_id );
					$this->session->set_flashdata('system_message' ,'<div class="alert alert-success">'.$result['log'].'</div>');					
					$this->token->destroy_token();
					redirect(current_url());
				}else{
					log_message('error',$result['log']);
					$this->view_data['system_message'] ='<div class="alert alert-danger">'.$result['log'].'</div>';					
				}
				
				$this->update_enrollment_status($enrollment_id); //UPDATE ENROLLMENT IF PAID, FULL , PARTIAL
				
			}else{
				$this->view_data['system_message'] ='<div class="alert alert-danger">Invalid FOrm Submit</div>';		
			}
		}
	}
	
	public function update_studentpayment_details($enrollment_id = false, $total_amount = 0)
	{
		if($enrollment_id == false) { return false; }
		if($total_amount <= 0) { return false; }
		
		//TO APPLY THE PAYMENT TO UNPAID PAYMENT DETAILS
		
		$this->load->model(array('M_enrollments','M_studentpayments_details','M_student_total_file','M_grading_periods'));
		
		$payment_details = $this->M_studentpayments_details->get_record_by_enrollment_id($enrollment_id);
		
		$current_period = $this->M_grading_periods->get_current_grading_period();
		
		if($payment_details){
			
			foreach($payment_details as $obj){				
				
				if($total_amount > 0)
				{
					if($obj->is_paid == 0){ //IF NOT YET PAID APPLY THE PAYMENT
						$balance = $obj->balance - $total_amount;
						$balance = $balance <= 0 ? 0 : $balance; //BALANCE is 0 when paid all or excess
						$amount_paid = $obj->amount - $balance;
						$is_paid = $balance <= 0 ? 1 : 0;
						
						unset($for_payment);
						$for_payment['updated_at'] = NOW;
						$for_payment['date_applied'] = NOW;
						$for_payment['balance'] = $balance;
						$for_payment['is_paid'] = $is_paid;
						$for_payment['amount_paid'] = $amount_paid;
						
						//UPDATE PAYMENT DETAILS
						$rs = $this->M_studentpayments_details->update_record_by_id($for_payment, $obj->id);
						
						//IF THE AMOUNT PAID HAS EXCESS LOOP AGAIN UNTIL BALANCE WILL BE 0
						$total_amount = $total_amount - $obj->balance; 
					}
				}
			}
			
			
			
			//UPDATE CURRENT PAYMENT DETAILS : FOR NEXT PAYMENT DETAILS
			$payment_details = $this->M_studentpayments_details->get_record_by_enrollment_id($enrollment_id);
			
			$this->M_studentpayments_details->reset_current();
			
			foreach($payment_details as $obj)
			{
				//GET FIRST DETAIL WITH BALANCE
				if(floatval($obj->amount_paid) <= 0 && $obj->is_promisory == 0){
					$upd['is_current'] = 1;
					$this->M_studentpayments_details->update_record_by_id($upd, $obj->id);
					break;
				}
			}
		}
	}
	
	public function add($enrollment_id = false)
	{
		if($enrollment_id == false) { show_404(); }
		
		if($enrollment_id !== false AND ctype_digit($enrollment_id))
		{	
			$this->view_data['system_message'] = $this->session->flashdata('system_message');
			$this->session_checker->check_if_alive();
			$this->token->set_token();
			$this->view_data['form_token'] = $this->token->get_token();
			$this->view_data['enrollment_id'] = $enrollment_id;
			
			$this->view_data['profile'] = $rs = $this->M_enrollments->profile($enrollment_id);
		
			$this->view_data['student_total'] = false;
			$this->view_data['payment_details'] = false;
			$this->view_data['current_period'] = $this->M_grading_periods->get_current_grading_period();
			
			//GET PAYMENT DETAILS / PAYMENT DISTRIBUTION
			$this->view_data['student_total'] = $student_total = $this->M_student_total_file->get_student_total_file($enrollment_id);
			$this->view_data['payment_details'] = $payment_details = $this->M_studentpayments_details->get_record_by_enrollment_id($enrollment_id);
		
			$this->form_validation->set_message('is_unique', 'The %s Entered is already in Use');
		
			if($this->form_validation->run('record_payment2') == true)
			{	
				$form_token = $this->input->post('form_token');
				
				if($this->token->validate_token($form_token) == TRUE)
				{	
				
					$input['receipt_number'] 	= $this->input->post('receipt_number',TRUE);
					$input['group_or_no'] 		= $this->input->post('group_or_no',TRUE);
					$input['date_of_payment'] 	= $this->input->post('date_of_payment',TRUE);
					$input['amount'] = $amount 	= $this->input->post('amount',TRUE);
					$input['old_account'] 		= $old_account = $this->input->post('old_account',TRUE);
					$input['other'] = $other 	= $this->input->post('other',TRUE);
					$total_amount 				= floatval($amount) + floatval($old_account) + floatval($other);
					$input['remarks'] 			= $this->input->post('remarks',TRUE);
					
					$enrollment_id = $this->input->post('enrollment_id');
					
					//DOUBLE CHECK IF enrollment_id exist
					if(isset($enrollment_id) && $enrollment_id != ""){}
					else{
						$this->session->set_flashdata('system_message' ,'<div class="alert alert-warning">Something went wrong. Please try re-enter the student number.</div>');
						redirect(current_url());
					}
					
					//ADD TO STUDENT PAYMENT
					$result = $this->M_payment_records->add_payment_record($input,$enrollment_id);
					
					if($result['status'])
					{
						//DEDUCT TOTAL AMOUNT TO STUDENT TOTAL BALANCE
						$this->M_student_total_file->deduct_student_payment($enrollment_id, $total_amount);
						
						//UPDATE ENROLLMENT IF PAID, FULL , PARTIAL AND UPDATE SUBJECT LOADS
						$this->M_enrollments->update_enrollments_status($enrollment_id); 
						
						//UPDATE STUDENT PAYMENT DETAILS
						$this->update_studentpayment_details($enrollment_id, $total_amount);
						
						activity_log('add',$this->userlogin,'add payment record by: '.$this->user.' OR# '.$input['receipt_number'].'; Amount: '.$input['amount'].'; From student enrollment id: '.$enrollment_id);
						
						log_message('info','add payment record by: '.$this->user.' OR# '.$input['receipt_number'].'; Amount: '.$input['amount'].'; From student enrollment id: '.$enrollment_id );
						$this->session->set_flashdata('system_message' ,'<div class="alert alert-success">'.$result['log'].'</div>');					
						$this->token->destroy_token();
						
						
					}else{
						log_message('error',$result['log']);
						$this->view_data['system_message'] ='<div class="alert alert-danger">'.$result['log'].'</div>';					
					}
					
					redirect(current_url());
					
				}else{
					$this->view_data['system_message'] ='<div class="alert alert-danger">Invalid FOrm Submit</div>';		
				}
			}
		}
	}
	
	public function add_deduction($enrollment_id = false)
	{
		if($enrollment_id !== false AND ctype_digit($enrollment_id))
		{
			$this->view_data['system_message'] = $this->session->flashdata('system_message');
			$this->session_checker->check_if_alive();
			$this->view_data['enrollment_id'] = $enrollment_id;
			$this->token->set_token();
			$this->view_data['form_token'] = $this->token->get_token();
			
			$this->form_validation->set_rules('date','Date','required');
			$this->form_validation->set_rules('amount','Amount','required');
			if($this->form_validation->run() !== FALSE)
			{
				$form_token = $this->input->post('form_token');
				if($this->token->validate_token($form_token) == TRUE)
				{
					$input['amount'] =  $total_amount = $this->input->post('amount',TRUE);
					$input['remarks'] = $this->input->post('remarks',TRUE);
					$input['enrollment_id'] = $this->input->post('enrollment_id',TRUE);
					$result = $this->M_payment_records->add_deduction($input,$enrollment_id);
					if($result['status'] == 'true')
					{
						activity_log('add deductions',$this->userlogin,'add deductions: '.$this->user.'; Amount: '.$input['amount'].'; To student enrollment id: '.$input['enrollment_id']);
						
						log_message('info','add deductions: '.$this->user.'; Amount: '.$input['amount'].'; To student enrollment id: '.$input['enrollment_id']);
						$this->session->set_flashdata('system_message' ,'<div class="alert alert-success">'.$result['log'].'</div>');

						//DEDUCT TOTAL AMOUNT TO STUDENT TOTAL BALANCE
						$this->M_student_total_file->deduct_student_payment($enrollment_id, $total_amount, "DEDUCTION");
						
						//RECALCULATE STUDENT PAYMENT DETAILS
						$this->M_studentpayments_details->recalculate_studentpayment_details($enrollment_id);
						
						//UPDATE ENROLLMENT IF PAID, FULL , PARTIAL AND UPDATE SUBJECT LOADS
						$this->M_enrollments->update_enrollments_status($enrollment_id); 
						
						$this->token->destroy_token();
						redirect(current_url());
						
					}else
					{
						log_message('error',$result['log']);
						$this->view_data['system_message'] ='<div class="alert alert-danger">'.$result['log'].'</div>';					
					}
				}
				else
				{
						log_message('error','Invalid FOrm Submit');
						$this->view_data['system_message'] ='<div class="alert alert-danger">Invalid FOrm Submit</div>';
				}
			}
		}else
		{
			show_404();
		}
	}
	
	
	public function add_deduction_old($enrollment_id = false)
	{
		show_404(); //BACK UP
		
		if($enrollment_id !== false AND ctype_digit($enrollment_id))
		{
			$this->view_data['system_message'] = $this->session->flashdata('system_message');
			$this->session_checker->check_if_alive();
			$this->view_data['enrollment_id'] = $enrollment_id;
			$this->token->set_token();
			$this->view_data['form_token'] = $this->token->get_token();
			
			$this->form_validation->set_rules('date','Date','required');
			$this->form_validation->set_rules('amount','Amount','required');
			if($this->form_validation->run() !== FALSE)
			{
				$form_token = $this->input->post('form_token');
				if($this->token->validate_token($form_token) == TRUE)
				{
					$input['amount'] = $this->input->post('amount',TRUE);
					$input['remarks'] = $this->input->post('remarks',TRUE);
					$input['enrollment_id'] = $this->input->post('enrollment_id',TRUE);
					$result = $this->M_payment_records->add_deduction($input,$enrollment_id);
					if($result['status'] == 'true')
					{
					  $this->update_enrollment_status($enrollment_id);
						log_message('info','add deductions: '.$this->user.'; Amount: '.$input['amount'].'; To student enrollment id: '.$input['enrollment_id']);
						$this->session->set_flashdata('system_message' ,'<div class="alert alert-success">'.$result['log'].'</div>');					
						$this->token->destroy_token();
						redirect(current_url());
						
					}else
					{
						log_message('error',$result['log']);
						$this->view_data['system_message'] ='<div class="alert alert-danger">'.$result['log'].'</div>';					
					}
				}else
				{
						log_message('error',$result['log']);
						$this->view_data['system_message'] ='<div class="alert alert-danger">'.$result['log'].'</div>';
				}
			}
		}else
		{
			show_404();
		}
	}
	
	public function get_enrollment_total_fees($enrollment_id = false)
	{
			if($enrollment_id == false){
				
				return false;
			}
			
			$this->load->model(array('M_enrollments','M_student_finances','M_student_fees', 'M_student_previous_accounts', 'M_student_subjects', 'M_student_deductions','M_coursefees','M_assign_coursefees','M_studentpayments', 'M_additional_charges'));
			
#			Top header data of student
			$p = $this->M_enrollments->profile($enrollment_id);
			$this->view_data['enrollment_id'] = $enrollment_id;
			$this->view_data['is_paid'] = $p->is_paid;
			
			// Count Issues
			$this->issue_count($p->user_id);
			
#			Used for layouts/student_data
			$subjects =  $this->M_student_subjects->get_studentsubjects($enrollment_id);
			$subject_units = $this->M_student_subjects->get_studentsubjects_total($subjects);
		
			$this->view_data['student_profile'] = $this->view_data['student'] = $p;
			
			$student_finance =  $this->M_student_finances->get_student_finance($enrollment_id);
			
			$this->view_data['student_finance'] = $student_finance;
			
			if(!empty($student_finance))
			{
				// for total payment variables
				$totalprevious_account = 0.00;
				
				//Get course Finance
				$enrollee = $p;
				$data[] = $enrollee->year_id;
				$data[] = $enrollee->course_id;
				$data[] = $enrollee->sem_id;
				$data[] = $this->open_semester->academic_year_id;
				$assigned_course_fee = $this->M_assign_coursefees->get_assign_course_fee($data);
				if($assigned_course_fee){
					$studentfinance['coursefinance_id'] = $assigned_course_fee->coursefinance_id;
					$studentfinance['student_id']  = $enrollee->user_id;
					$studentfinance['enrollmentid']  = $enrollment_id;
					$studentfinance['year_id'] = $enrollee->year_id;
					$studentfinance['semester_id'] = $enrollee->sem_id;
					$studentfinance['course_id'] = $enrollee->course_id;
			
					$this->M_student_finances->add_student_finance($studentfinance);
				}
				
				$nstp_exist = 0;
				$lab_exist = 0;
				$lec_exist = 0;
				$bsba_units = 0;
				$lab1_units = 0;
				$lab2_units = 0;
		    
				if ($subjects)
				{
					foreach($subjects as $k => $s):
					  
						if(preg_match("/^nstp/", strtolower($s->code))){
							$nstp_exist++;
						}
						
						if($s->lab_kind == "BSBA"){
							$bsba_units += $s->lab;
						}
						
						if($s->lab_kind == "LAB1"){
							$lab1_units += $s->lab;
						}
						
						if($s->lab_kind == "LAB2"){
							$lab2_units += $s->lab;
						}
					endforeach;
				}
				
					
				$this->view_data["nstp"] = $nstp_exist;
				$student_total_units = $subject_units['total_units'];
				//End Student Subject Units
		
				// Loop through Coursefees
				//Temp-Variable
				$tuition_fee = 0.0;
				$lab_fee = 0.0;
				$rle_fee = 0.0;
				$misc_fee = 0.0;
				$other_fee = 0.0;
				$lab_a_fee = 0.0;
				$lab_b_fee = 0.0;
				$lab_c_fee = 0.0;
				$bsba_fee = 0.0;
				$lab1_fee = 0.0;
				$lab2_fee = 0.0;
			
				if($assigned_course_fee){
					$coursefees = $this->M_coursefees->get_all_course_fees($assigned_course_fee->coursefinance_id);
					
					if($coursefees)
					{
						foreach($coursefees as $cf):
							$studentfee['student_id'] = $enrollee->user_id;
							$studentfee['studentfinance_id'] = $student_finance->id;
				  
							if( strtolower($cf->name) == "tuition fee per unit" )
							{
								$studentfee['fee_id'] = $cf->fee_id;
								$tuition_fee += $cf->value;
							}
				  
							if( strtolower($cf->name) == "laboratory fee per unit" )
							{
								$studentfee['fee_id'] = $cf->fee_id;
								$lab_fee += $cf->value;
							}
							
							if (preg_match("/bsba laboratory/",$cf->name)){
							  $bsba_fee += $cf->value;
							}
							
							if (preg_match("/laboratory 1/",$cf->name)){
							  $lab1_fee += $cf->value;
							}
							
							if (preg_match("/laboratory 2/",$cf->name)){
							  $lab2_fee += $cf->value;
							}
							
							
				  
							if( $cf->is_misc == 1 )
							{
								$misc_fee += $cf->value;
							}
				  
							if( $cf->is_other == 1 )
							{
								$other_fee += $cf->value;
							}
				  
							$studentfee['fee_id'] = $cf->fee_id;
							$studentfee['value'] = $cf->value;
							$studentfee['position'] = $cf->position;
					
							#$this->M_student_fees->insert_student_fees($studentfee);
						endforeach;
					}
					
				}
			
			  $this->view_data['lab_a'] = $bsba_units;
			  $this->view_data['lab_b'] = $lab1_units;
			  $this->view_data['lab_c'] = $lab2_units;
			
			  $this->view_data['lab_a_fee'] = $bsba_fee;
			  $this->view_data['lab_b_fee'] = $lab1_fee;
			  $this->view_data['lab_c_fee'] = $lab2_fee;
			
			  $this->view_data['lab_a_fee_total'] = $lab1_total_fee = $bsba_units*198.00;
			  $this->view_data['lab_b_fee_total'] = $lab2_total_fee = $lab1_units*263.00;
			  $this->view_data['lab_c_fee_total'] = $lab3_total_fee = $lab2_units*318.00;
			  $total_custom_lab = $lab1_total_fee + $lab2_total_fee + $lab3_total_fee;
				
				
		
				# Computations for student total
				$student_total['enrollment_id'] = $enrollment_id;
				$student_total['total_units'] = $student_total_units;
				$student_total['total_lab_units'] = $subject_units['total_lab'];
				
				$student_total['tuition_fee_per_unit'] = $tuition_fee;
				$student_total['lab_fee_per_unit'] = $lab_fee;
				$student_total['total_misc_fee'] = $misc_fee;
				$student_total['total_other_fee'] = $other_fee;
				$student_total['additional_charge'] = $p->total_additional_charges;
				$student_total['previous_account'] = $totalprevious_account;
    
    
				$total_tuition_fee = $tuition_fee*$student_total_units;
				$total_misc = $misc_fee;
				$total_other = $other_fee;
				# End Computations
	  
				# Save Computation Totals
	  
				# Tuition Fee
				$student_total['tuition_fee'] = $total_tuition_fee;
				# RLE Fee
#				$student_total['total_rle'] = $total_rle;
				
				$total_stud_payment = $this->M_studentpayments->get_sum_student_payments($enrollment_id);
				$total_stud_dec = $this->M_student_deductions->get_sum_of_deductions($enrollment_id);
				if($total_stud_payment->account_recivable_student != '')
				{
					$student_total['total_payment'] = $total_stud_payment->account_recivable_student;
				}
				else
				{
					$student_total['total_payment'] = 0.00;
				}
				if($total_stud_dec->amount != '')
				{
					$student_total['total_deduction'] = $total_stud_dec->amount;
				}
				else
				{
					$student_total['total_deduction'] = 0.00;
				}
				
				$student_total["total_student_deduction"] = $total_stud_payment->account_recivable_student + $total_stud_dec->amount;
    
    
				# Total Charge
				$student_total['total_charge'] = $total_tuition_fee  + $total_misc + $total_other+$total_custom_lab;
				# Total Payable/ Amount
				$student_total['total_amount'] = $student_total['total_charge'] + $totalprevious_account;
				# Calculate Remaining Balance
				$student_total['remaining_balance'] = $student_total['total_amount'] - ($total_stud_dec->amount + $total_stud_payment->account_recivable_student);
				
				
				$this->view_data['student_total'] = $student_total;

				# End Save Computation Totals
				
				$this->view_data['sfid'] = $student_finance->id;		
			}
			
			
			$this->view_data['total_deductions'] = $this->M_student_deductions->sum_of_deductions($enrollment_id);
			
			// $student_fees = $this->M_student_fees->get_student_fees($student_finance->id);
			$this->view_data['previous'] =  $this->M_student_previous_accounts->get_student_previous_accounts($enrollment_id);
			
			$this->view_data['additional_charge'] =  $this->M_additional_charges->get_additional_charge($enrollment_id);
			
			/* get student subjects where student course_id,sem_id,year_id equal to subjects*/
			// $student_subjects = $this->M_student_subjects->get_student_subjects($enrollment_id,$p->course_id,$p->year_id,$p->sem_id);
			//$this->view_data['student_subjects'] =  $student_subjects;
			
			
			
			
			$this->view_data['course'] = $p->course_id;
			$this->view_data['year'] = $p->year_id;
			$this->view_data['semester'] = $p->sem_id;
			$this->view_data['enrollment_id'] = $p->id;
			
			//Get payment count for updating subject load
			// $this->view_data['payment_count'] =  $this->M_studentpayments->get_count_student_payments($enrollment_id);
			
			//Get payment count for updating subject load
			// $this->view_data['deduction_count'] =  $this->M_student_deductions->student_deductions_count($enrollment_id);
			
			
			return $this->view_data;
	}
	
	public function update_enrollment_status($enrollment_id = false){
		//UPDATES ENROLLMENTS is_paid, full_paid , partial_paid
		$this->load->model(array("M_subjects","M_student_total_file"));
		if(!$enrollment_id){ return false; }
		
		$total = $this->M_student_total_file->get_student_total_file($enrollment_id);
		
		if($total)
		{
			if(strtoupper($total->status) == "UNPAID")
			{
				$data['updated_at'] = NOW;
				$data['is_paid'] = 0;
				$data['full_paid'] = 0;
				$data['partial_paid'] = 0;
			}
			else if(strtoupper($total->status) == "PAID")
			{
				$data['updated_at'] = NOW;
				$data['is_paid'] = 1;
				$data['full_paid'] = 1;
				$data['partial_paid'] = 0;
			}
			else
			{
				$data['updated_at'] = NOW;
				$data['is_paid'] = 1;
				$data['full_paid'] = 0;
				$data['partial_paid'] = 1;
			}
			
			$result = $this->M_enrollments->update_enrollments($data, $enrollment_id);
		}
	}

	
	public function view_payment_record($enrollment_id = false)
	{
		if($enrollment_id !== false AND ctype_digit($enrollment_id))
		{
			$this->load->model(array('M_studentpayments','M_student_total_file'));
			$this->view_data['system_message'] = $this->session->flashdata('system_message');
			$this->view_data['payment_record'] = $this->M_studentpayments->get(false,array('id','remarks','created_at','account_recivable_student','or_no','old_account','other','total'),array('enrollmentid'=>$enrollment_id));
			$this->view_data['enrollment_id'] = $enrollment_id;
			$this->view_data['student_total'] = $this->M_student_total_file->get_student_total_file($enrollment_id);
		}else
		{
			show_404();
		}
	}
	
	public function show_payment($id = false,$eid = false)
	{
		if(($eid !== false AND ctype_digit($eid)) AND ($id !== false AND ctype_digit($id)))
		{
			$this->load->model('M_studentpayments');
			$this->view_data['payment_record'] = $this->M_studentpayments->get(false,array('id','remarks','created_at','account_recivable_student','or_no','group_or_no','old_account','other'),array('id'=>$id,'enrollmentid'=>$eid));
			$this->view_data['eid'] = $eid;
			$this->token->set_token();
			$this->view_data['form_token'] = $this->token->get_token();
		}else
		{
			if($this->input->post('delete_payment_record'))
			{
				$post_pid = $this->input->post('pid');
				$post_eid = $this->input->post('eid');
				$post_amt = $this->input->post('amount');
				$post_orno = $this->input->post('orno');
				
				$this->load->model('M_studentpayments');
				if($this->M_studentpayments->destroy_payment_record($post_pid,$post_eid))
				{
					$this->load->model('M_student_totals');
					$st = $this->M_student_totals->get_student_total($post_eid);
					$sp = $this->M_student_totals->get(false,array('total_payment'),array('enrollment_id'=>$post_eid));
					$rb = $st->remaining_balance + $post_amt;
					$tp = $sp[0]->total_payment  - $post_amt;
					if($this->M_student_totals->update_student_totals(array('remaining_balance'=>$rb,'total_payment'=>$tp),array('enrollment_id'=>$post_eid)))
					{
						log_message('info','destroy payment record by: '.$this->user.' OR# '.$post_orno.' Amount: '.$post_amt.' From student enrollment id: '.$post_eid);
						$this->session->set_flashdata('system_message','<div class="alert alert-success">Your Request was Succssfully completed</div>');
						$this->token->destroy_token();
						redirect('payment_record/view_payment_record/'.$post_eid);
						
					}else{
							log_message('error','payment_record/show_payments/update_student_totals Unable to update and add '.$rb.' to remaining_balance and subtract ' .$tp.' to totalpayments table,enrollment_id is:'.$post_eid);
							$this->session->set_flashdata('system_message','<div class="alert alert-danger">Payment record destroyed,but unaible to update student totals</div>');
							redirect('payment_record/show_payment/'.$post_pid.'/'.$post_eid);
					}
				}else{
					log_message('error','payment_record/show_payments/destroy_payment_record unable to delete payment record id no: '.$post_pid);
					$this->session->set_flashdata('system_message','<div class="alert alert-danger">Unable to destroy payment record, error processing request</div>');
					redirect('payment_record/show_payment/'.$post_pid.'/'.$post_eid);
				}
			}else
			{
				show_404();
			}
		}
	}
	
	public function delete_deduction($id =  false, $eid = false)
	{
		if($id == false) { show_404(); }
		if($eid == false) { show_404(); }
		
		$this->load->model(array('M_student_deductions'));
		
		$total = $this->M_student_deductions->get($id);
		
		if($total)
		{
			//REVERSE TOTAL AMOUNT TO STUDENT TOTAL BALANCE
			$this->M_student_total_file->reverse_student_total($eid, $total->amount,'DEDUCTION');
			
			//REVERSE STUDENT PAYMENT DETAILS
			$this->M_studentpayments_details->reverse_studentpayment_details($eid);
			
			//UPDATE ENROLLMENT IF PAID, FULL , PARTIAL AND UPDATE SUBJECT LOADS
			$this->M_enrollments->update_enrollments_status($eid); 
		}
		
		$result = $this->M_student_deductions->destroy_deducted_fee($id, $eid);
		
		if($result)
		{
			activity_log('delete deductions',$this->userlogin,'Deduction Deleted by: '.$this->user.'Success; Deduction Id: '.$id);
			
			log_message('error','Deduction Deleted by: '.$this->user.'Success; Deduction Id: '.$id);
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">Deducted successfully deleted.</div>');
			redirect('/fees/view_deducted_fees/'.$eid);
		}
	}
	
	
	public function destroy($id = false, $enrollment_id = false)
	{
		if($id == false) { show_404(); }
		
		$this->load->model(array('M_studentpayments', 'M_student_subjects','M_student_total_file'));
		
		$total = $this->M_studentpayments->get($id);
		
		if($total)
		{
			$enrollment_id = $total->enrollmentid;
			//REVERSE TOTAL AMOUNT TO STUDENT TOTAL BALANCE
			$this->M_student_total_file->reverse_student_total($enrollment_id, $total->total);
			
			//REVERSE TOTAL AMOUNT TO STUDENT TOTAL BALANCE
			$this->reverse_studentpayments_details($enrollment_id, $total->total);
			
			//UPDATE ENROLLMENT IF PAID, FULL , PARTIAL AND UPDATE SUBJECT LOADS
			$this->M_enrollments->update_enrollments_status($enrollment_id); 
		
			$result = $this->M_studentpayments->destroy_payment_record($id, $enrollment_id);
			
			if($result)
			{
				activity_log('delete payment',$this->userlogin,'Payment Deleted by: '.$this->user.'Success; Payment Id: '.$id.' Amount : '.$total->total);
				
				log_message('error','Payment Deleted by: '.$this->user.'Success; Payment Id: '.$id.' Amount : '.$total->total);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Payment successfully deleted.</div>');
				redirect('/payments/view_payment_record/'.$enrollment_id);
			}
		
		}
	}
	
	public function reverse_studentpayments_details($enrollment_id = false, $total_amount = 0)
	{
		if($enrollment_id != false && $total_amount > 0)
		{
			$with_balance = $this->M_studentpayments_details->get_record_with_amount_paid($enrollment_id);
			
			if($with_balance)
			{	
				foreach($with_balance as $obj)
				{
					if($total_amount > 0)
					{
					
						//SUBTACT DELETED AMOUNT TO EACH RECORD UNTIL ZERO
						$cur_amount_paid = $obj->amount_paid;
						$cur_amount_bal = $obj->balance;
						$amount = $obj->amount;
						$subtract_amount = 0;
						
						$now_amount_paid = $cur_amount_paid - $total_amount;
						
						if($total_amount == $amount)
						{
							$for_update['amount_paid'] = 0;
							$for_update['balance'] = $amount;
							
							$subtract_amount = $amount;
						}
						else if($total_amount > $amount)
						{
							$for_update['amount_paid'] = 0;
							$for_update['balance'] = $amount;
							
							$subtract_amount = $cur_amount_paid;
						}
						else
						{
							$for_update['amount_paid'] = $cur_amount_paid - $total_amount;
							$for_update['balance'] = $cur_amount_bal + $for_update['amount_paid'];
							
							$subtract_amount = $total_amount;
						}
						
						$for_update['is_paid'] = 0;
						$for_update['updated_at'] = NOW;
						
						
						$rs_upd = $this->M_studentpayments_details->update_record_by_id($for_update, $obj->id);
						
						$total_amount = $total_amount - $subtract_amount;
					}
				}
			}
		}
	}
	
	public function student_payments($page=0)
	{
		$this->load->Model(array(
			'M_enrollments',
			'M_fees',
			'M_student_payment_record',
			'M_studentpayments'));

		$this->view_data['custom_title'] = "Student Payment for Current Semester";
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."payment_record/student_payments";
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_enrollments->count_all_updaid_enrollments('paid');
		$config["per_page"] = 50;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		
		$filter[' AND is_paid = '] = 1; //ADD FILTER
		$this->pagination->initialize($config);
		$this->view_data['enrollments'] = $rs = $this->M_enrollments->get_all_updaid_enrollments($config["per_page"], $page, $filter);
		// vd($this->db->last_query());
		$data = false;
		$payments = false;
		
		if($rs){
			
			foreach($rs as $obj)
			{
				$payments[$obj->id] = $this->M_student_payment_record->get_enrollee_payment_record($obj->id);
			}	
		}
		$this->view_data['payments'] = $payments;
		$this->view_data['links'] = $this->pagination->create_links();
	}
	
	public function remaining_balance_report($page = 0)
	{
		$this->load->Model(array('M_enrollments','M_fees','M_studentpayments'));
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."payment_record/remaining_balance_report";
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_enrollments->count_all_updaid_enrollments('unpaid');
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		
		$filter[' AND full_paid <> '] = 1; //ADD FILTER
		$this->pagination->initialize($config);
		$rs = $this->M_enrollments->get_all_updaid_enrollments($config["per_page"], $page, $filter);
		
		$data = false;

		$this->view_data['total_amount_due'] = 0;
		$this->view_data['total_deduction'] = 0;
		$this->view_data['total_amount_paid'] = 0;
		$this->view_data['total_balance'] = 0;
		
		if($rs){
			$this->load->library('_Student', array('enrollment_id'=> 0));
			foreach($rs as $k => $obj)
			{
				$this->_student->enrollment_id = $obj->id;
        $this->_student->load_default_function();
				$p = $this->_student->profile;
				if($p){
					$data[$k]['profile'] = $p;
					$data[$k]['fees'] = $this->_student->get_student_fee_profile();
					$data[$k]['total'] = $t = $this->_student->student_payment_totals;

					$this->view_data['total_amount_due'] += $t->total_amount_due;
					$this->view_data['total_deduction'] += $t->total_deduction_amount;
					$this->view_data['total_amount_paid'] += $t->total_amount_paid;
					$this->view_data['total_balance'] += $t->total_balance;
				}
			}	
		}
		
		$this->view_data['enrollments'] = $data;
		$this->view_data['links'] = $this->pagination->create_links();
	}
	
	public function download_remaining_balance_report()
	{
		$this->load->Model(array('M_enrollments','M_fees','M_studentpayments'));
		
		$this->load->helper('my_dropdown');
		
		if($_POST)
		{
			$filter[' AND full_paid <> '] = 1; //ADD FILTER
			$year_id = $this->input->post('year_id');
			if($year_id != ""){
				$filter[' AND year_id = '] = $year_id;
			}
			$course_id = $this->input->post('course_id');
			if($course_id != ""){
				$filter[' AND course_id = '] = $course_id;
			}
			
			$rs = $this->M_enrollments->get_all_updaid_enrollments(0, 0, $filter, true);
			
			$data = false;
			$data['total_amount_due'] = 0;
			$data['total_deduction'] = 0;
			$data['total_amount_paid'] = 0;
			$data['total_balance'] = 0;
			
			if($rs)
			{
					$this->load->library('_student',array('enrollment_id'=>0));
					foreach($rs as $k => $obj)
					{
						$this->_student->enrollment_id = $obj->id;
        		$this->_student->load_default_function();
						$p = $this->_student->profile;
						if($p){
							$data['students'][$k]['profile'] = $p;
							$data['students'][$k]['fees'] = $this->_student->get_student_fee_profile();
							$data['students'][$k]['total'] = $t = $this->_student->student_payment_totals;

							$data['total_amount_due'] += $t->total_amount_due;
							$data['total_deduction'] += $t->total_deduction;
							$data['total_amount_paid'] += $t->total_amount_paid;
							$data['total_balance'] += $t->total_balance;
						}
					}	
					
					$this->load->library('../controllers/_the_downloadables');
					$this->_the_downloadables->_generate_remaining_balance_report($data);					
				
			}else{
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">No record found.</div>');
				redirect(current_url());
			}
		}
	}
	
	public function add_refund($id = false)
	{
		if($id == false) { show_404(); }
		
		$this->view_data['enrollment_id'] = $id;
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$this->view_data['student_total'] = $total = $this->M_student_total_file->get_student_total_file($id);
		
		$this->session_checker->check_if_alive();
		$this->token->set_token();
		$this->view_data['form_token'] = $this->token->get_token();
		
		if($total && $total->balance < 0)
		{
			if($_POST)
			{
				$data['created_at'] = NOW;
				$data['updated_at'] = NOW;
				$data['date'] = $this->input->post('date');
				$data['amount'] = $amount = floatval($this->input->post('amount'));
				$data['remarks'] = $this->input->post('remarks');
				$data['enrollment_id'] = $id;
				
				if($data['amount'] > 0)
				{
					$rs = $this->M_student_refund->create_student_refund($data);
					
					if($rs['status'])
					{
						activity_log('Add Refund',$this->userlogin, 'Add Refund of '.$amount.' Student Refund ID :'.$rs['id']);
						
						//UPDATE REMAINING BALANCE
						unset($data);
						$data['updated_at'] = NOW;
						$data['balance'] = $total->balance + $amount;
						$rs = $this->M_student_total_file->update_student_total_file($data, $id);
						
						if($data['balance'] == 0)
						{
							$this->session->set_flashdata('system_message', '<div class="alert alert-success">Refund successfully added.</div>');
							redirect('fees/view_fees/'.$id);
						}
						$this->session->set_flashdata('system_message', '<div class="alert alert-success">Refund successfully added.</div>');
						redirect(current_url());
					}
				}
			}
		}
		else
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-danger">No excess amount to refund.</div>');
			redirect('fees/view_fees/'.$id);
		}
	}
	
	public function destroy_refund($id =  false)
	{
		show_404(); //REMOVE DELETING OF REFUND
		if($id == false){ show_404(); }
		
		$refund = $this->M_student_refund->get_student_refund($id);
		
		if($refund)
		{
			$this->view_data['student_total'] = $total = $this->M_student_total_file->get_student_total_file($refund->enrollment_id);
			
			//REVERSE STUDENT TOTAL
			$data['updated_at'] = NOW;
			
			if($total->balance < 0)
			{
				//NEGATIVE BALANCE
				$data['balance'] = $total->balance + ($refund->amount * -1);
			}
			else
			{
				//POSITIVE BALANCE
				$data['balance'] = ($total->balance + $refund->amount) * -1;
			}
			
			$rs = $this->M_student_total_file->update_student_total_file($data, $refund->enrollment_id);
			
			//DELETE
			$rs = $this->M_student_refund->delete_student_refund($id);
			if($rs['status']):
				activity_log('Delete Refund',$this->userlogin, 'Delete Refund of '.$refund->amount.' Student Refund ID :'.$refund->id);
				
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Refund successfully deleted.</div>');
				redirect('fees/view_refund/'.$refund->enrollment_id);
			endif;
			
		}
	}
	
	public function add_excess_prev($enrollment_id = false)
	{
		if($enrollment_id == false) { show_404(); }
		
		$this->view_data['enrollment'] = $enrollment = $this->M_enrollments->profile($enrollment_id);
		
		$this->view_data['custom_title'] = "Add : Excess Payment From Previous Enrollments";
		
		$this->view_data['enrollment_id'] = $enrollment_id;
		
		if($enrollment)
		{
			$this->view_data['student_total'] = $this->M_student_total_file->get_student_total_file($enrollment_id);
			
			$this->view_data['excess_prev_sem'] = $excess_prev_sem = $this->M_enrollments->get_excess_prev_sem($enrollment->studid, $enrollment_id);
			
			if($_POST)
			{
				$amount = $this->input->post('amount');
				
				if($amount != "" && floatval($amount) > 0)
				{
					
				}
				else
				{
					$this->session->set_flashdata('system_message', '<div class="alert alert-warning">Invalid or 0 amount.</div>');
					redirect(current_url());
				}
			}
		}
		else
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Something went wrong. Please try again.</div>');
			redirect('fees/view_fees/'.$enrollment_id);
		}
	}
	
	public function view_excess_prev($enrollment_id = false)
	{
		if($enrollment_id == false) { show_404(); }
		
		
	}
}
