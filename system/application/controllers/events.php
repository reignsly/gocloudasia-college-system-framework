<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Events extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->menu_access_checker('events');

		$this->load->model(array(
			'M_events',
		));

		$this->load->helper('ellipsis');
	}
	
	public function index($year ='',$month ='')
	{	
		$this->view_data['custom_title'] = "Calendar";

		$year = $year == '' ? date('Y',time()) : $year;
		$month = $month == '' ? date('m',time()) : $month;

		$this->view_data['dates'] = $e = $this->M_events->get_events_byyearmonth($year.'-'.$month);
		
		$prefs['template'] = '

		   {table_open}<table class="table table-bordered" cellpadding="2" cellspacing="0">{/table_open}

		   {heading_row_start}<tr>{/heading_row_start}
		   {heading_previous_cell}<th colspan="7"><center><a href="{previous_url}" class="btn"><i class="icon-step-backward" id="icon-step-backward"><span class="glyphicon glyphicon-chevron-left" ></span> </i></a>{/heading_previous_cell}
		   {heading_title_cell}<span style="font-size:16px;">{heading}</span>{/heading_title_cell}
		   {heading_next_cell}<a href="{next_url}" class="btn"><i class="icon-step-forward" id="icon-step-forward"><span class="glyphicon glyphicon-chevron-right" ></span> </i></a>{/heading_next_cell}
		   {heading_row_end}<center></th></tr>{/heading_row_end}
		   
		   
		   {week_row_start}<tr>{/week_row_start}
		   {week_day_cell}<td class="center"><span class="label">{week_day}</span></td>{/week_day_cell}
		   {week_row_end}</tr>{/week_row_end}

		   {cal_row_start}<tr>{/cal_row_start}
		   {cal_cell_start}<td class="day-format">{/cal_cell_start}

		   {cal_cell_content}{day} {content}{/cal_cell_content}
		   {cal_cell_content_today}<div class="highlight" id="day_today">{day} {content}</div>{/cal_cell_content_today}

		   {cal_cell_no_content}{day}{/cal_cell_no_content}
		   {cal_cell_no_content_today}<div class="highlight">{day}</div>{/cal_cell_no_content_today}

		   {cal_cell_blank}&nbsp;{/cal_cell_blank}

		   {cal_cell_end}</td>{/cal_cell_end}
		   {cal_row_end}</tr>{/cal_row_end}

		   {table_close}</table>{/table_close}
		';
		
		// $prefs['start_day']    = 'sunday';
		$prefs['month_type']   ='long';
		$prefs['day_type']     ='long';
		$prefs['show_next_prev']  =TRUE;
		$prefs['next_prev_url'] = base_url().'events/index';
		
		$titles = "";
		$no_of_months = cal_days_in_month(CAL_GREGORIAN,$month,$year);
		$links = array();
		$time = strtotime(date('Y-m-d'));
		
		for($x = 1; $x <= $no_of_months; $x++){
			$xx = str_pad($x, 2, "0", STR_PAD_LEFT);
			$titles = "";
			$editlinks = "";
			if( strtotime($year.'-'.$month.'-'.$xx) <= $time ){
				$editlinks = "<br/>";
				if( date('d-m-Y',$time) === $xx.'-'.$month.'-'.$year){
					
					if($this->departments->edit_calendar == "1"){
						$editlinks = "<span class='label label-success'>
								<a href='".base_url()."events/create/".$year."/".$month."/".$x."' style='text-decoration:none;font-size;10pt;color:white;' >
									Create Event
								</a>
							</span><p></p>";	
					}
				}
			}
			else {
				if($this->departments->edit_calendar == "1"){
			    $editlinks = "<span class='label label-success'>
							<a href='".base_url()."events/create/".$year."/".$month."/".$x."' style='text-decoration:none;font-size;10pt;color:white;' >
								Create Event
							</a>
						</span><p></p>";
				}
			}

			$rs_events = $this->M_events->get_events_bydate($year.'-'.$month.'-'.$x);		
			
			if($rs_events)	
			{
				$titles .= "<div class='row'><div class='col-md-12'>";
				$titles .= "<ul class='nav nav-pills nav-stacked'>";
				foreach ($rs_events as $key => $obj) {
					$ttl = ellipsis($obj->title, 15);		
					$xclass = $obj->is_holiday == '1' ? "is_holiday" : "";
					if($this->departments->edit_calendar == "1"){
						$titles .= "<li class='' ><a style='padding:2px 2px !important' class='".$xclass."' href='".base_url('events/edit/'.__link($obj->id))."''>";
						$titles .= '<i class="fa fa-hand-o-right"></i> '.$ttl;
					}else{
						$titles .= "<li class='' ><a style='padding:2px 2px !important' class='btn-view-event ".$xclass."' href='".base_url('ajax/view_event/'.__link($obj->id))."''>";
						$titles .= '<i class="fa fa-hand-o-right"></i> '.$ttl;
					}
					$titles .= "</a></li>";
				}
				$titles .= "<ul></div></div>";				
			}

			$editlinks .= $titles;	
			
			$links[$x] = $editlinks;
		}
		// vd($links);
		$this->load->library('calendar',$prefs);
		$this->view_data['calendar'] = $this->calendar->generate_calendar($year,$month,$links);
	}
	
	public function create($year = false, $month = false, $day = false)
	{
		$this->check_stud_access('edit_calendar');
		
		$this->view_data['event'] = FALSE;
		$this->view_data['date'] = (!$year || !$month || !$day) ? date('Y-m-d h:i a') : $year.'-'.$month.'-'.$day.' '.date('h:i a');
		
		if($this->input->post())
		{
			$this->load->helper('time');

			$data['is_holiday'] = 0;
			$data['to_all'] = 0;
			if($_POST['to_what']){
				while(list(,$val) = each($_POST['to_what'])){
					if($val=='is_holiday'){$data['is_holiday']=1;}
					if($val=='to_all'){$data['to_all']=1;}
				}
			}
			$data['description'] = $this->input->post('description');
			$data['title'] = $this->input->post('title');
			$data['start_date'] = $sd = date('Y-m-d H:i:s', strtotime($this->input->post('start_date')));
			$data['end_date'] = $ed = date('Y-m-d H:i:s', strtotime($this->input->post('end_date')));
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');

			$r_year = date('Y', strtotime($sd));
			$r_mon = date('m', strtotime($sd));
			
			$result = $this->M_events->create_event($data);

			if($result['status'])
			{
				$id = $result['id'];
				activity_log('create events',$this->userlogin,'Created by: '.$this->user.'Success; Event Id: '.$id);
				$this->_msg('s','You have successfully created an event', "events/index/$r_year/$r_mon");
			}
			else
			{
				$this->_msg('e','Something went wrong, please try again', current_url());
			}
		}
	
	}
	
	public function edit($hash='')
	{
		$this->check_stud_access('edit_calendar');
		$id = $this->check_hash($hash);
		$this->view_data['event'] = $this->M_events->get_events($id);
		
		if($_POST)
		{
			if($_POST)
			{
				$data['is_holiday'] = 0;
				$data['to_all'] = 0;
				while(list(,$val) = each($_POST['to_what'])){
					if($val=='is_holiday'){$data['is_holiday']=1;}
					if($val=='to_all'){$data['to_all']=1;}
				}
				$data['title'] = $_POST['title'];
				$data['description'] = $_POST['description'];
				$data['start_date'] = date('Y-m-d H:i:s',strtotime($_POST['start_date']));
				$data['end_date'] = date('Y-m-d H:i:s',strtotime($_POST['end_date']));
				$data['updated_at'] = date('Y-m-d H:i:s');
				$id = $_POST['id'];
				
				$result = $this->M_events->update_event($data,$id);
				if($result['status'])
				{
					activity_log('update events',$this->userlogin,'Updated by: '.$this->user.'Success; Event Id: '.$id);
					
					redirect('events');
				}
			}
		}
	}
	
	public function display($hash='')
	{
		$id = $this->check_hash($hash);
		$this->view_data['event'] = $this->M_events->get_events($id);
	}
	public function destroy($hashid)
	{
		$id = $this->check_hash($hashid);
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_events'));
		
		$result = $this->M_events->delete($id);
		
		if($result){
			$this->_msg('s','Event successfully removed','events');
		}else{
			$this->_msg('e','Unable to remove event, please try again','events');
		}
	}

	public function calendar($year ='',$month ='')
	{
		
		
	}
}