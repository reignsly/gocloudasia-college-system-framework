<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Departments_cpanel extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('M_departments','M_main_menus','M_menus','M_menus_method','M_main_menus_method'));
		$this->userlogin = "CPanel";
	}
	
	// Create
	public function create()
	{
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->token->set_token();
		
		if($_POST)
		{
			if($this->form_validation->run('departments') == true)
			{	
				$data = $this->input->post('departments');
				$data['visible'] = isset($data['visible']) ? 1 : 0;
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['updated_at'] = date('Y-m-d H:i:s');
				$data['controller'] = 'login';
				
				if (isset($_FILES['userfile']) && is_uploaded_file($_FILES['userfile']['tmp_name'])) {
				
					$config['upload_path'] = $path = 'assets/images/logo';
					$config['allowed_types'] = 'jpg|png';
					$config['max_size']	= '100';
					$config['max_width']  = '1024';
					$config['max_height']  = '768';
					$config['remove_spaces']  = true;
					$config['overwrite']  = true;
					$config['encrypt_name']  = true;

					$this->load->library('upload', $config);
					
					if ( ! $this->upload->do_upload())
					{
						// vd($this->upload->display_errors());
						$this->session->set_flashdata('system_message', '<div class="alert alert-danger">'.$this->upload->display_errors().'.</div>');
						redirect('departments_cpanel/create');
					}
					else
					{
						$xfile = $this->upload->data();
						$data['icon'] = $path.'/'.$xfile['file_name'];
					}
				}
				
				$result = $this->M_departments->insert($data);
				
				if($result['status'])
				{
					$id = $result['id'];
					activity_log('Create Department', $this->userlogin, 'Department ID'.$id);
					$this->session->set_flashdata('system_message', '<div class="alert alert-success"><b><span class="glyphicon glyphicon-ok"></span>&nbsp; Department successfully added.</b></div>');
					redirect('departments_cpanel');
				}
			}
		}
	}
	
	// Retrieve
	public function index($page = 0)
	{
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		//PAGINATION
		$filter = false;
		$like = false;
		$order_by = false;
		
		// $filter['departments.department <> '] = 'student'; // exclude  
		
		if($_GET)
		{
			if(isset($_GET['submit']) && $_GET['submit'] == "Search")
			{
				$page = 0;
			}
			
			if(isset($_GET['department']) && trim($_GET['department']) != ''){
				$this->view_data['department'] = $department = trim($_GET['department']);
				$like['departments.department'] = $department;
			}
		}
		
		//CONFIGURATION
		// $get['fields'] = array(
				// 'enrollments.id',
				// 'enrollments.studid',
				// 'enrollments.name' ,
				// 'years.year',
				// 'courses.course' 
		// );
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		// $get['join'] = array(
			
			// 1 => array(
				// "table" => "courses",
				// "on"	=> "courses.id = enrollments.course_id",
				// "type"  => "LEFT"
			// ),
			// 2 => array(
				// "table" => "years",
				// "on"	=> "years.id = enrollments.year_id",
				// "type"  => "LEFT"
			// )
		// );
		$get['order'] = "departments.ord";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."departments_cpanel/index";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_departments->get_record("departments", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_departments->get_record("departments", $get);
		$this->view_data['links'] = $this->pagination->create_links();	
	}
	
	public function display($id)
	{
		$this->load->model(array('M_years'));
		
		$this->view_data['years'] = $this->M_years->get_years($id);
	}
	
	// Update
	public function edit($id = false)
	{
		if(!$id){ show_404(); }
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$this->view_data['departments'] = $this->M_departments->get($id);
		
		$this->view_data['edit'] = true;
		
		$this->view_data['id'] = $id;
		
		if($_POST)
		{
			if($this->form_validation->run('departments2') == true)
			{	
				$data = $this->input->post('departments');
				$data['visible'] = isset($data['visible']) ? 1 : 0;
				$data['updated_at'] = date('Y-m-d H:i:s');
				if (isset($_FILES['userfile']) && is_uploaded_file($_FILES['userfile']['tmp_name'])) {
				
					$config['upload_path'] = $path = 'assets/images/logo';
					$config['allowed_types'] = 'jpg|png';
					$config['max_size']	= '100';
					$config['max_width']  = '1024';
					$config['max_height']  = '768';
					$config['remove_spaces']  = true;
					$config['overwrite']  = true;
					$config['encrypt_name']  = true;

					$this->load->library('upload', $config);
					
					if ( ! $this->upload->do_upload())
					{
						// vd($this->upload->display_errors());
						$this->session->set_flashdata('system_message', '<div class="alert alert-danger">'.$this->upload->display_errors().'.</div>');
						redirect('departments_cpanel/update');
					}
					else
					{
						$xfile = $this->upload->data();
						$data['icon'] = $path.'/'.$xfile['file_name'];
					}
				}
				// vp($_POST);
				// vp($data);
				// vd($_POST);
				$result = $this->M_departments->update($id, $data);
				
				if($result)
				{
					$id = $result['id'];
					activity_log('Updated Department', $this->userlogin, 'Department ID'.$id);
					$this->session->set_flashdata('system_message', '<div class="alert alert-success"><b><span class="glyphicon glyphicon-ok"></span>&nbsp; Department successfully updated.</b></div>');
					redirect(current_url());
				}
			}
		}
	}
	
	public function hide($id = false)
	{
		if(!$id){ show_404(); }
		
		$rs = $this->M_departments->get($id);
		
		if($rs)
		{
			$data['visible'] = 0;
			$rs = $this->M_departments->update($id, $data);
			if($rs)
			{
				activity_log('Hide Department', $this->userlogin,' Department Code : '.$rs->department.' ID : '.$id);
				
				$this->session->set_flashdata('system_message', '<div class="alert alert-success"><b><span class="glyphicon glyphicon-ok"></span>&nbsp; Department successfully hidden.</b></div>');
				
				redirect('departments_cpanel');
			}
		}
	}
	
	public function show($id =  false)
	{
		if(!$id){ show_404(); }
		
		$rs = $this->M_departments->get($id);
		
		if($rs)
		{
			$data['visible'] = 1;
			$rs = $this->M_departments->update($id, $data);
			if($rs)
			{
				activity_log('Show Department', $this->userlogin,' Department Code : '.$rs->department.' ID : '.$id);
				
				$this->session->set_flashdata('system_message', '<div class="alert alert-success"><b><span class="glyphicon glyphicon-ok"></span>&nbsp; Department successfully showed.</b></div>');
				
				redirect('departments_cpanel');
			}
		}
	}
	
	public function menus($id = false, $page = 0)
	{
		if(!$id){ show_404(); }
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->view_data['id'] = $id;
		$this->view_data['departments'] = $this->M_departments->get($id);
		$dep = $this->M_departments->get($id);
		
		if($dep)
		{
			$get['where']['department'] = $dep->department;
			$get['where']['menu_lvl'] = 1;
			$get['order'] = 'menu_num';
		
	
			$this->load->library("pagination");
			$config = $this->pagination_style();
			$config["base_url"] = base_url() ."departments_cpanel/menus/".$id;
			$suffix = '?'.http_build_query($_GET, '', "&");
			$suffix = str_replace("&submit=Search", "", $suffix);
			$config['suffix'] = $suffix;
			$config['first_url'] = $config['base_url'].$config['suffix']; 
			
			$get['count'] = true;

			$this->view_data['total_rows'] = $config["total_rows"] = $this->M_menus->get_record(false, $get);
			
			$config["per_page"] = 8;
			$config['num_links'] = 10;
			$config["uri_segment"] = 4;
			// $config['use_page_numbers'] = TRUE;
			$this->pagination->initialize($config);
			
			$config['start'] = $page;
			$config['limit'] = $config['per_page'];
			
			$get['start'] = $page;
			$get['limit'] = $config['per_page'];
			$get['count'] = false;
			
			$this->view_data['search'] = $search = $this->M_menus->get_record(false, $get);
			$this->view_data['links'] = $this->pagination->create_links();
			
			$this->view_data['menus'] = $m = $this->M_menus->fetch_all_menus($search, $dep->department);
			
		}
		else
		{
			show_404();
		}
	}
	
	public function edit_menu_group($id = false, $menu_grp_id = false)
	{
		if(!$id) { show_404(); }
		
		$this->view_data['id'] = $id;
		
		$this->view_data['departments'] = $this->M_departments->get($id);
		
		$this->view_data['menu_grp_id'] = $menu_grp_id;
		
		$this->view_data['menu_group'] = $this->M_menus->get($menu_grp_id);
		
		if($_POST)
		{
			if($this->form_validation->run('edit_menu_1') == true)
			{	
				$data = $this->input->post('menu_group');
				$data['visible'] = isset($data['visible']) ? 1 : 0;
				
				$rs = $this->M_menus->update($menu_grp_id, $data);
				
				if($rs)
				{
					activity_log('Update Menu Group', $this->userlogin,' Menu ID : '.$menu_grp_id);
				
					$this->session->set_flashdata('system_message', '<div class="alert alert-success"><b><span class="glyphicon glyphicon-ok"></span>&nbsp; Menu Group Successfully Updated.</b></div>');
					
					redirect(current_url());
				}
			}
		}
	}
	
	public function menu_list($id = false, $menu_grp_id = false)
	{
		if(!$id) { show_404(); }
		
		$this->view_data['id'] = $id;
		
		$this->view_data['departments'] = $this->M_departments->get($id);
		
		$this->view_data['menu_grp_id'] = $menu_grp_id;
		
		$head = $this->M_menus->get($menu_grp_id);
		
		$this->view_data['menu_list'] = false;
		
		if($head):
		
		$this->view_data['menu_list'] = $this->M_menus->fetch_menu_list($head->department, $head->menu_sub); 
		
		if($_POST):
		
		$menus = $this->input->post('menu');
		
		foreach($menus as $keyid => $inputs)
		{
			$inputs['visible'] = isset($inputs['visible']) ? 1 : 0;
			$rs = $this->M_menus->update($keyid, $inputs);
			if($rs)
			{
				activity_log('Update Menu', $this->userlogin,' Menu ID : '.$keyid);
			}
		}
		
		$this->session->set_flashdata('system_message', '<div class="alert alert-success"><b><span class="glyphicon glyphicon-ok"></span>&nbsp; Menus Successfully Updated.</b></div>');
					
		redirect(current_url());
		
		endif;
		
		endif;
	}
	
	public function add_menu($id = false, $menu_grp_id = false)
	{
		if(!$id) { show_404(); }
		
		$this->view_data['id'] = $id;
		
		$this->view_data['departments'] = $this->M_departments->get($id);
		
		$this->view_data['menu_grp_id'] = $menu_grp_id;
		
		$this->view_data['head'] = $head = $this->M_menus->get($menu_grp_id);
		
		$dep = $this->M_departments->get($id);
		
		$this->view_data['menu_list'] = false;
		
		$already_added = false;
		
		if($head)
		{
			$already_added = $this->M_menus->fetch_menu_list($head->department, $head->menu_sub); 
		}
		
		$main_menus = $this->M_main_menus->get_all_main_menus();
		
		$menu_list = array();
		
		if($main_menus):
		
		foreach($main_menus as $obj):
			
			$pass = false;
			
			if($already_added)
			{
				//EXCLUDE ADDED MENU LIST
				foreach($already_added as $added):
					if($added->main_menu_id == $obj->id)
					{
						$pass = true;
						break;
					}
				endforeach;				
			}
			
			if(!$pass):
				$menu_list[] = $obj;
				
			endif;
		
		endforeach;
		
		endif;
	
		$this->view_data['menu_list'] = $menu_list;
		
		if($_POST):
		
		$menus = $this->input->post('menus');
		
		if(!$menus){
			
			$this->session->set_flashdata('system_message', '<div class="alert alert-danger"><b><span class="glyphicon glyphicon-ok"></span>&nbsp; No Menu List was checked.</b></div>');
					
			redirect(current_url());
		}
		
		$xmen_sub = false;
		
		if(isset($_POST['menu_group']) && trim($_POST['menu_group']) != "")
		{
			/*
				IF SELECTED RADIO IS TO SELECT FROM PREVIOUS MENU HEAD
				GET MENU SUB OF THE DATA
			*/
			$sel_menu_head = $this->M_menus->get($_POST['menu_group']);
			
			$xmen_sub = false;
			if($sel_menu_head){
				//CHECK IF MENU HEAD ALREADY ADDED IN THE DEPARTMENT MENU
				$where = array(
					"department" => $dep->department,
					"caption" => $sel_menu_head->caption,
					"menu_lvl" => 1
				);
				$rs_alread_added = $this->M_menus->fetch_all($where);
				if($rs_alread_added)
				{
					//MENU HEAD IS ALREADY IN THE DEPARTMENTS MENU
					$xmen_sub = $sel_menu_head->menu_sub;
					//vd($rs_alread_added);
				}
				else
				{
					//INSERT THE NEW MENU HEAD SELECTED
					unset($data);
					$data['department'] = $dep->department;
					$data['controller'] = $sel_menu_head->controller;
					$data['caption'] = $sel_menu_head->caption;
					$data['menu_grp'] = $sel_menu_head->menu_grp;
					$data['menu_sub'] = $sel_menu_head->menu_sub;
					$data['menu_num'] = $sel_menu_head->menu_num;
					$data['menu_lvl'] = $sel_menu_head->menu_lvl;
					$data['menu_icon'] = "glyphicon glyphicon-globe";
					$data['attr'] = $sel_menu_head->attr;
					
					$rs = $this->M_menus->insert($data);
					if($rs['status'])
					{
						activity_log('Create New Menu Head/Group', $this->userlogin,' Menu ID : '.$rs['id']);
						
						$xmen_sub = $data['menu_sub'];
					}
				}
			}
		}
		elseif(isset($_POST['create_menu_group']) && trim($_POST['create_menu_group']) != "")
		{
			/*
				IF SELECTED RADIO IS TO CREATE NEW MENU HEAD
				SAVE DATA AND GET MENU SUB
			*/
			$new_head_caption = trim($_POST['create_menu_group']);
			$data['caption'] = $new_head_caption;
			$data['controller'] = '#';
			$data['menu_lvl'] = 1;
			$rs = $this->M_menus->insert($data);
			
			if($rs['status'])
			{
				$insert_id = $rs['id'];
				unset($data);
				$data['menu_num'] = $insert_id;		
				$data['menu_grp'] = $insert_id;		
				$data['menu_sub'] = $insert_id.'_SUB';
				$data['department'] = $dep->department;
				$data['menu_icon'] = "glyphicon glyphicon-globe";
				$this->M_menus->update($insert_id, $data);
			
				$xmen_sub = $data['menu_sub'];
				
				activity_log('Create New Menu Head/Group', $this->userlogin,' Menu ID : '.$insert_id);
			}
		}
		else
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-danger"><b><span class="glyphicon glyphicon-ok"></span>&nbsp; No Menu Group Selected or Created.</b></div>');
					
			redirect(current_url());
		}
		
		/*
			IF MENU SUB WAS SUCCESSFULLY CREATED
			SAVE THE CHECKED MENU LIST
		*/
		if($xmen_sub)
		{
			foreach($menus as $menu_id){
				$rs_menu = $this->M_main_menus->get($menu_id);
				
				if($rs_menu){
					
					$where = array(
						'department' => $dep->department,
						'main_menu_id' => $menu_id
					);
					$rs_alread_added = $this->M_menus->fetch_all($where);
					
					if(!$rs_alread_added){
					
						unset($data);
						$data['department'] = $dep->department;
						$data['menu_grp'] = $xmen_sub;
						$data['main_menu_id'] = $menu_id;
						$data['controller'] = $rs_menu->controller;
						$data['caption'] = $rs_menu->caption;
						$data['menu_lvl'] = $rs_menu->menu_lvl;
						$data['menu_num'] = $rs_menu->menu_num;
						$data['menu_icon'] = "glyphicon glyphicon-globe";
						$data['visible'] = $rs_menu->visible;
						$data['attr'] = $rs_menu->attr;
						
						$rs = $this->M_menus->insert($data);
						
						if($rs['status'])
						{
							activity_log('Add New Menu', $this->userlogin,' Menu ID : '.$rs['id']);

							//GET MENU FUNCTION/METHOD AND INSERT
							$rs_method = $this->M_main_menus_method->fetch_all(array('main_menu_id'=>$rs_menu->id));
							if($rs_method){
								foreach ($rs_method as $key => $m_method) {
									unset($data);
									$data['menu_id'] = $rs['id'];
									$data['method'] = $m_method->method;
									$data['access'] = 1;
									$rs_m_method = $this->M_menus_method->insert($data);
								}
							}

						}
					}
				}
			}
			
			$this->session->set_flashdata('system_message', '<div class="alert alert-success"><b><span class="glyphicon glyphicon-ok"></span>&nbsp; Your menu list was successfully updated/created.</b></div>');
					
			redirect(current_url());
		}
		else
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-danger"><b><span class="glyphicon glyphicon-remove"></span>&nbsp; New Menu List was failed to save. Please try again.</b></div>');
					
			redirect(current_url());
		}
		
		
		endif;
	}
	
	public function delete_menu_head($id = false, $menu_grp_id = false)
	{
		if(!$id) { show_404(); }
		if(!$menu_grp_id) { show_404(); }
		
		$this->view_data['id'] = $id;
		
		$this->view_data['menu_grp_id'] = $menu_grp_id;
		
		$this->view_data['head'] = $head = $this->M_menus->get($menu_grp_id);
		
		$dep = $this->M_departments->get($id);
		
		if($head)
		{
			$xwhere['department'] = $dep->department;
			$xwhere['menu_grp'] = $head->menu_sub;
			
			$rs_menus = $this->M_menus->fetch_all($xwhere);
			
			if($rs_menus):
				foreach ($rs_menus as $key => $value) {
					$this->M_menus_method->delete(array('menu_id' => $value->id));
				}
			endif;

			$this->M_menus->delete($xwhere); //DELETE SUB MENUS
			
			$this->M_menus->delete($menu_grp_id); //DELETE MENU HEADER
			
			activity_log('Delete Menu Header', $this->userlogin,' Menu ID : '.$menu_grp_id.' Caption : '.$head->caption);
			
			$this->session->set_flashdata('system_message', '<div class="alert alert-success"><b><span class="glyphicon glyphicon-ok"></span>&nbsp; Menu Group successfully deleted.</b></div>');
					
			redirect(base_url('departments_cpanel/menus/'.$id));
		}
		
	}
	
	public function delete_menu_list($id = false, $menu_id = false)
	{
		if(!$id) { show_404(); }
		if(!$menu_id) { show_404(); }
		
		$this->view_data['id'] = $id;
		
		$this->view_data['menu'] = $menu = $this->M_menus->get($menu_id);

		if($menu)
		{
			
			$this->M_menus->delete($menu_id); //DELETE MENU HEADER

			$this->M_menus_method->delete(array('menu_id' => $menu_id));
			
			activity_log('Delete Menu', $this->userlogin,' Menu ID : '.$menu_id.' Caption : '.$menu->caption);
			
			$this->session->set_flashdata('system_message', '<div class="alert alert-success"><b><span class="glyphicon glyphicon-ok"></span>&nbsp; Menu successfully deleted.</b></div>');
					
			redirect(base_url('departments_cpanel/menus/'.$id));
		}
	}
	
	public function student_profile_access($id = false)
	{
		if(!$id) { show_404(); }
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$this->view_data['departments'] = $this->view_data['spa'] = $this->M_departments->get($id);
		
		$this->view_data['id'] = $id;
		
		if($_POST)
		{
			unset($data);
			$data['stud_profile'] = $stud_profile = isset($_POST['stud_profile']) ? 1 : 0;
			$data['stud_grade'] = $stud_grade = isset($_POST['stud_grade']) ? 1 : 0;
			$data['stud_fees'] = $stud_fees = isset($_POST['stud_fees']) ? 1 : 0;
			$data['stud_otr'] = $stud_otr = isset($_POST['stud_otr']) ? 1 : 0;
			$data['stud_issues'] = $stud_issues = isset($_POST['stud_issues']) ? 1 : 0;

			$data['stud_edit_profile'] = isset($_POST['stud_edit_profile']) ? 1 : 0;
			$data['stud_edit_grade'] = isset($_POST['stud_edit_grade']) ? 1 : 0;
			$data['stud_add_fees'] = isset($_POST['stud_add_fees']) ? 1 : 0;
			$data['stud_add_issues'] = isset($_POST['stud_add_issues']) ? 1 : 0;
			$data['stud_subject'] = isset($_POST['stud_subject']) ? 1 : 0;
			$data['stud_edit_subject'] = isset($_POST['stud_edit_subject']) ? 1 : 0;
			$data['stud_portal_activation'] = isset($_POST['stud_portal_activation']) ? 1 : 0;

			$data['edit_announcement'] = isset($_POST['edit_announcement']) ? 1 : 0;
			$data['edit_calendar'] = isset($_POST['edit_calendar']) ? 1 : 0;
			// vd($data);
			$result = $this->M_departments->update($id, $data);
			// vp($this->db->last_query());
			// vd($result);
			if($result)
			{
				activity_log('Updated Department Student Profile Access', $this->userlogin, 'Department ID'.$id.' Access : '.implode($data));
				$this->session->set_flashdata('system_message', '<div class="alert alert-success"><b><span class="glyphicon glyphicon-ok"></span>&nbsp; Student Profile Access updated.</b></div>');
				redirect(current_url());
			}
		}
	}

	public function ajax_set_menu()
	{
		$this->disable_menus = true;
	 	$this->disable_views = true;

	 	$xid = $this->input->post('id');		
	 	$xaccess = $this->input->post('access');		

	 	$result['status'] = 0;

	 	if($xid){
	 		$data['visible'] = $xaccess == 'true' ? 1 : 0;	
	 		$cap = $xaccess == 'true' ? 'visible' : 'hidden';	
	 		$rs = $this->M_menus->update($xid,$data);
	 		if($rs){
	 			$result['status'] = 1;
	 			$result['title'] = 'Success';
	 			$result['msg'] = "<div class='alert alert-success'>The menu was set to {$cap}</div>";

	 			activity_log("Menu set to {$cap} ", $this->userlogin, 'Menus Id : '.$xid);
	 		}
	 		else{
	 			$result['status'] = 0;
	 			$result['title'] = 'Failed';
	 			$result['msg'] = "<div class='alert alert-warning'>The action was failed. Please try again.</div>";	
	 		}
	 	}
	 	else{
	 			$result['status'] = 0;
	 			$result['title'] = 'Error';
	 			$result['msg'] = "<div class='alert alert-danger'>Ajax Failed. Missing Parameter.</div>";	
	 	}
	 	
	 	echo json_encode($result);
	 	exit(0);
	}

	public function ajax_set_method()
	{
		$this->disable_menus = true;
	 	$this->disable_views = true;

	 	$xid = $this->input->post('id');		
	 	$xaccess = $this->input->post('access');		

	 	$result['status'] = 0;

	 	if($xid){
	 		$data['access'] = $xaccess == 'true' ? 1 : 0;	
	 		$cap = $xaccess == 'true' ? 'visible' : 'hidden';	
	 		$rs = $this->M_menus_method->update($xid,$data);
	 		if($rs){
	 			$result['status'] = 1;
	 			$result['title'] = 'Success';
	 			$result['msg'] = "<div class='alert alert-success'>The method was set to {$cap}</div>";

	 			activity_log("Method set to {$cap} ", $this->userlogin, 'Menus Id : '.$xid);
	 		}
	 		else{
	 			$result['status'] = 0;
	 			$result['title'] = 'Failed';
	 			$result['msg'] = "<div class='alert alert-warning'>The action was failed. Please try again.</div>";	
	 		}
	 	}
	 	else{
	 			$result['status'] = 0;
	 			$result['title'] = 'Error';
	 			$result['msg'] = "<div class='alert alert-danger'>Ajax Failed. Missing Parameter.</div>";	
	 	}
	 	
	 	echo json_encode($result);
	 	exit(0);
	}
}
?>