<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class System_settings extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->check_if_alive();
		$this->load->model(['M_sys_par','M_system_custom_message']);
	}
	
	public function colap_menu($status = NULL){
		if($status === NULL){ show_404(); }

		$sp = $this->M_sys_par->get_sys_par();

		if($sp){

			unset($data);
			$data['is_menu_accordion'] = $status == 1 ? 0 : 1;
			$this->M_sys_par->update($sp->id, $data);

		}else{
			$this->_msg('e','System Settings not properly set.', current_url());
		}
	}

	######## System Custom Messages Function ########
		/**
		 * Custom Message
		 */
		public function system_custom_message()
		{
			$this->menu_access_checker(); // Check access to this menu
			$this->view_data['system_custom_message'] = $m = $this->M_system_custom_message->get_all_messages();
		}

		public function edit_custom_message($id)
		{
			$this->menu_access_checker('system_settings/system_custom_message'); // Check access to this menu

			$this->view_data['id'] = $id;
			$this->view_data['message'] = $this->M_system_custom_message->pull($id);

			if($this->input->post()){
				$rs = $this->M_system_custom_message->update_message($id, $this->input->post());
				$this->_msg($rs->code, $rs->msg, current_url());
			}
		}
	######## End of System Custom Messages ##########

	######## Advance Settings #######################
		/**
		 * Advance Setup for the system
		 */
		public function advance_settings()
		{
			$this->menu_access_checker(); // Check access to this menu
			$this->view_data['sys_par'] = $this->syspar;

			if($this->input->post('update_portal')){
				$data = array();
				$syspar = $this->input->post('syspar');

				$data['enable_portal'] = isset($syspar['enable_portal']) ? 1 : 0;

				$rs = (object)$this->M_sys_par->update_sys_par($data);
				if($rs->status){
					activity_log('Update Student Portal',false, 'Data : '.arr_str($data));
					$this->_msg('s','Student Portal was successfully updated.', current_url());
				}else{
					$this->_msg('e','Student Portal failed to be updated, please try again.', current_url());
				}
			}
		}
	######## End of Advance Settings ################
} 