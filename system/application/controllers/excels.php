<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Excels extends MY_Controller {
	
	//ALL EXCEL MUST BE HERE
	
	public function __construct()
	{
		parent::__construct();

		$this->session_checker->check_if_alive();

		// load models
		$this->load->model(
				'M_student_payment_record'
			);
	}
	
	public function index(){
	
	}
	
	public function student_charges()
	{
		$this->load->Model(array('M_enrollments','M_fees'));
		
		$this->load->helper('my_dropdown');
		
		if($_POST){
			
			$this->disable_layout = true;
			$this->output->enable_profiler(FALSE);

			$year_id = $this->input->post('year_id');
			$course_id = $this->input->post('course_id');
			
			
			$param = array();
			$param[' AND full_paid <> '] = 1;
			if($year_id != ""){
				$param[' AND year_id = '] = $year_id;
				$param[' AND course_id = '] = $course_id;
			}
			
			$rs = $this->M_enrollments->get_all_updaid_enrollments(0,100, $param, $all = true);
			
			if($rs){
				
					$data = array();
					
					foreach($rs as $obj)
					{
						$data[] = $this->M_fees->get_enrollment_total_fees($obj->id); //GET FEE PROFILES
					}

					$this->load->library('../controllers/_the_downloadables');
					$this->_the_downloadables->_generate_student_charges2($data);					
				
			}else{
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">No record found.</div>');
				redirect(current_url());
			}
			
		}
	}

	public function student_payments()
	{
		$this->menu_access_checker();

		$this->load->Model(array('M_enrollments','M_fees','M_studentpayments'));
		
		$this->load->helper('my_dropdown');
		
		$this->view_data['academic_year_id'] = $this->cos->user->academic_year_id;
		
		if($this->input->post('download_by_course_year')){
			$this->load->library('../controllers/_the_downloadables');
			$report_type = $this->input->post('report_type');
			$rs = false;

			if($report_type == "list"){
				$rs = $this->M_student_payment_record->student_payment_record_list($this->input->post());
				if($rs){
					$this->_the_downloadables->_generate_student_payment_record_list($rs);
				}
			}
			if($report_type == "summary_year"){
				$rs = $this->M_student_payment_record->student_payment_record_year($this->input->post());
				if($rs){
					$this->_the_downloadables->_generate_student_payment_record_year($rs);
				}
			}
			if($report_type == "summary_course"){
				$rs = $this->M_student_payment_record->student_payment_record_course($this->input->post());
				if($rs){
					$this->_the_downloadables->_generate_student_payment_record_course($rs);
				}
			}
			
			if(!$rs)
			{
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">No record found.</div>');
				redirect(current_url());
			}
		}

		if($this->input->post('download_by_ay')){
			$this->load->library('../controllers/_the_downloadables');
			$report_type = $this->input->post('report_type');
			$rs = false;

			if($report_type == "list"){
				$rs = $this->M_student_payment_record->student_payment_record_ay_list($this->input->post());
				if($rs){
					$this->_the_downloadables->_generate_student_payment_record_ay_list($rs,$this->input->post());
				}
			}
			if($report_type == "summary_year"){
				$rs = $this->M_student_payment_record->student_payment_record_ay_year($this->input->post());
				if($rs){
					$this->_the_downloadables->_generate_student_payment_record_ay_year($rs,$this->input->post());
				}
			}
			if($report_type == "summary_course"){
				$rs = $this->M_student_payment_record->student_payment_record_ay_course($this->input->post());
				if($rs){
					$this->_the_downloadables->_generate_student_payment_record_ay_course($rs,$this->input->post());
				}
			}
			if($report_type == "summary_semester"){
				$rs = $this->M_student_payment_record->student_payment_record_ay_semester($this->input->post());
				if($rs){
					$this->_the_downloadables->_generate_student_payment_record_ay_semester($rs,$this->input->post());
				}
			}
			
			if(!$rs)
			{
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">No record found.</div>');
				redirect(current_url());
			}
		}
	}
	
	public function student_deductions($page = 0)
	{
		$this->load->Model(array('M_enrollments','M_student_deduction_record'));
		
		$this->load->helper('my_dropdown');
		
		if($_POST){
			
			$year_id = $this->input->post('year_id');
			$course_id = $this->input->post('course_id');
			
			
			$param = array();
			if($year_id != ""){
				$param[' AND e.year_id = '] = $year_id;
				$param[' AND e.course_id = '] = $course_id;
			}
			
			$rs = $this->M_student_deduction_record->get_all_deduction(0,100, $param, true);
			
			if($rs){
				
					$data = false;
					
					foreach($rs as $obj)
					{
						$data[$obj->studid] = $this->M_student_deduction_record->get_student_deduction_byenrollment_id($obj->enrollment_id); //GET DEDUCTIONs
					}

					$this->load->library('../controllers/_the_downloadables');
					$this->_the_downloadables->_generate_student_deductions($rs, $data);					
				
			}else{
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">No record found.</div>');
				redirect(current_url());
			}
			
		}
	}
	
	public function download_all_student_profile()
	{
		$this->session_checker->check_if_alive(); // check if session is valid		
		
		$this->load->helper('my_dropdown');
			
		if($_POST)
		{
			$this->load->model('M_enrollments');
			
			$year_id = $this->input->post('year_id');
			
			$course_id = $this->input->post('course_id');
			
			$param = false;
			
			if($year_id != ""){
				$param['enrollments.year_id = '] = $year_id;
				$param['enrollments.course_id = '] = $course_id;
			}
			
			$rs = $this->M_enrollments->get_all_student_profile_for_report($param);
			if($rs)
			{
				$this->load->library('../controllers/_the_downloadables');// include downloadables controller
				$this->_the_downloadables->_print_all_student_profiles($rs);// calls print method on downloadables controller
			}else
			{
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">No record found.</div>');
				redirect(current_url());
			}
		}
	}
	
  public function collegiate_record($id = false)
	{
		if($id === false){ show_404(); }
		$this->check_student_profile_access('otr');
		$this->check_access_studentprofile_menu();

		$this->load->model(array("M_transcript","M_grades_file"));

		#load student library
		set_time_limit(0);
		$this->load->library('_student',array('enrollment_id'=>$id));
		$my_data['_student'] = $this->_student; if($this->_student->valid === false){ show_404(); }
		$my_data['transcript'] = $tr = $this->M_transcript->get_transcript($this->_student->profile->studid);
		$my_data['enrollment_id'] = $id;

		$this->load->library('../controllers/_the_downloadables');// include downloadables controller
		$this->_the_downloadables->_collegiate_record($my_data);// calls print method on downloadables controller
  }

	public function otr($id)
	{
		$this->load->library('../controllers/_the_downloadables');// include downloadables controller
		$this->_the_downloadables->_otr($id);// calls print method on downloadables controller
	}

	public function student_ledger($id = false)
	{
		if($id === false){ show_404(); }

		$this->menu_access_checker(array(
			'search/search_student',
			'enrollees/daily_enrollees',
			'search/master_list_by_course',
			'search/search_enrollee',
			'search/list_students/paid',
			'search/list_students/unpaid',
			'search/list_students/fullpaid',
			'search/list_students/partialpaid',
			'fees/view_fees',
		));

		set_time_limit(0);

		#load student library
		$this->load->library('_Student',array('enrollment_id'=>$id));
		$my_data['_student'] = $this->_student;
		$my_data['student_ledger'] = $ledger = $this->_student->ledger();

		$this->load->library('../controllers/_the_downloadables');// include downloadables controller
		$this->_the_downloadables->_student_ledger($my_data);// calls print method on downloadables controller
	}
}