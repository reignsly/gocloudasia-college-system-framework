<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Open_enrollment Extends MY_Controller
{
	public function __construct(){
		parent::__construct();
		$this->session_checker->open_semester();
		$this->load->helper(array('url_encrypt'));
		// $this->session_checker->secure_page('admin');
		$this->menu_access_checker();
	}
		

	public function index()
	{
		$this->load->model(array('M_open_semesters','M_enrollments'));
		//GET OPEN SEMESTER
		$this->view_data['open_enrollment'] = $open = $this->M_open_semesters->get_ay_sem();
		
		if($open)
		{
			if($_POST)
			{	
				

				//update
				$data['open_enrollment'] = isset($_POST['open_enrollment']) && $_POST['open_enrollment'] == 'on' ? 1 : 0;
				$status = isset($_POST['open_enrollment']) && $_POST['open_enrollment'] == 'on' ? 'open' : 'close';
				$data['updated_at'] = NOW;
				$rs = $this->M_open_semesters->update_open_semesters($data, $open->id);
				
				log_message('success','Open semester by: '.$this->user.'Success; Years Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Enrollment was successfully '.$status.'.</div>');
				redirect(current_url());
			}
		}
		else
		{
			// $this->session->set_flashdata('system_message', '<div class="alert alert-danger">Enrollment was successfully '.$status.'.</div>');
			// 	redirect(current_url());
		}
	}
	
}
