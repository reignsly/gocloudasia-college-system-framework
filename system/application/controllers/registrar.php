<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrar extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->load->helper(array('url_encrypt'));
	}
	
	public function index()
	{
		$this->session_checker->check_if_alive();
	}
	
	public function generate_student_profile_excel()
	{	
		$this->session_checker->check_if_alive(); // check if session is valid
		$this->load->helper(array('url_encrypt')); // load encryption helper
		$id = $this->input->get('id'); // receives get id id
		$di = $this->input->get('di'); // receives get id di
		$result = _sd($id,$di); //decrypts encrypted get variables
		// $this->_set_error_handler();// Catches errors and logs to system logs
		
		// if($result->status == 'TRUE')
		// {	
			$this->load->model('M_enrollments');
			$rs = $this->M_enrollments->get_all_student_profile_for_report();
			$this->load->library('../controllers/_the_downloadables');// include downloadables controller
			$this->_the_downloadables->_print_all_student_profiles($rs);// calls print method on downloadables controller
		// }else{
			// show_404();
		// }
	}
	
	
	public function subject_grades()
	{
		$this->load->model('m_subjects','m');
		$this->load->library('pagination');
		$config = $this->pagination_style();
    $config["base_url"] = base_url("registrar/subject_grades");
		$config["per_page"] = $p = 40;
    $config["uri_segment"] = 3;
    $config["num_links"] = 10;
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$new = $this->input->get('id');
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($this->input->post('search_subjects') == false)
		{	
			if($this->session->userdata('search_data') == false OR $new == 'main')
			{
				$this->session->set_userdata('search_data','');
				$config["total_rows"] = $t = $this->m->count_subjects_for_current_open_semester();
				$this->pagination->initialize($config);
				$this->view_data['system_message'] ='<div class="alert alert-success">Found <strong>'.htmlentities($t).'</strong> records</div>';
				$this->view_data['results'] = $this->m->search_student_subject(false,$p,$page);
			}else
			{
				$data_input = $this->session->userdata('search_data');
				$config["total_rows"] = $t = $this->m->count_subjects_for_current_open_semester($data_input);
				$this->pagination->initialize($config);
				$query_strings = implode(' ',array_values($data_input));
				$this->view_data['results'] = $this->m->search_student_subject($data_input,$p,$page);
				$this->view_data['system_message'] ='<div class="alert alert-success">Found <strong>'.htmlentities($t).'</strong> For <strong>'.htmlentities($query_strings).'</strong></div>';
			}
		}else{
			$this->session->set_userdata('search_data','');
			
			$data_input['code'] = trim(stripslashes(strip_tags($this->input->post('code'))));
			$data_input['sc_id'] = trim(stripslashes(strip_tags($this->input->post('sc_id'))));
			$data_input['subject'] = trim(stripslashes(strip_tags($this->input->post('subject'))));
			
			if($data_input['code'] !== '' OR $data_input['sc_id'] !== '' OR $data_input['subject'] !== '')
			{
				$config["total_rows"] = $t = $this->m->count_subjects_for_current_open_semester($data_input);
				if($t > 0)
				{
					$this->session->set_userdata('search_data',$data_input);
					$this->pagination->initialize($config);
				}
				$query_strings = implode(' ',array_values($data_input));
				$this->view_data['system_message'] ='<div class="alert alert-success">Found ('.htmlentities($t).') records For ('.htmlentities($query_strings).')</div>';
				$this->view_data['results'] = $this->m->search_student_subject($data_input,$p,$page);
			}
		}
		$this->view_data['links'] = $this->pagination->create_links();
	}
	
	public function view_subject_grades()
	{
		$id=$this->input->get('id');
		$di=$this->input->get('di');
		$result = _sd($id,$di);
		
		if($result->status == 'TRUE')
		{
			$this->load->library('pagination');
			$this->load->model('m_subjects','ms');
			$this->load->model('m_student_grades','mss');
			$this->view_data['subject_profile'] = $this->ms->get(false,array('*'),array('id'=>$result->link));
			$this->view_data['subject_student_grades'] = $p = $this->mss->get_student_grades_for_subject($result->link);
		}else{
			show_404();
		}
	}
	
	
	public function generate_grade_slip()
	{
	
	
	
	}
	
	// Logs all E_ERROR or fatal error  
	public function _set_error_handler()
	{		
		function myErrorHandler($code, $message, $file, $line) 
		{
			$ticket_number = substr(md5(rand(1000,9999).time()),0,15);
			log_message('error',$code.' | '.$message.' | '.$file.' | '.$line.' |error_number: '.$ticket_number);
			die('<h1>ERROR</h1><br><b>An Error Occured while processing request Contact system administrator , error log number </b><strong>('.$ticket_number.')</strong>');
		}
		function fatalErrorShutdownHandler()
		{
			$last_error = error_get_last();
			if ($last_error['type'] === E_ERROR) 
			{
				myErrorHandler(E_ERROR, $last_error['message'], $last_error['file'], $last_error['line']);
			}
		}	
		set_error_handler('myErrorHandler');
		register_shutdown_function('fatalErrorShutdownHandler');
	}
	
	public function dropped_student($id = false)
	{
		//CHECK DEPARTMENT STUDENT PROFILE ACCESS 
		$this->check_stud_access('stud_profile');

		if($id == false) { show_404(); } 
		$data['updated_at'] = NOW;
		$data['is_deleted'] = 1;
		$data['is_drop'] = 1;
		$this->load->model(array('M_enrollments', 'M_student_total_file'));
		
		$rs = $this->M_enrollments->update($id, $data);
		
		if($rs)
		{
			log_message('success','Enrollment Deleted/Dropped by: '.$this->user.'Success; Enrollment Id: '.$id);
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">Student successfully dropped/deleted.</div>');
			redirect('search/search_student');
		}
		
	}
}
