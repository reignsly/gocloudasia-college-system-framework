<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fixes extends CI_Controller {
	
  public function __construct()
	{
		parent::__construct();
	}
	
	public function fix_users_table()
	{
		//FIX THE USERS : UPDATE DEPARTMENT FIELD
		//Should be remove
		$this->load->model('M_users');
		$rs = $this->M_users->fix_users_table();
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($rs)
		{
			// $this->session->set_flashdata('system_message', '<div class="alert alert-success">Table successfully updated.</div>');
			echo "Fix Successful.";
		}
		else
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">No affected rows.</div>');
		}
	}
}
