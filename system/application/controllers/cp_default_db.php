<?php
class Cp_default_db Extends Dev_Controller
{
	public function __construct(){
		parent::__construct();
		$this->check_if_logged_in();
		$this->load->model(array(
			'M_departments',
			'M_main_menus',
			'M_menus',
			'M_menus_method',
			'M_main_menus_method'));
	}

	/**
	*show a list of menus 
	*/
	public function create_def_structure($action = false)
	{
		if($action === "generate"){

			$this->load->helper('default_db_helper');
			$sql = get_db_default_sql();
			$this->M_core_model->run_sql($sql);
		}
	}
}
