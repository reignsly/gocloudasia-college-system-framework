<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Issues extends MY_Controller {
  
	public function __construct(){
		parent::__construct();
		$this->session_checker->open_semester();
		$this->load->helper(array('url_encrypt'));
		$this->load->model(array('M_issues'));
		
		$this->menu_access_checker(array(
			'search/search_student',
			'enrollees/daily_enrollees',
			'search/master_list_by_course',
			'search/search_enrollee',
			'search/list_students/paid',
			'search/list_students/unpaid',
			'search/list_students/fullpaid',
			'search/list_students/partialpaid',
		));
	}
	
	public function view($id = false, $search_type = "All")
	{
		$this->load->helper('random_string');
	  //CHECK DEPARTMENT STUDENT PROFILE ACCESS 
		$this->check_stud_access('stud_issues');
	  
	  if(!$id) { show_404(); } 
	  
	  $this->load->model(array("M_issues", "M_enrollments"));
	  
	  //Top header data of student
		$this->view_data['student'] = $p = $this->M_enrollments->profile($id);
		$this->view_data['enrollment_id'] = $id;
		$user_id = $p->user_id;
		
	  $this->view_data['issues'] = $issues =  $this->M_issues->get_all_by_user_id($user_id, "No");
	  $this->view_data['res_issues'] = $issues =  $this->M_issues->get_all_by_user_id($user_id, "Yes");

	  #load student library
		set_time_limit(0);
		$this->load->library('_student',array('enrollment_id'=>$id));
		$this->view_data['_student'] = $this->_student;
	  
	  
	  // Count Issues
	  $this->issue_count($p->user_id);

	  //Top Search Parameter
		$this->view_data['search_type'] = $search_type;
	}
	
	public function view_issues($id, $user_id) //FOR CUSTODIAN
	{
	  
	  $this->load->model(array("M_issues", "M_enrollments"));
	  
	  //Top header data of student
		$this->view_data['student'] = $p = $this->M_enrollments->profile($id);
		$this->view_data['enrollment_id'] = $id;
		
	  $this->view_data['issues'] = $issues =  $this->M_issues->get_all_by_user_id($user_id);
	  
	  
	  // Count Issues
	  $this->issue_count($p->user_id);
	}
	
	// Create
	public function create($enrollment_id, $user_id)
	{

		//CHECK DEPARTMENT STUDENT PROFILE ACCESS 
		$this->check_stud_access('stud_add_issues');
		
		$this->view_data['issue'] = FALSE;
		$this->view_data['user_id'] = $user_id;
		$this->view_data['enrollment_id'] = $enrollment_id;
		
		
		if($_POST)
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('issue_comment', 'Comment', 'required');
			$this->form_validation->set_rules('issue_resolved', 'Issue Resolved', 'required');
			
			if ($this->form_validation->run() !== FALSE)
			{

				$data = $this->input->post('issues');
				$data['comment'] = $this->input->post('issue_comment');
				$data['resolved'] = $this->input->post('issue_resolved');
				$data['enrollment_id'] = $enrollment_id;
				$data['user_id'] = $user_id;
				$data['issued_by'] = strtoupper($this->session->userdata['userType']);
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['updated_at'] = date('Y-m-d H:i:s');

				if($data['resolved'] === "Yes"){
					$data['resolved_id'] = $this->userid;
					$data['resolved_date'] = NOW;
					$data['resolved_remark'] = $this->input->post('resolve_remark');
				}
				
				$result = $this->M_issues->create_issues($data);
				if($result['status'])
				{
					$id = $result['id'];
					activity_log('Create Issue', $this->userlogin, "Issue ID : ".$id);
					log_message('error','Issue Created by: '.$this->user.'Success; User Id: '.$id);
					$this->session->set_flashdata('system_message', '<div class="alert alert-success">Issue successfully added.</div>');
					redirect(current_url());
				}
			}
		}
	}
	
	
	// Update
	public function edit($id, $enrollment_id)
	{
		//CHECK DEPARTMENT STUDENT PROFILE ACCESS 
		$this->check_stud_access('stud_add_issues');

		$this->view_data['issues'] = $issue =  $this->M_issues->get_issues($id);
		$this->view_data['user_id'] = $issue->user_id;
		$this->view_data['enrollment_id'] = $enrollment_id;
		
		if($_POST)
		{
			$data = $this->input->post('issues');
			$data['resolved'] = $_POST['issue_resolved'];
			$data['issued_by'] = strtoupper($this->session->userdata['userType']);
			$data['updated_at'] = date('Y-m-d H:i:s');

			$this->load->library('form_validation');
			$this->form_validation->set_rules('issue_comment', 'Comment', 'required');
			$this->form_validation->set_rules('issue_resolved', 'Issue Resolved', 'required');
			
			if ($this->form_validation->run() !== FALSE)
			{
				if($data['resolved'] === "Yes")
				{
					$data['resolved_id'] = $this->userid;
					$data['resolved_date'] = NOW;
					$data['resolved_remark'] = $this->input->post('resolve_remark');
				}else{
					$data['resolved_id'] = 0;
					$data['resolved_date'] = NULL;
					$data['resolved_remark'] = "";
				}
				
				$result = $this->M_issues->update_issues($data, $id);
				
				if($result['status'])
				{	
					activity_log('Update Issue', $this->userlogin, "Issue ID : ".$id);
					log_message('error','issues Updated by: '.$this->user.'Success; issues Id: '.$id);
					$this->session->set_flashdata('system_message', '<div class="alert alert-success">Issue successfully updated.</div>');
					redirect(current_url());
				}
			}
		}
	}
	
	// Update
	public function destroy($id, $enrollment_id)
	{
		//CHECK DEPARTMENT STUDENT PROFILE ACCESS 
		$this->check_stud_access('stud_add_issues');

		$issue =  $this->M_issues->get_issues($id);
		$user_id = $issue->user_id;
		$result = $this->M_issues->delete_issues($id);
		if($issue)
		{
			activity_log('Delete Issue', $this->userlogin, "Issue ID : ".$id."User ID : ".$issue->user_id);
			log_message('error','issues Deleted by: '.$this->user.'Success; issues Id: '.$id);
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">Issue successfully deleted.</div>');
			redirect('issues/view/'.$enrollment_id.'/'.$user_id);
		}
	}
}
