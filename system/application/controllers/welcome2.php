<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		//$aliveandkickin = $this->session_checker->check_if_alive();
		//if($aliveandkickin)
		//{
			//redirect($this->ci->session->userdata('userType'));
		//}
	}

	public function index()
	{
		$this->check_if_already_logged_in();

		$this->load->model('M_departments');
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$dep = $this->M_departments->get_all_department_for_welcome_screen(1);
		
		$this->view_data['levels'] = $level_dep = $this->M_departments->get_departments_group_by_levels(1);
		
		if($dep == false){
			echo "The system 'departments' was not set properly please contact your provider.";
			die();
		}
		
		//ARRANGE DEPARTMENT BY LEVELS PUT TO ARRAY WITH LEVEL AS INDEX
		$departments = array();
		foreach($level_dep as $obj){
			foreach($dep as $obj_dep){
				
				if($obj->level == $obj_dep->level){	
					$departments[$obj->level][] = $obj_dep;
				}
			}
			
		}
		
		$this->view_data['departments'] = $departments;
		
		// vd($departments);
		
		$this->layout_view = $this->welcome_view;
		
		
		
	}

	public function index2()
	{
		$this->load->model('M_departments');
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$dep = $this->M_departments->get_all_department_for_welcome_screen(1);
		
		$this->view_data['levels'] = $level_dep = $this->M_departments->get_departments_group_by_levels(1);
		
		if($dep == false){
			echo "The system 'departments' was not set properly please contact your provider.";
			die();
		}
		
		//ARRANGE DEPARTMENT BY LEVELS PUT TO ARRAY WITH LEVEL AS INDEX
		$departments = array();
		foreach($level_dep as $obj){
			foreach($dep as $obj_dep){
				
				if($obj->level == $obj_dep->level){	
					$departments[$obj->level][] = $obj_dep;
				}
			}
			
		}
		
		$this->view_data['departments'] = $departments;
		
		// vd($departments);
		
		$this->layout_view = 'welcome';
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
