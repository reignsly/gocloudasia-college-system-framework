<?php
class Packages Extends Backend_Controller
{
	public function __construct(){
		parent::__construct();
		$this->userlogin = "CPanel";

		$this->load->model(array(
				'M_packages',
				'M_departments',
				'M_package_departments',
				'M_package_menus',
				'M_menus',
				'M_main_menus',
			));

		//LOAD LIBRARY
		$this->load->library('form_validation');
	}

	/**
	 * System Packages
	 */
	public function index()
	{
		$this->view_data['custom_title'] = "All";

		$get = false;
		$get['order'] = 'level';
		$this->view_data['packages'] = $this->M_packages->get_record(false, $get);
	}

	/**
	 * Create New Package
	 */
	public function create()
	{
		if($this->input->post('create_package')){

			$package = $this->input->post('package');
			if($this->form_validation->run('packages') === TRUE){
				$rs_add = (object)$this->M_packages->insert($package);
				if($rs_add->status){
					$this->_msg('s','Package successfully created.', current_url());
				}else{
					$this->_msg('e','Transaction failed. Please try again.', current_url());
				}
			}
		}
	}

	/**
	 * Clone or Copy Package
	 * @param int $p_id Package ID of Source
	 */
	public function clone_package($p_id = false)
	{
		$this->view_data['package_id'] = $p_id;
		
		unset($get);
		$get['order'] = 'level';
		$this->view_data['packages'] = $p = $this->M_packages->get_record(false, $get);
		$this->view_data['package'] = $this->M_packages->get($p_id);
		$this->view_data['departments'] = $rs_d = $this->M_package_departments->get_departments($p_id); 

		if($this->input->post('clone_package')){
			
			$input_package = $this->input->post('package');
			$clone_department = $this->input->post('clone_department');
			// vd($clone_department);
			if($this->form_validation->run('packages') === TRUE){
				$rs_add = (object)$this->M_packages->insert($input_package);
				if($rs_add->status){

					// clone department
					if($clone_department){
						foreach ($clone_department as $k => $p_dep_id) {
							$pack_dep = $this->M_package_departments->get($p_dep_id);
							unset($data);
							$data['package_id'] = $rs_add->id;
							$data['department_id'] = $pack_dep->department_id;
							$data['stud_profile'] = $pack_dep->stud_profile;
							$data['stud_fees'] = $pack_dep->stud_fees;
							$data['stud_grade'] = $pack_dep->stud_grade;
							$data['stud_issues'] = $pack_dep->stud_issues;
							$data['stud_otr'] = $pack_dep->stud_otr;

							$data['stud_edit_profile'] = $pack_dep->stud_profile;
							$data['stud_add_fees'] = $pack_dep->stud_fees;
							$data['stud_edit_grade'] = $pack_dep->stud_grade;
							$data['stud_add_issues'] = $pack_dep->stud_issues;
							$data['stud_edit_subject'] = $pack_dep->stud_edit_subject;

							$rs_add_dep = (object)$this->M_package_departments->insert($data);
							if($rs_add_dep){

								//clone menus
								$package_menus = $this->M_package_menus->get_package_menus($p_id,$pack_dep->department_id, true);
								if($package_menus){
									foreach ($package_menus as $pm_k => $pm) {
										unset($data);
										$data = $pm;
										unset($data['id']);
										unset($data['created_at']);
										unset($data['updated_at']);
										$data['package_id'] = $rs_add->id;
										$rs_clone_menu = $this->M_package_menus->insert($data);
									}
								}
							}
						}
					}

					$this->_msg('s','Package successfully cloned.', 'packages');
				}else{
					$this->_msg('e','Transaction failed. Please try again.', current_url());
				}
			}
		}
	}

	/**
	 * Package Profile - department, update department, delete
	 */
	public function profile($tab = 'edit', $p_id = false, $department_id = false, $sel_dep_id = false)
	{
		if($p_id === false){ show_404(); }
		$this->view_data['custom_title'] = ucwords($tab);
		$this->view_data['p_id'] = $p_id;
		$this->view_data['department_id'] = $department_id ? $department_id : 0;
		$this->view_data['tab'] = $tab; // CONTINUE HERE 
		$this->view_data['package'] = $p = $this->M_packages->get($p_id); if($p === false){ show_404(); }
		$this->view_data['department']  = $this->M_departments->pull($department_id, 'id,department, description');
		$this->view_data['selected_department']  = $d = $this->M_departments->pull($sel_dep_id, 'id,department, description');
		$this->view_data['departments'] = $rs_d = $this->M_package_departments->get_departments($p_id);
		$this->view_data['department_menus'] = $d_m = $this->M_package_menus->get_department_menus($p_id, $department_id);
		
		//for add department
		$this->view_data['departments_list'] = $dl = $this->M_departments->get_available_department_except($p_id);
		// vd($this->db->last_query());
		//for add menu
		$this->view_data['departments_dd'] = $d_dd = $this->M_package_departments->get_departments_dd($p_id); // for dropdown
		$this->view_data['menugroup_dd'] = $mg_dd = $department_id ? $this->M_package_menus->get_menugroup_dd($p_id, $department_id) : false; // for dropdown
		$this->view_data['menu_list'] = $m_l = $this->M_main_menus->get_menu_list_except_package_dd($p_id, $sel_dep_id); // for dropdown

		/** Update Package Info*/
			if($this->input->post('update_package')){
				//LOAD LIBRARY
				$this->load->library('form_validation');

				$package = $this->input->post('package');
				if($this->form_validation->run('packages') === TRUE){
					$rs_upd = $this->M_packages->update($p_id, $package);
					if($rs_upd){
						$this->_msg('s','Package successfully updated.', current_url());
					}else{
						$this->_msg('e','Transaction failed. Please try again.', current_url());
					}
				}
			}
		/** Add Departments*/
			if($this->input->post('add_department')){
				$check_depa = $this->input->post('add_dep');
				if($check_depa){
					$ctr = 0;
					foreach ($check_depa as $k => $x_d_id) {
						$rs_dep = $this->M_departments->pull($x_d_id, 'id,department');
						if($rs_dep){
							unset($p_data);
							$p_data['package_id'] = $p_id;
							$p_data['department_id'] = $rs_dep->id;
							$rs = (object)$this->M_package_departments->insert($p_data);
							if($rs->status){ $ctr++; }
						}
					}
					if($ctr>0){
						$this->_msg('s', 'Department/s successfully added', 'packages/profile/departments/'.$p_id);
					}else{
						$this->_msg('e', 'Transaction failed, please try again', 'packages/profile/departments/'.$p_id);
					}
				}
			}
		
		/** Update Department Student Profile Access*/
			if($this->input->post('update_stud_access')){
				
				$pd_id = $this->input->post('pd_dep_id');
				if($pd_id){
					$data['stud_profile'] = $stud_profile = isset($_POST['stud_profile']) ? 1 : 0;
					$data['stud_grade'] = $stud_grade = isset($_POST['stud_grade']) ? 1 : 0;
					$data['stud_fees'] = $stud_fees = isset($_POST['stud_fees']) ? 1 : 0;
					$data['stud_otr'] = $stud_otr = isset($_POST['stud_otr']) ? 1 : 0;
					$data['stud_issues'] = $stud_issues = isset($_POST['stud_issues']) ? 1 : 0;

					$data['stud_edit_profile'] = $stud_profile = isset($_POST['stud_edit_profile']) ? 1 : 0;
					$data['stud_edit_grade'] = $stud_grade = isset($_POST['stud_edit_grade']) ? 1 : 0;
					$data['stud_add_fees'] = $stud_fees = isset($_POST['stud_add_fees']) ? 1 : 0;
					$data['stud_add_issues'] = $stud_issues = isset($_POST['stud_add_issues']) ? 1 : 0;
					$data['stud_subject'] = $stud_issues = isset($_POST['stud_subject']) ? 1 : 0;
					$data['stud_edit_subject'] = $stud_issues = isset($_POST['stud_edit_subject']) ? 1 : 0;
					$data['stud_portal_activation'] = $stud_portal_activation = isset($_POST['stud_portal_activation']) ? 1 : 0;
					
					$result = $this->M_package_departments->update($pd_id, $data);
					if($result>0){
						$this->_msg('s', 'Department student access successfully updated', 'packages/profile/departments/'.$p_id);
					}else{
						$this->_msg('e', 'Transaction failed, please try again', 'packages/profile/departments/'.$p_id);
					}
				}
			}

		/** Add Menu*/
			if($this->input->post('add_menu')){
				// vd($_POST);

				if($this->form_validation->run('packages_add_menu') === TRUE){

					$menu_group_type = $this->input->post('menu_group_type');					
					$menu_group_id = $this->input->post('menugroup_id');
					$dep = $this->M_departments->get($this->input->post('department_id'));
					$xmen_sub = false;
					$xctr = 0;
					#STEP 1 : Check if Menu Group is Selected or Created
						// IF selected 
						if($menu_group_type === "selection"){
							$sel_menu_group = $this->M_package_menus->get($menu_group_id);
							if($sel_menu_group){

								$xmen_sub = $sel_menu_group->menu_sub;
							}
						}else{ // if created
							$new_head_caption = $this->input->post('menu_group', true);
							$h_data['department_id'] = $dep_id = $this->input->post('department_id');
							$h_data['package_id'] = $p_id;
							$h_data['caption'] = $new_head_caption;
							$h_data['controller'] = '#';
							$h_data['menu_lvl'] = 1;
							$rs = (object)$this->M_package_menus->insert($h_data);
							if($rs->status)
							{
								$insert_id = $rs->id;
								unset($data);
								$data['menu_num'] = $insert_id;		
								$data['menu_grp'] = $insert_id;		
								$data['menu_sub'] = $insert_id.'_SUB';
								$data['department'] = $dep->department;
								$data['menu_icon'] = "glyphicon glyphicon-globe";
								$this->M_package_menus->update($insert_id, $data);
							
								$xmen_sub = $data['menu_sub'];
							}
						}
					
					#STEP 2 : With the Sub Menu, Loop the Menu Selected and Add to package_menus Table
						if($xmen_sub){
							$menulist = $this->input->post('menu_list');
							if($menulist){
								foreach ($menulist as $k => $menu_id) {
									$rs_menu = $this->M_main_menus->get($menu_id);
									if($rs_menu){

										unset($data);
										$data['package_id'] = $p_id;
										$data['department_id'] = $dep->id;
										$data['department'] = $dep->department;
										$data['menu_grp'] = $xmen_sub;
										$data['main_menu_id'] = $menu_id;
										$data['controller'] = $rs_menu->controller;
										$data['caption'] = $rs_menu->caption;
										$data['menu_lvl'] = $rs_menu->menu_lvl;
										$data['menu_num'] = $rs_menu->menu_num;
										$data['menu_icon'] = "glyphicon glyphicon-globe";
										$data['visible'] = $rs_menu->visible;
										$data['attr'] = $rs_menu->attr;
										
										$rs = (object)$this->M_package_menus->insert($data);
										
										if($rs->status)
										{
											$xctr++;
											activity_log('Add New Package Menu', $this->userlogin,' Package_menu_id : '.$rs->id);
										}
									}
								}
							}
						}
					
					if($xctr>0){
						$this->_msg('s','Menu/s successfully added.','packages/profile/add_menu/'.$p_id.'/'.$department_id);
					}else{
						$this->_msg('e','Transaction failed, please try again.','packages/profile/add_menu/'.$p_id.'/'.$department_id);
					}
				}
			}
		
		/** Update to Show (visible = 1) selected menu */
			if($this->input->post('show_selected_menu')){
				$selected = $this->input->post('menu_select');

				if($selected){
					$ctr = 0;
					foreach ($selected as $k => $pm_id) {
						unset($data);
						$data['visible'] = 1;
						if($this->M_package_menus->update($pm_id, $data)){ $ctr++; }
					}
					if($ctr > 0){
						$this->_msg('s','Successfully updated menu to visible', "packages/profile/$tab/$p_id/$department_id");
					}else{
						$this->_msg('e','Transaction failed, please try again', "packages/profile/$tab/$p_id/$department_id");
					}
				}
			}
		/** Update to Invisible (visible = 0) selected menu */
			if($this->input->post('hide_selected_menu')){
				// vd($_POST);
				$selected = $this->input->post('menu_select');
				if($selected){
					$ctr = 0;
					foreach ($selected as $k => $pm_id) {
						unset($data);
						$data['visible'] = 0;
						if($this->M_package_menus->update($pm_id, $data)){ $ctr++; }
					}
					if($ctr > 0){
						$this->_msg('s','Successfully updated menu to visible', "packages/profile/$tab/$p_id/$department_id");
					}else{
						$this->_msg('e','Transaction failed, please try again', "packages/profile/$tab/$p_id/$department_id");
					}
				}
			}
		/** Delete selected menu */
			if($this->input->post('del_selected_menu')){
				$selected = $this->input->post('menu_select');
				if($selected){
					$ctr = 0;
					foreach ($selected as $k => $pm_id) {
						if($this->M_package_menus->delete($pm_id)){ $ctr++; }
					}
					if($ctr > 0){
						$this->_msg('s','Menu/s successfully removed.', "packages/profile/$tab/$p_id/$department_id");
					}else{
						$this->_msg('e','Transaction failed, please try again', "packages/profile/$tab/$p_id/$department_id");
					}
				}
			}

		/** Update to Show (visible = 1) selected menu group */
			if($this->input->post('show_selected_menu_group')){
				$selected = $this->input->post('select_menu_group');

				if($selected){
					$ctr = 0;
					foreach ($selected as $k => $pm_id) {
						//Update the Menu Group
						$rs_mg = $this->M_package_menus->pull($pm_id, 'menu_sub,package_id,department_id');
						if($rs_mg){
							unset($data);
							$data['visible'] = 1;
							if($this->M_package_menus->update($pm_id, $data)){ $ctr++; }

							//Update Menu Group Sub-menu
							unset($where);
							$where['package_id'] = $rs_mg->package_id;
							$where['department_id'] = $rs_mg->department_id;
							$where['menu_grp'] = $rs_mg->menu_sub;
							$rs = $this->M_package_menus->update($where, $data);
							if($this->M_package_menus->update($where, $data)){ 
								$ctr++; 
							}
						}
					}
					if($ctr > 0){
						$this->_msg('s','Successfully updated menu group/s to visible', "packages/profile/$tab/$p_id/$department_id");
					}else{
						$this->_msg('e','Transaction failed, please try again', "packages/profile/$tab/$p_id/$department_id");
					}
				}
			}
		/** Hide (visible = 0) selected menu group */
			if($this->input->post('hide_selected_menu_group')){
				$selected = $this->input->post('select_menu_group');

				if($selected){
					$ctr = 0;
					foreach ($selected as $k => $pm_id) {
						//Update the Menu Group
						$rs_mg = $this->M_package_menus->pull($pm_id, 'menu_sub,package_id,department_id');
						if($rs_mg){
							unset($data);
							$data['visible'] =0;
							if($this->M_package_menus->update($pm_id, $data)){ $ctr++; }

							//Update Menu Group Sub-menu
							unset($where);
							$where['package_id'] = $rs_mg->package_id;
							$where['department_id'] = $rs_mg->department_id;
							$where['menu_grp'] = $rs_mg->menu_sub;
							$rs = $this->M_package_menus->update($where, $data);
							if($this->M_package_menus->update($where, $data)){ 
								$ctr++; 
							}
						}
					}
					if($ctr > 0){
						$this->_msg('s','Successfully hide menu group/s.', "packages/profile/$tab/$p_id/$department_id");
					}else{
						$this->_msg('e','Transaction failed, please try again', "packages/profile/$tab/$p_id/$department_id");
					}
				}
			}
		/** Delete selected menu menu group */
			if($this->input->post('del_selected_menu_group')){
				$selected = $this->input->post('select_menu_group');

				if($selected){
					$ctr = 0;
					foreach ($selected as $k => $pm_id) {
						$rs_mg = $this->M_package_menus->pull($pm_id, 'menu_sub,package_id,department_id');
						if($rs_mg){
							unset($where);
							$where['package_id'] = $rs_mg->package_id;
							$where['department_id'] = $rs_mg->department_id;
							$where['menu_grp'] = $rs_mg->menu_sub;
							$rs = $this->M_package_menus->update($where, $data);
							if($this->M_package_menus->delete($where)){ $ctr++; } // Remove Sub Menu
							if($this->M_package_menus->delete($pm_id)){ $ctr++; } // Remove Menu Group
						}
					}
					if($ctr > 0){
						$this->_msg('s','Menu Group successfully deleted.', "packages/profile/$tab/$p_id/$department_id");
					}else{
						$this->_msg('e','Transaction failed, please try again', "packages/profile/$tab/$p_id/$department_id");
					}
				}
			}

		/** Update Sub-menu menu_num or order */
			if($this->input->post('save_submenu_num')){
				$selected = $this->input->post('sub_menu_num');

				if($selected){
					$ctr = 0;
					foreach ($selected as $k_id => $menu_num) {
						//Update the Menu
						$rs_mg = $this->M_package_menus->pull($k_id, 'id');
						if($rs_mg){
							unset($data);
							$data['menu_num'] = $menu_num;
							$rs = $this->M_package_menus->update($k_id, $data);
							if($rs){
								$ctr++;
							}
						}
					}
					if($ctr > 0){
						$this->_msg('s','Menu Order Successfully Saved.', "packages/profile/$tab/$p_id/$department_id");
					}else{
						$this->_msg('e','Transaction failed, please try again', "packages/profile/$tab/$p_id/$department_id");
					}
				}
			}

		/** Update Menu Group*/
			if($this->input->post('update_header_menu')){
				$menu_group = $this->input->post('head_menu');
				if($menu_group){
					$ctr = 0;
					foreach ($menu_group as $menu_group_id => $data) {
						$rs = $this->M_package_menus->update($menu_group_id, $data);
						if($rs){ 
							$ctr++; 
						}
					}
					if($ctr > 0){
						$this->_msg('s','Successfully updated menu group', "packages/profile/$tab/$p_id/$department_id");
					}else{
						$this->_msg('e','Transaction failed, please try again', "packages/profile/$tab/$p_id/$department_id");
					}
				}
			}
	}

	/**
	 * Destroy Package Department
	 * @param int $p_id Package ID
	 * @param int $d_id Department ID
	 */
	public function destroy_package_department($p_id = false, $d_id = false)
	{
		if($p_id === false){ show_404(); }
		if($d_id === false){ show_404(); }

		$package = $this->M_packages->pull($p_id, 'id, package'); if($package === false){ show_404(); }
		$dep = $this->M_package_departments->pull($d_id, 'id, department_id'); if($package === false){ show_404(); }

		//destroy menus
		$this->M_package_menus->delete(array(
				'package_id' => $p_id,
				'department_id' => $dep->department_id,
			));

		// destroy package
		if($this->M_package_departments->delete($d_id)){
			$this->_msg('s','Package Department was successfully removed and all of its content.', 'packages/profile/departments/'.$p_id);
		}else{
			$this->_msg('s','Transaction failed, please try again', 'packages/profile/departments/'.$p_id);
		}
	}

	/**
	 * Delete Package and all its department and menus
	 * @param int $p_id Package ID
	 */
	public function destroy($p_id = false)
	{
		$package = $this->M_packages->pull($p_id, 'id'); if($package === false){ show_404(); }

		//remove departments
		$this->M_package_departments->delete(array('package_id'=>$p_id));

		// remove Menus
		$this->M_package_menus->delete(array('package_id'=>$p_id));

		// remove package

		$rs = $this->M_packages->delete($p_id);

		if($rs){
			$this->_msg('s','Package was successfully removed', 'packages');
		}else{
			$this->_msg('e','Transaction failed, please try again', 'packages');
		}
	}

	/**
	 * Change Current Package of the System
	 */
	public function set()
	{
		$get = array('where'=>array("department <> "=> 'hrd'),'order' => 'department' );
		$this->view_data['departments'] = $this->M_departments->get_record(false, $get);
		$this->view_data['is_dep_set'] = function($p_id, $d_id){ return $this->M_package_departments->has_department($p_id, $d_id); };

		$get = false;
		$get['order'] = 'level';
		$this->view_data['packages'] = $this->M_packages->get_record(false, $get);
	}

	/**
	 * Change current Package
	 * @param int $p_id Package ID
	 */
	public function change_package($p_id = false)
	{
		/*
			Logic 
			 - the system uses table 'departments' and 'menus' for its available function 
			 - to change plan , just copy and replace all menus under package_menus and alter visibiliy in departments using package departments
			 - update package is_set
		*/

		set_time_limit(0);
		$package = $this->M_packages->get($p_id); if($package === false){ show_404(); }
		$main_departments = $this->M_departments->fetch_all();
		
		#step 1 : update departments based on package_departments
		if($main_departments){
			foreach ($main_departments as $l => $d) {
				$has_dep = $this->M_package_departments->has_department($p_id, $d->id, true);
				// vd($has_dep);
				if($has_dep){
					unset($data);
					$data['visible'] = 1;
					$data['stud_profile'] = $has_dep->stud_profile;
					$data['stud_fees'] = $has_dep->stud_fees;
					$data['stud_grade'] = $has_dep->stud_grade;
					$data['stud_issues'] = $has_dep->stud_issues;
					$data['stud_otr'] = $has_dep->stud_otr;

					$data['stud_edit_profile'] = $has_dep->stud_edit_profile;
					$data['stud_add_fees'] = $has_dep->stud_add_fees;
					$data['stud_edit_grade'] = $has_dep->stud_edit_grade;
					$data['stud_add_issues'] = $has_dep->stud_add_issues;
					$data['stud_edit_subject'] = $has_dep->stud_edit_subject;
						
				}else{
					unset($data);
					$data['visible'] = 0;
					$data['stud_profile'] = 0;
					$data['stud_fees'] = 0;
					$data['stud_grade'] = 0;
					$data['stud_issues'] = 0;
					$data['stud_otr'] = 0;

					$data['stud_edit_profile'] = 0;
					$data['stud_add_fees'] = 0;
					$data['stud_edit_grade'] = 0;
					$data['stud_add_issues'] = 0;
					$data['stud_edit_subject'] = 0;
				}

				$rs = $this->M_departments->update($d->id, $data);
			}
		}

		#Step 2 : Truncate menus and copy package_menus
		$this->load->model('M_menus');
		$this->M_menus->copy_menus_from_package($p_id);

		#step 3: Update to Current Package
		unset($data);
		$data['is_set'] = 0;
		$this->M_packages->update(false, $data);

		unset($data);
		$data['is_set'] = 1;
		$rs = $this->M_packages->update($p_id, $data);

		if($rs){
			$this->_msg('s','Current package was successfully change', "packages/set");
		}else{
			$this->_msg('e','Transaction failed, please try again.', "packages/set");
		}
	}

	/**
	 * Print Package Summary
	 */
	public function print_summary()
	{
		set_time_limit(0);
		
		$this->view_data['packages'] = $this->M_packages->get_record(false, array('order'=>'level'));
		$this->view_data['get_package_department'] = function($p_id){ return $this->M_package_departments->get_departments_string($p_id); }; 

		#load student library
		$this->load->library('../controllers/pdf_cpanel');
		$this->pdf_cpanel->package_summary_report($this->view_data);
	}

	public function print_detailed($p_id = false)
	{
		set_time_limit(0);

		if($p_id){
			$get['where']['id'] = $p_id;
		}
		$get['order'] = 'level';
		$p = $this->M_packages->get_record(false, $get);

		if($p_id){
			$this->view_data['package'] = $p?$p[0]:false;
		}

		//get package departments
		$pd = false;
		if($p){
			foreach ($p as $k => $v) {
				$pd[$v->id] = $this->M_packages->get_profile($v->id);
			}
		}
		
		$this->view_data['packages'] = $pd;
		#load student library
		$this->load->library('../controllers/pdf_cpanel');
		$this->pdf_cpanel->package_detailed_report($this->view_data);
	}
}
