<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_subjects extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->menu_access_checker();
		$this->settings 	= $this->M_settings->get_settings();

		$this->load->model(array(
			'M_subjects',
			'M_student_subjects',
			'M_grading_periods',
			'M_grades_file',
			'M_grading_system'
		));

		$this->load->helper(array('panel','grade'));
	}

	public function index($page = 0)
	{
		$like = false;
		$filter = false;

		$filter['subjects.year_from'] = $this->cos->user->year_from;
		$filter['subjects.year_to'] = $this->cos->user->year_to;
		$filter['subjects.semester_id'] = $this->cos->user->semester_id;
		$filter['subjects.teacher_user_id'] = $this->userid;
		
		if($_GET)
		{
			if(isset($_GET['sc_id']) && trim($_GET['sc_id']) != ""){
				$this->view_data['sc_id'] = $sc_id = trim($_GET['sc_id']);
				$like['subjects.sc_id'] = $sc_id;
			}
			
			if(isset($_GET['code']) && trim($_GET['code'])){
				$this->view_data['code'] = $code = trim($_GET['code']);
				$like['subjects.code'] = $code;
			}
			
			if(isset($_GET['subject']) && trim($_GET['subject'])){
				$this->view_data['subject'] = $subject = trim($_GET['subject']);
				$like['subjects.subject'] = $subject;
			}
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'subjects.id',
				'subjects.ref_id',
				'master_subjects.code',
				'master_subjects.subject',
				'master_subjects.units',
				'master_subjects.lec',
				'master_subjects.lab',
				'subjects.time',
				'subjects.day',
				'subjects.room_id',
				'subjects.subject_load',
				'subjects.subject_taken',
				'subjects.original_load',
				'subjects.year_to',
				'subjects.year_from',
				'subjects.teacher_user_id',
				'rooms.name AS room',
				'users.name AS instructor' 
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			0 => array(
				"table" => "master_subjects",
				"on"	=> "master_subjects.ref_id = subjects.ref_id",
				"type"  => "LEFT"
			),
			1 => array(
				"table" => "rooms",
				"on"	=> "rooms.id = subjects.room_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "users",
				"on"	=> "users.id = subjects.teacher_user_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "master_subjects.subject";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."my_subjects/index";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$this->view_data['get_url'] = $config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("subjects", $get);
		
		$config["per_page"] = 20;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		// $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['results'] = $search = $this->M_core_model->get_record("subjects", $get);
		$this->view_data['links'] = $this->pagination->create_links();	
		$this->view_data['custom_title'] = $config["total_rows"].' subjects';
	}

	public function grading($subject_ref_id = '', $action = false)
	{
		//check student grade access
		if($this->departments->stud_grade !== "1"){
			show_error("You don't have an access to this page");
		}

		$this->view_data['subject'] = $sub = $this->M_subjects->get_profile_ref_id($subject_ref_id); if($sub === false){ show_404(); }

		// check if subject is under the teacher
		if(!$this->M_subjects->check_subject_under_teacher($this->userid, $sub->id)){ show_404(); }

		//Grading System
		$this->view_data['c_grading_period'] = $cgp = $this->current_grading_period;
		$this->view_data['is_final_gp'] = $ifg = $this->M_grading_periods->is_current_last_gp();
		$this->view_data['grading_system'] = $gs = isset($this->syspar->grading_system) && $this->syspar->grading_system ? $this->syspar->grading_system : 'input';
		$this->view_data['grading_table'] = $this->M_grading_system->get_system();
		$this->view_data['max_grade'] = $max = $this->M_grading_system->maximum_score();
		$this->view_data['min_grade'] = $min = $this->M_grading_system->minimum_score();

		$subject_id = $sub->id;
		$this->view_data['subjects'] = $subs = $this->M_subjects->get_teachers_subjects($this->userid);
		$this->view_data['subject_id'] = $subject_id;
		$this->view_data['ref_id'] = $subject_ref_id;
		$this->view_data['students'] = $students = $this->M_student_subjects->get_subjects_students($subject_id); 
		$this->view_data['grading_period'] = $gps = $this->M_grading_periods->get_all();
		$this->view_data['current_gp'] = $cgp = $this->cos->grading_period;
		$this->view_data['edit_all_period']  = false;
		$this->view_data['edit_grade']  = true;
		$this->view_data['recalculate_grade']  = true;
		
		if($this->input->post('update_grade') && $this->departments->stud_edit_grade === "1"){
			$s = $this->M_grades_file->teacher_update_grade($this->input->post());
			$this->_msg($s->code, $s->msg, current_url());
		}
	}

	public function print_grading($subject_id = false)
	{
		if($subject_id === false){ show_404(); }

		//check student grade access
		if($this->departments->stud_grade !== "1"){
			show_error("You don't have an access to this page");
		}

		$this->view_data['subject'] = $sub = $this->M_subjects->get_profile($subject_id); if($sub === false){ show_404(); }
		
		if(!$this->M_subjects->check_subject_under_teacher($this->userid, $subject_id)){
			show_404(); // check if subject is under the teacher
		}

		$this->view_data['subjects'] = $sub = $this->M_subjects->get_teachers_subjects($this->userid);
		$this->view_data['subject_id'] = $subject_id;
		$this->view_data['students'] = $students = $this->M_student_subjects->get_subjects_students($subject_id);
		$this->view_data['grading_period'] = $gps = $this->M_grading_periods->get_all();
		$this->view_data['current_gp'] = $cgp = $this->cos->grading_period;

		$this->load->library('../controllers/pdf');// include pdf controller
		$this->pdf->grading($this->view_data);// calls print method
	}

	/**
	 * Recompute Grade Conversion
	 * @param string $ref_id Subjects ref_id
	 */
	public function recalculate($ref_id = false)
	{
		$this->load->model('M_grades_file');

		if($ref_id===false){ show_404(); }
		$sub = $this->M_subjects->get_profile_ref_id($ref_id); if($sub === false){ show_404(); }
		$students = $this->M_student_subjects->get_subjects_students($sub->id); 
		if($students){
			foreach ($students as $k => $v) {
				$p = $v['profile'];
				if($p){
					$r = $this->M_grades_file->recompute_by_subject($p->id,$ref_id);
					if($r){
						$ctr++;
					}
				}
			}
		}

		if($ctr>0){
			$this->_msg('s','Grade Conversion was successful.', "my_subjects/grading/$ref_id");
		}else{
			$this->_msg('e','Either grades already updated or the process failed.', "my_subjects/grading/$ref_id");
		}
	}
}
