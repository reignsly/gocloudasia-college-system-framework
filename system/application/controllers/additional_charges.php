<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Additional_charges extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_additional_charges',
		'M_enrollments',
		'M_studentpayments_details',
		'M_student_total_file',
		'M_grading_periods',
		'M_settings',
		'M_additional_charges'));
		
		$this->menu_access_checker(array(
			'search/search_student',
			'enrollees/daily_enrollees',
			'search/master_list_by_course',
			'search/search_enrollee',
			'search/list_students/paid',
			'search/list_students/unpaid',
			'search/list_students/fullpaid',
			'search/list_students/partialpaid',
		));
	}
	
	// Create
	public function create($enrollment_id)
	{
		
		$this->view_data['academic_year'] = FALSE;
		$this->view_data['enrollment_id'] = $enrollment_id;
		$this->view_data['profile'] = $profile = $this->M_enrollments->profile($enrollment_id);
		$this->view_data['payment_details'] = $payment_details = $this->M_studentpayments_details->get_record_by_enrollment_id($enrollment_id);
		$this->view_data['current_period'] = $this->M_grading_periods->get_current_grading_period();
		$this->view_data['student_total'] = $student_total = $this->M_student_total_file->get_student_total_file($enrollment_id);
		
		if($_POST)
		{
			$data = $this->input->post('additional_charge');
			$data['enrollment_id'] = $enrollment_id;
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			$value = floatval($data['value']);
			
			$result = $this->M_additional_charges->create($data);
			
			if($result['status'])
			{
				//UPDATE STUDENT TOTAL FILE : ADD CHARGES
				$rs = $this->M_student_total_file->add_additional_charge($enrollment_id, $data['value']);
				
				if($rs['status'])
				{
					//RECALCULATE STUDENT PAYMENT DETAILS && PAYMENTS
					$this->M_studentpayments_details->recalculate_studentpayment_details($enrollment_id);
				}
				
				activity_log('create',$this->userlogin,'Success; Additional Charge Id: '.$result['id']);
				
				log_message('error','Additional Charge Created by: '.$this->user.'Success; Additional Charge Id: '.$result['id']);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Additional charges successfully added.</div>');
				redirect('additional_charges/index/'. $enrollment_id);
			}
		}
	}
	
	// Retrieve
	public function index($enrollment_id)
	{
		$this->load->model(array('M_additional_charges'));
		$this->view_data["enrollment_id"] = $enrollment_id;
		$this->view_data['additional_charges'] = $this->M_additional_charges->find_all($enrollment_id);
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
	}
	
	public function display($id)
	{
		$this->load->model(array('M_additional_charges'));
		
		$this->view_data['additional_charges'] = $this->M_additional_charges->get($id);
	}
	
	// Update
	public function edit($id)
	{
		$this->load->model(array('M_additional_charges'));
		
		$this->view_data['additional_charges'] = $this->M_additional_charges->get_additional_charges($id);
		
		if($_POST)
		{
			$data = $this->input->post('additional_charges');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_additional_charges->update($data, $id);
			
			if($result['status'])
			{
				
				activity_log('update additional charges',$this->userlogin,'Additional Charge Updated by: '.$this->user.'Success; Additional Charge Id: '.$id);
				
				log_message('error','Additional Charge Updated by: '.$this->user.'Success; Additional Charge Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Academic Year successfully updated.</div>');
				redirect('additional_charges');
			}
		}
	}
	
	// Update
	public function destroy($id, $enrollment_id)
	{
		$this->load->model(array('M_additional_charges','M_enrollments'));
		
		$total = $this->M_additional_charges->get($id);
		
		if($total)
		{
			//REVERSE TOTAL AMOUNT TO STUDENT TOTAL BALANCE
			$this->M_student_total_file->reverse_additional_charge($enrollment_id, $total->value);
			
			//RECALCULATE STUDENT PAYMENT DETAILS && PAYMENTS
			$this->reverse_studentpayment_details($enrollment_id, $total->value);
			
			//UPDATE ENROLLMENT IF PAID, FULL , PARTIAL AND UPDATE SUBJECT LOADS
			$this->M_enrollments->update_enrollments_status($enrollment_id); 
		}
		
		$result = $this->M_additional_charges->delete($id);
		
		activity_log('delete additional charges',$this->userlogin,'Additional Charge Deleted by: '.$this->user.'Success; Additional Charge Id: '.$id);
		
		log_message('error','Additional Charge Deleted by: '.$this->user.'Success; Additional Charge Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-success">Additional Charge successfully deleted.</div>');
		redirect('additional_charges/index/'.$enrollment_id);
	}
}
