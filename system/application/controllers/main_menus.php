<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_menus extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		// $this->session_checker->secure_page('admin');
		$this->load->library(array('form_validation'));
		$this->load->helper(array('my_dropdown'));
		$this->load->model(array('M_main_menus'));
	}
	
	// Create
	public function create()
	{
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->token->set_token();
		
		if($_POST)
		{
			if($this->form_validation->run('main_menus') == true)
			{
				$data['controller'] = $this->input->post('controller');
				$data['caption'] = $this->input->post('caption');
				$data['remarks'] = $this->input->post('remarks');
				$data['menu_lvl'] = 2;
				
				$result = $this->M_main_menus->insert($data);
				
				if($result['status'])
				{
					$id = $result['id'];
					activity_log('Create Main_menus', $this->userlogin, 'Main Menu ID'.$id);
					$this->session->set_flashdata('system_message', '<div class="alert alert-success"><b><span class="glyphicon glyphicon-ok"></span>&nbsp; Main Menu successfully added.</b></div>');
					redirect(current_url());
				}
			}
		}
	}
	
	// Retrieve
	public function index($page = 0)
	{
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		//PAGINATION
		$filter = false;
		$like = false;
		$order_by = false;
		
		if($_GET)
		{
			if(isset($_GET['controller']) && trim($_GET['controller']) != ''){
				$this->view_data['controller'] = $controller = trim($_GET['controller']);
				$like['main_menus.controller'] = $controller;
			}
			if(isset($_GET['caption']) && trim($_GET['caption']) != ''){
				$this->view_data['caption'] = $caption = trim($_GET['caption']);
				$like['main_menus.caption'] = $caption;
			}
		}
		
		//CONFIGURATION
		// $get['fields'] = array(
				// 'enrollments.id',
				// 'enrollments.studid',
				// 'enrollments.name' ,
				// 'years.year',
				// 'courses.course' 
		// );
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		// $get['join'] = array(
			
			// 1 => array(
				// "table" => "courses",
				// "on"	=> "courses.id = enrollments.course_id",
				// "type"  => "LEFT"
			// ),
			// 2 => array(
				// "table" => "years",
				// "on"	=> "years.id = enrollments.year_id",
				// "type"  => "LEFT"
			// )
		// );
		$get['order'] = "main_menus.caption";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."main_menus/index";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_departments->get_record("main_menus", $get);
		
		$config["per_page"] = 20;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_departments->get_record("main_menus", $get);
		$this->view_data['links'] = $this->pagination->create_links();	
	}
	
	// Update
	public function edit($id = false)
	{
		if(!$id){ show_404(); }
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');

		$this->view_data['main_menus'] = $main_menus =$this->M_main_menus->get($id);
		
		if($main_menus):

		if($_POST)
		{
			if($this->form_validation->run('main_menus') == true)
			{	
				$data['controller'] = $this->input->post('controller');
				$data['caption'] = $this->input->post('caption');
				$data['remarks'] = $this->input->post('remarks');
				
				$result = $this->M_main_menus->update($id, $data);
				
				if($result)
				{
					activity_log('Edit Main_menus', $this->userlogin, 'Main Menu ID'.$id);
					$this->session->set_flashdata('system_message', '<div class="alert alert-success"><b><span class="glyphicon glyphicon-ok"></span>&nbsp; Main Menu successfully updated.</b></div>');
					redirect(current_url());
				}
			}
		}

		endif;
	}

	public function delete($id = false)
	{
		if(!$id) { show_404(); }
		$data = $this->M_main_menus->get($id);
		if($data):
		$result = $this->M_main_menus->delete($id);
		activity_log('destroy Main_menu', $this->userlogin, 'Menu ID : '.$id.' Caption : '.$data->caption);
		$this->session->set_flashdata('system_message', '<div class="alert alert-danger"><span class = "glyphicon glyphicon-ok"></span>&nbsp; Main Menu successfully deleted.</div>');
		redirect('main_menus');
		endif;
	}
}

?>