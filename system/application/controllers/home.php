<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('M_settings','M_users','M_events'));
		$this->load->model('M_core_model','m');
		$this->session_checker->open_semester();
		$this->session_checker->check_if_alive();
	}
	
	public function index()
	{
		$this->load->model('M_announcements','m_a');

		$this->view_data['profile'] = $p = $this->M_users->profile($this->userid);
		$this->view_data['announcements'] = $a = $this->m_a->for_employee(10);
		$this->view_data['events'] = $e = $this->M_events->get_incoming_events();
	}
	
	public function out()
	{
		$view_data['setting'] = $this->M_settings->get_settings();
		
		$view_data['system_message'] = "<div class='alert alert-danger'>Thank you for using the system. You have been successfully logout.</div>";
		
		$this->load->view('layouts/_home', $view_data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */