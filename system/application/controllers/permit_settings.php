<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permit_settings extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		// $this->session_checker->secure_page(array('admin','finance'));
		$this->menu_access_checker();
	}
	
	// Create
	public function create()
	{
		$this->load->model(array('M_permit_settings'));
		
		$this->view_data['permit_settings'] = FALSE;
		
		if($_POST)
		{
			$data = $this->input->post('permit_settings');
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_permit_settings->create_permit_settings($data);
			
			if($result['status'])
			{
				log_message('error','Grade Slip settings Created by: '.$this->user.'Success; Grade Slip settings Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Grade Slip settings successfully added.</div>');
				redirect('permit_settings');
			}
		}
	}
	
	// Retrieve
	public function index()
	{
		// $this->load->model(array('M_permit_settings'));
		// $this->view_data['permit_settings'] = $this->M_permit_settings->find_all();
		// $this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		redirect('permit_settings/edit/1');
	}
	
	public function display($id)
	{
		$this->load->model(array('M_permit_settings'));
		
		$this->view_data['permit_settings'] = $this->M_permit_settings->get_permit_settings($id);
	}
	
	// Update
	public function edit($id)
	{
		$this->load->model(array('M_permit_settings'));
		
		$this->view_data['permit_settings'] = $this->M_permit_settings->get_permit_settings($id);
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($_POST)
		{
			$data = $this->input->post('permit_settings');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_permit_settings->update_permit_settings($data, $id);
			// var_dump($result);
			if($result['status'])
			{
				log_message('error','Permit_settings Updated by: '.$this->user.'Success; Permit_settings Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Permit_settings successfully updated.</div>');
				redirect('permit_settings/edit/'.$id);
			}
		}
	}
	
	// Update
	public function destroy($id)
	{
		$this->load->model(array('M_permit_settings'));
		
		$result = $this->M_permit_settings->delete_permit_settings($id);
		log_message('error','Permit_settings Deleted by: '.$this->user.'Success; Permit_settings Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Permit_settings successfully deleted.</div>');
		redirect('permit_settings');
	}
}