<?php

class Hrd Extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->session_checker->secure_page(array('hrd'));
		$this->session_checker->open_semester();
		$this->load->helper('message');
	}
	
	public function index(){
		$this->session_checker->check_if_alive(false, true);
		
		// $this->load->library('login');
		// $password = "12345678";
		// $this->login->_generate_password($password);
		// $this->dump($this->login->get_password());
		// $this->dump($this->login->get_salt());
	}
	
	public function add_employee(){}
	
	// LOAD MODELS
	public function load_employee_attendance_db(){$this->load->model("m_employee_attendances");}
	public function load_employee_db(){$this->load->model("m_employees");}
	public function load_user_db(){$this->load->model("m_users");}
	public function load_assignsubject_db(){$this->load->model("m_assignsubjects");}
	public function load_assign_course_db(){$this->load->model("m_assign_courses");}
	// END LOAD MODELS
	
	
	// Employee Section
	
	public function view_employee_profile($id){
	  $this->load_employee_db();
	  $this->load_assign_course_db();
	  $this->load_assignsubject_db();
	  $this->view_data['employee_profile'] = $this->m_employees->load_employee($id);
	  $this->view_data['assign_courses'] = $this->m_assign_courses->get_employee_assigned_courses($id);
	  $this->view_data['assignsubjects'] = $this->m_assignsubjects->get_employee_assignsubjects($id);
	  
	}
	
	public function edit_employee_profile($id){
	  $this->load_employee_db();
	  $this->view_data["roles"] = array("Teacher", "Registrar", "Finance", "HRD", "Librarian", "Dean", "Custodian", "Cashier");
	  $this->view_data["genders"] = array("Male", "Female");
	  $this->view_data["martial_status"] = array("Single", "Married", "Divorced", "Widowed");
	  $this->view_data['employee_profile'] = $this->m_employees->load_employee($id);
	  $this->view_data["system_message"] = show_flash_message();
	}
	
	public function update_employee_profile($id){
	  $this->load_employee_db();
	  if ($this->m_employees->update_employee($this->input->post(), $id))
	  {
	    set_flash_message('success', 'Successfully updated employee profile.', '');
	  }
	  else
	  {
	    set_flash_message('error', 'Sorry, something went wrong.',''); 
	  }
	  
	  
	  redirect("hrd/edit_employee_profile/". $id);
	}
	
	
	// Activating User Accounts
	public function activate_employee_accounts(){
	   $this->load_employee_db();
	   $this->view_data["employees"] = $this->m_employees->load_unactivated_employees();
	   $this->view_data["system_message"] = show_flash_message();
	}
	
	public function activate_accounts()
	{
	  $this->load->helper('message');
	  $this->load_user_db();
	  if ($this->input->post())
	  {
	      $this->m_users->activate_user_account($this->input->post());
	  }
	  set_flash_message('success', 'Successfully activated employee accounts.', site_url('hrd/activate_employee_accounts'));
	}
	
	public function delete_assignsubject($id, $employee_id)
	{
	   $this->load_assignsubject_db();
	   $this->m_assignsubjects->delete_from_db($id);
	   set_flash_message("error", 'Successfully deleted assigned subject.',"employees/display/". $employee_id );
	}
	
	
	// Employee Attendance
	public function employee_calendar($year=false, $month=false)
	{
	  $this->load->helper('date');
	  $prefs=array(
	    'show_next_prev' => TRUE,
	    'next_prev_url' => base_url() . 'hrd/employee_calendar/',
	    'day_type' => 'abr'
	  );
	  if ($year==false)
	  {
	    $year=date('Y');
	  }
	  if ($month==false)
	  {
	    $month=date('m');
	  }
	  
	  $number_of_days = days_in_month($month, $year) + 1;
	  
	  for ($i = 0; $i < $number_of_days; $i++)
	  {
	    $day[$i] = site_url('hrd/add_employee_attendance/?year='. $year . '&month=' . $month . '&day=' . $i);
	  }
	  $prefs['template'] = '

   {table_open}<table border="0" cellpadding="0" cellspacing="0" class="calendar">{/table_open}

   {heading_row_start}<tr>{/heading_row_start}

   {heading_previous_cell}<th><a href="{previous_url}"align="left">&lt;&lt;</a></th>{/heading_previous_cell}
   {heading_title_cell}<th colspan="{colspan}" align="center" class="header">{heading}</th>{/heading_title_cell}
   {heading_next_cell}<th align="right"><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}

   {heading_row_end}</tr>{/heading_row_end}

   {week_row_start}<tr>{/week_row_start}
   {week_day_cell}<td align="center">{week_day}</td>{/week_day_cell}
   {week_row_end}</tr>{/week_row_end}

   {cal_row_start}<tr>{/cal_row_start}
   {cal_cell_start}<td height="50">{/cal_cell_start}

   {cal_cell_content}
   <div><a href="{content}" class="day">{day}</a></div>
   
   <div class="day_links">
   <center>
   <a href="{content}" >Add</a><br><br>
   <a href="'. site_url('hrd/view_employee_attendance/?year='. $year . '&month=' . $month . '&day=') .'{day}" >View</a>
   </center>
   </div>
   {/cal_cell_content}
   
   {cal_cell_content_today}
   <div class="highlight"><a href="{content}" >{day}</a></div>
   
   <div class="day_links">
   <center>
   <a href="{content}" >Add</a><br><br>
   <a href="'. site_url('hrd/view_employee_attendance/?year='. $year . '&month=' . $month . '&day=') .'{day}" >View</a>
   </center>
   </div>
   {/cal_cell_content_today}

   {cal_cell_no_content}{day}{/cal_cell_no_content}
   {cal_cell_no_content_today}<div class="highlight">{day}</div>{/cal_cell_no_content_today}

   {cal_cell_blank}&nbsp;{/cal_cell_blank}

   {cal_cell_end}</td>{/cal_cell_end}
   {cal_row_end}</tr>{/cal_row_end}

   {table_close}</table>{/table_close}
';
	  $this->load->library('calendar', $prefs);
	  $this->view_data['calendar'] = $this->calendar->generate($year, $month, $day);
	}
	
	
	// Employee Attendances
	public function add_employee_attendance()
	{
	   $year = $this->input->get('year');
	   $month = $this->input->get('month');
	   $day = $this->input->get('day');
	   $this->view_data["year"] = $year;
	   $this->view_data["month"] = $month;
	   $this->view_data["day"] = $day;
	   $this->load_employee_db();
	   $this->load_employee_attendance_db();
	   $this->view_data["employees"] = $this->m_employees->load_active_employees();
	   
	   
	    $this->load->helper('message');
	    if ($this->input->post())
	    { 
	      $this->m_employee_attendances->create_attendances($this->input->post(), $year, $month, $day);
	      set_flash_message('success', 'Successfully created employee attendance.', site_url('hrd/add_employee_attendance/?year='. $year . '&month=' . $month . '&day=' . $day));
	    }
	    
	}
	
	public function view_employee_attendance()
	{
	   $year = $this->input->get('year');
	   $month = $this->input->get('month');
	   $day = $this->input->get('day');
	   
	   
	   $this->load_employee_db();
	   $this->load_employee_attendance_db();  
	   
	   
	   $this->view_data["year"] = $year;
	   $this->view_data["month"] = $month;
	   $this->view_data["day"] = $day;
	   $this->view_data['number_of_days'] = days_in_month($month, $year);
	   $this->view_data["employees"] = $this->m_employees->load_active_employees();
	   
	}
	
	
	
	
	
	
}
