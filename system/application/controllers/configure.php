<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configure extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();

		$this->menu_access_checker('configure/school');
	}
	
	public function school()
	{
		$this->menu_access_checker();
		$this->view_data['custom_title'] = 'Settings';
		$this->view_data['par'] = 'Edit';
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->view_data['settings'] = $this->M_settings->get_settings();
	}
	
	public function edit($id = false)
	{
		if($id == false) { show_404(); }
		
		$data = $this->input->post('setting');
		
		if($_POST)
		{
			if (isset($_FILES['userfile']) && is_uploaded_file($_FILES['userfile']['tmp_name'])) {
			
				$config['upload_path'] = $path = 'assets/images/logo';
				$config['allowed_types'] = 'jpg|png';
				$config['max_size']	= '100';
				$config['max_width']  = '1024';
				$config['max_height']  = '768';
				$config['remove_spaces']  = true;
				$config['overwrite']  = true;
				$config['encrypt_name']  = true;

				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload())
				{
					// vd($this->upload->display_errors());
					$this->session->set_flashdata('system_message', '<div class="alert alert-danger">'.$this->upload->display_errors().'.</div>');
					redirect('configure/school');
				}
				else
				{
					$xfile = $this->upload->data();
					$data['logo'] = $path.'/'.$xfile['file_name'];
				}
			}
			
			if($this->M_settings->update_table($data,$id) == TRUE)
			{
				activity_log('update School Settings',$this->userlogin,'School Settings Updated by: '.$this->user.'; Settings Id: '.$id);
				log_message('info','School Settings Updated by: '.$this->user.'; Settings Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">School successfully updated.</div>');
				// var_dump('x');
			}else
			{
				log_message('error','School Settings Updated by: '.$this->user.'FAILED; Settings Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">School successfully updated failed</div>');
			}
			
		}
		
		redirect('configure/school');
	}
	
	public function index()
	{
		// $this->menu_access_checker(array('configure/school'));
	}
}

?>