<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_students extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->menu_access_checker();
		$this->settings 	= $this->M_settings->get_settings();

		$this->load->model(array(
			'M_subjects',
			'M_years',
			'M_student_subjects',
			'M_grading_periods',
			'M_grades_file',
			'M_grading_system'
		));

		$this->load->helper(['text','grade']);
	}

	/** Index : Get All Students of the teacher logged in */
	public function index($page = 0)
	{
		$this->view_data['years'] = $this->M_years->get_for_dd(array('id','year'), false, 'Select Year');

		$like = false;
		$filter = false;

		$filter['enrollments.sy_from'] = $this->cos->user->year_from;
		$filter['enrollments.sy_to'] = $this->cos->user->year_to;
		$filter['enrollments.semester_id'] = $this->cos->user->semester_id;
		$filter['subjects.teacher_user_id'] = $this->userid;
		
		$this->view_data['studid'] = "";
		$this->view_data['lastname'] = "";
		$this->view_data['firstname'] = "";
		$this->view_data['middlename'] = "";
		$this->view_data['year_id'] = "";

		if($_GET)
		{
			if(isset($_GET['studid']) && trim($_GET['studid']) != ""){
				$this->view_data['studid'] = $studid = trim($_GET['studid']);
				$like['enrollments.studid'] = $studid;
			}
			
			if(isset($_GET['lastname']) && trim($_GET['lastname'])){
				$this->view_data['lastname'] = $lastname = trim($_GET['lastname']);
				$like['enrollments.lastname'] = $lastname;
			}
			
			if(isset($_GET['firstname']) && trim($_GET['firstname'])){
				$this->view_data['firstname'] = $firstname = trim($_GET['firstname']);
				$like['enrollments.firstname'] = $firstname;
			}

			if(isset($_GET['middlename']) && trim($_GET['middlename'])){
				$this->view_data['middlename'] = $middlename = trim($_GET['middlename']);
				$like['enrollments.middlename'] = $middlename;
			}

			if(isset($_GET['year_id']) && trim($_GET['year_id'])){
				$this->view_data['year_id'] = $year_id = trim($_GET['year_id']);
				$filter['enrollments.year_id'] = $year_id;
			}
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'enrollments.id',
				'enrollments.name',
				'enrollments.studid',
				'years.year',
				'courses.course_code'
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			0 => array(
				"table" => "subjects",
				"on"	=> "subjects.id = studentsubjects.subject_id",
				"type"  => "LEFT"
			),
			1 => array(
				"table" => "enrollments",
				"on"	=> "enrollments.id = studentsubjects.enrollment_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			),
			3 => array(
				"table" => "courses",
				"on"	=> "courses.id = enrollments.course_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "enrollments.lastname";
		$get['group'] = "studentsubjects.enrollment_id";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."my_students/index";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$this->view_data['get_url'] = $config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("studentsubjects", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		// $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['results'] = $search = $this->M_core_model->get_record("studentsubjects", $get);
		$this->view_data['links'] = $this->pagination->create_links();	
		$this->view_data['custom_title'] = $config["total_rows"].' students';
	}

	/**
	 * Student Subjects - View All Student Subjects
	 * @param int $id Enrollment ID
	 * @param string $action More Action Like PDF for Printing
	 */
	public function subjects($id = false, $action = false)
	{
		if($id === false){ show_404(); }

		//check student grade access
		if($this->departments->stud_grade !== "1"){
			show_error("You don't have an access to this page");
		}

		$this->load->model('M_grades_file','m_gf');
		$this->load->model('M_student_subjects','m_ss');

		$this->load->library('_Student', array('enrollment_id'=>$id));
		$this->view_data['profile'] = $p = $this->_student->profile; if($p === false){ show_404(); }

		//Check if Student Belongs to the teacher
		if(!$this->M_student_subjects->is_enrollment_belong_to_teacher($this->userid, $id)){ show_404(); }

		//Grading System
		$this->view_data['c_grading_period'] = $cgp = $this->current_grading_period;
		$this->view_data['is_final_gp'] = $ifg = $this->M_grading_periods->is_current_last_gp();
		$this->view_data['grading_system'] = $gs = isset($this->syspar->grading_system) && $this->syspar->grading_system ? $this->syspar->grading_system : 'input';
		$this->view_data['grading_table'] = $this->M_grading_system->get_system();
		$this->view_data['max_grade'] = $max = $this->M_grading_system->maximum_score();
		$this->view_data['min_grade'] = $min = $this->M_grading_system->minimum_score();

		$this->view_data['grades'] = $ss = $this->_student->get_student_subject_grades($this->userid);
		$this->view_data['students'] = $s = $this->M_student_subjects->get_student_subjects_under_teacher($id, $this->userid);
		$this->view_data['grading_period'] = $gps = $this->M_grading_periods->get_all();
		$this->view_data['enrollment_id'] = $id;
		$this->view_data['current_gp'] = $cgp = $this->cos->grading_period;
		$this->view_data['edit_all_period']  = false;
		$this->view_data['edit_grade']  = true;
		$this->view_data['recalculate_grade']  = true;

		 // Grade Editing
		if($this->input->post('update_grade') && $this->departments->stud_edit_grade === "1")
		{
			$s = $this->M_grades_file->teacher_update_student_grade($this->input->post());
			$this->_msg($s->code, $s->msg, current_url());
		}
	}

	public function print_subjects($id = false)
	{
		if($id === false){ show_404(); }

		//check student grade access
		if($this->departments->stud_grade !== "1"){
			show_error("You don't have an access to this page");
		}

		$this->load->model('M_grades_file','m_gf');
		$this->load->model('M_student_subjects','m_ss');

		$this->load->library('_Student', array('enrollment_id'=>$id));
		$this->view_data['profile'] = $p = $this->_student->profile; if($p === false){ show_404(); }

		//Check if Student Belongs to the teacher
		if(!$this->M_student_subjects->is_enrollment_belong_to_teacher($this->userid, $id)){ show_404(); }

		$this->view_data['grades'] = $ss = $this->_student->get_student_subject_grades($this->userid);
		$this->view_data['students'] = $s = $this->M_student_subjects->get_student_subjects_under_teacher($id, $this->userid);
		$this->view_data['grading_period'] = $gps = $this->M_grading_periods->get_all();
		$this->view_data['enrollment_id'] = $id;
		$this->view_data['current_gp'] = $cgp = $this->cos->grading_period;

		$this->load->library('../controllers/pdf');// include pdf controller
		$this->pdf->subject_grade($this->view_data);// calls print method
	}

	/**
	 * Recompute Grade Convertion by Student
	 * @param  string $hash enrollment hash
	 */
	public function recalculate($hash='')
	{
		$id = $this->check_hash($hash);

		//Check if Student Belongs to the teacher
		if(!$this->M_student_subjects->is_enrollment_belong_to_teacher($this->userid, $id)){ show_404(); }

		$this->load->library('_Student', array('enrollment_id'=>$id));
		$this->view_data['profile'] = $p = $this->_student->profile; if($p === false){ show_404(); }

		//Check if Student Belongs to the teacher
		if(!$this->M_student_subjects->is_enrollment_belong_to_teacher($this->userid, $id)){ show_404(); }

		$ss = $this->_student->get_student_subject_grades($this->userid);
		$ctr = 0;
		foreach ($ss->subjects as $k => $v) {
			if($v['grade']){
				foreach ($v['grade'] as $gk => $gf) {
					$r = $this->M_grades_file->recalculate_per_record($gf->id);
					if($r){
						$ctr++;
					}		
				}
			}
		}
		if($ctr>0){
			$this->_msg('s','Grade Conversion was succesfully updated','my_students/subjects/'.$id);
		}

		$this->_msg('e','Either grade is already updated or process failed.','my_students/subjects/'.$id);
	}
}
