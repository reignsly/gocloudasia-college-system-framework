<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Borrow_items extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->load->helper('my_dropdown');
		$this->load->library(array('form_validation'));
	}
	
	// Create
	public function create()
	{
		$this->menu_access_checker();

		$this->load->model(array('M_items','M_demands'));
		
		$this->load->helper('my_dropdown');
		
		if($_POST)
		{
			if($this->form_validation->run('borrow_items') == true)
			{	
				$data = $this->input->post('borrow_items');
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['updated_at'] = date('Y-m-d H:i:s');
				$data['status'] = "UNRETURN";
				$data['unit_unreturn'] = $data['unit'];
				$data['unit_return'] = 0;
				
				
				
				$unit =  (float)$data['unit'];
				$item_id = $data['item_id'];
				
				$item = $this->M_items->get_items($item_id);
				
				//DOUBLE CHECK ITEM AVAILABILITY
				if((float)$item->unit_left >= (float)$unit)
				{
				
					$result = $this->M_demands->create_demands($data);
					
					if($result['status'])
					{
						activity_log('create Borrow_items/Demand',$this->userlogin,'Borrow_items/Demand Created by: '.$this->user.'Success; Demand Id: '.$result['id']);
						
						log_message('error','Borrow_items/Demand Created by: '.$this->user.'Success; Demand Id: '.$result['id']);
					
						//IF SAVING SUCCESSFULL UPDATE ITEM UNIT LEFT
						unset($data);
						$data['updated_at'] = date('Y-m-d H:i:s');
						$data['unit_left'] = $item->unit_left - $unit;
						$rs = $this->M_items->update_items($data, $item_id);
						
						//UPDATE ITEM STATUS
						$rs = $this->M_items->update_item_status($item_id);
						
						
						$this->session->set_flashdata('system_message', '<div class="alert alert-success">Item successfully borrowed.</div>');
						redirect(current_url());
					}
				}
				else
				{
					$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Item don\'t have enough unit left ('.$item->unit_left.').</div>');
					redirect(current_url());
				}
			}
		}
	}
	
	public function records($page=0)
	{
		$this->menu_access_checker();

		$this->load->model(array('M_demands'));
		
		$this->load->model(array('M_products','M_items'));
		$this->load->helper(array('my_url'));
		
		//PAGINATION
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		$order_by = 'items.id ASC';
		
		if($_POST){
			if($this->input->post('submit') == "Search"){
				$this->view_data['fields'] = $field = $this->input->post('fields');
				$this->view_data['keyword'] = $keyword = $this->input->post('keyword');
				$this->view_data['status'] = $status = $this->input->post('status');
				if(trim($keyword) != ""){
					$filter[$field.' LIKE '] = "%".trim($keyword)."%";
				}
				if($status != ""){
					$filter['demands.status = '] = strtoupper($status);
				}
				$order_by = 'demands'.$field.' ASC';
				
				$page = 0;
			}
		}
		
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."borrow_items/records";
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_demands->find(0,0,$filter, $order_by,true, true);
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$this->view_data['items'] = $items = $this->M_demands->find($page, $config["per_page"], $filter, $order_by);
		$this->view_data['links'] = $this->pagination->create_links();
	}
	
	// Retrieve
	public function index($page=0)
	{
		show_404();
		
	}
	
	public function view_return($id = false)
	{
		$this->menu_access_checker('borrow_items/records');

		if($id == false){ show_404(); }
		
		$this->load->model(array('M_demands','M_demands_return','M_items','M_demands_return_lost'));
		
		$this->view_data['return'] = $ret = $this->M_demands_return->get_demands_return_by_demand_id($id);
		
		$this->view_data['borrow_items'] = $demands = $this->M_demands->get_demands($id);
		
		if($demands == false) { show_404(); }
		
		$this->view_data['demands_id'] = $id;
		
		$this->view_data['demands_return'] = $demands_return = $this->M_demands_return->get_demands_return_by_demand_id($id);
		
		$this->view_data['demands_return_lost'] = $demands_return_lost = $this->M_demands_return_lost->get_demands_return_lost_by_demand_id($id);
		
		$this->view_data['items'] = $items = $this->M_items->get_items($demands->item_id);
	}
	
	public function return_lost_or_damage($id = false)
	{
		$this->menu_access_checker('borrow_items/records');

		if($id == false){ show_404(); }
		
		if($id == false){ show_404(); }
		
		$this->load->model(array('M_demands','M_demands_return_lost','M_items'));
		
		$this->view_data['borrow_items'] = $borrow_items = $this->M_demands->get_demands($id);
		
		if($_POST)
		{
			$unit_return = $this->input->post('unit_return');	
			$date_return = $this->input->post('date_return');	
			$remarks = $this->input->post('remarks');	
			$category = $this->input->post('category');	
		
			if(isset($unit_return) && $unit_return != "")
			{
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['updated_at'] = date('Y-m-d H:i:s');
				$data['demands_id'] = $id;
				$data['unit_return'] = $unit_return;
				$data['date_return'] = $date_return.' '.date('g:h:s');
				$data['remarks'] = $remarks;
				$data['category'] = $category;
				
				$rs = $this->M_demands_return_lost->create_demands_return_lost($data);
				
				
				if($rs['status'])
				{
					activity_log('borrow Borrow_items/Demand',$this->userlogin,'Borrow Item Lost/Damage Return by: '.$this->user.'Success; Demand Return Lost Id: '.$rs['id']);
					
					log_message('error','Borrow Item Lost/Damage Return by: '.$this->user.'Success; Demand Return Lost Id: '.$rs['id']);
					
					#region ADD RETURN ITEM TO ITEM UNIT LEFT
					$item = $this->M_items->get_items($borrow_items->item_id);
					
					$data2['updated_at'] = date('Y-m-d H:i:s');
					$data2['unit_left'] = (float)$item->unit_left + (float)$data['unit_return'];
					
					$rs = $this->M_items->update_items($data2, $item->id);
					if($rs['status']){
						$this->M_items->update_item_status($item->id); //UPDATE ITEM STATUS
					}
					#endregion
					
					#region ADD RETURN ITEM TO BORROW ITEM UNIT RETURN AND UPDATE UNIT UNRETURN
					$data3['updated_at'] = date('Y-m-d H:i:s');
					$data3['unit_lost_return'] = (float)$borrow_items->unit_lost_return + (float)$data['unit_return'];
					$data3['unit_unreturn'] = (float)$borrow_items->unit_unreturn - (float)$data['unit_return'];
					
					$rs = $this->M_demands->update_demands($data3, $borrow_items->id);
					if($rs['status']){
						$this->M_demands->update_demands_status($borrow_items->id); //UPDATE DEMANDS/BORROWING STATUS
					}
					
					#endregion
					$this->session->set_flashdata('system_message', '<div class="alert alert-success">Item successfully returned.</div>');
					redirect('borrow_items/view_return/'.$borrow_items->id);
				}
			}
		}
	}
	
	public function return_item($id = false)
	{
		$this->menu_access_checker('borrow_items/records');

		if($id == false){ show_404(); }
		
		$this->load->model(array('M_demands','M_demands_return','M_items'));
		
		$this->view_data['borrow_items'] = $borrow_items = $this->M_demands->get_demands($id);
		
		if($_POST)
		{
			$unit_return = $this->input->post('unit_return');	
			$date_return = $this->input->post('date_return');	
			$remarks = $this->input->post('remarks');	
		
			if(isset($unit_return) && $unit_return != "")
			{
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['updated_at'] = date('Y-m-d H:i:s');
				$data['demands_id'] = $id;
				$data['unit_return'] = $unit_return;
				$data['date_return'] = $date_return.' '.date('g:h:s');
				$data['remarks'] = $remarks;
				
				$rs = $this->M_demands_return->create_demands_return($data);
				
				if($rs['status'])
				{
					activity_log('return Borrow_items/Demand',$this->userlogin,'Borrow Item Return by: '.$this->user.'Success; Demand Return Id: '.$rs['id']);
					
					log_message('error','Borrow Item Return by: '.$this->user.'Success; Demand Return Id: '.$rs['id']);
					
					#region ADD RETURN ITEM TO ITEM UNIT LEFT
					$item = $this->M_items->get_items($borrow_items->item_id);
					
					$data2['updated_at'] = date('Y-m-d H:i:s');
					$data2['unit_left'] = (float)$item->unit_left + (float)$data['unit_return'];
					
					$rs = $this->M_items->update_items($data2, $item->id);
					if($rs['status']){
						$this->M_items->update_item_status($item->id); //UPDATE ITEM STATUS
					}
					#endregion
					
					#region ADD RETURN ITEM TO BORROW ITEM UNIT RETURN AND UPDATE UNIT UNRETURN
					$data3['updated_at'] = date('Y-m-d H:i:s');
					$data3['unit_return'] = (float)$borrow_items->unit_return + (float)$data['unit_return'];
					$data3['unit_unreturn'] = (float)$borrow_items->unit_unreturn - (float)$data['unit_return'];
					
					$rs = $this->M_demands->update_demands($data3, $borrow_items->id);
					if($rs['status']){
						$this->M_demands->update_demands_status($borrow_items->id); //UPDATE DEMANDS/BORROWING STATUS
					}
					#endregion
					$this->session->set_flashdata('system_message', '<div class="alert alert-success">Item successfully returned.</div>');
					redirect('borrow_items/view_return/'.$borrow_items->id);
				}
			}
		}
	}
	
	// Update
	public function edit($id = false)
	{
		if($id == false){ show_404(); }
		
		$this->load->model(array('M_demands','M_demands_return'));
		
		$this->view_data['borrow_items'] = $borrow_items = $this->M_demands->get_demands($id);

	}
	
	// Update
	public function destroy($id = false)
	{
		$this->menu_access_checker('borrow_items/records');

		if($id == false){ show_404(); }
		$this->load->model(array('M_demands','M_items'));
		
		$demands = $this->M_demands->get_demands($id);
		
		if($demands == false) {show_404();}
		
		$unit_borrowed = $demands->unit;
		
		$items = $this->M_items->get_items($demands->item_id);
		
		
		
		if($items)
		{
			$result = $this->M_demands->delete_demands($id);
			
			if($result['status'])
			{
				activity_log('deleted Borrow_items/Demand',$this->userlogin,'Demands Deleted by: '.$this->user.'Success; Demand Id: '.$id);
				
				log_message('error','Demands Deleted by: '.$this->user.'Success; Demand Id: '.$id);
				
				//UPDATE ITEM UNIT LEFT
				$data['updated_at'] = NOW;
				$data['unit_left'] = $items->unit_left + $unit_borrowed;
				$rs = $this->M_items->update_items($data, $items->id);
				
				if($rs['status']){
					$this->M_items->update_item_status($items->id);
				}
				
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Borrowed Item successfully deleted.</div>');
				redirect('borrow_items/records');
			}
		}
		else
		{
			show_404();
		}
	}
	
	public function print_return($id = false)
	{
		if($id == false) { show_404(); }		
		
		$this->load->helper('print');
		
		$this->load->model(array('M_demands','M_demands_return','M_items','M_demands_return_lost'));
		
		$this->view_data['return'] = $ret = $this->M_demands_return->get_demands_return_by_demand_id($id);
		
		$this->view_data['borrow_items'] = $demands = $this->M_demands->get_demands($id);
		
		if($demands == false) { show_404(); }
		
		$this->view_data['demands_id'] = $id;
		
		$this->view_data['demands_return'] = $demands_return = $this->M_demands_return->get_demands_return_by_demand_id($id);
		
		$this->view_data['demands_return_lost'] = $demands_return_lost = $this->M_demands_return_lost->get_demands_return_lost_by_demand_id($id);
		
		$this->view_data['items'] = $items = $this->M_items->get_items($demands->item_id);
		
		$html = _html_borrow_items($this->view_data); 

		$this->load->library('mpdf');
		
		$mpdf=new mPDF('','FOLIO','','',15,5,5,5,0,0); 

		$mpdf->AddPage('P');

		$mpdf->WriteHTML($html);

		$mpdf->Output();
	}
}