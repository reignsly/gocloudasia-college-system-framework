<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Certificate_of_enrollment_settings extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
	}

	public function index()
	{
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_certificate_of_enrollment_settings'));
		
		$this->view_data['coes'] = $this->M_certificate_of_enrollment_settings->get_certificate_of_enrollment();
	}
	
	public function show($id)
	{
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_certificate_of_enrollment_settings'));
		
		$this->view_data['coes'] = $this->M_certificate_of_enrollment_settings->get_certificate_of_enrollment($id);
	}
	
	public function edit($id)
	{
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_certificate_of_enrollment_settings'));
		
		$this->view_data['coes'] = $this->M_certificate_of_enrollment_settings->get_certificate_of_enrollment($id);
		
		if($_POST)
		{
			if($_POST)
			{
			
			$data['signatory'] = $_POST['signatory'];
			$data['signatory_position'] = $_POST['signatory_position'];
			$data['first_paragraph'] = $_POST['first_paragraph'];
			$data['second_paragraph'] = $_POST['second_paragraph'];
			$data['updated_at'] = date('Y-m-d H:i:s');
			$id = $_POST['id'];
			
			$result = $this->M_certificate_of_enrollment_settings->update_certificate_of_enrollment_settings($data,$id);
			if($result['status'])
			{
				activity_log('edit certificate_of_enrollment_settings',$this->userlogin,' ID : '.$id);
				
				redirect('certificate_of_enrollment_settings');
			}
			}
		}
	}
}