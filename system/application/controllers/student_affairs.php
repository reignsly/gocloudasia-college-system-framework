<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Student_affairs extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->session_checker->secure_page(array('student_affairs'));
		$this->load->helper(array('url_encrypt'));
	}

	public function index(){
		
	}
}
?>
