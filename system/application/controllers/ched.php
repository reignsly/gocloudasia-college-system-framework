<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ched extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->menu_access_checker();
	}
	
	public function index()
	{	
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_years','M_semesters','M_courses','M_enrollments','M_student_subjects'));
		
		$this->load->helper('my_dropdown');
		
		$this->view_data['no_subjects'] = 0;
		$this->view_data['students'] = false;
		if($_POST)
		{
			$this->view_data['year_id'] = $year_id = $this->input->post('year_id');
			$this->view_data['sem_id'] = $sem_id = $this->input->post('semester_id');
			$this->view_data['course_id'] = $course_id = $this->input->post('course_id');
			
			$this->view_data['years'] = $this->M_years->get($year_id,array('id','year'));
			$this->view_data['semesters'] = $this->M_semesters->get($sem_id,array('id','name'));
			$this->view_data['courses'] = $this->M_courses->get_courses($course_id);
			
			$this->view_data['students'] = $students = $this->M_enrollments->get_enrollments_for_ched_report($course_id,$year_id,$sem_id,$this->open_semester->year_from,$this->open_semester->year_to);
		
			$b = 0;
			
			$no_subjects = 0;
			
			if($students){

				foreach($students as $student)
				{
					$this->view_data['studentsubjects'][$student->id] = $studentsubjects = $this->M_student_subjects->get_student_subjects($student->id,$course_id,$year_id,$sem_id);
					
					if($studentsubjects)
					{
						if($no_subjects < count($studentsubjects))
						{
							$no_subjects = count($studentsubjects);
						}
					}
				}
				
				
				$this->view_data['no_subjects'] = $no_subjects;
			
			
				//IF POST IS DOWNLOAD
				if($this->input->post('submit') == "Download Excel")
				{	
					// $this->disable_layout = true;
					// $this->disable_views = true;
					
					$this->output->enable_profiler(FALSE);
					
					$this->download_excel($this->view_data);
				}
			}
			
		}
	}
	
	public function download_excel($data = false)
	{
		if($data == false) { show_404(); }

		// $this->output->enable_profiler(FALSE);

		// $this->disable_layout = true;
		
		$this->load->library('../controllers/_the_downloadables');
		
		$this->_the_downloadables->_generate_ched_report($data);		
	}
	
	public function show($year_id,$sem_id,$course_id)
	{
		// $this->disable_layout = TRUE;
		
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_years','M_semesters','M_courses','M_enrollments','M_student_subjects'));
		
		$this->view_data['year_id'] = $year_id;
		$this->view_data['sem_id'] = $sem_id;
		$this->view_data['course_id'] = $course_id;
		$year = $this->M_years->get($year_id,array('year'));
		$semester = $this->M_semesters->get($sem_id,array('name'));
		$course = $this->M_courses->get($course_id,array('course'));
		
		$this->view_data['title'] = $year->year." | ".$semester->name." | ".$course->course;
		
		$this->view_data['students'] = $students = $this->M_enrollments->get_enrollments_for_ched_report($course_id,$year_id,$sem_id,$this->open_semester->year_from,$this->open_semester->year_to);
		// var_dump($this->view_data['students']);
		$b = 0;
		
		if($students){

		foreach($students as $student):
			$this->view_data['studentsubjects'] = $studentsubjects = $this->M_student_subjects->get_student_subjects($student->id,$course_id,$year_id,$sem_id);
		endforeach;
		
		foreach($students as $student):
		$a = 0;
		
		foreach($studentsubjects as $studentsubject):
		if($student->id == $studentsubject->enrollmentid){
		$a++;
		}
		endforeach;
		
		if($a > $b){
		$b = $a;
		}else{
		$b = $b;
		}
		endforeach;
		
		}
		
		$this->view_data['no_subjects'] = $b;
		
		//$this->view_data['studentsubjects'] = $this->M_student_subjects->get_all_student_subjects();
	}
	
	public function print_show($year_id,$sem_id,$course_id)
	{
		$this->session_checker->check_if_alive();
		
		
		if($course_id !== false AND $year_id !== false AND $sem_id !==false)
		{
			$this->load->model(array('M_enrollments','M_student_subjects','M_settings'));

			$this->load->helper('print_ched');
			
			$setting = $this->M_settings->get_settings();
			$students = $this->M_enrollments->get_enrollments_for_ched_report($course_id,$year_id,$sem_id,$this->setting->year_from,$this->setting->year_to);
			
			$b = 0;
		
		if($students){

		foreach($students as $student):
			$studentsubjects = $this->M_student_subjects->get_student_subjects($student->id,$course_id,$year_id,$sem_id);
		endforeach;
		
		foreach($students as $student):
		$a = 0;
		
		foreach($studentsubjects as $studentsubject):
		if($student->id == $studentsubject->enrollmentid){
			$a++;
		}
		endforeach;
		
		if($a > $b){
		$b = $a;
		}else{
		$b = $b;
		}
		endforeach;
		
		}
		
			// print_html defined at helpers/print_ched_helper.php
			$html = print_html($students,$setting,$b,$studentsubjects); 
			$this->load->library('mpdf');
		
			$mpdf=new mPDF('','FOLIO','','',5,5,5,5,0,0); 

			$mpdf->AddPage('L');

			$mpdf->WriteHTML($html);

			$mpdf->Output();
		}else{
			show_404();
		}
		
	}
}