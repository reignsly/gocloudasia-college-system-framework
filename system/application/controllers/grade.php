<?php
	
class Grade Extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('M_enrollments', 'M_student_grades', 'M_issues','M_grading_periods','M_grading_system','M_grades_file'));
		$this->session_checker->open_semester();
		$this->load->helper(['text','grade']);
	}
	
	public function view($id = false,$search_type = "all")
	{
		if($id === false){ show_404(); }

		//CONTROLLERS THAT CAN ACCESS THIS METHOD
		$this->check_access_studentprofile_menu();
		
		//CHECK DEPARTMENT STUDENT PROFILE ACCESS 
		$this->check_stud_access('stud_grade');

		//Grading System
		$this->view_data['edit_grade_by_period'] = true;
		$this->view_data['recalculate_grade'] = true;
		$this->view_data['c_grading_period'] = $cgp = $this->current_grading_period;
		$this->view_data['is_final_gp'] = $ifg = $this->M_grading_periods->is_current_last_gp();
		$this->view_data['grading_system'] = $gs = isset($this->syspar->grading_system) && $this->syspar->grading_system ? $this->syspar->grading_system : 'input';
		$this->view_data['grading_table'] = $this->M_grading_system->get_system();

		//LOAD MODELS
		$this->load->model('M_student_subjects','m_ss');
		$this->load->model('M_subjects2','m_s');
		$this->load->model('M_grading_periods','m_gp');
		$this->load->model('M_grades_file','m_gf');

		$this->M_grades_file->recompute($id);

		#load student library
		set_time_limit(0);
		$this->load->library('_student',array('enrollment_id'=>$id));
		$this->view_data['student'] = $p = $this->_student->profile;
		$this->view_data['grading_periods'] = $this->m_gp->get_all();
		$this->view_data['grades'] = $sg = $this->_student->get_student_subject_grades();
		$this->view_data['student_grades'] = $this->_student->student_subjects;
		$this->view_data['_student'] = $this->_student;
		$this->view_data['id'] = $id;
		$this->view_data['max_grade'] = $max = $this->M_grading_system->maximum_score();
		$this->view_data['min_grade'] = $min = $this->M_grading_system->minimum_score();
		
		//If Enrollment already archived do not allow to edit grade
		$this->view_data['mydepartment']->stud_edit_grade = $p->archived === "1" ? 0 : $this->view_data['mydepartment']->stud_edit_grade;

		//Top Search Parameter
		$this->view_data['search_type'] = $search_type;
	  
	  // Count Issues
		$this->issue_count($this->_student->profile->user_id);

	 // Grade Editing
		if($this->input->post('update_grade') && $this->departments->stud_edit_grade === "1")
		{
			$s = $this->m_gf->update_grade($this->input->post());
			$this->_msg($s->code, $s->msg,current_url());
		}
	}

	public function view2($id)
	{
		//OLD
		show_404();
		if($id === false){ show_404(); }

		//CONTROLLERS THAT CAN ACCESS THIS METHOD
		$this->menu_access_checker(array(
			'search/search_student',
			'enrollees/daily_enrollees',
			'search/master_list_by_course',
			'search/search_enrollee',
			'search/list_students/paid',
			'search/list_students/unpaid',
			'search/list_students/fullpaid',
			'search/list_students/partialpaid',
		));
		
		//CHECK DEPARTMENT STUDENT PROFILE ACCESS 
		$this->check_student_profile_access('grade');

		//LOAD MODELS
		$this->load->model('M_student_subjects','m_ss');
		$this->load->model('M_subjects2','m_s');
		$this->load->model('M_grading_periods','m_gp');

		#load student library
		set_time_limit(0);
		$this->load->library('_student',array('enrollment_id'=>$id));
		$this->view_data['student'] = $this->_student->profile;
		$this->view_data['grading_periods'] = $this->m_gp->get_all();
		$this->view_data['student_grades'] = $this->_student->student_subjects;
		$this->view_data['_student'] = $this->_student;
		$this->view_data['id'] = $id;
	  // vd($sg);
	  // Count Issues
		  $this->issue_count($this->_student->profile->user_id);

	 // Grade Editing
		if($_POST)
		{
			$subjects = $this->input->post('subject');
			// vd($subjects);
			if(is_array($subjects)){
				foreach ($subjects as $ss_id => $cat) {
					
					//GET STUDENT SUBJECT IF EXIST
					$stud_sub = $this->m_ss->pull($ss_id,array('id','preliminary','midterm','finals','remarks'));
					if($stud_sub)
					{	
						unset($grade);
						$grade['preliminary'] = trim($cat['preliminary']);
						$grade['midterm'] = trim($cat['midterm']);
						$grade['finals'] = trim($cat['finals']);
						$grade['remarks'] = trim($cat['remarks']);

						//CHECK IF GRADE IS REALLY CHANGED OR NOT BEFORE SAVING
						foreach ($grade as $key => $value) {
							if($value != $stud_sub->$key){
								unset($data);
								$data[$key] = $value;
								$data['user_id'] = $this->userid;
								$rs = $this->m_ss->update($ss_id, $data);

								$log['Updated by'] = $this->userlogin;
								$log['Category'] = $key;
								$log['Grade'] = $value;
								activity_log("Update Grade",$this->userlogin,'Data : '.arr_str($log));
							}
						}
					}
				}
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Grade successfully updated.</div>');
				redirect('/grade/view/'.$id);
			}
		}
	}


	// Update
	public function edit_grade($id,$eid)
	{
		
		$this->view_data['student_grade_category'] = $this->M_student_grades->get_sgc($id);
		
		if($_POST)
		{
			$data['value'] = $this->input->post('value');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_student_grades->update_student_grade_category($data, $id);
			
			if($result['status'])
			{
				activity_log('Update Grade',$this->userlogin,'Grade Id: '.$id.' Value : '.$data['value']);
				
				log_message('error','Student Grade Updated by: '.$this->user.'Success; Student Grade Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Grade successfully updated.</div>');
				redirect('/grade/view/'.$eid);
			}
		}
	}
	
	/**
	 * Recompute Grade Conversion
	 * @param string $hash Enrollment Id Hash
	 */
	public function recalculate($hash='', $search_type = '')
	{
		$id = $this->check_hash($hash);
		$this->M_enrollments->check_id($id);

		$this->load->model('M_grades_file');

		$rs = $this->M_grades_file->recompute($id);
		if($rs){
			$this->_msg('s','Recomputation of grades was successful.', "grade/view/$id/$search_type");
		}else{
			$this->_msg('e','Either grades already updated or the process failed.', "grade/view/$id/$search_type");
		}
	}
}
