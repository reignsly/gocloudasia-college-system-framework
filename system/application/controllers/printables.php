<?php

Class Printables extends MY_Controller {

  public function __contuct()
  {
    parent::__construct();
    $this->disable_layout = true;
    $this->disable_menus = true;
    $this->disable_views = true; 
  }
  
  /**
   * Enrollment Registration Form
   * @param  int $enrollment_id enrollments table id or temp_enrollments table id
   * @param  string  $cat           if temp use table temp_enrollments else enrollments
   * @param  string  $copy          whose copy student/finance/registrar
   */
  public function registration_form($enrollment_id = false, $copy = 'student'){

      $this->load->model(array('M_enrollments', 'M_settings','M_student_subjects','M_temp_enrollments','M_temp_student_subjects'));
      $this->load->helper('time');

      //LOAD STUDENT OBJECTS FROM LIBRARY
      set_time_limit(0);
      $this->load->library('_student',array('enrollment_id'=>$enrollment_id));
      $this->view_data['_student'] = $this->_student;
      $data['profile'] = $p =  $this->_student->profile; if($p==false){ show_404(); }
      $data["subjects"] = $this->_student->student_subjects;
      $data['fees'] = $f = $this->_student->get_student_fee_profile();
      $data['plan_modes'] = $pm = $this->_student->student_plan_modes;
      $data['payment_total'] = $pt = $this->_student->student_payment_totals;
      $data['school'] = $s = $this->setting;
      // vp($pm);
      // vd($s);
    	$data["enrollment"] = $enrollment;
      $data["setting"] = $this->M_settings->get_settings();
      $data["title"] = "CERTIFICATE OF REGISTRATION";
      $data['user'] = $copy;
    	$this->load->library('mpdf');
    	$html = $this->load->view('printables/registration_form', $data, true);	

      $debug = true;
      $debug = false;

      if($debug){
        echo $html;
        exit(0);
      }

      $mpdf=new mPDF('','FOLIO','','',10,10,10,10,0,0); 
      $mpdf->WriteHTML($html);
      $mpdf->Output();
  }  

  public function certificate_of_enrollment($enrollment_id = false){

    $this->load->model(array('M_enrollments', 'M_settings','M_student_subjects', 'M_certificate_of_enrollment_settings'));

    #load library student subjects
    $this->load->library('_Student',array('enrollment_id'=>$enrollment_id));
    $enrollment = $p = $this->_student->profile; if($enrollment == false){ show_404(); } 
    $title = $this->M_enrollments->gender_title($enrollment->sex);
    $certificate = $this->M_certificate_of_enrollment_settings->get_certificate_of_enrollment();
    $studentsubjects = $this->_student->student_subjects;
  	// $student_subjects = $this->M_student_subjects->get_student_subjects($enrollment_id,$p->course_id,$p->year_id,$p->sem_id);
  	// $subject_units = $this->M_student_subjects->get_studentsubjects_total($student_subjects);

    //Variable For Printing
    $data["studentsubjects"] = $studentsubjects ;
    $data["enrollment"] = $enrollment;
    $data["setting"] = $this->M_settings->get_settings();
    $data["certificate"] = $certificate;
    $data["title"] = $title;
    $this->load->library('mpdf');
    $html = $this->load->view('printables/certificate_of_enrollment', $data, true);	
    $mpdf=new mPDF('','FOLIO','','',5,5,5,5,0,5); 
    $mpdf->SetHTMLFooter('
    <!--div id="signatory_certificate">
    <b>Name of Registrar</b><br />
    OIC-Registrar
    </div-->
    <div class="clear"></div>

    <div id="bottom-left">
    Not Valid without<br />
    School Seal
    </div>
    <div id="bottom-right"> 
    THIS IS A COMPUTER GENERATED FORM
    </div>
    <div class="clear"></div>

    ');
    $mpdf->WriteHTML($html);
    $mpdf->Output();
  }  

  public function certificate_of_grade($enrollment_id = false){

    $this->load->model(array('M_student_grades','M_enrollments', 'M_settings','M_student_subjects', 'M_certificate_of_enrollment_settings'));
    $enrollment = $p = $this->M_enrollments->profile($enrollment_id);
    $title = $this->M_enrollments->gender_title($enrollment->sex);
    $certificate = $this->M_certificate_of_enrollment_settings->get_certificate_of_enrollment();
    $student_subjects = $this->M_student_subjects->get_student_subjects($enrollment_id,$p->course_id,$p->year_id,$p->sem_id);
    $subject_units = $this->M_student_subjects->get_studentsubjects_total($student_subjects);
    $grade = $this->M_student_grades->get_student_grades($enrollment_id);

    //load student library
    $this->load->library('_student',array('enrollment_id'=>$enrollment_id));
    $data['subjects'] = $sub = $this->_student->student_subjects;

	   //Variable For Printing
    $data["enrollment"] = $enrollment;
    $data["setting"] = $this->M_settings->get_settings();
    $data["certificate"] = $certificate;
    $data["title"] = $title;
    $data["grade"] = $grade; 
    $this->load->library('mpdf');
    $html = $this->load->view('printables/certificate_of_grades', $data, true);	
    // echo $html;
    // exit(0);
    $mpdf=new mPDF('','FOLIO','','',15,7,10,5,0,10); 
    $mpdf->SetHTMLFooter('
      <!--div id="signatory_certificate">
      <b>Name of Registrar</b><br />
      OIC-Registrar
      </div-->
      <div class="clear"></div>

      <div id="bottom-left">
      Not Valid without<br />
      School Seal
      </div>
      <div id="bottom-right"> 
      THIS IS A COMPUTER GENERATED FORM
      </div>
      <div class="clear"></div>

      ');
    $mpdf->WriteHTML($html);
    $mpdf->Output();
  }  

  public function transcript($enrollment_id = false){
    $this->load->model(array("M_settings", "M_student_grades", "M_enrollments"));
  	  
    //Top header data of student
    $this->view_data['student'] = $p = $this->M_enrollments->profile($enrollment_id);
    $this->view_data['enrollment_id'] = $enrollment_id;
  			
    // Get Grades
    $student_grades = $this->M_student_grades->get_student_grades_transcript_asc($p->studid);



  	//Variable For Printing
  	$data["student_grades"] = $student_grades;
          $data["setting"] = $this->M_settings->get_settings();
  	$this->load->library('mpdf');
  	$html = $this->load->view('printables/transcript', $data, true);	
  	$mpdf=new mPDF('','FOLIO','','',5,5,5,5,0,5); 
  	$mpdf->WriteHTML($html);
  	$mpdf->Output();
  }  
}
