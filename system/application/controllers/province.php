<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class province extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->session_checker->secure_page('admin');
		$this->load->model(array('M_province'));
	}
	
	public function insert_provinces()
	{
		show_404();
		$country = array(
				 "Abra",
				  "Agusan del Norte",
				  "Agusan del Sur",
				  "Aklan",
				  "Albay",
				  "Antique",
				  "Apayao",
				  "Aurora",
				  "Basilan",
				  "Bataan",
				  "Batanes",
				  "Batangas",
				  "Benguet",
				  "Biliran",
				  "Bohol",
				  "Bukidnon",
				  "Bulacan",
				  "Cagayan",
				  "Camarines Norte",
				  "Camarines Sur",
				  "Camiguin",
				  "Capiz",
				  "Catanduanes",
				  "Cavite",
				  "Cebu",
				  "Compostela Valley",
				  "Cotabato",
				  "Davao del Norte",
				  "Davao del Sur",
				  "Davao Oriental",
				  "Eastern Samar",
				  "Guimaras",
				  "Ifugao",
				  "Ilocos Norte",
				  "Ilocos Sur",
				  "Iloilo",
				  "Isabela",
				  "Kalinga",
				  "La Union",
				  "Laguna",
				  "Lanao del Norte",
				  "Lanao del Sur",
				  "Leyte",
				  "Maguindanao",
				  "Marinduque",
				  "Masbate",
				  "Metro Manila",
				  "Misamis Occidental",
				  "Misamis Oriental",
				  "Mountain Province",
				  "Negros Occidental",
				  "Negros Oriental",
				  "Northern Samar",
				  "Nueva Ecija",
				  "Nueva Vizcaya",
				  "Occidental Mindoro",
				  "Oriental Mindoro",
				  "Palawan",
				  "Pampanga",
				  "Pangasinan",
				  "Quezon",
				  "Quirino",
				  "Rizal",
				  "Romblon",
				  "Samar",
				  "Sarangani",
				  "Siquijor",
				  "Sorsogon",
				  "South Cotabato",
				  "Southern Leyte",
				  "Sultan Kudarat",
				  "Sulu",
				  "Surigao del Norte",
				  "Surigao del Sur",
				  "Tarlac",
				  "Tawi Tawi",
				  "Zambales",
				  "Zamboanga del Norte",
				  "Zamboanga del Sur",
				  "Zamboanga Sibugay",
				  "Others"
			);
			
		foreach($country as $prov)
		{
			$data['created_at'] = NOW;
			$data['updated_at'] = NOW;
			$data['province'] = $prov;
			
			$this->M_province->create_province($data);
		}
	}
	
	// Create
	public function create()
	{
		$this->load->model(array('M_province'));
		
		$this->view_data['province'] = FALSE;
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($_POST)
		{
			$data = $this->input->post('province');
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_province->create_province($data);
			
			if($result['status'])
			{
				log_message('error','province Created by: '.$this->user.'Success; province Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Province successfully added.</div>');
				redirect('province');
			}
		}
	}
	
	// Retrieve
	public function index()
	{
		$this->load->model(array('M_province'));
		$this->view_data['province'] = $this->M_province->find_all();
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
	}
	
	public function display($id)
	{
		$this->load->model(array('M_province'));
		
		$this->view_data['province'] = $this->M_province->get_province($id);
	}
	
	// Update
	public function edit($id)
	{
		$this->load->model(array('M_province'));
		
		$this->view_data['province'] = $this->M_province->get_province($id);
		
		if($_POST)
		{
			$data = $this->input->post('province');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_province->update_province($data, $id);
			
			if($result['status'])
			{
				log_message('error','province Updated by: '.$this->user.'Success; province Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Province successfully updated.</div>');
				redirect('province');
			}
		}
	}
	
	public function destroy($id)
	{
		$this->load->model(array('M_province'));
		
		$result = $this->M_province->delete_province($id);
		log_message('error','province Deleted by: '.$this->user.'Success; province Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Province successfully deleted.</div>');
		redirect('province');
	}
}
