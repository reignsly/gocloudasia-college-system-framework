<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('M_users','M_departments','M_settings'));
		$this->load->library(array('form_validation'));
		$this->load->helper(array('url','form'));
		
		$this->settings 	= $this->M_settings->get_settings();

		/*
		if($this->session->userdata('logged_in') == TRUE AND strtolower($this->uri->segment(1)) !== 'logout')
		{
			redirect($this->session->userdata('userType'));
		}
		*/
	}
	
	function get_login_types()
	{
		$ret = array();
		$rs = $this->M_departments->get_all_department_for_welcome_screen(1);
		if($rs)
		{
			foreach($rs as $obj)
			{
				$ret[] = $obj->department;
			}
		}
		
		return $ret;
	}

	private function _captcha()
	{
		$this->load->helper('captcha');
		$system_counted_hash = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));

		$vals = array(
			'word'	 	 => str_replace('0','', substr(md5(rand(1,100).$system_counted_hash),3,rand(2,5))),
			'img_path'	 => 'assets/captcha/',
			'img_url'	 => base_url().'assets/captcha/',
			'font_path'  => 'assets/fonts/comicbd.ttf',
			'img_width'	 => 220,
    	'img_height' => 50,
			'expiration' => 7200
			);
		
		$cap = create_captcha($vals);
		$image['word'] = $cap['word'];
		$image['image'] ='<image src="'.base_url().'assets/captcha/'.$cap['time'].'.jpg" class="img-responsive"/>';
		$image['link'] = 'assets/captcha/'.$cap['time'].'.jpg';

		return (object)$image;
	}
	
	/*12-6-12 Logs the user in the system*/
	function login($userType = false)
	{
		$get = array(
				'where' => array(
						'visible' => 1,
						'department <>' => 'student'
					),
			);
		$this->view_data['departments'] = $d = $this->M_departments->get_where_dd(false,$get, false, array('department','description')); 
		$this->view_data['department'] = $userType; 

		if(!$userType){ show_404(); }

		$captcha = $this->_captcha(); //generate captcha
		$this->session->set_flashdata('image_url',$captcha->link); // save path of generated captcha to session

		$login_types = $this->get_login_types();
		if(in_array(strtolower(strip_tags(htmlspecialchars($userType))),$login_types))
		{
			$this->layout_view = $this->welcome_view;
			$this->load->library(array('form_validation','login'));		
			$this->view_data['UserType'] = $userType;
			$this->view_data['cap_image'] = $captcha->image;
			#endregion
			
			if($_POST)
			{
				$this->view_data['login'] = $this->input->post('login');
				
				$this->form_validation->set_rules('login', 'login', 'required|strip_tags|htmlspecialchars');
				$this->form_validation->set_rules('password', 'password', 'required|strip_tags|htmlspecialchars');
				$this->form_validation->set_rules('department', 'departments', 'required|strip_tags|htmlspecialchars');
				$this->form_validation->set_rules('captcha', 'Captcha', 'required|callback_validate_captcha');
				
				$userType = $this->input->post('department');
				if($this->form_validation->run() !== FALSE)
				{
					
					$val = $this->login->_validate_password($_POST['login'],$_POST['password'], $userType);
				
					if($val == FALSE)
					{
						$this->session->set_userdata('word',$captcha->word);
						activity_log('LOGIN ATEMPT FAILED', "","Username:".$_POST['login']." Password:".$_POST['password']." Department:".$userType);
						$this->view_data['system_message'] = '<div class="alert alert-danger">Invalid username/password Combination</div>';
					}
					else
					{
						$this->verify_user($val);
					}
				}
				else
				{
					$this->session->set_userdata('word',$captcha->word);
				}
			}
			else
			{
				$this->session->set_userdata('word',$captcha->word);
			}
		}else{
			show_404();
		}
	}

  public function validate_captcha()
	{
		$captcha_word = md5(strtolower($this->session->userdata('word')));
		$sent_captcha_text = md5(strtolower($this->input->post('captcha')));
		$captcha_image_link = $this->session->flashdata('image_url');
		// vp($captcha_word);
		// vd($sent_captcha_text);
		if($captcha_word !== $sent_captcha_text){
				$this->form_validation->set_message('validate_captcha', 'Wrong captcha code. Please try again.');
				return false;
		}

		$this->session->set_userdata('word',''); //destroy captcha from session
		unlink($captcha_image_link);//delete captcha image from file

		return true;
	}
	
	public function validate_captcha_from_db()
	{
		$cap = $this->input->post('captcha');
		$ip = $this->input->ip_address();
		
		$expiration = time()-7200; // Two hour limit
		$this->M_captcha->delete_recent($expiration);

		$rs = $this->M_captcha->check_captcha($cap, $ip, $expiration);
		
		if($rs == false)
		{
			$this->form_validation->set_message('validate_captcha', 'Wrong captcha code. Please try again.');
			return false;
		}
        return true;
	}

  function logout()
  {

  	$this->load->model('M_users');
  	$user_ = $this->M_users->pull($this->userid, 'id, name, department, login');
  	if($user_){
  		activity_log('LOGOUT', $user_->login,"User ID : ".$user_->id." Name : ".$user_->name ." Department : ".$user_->department);
  	}
    // $this->session->sess_destroy();

  	$userdata = array(
			'userid' => '',
			'username' => '',
			'userlogin' => '',
			'userType' => '',
			'logged_in' => FALSE
		);
		
		$this->session->set_userdata($userdata);

		$this->session->set_flashdata('system_message', $this->_msg());
		redirect('logout');
  }
	
	public function verify_user($id=FALSE)
	{
		$this->session_checker->verify_user($id);
	}
}
