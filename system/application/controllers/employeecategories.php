<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employeecategories extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		// $this->session_checker->secure_page('admin');
		$this->menu_access_checker();
	}
	
	// Create
	public function create()
	{
		$this->load->model(array('M_employeecategories'));
		
		$this->view_data['employeecategory'] = FALSE;
		
		if($_POST)
		{
			$data = $this->input->post('employeecategories');
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_employeecategories->create_employeecategories($data);
			
			if($result['status'])
			{
				$id = $result['id'];
				activity_log('Create employee category',$this->userlogin,'Created by: '.$this->userlogin.'Success;Employee Category Id : '.$id);
				
				log_message('error','Employee categories Created by: '.$this->user.'Success; Employee Categories Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Employee Category successfully added.</div>');
				redirect('employeecategories');
			}
		}
	}
	
	// Retrieve
	public function index()
	{
		$this->load->model(array('M_employeecategories'));
		$this->view_data['employeecategories'] = $this->M_employeecategories->find_all();
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
	}
	
	public function display($id)
	{
		$this->load->model(array('M_employeecategories'));
		
		$this->view_data['employeecategories'] = $this->M_employeecategories->get_employeecategories($id);
	}
	
	// Update
	public function edit($id)
	{
		$this->load->model(array('M_employeecategories'));
		
		$this->view_data['employeecategories'] = $this->M_employeecategories->get_employeecategories($id);
		
		if($_POST)
		{
			$data = $this->input->post('employeecategories');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_employeecategories->update_employeecategories($data, $id);
			
			if($result['status'])
			{
				activity_log('update employee category',$this->userlogin,'Updated by: '.$this->userlogin.'Success;Employee Category Id : '.$id);
				
				log_message('error','Employee Categories Updated by: '.$this->user.'Success; Employee Categories Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Employee Categories successfully updated.</div>');
				redirect('employeecategories');
			}
		}
	}
	
	// Update
	public function destroy($id =false )
	{
		if(!$id){ show_404(); }
		$this->load->model(array('M_employeecategories'));
		$p = $this->M_employeecategories->get_employeecategories($id);
		if($p):
			$result = $this->M_employeecategories->delete_employeecategories($id);
			activity_log('update employee category',$this->userlogin,'Updated by: '.$this->userlogin.'Success;Employee Category Id : '.$id.' Category Name : '.$p->category);
			log_message('error','Employee Categories Deleted by: '.$this->user.'Success; Employee Categories Id: '.$id);
			$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Employee Categories successfully deleted.</div>');
			redirect('employeecategories');
		endif;
	}
}