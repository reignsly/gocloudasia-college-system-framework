<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee_attendance extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		// $this->session_checker->secure_page('hrd');
		$this->menu_access_checker(array(
			'employee_attendance/calendar'
		));
	}
	
	// Create
	public function create()
	{
		$this->load->library('form_validation');
		
		$this->load->model(array('M_employees','M_career_services','M_work_experiences','M_voluntary_works','M_training_programs','M_other_informations','M_users'));
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($_POST)
		{
			if($this->form_validation->run('users') !== FALSE)
			{
				$all = $this->input->post('user');
				
				$employee_attr = $all['employees_attributes'];
				
				$personal_info = $employee_attr['personal_info'];
				
				$personal_info['created_at'] = date('Y-m-d H:i:s');
				$personal_info['updated_at'] = date('Y-m-d H:i:s');
				
				$result = $this->M_employees->create_employee($personal_info);
				
				if($result['status'])
				{
					$id = $result['id'];
				
					#region SAVE CAREER SERVICES
					foreach($employee_attr['career_services_attributes'] as $key => $value){
						if(trim($value['career']) != ""){
						
							$value['employee_id'] = $id;
							$value['created_at'] = date('Y-m-d H:i:s');
							$value['updated_at'] = date('Y-m-d H:i:s');
								
							$career_result = $this->M_career_services->create_career_services($value);
						}
					}
					#endregion
					
					#region SAVE WORK EXPERIENCES
					foreach($employee_attr['work_experiences_attributes'] as $key => $value){
						
						if(trim($value['position']) != ""){
						
							$value['employee_id'] = $id;
							$value['created_at'] = date('Y-m-d H:i:s');
							$value['updated_at'] = date('Y-m-d H:i:s');
								
							$we_result = $this->M_work_experiences->create_work_experiences($value);
						}
					}
					#endregion
					
					#region SAVE VOLUNTARY WORKS
					foreach($employee_attr['voluntary_works_attributes'] as $key => $value){
					
						if(trim($value['name']) != ""){
						
							$value['employee_id'] = $id;
							$value['created_at'] = date('Y-m-d H:i:s');
							$value['updated_at'] = date('Y-m-d H:i:s');
								
							$vo_result = $this->M_voluntary_works->create_voluntary_works($value);
						}
					}
					#endregion
					
					#region SAVE TRAINING PROGRAMS
					foreach($employee_attr['training_programs_attributes'] as $key => $value){
					
						if(trim($value['title']) != ""){
						
							$value['employee_id'] = $id;
							$value['created_at'] = date('Y-m-d H:i:s');
							$value['updated_at'] = date('Y-m-d H:i:s');
								
							$tp_result = $this->M_training_programs->create_training_programs($value);
						}
					}
					#endregion
					
					#region SAVE OTHER INFORMATION
					foreach($employee_attr['other_informations_attributes'] as $key => $value){
					
						if(trim($value['recognition']) != ""){
						
							$value['employee_id'] = $id;
							$value['created_at'] = date('Y-m-d H:i:s');
							$value['updated_at'] = date('Y-m-d H:i:s');
								
							$tp_result = $this->M_other_informations->create_other_informations($value);
						}
					}
					#endregion
					
					#region CREATE USER
					$user_info['login'] = trim($personal_info['employeeid']);
					$user_info['employee_id'] = $id;
					$user_info['name'] = trim($personal_info['first_name']).' '.trim($personal_info['middle_name']).' '.trim($personal_info['last_name']);
					$user_info['email'] = trim($personal_info['email']);
					$user_info['created_at'] = date('Y-m-d H:i:s');
					$user_info['updated_at'] = date('Y-m-d H:i:s');
					$user_info['is_activated'] = 1;
					$user_info[strtolower(trim($personal_info['role']))] = 1;
					
					//TO DO LIST PASSWORD GENERATION
					
					$user_result = $this->M_users->create_users($user_info);
					#endregion
					
					activity_log('create employee attendance',$this->userlogin,'User/employee attendance Created by: '.$this->user.'Success; Employee Id: '.$id,' User ID : '.$user_result['id']);
					
					log_message('error','Employee Created by: '.$this->user.'Success; Employee Id: '.$id);
					log_message('error','User Created by: '.$this->user.'Success; User Id: '.$id);
					$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Employee successfully added.</div>');
					redirect('employees/create');
				}
			}
		}
	}
	
	
		// Retrieve
	public function index()
	{
		$this->load->model(array('M_employees','M_career_services','M_work_experiences','M_voluntary_works','M_training_programs','M_other_informations','M_users'));
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
	}
	
	public function calendar($year ='',$month ='')
	{
		$this->load->model(array('M_employeeattendances'));
		
		
		$year = $year == '' ? date('Y',time()) : $year;
		$month = $month == '' ? date('m',time()) : $month;
		
		$prefs['template'] = '

		   {table_open}<table class="attendance_calendar" border="1" cellpadding="2" cellspacing="0">{/table_open}

		   {heading_row_start}<tr>{/heading_row_start}
		   {heading_previous_cell}<th colspan="7"><center><a href="{previous_url}" class="btn"><i class="icon-step-backward" id="icon-step-backward"><span class="glyphicon glyphicon-chevron-left" ></span> </i></a>{/heading_previous_cell}
		   {heading_title_cell}<span style="font-size:16px;">{heading}</span>{/heading_title_cell}
		   {heading_next_cell}<a href="{next_url}" class="btn"><i class="icon-step-forward" id="icon-step-forward"><span class="glyphicon glyphicon-chevron-right" ></span> </i></a>{/heading_next_cell}
		   {heading_row_end}<center></th></tr>{/heading_row_end}
		   
		   
		   {week_row_start}<tr>{/week_row_start}
		   {week_day_cell}<td class="center"><span class="label">{week_day}</span></td>{/week_day_cell}
		   {week_row_end}</tr>{/week_row_end}

		   {cal_row_start}<tr>{/cal_row_start}
		   {cal_cell_start}<td class="day-format">{/cal_cell_start}

		   {cal_cell_content}{day}<br>{content}{/cal_cell_content}
		   {cal_cell_content_today}<div class="highlight" id="day_today">{day}<br> {content}</div>{/cal_cell_content_today}

		   {cal_cell_no_content}{day}{/cal_cell_no_content}
		   {cal_cell_no_content_today}<div class="highlight">{day}</div>{/cal_cell_no_content_today}

		   {cal_cell_blank}&nbsp;{/cal_cell_blank}

		   {cal_cell_end}</td>{/cal_cell_end}
		   {cal_row_end}</tr>{/cal_row_end}

		   {table_close}</table>{/table_close}
		';
		
		// $prefs['start_day']    = 'sunday';
		$prefs['month_type']   ='long';
		$prefs['day_type']     ='long';
		$prefs['show_next_prev']  =TRUE;
		$prefs['next_prev_url'] = base_url().'employee_attendance/calendar';
		
		$no_of_months = date("t");
		$links = array();
		for($x = 1; $x <= $no_of_months; $x++){
			
			$editlinks = "<a href='".base_url()."employee_attendance/make_attendance/".$year."/".$month."/".$x."'>Make Attendance</a><br/>";
			
			if($this->M_employeeattendances->get_employeeattendances_bydate($year, $month, $x) != false){
				$editlinks = "<a href='".base_url()."employee_attendance/edit_attendance/".$year."/".$month."/".$x."'>Edit Attendance</a><br/>";
			}
			
			$links[$x] = 
				$editlinks
				."<a href='".base_url()."employee_attendance/display_attendance/".$year."/".$month."/".$x."'>View Attendance</a>";
		}
		
		$this->load->library('calendar',$prefs);
		$this->view_data['calendar'] = $this->calendar->generate_calendar($year,$month,$links);
		
	}
	
	public function make_attendance($year, $month, $day){
	
		$this->load->model(array('M_employees','M_employeeattendances'));
		
		$this->load->library('calendar');
		
		$this->view_data['year'] = $year;
		$this->view_data['month'] = $month;
		$this->view_data['day'] = $day;
		$monthname = date("F", strtotime($year.'-'.$month.'-1'));
		$this->view_data['date'] = $monthname.' '.$day.', '.$year;
		
		$this->view_data['employees'] = $this->M_employees->load_active_employees();
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($_POST){
			
			$values = $this->input->post('attendance');
			
			foreach($values as $employeeid => $val){
				
				$data['reason'] = isset($val['reason']) ? "Absent" : "Present";
				$data['timein'] = $val['timein'];
				$data['timeout'] = $val['timeout'];
				$data['employee_id'] = $employeeid;
				$data['year'] = $year;
				$data['month'] = $month;
				$data['day'] = $day;
				$data['start_date'] = $year.'-'.$month.'-'.$day;
				$data['end_date'] = $year.'-'.$month.'-'.$day;
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['updated_at'] = date('Y-m-d H:i:s');
				
				$result = $this->M_employeeattendances->create_employeeattendances($data);				
			}
			
			activity_log('create employee attendance',$this->user,'Employee attendances created by: '.$this->user.'Success; Date: '.$data['start_date']);
			
			log_message('error','Employee attendances created by: '.$this->user.'Success; Date: '.$data['start_date']);
			$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Employee attendance successfully created.</div>');
			
			redirect('employee_attendance/make_attendance/'.$year.'/'.$month.'/'.$day);
		}
	}
	
	public function edit_attendance($year, $month, $day){
	
		$this->load->model(array('M_employees','M_employeeattendances'));
		
		$this->view_data['year'] = $year;
		$this->view_data['month'] = $month;
		$this->view_data['day'] = $day;
		$monthname = date("F", strtotime($year.'-'.$month.'-1'));
		$this->view_data['date'] = $monthname.' '.$day.', '.$year;
		
		$this->view_data['employees'] = $this->M_employees->load_active_employees();
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$this->view_data['employeeattendances'] = $this->M_employeeattendances->get_employeeattendances_bydate($year, $month, $day);
		
		if($_POST){
			
			$values = $this->input->post('attendance');
			
			foreach($values as $employeeid => $val){
				
				$data['reason'] = isset($val['reason']) ? "Absent" : "Present";
				$data['timein'] = $val['timein'];
				$data['timeout'] = $val['timeout'];
				$data['updated_at'] = date('Y-m-d H:i:s');
				
				$result = $this->M_employeeattendances->update_employeeattendances($data, $val['id']);
				
				activity_log('update employee attendance',$this->user,'Employee attendances updated by: '.$this->user.'Success; Date: '.$year.'-'.$month.'-'.$day);
				
				log_message('error','Employee attendances updated by: '.$this->user.'Success; Date: '.$year.'-'.$month.'-'.$day);
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Employee attendance successfully updated.</div>');
				
			}
			
			redirect('employee_attendance/edit_attendance/'.$year.'/'.$month.'/'.$day);
			
		}
	}
	
	public function display_attendance($year, $month, $day){
		
		$this->load->model(array('M_employees','M_employeeattendances'));
		
		$this->view_data['year'] = $year;
		$this->view_data['month'] = $month;
		$this->view_data['day'] = $day;
		$monthname = date("F", strtotime($year.'-'.$month.'-1'));
		$this->view_data['date'] = $monthname.', '.$day.' '.$year;
		
		$this->view_data['employees'] = $this->M_employees->load_active_employees();
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$this->view_data['employeeattendances'] = $this->M_employeeattendances->get_employeeattendances_bydate($year, $month, $day);
		
		// var_dump($this->view_data['employeeattendances']);
	}
	
	public function view_attendance($year, $month, $day){
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
	}
	

}