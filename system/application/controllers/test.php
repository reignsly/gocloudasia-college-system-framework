<?php
class Test extends CI_Controller {
	public function index(){
		
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('Caritas');
		$this->excel->getActiveSheet()->setCellValue('A1', 'Fullname');
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->setCellValue('B1', 'Age');
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->setCellValue('C1', 'Birth Date');
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->setCellValue('D1', 'Address');
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		$filename='caritas.xls';
		
		/* if (!headers_sent()) {
		  foreach (headers_list() as $header)
			if($header){
				$h = explode(":", $header);
				header_remove($h[0]);
			}
		} */
		
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		$objWriter->save('php://output');
	}
}
?>
