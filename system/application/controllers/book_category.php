<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Book_category extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->menu_access_checker();
	}
	
	// Create
	public function create()
	{
		$this->load->model(array('M_librarycategory'));
		
		$this->view_data['libraries'] = FALSE;
		
		if($_POST)
		{
			$data = $this->input->post('librarycategory');
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_librarycategory->create_librarycategory($data);
			
			if($result['status'])
			{
				activity_log('create book category',$this->userlogin,'Library Category Created by: '.$this->user.'Success; Library Category Id: '.$result['id']);
				
				log_message('error','Library Category Created by: '.$this->user.'Success; Library Category Id: '.$result['id']);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Library Category successfully added.</div>');
				redirect(current_url());
			}
		}
	}
	
	// Retrieve
	public function index($page=0)
	{
		$this->load->model(array('M_librarycategory'));
		$this->load->helper('my_dropdown');
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."books/index";
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_librarycategory->find_all(0,0,false, true, true);
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$this->view_data['librarycategory'] = $this->M_librarycategory->find_all($page, $config["per_page"]);
		$this->view_data['links'] = $this->pagination->create_links();
	}
	
	// Update
	public function edit($id = false)
	{
		if($id == false){ show_404(); }
		
		$this->load->model(array('M_librarycategory'));
		
		$this->view_data['librarycategory'] = $this->M_librarycategory->get_librarycategory($id);
		
		if($_POST)
		{
			$data = $this->input->post('librarycategory');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_librarycategory->update_librarycategory($data, $id);
			
			if($result['status'])
			{
				activity_log('update book category',$this->userlogin,'Book Category Updated by: '.$this->user.'Success; Book Category Id: '.$id);
				
				log_message('error','Book Category Updated by: '.$this->user.'Success; Book Category Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Book Category successfully updated.</div>');
				redirect(current_url());
			}
		}
	}
	
	// Update
	public function destroy($id = false)
	{
		if($id == false){ show_404(); }
		$this->load->model(array('M_librarycategory'));
		
		$result = $this->M_librarycategory->delete_librarycategory($id);
		
		activity_log('destroy book category',$this->userlogin,'Book Category Deleted by: '.$this->user.'Success; Book Category Id: '.$id);
		
		log_message('error','Book Category Deleted by: '.$this->user.'Success; Book Category Id: '.$id);
		
		$this->session->set_flashdata('system_message', '<div class="alert alert-success">Book Category successfully deleted.</div>');
		redirect('book_category');
	}
}