<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {
	
  public function __construct()
	{
		parent::__construct();
		$this->session_checker->check_if_alive();
		$this->load->model(array('M_student_total_file'));

		//ONLY AJAX REQUEST CAN ACCESS THIS CONTROLLER
		if(!IS_AJAX){
			show_404();
		}
	}

	public function check_employee_duplicate(){
		$this->load->model(array('M_employees','M_users'));
		$employeeid = $this->input->post('employeeid');
		$id = $this->input->post('id');
		$par = $this->input->post('par');

		$param['employeeid'] = $employeeid;
		$param2['login'] = $employeeid;
		
		if($par == "edit")
		{
			$enrollment = $this->M_employees->get_employee($id);

			if($enrollment)
			{
				$param['employeeid <> '] = $enrollment->employeeid;
				$param2['login <> '] = $enrollment->employeeid;
			}

			$data['isduplicate'] = FALSE;

			echo json_encode($data);

			exit(0);
		}
		//CHECK IF DUPLICATE
		$data['isduplicate'] = FALSE;
		
		$rs = $this->M_employees->get_enrollment_by($param, true);
		if($rs){
			$data['isduplicate'] = true;
		}
		else
		{
			$rs = $this->M_users->get_users_by($param2, true);
			if($rs){
				$data['isduplicate'] = true;
			}
		}
		
		echo json_encode($data);
	}
	
	public function check_dublicate_key($tablename = "", $key = "")
	{
		$field = "";
		switch(strtolower($tablename))
		{
			case 'studentpayments':
				$field = "or_no";
			break;
		}
		
		$this->load->helper('my_duplicate');
		
		$data['is_duplicate'] = is_key_duplicate($tablename, $field, $key);
		
		echo json_encode($data);
		exit(0);
	}
	
	// Add Subject Ajax
	public function add_subject_ajax($subject_id, $y, $c, $s, $eid){
	  $this->load->helper('url');
		$this->load->model(array('M_student_subjects', 'M_subjects', 'M_enrollments','M_student_grades','M_studentpayments_details'));
		
		
		$data['enrollmentid'] = $eid;
		$data['subject_id'] = $subject_id;
		$data['year_id'] = $y;
		$data['semester_id'] = $s;
		$data['course_id'] = $c;
		$sgf_id = $this->M_student_grades->get_student_grade_file_id($eid);
		if (empty($sgf_id)) {
		  $sgf_data['user_id'] = $eid;
		  $sgf_id = $this->M_student_grades->create_student_grade_file($sgf_data);	
		}
		$sg_data['student_grade_file_id'] = $sgf_id ;
		$sg_data['subject_id'] = $subject_id ;
		$sg_data['subjectid'] = $subject_id ;
		$sg_data['created_at'] = NOW ;
		$sg_data['updated_at'] = NOW ;
		
		$result = $this->M_student_subjects->add_ajax($data);
		$units = $this->M_student_subjects->get_units($eid);
		$sg = $this->M_student_grades->create_student_grade($sg_data);
		$profile = $this->M_enrollments->profile($eid);
		
		$link = array();
		
		if($result['id'])
		{
		
			activity_log('add student subject',$profile->studid,'Add student subject by: '.$profile->studid.'Success; Subject Id: '.$subject_id);
			
			
			$categories = array('Preliminary', 'Midterm', 'Finals');
			foreach ($categories as $category) {
			  $sgc_data['category'] = $category;
			  $sgc_data['student_grade_id'] = $sg['id'];
			  $sgc_data['value'] = 0;
			  $sgc_data['created_at'] = NOW;
			  $sgc_data['updated_at'] = NOW;
			  $this->M_student_grades->create_student_grade_categories($sgc_data);
			}
			
			$href = base_url("ajax/destroy_subject/".$y.'/'.$c.'/'.$s.'/'.$eid.'/'.$result['id'].'/'.$subject_id);
			$link['url'] = "<a class='badge btn-danger delete_subject' href=".$href.">Delete</a> ";
			$link['units'] = $units->total_units > 0 ? $units->total_units : 0;
			$link['labs'] = $units->total_labs > 0 ? $units->total_labs : 0 ;
			
			if ($this->M_enrollments->check_if_paid($eid)) {
			  $this->M_subjects->recalculate_subject_load($subject_id, 'remove');
			}
			
			//UPDATE STUDENT TOTALS : ADD CHARGE AMOUNT PER SUBJECT
			$added_subject = $this->M_subjects->get($subject_id);//TO GET UNITS
			$enrollments = $this->M_student_total_file->get_student_total_file($eid); //TO GET TUITION FEE AMOUNT
			if($enrollments && $added_subject)
			{
				$subject_charge_amount = $enrollments->tuition_fee * $added_subject->units;
				if($subject_charge_amount > 0)
				{
					//ADD SUBJECT CHARGE TO TOTALS
					$rs = $this->M_student_total_file->add_subject_amount($eid, $subject_charge_amount, $subject_id);
					
					//REVERSE STUDENT PAYMENT DETAILS
					$this->M_studentpayments_details->reverse_studentpayment_details($eid);
					
					//UPDATE ENROLLMENT IF PAID, FULL , PARTIAL AND UPDATE SUBJECT LOADS
					$this->M_enrollments->update_enrollments_status($eid); 
				}
			}
		}
    
		echo json_encode($link);
    exit(0);
	}
	
	public function search_users($page = 0){
		
		$this->load->model(array('M_users'));
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		
		if($_POST){
			
			$login = $this->input->post('borrower_login');
			$name = $this->input->post('borrower_name');
			if(trim($login) != ""){
				$filter['users.login LIKE '] = "%".$login."%";
			}
			if(trim($name) != ""){
				$filter['users.name LIKE '] = "%".$name."%";
			}
			
			$page = 0;
		}
		
		$this->load->library("pagination");
		 $config['full_tag_open'] = '<ul class="pagination">';
		 $config['full_tag_close'] = '</ul>';
		 $config['prev_link'] = '&lt; Prev';
		 $config['prev_tag_open'] = '<li>';
		 $config['prev_tag_close'] = '</li>';
		 $config['next_link'] = 'Next &gt;';
		 $config['next_tag_open'] = '<li>';
		 $config['next_tag_close'] = '</li>';
		 $config['cur_tag_open'] = '<li class="active"><a href="#">';
		 $config['cur_tag_close'] = '</a></li>';
		 $config['num_tag_open'] = '<li>';
		 $config['num_tag_close'] = '</li>';
		 $config['first_link'] = FALSE;
		 $config['last_link'] = FALSE;
		
		$config["base_url"] = base_url() ."books/create_borrower";
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_users->find(0,0,$filter, true, true);
		$config["per_page"] = 30;
		$config['num_links'] = 5;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$this->view_data['users'] =  $users = $this->M_users->find($page, $config["per_page"], $filter);
		$this->view_data['links'] = $this->pagination->create_links();
		$this->view_data['count'] = count($users);
		
		echo json_encode($this->view_data);
		exit(0);
	}
	
	public function search_items($page = 0){
		
		$this->load->model(array('M_items'));
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		
		if($_POST){
			
			$field = $this->input->post('fields');
			$keyword = $this->input->post('keyword');
			if(trim($keyword) != ""){
				$filter[$field.' LIKE '] = "%".$keyword."%";
			}
			$page = 0;
		}
		
		$this->load->library("pagination");
		 $config['full_tag_open'] = '<ul class="pagination">';
		 $config['full_tag_close'] = '</ul>';
		 $config['prev_link'] = '&lt; Prev';
		 $config['prev_tag_open'] = '<li>';
		 $config['prev_tag_close'] = '</li>';
		 $config['next_link'] = 'Next &gt;';
		 $config['next_tag_open'] = '<li>';
		 $config['next_tag_close'] = '</li>';
		 $config['cur_tag_open'] = '<li class="active"><a href="#">';
		 $config['cur_tag_close'] = '</a></li>';
		 $config['num_tag_open'] = '<li>';
		 $config['num_tag_close'] = '</li>';
		 $config['first_link'] = FALSE;
		 $config['last_link'] = FALSE;
		
		$config["base_url"] = base_url() ."borrow_items/create";
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_items->find(0,0,$filter, true, true);
		$config["per_page"] = 30;
		$config['num_links'] = 5;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$this->view_data['items'] =  $items = $this->M_items->find($page, $config["per_page"], $filter);
		$this->view_data['links'] = $this->pagination->create_links();
		$this->view_data['count'] = count($items);
		
		echo json_encode($this->view_data);
		exit(0);
	}
	
	public function search_students($page = 0, $semester_id='', $year_from = '', $year_to = ''){
		
		$this->load->model(array('M_enrollments'));
		
		$filter = false;
		
		if($_POST){
			
			$login = $this->input->post('studid');
			$name = $this->input->post('name');
			if(trim($login) != ""){
				$filter['enrollments.studid LIKE '] = "%".$login."%";
			}
			if(trim($name) != ""){
				$filter['enrollments.name LIKE '] = "%".$name."%";
			}
			
			$filter['enrollments.semester_id'] = $semester_id;
			$filter['enrollments.sy_from'] = $year_from;
			$filter['enrollments.sy_to'] = $year_to;
			$filter['enrollments.is_deleted'] = 0;
			
			$page = 0;
		}
		
		$this->load->library("pagination");
		 $config['full_tag_open'] = '<ul class="pagination">';
		 $config['full_tag_close'] = '</ul>';
		 $config['prev_link'] = '&lt; Prev';
		 $config['prev_tag_open'] = '<li>';
		 $config['prev_tag_close'] = '</li>';
		 $config['next_link'] = 'Next &gt;';
		 $config['next_tag_open'] = '<li>';
		 $config['next_tag_close'] = '</li>';
		 $config['cur_tag_open'] = '<li class="active"><a href="#">';
		 $config['cur_tag_close'] = '</a></li>';
		 $config['num_tag_open'] = '<li>';
		 $config['num_tag_close'] = '</li>';
		 $config['first_link'] = FALSE;
		 $config['last_link'] = FALSE;
		
		$config["base_url"] = base_url() ."books/create_borrower";
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_enrollments->find(0,0,$filter, true, true);
		$config["per_page"] = 30;
		$config['num_links'] = 5;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$this->view_data['users'] =  $users = $this->M_enrollments->find($page, $config["per_page"], $filter);
		$this->view_data['links'] = $this->pagination->create_links();
		$this->view_data['count'] = count($users);
		
		echo json_encode($this->view_data);
		exit(0);
	}
	
	public function search_students_for_grade_slip($page = 0, $year_from = '', $year_to = ''){
		
		$this->load->model(array('M_enrollments'));
		
		$filter = false;
		
		if($_POST){
			
			$field = $this->input->post('fields');
			$keyword = $this->input->post('keyword');
			
			$year_id = $this->input->post('year_id');
			$semester_id = $this->input->post('semester_id');
			$course_id = $this->input->post('course_id');
			
			if(trim($keyword) != ""){
				$filter[$field.' LIKE '] = "%".$keyword."%";
			}
			
			if(trim($year_id) != ""){
				$filter['enrollments.year_id'] = $year_id;
			}
			if(trim($semester_id) != ""){
				$filter['enrollments.semester_id'] = $semester_id;
			}
			if(trim($course_id) != ""){
				$filter['enrollments.course_id'] = $course_id;
			}
			
			$filter['enrollments.sy_from'] = $year_from;
			$filter['enrollments.sy_to'] = $year_to;
			$filter['enrollments.is_deleted'] = 0;
			$filter['enrollments.is_paid'] = 1;

		}
		
		$this->load->library("pagination");
		 $config['full_tag_open'] = '<ul class="pagination">';
		 $config['full_tag_close'] = '</ul>';
		 $config['prev_link'] = '&lt; Prev';
		 $config['prev_tag_open'] = '<li>';
		 $config['prev_tag_close'] = '</li>';
		 $config['next_link'] = 'Next &gt;';
		 $config['next_tag_open'] = '<li>';
		 $config['next_tag_close'] = '</li>';
		 $config['cur_tag_open'] = '<li class="active"><a href="#">';
		 $config['cur_tag_close'] = '</a></li>';
		 $config['num_tag_open'] = '<li>';
		 $config['num_tag_close'] = '</li>';
		 $config['first_link'] = FALSE;
		 $config['last_link'] = FALSE;
		
		$config["base_url"] = base_url() ."ajax/search_students_for_grade_slip";
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_enrollments->find(0,0,$filter, true, true);
		$config["per_page"] = 30;
		$config['num_links'] = 5;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$this->view_data['users'] =  $users = $this->M_enrollments->find($page, $config["per_page"], $filter);
		$this->view_data['links'] = $this->pagination->create_links();
		$this->view_data['count'] = count($users);
		
		echo json_encode($this->view_data);
		exit(0);
	}
	
	public function check_item()
	{
		$this->load->model('M_items');
		
		$id = $this->input->post('id');
		$unit = $this->input->post('unit');
		
		$item = $this->M_items->get($id);
		
		if((float)$unit > (float)$item->unit_left)
		{
			$data['validate'] = false;
			$data['msg'] = 'There are only '. $item->unit_left .' unit/s left. Please change the quantity. ';
		}
		else
		{
			$data['validate'] = true;
			$data['msg'] = '';
		}
		
		echo json_encode($data);
		exit(0);
	}
	
	public function save_menu()
	{
		$this->load->model('M_menus');
		$data['department'] = $this->input->post('department');
		$data['controller'] = $this->input->post('controller');
		$data['caption'] = 	$this->input->post('caption');
		$data['menu_lvl'] = $this->input->post('menu_lvl');
		$data['menu_grp'] = $this->input->post('menu_grp');
		$data['menu_sub'] = $this->input->post('menu_sub');
		$data['menu_num'] = $this->input->post('menu_num');
		
		$rs = $this->M_menus->create_menus($data);
		
		exit(0);
	}
	
	
    public function destroy_subject($y,$c, $s,$eid, $id,  $subject_id)
    {
		$this->load->model(array('M_student_subjects', "M_subjects", "M_enrollments","M_student_grades","M_studentpayments_details"));
		$sgf_id = $this->M_student_grades->get_student_grade_file_id($eid);
		$sg_id = $this->M_student_grades->get_student_grade_id($sgf_id, $subject_id);
		$check_grade = $this->M_student_grades->check_containing_grade($sg_id);
		$profile = $this->M_enrollments->profile($eid);
	  
		if (!$check_grade) 
		{
			$data_units['status'] = FALSE ;
			
			//UPDATE STUDENT TOTALS : DEDUCT/REVERSE CHARGE AMOUNT PER SUBJECT			
			$added_subject = $this->M_subjects->get($subject_id);//TO GET UNITS
			$enrollments = $this->M_student_total_file->get_student_total_file($eid); //TO GET TUITION FEE AMOUNT
			if($enrollments && $added_subject)
			{
				$subject_charge_amount = $enrollments->tuition_fee * $added_subject->units;
				if($subject_charge_amount > 0)
				{
					//SUBTRACT SUBJECT CHARGE TO TOTALS
					$rs = $this->M_student_total_file->reverse_subject_amount($eid, $subject_charge_amount,$subject_id);
					
					//REVERSE STUDENT PAYMENT DETAILS
					$this->M_studentpayments_details->reverse_studentpayment_details($eid);
					
					//UPDATE ENROLLMENT IF PAID, FULL , PARTIAL AND UPDATE SUBJECT LOADS
					$this->M_enrollments->update_enrollments_status($eid); 
				}
			}
			
			if ($this->M_enrollments->check_if_paid($eid)) 
			{
				$this->M_subjects->recalculate_subject_load($subject_id, 'add');
			}
			
			$this->M_student_grades->delete_student_grade_categories($sg_id);
			$this->M_student_grades->delete_student_grade($sg_id);
			$this->M_student_subjects->destroy_subject($id);
			
			activity_log('delete student subject',$profile->studid,'Deduction student subject by: '.$profile->studid.'Success; Subject Id: '.$subject_id);
       
		}else
		{
			$data_units['status'] = TRUE;
		}

		$units = $this->M_student_subjects->get_units($eid);
		
		$data_units['units'] = $units->total_units > 0 ? $units->total_units : 0;
		$data_units['labs'] = $units->total_labs > 0 ? $units->total_labs : 0 ;
	 
		$this->session->set_flashdata('system_message', '<div class="alert alert-success">Successfully deleted student subject.</div>');
		
		echo json_encode($data_units);
		exit(0);
    }

    
	public function edit_grade($id)
	{
         $this->load->model(array('M_student_grades'));
	 // Grade Editing

		$value = $this->input->post('value');
        if(isset($value) && $value != ""){
          	$sgc_id = $this->input->post('sgc_id');
          	$data['value'] = $this->input->post('value');
          	$data['updated_at'] = date('Y-m-d H:i:s');
          		
          	$result = $this->M_student_grades->update_student_grade_category($data, $sgc_id);
          	$return['new_value'] = $result['value'];
          }else{
		$sgc = $this->M_student_grades->get_sgc($this->input->post('sgc_id'));
	  	$return['new_value'] = $sgc->value;
 	  }
	  
          	echo json_encode($return);
          	exit(0);
	}
	
	public function set_tuition_fee()
	{
		 $this->load->model(array('M_fees'));
		 $id = $this->input->post('id');
		 $data['updated_at'] = NOW;
		 $this->M_fees->reset_fees_tuition_fee();
		 $data['is_tuition_fee'] = 1;
		 $rs = $this->M_fees->update_fees($data, $id);
		 echo json_encode($rs);
		 exit(0);
	}
	
	public function get_student_accounts()
	{
		$studid = $this->input->post('studid');
		$semester_id = $this->input->post('semester_id');
		$year_from = $this->input->post('year_from');
		$year_to = $this->input->post('year_to');
		
		$this->load->model(array('M_enrollments','M_studentpayments_details','M_student_total_file','M_grading_periods'));
		$this->view_data['student'] = $this->M_enrollments->get_by(array('studid'=> $studid));
		$this->view_data['profile'] = $rs = $this->M_enrollments->get_unpaid_enrollment_by_studid($studid, $semester_id, $year_from, $year_to);
		
		$this->view_data['student_total'] = false;
		$this->view_data['payment_details'] = false;
		$this->view_data['current_period'] = $this->M_grading_periods->get_current_grading_period();
		
		if($rs)
		{
			$enrollment_id = $rs->id;
			//GET PAYMENT DETAILS / PAYMENT DISTRIBUTION
			$this->view_data['student_total'] = $student_total = $this->M_student_total_file->get_student_total_file($enrollment_id);
			$this->view_data['payment_details'] = $payment_details = $this->M_studentpayments_details->get_record_by_enrollment_id($enrollment_id);
		}
		
		$this->load->view('payments/account_details', $this->view_data);
	}

	public function update_dock_status()
	{
		$this->load->model('M_sys_par');
		$sp = $this->M_sys_par->get_sys_par();
		if($sp){
			unset($data);
			$data['show_menu'] =  $this->input->post('to_show');
			$this->M_sys_par->update($sp->id, $data);
		}

		$ret['status'] = true;
		echo json_encode($ret);
		exit(0);
	}

	public function colap_menu()
	{
		$this->load->model('M_sys_par');
		$sp = $this->M_sys_par->get_sys_par();
		if($sp){
			unset($data);
			$data['is_menu_accordion'] = $this->input->post('to_show') == "1" ? 0 : 1;
			$this->M_sys_par->update($sp->id, $data);
		}

		$ret['status'] = true;
		echo json_encode($ret);
		exit(0);
	}

	public function finance_search_student()
	{
		$this->load->model('M_enrollments');
		$data = $this->M_enrollments->search_students_automcomplete($_GET);
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($data));
	}

	public function update_course_specialization($id)
	{
		$this->load->model('M_course_specialization','m');
		$data = $this->input->post('option');
		$rs = $this->m->update_specialization($data, $id);
		if($rs){

			echo json_encode(1);
			exit(0);
		}else{

			echo json_encode(0);
			exit(0);
		}
	}

	public function delete_course_specialization($id)
	{
		$this->load->model('M_course_specialization','m');
		$rs = $this->m->delete_specialization($id);
		if($rs){

			echo json_encode(1);
			exit(0);
		}else{

			echo json_encode(0);
			exit(0);
		}
	}

	/**
	 * Search Subject - in subject schedule CRUD
	 */
	public function search_subject()
	{
		$this->load->model('M_master_subjects');
		$this->view_data['results'] = $this->M_master_subjects->ajax_search_subject($this->input->post());
		echo $this->load->view('subjects/search_result',$this->view_data,true);
		// vp($this->db->last_query());
		exit(0);
	}

	/**
	 * Onchange - Select Subject on Subject Schedul Crud
	 */
	public function onchange_select_subject()
	{
		$this->load->model('M_master_subjects');
		$r = $this->M_master_subjects->pull(array('ref_id'=>$this->input->post('ref_id')));
		if($r){
			echo json_encode($r);
			exit(0);
		}

		echo json_encode(0);
		exit(0);
	}

	/**
	 * Course Checklist display in subject schedule form
	 * Allow user to click subject for easy search
	 */
	public function select_subject_in_checklist()
	{
		$this->load->model(['M_curriculum','M_curriculum_subjects','M_subject_pre_requisite','M_master_subjects']);

		$id = $this->input->post('course_id');
		$s_id = $this->input->post('specialization_id');
		$c_id = $this->M_curriculum->get_current_course_curriculum($id,$s_id);
		
		$this->view_data['curriculum_id'] = $c_id;
		$this->view_data['major_id'] = $s_id;
		$this->view_data['curriculum'] = $curriculum = $this->M_curriculum->get_curriculum($c_id,$s_id);
		$this->view_data['curriculum_subjects'] = $cs = $this->M_curriculum_subjects->get_curriculum_subjects($c_id,$s_id);

		//closure function to get pre-requisite
		$this->view_data['get_pre_requisite'] = function($c_id, $s_id){return $this->M_subject_pre_requisite->get_pre_requisite($c_id, $s_id);};

		//closure function to check if master subject already has schedule created
		$this->view_data['is_subject_has_schedule'] = function($c_id, $ref_id, $s_id){return $this->M_master_subjects->is_subject_has_schedule($c_id, $ref_id, $s_id);};
		
		echo $this->load->view('curriculum/_curriculum_subjects_for_schedule', $this->view_data,true);
		exit(0);
	}

	public function view_event($hash='')
	{
			$id = $this->check_hash($hash);
			if($id){
				$this->load->model('M_events');
				$data['event'] = $this->M_events->get_events($id);
				echo $this->load->view('events/ajax_view_event',$data,true);
			}else{
				echo "<div class='alert alert-google-plus'>Invalid Ajax Parameters</div>";
			}
	}

	/**
	 * Get Major or Specialization for course
	 * controller : curriculum/create
	 */
	public function get_major()
	{
		$this->load->model('M_course_specialization','m');
		$course_id = $this->input->post('course_id');
		$major_id = $this->input->post('major_id') ? $this->input->post('major_id') : '';

		$majors = $this->m->get_all_specialties($course_id);
		if($majors){
			$r = array();
			foreach ($majors as $k => $v) {
				$r[$v->id] = $v->specialization;
			}
			echo form_dropdown('specialization_id',$r,$major_id,' id="specialization_id" class="form-control"');
		}else{
			echo "<p><i>No Available Specialization</i></p>";
		}
		exit(0);
	}

	/**
	 * Check or get id of a hash string usually a table id
	 * @param string $hash 
	 * @param bool $show_error - show_404 if false
	 */
	public function check_hash($hash, $show_error = false){
		$x = $this->hs->decrypt($hash);
		
		if($x){
			return $x[0];
		}
		else{
			if($show_error === true){
				show_404();
			}else{
				return false;
			}
		}
	}
}
