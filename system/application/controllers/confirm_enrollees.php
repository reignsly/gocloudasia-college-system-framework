<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Confirm_enrollees Extends MY_Controller
{
	public function __construct(){
		parent::__construct();
		$this->session_checker->open_semester();
		$this->menu_access_checker();
		$this->load->model(array('M_enrollments'));
		$this->load->helper(array('url_encrypt','time'));
		$this->load->model(array(
				'M_core_model',
				'M_temp_enrollments',
				'M_users',
				'M_student_subjects',
				'M_temp_student_subjects',
				'M_sys_par',
				'M_temp_start'
				));
		$this->load->helper('my_dropdown');
	}

	public function index($page = 0)
	{
		$this->session_checker->check_if_alive();
		
		//PAGINATION
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->view_data['page_title'] = "Unconfirmed New Enrollees";

		$filter = false;
		$like = false;
		$order_by = false;
		
		$filter['temp_enrollments.semester_id'] = $this->cos->enrollment->semester_id;
		$filter['temp_enrollments.sy_from'] = $this->cos->enrollment->year_from;
		$filter['temp_enrollments.sy_to'] = $this->cos->enrollment->year_to;
		$filter['temp_enrollments.is_deleted'] = 0;
		$filter['temp_enrollments.confirm_status'] = 'PENDING';
		$filter['temp_enrollments.status <>'] = 'old';
		
		$this->view_data['checkdate'] = "";

		if($_GET)
		{
			if($this->input->get('checkdate')){
				$this->view_data['checkdate'] = "checked";

				if($this->input->get('created_at')){
					$this->view_data['created_at'] = $created_at = $this->input->get('created_at', true);
					$filter['DATE(temp_enrollments.created_at)'] = $created_at;
				}
			}
			
			if($this->input->get('lastname')){
				$this->view_data['lastname'] = $lastname = trim($_GET['lastname']);
				$like['temp_enrollments.lastname'] = $lastname;
				
			}
			
			if($this->input->get('studid')){
				$this->view_data['studid'] = $studid = trim($_GET['studid']);
				$like['temp_enrollments.studid'] = $studid;
			}
			
			if($this->input->get('year_id')){
				$this->view_data['year_id'] = $year_id = trim($_GET['year_id']);
				$filter['temp_enrollments.year_id'] = $year_id;
			}

			# Confirm All Submission
			if($this->input->get('confirm_all')){
				$selected = $_GET['selected'];
				if($selected && is_array($selected))
				{	
					$cc = 0;
					foreach($selected as $id){
						
						$rs_c = $this->M_temp_enrollments->confirm_enrollees_by_id($id);
		
						$profile = $this->M_temp_enrollments->get($id);
						
						if($rs_c):
							$cc++;
							activity_log('Confirm Enrollment',$this->userlogin,'Success; Student ID: '.$profile->studid);
						endif;
					}
					
					if($cc > 0)
					{
						$this->session->set_flashdata('system_message', '<div class="alert alert-success">Selected Enrollments successfully confirmed.</div>');
						
						redirect('confirm_enrollees');
					}
				}
			}
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'temp_enrollments.id',
				'temp_enrollments.temp_start_id',
				'temp_enrollments.studid',
				'temp_enrollments.name' ,
				'temp_enrollments.created_at' ,
				'years.year',
				'courses.course' 
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "courses",
				"on"	=> "courses.id = temp_enrollments.course_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "years",
				"on"	=> "years.id = temp_enrollments.year_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "temp_enrollments.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."confirm_enrollees/index";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$this->view_data['back_url'] = $config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("temp_enrollments", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->post('submit') == "Print")
		{
			$get['all'] = true;
		}
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("temp_enrollments", $get);
		$this->view_data['links'] = $l = $this->pagination->create_links();
	}

	public function lists($page = 0)
	{
		$this->session_checker->check_if_alive();
		
		//PAGINATION
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		$like = false;
		$order_by = false;
		
		$filter['temp_enrollments.semester_id'] = $this->cos->enrollment->semester_id;
		$filter['temp_enrollments.sy_from'] = $this->cos->enrollment->year_from;
		$filter['temp_enrollments.sy_to'] = $this->cos->enrollment->year_to;
		$filter['temp_enrollments.is_deleted'] = 0;
		$filter['temp_enrollments.confirm_status'] = 'CONFIRM';
		
		$this->view_data['checkdate'] = "";

		if($_GET)
		{
			if($this->input->get('checkdate')){
				$this->view_data['checkdate'] = "checked";

				if($this->input->get('confirm_date')){
					$this->view_data['confirm_date'] = $confirm_date = $this->input->get('confirm_date', true);
					$filter['DATE(temp_enrollments.confirm_date)'] = $confirm_date;
				}
			}
			
			if($this->input->get('lastname')){
				$this->view_data['lastname'] = $lastname = trim($_GET['lastname']);
				$like['temp_enrollments.lastname'] = $lastname;
				
			}
			
			if($this->input->get('studid')){
				$this->view_data['studid'] = $studid = trim($_GET['studid']);
				$like['temp_enrollments.studid'] = $studid;
			}
			
			if($this->input->get('year_id')){
				$this->view_data['year_id'] = $year_id = trim($_GET['year_id']);
				$filter['temp_enrollments.year_id'] = $year_id;
			}

			if($this->input->get('confirm_all')){
				$selected = $_GET['selected'];
				if($selected && is_array($selected))
				{	
					$cc = 0;
					foreach($selected as $id){
						
						$rs_c = $this->M_temp_enrollments->confirm_enrollees_by_id($id);
		
						$profile = $this->M_temp_enrollments->get($id);
						
						if($rs_c):
							$cc++;
							activity_log('Confirm Enrollment',$this->userlogin,'Success; Student ID: '.$profile->studid);
						endif;
					}
					
					if($cc > 0)
					{
						$this->session->set_flashdata('system_message', '<div class="alert alert-success">Selected Enrollments successfully confirmed.</div>');
						
						redirect('confirm_enrollees');
					}
				}
			}
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'temp_enrollments.id',
				'temp_enrollments.temp_start_id',
				'temp_enrollments.studid',
				'temp_enrollments.name' ,
				'temp_enrollments.confirm_date' ,
				'years.year',
				'courses.course' 
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "courses",
				"on"	=> "courses.id = temp_enrollments.course_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "years",
				"on"	=> "years.id = temp_enrollments.year_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "temp_enrollments.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."confirm_enrollees/index";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$this->view_data['back_url'] = $config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("temp_enrollments", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->post('submit') == "Print")
		{
			$get['all'] = true;
		}
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("temp_enrollments", $get);
		$this->view_data['links'] = $l = $this->pagination->create_links();
	}
	
	public function confirm($hash_id = false)
	{
		$id = $this->check_hash($hash_id);
		
		#check ID if exist
		$is_exist = $this->M_temp_enrollments->get($id);
		if($is_exist === false){ show_404(); }

		#check if student still pending or already confirm
		unset($get);
		$get['where']['confirm_status'] = "PENDING";
		$get['single'] = true;
		$is_pending = $this->M_temp_enrollments->get_record(false, $get);
		if($is_pending === false){
			$this->_msg('e','Student not available, maybe it was already confirm by other registrar users.', 'confirm_enrollees');
		}

		$rs = $this->M_temp_enrollments->confirm_enrollees_by_id($id);
		
		$profile = $this->M_temp_enrollments->get($id);
		
		$sys_par = $this->M_sys_par->get_sys_par();
		
		if($rs):
			
			if($sys_par->is_auto_id==1):
				$data['updated_at'] = NOW;
				$data['studid_auto'] = $this->M_sys_par->get_studid_and_update_to_next_series();
				$this->M_temp_enrollments->update_enrollments($data, $id);
			endif;
			
			activity_log('Confirm Enrollment',$this->userlogin,'Success; Student ID: '.$profile->studid);
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">Enrollment successfully confirmed.</div>');
			redirect('confirm_enrollees');
		else:
			$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Enrollment failed to confirm. Please try again</div>');
			redirect('confirm_enrollees');
		endif;
		
	}
	
	public function unconfirm($id = false)
	{
		#remove for now - by ma'am vivian (6-27-2014)
		show_404();
		if($id == false) { show_404(); }
		
		$rs = $this->M_temp_enrollments->unconfirm_enrollees_by_id($id);
		
		$profile = $this->M_temp_enrollments->get($id);
		
		if($rs):
			
			activity_log('Unconfirm Enrollment',$this->userlogin,'Unconfirm; Student ID: '.$profile->studid);
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">Enrollment successfully rejected.</div>');
			redirect('confirm_enrollees');
		else:
			$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Enrollment failed to unconfirm. Please try again</div>');
			redirect('confirm_enrollees');
		endif;
	}
	
	public function profile($hash_id = false, $hash_temp_start_id = false)
	{
		$id = $this->check_hash($hash_id);
		$temp_start_id = $this->check_hash($hash_temp_start_id);
		if(!$temp_start_id){ show_404(); }
		
		$this->view_data['profile'] = $this->view_data['enroll'] = $p = $this->M_temp_enrollments->profile($id); // student profile
		if(!$p){show_404();}
		$this->view_data['enrollment_id'] = $id;
		$this->view_data['form_token'] = false;
		
		$this->view_data['subjects']	 = $subjects =  $this->M_temp_student_subjects->get_temp_student_subjects($temp_start_id);
		$this->view_data['user'] = $this->user;
		
		$subject_units = 0;
		$subject_lab = 0;
		$subject_lec = 0;
		
		if($subjects){
			foreach($subjects as $obj){
				$subject_units += $obj->units;
				$subject_lab += $obj->lab;
				$subject_lec += $obj->lec;
			}
		}
		
		$this->view_data['subject_units']['subject_units'] = $subject_units;
		$this->view_data['subject_units']['total_lab'] = $subject_lab;
		$this->view_data['subject_units']['total_lec'] = $subject_lec;
		$this->view_data['total_lec'] = $subject_lec;
		$this->view_data['total_lab'] = $subject_lab;
		
		$this->view_data['course'] = $p->course_id;
		$this->view_data['year'] = $p->year_id;
		$this->view_data['semester'] = $p->sem_id;
		$this->view_data['enrollment_id'] = $p->id;
		
		
		
		$this->view_data['studid'] = $p->studid;
	}

	public function delete($hash='')
	{
		$id = $this->check_hash($hash);
		$e = $this->M_temp_enrollments->pull(array('temp_start_id'=>$id),'name'); if(!$e){ show_404(); }
		$del = $this->M_temp_start->remove_registration($id);

		if($del){
			$this->_msg('s',"Enrollment of ".ucwords(strtolower($e->name))." was successfully removed",'confirm_enrollees');
		}else{
			$this->_msg('s',"Transaction failed, please try again",'confirm_enrollees');
		}
	}
}
