<?php

class Scholarship Extends MY_Controller
{
	public function __construct(){
		parent::__construct();
		$this->session_checker->check_if_alive();
		$this->session_checker->open_semester();
		$this->load->helper(array('url_encrypt'));
		
		$this->menu_access_checker('scholarship');

		//LOAD MODELS
		$this->load->model('M_student_scholarship','m_ss');
		$this->load->model('M_core_model');

		//LOAD HELPERS & LIBRARY
		$this->load->helper('my_dropdown');
		$this->load->library('form_validation');
	}

	public function index($page = 0)
	{	
		$this->menu_access_checker();
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		$like = false;
		$order_by = false;
		
		if($_GET)
		{
			
			if(isset($_GET['name']) && trim($_GET['name']) != ''){
				$this->view_data['name'] = $name = trim($_GET['name']);
				$like['name'] = $name;
			}

			if(isset($_GET['desc']) && trim($_GET['desc']) != ''){
				$this->view_data['desc'] = $desc = trim($_GET['desc']);
				$like['desc'] = $desc;
			}
		}
		
		//CONFIGURATION
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['order'] = "name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."scholarship/index";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("student_scholarships", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		// $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("student_scholarships", $get);
		$this->view_data['links'] = $this->pagination->create_links();
	}

	public function create()
	{
		$this->menu_access_checker();

		$this->view_data['system_message'] = $this->session->flashdata('system_message');

		if($this->input->post('add_scholarship'))
		{
			if($this->form_validation->run('student_scholarship') === FALSE){
				$this->_msg('e',validation_errors(),'scholarship/create');
			}
			else{
				//SAVE DATA
				unset($data);
				$data['name'] = $log['Name'] = trim($this->input->post('name'));
				$data['desc'] = trim($this->input->post('desc'));
				$data['type'] = $log['Type'] = trim($this->input->post('type'));
				if($data['type'] == 'CASH'){
					$data['cash_amount'] = $log['Cash Amount'] = floatval($this->input->post('cash_amount'));
					$data['percentage'] = 0;

				}else{
					$data['cash_amount'] = 0;
					$data['percentage'] = $log['Percentage'] = floatval($this->input->post('percentage'));
					
					if(isset($_POST['applied_to'])){
						$applied_to = $this->input->post('applied_to'); //TUITION , MISC OR TUITION & MISC
						if(is_array($applied_to)){
							if(count($applied_to) == 2)
							{
								if(in_array("TUITION", $applied_to) && in_array("MISC", $applied_to)){
									$data['applied_to'] = $log['Applied to'] = "TUITION & MISC";
								}
							}else{
								if(in_array("TUITION", $applied_to)){
									$data['applied_to'] = $log['Applied to'] = "TUITION";
								}else{
									$data['applied_to'] = $log['Applied to'] = "MISC";
								}
							}
						}	
					}
				}

				$rs = (object)$this->m_ss->insert($data);

				if($rs->status){
					$log['student_scholarship_id'] = $rs->id;

					activity_log('Create Scholarships',$this->userlogin,'Data :'.arr_str($log));
					$this->_msg('s','Scholarship successfully added','scholarship/create');
				}
				else
				{
					$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Saving failed. Please try again.</div>');
					redirect(current_url());
				}

			}
		}
	}

	// Update
	public function edit($id = false)
	{
		if(!$id) { show_404(); }

		$this->view_data['system_message'] = $this->_msg();

		$this->view_data['scholarship'] = $d = $this->m_ss->get($id);
		// vd($d);
		if($d){

			if($this->input->post('update_scholarship'))
			{
				if($this->form_validation->run('student_scholarship') == FALSE){	
				}
				else{
					//SAVE DATA
					unset($data);
					$data['name'] = $log['Name'] = trim($this->input->post('name'));
					$data['desc'] = trim($this->input->post('desc'));
					$data['type'] = $log['Type'] = trim($this->input->post('type'));

					if($data['type'] == 'CASH'){
						$data['cash_amount'] = $log['Cash Amount'] = floatval($this->input->post('cash_amount'));
						$data['percentage'] = 0;

					}else{
						$data['cash_amount'] = 0;
						$data['percentage'] = $log['Percentage'] = floatval($this->input->post('percentage'));
						
						if(isset($_POST['applied_to'])){
							$applied_to = $this->input->post('applied_to'); //TUITION , MISC OR TUITION & MISC
							if(is_array($applied_to)){
								if(count($applied_to) == 2)
								{
									if(in_array("TUITION", $applied_to) && in_array("MISC", $applied_to)){
										$data['applied_to'] = $log['Applied to'] = "TUITION & MISC";
									}
								}else{
									if(in_array("TUITION", $applied_to)){
										$data['applied_to'] = $log['Applied to'] = "TUITION";
									}else{
										$data['applied_to'] = $log['Applied to'] = "MISC";
									}
								}
							}	
						}
					}

					$rs = $this->m_ss->update($id,$data);

					if($rs){
						$log['student_scholarship_id'] = $id;
						activity_log('Update Scholarships',$this->userlogin,'Data :'.arr_str($log));

						$this->_msg('s','Scholarship was successfully updated.','scholarship/edit/'.$id);
					}
					else
					{
						$this->_msg('e','Saving failed. Please try again','scholarship/edit/'.$id);
					}

				}
			}
		}else{
			$this->_msg('e','Unknown record','scholarship');
		}
	}

	public function delete($id = false)
	{
		if(!$id) { show_404(); }

		$p = $this->m_ss->get($id);

		if($p):
			$log['Name'] = $p->name;
			$log['Type'] = $p->type;
			if($p->type == 'CASH'){
				$log['cash_amount'] = $p->cash_amount;
			}else{
				$log['Percentage'] = $p->percentage;
				$log['Applied TO'] = $p->applied_to;
			}
			
			$result = $this->m_ss->delete($id);

			if($result)
			{
				activity_log('delete scholarships',$this->userlogin,'Data : '.arr_str($log));

				$this->_msg('s','Scholarships was deleted successfully.','scholarship');

			}else
			{
				$this->_msg('e','Scholarships was not deleted. Please try again.','scholarship');				
			}
		else:
			redirect('scholarship');
		endif;
	}
}