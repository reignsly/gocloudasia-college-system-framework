<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Open_semester_employee_settings extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->islogin===FALSE){
			show_404();
		}

		$this->load->model('M_open_semester_employee_settings','m_oses');
	}

	// Retrieve
	public function index()
	{
		$this->view_data['custom_title'] = "Change";
		$this->load->model(array('m_open_semesters'));
		$this->view_data['open_semesters'] = $os = $this->m_open_semesters->get_all_list();
		$this->view_data['user_open_semester'] = $uos = $this->m_oses->get_open_semester_userId($this->userid);
		
		if($this->input->post()){

			$this->load->library('form_validation');

			$this->form_validation->set_rules('current', 'Your Open Semester', 'required');

			if ($this->form_validation->run() !== FALSE)
			{
				$open_id = $this->input->post('current');
				if($open_id != $uos->open_semester_id){

					unset($save);
					$save['open_semester_id'] = $open_id;
					$rs = $this->m_oses->update($uos->id, $save);

					$s_msg = "Your <strong>open semester</strong> was successfully changed.";
					$f_msg = "There was an error during the actual saving of the data, please try again.";

					unset($log);
					$log['activity'] = "Change Open Semester";
					$log['log'] = "Change from open_semester id ".$uos->open_semester_id." to ".$open_id;

					$this->__msg($rs, current_url(), $s_msg, $f_msg, $log);

				}else{
					$this->_msg('s','Your Open Semester was not change.', current_url());
				}
			}
		}
	}
	
	public function edit_employee($data)
	{
		$this->session_checker->open_semester();
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_open_semesters'));
		
		$this->view_data['opensemesters'] = $this->M_open_semesters->get_all_list();
		$this->view_data['opensemesteruser'] = $opensemesteruser = $this->M_open_semesters->get_open_semester_userId($this->session->userdata['userid']);
		
		if($_POST)
		{
			$open_semester_id = $_POST['open_semester_id'];
			if($opensemesteruser){
				$updateemployeesetting = $this->M_open_semesters->update_open_semester_userId($open_semester_id,$this->session->userdata['userid']);
				redirect(current_url());
			}else{
				$insertemployeesetting = $this->M_open_semesters->insert_open_semester_userId($open_semester_id,$this->session->userdata['userid']);
				redirect(current_url());
			}
		}
	}
	
	// Create
	public function create($id)
	{
		$this->load->model(array('M_open_semesters','M_academic_years','M_semesters','M_open_semester_employee_settings'));
		
		$this->view_data['opensemesters'] = $this->M_open_semesters->get_all_list();
		
		$this->disable_menus = true;
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($_POST)
		{
			$data['open_semester_id'] = $this->input->post('open_semester_id');
			$data['user_id'] = $id;
			$data['updated_at'] = date('Y-m-d H:i:s');
			$data['created_at'] = date('Y-m-d H:i:s');
		
			$result = $this->M_open_semester_employee_settings->create_open_semester_employee_settings($data);
			
			if($result['status'])
			{
				log_message('error','Open Semester Employee Settings Created by: '.$this->user.'Success; Open Semester Id: '.$result['id']);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Open Semester Employee Setting successfully updated.</div>');
				redirect('home');
			}
		}
	}
	
	// Update
	public function edit($id)
	{
		$this->load->model(array('M_open_semesters','M_academic_years','M_semesters','M_open_semester_employee_settings'));
		
		$this->view_data['opensemesters'] = $this->M_open_semesters->get_all_list();
		
		$this->view_data['opensemesteruser'] = $this->M_open_semesters->get_open_semester_userId($id);
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($this->view_data['opensemesteruser'] != false){
		
			if($_POST)
			{
				$data['open_semester_id'] = $this->input->post('open_semester_id');
				$data['updated_at'] = date('Y-m-d H:i:s');
			
				$result = $this->M_open_semester_employee_settings->update_open_semester_employee_settings($data, $this->view_data['opensemesteruser']->id);
				
				if($result['status'])
				{
					log_message('error','Open Semester Employee Settings Updated by: '.$this->user.'Success; Open Semester Id: '.$this->view_data['opensemesteruser']->id);
					$this->session->set_flashdata('system_message', '<div class="alert alert-success">Open Semester Employee Setting successfully updated.</div>');
					redirect('open_semester_employee_settings/edit/'.$id);
				}
			}
		}else{
			 redirect('open_semester_employee_settings/create/'.$id);
		}
	}
	
	// Update
	public function destroy($id)
	{
		$this->load->model(array('M_open_semesters'));
		
		$result = $this->M_open_semesters->delete_open_semesters($id);
		log_message('error','Grading Period Deleted by: '.$this->user.'Success; Grading Period Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Grading Period successfully deleted.</div>');
		redirect('open_semester');
	}
}
