<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tesda extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->menu_access_checker(array('tesda/tesda_by_course'));
	}
	
	public function tesda_by_course()
	{
		$this->session_checker->check_if_alive();
		$this->load->helper('url_encrypt');
		$this->load->model('M_courses');
		$this->view_data['courses'] = $this->M_courses->get(FALSE,array('id','course'));
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
	}
	
	public function tesda_download_by_course()
	{
		$this->load->helper('url_encrypt');
		$id = $this->input->get('id');
		$di = $this->input->get('di');
		$result = _sd($id,$di);
		
		
		if($result->status == 'TRUE')
		{
			// inlcude download controller
			$this->load->library('../controllers/_the_downloadables');
			$id = $result->link;
			$this->_the_downloadables->_generate_tesda_by_course_report($id);
		}else{
			show_404('who are you?');
		}
	}
}

?>