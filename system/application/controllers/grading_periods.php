<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Grading_periods extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		// $this->session_checker->secure_page('admin');
		$this->menu_access_checker('grading_periods');
	}
	
	// Create
	public function create()
	{
		$this->load->model(array('M_grading_periods'));
		
		$this->view_data['grading_period'] = FALSE;
		
		if($_POST)
		{
			$data = $this->input->post('grading_periods');
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_grading_periods->create_grading_periods($data);
			
			if($result['status'])
			{
				$id = $result['id'];
				
				activity_log('create grading period',$this->userlogin,'Created by: '.$this->userlogin.'Success;Grading Period Id : '.$id);
				
				log_message('error','Grading Periods Created by: '.$this->user.'Success; Grading Periods Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Grading Period successfully added.</div>');
				redirect(current_url());
			}
		}
	}
	
	// Retrieve
	public function index()
	{
		$this->load->model(array('M_grading_periods'));
		$this->view_data['grading_periods'] = $this->M_grading_periods->find_all();
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($_POST)
		{
			$current = $_POST['current'];
			if($current != ''){
				$id = $current;
				$data['is_set'] = 1;
				$data['updated_at'] = date('Y-m-d H:i:s');
				
				$this->M_grading_periods->reset_grading_period();
				$result = $this->M_grading_periods->update_grading_periods($data, $current);
				
				if($result['status'])
				{
					activity_log('Grading Period Set Current',$this->userlogin,'Set by: '.$this->userlogin.'Success;Grading Period Id : '.$id);
					
					log_message('error','Grading Period Set by: '.$this->user.'Success; Grading Period Id: '.$id);
					$this->session->set_flashdata('system_message', '<div class="alert alert-success">Successfully Setup Grading Period.</div>');
					redirect('grading_periods');
				}
			}
		}
	}
	
	public function display($id)
	{
		$this->load->model(array('M_grading_periods'));
		
		$this->view_data['grading_periods'] = $this->M_grading_periods->get_grading_periods($id);
	}
	
	// Update
	public function edit($id = false)
	{
		if(!$id) { show_404(); }

		$this->load->model(array('M_grading_periods'));
		
		$this->view_data['grading_periods'] = $this->M_grading_periods->get_grading_periods($id);
		
		if($_POST)
		{
			$data = $this->input->post('grading_periods');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_grading_periods->update_grading_periods($data, $id);
			
			if($result['status'])
			{
				activity_log('Grading Period updated',$this->userlogin,'Updated by: '.$this->userlogin.'Success;Grading Period Id : '.$id);
				log_message('error','Grading Period Updated by: '.$this->user.'Success; Grading Period Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Grading Period successfully updated.</div>');
				redirect(current_url());
			}
		}
	}
	
	// Update
	public function destroy($id = false)
	{
		if(!$id){ show_404(); }
		$this->load->model(array('M_grading_periods'));
		
		$result = $this->M_grading_periods->delete_grading_periods($id);
		activity_log('Grading Period Delete',$this->userlogin,'Deleted by: '.$this->userlogin.'Success;Grading Period Id : '.$id);
		log_message('error','Grading Period Deleted by: '.$this->user.'Success; Grading Period Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-success">Grading Period successfully deleted.</div>');
		redirect('grading_periods');
	}
}

?>