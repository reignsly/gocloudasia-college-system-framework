<?php 

class Finance Extends MY_Controller
{
	private $error_handler_message = false;
	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->load->helper(array('url_encrypt'));
	}
	
	public function index()
	{
	
	}
	
	public function add_payment_report()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('group_payment_end_date','End Date','callback__date_check');
		$this->form_validation->set_rules('group_payment_start_date','Start Date','callback__date_check');
		$this->form_validation->set_message('_date_check','%s has invalid date');
		
		if($this->form_validation->run() !== FALSE)
		{
			$this->load->model('m_finance_queries','m');
			$input['start_date'] = $this->input->post('group_payment_start_date');
			$input['end_date'] = $this->input->post('group_payment_end_date');
			
			if($input['start_date'] > $input['end_date'] OR $input['start_date'] == $input['end_date'])
			{
				$this->view_data['system_message'] = '<div class="alert alert-danger">End Date Cannot be Smaller than Start Date or Equal</div>';	
			}else{
				$result = $this->m->new_group_payment($input);
				if($result['status'] == 'TRUE')
				{
					
					activity_log('ADD New Payment',$this->userlogin,$result['log']);
					
					$this->view_data['system_message'] = '<div class="alert alert-success">'.$result['log'].'</div>';
				}else{
					$this->view_data['system_message'] = '<div class="alert alert-danger">'.$result['log'].'</div>';
				}
			}
		}
	}
	
	public function all_payment_report()
	{
		$this->load->model('m_finance_queries','m');
		$this->view_data['group_payments'] = $this->m->all_group_payments();
	}
	
	public function show_payment_data()
	{
		$this->load->helper(array('money'));
		$id = $this->input->get('id'); // encrypted id
		$hs = $this->input->get('di'); // hash value for checksum
		$ts = $this->input->get('stat');
		$result = _sd($id,$hs);
		
		if($ts == 'new')
		{
			$this->session->set_userdata('payment_id','');
			if($result->status == 'TRUE')
			{
				$this->session->set_userdata('payment_id',$result->link);
			}
		}
		if($this->session->userdata('payment_id'))
		{
			$this->load->model('m_finance_queries','m');
			$this->load->library('pagination');

			$id = $this->session->userdata('payment_id');

			$config["base_url"] = base_url() . "finance/show_payment_data/";
			$config["total_rows"] = $this->m->count_payment_data($id);
			$config['full_tag_open'] = '<div class="pager">';
			$config['full_tag_close'] = '</div>';
			$config['cur_tag_open'] = '<span class="active">';
			$config['cur_tag_close'] = '</span>';
			$config["per_page"] = 20;
			$config["uri_segment"] = 3;
			$choice = $config["total_rows"] / $config["per_page"];
			$config["num_links"] = round($choice);
			$this->pagination->initialize($config);

			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$this->view_data['results'] = $this->m->get_payment_data($id,$config['per_page'], $page);
			$this->view_data['links'] = $this->pagination->create_links();
			$this->view_data['id'] = $id;
		}else{
			show_404();
		}
	}
	
	public function generate_payment_report()
	{
		$id= $this->input->get('id');
		$di= $this->input->get('di');
		$result = _sd($id,$di);
		$this->_set_error_handler();
		
		if($result->status == 'TRUE')
		{
			// include downloadables controller
			$this->load->library('../controllers/_the_downloadables');
			$id = $result->link;
			$this->_the_downloadables->_generate_payment_report($id);
		}else{
			show_404();
		}
	}
	
	public function generate_student_charges()
	{
		$verify_crypt = $this->input->get('verify');
		$verify_hash = $this->input->get('code');
		$result = _sd($verify_crypt,$verify_hash);
		$this->_set_error_handler();
		
		if($result->status == 'TRUE')
		{
			// include downloadables controller
			$this->load->library('../controllers/_the_downloadables');
			$this->_the_downloadables->_generate_student_charges();
		}else{
			show_404();
		}
	}
	
	public function generate_student_payment_report_excel()
	{
		$id = $this->input->get('id');
		$di = $this->input->get('di');
		$result = _sd($id,$di);
		$this->_set_error_handler();
				
		if($result->status == 'TRUE')
		{
			// include downloadables controller
			$this->load->library('../controllers/_the_downloadables');
			$this->_the_downloadables->_generate_student_payments_report();
		}else{
			show_404();
		}
	}
	
	public function generate_student_deductions_excel()
	{
		$id = $this->input->get('id');
		$di = $this->input->get('di');
		$result = _sd($id,$di);
		$this->_set_error_handler();
		
		if($result->status == 'TRUE')
		{
			$this->load->library('../controllers/_the_downloadables');
			$this->_the_downloadables->_generate_student_deductions_report();
		}else{
			show_404();
		}
	}
	
	public function generate_remaining_balance_excel()
	{
		$id = $this->input->get('id');
		$di = $this->input->get('di');
		$result = _sd($id,$di);
		$this->_set_error_handler();
		if($result->status == 'TRUE')
		{
			// include downloadables controller
			$this->load->library('../controllers/_the_downloadables');
			$this->_the_downloadables->_generate_remaining_balance();
		}else{
			show_404();
		}
	}
	
	public function generate_student_profile_excel()
	{
		$id = $this->input->get('id');
		$di = $this->input->get('di');
		$result = _sd($id,$di);
		// Catches errors and logs to system logs
		$this->_set_error_handler();
		
		if($result->status == 'TRUE')
		{	
			// include downloadables controller
			$this->load->library('../controllers/_the_downloadables');
			$this->_the_downloadables->_print_all_student_profiles();
		}else{
			show_404();
		}
	}
	
	public function student_charges()
	{
		$this->load->helper(array('money'));
		$this->load->library('pagination');
		$this->load->model('m_finance_queries','m');
		$config["base_url"] = base_url() . "finance/student_charges/";
		$config['total_rows']  = count($this->m->fetch_student_charges());
		$config['per_page'] = 20;
		$config['full_tag_open'] = '<div class="pager">';
		$config['full_tag_close'] = '</div>';
		$config['cur_tag_open'] = '<span class="active">';
		$config['cur_tag_close'] = '</span>';
		$config['uri_segment'] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		if(is_numeric($page))
		{
			$this->view_data['results'] = $this->m->fetch_student_charges($config['per_page'], $page);
			$this->view_data['pagination_links'] = $this->pagination->create_links();
		}else{
			show_404();
		}
	}
	
	public function student_payments()
	{
		$this->load->model('m_finance_queries','m');
		$this->token->set_token();
		
		$this->load->helper(array('money'));
		$this->load->library('pagination');
		$this->load->model('m_finance_queries','m');
		$this->view_data['form_token'] = $this->token->get_token();
		$config["base_url"] = base_url() . "finance/student_payments/";
		$config['per_page'] = 40;
		$config['full_tag_open'] = '<div class="pager">';
		$config['full_tag_close'] = '</div>';
		$config['cur_tag_open'] = '<span class="active">';
		$config['cur_tag_close'] = '</span>';
		$config['uri_segment'] = 3;
		$config["num_links"] = 30;
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$this->view_data['search_strings'] = NULL;
		if($this->input->post('search') == false)
		{
			if($this->session->userdata('searchdata') == false OR $this->input->get('new') == sha1('new'))
			{
				if(is_numeric($page))
				{
					$this->session->set_userdata('searchdata','');
					$config['total_rows']  = $this->m->get_student_payments();
					$this->pagination->initialize($config);
					$this->view_data['results'] = $this->m->get_student_payments(false,$config['per_page'], $page);
				}else{
					show_404();
				}
			}else
			{
				if(is_numeric($page))
				{
					$data = $this->session->userdata('searchdata');
					$config['total_rows']  = $this->m->count_searched_student_payments($data);
					$this->pagination->initialize($config);
					$this->view_data['results'] = $this->m->get_student_payments($data,$config['per_page'], $page);
					$this->view_data['search_strings'] = '('.implode(',',array_values($data)).') Found ('.$config['total_rows'].') results';
				}else{
					show_404();
				}
			}		
		}else{
			if($this->input->post('name') !== '' OR $this->input->post('student_id') !== '' OR $this->input->post('orno') !== '')
			{
				if($this->token->validate_token($this->input->post('form_token')))
				{
					$this->session->set_userdata('searchdata','');
					foreach($this->input->post() as $key => $value)
					{
						if(!empty($value))
						{
							if($key =='name'){
								$input['p.first_name'] = $value;
								$input['p.last_name'] = $value;
								$input['p.middle_name'] = $value;
							}elseif($key == 'student_id'){
								$input['e.studentid'] = $value;
							}elseif($key == 'orno'){
								$input['sp.or_no'] = $value;
							}
						}
					}
					if(is_numeric($page))
					{
						$this->session->set_userdata('searchdata',$input);
						$config['total_rows']  = $this->m->count_searched_student_payments($input);
						$this->pagination->initialize($config);
						$this->view_data['results'] = $this->m->get_student_payments($input,$config['per_page'], $page);
						$this->view_data['search_strings'] = '('.implode(',',array_values($input)).') Found ('.$config['total_rows'].') results';
					}else{
						show_404();
					}
				}else{
					$this->view_data['system_message'] = '<div class="alert alert-danger">Unknown Token Please refresh page and continue</div>';
				}
			}
		}
		$this->view_data['pagination_links'] = $this->pagination->create_links();
	}
	
	public function del_group($id)
	{
			$jquery = $this->input->post('jquery');
			if($jquery == false)
			{
				$this->load->model('m_finance_queries','m');		
				if($this->m->del_group($id))
				{
					activity_log('Delete Payment Group',$this->userlogin,'ID : '.$id);
					$this->session->set_flashdata('system_message', '<div class="alert alert-success">Update was successful</div>');
					redirect(current_url());
				}else
				{
					 $this->view_data['system_message'] = '<div class="alert alert-danger">An error was encountered while proccessing your request</div>';
				}
			}else
			{
				$this->load->model('m_finance_queries','m');		
				if($this->m->del_group($id))
				{
					echo 'true';
					die();//stops php from sending the views so that browser will only receive true
				}else
				{
					echo 'false';
					die();//stops php from sending the views so that browser will only receive false
				}
			}
	}
	
	public function student_deductions()
	{
		$this->load->model('m_student_deductions','m');
		$this->load->library('pagination');
		
		$config["base_url"] = base_url() . "finance/student_deductions/";
		$config['per_page'] = $p = 40;
		$config['full_tag_open'] = '<div class="pager">';
		$config['full_tag_close'] = '</div>';
		$config['cur_tag_open'] = '<span class="active">';
		$config['cur_tag_close'] = '</span>';
		$config['uri_segment'] = 3;
		$config["num_links"] = 30;
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$this->token->set_token();
		$this->view_data['form_token'] = $this->token->get_token();
		
		
		if($this->input->post('search') == false)
		{
			$input = $this->session->userdata('searchdata');
			if($input == false OR $this->input->get('new') == sha1('new'))
			{
				if(is_numeric($page))
				{
					$this->session->set_userdata('searchdata','');
					$this->view_data['search_strings']='Total data gathered'.$config["total_rows"] = $this->m->fetch_student_deductions_with_profiles();
					$this->pagination->initialize($config);
					$this->view_data['results'] = $this->m->fetch_student_deductions_with_profiles(false,$p,$page);	
				}else{
					show_404();
				}
			}else{
				if(is_numeric($page))
				{
					$config["total_rows"] = $t = $this->m->get_total_num_rows($input);
					$this->pagination->initialize($config);
					$this->view_data['results'] = $p =$this->m->fetch_student_deductions_with_profiles($input,$p,$page);
					$this->view_data['search_strings']= 'searched for '.implode(array_values($input)).' Total data found ('.$t.')';
				}else{
					show_404();
				}
			}
		}else{
			$this->session->set_userdata('searchdata','');
			foreach($this->input->post() as $postkey => $postvalue)
			{
				if(!empty($postvalue))
				{
					if($postkey == 'name')
					{
						$input['name'] = $postvalue; 
					}elseif($postkey == 'student_id')
					{	
						$input['student_id'] = $postvalue;
					}
				}
			}
			if(is_numeric($page))
			{
				$this->session->set_userdata('searchdata',$input);
				$config["total_rows"] = $t = $this->m->get_total_num_rows($input);
				$this->pagination->initialize($config);
				$this->view_data['results'] = $p =$this->m->fetch_student_deductions_with_profiles($input,$p,$page);
				$this->view_data['search_strings']= 'searched for '.implode(array_values($input)).' Total data found ('.$t.')';
			}else{
				show_404();
			}
		}
		$this->view_data['pagination_links'] = $this->pagination->create_links();
	}
	
	public function remaining_balance_report()
	{
		$this->load->model('m_student_totals','m');
		$this->load->library('pagination');
		
		
		$config["base_url"] = base_url() . "finance/remaining_balance_report/";
		$config['per_page'] = $p = 30;
		$config['full_tag_open'] = '<div class="pager">';
		$config['full_tag_close'] = '</div>';
		$config['cur_tag_open'] = '<span class="active">';
		$config['cur_tag_close'] = '</span>';
		$config['uri_segment'] = 3;
		$config["num_links"] = 30;
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$this->token->set_token();
		$this->view_data['form_token'] = $this->token->get_token();
		
		if($this->input->post('search') == false)
		{
			$input = $this->session->userdata('searchdata');
			if($input == false OR $this->input->get('new') == sha1('new'))
			{
				if(is_numeric($page))
				{
					$this->session->set_userdata('searchdata','');
					$this->view_data['search_strings']='Total data gathered '.$config["total_rows"] = count($this->m->get_all_student_totals_remaining_balance_with_profiles());
					$this->pagination->initialize($config);
					$this->view_data['results'] = $this->m->fetch_remaining_balance_with_profiles(false,$p,$page);	
				}else{
					show_404();
				}
			}else{
				if(is_numeric($page))
				{
					$config["total_rows"] = $t = $this->m->count_searched_student_remaining_balance($input);
					$this->pagination->initialize($config);
					$this->view_data['results'] = $p =$this->m->fetch_remaining_balance_with_profiles($input,$p,$page);
					$this->view_data['search_strings']= 'searched for '.implode(array_values($input)).' Total data found ('.$t.')';
				}else{
					show_404();
				}
			}
		}else{
			$this->session->set_userdata('searchdata','');
			foreach($this->input->post() as $postkey => $postvalue)
			{
				if(!empty($postvalue))
				{
					if($postkey == 'name')
					{
						$input['name'] = $postvalue; 
					}elseif($postkey == 'student_id')
					{	
						$input['student_id'] = $postvalue;
					}
				}
			}
			if(is_numeric($page))
			{
				$this->session->set_userdata('searchdata',$input);
				$config["total_rows"] = $t = $this->m->count_searched_student_remaining_balance($input);
				$this->pagination->initialize($config);
				$this->view_data['results'] = $p =$this->m->fetch_remaining_balance_with_profiles($input,$p,$page);
				$this->view_data['search_strings']= 'searched for '.implode(array_values($input)).' Total data found ('.$t.')';
			}else{
				show_404();
			}
		}
		$this->view_data['pagination_links'] = $this->pagination->create_links();
	}

	// validation callback for date
	public function _date_check($str)
	{
		$regex = '/^(19|20)\d\d[-\/.](0[1-9]|1[012])[-\/.](0[1-9]|[12][0-9]|3[01])$/';
		if(!preg_match($regex,$str))
		{
			return FALSE;
		}else{
			$date = strtotime($str);	
			if(checkdate(date('m',$date),date('d',$date),date('Y',$date)))
			{
				return TRUE;
			}else{
				return FALSE;
			}
		}
	}
	
	// error handler logs php E_ERRORS
	public function _set_error_handler()
	{		
		function myErrorHandler($code, $message, $file, $line) 
		{
			$ticket_number = substr(md5(rand(1000,9999).time()),0,15);
			log_message('error',$code.' | '.$message.' | '.$file.' | '.$line.' |error_number: '.$ticket_number);
			die('<h1>ERROR</h1><br><b>An Error Occured while processing request Contact system administrator , error log number </b><strong>('.$ticket_number.')</strong>');
		}
		function fatalErrorShutdownHandler()
		{
			$last_error = error_get_last();
			if ($last_error['type'] === E_ERROR) 
			{
				myErrorHandler(E_ERROR, $last_error['message'], $last_error['file'], $last_error['line']);
			}
		}	
		set_error_handler('myErrorHandler');
		register_shutdown_function('fatalErrorShutdownHandler');
	}	
}