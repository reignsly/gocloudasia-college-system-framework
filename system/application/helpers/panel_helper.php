<?php
#PANEL HEADER
	/*
		CONFIGS
		$data['panel_title']
		$data['panel_name']
		$data['panel_btn'][1]['class']
	    $data['panel_btn'][1]['attr']
	    $data['panel_btn'][1]['tooltip']
	    $data['panel_btn'][1]['title']*/

	function panel_head($data = false){
		$ci =& get_instance();
		$ci->load->view('layouts/_panel_head', $data);
	}

#PANEL FOOTER
	function panel_foot(){
		$ci =& get_instance();
		$ci->load->view('layouts/_panel_foot');
	}

#PANEL FOOTER
	function panel_tail(){
		$ci =& get_instance();
		$ci->load->view('layouts/_panel_foot');
	}

if(!function_exists('panel_head2'))
{
	function panel_head2($title = "Panel Title", $btns = false, $theme = "inverse")
	{
		echo "

			<div class='panel panel-$theme'>
			  <div class='panel-heading'>
			  	<h3 class='panel-title'>$title
						<span class='pull-right clickable' style = 'cursor:pointer;'><i class='glyphicon glyphicon-chevron-up'></i></span>
			  	 ";

		if($btns && is_array($btns)){
			echo '<div class="btn-group pull-right">';
			foreach ($btns as $key => $btn) {
				$url = isset($btn['url']) ? $btn['url'] : "";
				$btn_title = isset($btn['title']) ? $btn['title'] : "";
				$attr = isset($btn['attr']) ? $btn['attr'] : "";
				echo "<a style = 'color:#FFFFFF !important;' href='$url' $attr >$btn_title </a> &nbsp;";
			}
			echo '</div>';
		}

		echo		 "
				</h3>
			  </div>
			  <div class='panel-body' style='padding : 3px !important;'>
  				<div class=''>
		";
	}
}

if(!function_exists('panel_head3'))
{
	function panel_head3($title = "Panel Title", $btns = false, $theme = "primary")
	{
		echo "

			<div class='panel panel-$theme'>
			  <div class='panel-heading'>
			  	<h3 class='panel-title'>$title
						<span class='pull-right clickable' style = 'cursor:pointer;'><i class='glyphicon glyphicon-chevron-up'></i></span>
			  	 ";

		if($btns && is_array($btns)){
			echo '<div class="btn-group pull-right">';
			foreach ($btns as $key => $btn) {
				$url = isset($btn['url']) ? $btn['url'] : "";
				$btn_title = isset($btn['title']) ? $btn['title'] : "";
				$attr = isset($btn['attr']) ? $btn['attr'] : "";
				echo "<a style = 'color:#FFFFFF !important;' href='$url' $attr >$btn_title </a> &nbsp;";
			}
			echo '</div>';
		}

		echo		 "
			  </div>
			  <div class='panel-body' style='padding : 3px !important;'>
  				<div class=''>
		</h3>";
	}
}

if(!function_exists('panel_tail2'))
{
	function panel_tail2()
	{
		echo "
			</div> 
		  </div>
		</div>
		";
	}
}