<?php

/*
	Is Key Duplicate
	@param string $tablename table to check
	@param mixed $tablefield table field name to check
*/
function is_key_duplicate($tablename,$tablefield, $key){

	$ci =& get_instance();
	$ci->load->model('M_mymodel');
	$rs = $ci->M_mymodel->get_duplicate($tablename,$tablefield, $key);	

	return $rs != false ? true : false;
}


