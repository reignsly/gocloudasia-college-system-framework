<?php
	/**
	*Return the default database structure of the school system
	*/
	function get_db_default_sql(){
		return "/*
					SQLyog Ultimate v11.11 (64 bit)
					MySQL - 5.6.14 
					By : Sylver 2014-0710
					*********************************************************************
					*/


					/*!40101 SET NAMES utf8 */;

					/*!40101 SET SQL_MODE=''*/;

					/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
					/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
					/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
					/*Table structure for table `academic_years` */

					DROP TABLE IF EXISTS `academic_years`;

					CREATE TABLE `academic_years` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `year_from` int(11) DEFAULT NULL,
					  `year_to` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

					/*Data for the table `academic_years` */

					insert  into `academic_years`(`id`,`year_from`,`year_to`,`created_at`,`updated_at`) values (1,2013,2014,'2014-01-13 12:04:15','2014-01-13 12:04:15');
					insert  into `academic_years`(`id`,`year_from`,`year_to`,`created_at`,`updated_at`) values (2,2014,2015,'2014-06-04 12:06:35','2014-06-04 12:06:35');

					/*Table structure for table `activity_log` */

					DROP TABLE IF EXISTS `activity_log`;

					CREATE TABLE `activity_log` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `user` varchar(50) DEFAULT NULL COMMENT 'USERID/STUDID/EMPLOYEEID',
					  `action` varchar(255) DEFAULT NULL COMMENT 'ADD/EDIT/DELETE/OTHERS',
					  `remarks` text COMMENT 'REMARKS PUT ID OF ADDED/DELETED/UPDATED/OTHER',
					  `url` varchar(255) DEFAULT NULL COMMENT 'FULL URL TO KNOW CONTROLLER & METHOD',
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=338 DEFAULT CHARSET=latin1;

					/*Data for the table `activity_log` */

					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (99,'registrar','Confirm Enrollment','Success; Student ID: pedro','http://ub.384971892569381.info/confirm_enrollees/confirm/10','2014-01-22 15:32:45','2014-01-22 15:32:45');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (100,'registrar','Confirm Enrollment','Success; Student ID: ER-00000001','http://ub.384971892569381.info/confirm_enrollees/confirm/11','2014-01-24 11:14:47','2014-01-24 11:14:47');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (101,'registrar','add','add payment record by: registrar OR# OR-123; Amount: 3832.60; From student enrollment id: 4','http://ub.384971892569381.info/payments/add/4','2014-01-24 11:15:42','2014-01-24 11:15:42');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (102,'cashier','Re-assess fee','Re-assess fee by: cashierEnrollment id: 2','http://ub.384971892569381.info/fees/reasses_fees/2','2014-01-29 13:25:04','2014-01-29 13:25:04');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (103,'cashier','add','add payment record by: cashier OR# 1234561; Amount: 3640.20; From student enrollment id: 2','http://ub.384971892569381.info/payments/add/2','2014-01-29 13:37:31','2014-01-29 13:37:31');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (104,'studentaffairs','Grant Promisory','Grading Period :  Enrollment ID 4. Student Payment Detail ID : 13','http://ub.384971892569381.info/promisory/grant/13/4','2014-01-30 18:23:09','2014-01-30 18:23:09');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (105,'registree','Confirm Enrollment','Success; Student ID: ER-00000006','http://ub.384971892569381.info/confirm_enrollees/confirm/12','2014-03-31 10:21:28','2014-03-31 10:21:28');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (106,'cashier','add','add payment record by: cashier OR# or123sad; Amount: 3832.60; From student enrollment id: 5','http://ub.384971892569381.info/payments/add/5','2014-03-31 10:27:47','2014-03-31 10:27:47');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (107,'admin','assign course blocks','Assign course blocks Set by: adminSuccess; Assign course blocks Id: 2','http://ub.384971892569381.info/block_section_settings/course_blocks/1','2014-04-28 09:42:18','2014-04-28 09:42:18');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (108,'registrar','Confirm Enrollment','Success; Student ID: ER-00000007','http://ub.384971892569381.info/confirm_enrollees/confirm/9','2014-04-28 12:39:27','2014-04-28 12:39:27');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (109,'support','update School Settings','School Settings Updated by: admin; Settings Id: 1','http://ub.384971892569381.info/configure/edit/1','2014-04-28 15:28:04','2014-04-28 15:28:04');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (110,'support','Delete Menu Header',' Menu ID : 212 Caption : All Other Menus','http://ub.384971892569381.info/departments/delete_menu_head/2/212','2014-04-28 15:32:58','2014-04-28 15:32:58');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (111,'registrar','Confirm Enrollment','Success; Student ID: test','http://ub.384971892569381.info/confirm_enrollees/confirm/1','2014-05-01 05:46:31','2014-05-01 05:46:31');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (112,'registrar','Confirm Enrollment','Success; Student ID: 20142478','http://ub.384971892569381.info/confirm_enrollees/confirm/3','2014-05-01 05:46:57','2014-05-01 05:46:57');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (113,'support','Grading Period Set Current','Set by: supportSuccess;Grading Period Id : 2','http://ub.384971892569381.info/grading_periods','2014-05-12 14:40:15','2014-05-12 14:40:15');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (114,'support','updated fee','Updated by: supportSuccess; Fee Id: 1','http://ub.384971892569381.info/fees/edit/1/TUITION','2014-06-04 10:35:06','2014-06-04 10:35:06');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (115,'support','Add New Menu',' Menu ID : 276','http://ub.384971892569381.info/departments/add_menu/2/122','2014-06-04 11:22:37','2014-06-04 11:22:37');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (116,'support','Update Menu',' Menu ID : 124','http://ub.384971892569381.info/departments/menu_list/2/122','2014-06-04 11:23:10','2014-06-04 11:23:10');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (117,'support','Update Menu',' Menu ID : 123','http://ub.384971892569381.info/departments/menu_list/2/122','2014-06-04 11:23:10','2014-06-04 11:23:10');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (118,'support','Update Menu',' Menu ID : 276','http://ub.384971892569381.info/departments/menu_list/2/122','2014-06-04 11:23:10','2014-06-04 11:23:10');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (119,'support','Update Menu',' Menu ID : 276','http://ub.384971892569381.info/departments/menu_list/2/122','2014-06-04 11:23:18','2014-06-04 11:23:18');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (120,'support','Update Menu',' Menu ID : 123','http://ub.384971892569381.info/departments/menu_list/2/122','2014-06-04 11:23:18','2014-06-04 11:23:18');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (121,'support','Update Menu',' Menu ID : 124','http://ub.384971892569381.info/departments/menu_list/2/122','2014-06-04 11:23:18','2014-06-04 11:23:18');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (122,'support','Update Menu',' Menu ID : 124','http://ub.384971892569381.info/departments/menu_list/2/122','2014-06-04 11:23:28','2014-06-04 11:23:28');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (123,'support','Update Menu',' Menu ID : 276','http://ub.384971892569381.info/departments/menu_list/2/122','2014-06-04 11:23:28','2014-06-04 11:23:28');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (124,'support','Update Menu',' Menu ID : 123','http://ub.384971892569381.info/departments/menu_list/2/122','2014-06-04 11:23:28','2014-06-04 11:23:28');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (125,'support','updated fee','Updated by: supportSuccess; Fee Id: 22','http://ub.384971892569381.info/fees/edit/22/MISC','2014-06-04 11:34:45','2014-06-04 11:34:45');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (126,'support','updated fee','Updated by: supportSuccess; Fee Id: 2','http://ub.384971892569381.info/fees/edit/2/MISC','2014-06-04 11:35:32','2014-06-04 11:35:32');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (127,'support','updated fee','Updated by: supportSuccess; Fee Id: 3','http://ub.384971892569381.info/fees/edit/3/MISC','2014-06-04 11:36:01','2014-06-04 11:36:01');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (128,'support','updated fee','Updated by: supportSuccess; Fee Id: 4','http://ub.384971892569381.info/fees/edit/4/MISC','2014-06-04 11:36:32','2014-06-04 11:36:32');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (129,'support','updated fee','Updated by: supportSuccess; Fee Id: 4','http://ub.384971892569381.info/fees/edit/4/MISC','2014-06-04 11:36:36','2014-06-04 11:36:36');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (130,'support','updated fee','Updated by: supportSuccess; Fee Id: 5','http://ub.384971892569381.info/fees/edit/5/MISC','2014-06-04 11:37:11','2014-06-04 11:37:11');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (131,'support','updated fee','Updated by: supportSuccess; Fee Id: 6','http://ub.384971892569381.info/fees/edit/6/MISC','2014-06-04 11:37:53','2014-06-04 11:37:53');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (132,'support','updated fee','Updated by: supportSuccess; Fee Id: 7','http://ub.384971892569381.info/fees/edit/7/MISC','2014-06-04 11:38:40','2014-06-04 11:38:40');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (133,'support','updated fee','Updated by: supportSuccess; Fee Id: 8','http://ub.384971892569381.info/fees/edit/8/MISC','2014-06-04 11:39:30','2014-06-04 11:39:30');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (134,'support','updated fee','Updated by: supportSuccess; Fee Id: 9','http://ub.384971892569381.info/fees/edit/9/MISC','2014-06-04 11:40:07','2014-06-04 11:40:07');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (135,'support','updated fee','Updated by: supportSuccess; Fee Id: 10','http://ub.384971892569381.info/fees/edit/10/MISC','2014-06-04 11:40:41','2014-06-04 11:40:41');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (136,'support','updated fee','Updated by: supportSuccess; Fee Id: 11','http://ub.384971892569381.info/fees/edit/11/MISC','2014-06-04 11:41:23','2014-06-04 11:41:23');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (137,'support','updated fee','Updated by: supportSuccess; Fee Id: 12','http://ub.384971892569381.info/fees/edit/12/MISC','2014-06-04 11:42:12','2014-06-04 11:42:12');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (138,'support','updated fee','Updated by: supportSuccess; Fee Id: 13','http://ub.384971892569381.info/fees/edit/13/MISC','2014-06-04 11:42:45','2014-06-04 11:42:45');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (139,'support','updated fee','Updated by: supportSuccess; Fee Id: 14','http://ub.384971892569381.info/fees/edit/14/MISC','2014-06-04 11:43:20','2014-06-04 11:43:20');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (140,'support','updated fee','Updated by: supportSuccess; Fee Id: 15','http://ub.384971892569381.info/fees/edit/15/MISC','2014-06-04 11:43:52','2014-06-04 11:43:52');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (141,'support','updated fee','Updated by: supportSuccess; Fee Id: 16','http://ub.384971892569381.info/fees/edit/16/MISC','2014-06-04 11:45:49','2014-06-04 11:45:49');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (142,'support','updated fee','Updated by: supportSuccess; Fee Id: 21','http://ub.384971892569381.info/fees/edit/21/MISC','2014-06-04 11:46:29','2014-06-04 11:46:29');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (143,'support','updated fee','Updated by: supportSuccess; Fee Id: 17','http://ub.384971892569381.info/fees/edit/17/MISC','2014-06-04 11:47:07','2014-06-04 11:47:07');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (144,'support','updated fee','Updated by: supportSuccess; Fee Id: 20','http://ub.384971892569381.info/fees/edit/20/MISC','2014-06-04 11:47:38','2014-06-04 11:47:38');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (145,'support','updated fee','Updated by: supportSuccess; Fee Id: 18','http://ub.384971892569381.info/fees/edit/18/LAB','2014-06-04 11:50:20','2014-06-04 11:50:20');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (146,'support','updated fee','Updated by: supportSuccess; Fee Id: 19','http://ub.384971892569381.info/fees/edit/19/OTHER','2014-06-04 11:51:31','2014-06-04 11:51:31');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (147,'support','create fee','Created by: supportSuccess; Fee Id: 23','http://ub.384971892569381.info/fees/create/NSTP','2014-06-04 11:57:06','2014-06-04 11:57:06');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (148,'support','create academic years','Academic Years Created by: supportSuccess; Academic Years Id: ','http://ub.384971892569381.info/academic_years/create','2014-06-04 12:06:35','2014-06-04 12:06:35');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (149,'support','Grading Period Set Current','Set by: supportSuccess;Grading Period Id : 1','http://ub.384971892569381.info/grading_periods','2014-06-04 12:07:14','2014-06-04 12:07:14');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (150,'support','create payment_type','Created by: supportSuccess;Payment Type Id : 1','http://ub.384971892569381.info/payment_type/create','2014-06-05 06:11:32','2014-06-05 06:11:32');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (151,'support','create payment_type','Created by: supportSuccess;Payment Type Id : 2','http://ub.384971892569381.info/payment_type/create','2014-06-05 06:11:43','2014-06-05 06:11:43');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (152,'support','create Block section settings','Block_section_settings Created by: adminSuccess; Block_section_settings Id: 2','http://ub.384971892569381.info/block_section_settings/create','2014-06-05 17:54:04','2014-06-05 17:54:04');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (153,'admin','assign course blocks','Assign course blocks Set by: adminSuccess; Assign course blocks Id: 3','http://ub.384971892569381.info/block_section_settings/course_blocks/2','2014-06-05 17:55:27','2014-06-05 17:55:27');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (154,'support','create assign course fee','Assign Course Created by: adminSuccess; Assign Coure Id: 2','http://ub.384971892569381.info/coursefinances/assign_course/2','2014-06-20 14:37:25','2014-06-20 14:37:25');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (155,'support','delete course fee','Course Fee Deleted by: adminSuccess; Course Fee Id: 2','http://ub.384971892569381.info/coursefinances/destroy/2','2014-06-20 14:39:51','2014-06-20 14:39:51');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (156,'support','create course finance','Course Finances Created by: adminSuccess; Course Finances Id: 3','http://ub.384971892569381.info/coursefinances/create','2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (157,'registrar','Unconfirm Enrollment','Unconfirm; Student ID: ER-00000018','http://ub.384971892569381.info/confirm_enrollees/unconfirm/16','2014-06-20 18:20:53','2014-06-20 18:20:53');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (158,'registrar','Confirm Enrollment','Success; Student ID: ER-00000020','http://ub.384971892569381.info/confirm_enrollees/confirm/17','2014-06-20 18:33:30','2014-06-20 18:33:30');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (159,'support','create assign course fee','Assign Course Created by: adminSuccess; Assign Coure Id: 3','http://ub.384971892569381.info/coursefinances/assign_course/3','2014-06-20 19:02:08','2014-06-20 19:02:08');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (160,'support','updated fee','Updated by: supportSuccess; Fee Id: 19','http://ub.384971892569381.info/fees/edit/19/OTHER','2014-06-20 19:50:44','2014-06-20 19:50:44');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (161,'support','update course finance','Course Finances Updated by: adminSuccess; Course Finances Id: 3','http://ub.384971892569381.info/coursefinances/edit/3','2014-06-21 14:05:40','2014-06-21 14:05:40');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (162,'support','update course finance','Course Finances Updated by: adminSuccess; Course Finances Id: 3','http://ub.384971892569381.info/coursefinances/edit/3','2014-06-21 14:54:05','2014-06-21 14:54:05');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (163,'registrar','Confirm Enrollment','Success; Student ID: ER-00000021','http://ub.384971892569381.info/confirm_enrollees/confirm/18','2014-06-21 14:57:23','2014-06-21 14:57:23');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (164,'support','Updated Department Student Profile Access','Department ID5 Access : 11011','http://ub.384971892569381.info/departments/student_profile_access/5','2014-06-21 16:02:38','2014-06-21 16:02:38');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (165,'support','Updated Department Student Profile Access','Department ID6 Access : 10101','http://ub.384971892569381.info/departments/student_profile_access/6','2014-06-21 16:03:04','2014-06-21 16:03:04');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (166,'support','Hide Department',' Department Code :  ID : 7','http://ub.384971892569381.info/departments/hide/7','2014-06-21 16:04:02','2014-06-21 16:04:02');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (167,'support','Create New Menu Head/Group',' Menu ID : 277','http://ub.384971892569381.info/departments/add_menu/2','2014-06-21 16:05:07','2014-06-21 16:05:07');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (168,'support','Add New Menu',' Menu ID : 278','http://ub.384971892569381.info/departments/add_menu/2','2014-06-21 16:05:07','2014-06-21 16:05:07');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (169,'support','Add New Menu',' Menu ID : 279','http://ub.384971892569381.info/departments/add_menu/2','2014-06-21 16:05:07','2014-06-21 16:05:07');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (170,'support','Add New Menu',' Menu ID : 280','http://ub.384971892569381.info/departments/add_menu/2','2014-06-21 16:05:07','2014-06-21 16:05:07');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (171,'support','Delete Menu Header',' Menu ID : 132 Caption : Employee Section','http://ub.384971892569381.info/departments/delete_menu_head/2/132','2014-06-21 16:09:35','2014-06-21 16:09:35');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (172,'support','Delete Menu',' Menu ID : 280 Caption : Employee Attendance','http://ub.384971892569381.info/departments/delete_menu_list/2/280','2014-06-21 16:09:47','2014-06-21 16:09:47');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (173,'support','Add New Menu',' Menu ID : 281','http://ub.384971892569381.info/departments/add_menu/2/277','2014-06-21 16:10:04','2014-06-21 16:10:04');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (174,'finance-1','Recompute Fees','Data : [Enrollment id:7] - [Users Id:12]','http://ub.384971892569381.info/fees/recompute/7','2014-06-21 16:19:51','2014-06-21 16:19:51');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (175,'support','Updated Department','Department ID','http://ub.384971892569381.info/departments/edit/6','2014-06-21 16:24:26','2014-06-21 16:24:26');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (176,'support','updated payment_type','Updated by: supportSuccess;Payment Type Id : 1','http://ub.384971892569381.info/payment_type/edit/1','2014-06-21 16:51:22','2014-06-21 16:51:22');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (177,'support','updated payment_type','Updated by: supportSuccess;Payment Type Id : 1','http://ub.384971892569381.info/payment_type/edit/1','2014-06-21 16:51:52','2014-06-21 16:51:52');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (178,'registrar','Confirm Enrollment','Success; Student ID: ER-00000024','http://ub.384971892569381.info/confirm_enrollees/confirm/19','2014-06-21 17:45:50','2014-06-21 17:45:50');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (179,'support','Delete Menu Header',' Menu ID : 130 Caption : Report Settings','http://ub.384971892569381.info/departments/delete_menu_head/2/130','2014-06-24 17:30:25','2014-06-24 17:30:25');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (180,'support','Hide Department',' Department Code :  ID : 10','http://ub.384971892569381.info/departments/hide/10','2014-06-26 13:57:19','2014-06-26 13:57:19');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (181,'registrar','Confirm Enrollment','Success; Student ID: ER-00000029','http://ub.384971892569381.info/confirm_enrollees/confirm/22','2014-06-26 16:45:50','2014-06-26 16:45:50');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (182,'support','create fee','Created by: support Success; Fee Id: 24','http://ub.384971892569381.info/fees/create/TUITION','2014-06-26 17:55:29','2014-06-26 17:55:29');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (183,'support','destroy fee','Deleted by: supportSuccess; Fee Id: 19','http://ub.384971892569381.info/fees/destroy/19/OTHER','2014-06-26 17:56:02','2014-06-26 17:56:02');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (184,'support','updated fee','Updated by: supportSuccess; Fee Id: 24','http://ub.384971892569381.info/fees/edit/24/OTHER_SCHOOL','2014-06-26 17:57:41','2014-06-26 17:57:41');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (185,'support','create fee','Created by: support Success; Fee Id: 25','http://ub.384971892569381.info/fees/create/OTHER','2014-06-26 17:58:40','2014-06-26 17:58:40');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (186,'support','create fee','Created by: support Success; Fee Id: 26','http://ub.384971892569381.info/fees/create/OTHER','2014-06-26 18:00:11','2014-06-26 18:00:11');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (187,'support','create course fee','Course Fee Created by: adminSuccess; Coure Fee Id: 3','http://ub.384971892569381.info/coursefinances/create_fees/3','2014-06-26 18:26:53','2014-06-26 18:26:53');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (188,'support','Delete Menu',' Menu ID : 174 Caption : Confirm Enrollees','http://ub.384971892569381.info/departments/delete_menu_list/5/174','2014-06-27 07:05:32','2014-06-27 07:05:32');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (189,'support','Create New Menu Head/Group',' Menu ID : 282','http://ub.384971892569381.info/departments/add_menu/5/91','2014-06-27 07:05:56','2014-06-27 07:05:56');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (190,'support','Add New Menu',' Menu ID : 283','http://ub.384971892569381.info/departments/add_menu/5/91','2014-06-27 07:05:56','2014-06-27 07:05:56');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (191,'support','Update Menu Group',' Menu ID : 282','http://ub.384971892569381.info/departments/edit_menu_group/5/282','2014-06-27 07:06:35','2014-06-27 07:06:35');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (192,'support','Add New Menu',' Menu ID : 284','http://ub.384971892569381.info/departments/add_menu/5/282','2014-06-27 07:48:17','2014-06-27 07:48:17');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (193,'registrar','Update Menu',' Menu ID : 283','http://ub.384971892569381.info/departments/menu_list/5/282','2014-06-27 07:52:55','2014-06-27 07:52:55');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (194,'registrar','Update Menu',' Menu ID : 284','http://ub.384971892569381.info/departments/menu_list/5/282','2014-06-27 07:52:55','2014-06-27 07:52:55');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (195,'registrar','Delete Menu',' Menu ID : 284 Caption : List of Confirm Enrollee','http://ub.384971892569381.info/departments/delete_menu_list/5/284','2014-06-27 07:54:24','2014-06-27 07:54:24');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (196,'registrar','Add New Menu',' Menu ID : 285','http://ub.384971892569381.info/departments/add_menu/5/282','2014-06-27 07:54:48','2014-06-27 07:54:48');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (197,'registrar','Update Menu',' Menu ID : 283','http://ub.384971892569381.info/departments/menu_list/5/282','2014-06-27 07:55:06','2014-06-27 07:55:06');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (198,'registrar','Update Menu',' Menu ID : 285','http://ub.384971892569381.info/departments/menu_list/5/282','2014-06-27 07:55:06','2014-06-27 07:55:06');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (199,'registrar','Confirm Enrollment','Success; Student ID: ER-00000032','http://ub.384971892569381.info/confirm_enrollees/confirm/23','2014-06-27 09:13:21','2014-06-27 09:13:21');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (200,'support','Delete Menu',' Menu ID : 94 Caption : Enrollees','http://ub.384971892569381.info/departments/delete_menu_list/5/94','2014-06-27 10:26:36','2014-06-27 10:26:36');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (201,'support','Add New Menu',' Menu ID : 286','http://ub.384971892569381.info/departments/add_menu/5/282','2014-06-27 10:26:48','2014-06-27 10:26:48');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (202,'registrar','Delete Menu',' Menu ID : 181 Caption : Officially Enrolled','http://ub.384971892569381.info/departments/delete_menu_list/6/181','2014-06-27 10:49:55','2014-06-27 10:49:55');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (203,'registrar','Delete Menu',' Menu ID : 182 Caption : Enrollees','http://ub.384971892569381.info/departments/delete_menu_list/6/182','2014-06-27 10:49:59','2014-06-27 10:49:59');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (204,'registrar','Create New Menu Head/Group',' Menu ID : 287','http://ub.384971892569381.info/departments/add_menu/6/180','2014-06-27 10:50:30','2014-06-27 10:50:30');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (205,'registrar','Add New Menu',' Menu ID : 288','http://ub.384971892569381.info/departments/add_menu/6/180','2014-06-27 10:50:30','2014-06-27 10:50:30');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (206,'registrar','Add New Menu',' Menu ID : 289','http://ub.384971892569381.info/departments/add_menu/6/180','2014-06-27 10:50:30','2014-06-27 10:50:30');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (207,'registrar','Update Menu Group',' Menu ID : 287','http://ub.384971892569381.info/departments/edit_menu_group/6/287','2014-06-27 10:51:32','2014-06-27 10:51:32');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (208,'support','Delete Menu',' Menu ID : 37 Caption : Officially Enrolled','http://ub.384971892569381.info/departments/delete_menu_list/11/37','2014-06-27 11:07:04','2014-06-27 11:07:04');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (209,'support','Delete Menu',' Menu ID : 38 Caption : Enrollees','http://ub.384971892569381.info/departments/delete_menu_list/11/38','2014-06-27 11:07:09','2014-06-27 11:07:09');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (210,'support','Create New Menu Head/Group',' Menu ID : 290','http://ub.384971892569381.info/departments/add_menu/11/36','2014-06-27 11:07:39','2014-06-27 11:07:39');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (211,'support','Add New Menu',' Menu ID : 291','http://ub.384971892569381.info/departments/add_menu/11/36','2014-06-27 11:07:39','2014-06-27 11:07:39');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (212,'support','Add New Menu',' Menu ID : 292','http://ub.384971892569381.info/departments/add_menu/11/36','2014-06-27 11:07:39','2014-06-27 11:07:39');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (213,'support','Update Menu Group',' Menu ID : 290','http://ub.384971892569381.info/departments/edit_menu_group/11/290','2014-06-27 11:07:54','2014-06-27 11:07:54');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (214,'support','Updated Department Student Profile Access','Department ID11 Access : 10101','http://ub.384971892569381.info/departments/student_profile_access/11','2014-06-27 11:09:12','2014-06-27 11:09:12');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (215,'support','Delete Menu',' Menu ID : 183 Caption : Payment Center','http://ub.384971892569381.info/departments/delete_menu_list/6/183','2014-06-27 11:23:04','2014-06-27 11:23:04');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (216,'support','Delete Menu',' Menu ID : 39 Caption : Payment Center','http://ub.384971892569381.info/departments/delete_menu_list/11/39','2014-06-27 11:23:27','2014-06-27 11:23:27');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (217,'finance','Recompute Fees','Data : [Enrollment id:11] - [Users Id:4]','http://ub.384971892569381.info/fees/recompute/11','2014-06-27 15:37:39','2014-06-27 15:37:39');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (218,'finance','Recompute Fees','Data : [Enrollment id:8] - [Users Id:4]','http://ub.384971892569381.info/fees/recompute/8','2014-06-27 15:38:12','2014-06-27 15:38:12');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (219,'finance','Recompute Fees','Data : [Enrollment id:7] - [Users Id:4]','http://ub.384971892569381.info/fees/recompute/7','2014-06-27 15:38:26','2014-06-27 15:38:26');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (220,'support','Delete Menu Header',' Menu ID : 282 Caption : Enrollment','http://ub.384971892569381.info/departments/delete_menu_head/5/282','2014-06-27 17:51:18','2014-06-27 17:51:18');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (221,'support','Create New Menu Head/Group',' Menu ID : 293','http://ub.384971892569381.info/departments/add_menu/5/91','2014-06-27 17:51:55','2014-06-27 17:51:55');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (222,'support','Add New Menu',' Menu ID : 294','http://ub.384971892569381.info/departments/add_menu/5/91','2014-06-27 17:51:55','2014-06-27 17:51:55');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (223,'support','Add New Menu',' Menu ID : 295','http://ub.384971892569381.info/departments/add_menu/5/91','2014-06-27 17:51:55','2014-06-27 17:51:55');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (224,'support','Add New Menu',' Menu ID : 296','http://ub.384971892569381.info/departments/add_menu/5/91','2014-06-27 17:51:55','2014-06-27 17:51:55');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (225,'support','Update Menu Group',' Menu ID : 293','http://ub.384971892569381.info/departments/edit_menu_group/5/293','2014-06-27 17:52:14','2014-06-27 17:52:14');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (226,'support','Update Menu',' Menu ID : 294','http://ub.384971892569381.info/departments/menu_list/5/293','2014-06-27 17:52:34','2014-06-27 17:52:34');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (227,'support','Update Menu',' Menu ID : 295','http://ub.384971892569381.info/departments/menu_list/5/293','2014-06-27 17:52:34','2014-06-27 17:52:34');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (228,'support','Update Menu',' Menu ID : 296','http://ub.384971892569381.info/departments/menu_list/5/293','2014-06-27 17:52:34','2014-06-27 17:52:34');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (229,'support','Delete Menu Header',' Menu ID : 287 Caption : Enrollment','http://ub.384971892569381.info/departments/delete_menu_head/6/287','2014-06-27 17:55:26','2014-06-27 17:55:26');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (230,'support','Create New Menu Head/Group',' Menu ID : 297','http://ub.384971892569381.info/departments/add_menu/6/180','2014-06-27 17:55:47','2014-06-27 17:55:47');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (231,'support','Add New Menu',' Menu ID : 298','http://ub.384971892569381.info/departments/add_menu/6/180','2014-06-27 17:55:47','2014-06-27 17:55:47');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (232,'support','Add New Menu',' Menu ID : 299','http://ub.384971892569381.info/departments/add_menu/6/180','2014-06-27 17:55:47','2014-06-27 17:55:47');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (233,'support','Delete Menu Header',' Menu ID : 290 Caption : Enrollment','http://ub.384971892569381.info/departments/delete_menu_head/11/290','2014-06-27 17:56:09','2014-06-27 17:56:09');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (234,'support','Create New Menu Head/Group',' Menu ID : 300','http://ub.384971892569381.info/departments/add_menu/11/36','2014-06-27 17:56:29','2014-06-27 17:56:29');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (235,'support','Add New Menu',' Menu ID : 301','http://ub.384971892569381.info/departments/add_menu/11/36','2014-06-27 17:56:29','2014-06-27 17:56:29');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (236,'support','Add New Menu',' Menu ID : 302','http://ub.384971892569381.info/departments/add_menu/11/36','2014-06-27 17:56:29','2014-06-27 17:56:29');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (237,'support','Update Menu Group',' Menu ID : 300','http://ub.384971892569381.info/departments/edit_menu_group/11/300','2014-06-27 17:56:48','2014-06-27 17:56:48');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (238,'support','Update Menu Group',' Menu ID : 297','http://ub.384971892569381.info/departments/edit_menu_group/6/297','2014-06-27 17:57:11','2014-06-27 17:57:11');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (239,'support','Add New Menu',' Menu ID : 303','http://localhost/ub_system/departments/add_menu/2/113','2014-06-30 17:24:07','2014-06-30 17:24:07');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (240,'support','Delete Menu',' Menu ID : 303 Caption : Curicullum','http://localhost/ub_system/departments/delete_menu_list/2/303','2014-06-30 17:24:38','2014-06-30 17:24:38');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (241,'support','Add New Menu',' Menu ID : 304','http://localhost/ub_system/departments/add_menu/2/113','2014-06-30 17:25:12','2014-06-30 17:25:12');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (242,'support','Add New Menu',' Menu ID : 305','http://localhost/ub_system/departments/add_menu/2/170','2014-07-01 11:48:30','2014-07-01 11:48:30');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (243,'support','Updated Department Student Profile Access','Department ID2 Access : 11111','http://localhost/ub_system/departments/student_profile_access/2','2014-07-01 11:48:53','2014-07-01 11:48:53');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (244,'support','Add Payment Record','Data : [Enrollment ID:7] - [OR NO:asdf3234] - [Date of Payment:2014-07-01]','http://localhost/ub_system/fees/add_student_payment/7','2014-07-01 11:49:14','2014-07-01 11:49:14');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (245,'support','create curriculum subject','Data : [curriculum_id:1] - [subject_id:2] - [year_id:3] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/1','2014-07-01 15:06:28','2014-07-01 15:06:28');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (246,'support','create Subject pre-requisite','ID : 1Data : [curriculum_id:1] - [curriculum_sub_id:3] - [subject_id:7]','http://localhost/ub_system/curriculum/add_subjects/1/single','2014-07-01 17:39:00','2014-07-01 17:39:00');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (247,'support','create Subject pre-requisite','ID : 2Data : [curriculum_id:1] - [curriculum_sub_id:3] - [subject_id:8]','http://localhost/ub_system/curriculum/add_subjects/1/single','2014-07-01 17:39:00','2014-07-01 17:39:00');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (248,'support','create curriculum subject','ID : 3Data : [curriculum_id:1] - [curriculum_sub_id:3] - [subject_id:8]','http://localhost/ub_system/curriculum/add_subjects/1/single','2014-07-01 17:39:00','2014-07-01 17:39:00');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (249,'support','create course','Courses Created by: adminSuccess; Courses Id: ','http://localhost/ub_system/courses/create','2014-07-01 18:03:48','2014-07-01 18:03:48');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (250,'support','create course','Courses Created by: adminSuccess; Courses Id: ','http://localhost/ub_system/courses/create','2014-07-01 18:07:56','2014-07-01 18:07:56');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (251,'support','create course','Courses Created by: adminSuccess; Courses Id: ','http://localhost/ub_system/courses/create','2014-07-01 18:08:51','2014-07-01 18:08:51');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (252,'support','create course','Courses Created by: adminSuccess; Courses Id: ','http://localhost/ub_system/courses/create','2014-07-01 18:09:00','2014-07-01 18:09:00');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (253,'support','Deactive curriculum','ID : 1 Data : [is_deleted:1] - [deleted_by:1] - [deleted_date:2014-07-02 11:03:35]','http://localhost/ub_system/curriculum/deactivate/1','2014-07-02 11:03:35','2014-07-02 11:03:35');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (254,'support','Active curriculum','ID : 1 Data : [is_deleted:0] - [deleted_by:1] - [deleted_date:2014-07-02 11:07:53]','http://localhost/ub_system/curriculum/activate/1','2014-07-02 11:07:53','2014-07-02 11:07:53');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (255,'support','Deactive curriculum','ID : 1 Data : [is_deleted:1] - [deleted_by:1] - [deleted_date:2014-07-02 11:08:11]','http://localhost/ub_system/curriculum/deactivate/1','2014-07-02 11:08:11','2014-07-02 11:08:11');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (256,'support','Active curriculum','ID : 1 Data : [is_deleted:0] - [deleted_by:1] - [deleted_date:2014-07-02 11:08:18]','http://localhost/ub_system/curriculum/activate/1','2014-07-02 11:08:18','2014-07-02 11:08:18');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (257,'support','create curriculum','Data : [name:BIT] - [course_id:2] - [academic_year_id:2] - [sy_from:2014] - [sy_to:2015]','http://localhost/ub_system/curriculum/create','2014-07-02 11:09:04','2014-07-02 11:09:04');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (258,'support','create curriculum','Data : [name:BSIT-A] - [course_id:2] - [academic_year_id:2] - [sy_from:2014] - [sy_to:2015]','http://localhost/ub_system/curriculum/create','2014-07-02 11:22:25','2014-07-02 11:22:25');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (259,'support','create curriculum','Data : [name:BSIT] - [course_id:2] - [academic_year_id:2] - [sy_from:2014] - [sy_to:2015] - [is_deleted:0]','http://localhost/ub_system/curriculum/create','2014-07-02 11:24:58','2014-07-02 11:24:58');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (260,'support','create curriculum','Data : [name:BSIT] - [course_id:2] - [academic_year_id:2] - [sy_from:2014] - [sy_to:2015] - [is_deleted:1]','http://localhost/ub_system/curriculum/create','2014-07-02 11:26:12','2014-07-02 11:26:12');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (261,'support','create curriculum','Data : [name:asdfasdf] - [course_id:2] - [academic_year_id:2] - [sy_from:2014] - [sy_to:2015] - [is_deleted:1]','http://localhost/ub_system/curriculum/create','2014-07-02 11:31:12','2014-07-02 11:31:12');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (262,'support','create curriculum subject','Data : [curriculum_id:6] - [subject_id:2] - [year_id:3] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/6','2014-07-02 11:33:27','2014-07-02 11:33:27');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (263,'support','create curriculum subject','Data : [curriculum_id:6] - [subject_id:3] - [year_id:3] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/6','2014-07-02 11:33:27','2014-07-02 11:33:27');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (264,'support','create curriculum subject','Data : [curriculum_id:6] - [subject_id:4] - [year_id:3] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/6','2014-07-02 11:33:27','2014-07-02 11:33:27');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (265,'support','create curriculum subject','Data : [curriculum_id:6] - [subject_id:10] - [year_id:3] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/6','2014-07-02 11:33:27','2014-07-02 11:33:27');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (266,'support','create curriculum subject','Data : [curriculum_id:6] - [subject_id:7] - [year_id:3] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/6','2014-07-02 11:33:27','2014-07-02 11:33:27');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (267,'support','Active curriculum','ID : 6 Data : [is_deleted:0] - [deleted_by:1] - [deleted_date:2014-07-02 11:36:34]','http://localhost/ub_system/curriculum/activate/6','2014-07-02 11:36:34','2014-07-02 11:36:34');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (268,'support','Active curriculum','ID : 5 Data : [is_deleted:0] - [deleted_by:1] - [deleted_date:2014-07-02 11:37:32]','http://localhost/ub_system/curriculum/activate/5','2014-07-02 11:37:32','2014-07-02 11:37:32');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (269,'support','Active curriculum','ID : 6 Data : [is_deleted:0] - [deleted_by:1] - [deleted_date:2014-07-02 11:42:37]','http://localhost/ub_system/curriculum/activate/6','2014-07-02 11:42:37','2014-07-02 11:42:37');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (270,'support','create curriculum','Data : [name:COE] - [course_id:6] - [academic_year_id:2] - [sy_from:2014] - [sy_to:2015] - [is_deleted:0]','http://localhost/ub_system/curriculum/create','2014-07-02 11:43:06','2014-07-02 11:43:06');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (271,'support','create curriculum','Data : [name:COA] - [course_id:5] - [academic_year_id:2] - [sy_from:2014] - [sy_to:2015] - [is_deleted:0]','http://localhost/ub_system/curriculum/create','2014-07-02 17:38:32','2014-07-02 17:38:32');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (272,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:2] - [year_id:3] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:08','2014-07-02 17:39:08');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (273,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:3] - [year_id:3] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:08','2014-07-02 17:39:08');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (274,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:4] - [year_id:3] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:08','2014-07-02 17:39:08');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (275,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:10] - [year_id:3] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:08','2014-07-02 17:39:08');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (276,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:7] - [year_id:3] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:08','2014-07-02 17:39:08');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (277,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:8] - [year_id:3] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:08','2014-07-02 17:39:08');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (278,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:9] - [year_id:3] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:08','2014-07-02 17:39:08');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (279,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:5] - [year_id:3] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:08','2014-07-02 17:39:08');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (280,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:6] - [year_id:3] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:08','2014-07-02 17:39:08');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (281,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:11] - [year_id:3] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:08','2014-07-02 17:39:08');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (282,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:2] - [year_id:3] - [semester_id:2]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:28','2014-07-02 17:39:28');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (283,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:3] - [year_id:3] - [semester_id:2]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:28','2014-07-02 17:39:28');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (284,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:4] - [year_id:3] - [semester_id:2]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:28','2014-07-02 17:39:28');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (285,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:10] - [year_id:3] - [semester_id:2]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:28','2014-07-02 17:39:28');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (286,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:7] - [year_id:3] - [semester_id:2]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:28','2014-07-02 17:39:28');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (287,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:2] - [year_id:3] - [semester_id:3]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:42','2014-07-02 17:39:42');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (288,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:3] - [year_id:3] - [semester_id:3]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:42','2014-07-02 17:39:42');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (289,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:4] - [year_id:3] - [semester_id:3]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:42','2014-07-02 17:39:42');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (290,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:5] - [year_id:3] - [semester_id:3]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:42','2014-07-02 17:39:42');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (291,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:6] - [year_id:3] - [semester_id:3]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:42','2014-07-02 17:39:42');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (292,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:11] - [year_id:3] - [semester_id:3]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:39:42','2014-07-02 17:39:42');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (293,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:2] - [year_id:4] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:42:39','2014-07-02 17:42:39');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (294,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:3] - [year_id:4] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:42:39','2014-07-02 17:42:39');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (295,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:10] - [year_id:4] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:42:39','2014-07-02 17:42:39');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (296,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:7] - [year_id:4] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:42:39','2014-07-02 17:42:39');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (297,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:5] - [year_id:4] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:42:39','2014-07-02 17:42:39');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (298,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:6] - [year_id:4] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:42:39','2014-07-02 17:42:39');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (299,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:11] - [year_id:4] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:42:39','2014-07-02 17:42:39');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (300,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:2] - [year_id:4] - [semester_id:2]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:42:55','2014-07-02 17:42:55');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (301,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:3] - [year_id:4] - [semester_id:2]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:42:55','2014-07-02 17:42:55');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (302,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:4] - [year_id:4] - [semester_id:2]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:42:55','2014-07-02 17:42:55');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (303,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:10] - [year_id:4] - [semester_id:2]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:42:55','2014-07-02 17:42:55');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (304,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:7] - [year_id:4] - [semester_id:2]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:42:55','2014-07-02 17:42:55');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (305,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:5] - [year_id:4] - [semester_id:2]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:42:55','2014-07-02 17:42:55');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (306,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:6] - [year_id:4] - [semester_id:2]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:42:55','2014-07-02 17:42:55');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (307,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:11] - [year_id:4] - [semester_id:2]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:42:55','2014-07-02 17:42:55');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (308,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:2] - [year_id:4] - [semester_id:3]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:09','2014-07-02 17:43:09');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (309,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:3] - [year_id:4] - [semester_id:3]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:09','2014-07-02 17:43:09');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (310,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:10] - [year_id:4] - [semester_id:3]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:09','2014-07-02 17:43:09');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (311,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:2] - [year_id:5] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:23','2014-07-02 17:43:23');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (312,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:3] - [year_id:5] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:23','2014-07-02 17:43:23');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (313,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:4] - [year_id:5] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:23','2014-07-02 17:43:23');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (314,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:10] - [year_id:5] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:23','2014-07-02 17:43:23');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (315,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:7] - [year_id:5] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:23','2014-07-02 17:43:23');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (316,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:5] - [year_id:5] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:23','2014-07-02 17:43:23');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (317,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:6] - [year_id:5] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:23','2014-07-02 17:43:23');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (318,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:11] - [year_id:5] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:23','2014-07-02 17:43:23');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (319,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:2] - [year_id:5] - [semester_id:3]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:41','2014-07-02 17:43:41');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (320,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:3] - [year_id:5] - [semester_id:3]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:41','2014-07-02 17:43:41');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (321,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:10] - [year_id:5] - [semester_id:3]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:41','2014-07-02 17:43:41');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (322,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:2] - [year_id:6] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:54','2014-07-02 17:43:54');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (323,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:8] - [year_id:6] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:54','2014-07-02 17:43:54');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (324,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:9] - [year_id:6] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:54','2014-07-02 17:43:54');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (325,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:5] - [year_id:6] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:54','2014-07-02 17:43:54');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (326,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:6] - [year_id:6] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:54','2014-07-02 17:43:54');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (327,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:11] - [year_id:6] - [semester_id:1]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:43:54','2014-07-02 17:43:54');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (328,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:10] - [year_id:6] - [semester_id:2]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:44:06','2014-07-02 17:44:06');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (329,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:8] - [year_id:6] - [semester_id:2]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:44:06','2014-07-02 17:44:06');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (330,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:9] - [year_id:6] - [semester_id:2]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:44:06','2014-07-02 17:44:06');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (331,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:5] - [year_id:6] - [semester_id:2]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:44:06','2014-07-02 17:44:06');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (332,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:6] - [year_id:6] - [semester_id:2]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:44:06','2014-07-02 17:44:06');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (333,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:11] - [year_id:6] - [semester_id:2]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:44:06','2014-07-02 17:44:06');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (334,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:2] - [year_id:6] - [semester_id:3]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:44:13','2014-07-02 17:44:13');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (335,'support','create curriculum subject','Data : [curriculum_id:8] - [subject_id:3] - [year_id:6] - [semester_id:3]','http://localhost/ub_system/curriculum/add_subjects/8','2014-07-02 17:44:13','2014-07-02 17:44:13');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (336,'support','create employee','User/employee Created by: adminSuccess; Employee Id: 9','http://ub.dev/employees/create','2014-07-10 10:32:42','2014-07-10 10:32:42');
					insert  into `activity_log`(`id`,`user`,`action`,`remarks`,`url`,`created_at`,`updated_at`) values (337,'support','DE-ACTIVATE  Employee','User ID : 22 User Login : admin','http://ub.dev/users/activate/22/1','2014-07-10 10:36:40','2014-07-10 10:36:40');

					/*Table structure for table `additional_charges` */

					DROP TABLE IF EXISTS `additional_charges`;

					CREATE TABLE `additional_charges` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `value` float DEFAULT NULL,
					  `remarks` text,
					  `enrollment_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `additional_charges` */

					/*Table structure for table `admission_slips` */

					DROP TABLE IF EXISTS `admission_slips`;

					CREATE TABLE `admission_slips` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `date` datetime DEFAULT NULL,
					  `inclusive_dates_of_absences` varchar(255) DEFAULT NULL,
					  `subject` varchar(255) DEFAULT NULL,
					  `reason` varchar(255) DEFAULT NULL,
					  `categoy` varchar(255) DEFAULT NULL,
					  `sao_remark` varchar(255) DEFAULT NULL,
					  `enrollment_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `name` varchar(255) DEFAULT NULL,
					  `course_and_year` varchar(255) DEFAULT NULL,
					  `category` varchar(255) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `admission_slips` */

					/*Table structure for table `announcements` */

					DROP TABLE IF EXISTS `announcements`;

					CREATE TABLE `announcements` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `message` text,
					  `to_student` tinyint(1) DEFAULT NULL,
					  `to_employee` tinyint(1) DEFAULT NULL,
					  `to_all` tinyint(1) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `user_id` int(11) DEFAULT NULL,
					  `date` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `announcements` */

					/*Table structure for table `assesment_settings` */

					DROP TABLE IF EXISTS `assesment_settings`;

					CREATE TABLE `assesment_settings` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `title` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `assesment_settings` */

					/*Table structure for table `assign_coursefees` */

					DROP TABLE IF EXISTS `assign_coursefees`;

					CREATE TABLE `assign_coursefees` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `year_id` int(11) DEFAULT NULL,
					  `course_id` int(11) DEFAULT NULL,
					  `coursefinance_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `semester_id` int(11) DEFAULT NULL,
					  `academic_year_id` int(11) DEFAULT NULL,
					  `is_old` tinyint(1) NOT NULL DEFAULT '0',
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

					/*Data for the table `assign_coursefees` */

					insert  into `assign_coursefees`(`id`,`year_id`,`course_id`,`coursefinance_id`,`created_at`,`updated_at`,`semester_id`,`academic_year_id`,`is_old`) values (3,3,1,3,'2014-06-20 19:02:08','2014-06-20 19:02:08',1,2,0);

					/*Table structure for table `assign_courses` */

					DROP TABLE IF EXISTS `assign_courses`;

					CREATE TABLE `assign_courses` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `course_id` int(11) DEFAULT NULL,
					  `employee_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `assign_courses` */

					/*Table structure for table `assign_subjects` */

					DROP TABLE IF EXISTS `assign_subjects`;

					CREATE TABLE `assign_subjects` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `room` varchar(255) DEFAULT NULL,
					  `day` varchar(255) DEFAULT NULL,
					  `time_schedule_from` varchar(255) DEFAULT NULL,
					  `time_schedule_to` varchar(255) DEFAULT NULL,
					  `subject` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `assign_subjects` */

					/*Table structure for table `assign_teachers` */

					DROP TABLE IF EXISTS `assign_teachers`;

					CREATE TABLE `assign_teachers` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `room` varchar(255) DEFAULT NULL,
					  `day` varchar(255) DEFAULT NULL,
					  `time_schedule_from` varchar(255) DEFAULT NULL,
					  `time_schedule_to` varchar(255) DEFAULT NULL,
					  `teacher_name` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `assign_teachers` */

					/*Table structure for table `assignsts` */

					DROP TABLE IF EXISTS `assignsts`;

					CREATE TABLE `assignsts` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `assignsubject_id` int(11) DEFAULT NULL,
					  `employee_id` int(11) DEFAULT NULL,
					  `subjecttimeschedule_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `assignsts` */

					/*Table structure for table `assignsubjects` */

					DROP TABLE IF EXISTS `assignsubjects`;

					CREATE TABLE `assignsubjects` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `subject_id` varchar(255) DEFAULT NULL,
					  `employee_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `sts` varchar(255) DEFAULT NULL,
					  `year_id` int(11) DEFAULT NULL,
					  `semester_id` int(11) DEFAULT NULL,
					  `course_id` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `assignsubjects` */

					/*Table structure for table `attendance_files` */

					DROP TABLE IF EXISTS `attendance_files`;

					CREATE TABLE `attendance_files` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `subject_id` int(11) DEFAULT NULL,
					  `date` datetime DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `employee_id` int(11) NOT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `attendance_files` */

					/*Table structure for table `attendances` */

					DROP TABLE IF EXISTS `attendances`;

					CREATE TABLE `attendances` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `enrollment_id` int(11) DEFAULT NULL,
					  `reason` varchar(255) DEFAULT NULL,
					  `attendance_file_id` int(11) DEFAULT NULL,
					  `employee_id` int(11) NOT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `attendances` */

					/*Table structure for table `block_subjects` */

					DROP TABLE IF EXISTS `block_subjects`;

					CREATE TABLE `block_subjects` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `subject_id` int(11) DEFAULT NULL,
					  `block_system_setting_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

					/*Data for the table `block_subjects` */

					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (1,2,1,'2014-01-14 16:32:34','2014-01-14 16:32:34');
					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (2,3,1,'2014-01-14 16:32:34','2014-01-14 16:32:34');
					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (3,4,1,'2014-01-14 16:32:34','2014-01-14 16:32:34');
					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (4,5,1,'2014-01-14 16:32:34','2014-01-14 16:32:34');
					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (5,6,1,'2014-01-14 16:32:34','2014-01-14 16:32:34');
					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (6,7,1,'2014-01-14 16:32:34','2014-01-14 16:32:34');
					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (7,8,1,'2014-01-14 16:32:34','2014-01-14 16:32:34');
					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (8,9,1,'2014-01-14 16:32:34','2014-01-14 16:32:34');
					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (9,10,1,'2014-01-14 16:32:34','2014-01-14 16:32:34');
					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (10,11,1,'2014-01-14 16:32:34','2014-01-14 16:32:34');
					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (11,2,2,'2014-06-05 17:54:04','2014-06-05 17:54:04');
					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (12,3,2,'2014-06-05 17:54:04','2014-06-05 17:54:04');
					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (13,4,2,'2014-06-05 17:54:04','2014-06-05 17:54:04');
					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (14,5,2,'2014-06-05 17:54:04','2014-06-05 17:54:04');
					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (15,6,2,'2014-06-05 17:54:04','2014-06-05 17:54:04');
					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (16,7,2,'2014-06-05 17:54:04','2014-06-05 17:54:04');
					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (17,8,2,'2014-06-05 17:54:04','2014-06-05 17:54:04');
					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (18,9,2,'2014-06-05 17:54:04','2014-06-05 17:54:04');
					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (19,10,2,'2014-06-05 17:54:04','2014-06-05 17:54:04');
					insert  into `block_subjects`(`id`,`subject_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (20,11,2,'2014-06-05 17:54:04','2014-06-05 17:54:04');

					/*Table structure for table `block_system_settings` */

					DROP TABLE IF EXISTS `block_system_settings`;

					CREATE TABLE `block_system_settings` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `name` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `year_id` int(11) DEFAULT NULL,
					  `semester_id` int(11) DEFAULT NULL,
					  `course_id` int(11) DEFAULT NULL,
					  `academic_year_id` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

					/*Data for the table `block_system_settings` */

					insert  into `block_system_settings`(`id`,`name`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`academic_year_id`) values (1,'Section LAA','2014-01-14 16:32:34','2014-01-14 18:14:08',NULL,1,NULL,1);
					insert  into `block_system_settings`(`id`,`name`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`academic_year_id`) values (2,'NLAA','2014-06-05 17:54:04','2014-06-05 17:54:04',NULL,1,NULL,2);

					/*Table structure for table `borrowers` */

					DROP TABLE IF EXISTS `borrowers`;

					CREATE TABLE `borrowers` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `users_id` int(11) DEFAULT NULL,
					  `date_due` datetime DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `librarycard_id` int(11) DEFAULT NULL,
					  `date_return` datetime DEFAULT NULL,
					  `date_borrowed` datetime DEFAULT NULL,
					  `date_to_be_returned` date DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `borrowers` */

					/*Table structure for table `brands` */

					DROP TABLE IF EXISTS `brands`;

					CREATE TABLE `brands` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `ext` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

					/*Data for the table `brands` */

					/*Table structure for table `captcha` */

					DROP TABLE IF EXISTS `captcha`;

					CREATE TABLE `captcha` (
					  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
					  `captcha_time` int(10) unsigned NOT NULL,
					  `ip_address` varchar(16) NOT NULL DEFAULT '0',
					  `word` varchar(20) NOT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`captcha_id`),
					  KEY `word` (`word`)
					) ENGINE=InnoDB AUTO_INCREMENT=1128 DEFAULT CHARSET=latin1;

					/*Data for the table `captcha` */

					insert  into `captcha`(`captcha_id`,`captcha_time`,`ip_address`,`word`,`created_at`,`updated_at`) values (1114,1404955224,'127.0.0.1','AQXtJ','2014-07-10 09:20:23','2014-07-10 09:20:23');
					insert  into `captcha`(`captcha_id`,`captcha_time`,`ip_address`,`word`,`created_at`,`updated_at`) values (1115,1404957035,'127.0.0.1','DVu','2014-07-10 09:50:35','2014-07-10 09:50:35');
					insert  into `captcha`(`captcha_id`,`captcha_time`,`ip_address`,`word`,`created_at`,`updated_at`) values (1116,1404958303,'127.0.0.1','vhC','2014-07-10 10:11:42','2014-07-10 10:11:42');
					insert  into `captcha`(`captcha_id`,`captcha_time`,`ip_address`,`word`,`created_at`,`updated_at`) values (1117,1404958305,'127.0.0.1','xGE','2014-07-10 10:11:45','2014-07-10 10:11:45');
					insert  into `captcha`(`captcha_id`,`captcha_time`,`ip_address`,`word`,`created_at`,`updated_at`) values (1118,1404958320,'127.0.0.1','qz','2014-07-10 10:12:00','2014-07-10 10:12:00');
					insert  into `captcha`(`captcha_id`,`captcha_time`,`ip_address`,`word`,`created_at`,`updated_at`) values (1119,1404958347,'127.0.0.1','qp','2014-07-10 10:12:27','2014-07-10 10:12:27');
					insert  into `captcha`(`captcha_id`,`captcha_time`,`ip_address`,`word`,`created_at`,`updated_at`) values (1120,1404959445,'127.0.0.1','6NZ','2014-07-10 10:30:45','2014-07-10 10:30:45');
					insert  into `captcha`(`captcha_id`,`captcha_time`,`ip_address`,`word`,`created_at`,`updated_at`) values (1121,1404959453,'127.0.0.1','WA','2014-07-10 10:30:53','2014-07-10 10:30:53');
					insert  into `captcha`(`captcha_id`,`captcha_time`,`ip_address`,`word`,`created_at`,`updated_at`) values (1122,1404959578,'127.0.0.1','fhcS6','2014-07-10 10:32:58','2014-07-10 10:32:58');
					insert  into `captcha`(`captcha_id`,`captcha_time`,`ip_address`,`word`,`created_at`,`updated_at`) values (1123,1404959587,'127.0.0.1','Ud3h7','2014-07-10 10:33:07','2014-07-10 10:33:07');
					insert  into `captcha`(`captcha_id`,`captcha_time`,`ip_address`,`word`,`created_at`,`updated_at`) values (1124,1404959680,'127.0.0.1','4e2GH','2014-07-10 10:34:40','2014-07-10 10:34:40');
					insert  into `captcha`(`captcha_id`,`captcha_time`,`ip_address`,`word`,`created_at`,`updated_at`) values (1125,1404959718,'127.0.0.1','Jk','2014-07-10 10:35:18','2014-07-10 10:35:18');
					insert  into `captcha`(`captcha_id`,`captcha_time`,`ip_address`,`word`,`created_at`,`updated_at`) values (1126,1404959808,'127.0.0.1','7Y1A3','2014-07-10 10:36:48','2014-07-10 10:36:48');
					insert  into `captcha`(`captcha_id`,`captcha_time`,`ip_address`,`word`,`created_at`,`updated_at`) values (1127,1404959816,'127.0.0.1','ytR','2014-07-10 10:36:56','2014-07-10 10:36:56');

					/*Table structure for table `career_services` */

					DROP TABLE IF EXISTS `career_services`;

					CREATE TABLE `career_services` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `career` varchar(255) DEFAULT NULL,
					  `rating` varchar(255) DEFAULT NULL,
					  `date_of_examination` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `employee_id` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

					/*Data for the table `career_services` */

					insert  into `career_services`(`id`,`career`,`rating`,`date_of_examination`,`created_at`,`updated_at`,`employee_id`) values (1,'reg','reg','reg','2014-01-22 15:24:13','2014-01-22 15:24:13',3);
					insert  into `career_services`(`id`,`career`,`rating`,`date_of_examination`,`created_at`,`updated_at`,`employee_id`) values (2,'reg','reg','reg','2014-01-22 15:24:13','2014-01-22 15:24:13',3);
					insert  into `career_services`(`id`,`career`,`rating`,`date_of_examination`,`created_at`,`updated_at`,`employee_id`) values (3,'reg','reg','reg','2014-01-22 15:24:13','2014-01-22 15:24:13',3);
					insert  into `career_services`(`id`,`career`,`rating`,`date_of_examination`,`created_at`,`updated_at`,`employee_id`) values (4,'reg','reg','reg','2014-01-22 15:24:13','2014-01-22 15:24:13',3);
					insert  into `career_services`(`id`,`career`,`rating`,`date_of_examination`,`created_at`,`updated_at`,`employee_id`) values (5,'reg','reg','reg','2014-01-22 15:24:13','2014-01-22 15:24:13',3);

					/*Table structure for table `carrer_services` */

					DROP TABLE IF EXISTS `carrer_services`;

					CREATE TABLE `carrer_services` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `carrer` varchar(255) DEFAULT NULL,
					  `rating` varchar(255) DEFAULT NULL,
					  `date_of_examination` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `carrer_services` */

					/*Table structure for table `categories` */

					DROP TABLE IF EXISTS `categories`;

					CREATE TABLE `categories` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `name` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `categories` */

					/*Table structure for table `categoryfinances` */

					DROP TABLE IF EXISTS `categoryfinances`;

					CREATE TABLE `categoryfinances` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `employeecategory_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `categoryfinances` */

					/*Table structure for table `certificate_of_enrollment_settings` */

					DROP TABLE IF EXISTS `certificate_of_enrollment_settings`;

					CREATE TABLE `certificate_of_enrollment_settings` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `first_paragraph` text,
					  `second_paragraph` text,
					  `signatory` varchar(255) DEFAULT NULL,
					  `signatory_position` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `third_paragraph` text,
					  `header` tinyint(1) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `certificate_of_enrollment_settings` */

					/*Table structure for table `ched_files` */

					DROP TABLE IF EXISTS `ched_files`;

					CREATE TABLE `ched_files` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `year_id` int(11) DEFAULT NULL,
					  `semester_id` int(11) DEFAULT NULL,
					  `course_id` int(11) DEFAULT NULL,
					  `enrollment_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `ched_files` */

					/*Table structure for table `cheds` */

					DROP TABLE IF EXISTS `cheds`;

					CREATE TABLE `cheds` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `ched_file_id` int(11) DEFAULT NULL,
					  `subject_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `enrollment_id` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `cheds` */

					/*Table structure for table `conversation` */

					DROP TABLE IF EXISTS `conversation`;

					CREATE TABLE `conversation` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `user1_id` bigint(20) DEFAULT NULL,
					  `user2_id` bigint(20) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `deleted` smallint(1) DEFAULT '0',
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `conversation` */

					/*Table structure for table `course_blocks` */

					DROP TABLE IF EXISTS `course_blocks`;

					CREATE TABLE `course_blocks` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `year_id` int(11) DEFAULT NULL,
					  `course_id` int(11) DEFAULT NULL,
					  `semester_id` int(11) DEFAULT NULL,
					  `academic_year_id` int(11) DEFAULT NULL,
					  `block_system_setting_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

					/*Data for the table `course_blocks` */

					insert  into `course_blocks`(`id`,`year_id`,`course_id`,`semester_id`,`academic_year_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (1,3,1,1,1,1,'2014-01-14 18:13:47','2014-01-14 18:13:47');
					insert  into `course_blocks`(`id`,`year_id`,`course_id`,`semester_id`,`academic_year_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (2,3,1,1,1,1,'2014-04-28 09:42:18','2014-04-28 09:42:18');
					insert  into `course_blocks`(`id`,`year_id`,`course_id`,`semester_id`,`academic_year_id`,`block_system_setting_id`,`created_at`,`updated_at`) values (3,3,1,1,2,2,'2014-06-05 17:55:27','2014-06-05 17:55:27');

					/*Table structure for table `coursefees` */

					DROP TABLE IF EXISTS `coursefees`;

					CREATE TABLE `coursefees` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `value` int(11) DEFAULT NULL,
					  `coursefinance_id` int(11) DEFAULT NULL,
					  `fee_id` int(11) DEFAULT NULL,
					  `position` int(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

					/*Data for the table `coursefees` */

					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (23,459,3,1,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (24,326,3,18,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (25,346,3,2,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (26,335,3,3,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (27,510,3,4,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (28,712,3,5,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (29,189,3,6,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (30,189,3,7,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (31,14,3,8,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (32,189,3,9,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (33,685,3,10,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (34,245,3,11,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (35,100,3,12,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (36,1027,3,13,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (37,90,3,14,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (38,191,3,15,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (39,292,3,16,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (40,113,3,17,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (41,200,3,20,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (42,92,3,21,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (43,72,3,22,NULL,'2014-06-20 17:54:52','2014-06-20 17:54:52');
					insert  into `coursefees`(`id`,`value`,`coursefinance_id`,`fee_id`,`position`,`created_at`,`updated_at`) values (45,200,3,24,NULL,'2014-06-26 18:26:53','2014-06-26 18:26:53');

					/*Table structure for table `coursefinances` */

					DROP TABLE IF EXISTS `coursefinances`;

					CREATE TABLE `coursefinances` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `course_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `fees` varchar(255) DEFAULT NULL,
					  `year_id` int(11) DEFAULT NULL,
					  `category` varchar(255) DEFAULT NULL,
					  `category2` varchar(255) NOT NULL,
					  `code` varchar(20) DEFAULT NULL,
					  `semester_id` int(11) DEFAULT NULL,
					  `academic_year_id` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

					/*Data for the table `coursefinances` */

					insert  into `coursefinances`(`id`,`course_id`,`created_at`,`updated_at`,`fees`,`year_id`,`category`,`category2`,`code`,`semester_id`,`academic_year_id`) values (3,NULL,'2014-06-20 17:54:52','2014-06-21 14:54:05',NULL,NULL,'BLMS','','BLMS',1,2);

					/*Table structure for table `courses` */

					DROP TABLE IF EXISTS `courses`;

					CREATE TABLE `courses` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `course_code` varchar(255) DEFAULT NULL,
					  `course` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `new_finance_category` int(11) DEFAULT NULL,
					  `old_finance_category` int(11) DEFAULT NULL,
					  `is_smaw` int(11) NOT NULL DEFAULT '0',
					  `is_tesda_course` int(11) NOT NULL DEFAULT '0',
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

					/*Data for the table `courses` */

					insert  into `courses`(`id`,`course_code`,`course`,`created_at`,`updated_at`,`new_finance_category`,`old_finance_category`,`is_smaw`,`is_tesda_course`) values (1,'BMLS','BACHELOR OF MEDICAL LABORATORY SCIENCE','2014-01-14 10:59:33','2014-07-02 10:54:22',0,0,0,0);
					insert  into `courses`(`id`,`course_code`,`course`,`created_at`,`updated_at`,`new_finance_category`,`old_finance_category`,`is_smaw`,`is_tesda_course`) values (2,'BSIT','Bachelor of Information Technology','2014-07-01 18:03:48','2014-07-01 18:03:48',NULL,NULL,0,0);
					insert  into `courses`(`id`,`course_code`,`course`,`created_at`,`updated_at`,`new_finance_category`,`old_finance_category`,`is_smaw`,`is_tesda_course`) values (4,'BSN','Bachelor of Nursing','2014-07-01 18:08:51','2014-07-01 18:08:51',NULL,NULL,0,0);
					insert  into `courses`(`id`,`course_code`,`course`,`created_at`,`updated_at`,`new_finance_category`,`old_finance_category`,`is_smaw`,`is_tesda_course`) values (5,'COA','College of Accounttancy','2014-07-01 18:09:00','2014-07-01 18:09:00',NULL,NULL,0,0);
					insert  into `courses`(`id`,`course_code`,`course`,`created_at`,`updated_at`,`new_finance_category`,`old_finance_category`,`is_smaw`,`is_tesda_course`) values (6,'COE','College of Engineering','2014-07-01 18:16:30','2014-07-01 18:16:30',NULL,NULL,0,0);

					/*Table structure for table `credits` */

					DROP TABLE IF EXISTS `credits`;

					CREATE TABLE `credits` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `summary_of_credit_id` int(11) DEFAULT NULL,
					  `subject` varchar(255) DEFAULT NULL,
					  `value` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `credits` */

					/*Table structure for table `curriculum` */

					DROP TABLE IF EXISTS `curriculum`;

					CREATE TABLE `curriculum` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `name` varchar(255) NOT NULL,
					  `course_id` bigint(20) NOT NULL,
					  `academic_year_id` bigint(20) NOT NULL,
					  `sy_from` int(11) NOT NULL,
					  `sy_to` int(11) NOT NULL,
					  `units` float DEFAULT NULL,
					  `lec` float DEFAULT NULL,
					  `lab` float DEFAULT NULL,
					  `fee_amount` float DEFAULT NULL,
					  `is_deleted` tinyint(1) DEFAULT '0',
					  `deleted_by` varchar(100) DEFAULT NULL,
					  `deleted_date` datetime DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

					/*Data for the table `curriculum` */

					insert  into `curriculum`(`id`,`name`,`course_id`,`academic_year_id`,`sy_from`,`sy_to`,`units`,`lec`,`lab`,`fee_amount`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (1,'asdfasd',1,2,2014,2015,NULL,NULL,NULL,NULL,0,'1','2014-07-02 11:08:18','2014-06-30 17:26:31','2014-07-02 11:08:18');
					insert  into `curriculum`(`id`,`name`,`course_id`,`academic_year_id`,`sy_from`,`sy_to`,`units`,`lec`,`lab`,`fee_amount`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (2,'BIT',2,2,2014,2015,NULL,NULL,NULL,NULL,1,'1','2014-07-02 11:42:37','2014-07-02 11:09:04','2014-07-02 11:42:37');
					insert  into `curriculum`(`id`,`name`,`course_id`,`academic_year_id`,`sy_from`,`sy_to`,`units`,`lec`,`lab`,`fee_amount`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (3,'BSIT-A',2,2,2014,2015,NULL,NULL,NULL,NULL,1,'1','2014-07-02 11:42:37','2014-07-02 11:22:25','2014-07-02 11:42:37');
					insert  into `curriculum`(`id`,`name`,`course_id`,`academic_year_id`,`sy_from`,`sy_to`,`units`,`lec`,`lab`,`fee_amount`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (4,'BSIT',2,2,2014,2015,NULL,NULL,NULL,NULL,1,'1','2014-07-02 11:42:37','2014-07-02 11:24:58','2014-07-02 11:42:37');
					insert  into `curriculum`(`id`,`name`,`course_id`,`academic_year_id`,`sy_from`,`sy_to`,`units`,`lec`,`lab`,`fee_amount`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (5,'BSIT',2,2,2014,2015,NULL,NULL,NULL,NULL,1,'1','2014-07-02 11:42:37','2014-07-02 11:26:12','2014-07-02 11:42:37');
					insert  into `curriculum`(`id`,`name`,`course_id`,`academic_year_id`,`sy_from`,`sy_to`,`units`,`lec`,`lab`,`fee_amount`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (6,'asdfasdf',2,2,2014,2015,NULL,NULL,NULL,NULL,0,'1','2014-07-02 11:42:37','2014-07-02 11:31:12','2014-07-02 11:42:37');
					insert  into `curriculum`(`id`,`name`,`course_id`,`academic_year_id`,`sy_from`,`sy_to`,`units`,`lec`,`lab`,`fee_amount`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (7,'COE',6,2,2014,2015,NULL,NULL,NULL,NULL,0,NULL,NULL,'2014-07-02 11:43:06','2014-07-02 11:43:06');
					insert  into `curriculum`(`id`,`name`,`course_id`,`academic_year_id`,`sy_from`,`sy_to`,`units`,`lec`,`lab`,`fee_amount`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (8,'COA',5,2,2014,2015,NULL,NULL,NULL,NULL,0,NULL,NULL,'2014-07-02 17:38:32','2014-07-02 17:38:32');

					/*Table structure for table `curriculum_subjects` */

					DROP TABLE IF EXISTS `curriculum_subjects`;

					CREATE TABLE `curriculum_subjects` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `curriculum_id` bigint(20) NOT NULL COMMENT 'Link to curriculum Table',
					  `subject_id` bigint(20) NOT NULL COMMENT 'Link to subjects table',
					  `subject_refid` varchar(25) DEFAULT NULL,
					  `req_year_id` varchar(100) DEFAULT NULL COMMENT 'Student year Eq 1st Year 2nd Year',
					  `year_id` bigint(20) DEFAULT NULL,
					  `semester_id` varchar(100) NOT NULL COMMENT 'Semester Eg 1st 2nd semester',
					  `total_units_req` float DEFAULT NULL COMMENT 'Required Finish Units of Students',
					  `enrollment_units_req` float DEFAULT NULL COMMENT 'Required Additional Subjects (Units)',
					  `is_deleted` tinyint(1) DEFAULT '0' COMMENT 'if deleted value is 1',
					  `deleted_by` varchar(100) DEFAULT NULL,
					  `deleted_date` datetime DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;

					/*Data for the table `curriculum_subjects` */

					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (1,1,2,NULL,NULL,3,'1',NULL,NULL,0,NULL,NULL,'2014-07-01 15:06:28','2014-07-01 15:06:28');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (2,1,6,NULL,'3',3,'1',NULL,NULL,0,NULL,NULL,'2014-07-01 17:38:20','2014-07-01 17:38:20');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (3,1,6,NULL,'3',3,'1',NULL,NULL,0,NULL,NULL,'2014-07-01 17:39:00','2014-07-01 17:39:00');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (4,6,2,NULL,NULL,3,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 11:33:27','2014-07-02 11:33:27');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (5,6,3,NULL,NULL,3,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 11:33:27','2014-07-02 11:33:27');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (6,6,4,NULL,NULL,3,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 11:33:27','2014-07-02 11:33:27');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (7,6,10,NULL,NULL,3,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 11:33:27','2014-07-02 11:33:27');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (8,6,7,NULL,NULL,3,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 11:33:27','2014-07-02 11:33:27');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (9,8,2,NULL,NULL,3,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:08','2014-07-02 17:39:08');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (10,8,3,NULL,NULL,3,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:08','2014-07-02 17:39:08');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (11,8,4,NULL,NULL,3,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:08','2014-07-02 17:39:08');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (12,8,10,NULL,NULL,3,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:08','2014-07-02 17:39:08');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (13,8,7,NULL,NULL,3,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:09','2014-07-02 17:39:09');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (14,8,8,NULL,NULL,3,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:09','2014-07-02 17:39:09');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (15,8,9,NULL,NULL,3,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:09','2014-07-02 17:39:09');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (16,8,5,NULL,NULL,3,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:09','2014-07-02 17:39:09');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (17,8,6,NULL,NULL,3,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:09','2014-07-02 17:39:09');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (18,8,11,NULL,NULL,3,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:09','2014-07-02 17:39:09');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (19,8,2,NULL,NULL,3,'2',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:28','2014-07-02 17:39:28');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (20,8,3,NULL,NULL,3,'2',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:28','2014-07-02 17:39:28');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (21,8,4,NULL,NULL,3,'2',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:29','2014-07-02 17:39:29');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (22,8,10,NULL,NULL,3,'2',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:29','2014-07-02 17:39:29');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (23,8,7,NULL,NULL,3,'2',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:29','2014-07-02 17:39:29');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (24,8,2,NULL,NULL,3,'3',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:42','2014-07-02 17:39:42');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (25,8,3,NULL,NULL,3,'3',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:42','2014-07-02 17:39:42');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (26,8,4,NULL,NULL,3,'3',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:42','2014-07-02 17:39:42');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (27,8,5,NULL,NULL,3,'3',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:43','2014-07-02 17:39:43');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (28,8,6,NULL,NULL,3,'3',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:43','2014-07-02 17:39:43');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (29,8,11,NULL,NULL,3,'3',NULL,NULL,0,NULL,NULL,'2014-07-02 17:39:43','2014-07-02 17:39:43');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (30,8,2,NULL,NULL,4,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:42:39','2014-07-02 17:42:39');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (31,8,3,NULL,NULL,4,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:42:39','2014-07-02 17:42:39');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (32,8,10,NULL,NULL,4,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:42:39','2014-07-02 17:42:39');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (33,8,7,NULL,NULL,4,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:42:39','2014-07-02 17:42:39');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (34,8,5,NULL,NULL,4,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:42:39','2014-07-02 17:42:39');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (35,8,6,NULL,NULL,4,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:42:39','2014-07-02 17:42:39');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (36,8,11,NULL,NULL,4,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:42:39','2014-07-02 17:42:39');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (37,8,2,NULL,NULL,4,'2',NULL,NULL,0,NULL,NULL,'2014-07-02 17:42:55','2014-07-02 17:42:55');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (38,8,3,NULL,NULL,4,'2',NULL,NULL,0,NULL,NULL,'2014-07-02 17:42:55','2014-07-02 17:42:55');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (39,8,4,NULL,NULL,4,'2',NULL,NULL,0,NULL,NULL,'2014-07-02 17:42:55','2014-07-02 17:42:55');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (40,8,10,NULL,NULL,4,'2',NULL,NULL,0,NULL,NULL,'2014-07-02 17:42:55','2014-07-02 17:42:55');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (41,8,7,NULL,NULL,4,'2',NULL,NULL,0,NULL,NULL,'2014-07-02 17:42:55','2014-07-02 17:42:55');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (42,8,5,NULL,NULL,4,'2',NULL,NULL,0,NULL,NULL,'2014-07-02 17:42:55','2014-07-02 17:42:55');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (43,8,6,NULL,NULL,4,'2',NULL,NULL,0,NULL,NULL,'2014-07-02 17:42:55','2014-07-02 17:42:55');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (44,8,11,NULL,NULL,4,'2',NULL,NULL,0,NULL,NULL,'2014-07-02 17:42:55','2014-07-02 17:42:55');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (45,8,2,NULL,NULL,4,'3',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:09','2014-07-02 17:43:09');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (46,8,3,NULL,NULL,4,'3',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:09','2014-07-02 17:43:09');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (47,8,10,NULL,NULL,4,'3',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:09','2014-07-02 17:43:09');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (48,8,2,NULL,NULL,5,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:23','2014-07-02 17:43:23');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (49,8,3,NULL,NULL,5,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:23','2014-07-02 17:43:23');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (50,8,4,NULL,NULL,5,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:23','2014-07-02 17:43:23');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (51,8,10,NULL,NULL,5,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:23','2014-07-02 17:43:23');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (52,8,7,NULL,NULL,5,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:23','2014-07-02 17:43:23');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (53,8,5,NULL,NULL,5,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:24','2014-07-02 17:43:24');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (54,8,6,NULL,NULL,5,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:24','2014-07-02 17:43:24');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (55,8,11,NULL,NULL,5,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:24','2014-07-02 17:43:24');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (56,8,2,NULL,NULL,5,'3',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:41','2014-07-02 17:43:41');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (57,8,3,NULL,NULL,5,'3',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:41','2014-07-02 17:43:41');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (58,8,10,NULL,NULL,5,'3',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:41','2014-07-02 17:43:41');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (59,8,2,NULL,NULL,6,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:54','2014-07-02 17:43:54');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (60,8,8,NULL,NULL,6,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:54','2014-07-02 17:43:54');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (61,8,9,NULL,NULL,6,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:54','2014-07-02 17:43:54');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (62,8,5,NULL,NULL,6,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:55','2014-07-02 17:43:55');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (63,8,6,NULL,NULL,6,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:55','2014-07-02 17:43:55');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (64,8,11,NULL,NULL,6,'1',NULL,NULL,0,NULL,NULL,'2014-07-02 17:43:55','2014-07-02 17:43:55');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (65,8,10,NULL,NULL,6,'2',NULL,NULL,0,NULL,NULL,'2014-07-02 17:44:06','2014-07-02 17:44:06');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (66,8,8,NULL,NULL,6,'2',NULL,NULL,0,NULL,NULL,'2014-07-02 17:44:06','2014-07-02 17:44:06');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (67,8,9,NULL,NULL,6,'2',NULL,NULL,0,NULL,NULL,'2014-07-02 17:44:07','2014-07-02 17:44:07');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (68,8,5,NULL,NULL,6,'2',NULL,NULL,0,NULL,NULL,'2014-07-02 17:44:07','2014-07-02 17:44:07');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (69,8,6,NULL,NULL,6,'2',NULL,NULL,0,NULL,NULL,'2014-07-02 17:44:07','2014-07-02 17:44:07');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (70,8,11,NULL,NULL,6,'2',NULL,NULL,0,NULL,NULL,'2014-07-02 17:44:07','2014-07-02 17:44:07');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (71,8,2,NULL,NULL,6,'3',NULL,NULL,0,NULL,NULL,'2014-07-02 17:44:13','2014-07-02 17:44:13');
					insert  into `curriculum_subjects`(`id`,`curriculum_id`,`subject_id`,`subject_refid`,`req_year_id`,`year_id`,`semester_id`,`total_units_req`,`enrollment_units_req`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (72,8,3,NULL,NULL,6,'3',NULL,NULL,0,NULL,NULL,'2014-07-02 17:44:14','2014-07-02 17:44:14');

					/*Table structure for table `delayed_jobs` */

					DROP TABLE IF EXISTS `delayed_jobs`;

					CREATE TABLE `delayed_jobs` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `priority` int(11) DEFAULT '0',
					  `attempts` int(11) DEFAULT '0',
					  `handler` text,
					  `last_error` text,
					  `run_at` datetime DEFAULT NULL,
					  `locked_at` datetime DEFAULT NULL,
					  `failed_at` datetime DEFAULT NULL,
					  `locked_by` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`),
					  KEY `delayed_jobs_priority` (`priority`,`run_at`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `delayed_jobs` */

					/*Table structure for table `deleted_students` */

					DROP TABLE IF EXISTS `deleted_students`;

					CREATE TABLE `deleted_students` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `name` varchar(255) DEFAULT NULL,
					  `studid` varchar(255) DEFAULT NULL,
					  `year` varchar(255) DEFAULT NULL,
					  `semester` varchar(255) DEFAULT NULL,
					  `course` varchar(255) DEFAULT NULL,
					  `academic_year` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `deleted_students` */

					/*Table structure for table `demands` */

					DROP TABLE IF EXISTS `demands`;

					CREATE TABLE `demands` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `borrow_no` varchar(50) DEFAULT NULL,
					  `date` date DEFAULT NULL,
					  `user_id` int(11) DEFAULT NULL,
					  `item_id` int(11) DEFAULT NULL,
					  `unit` int(11) DEFAULT NULL,
					  `unit_return` int(11) DEFAULT '0',
					  `unit_lost_return` int(11) DEFAULT '0',
					  `unit_unreturn` int(11) DEFAULT NULL,
					  `price` int(11) DEFAULT NULL,
					  `amount` int(11) DEFAULT NULL,
					  `status` varchar(50) DEFAULT NULL COMMENT 'STATUS',
					  `deducted` int(11) DEFAULT '1',
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `demands` */

					/*Table structure for table `demands_return` */

					DROP TABLE IF EXISTS `demands_return`;

					CREATE TABLE `demands_return` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `demands_id` bigint(20) DEFAULT NULL,
					  `unit_return` int(11) DEFAULT NULL,
					  `date_return` datetime DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `remarks` varchar(255) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `demands_return` */

					/*Table structure for table `demands_return_lost` */

					DROP TABLE IF EXISTS `demands_return_lost`;

					CREATE TABLE `demands_return_lost` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `demands_id` bigint(20) DEFAULT NULL,
					  `unit_return` int(11) DEFAULT NULL,
					  `date_return` datetime DEFAULT NULL,
					  `category` varchar(100) DEFAULT NULL,
					  `remarks` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `demands_return_lost` */

					/*Table structure for table `departments` */

					DROP TABLE IF EXISTS `departments`;

					CREATE TABLE `departments` (
					  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
					  `department` varchar(100) DEFAULT NULL,
					  `description` varchar(100) DEFAULT NULL,
					  `visible` int(1) DEFAULT '1',
					  `ord` int(11) DEFAULT '1',
					  `controller` varchar(100) DEFAULT NULL,
					  `icon` varchar(100) DEFAULT NULL,
					  `level` smallint(2) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `stud_profile` tinyint(1) DEFAULT '0',
					  `stud_fees` tinyint(1) DEFAULT '0',
					  `stud_grade` tinyint(1) DEFAULT '0',
					  `stud_issues` tinyint(1) DEFAULT '0',
					  `stud_otr` tinyint(1) DEFAULT '0',
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

					/*Data for the table `departments` */

					insert  into `departments`(`id`,`department`,`description`,`visible`,`ord`,`controller`,`icon`,`level`,`created_at`,`updated_at`,`stud_profile`,`stud_fees`,`stud_grade`,`stud_issues`,`stud_otr`) values (1,'president','President',1,1,'login','assets/images/president.png',1,NULL,NULL,0,0,0,0,0);
					insert  into `departments`(`id`,`department`,`description`,`visible`,`ord`,`controller`,`icon`,`level`,`created_at`,`updated_at`,`stud_profile`,`stud_fees`,`stud_grade`,`stud_issues`,`stud_otr`) values (2,'admin','System Administrator',1,3,'login','assets/images/administrator.png',1,NULL,'2014-07-01 11:48:53',1,1,1,1,1);
					insert  into `departments`(`id`,`department`,`description`,`visible`,`ord`,`controller`,`icon`,`level`,`created_at`,`updated_at`,`stud_profile`,`stud_fees`,`stud_grade`,`stud_issues`,`stud_otr`) values (3,'dean','Dean',1,4,'login','assets/images/dean.png',4,NULL,NULL,0,0,0,0,0);
					insert  into `departments`(`id`,`department`,`description`,`visible`,`ord`,`controller`,`icon`,`level`,`created_at`,`updated_at`,`stud_profile`,`stud_fees`,`stud_grade`,`stud_issues`,`stud_otr`) values (4,'school_administrator','School Administrator',1,2,'login','assets/images/president.png',1,NULL,NULL,0,0,0,0,0);
					insert  into `departments`(`id`,`department`,`description`,`visible`,`ord`,`controller`,`icon`,`level`,`created_at`,`updated_at`,`stud_profile`,`stud_fees`,`stud_grade`,`stud_issues`,`stud_otr`) values (5,'registrar','Registar',1,5,'login','assets/images/registrar.png',4,NULL,'2014-06-21 16:02:38',1,0,1,1,1);
					insert  into `departments`(`id`,`department`,`description`,`visible`,`ord`,`controller`,`icon`,`level`,`created_at`,`updated_at`,`stud_profile`,`stud_fees`,`stud_grade`,`stud_issues`,`stud_otr`) values (6,'finance','Finance',1,6,'login','assets/images/financer.png',4,NULL,'2014-06-21 16:24:26',1,1,0,1,0);
					insert  into `departments`(`id`,`department`,`description`,`visible`,`ord`,`controller`,`icon`,`level`,`created_at`,`updated_at`,`stud_profile`,`stud_fees`,`stud_grade`,`stud_issues`,`stud_otr`) values (7,'hrd','Human Resource',0,7,'login','assets/images/hrd.png',4,NULL,'2014-06-21 16:04:03',0,0,0,0,0);
					insert  into `departments`(`id`,`department`,`description`,`visible`,`ord`,`controller`,`icon`,`level`,`created_at`,`updated_at`,`stud_profile`,`stud_fees`,`stud_grade`,`stud_issues`,`stud_otr`) values (8,'teacher','Teacher',1,8,'login','assets/images/teacher.png',4,NULL,NULL,0,0,0,0,0);
					insert  into `departments`(`id`,`department`,`description`,`visible`,`ord`,`controller`,`icon`,`level`,`created_at`,`updated_at`,`stud_profile`,`stud_fees`,`stud_grade`,`stud_issues`,`stud_otr`) values (9,'librarian','Library',1,9,'login','assets/images/library.png',4,NULL,NULL,0,0,0,0,0);
					insert  into `departments`(`id`,`department`,`description`,`visible`,`ord`,`controller`,`icon`,`level`,`created_at`,`updated_at`,`stud_profile`,`stud_fees`,`stud_grade`,`stud_issues`,`stud_otr`) values (10,'custodian','Custodian',0,10,'login','assets/images/custodian.jpg',4,NULL,'2014-06-26 13:57:19',0,0,0,0,0);
					insert  into `departments`(`id`,`department`,`description`,`visible`,`ord`,`controller`,`icon`,`level`,`created_at`,`updated_at`,`stud_profile`,`stud_fees`,`stud_grade`,`stud_issues`,`stud_otr`) values (11,'cashier','Cashier',1,11,'login','assets/images/cashier.png',4,NULL,'2014-06-27 11:09:12',1,1,0,1,0);
					insert  into `departments`(`id`,`department`,`description`,`visible`,`ord`,`controller`,`icon`,`level`,`created_at`,`updated_at`,`stud_profile`,`stud_fees`,`stud_grade`,`stud_issues`,`stud_otr`) values (12,'guidance','Guidance',1,12,'login','assets/images/guidance.png',4,NULL,NULL,0,0,0,0,0);
					insert  into `departments`(`id`,`department`,`description`,`visible`,`ord`,`controller`,`icon`,`level`,`created_at`,`updated_at`,`stud_profile`,`stud_fees`,`stud_grade`,`stud_issues`,`stud_otr`) values (13,'student','Student',0,13,'login','assets/images/student.png',NULL,NULL,NULL,0,0,0,0,0);
					insert  into `departments`(`id`,`department`,`description`,`visible`,`ord`,`controller`,`icon`,`level`,`created_at`,`updated_at`,`stud_profile`,`stud_fees`,`stud_grade`,`stud_issues`,`stud_otr`) values (14,'student_affairs','Student Affairs',1,14,'login','assets/images/hrd.png',4,NULL,NULL,0,0,0,0,0);

					/*Table structure for table `employeeattendances` */

					DROP TABLE IF EXISTS `employeeattendances`;

					CREATE TABLE `employeeattendances` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `forenoon` tinyint(1) NOT NULL DEFAULT '1',
					  `afternoon` tinyint(1) NOT NULL DEFAULT '1',
					  `reason` varchar(255) DEFAULT NULL,
					  `start_date` datetime DEFAULT NULL,
					  `end_date` datetime DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `employee_id` int(11) DEFAULT NULL,
					  `year` int(11) DEFAULT NULL,
					  `month` int(11) DEFAULT NULL,
					  `day` int(11) DEFAULT NULL,
					  `timein` varchar(255) DEFAULT NULL,
					  `timeout` varchar(255) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `employeeattendances` */

					/*Table structure for table `employeecategories` */

					DROP TABLE IF EXISTS `employeecategories`;

					CREATE TABLE `employeecategories` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `category` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `employeecategories` */

					/*Table structure for table `employees` */

					DROP TABLE IF EXISTS `employees`;

					CREATE TABLE `employees` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `employeeid` varchar(255) DEFAULT NULL,
					  `first_name` varchar(255) DEFAULT NULL,
					  `middle_name` varchar(255) DEFAULT NULL,
					  `last_name` varchar(255) DEFAULT NULL,
					  `joining_date` varchar(255) DEFAULT NULL,
					  `gender` varchar(255) DEFAULT NULL,
					  `dob` varchar(255) DEFAULT NULL,
					  `employeecategory_id` int(11) DEFAULT NULL,
					  `status` tinyint(1) NOT NULL DEFAULT '1',
					  `martial_status` varchar(255) DEFAULT NULL,
					  `no_children` int(11) DEFAULT NULL,
					  `father_name` varchar(255) DEFAULT NULL,
					  `mother_name` varchar(255) DEFAULT NULL,
					  `spouse_name` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `email` varchar(255) DEFAULT NULL,
					  `name` varchar(255) DEFAULT NULL,
					  `admin` tinyint(1) DEFAULT NULL,
					  `teacher` tinyint(1) DEFAULT NULL,
					  `registrar` tinyint(1) DEFAULT NULL,
					  `finance` tinyint(1) DEFAULT NULL,
					  `role` varchar(255) DEFAULT NULL,
					  `sss_id_no` varchar(255) DEFAULT NULL,
					  `place_of_birth` varchar(255) DEFAULT NULL,
					  `citizenship` varchar(255) DEFAULT NULL,
					  `religion` varchar(255) DEFAULT NULL,
					  `pagibig_no` varchar(255) DEFAULT NULL,
					  `philhealth_no` varchar(255) DEFAULT NULL,
					  `residential_address` varchar(255) DEFAULT NULL,
					  `permanent_address` varchar(255) DEFAULT NULL,
					  `driver_license_no` varchar(255) DEFAULT NULL,
					  `tin` varchar(255) DEFAULT NULL,
					  `cellphone` varchar(255) DEFAULT NULL,
					  `spouse_last_name` varchar(255) DEFAULT NULL,
					  `spouse_first_name` varchar(255) DEFAULT NULL,
					  `spouse_middle_name` varchar(255) DEFAULT NULL,
					  `spouse_occupation` varchar(255) DEFAULT NULL,
					  `spouse_employer` varchar(255) DEFAULT NULL,
					  `spouse_business_address` varchar(255) DEFAULT NULL,
					  `spouse_telephone` varchar(255) DEFAULT NULL,
					  `child1` varchar(255) DEFAULT NULL,
					  `child1bday` varchar(255) DEFAULT NULL,
					  `child2` varchar(255) DEFAULT NULL,
					  `child2bday` varchar(255) DEFAULT NULL,
					  `child3` varchar(255) DEFAULT NULL,
					  `child3bday` varchar(255) DEFAULT NULL,
					  `child4` varchar(255) DEFAULT NULL,
					  `child4bday` varchar(255) DEFAULT NULL,
					  `child5` varchar(255) DEFAULT NULL,
					  `child5bday` varchar(255) DEFAULT NULL,
					  `child6` varchar(255) DEFAULT NULL,
					  `child6bday` varchar(255) DEFAULT NULL,
					  `child7` varchar(255) DEFAULT NULL,
					  `child7bday` varchar(255) DEFAULT NULL,
					  `father_first_name` varchar(255) DEFAULT NULL,
					  `father_middle_name` varchar(255) DEFAULT NULL,
					  `father_last_name` varchar(255) DEFAULT NULL,
					  `mother_last_name` varchar(255) DEFAULT NULL,
					  `mother_first_name` varchar(255) DEFAULT NULL,
					  `mother_middle_name` varchar(255) DEFAULT NULL,
					  `elementary_name` varchar(255) DEFAULT NULL,
					  `elementary_degree` varchar(255) DEFAULT NULL,
					  `elementary_year` varchar(255) DEFAULT NULL,
					  `secondary_name` varchar(255) DEFAULT NULL,
					  `secondary_degree` varchar(255) DEFAULT NULL,
					  `secondary_year` varchar(255) DEFAULT NULL,
					  `vocational_name` varchar(255) DEFAULT NULL,
					  `vocational_degree` varchar(255) DEFAULT NULL,
					  `vocational_year` varchar(255) DEFAULT NULL,
					  `college_name` varchar(255) DEFAULT NULL,
					  `college_degree` varchar(255) DEFAULT NULL,
					  `college_year` varchar(255) DEFAULT NULL,
					  `graduate_name` varchar(255) DEFAULT NULL,
					  `graduate_degree` varchar(255) DEFAULT NULL,
					  `graduate_year` varchar(255) DEFAULT NULL,
					  `date_from1` varchar(255) DEFAULT NULL,
					  `date_to1` varchar(255) DEFAULT NULL,
					  `agency1` varchar(255) DEFAULT NULL,
					  `comunity_tax` varchar(255) DEFAULT NULL,
					  `issued_at` varchar(255) DEFAULT NULL,
					  `issued_on` varchar(255) DEFAULT NULL,
					  `community_tax` varchar(255) DEFAULT NULL,
					  `department` varchar(255) DEFAULT NULL,
					  `employee_status` varchar(255) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

					/*Data for the table `employees` */

					insert  into `employees`(`id`,`employeeid`,`first_name`,`middle_name`,`last_name`,`joining_date`,`gender`,`dob`,`employeecategory_id`,`status`,`martial_status`,`no_children`,`father_name`,`mother_name`,`spouse_name`,`created_at`,`updated_at`,`email`,`name`,`admin`,`teacher`,`registrar`,`finance`,`role`,`sss_id_no`,`place_of_birth`,`citizenship`,`religion`,`pagibig_no`,`philhealth_no`,`residential_address`,`permanent_address`,`driver_license_no`,`tin`,`cellphone`,`spouse_last_name`,`spouse_first_name`,`spouse_middle_name`,`spouse_occupation`,`spouse_employer`,`spouse_business_address`,`spouse_telephone`,`child1`,`child1bday`,`child2`,`child2bday`,`child3`,`child3bday`,`child4`,`child4bday`,`child5`,`child5bday`,`child6`,`child6bday`,`child7`,`child7bday`,`father_first_name`,`father_middle_name`,`father_last_name`,`mother_last_name`,`mother_first_name`,`mother_middle_name`,`elementary_name`,`elementary_degree`,`elementary_year`,`secondary_name`,`secondary_degree`,`secondary_year`,`vocational_name`,`vocational_degree`,`vocational_year`,`college_name`,`college_degree`,`college_year`,`graduate_name`,`graduate_degree`,`graduate_year`,`date_from1`,`date_to1`,`agency1`,`comunity_tax`,`issued_at`,`issued_on`,`community_tax`,`department`,`employee_status`) values (1,'regisrar','Registrar','R','Registrar','2013-01-08','Female','1989-01-24',NULL,1,'Single',NULL,NULL,NULL,NULL,'2014-01-14 17:26:24','2014-01-14 17:26:24','',NULL,NULL,NULL,NULL,NULL,'registrar','','','','','','','','',NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,'','','','Registrar',NULL);
					insert  into `employees`(`id`,`employeeid`,`first_name`,`middle_name`,`last_name`,`joining_date`,`gender`,`dob`,`employeecategory_id`,`status`,`martial_status`,`no_children`,`father_name`,`mother_name`,`spouse_name`,`created_at`,`updated_at`,`email`,`name`,`admin`,`teacher`,`registrar`,`finance`,`role`,`sss_id_no`,`place_of_birth`,`citizenship`,`religion`,`pagibig_no`,`philhealth_no`,`residential_address`,`permanent_address`,`driver_license_no`,`tin`,`cellphone`,`spouse_last_name`,`spouse_first_name`,`spouse_middle_name`,`spouse_occupation`,`spouse_employer`,`spouse_business_address`,`spouse_telephone`,`child1`,`child1bday`,`child2`,`child2bday`,`child3`,`child3bday`,`child4`,`child4bday`,`child5`,`child5bday`,`child6`,`child6bday`,`child7`,`child7bday`,`father_first_name`,`father_middle_name`,`father_last_name`,`mother_last_name`,`mother_first_name`,`mother_middle_name`,`elementary_name`,`elementary_degree`,`elementary_year`,`secondary_name`,`secondary_degree`,`secondary_year`,`vocational_name`,`vocational_degree`,`vocational_year`,`college_name`,`college_degree`,`college_year`,`graduate_name`,`graduate_degree`,`graduate_year`,`date_from1`,`date_to1`,`agency1`,`comunity_tax`,`issued_at`,`issued_on`,`community_tax`,`department`,`employee_status`) values (2,'finance','Finance','F','Finance','2007-03-15','Male','1985-06-04',NULL,1,'Single',NULL,NULL,NULL,NULL,'2014-01-14 17:50:05','2014-01-14 17:50:05','',NULL,NULL,NULL,NULL,NULL,'finance','','','','','','','','',NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,'','','','Accounting',NULL);
					insert  into `employees`(`id`,`employeeid`,`first_name`,`middle_name`,`last_name`,`joining_date`,`gender`,`dob`,`employeecategory_id`,`status`,`martial_status`,`no_children`,`father_name`,`mother_name`,`spouse_name`,`created_at`,`updated_at`,`email`,`name`,`admin`,`teacher`,`registrar`,`finance`,`role`,`sss_id_no`,`place_of_birth`,`citizenship`,`religion`,`pagibig_no`,`philhealth_no`,`residential_address`,`permanent_address`,`driver_license_no`,`tin`,`cellphone`,`spouse_last_name`,`spouse_first_name`,`spouse_middle_name`,`spouse_occupation`,`spouse_employer`,`spouse_business_address`,`spouse_telephone`,`child1`,`child1bday`,`child2`,`child2bday`,`child3`,`child3bday`,`child4`,`child4bday`,`child5`,`child5bday`,`child6`,`child6bday`,`child7`,`child7bday`,`father_first_name`,`father_middle_name`,`father_last_name`,`mother_last_name`,`mother_first_name`,`mother_middle_name`,`elementary_name`,`elementary_degree`,`elementary_year`,`secondary_name`,`secondary_degree`,`secondary_year`,`vocational_name`,`vocational_degree`,`vocational_year`,`college_name`,`college_degree`,`college_year`,`graduate_name`,`graduate_degree`,`graduate_year`,`date_from1`,`date_to1`,`agency1`,`comunity_tax`,`issued_at`,`issued_on`,`community_tax`,`department`,`employee_status`) values (3,'registree','reg','reg','reg','reg','Male','reg',NULL,1,'Single',NULL,NULL,NULL,NULL,'2014-01-22 15:24:13','2014-01-22 15:24:13','reg',NULL,NULL,NULL,NULL,NULL,'registrar','reg','reg','reg','reg','reg','reg','reg','reg',NULL,'reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg','reg',NULL,NULL,NULL,NULL,'reg','reg','reg','reg',NULL);
					insert  into `employees`(`id`,`employeeid`,`first_name`,`middle_name`,`last_name`,`joining_date`,`gender`,`dob`,`employeecategory_id`,`status`,`martial_status`,`no_children`,`father_name`,`mother_name`,`spouse_name`,`created_at`,`updated_at`,`email`,`name`,`admin`,`teacher`,`registrar`,`finance`,`role`,`sss_id_no`,`place_of_birth`,`citizenship`,`religion`,`pagibig_no`,`philhealth_no`,`residential_address`,`permanent_address`,`driver_license_no`,`tin`,`cellphone`,`spouse_last_name`,`spouse_first_name`,`spouse_middle_name`,`spouse_occupation`,`spouse_employer`,`spouse_business_address`,`spouse_telephone`,`child1`,`child1bday`,`child2`,`child2bday`,`child3`,`child3bday`,`child4`,`child4bday`,`child5`,`child5bday`,`child6`,`child6bday`,`child7`,`child7bday`,`father_first_name`,`father_middle_name`,`father_last_name`,`mother_last_name`,`mother_first_name`,`mother_middle_name`,`elementary_name`,`elementary_degree`,`elementary_year`,`secondary_name`,`secondary_degree`,`secondary_year`,`vocational_name`,`vocational_degree`,`vocational_year`,`college_name`,`college_degree`,`college_year`,`graduate_name`,`graduate_degree`,`graduate_year`,`date_from1`,`date_to1`,`agency1`,`comunity_tax`,`issued_at`,`issued_on`,`community_tax`,`department`,`employee_status`) values (4,'cashier','cashier','m','cashier','2013-01-08','Female','1996-07-16',NULL,1,'Single',NULL,NULL,NULL,NULL,'2014-01-29 10:24:12','2014-01-29 13:07:01','',NULL,NULL,NULL,NULL,NULL,'cashier','','baguio city','','','','','','',NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,'','','','accounting',NULL);
					insert  into `employees`(`id`,`employeeid`,`first_name`,`middle_name`,`last_name`,`joining_date`,`gender`,`dob`,`employeecategory_id`,`status`,`martial_status`,`no_children`,`father_name`,`mother_name`,`spouse_name`,`created_at`,`updated_at`,`email`,`name`,`admin`,`teacher`,`registrar`,`finance`,`role`,`sss_id_no`,`place_of_birth`,`citizenship`,`religion`,`pagibig_no`,`philhealth_no`,`residential_address`,`permanent_address`,`driver_license_no`,`tin`,`cellphone`,`spouse_last_name`,`spouse_first_name`,`spouse_middle_name`,`spouse_occupation`,`spouse_employer`,`spouse_business_address`,`spouse_telephone`,`child1`,`child1bday`,`child2`,`child2bday`,`child3`,`child3bday`,`child4`,`child4bday`,`child5`,`child5bday`,`child6`,`child6bday`,`child7`,`child7bday`,`father_first_name`,`father_middle_name`,`father_last_name`,`mother_last_name`,`mother_first_name`,`mother_middle_name`,`elementary_name`,`elementary_degree`,`elementary_year`,`secondary_name`,`secondary_degree`,`secondary_year`,`vocational_name`,`vocational_degree`,`vocational_year`,`college_name`,`college_degree`,`college_year`,`graduate_name`,`graduate_degree`,`graduate_year`,`date_from1`,`date_to1`,`agency1`,`comunity_tax`,`issued_at`,`issued_on`,`community_tax`,`department`,`employee_status`) values (5,'studentaffairs','students','a','affairs','2013-01-01','Male','1996-01-10',NULL,1,'Single',NULL,NULL,NULL,NULL,'2014-01-29 14:02:15','2014-01-29 14:02:15','',NULL,NULL,NULL,NULL,NULL,'student_affairs','','','','','','','','',NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,'','','','admin',NULL);
					insert  into `employees`(`id`,`employeeid`,`first_name`,`middle_name`,`last_name`,`joining_date`,`gender`,`dob`,`employeecategory_id`,`status`,`martial_status`,`no_children`,`father_name`,`mother_name`,`spouse_name`,`created_at`,`updated_at`,`email`,`name`,`admin`,`teacher`,`registrar`,`finance`,`role`,`sss_id_no`,`place_of_birth`,`citizenship`,`religion`,`pagibig_no`,`philhealth_no`,`residential_address`,`permanent_address`,`driver_license_no`,`tin`,`cellphone`,`spouse_last_name`,`spouse_first_name`,`spouse_middle_name`,`spouse_occupation`,`spouse_employer`,`spouse_business_address`,`spouse_telephone`,`child1`,`child1bday`,`child2`,`child2bday`,`child3`,`child3bday`,`child4`,`child4bday`,`child5`,`child5bday`,`child6`,`child6bday`,`child7`,`child7bday`,`father_first_name`,`father_middle_name`,`father_last_name`,`mother_last_name`,`mother_first_name`,`mother_middle_name`,`elementary_name`,`elementary_degree`,`elementary_year`,`secondary_name`,`secondary_degree`,`secondary_year`,`vocational_name`,`vocational_degree`,`vocational_year`,`college_name`,`college_degree`,`college_year`,`graduate_name`,`graduate_degree`,`graduate_year`,`date_from1`,`date_to1`,`agency1`,`comunity_tax`,`issued_at`,`issued_on`,`community_tax`,`department`,`employee_status`) values (6,'finance-1','---','---','---','2014-01-08','Male','---',NULL,1,'Single',NULL,NULL,NULL,NULL,'2014-01-29 14:28:16','2014-01-29 14:28:16','',NULL,NULL,NULL,NULL,NULL,'finance','','','','','','','','',NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,'','','','Accounting',NULL);
					insert  into `employees`(`id`,`employeeid`,`first_name`,`middle_name`,`last_name`,`joining_date`,`gender`,`dob`,`employeecategory_id`,`status`,`martial_status`,`no_children`,`father_name`,`mother_name`,`spouse_name`,`created_at`,`updated_at`,`email`,`name`,`admin`,`teacher`,`registrar`,`finance`,`role`,`sss_id_no`,`place_of_birth`,`citizenship`,`religion`,`pagibig_no`,`philhealth_no`,`residential_address`,`permanent_address`,`driver_license_no`,`tin`,`cellphone`,`spouse_last_name`,`spouse_first_name`,`spouse_middle_name`,`spouse_occupation`,`spouse_employer`,`spouse_business_address`,`spouse_telephone`,`child1`,`child1bday`,`child2`,`child2bday`,`child3`,`child3bday`,`child4`,`child4bday`,`child5`,`child5bday`,`child6`,`child6bday`,`child7`,`child7bday`,`father_first_name`,`father_middle_name`,`father_last_name`,`mother_last_name`,`mother_first_name`,`mother_middle_name`,`elementary_name`,`elementary_degree`,`elementary_year`,`secondary_name`,`secondary_degree`,`secondary_year`,`vocational_name`,`vocational_degree`,`vocational_year`,`college_name`,`college_degree`,`college_year`,`graduate_name`,`graduate_degree`,`graduate_year`,`date_from1`,`date_to1`,`agency1`,`comunity_tax`,`issued_at`,`issued_on`,`community_tax`,`department`,`employee_status`) values (7,'registrar','Registrar','R','Registrar','2014-04-01','Male','1988-04-12',NULL,1,'Single',NULL,NULL,NULL,NULL,'2014-04-28 06:02:20','2014-04-28 06:02:20','',NULL,NULL,NULL,NULL,NULL,'registrar','','baguio city','','','','','','',NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,'','','','Registrar',NULL);
					insert  into `employees`(`id`,`employeeid`,`first_name`,`middle_name`,`last_name`,`joining_date`,`gender`,`dob`,`employeecategory_id`,`status`,`martial_status`,`no_children`,`father_name`,`mother_name`,`spouse_name`,`created_at`,`updated_at`,`email`,`name`,`admin`,`teacher`,`registrar`,`finance`,`role`,`sss_id_no`,`place_of_birth`,`citizenship`,`religion`,`pagibig_no`,`philhealth_no`,`residential_address`,`permanent_address`,`driver_license_no`,`tin`,`cellphone`,`spouse_last_name`,`spouse_first_name`,`spouse_middle_name`,`spouse_occupation`,`spouse_employer`,`spouse_business_address`,`spouse_telephone`,`child1`,`child1bday`,`child2`,`child2bday`,`child3`,`child3bday`,`child4`,`child4bday`,`child5`,`child5bday`,`child6`,`child6bday`,`child7`,`child7bday`,`father_first_name`,`father_middle_name`,`father_last_name`,`mother_last_name`,`mother_first_name`,`mother_middle_name`,`elementary_name`,`elementary_degree`,`elementary_year`,`secondary_name`,`secondary_degree`,`secondary_year`,`vocational_name`,`vocational_degree`,`vocational_year`,`college_name`,`college_degree`,`college_year`,`graduate_name`,`graduate_degree`,`graduate_year`,`date_from1`,`date_to1`,`agency1`,`comunity_tax`,`issued_at`,`issued_on`,`community_tax`,`department`,`employee_status`) values (8,'teachermakiling','maria','m','makiling','2012-04-01','Female','1987-07-14',NULL,1,'Single',NULL,NULL,NULL,NULL,'2014-04-28 06:06:36','2014-04-28 06:06:36','',NULL,NULL,NULL,NULL,NULL,'teacher','','Baguio','','','','','','',NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,'','','','Faculty',NULL);
					insert  into `employees`(`id`,`employeeid`,`first_name`,`middle_name`,`last_name`,`joining_date`,`gender`,`dob`,`employeecategory_id`,`status`,`martial_status`,`no_children`,`father_name`,`mother_name`,`spouse_name`,`created_at`,`updated_at`,`email`,`name`,`admin`,`teacher`,`registrar`,`finance`,`role`,`sss_id_no`,`place_of_birth`,`citizenship`,`religion`,`pagibig_no`,`philhealth_no`,`residential_address`,`permanent_address`,`driver_license_no`,`tin`,`cellphone`,`spouse_last_name`,`spouse_first_name`,`spouse_middle_name`,`spouse_occupation`,`spouse_employer`,`spouse_business_address`,`spouse_telephone`,`child1`,`child1bday`,`child2`,`child2bday`,`child3`,`child3bday`,`child4`,`child4bday`,`child5`,`child5bday`,`child6`,`child6bday`,`child7`,`child7bday`,`father_first_name`,`father_middle_name`,`father_last_name`,`mother_last_name`,`mother_first_name`,`mother_middle_name`,`elementary_name`,`elementary_degree`,`elementary_year`,`secondary_name`,`secondary_degree`,`secondary_year`,`vocational_name`,`vocational_degree`,`vocational_year`,`college_name`,`college_degree`,`college_year`,`graduate_name`,`graduate_degree`,`graduate_year`,`date_from1`,`date_to1`,`agency1`,`comunity_tax`,`issued_at`,`issued_on`,`community_tax`,`department`,`employee_status`) values (9,'admin','I','Am','Admin','2014-07-10','Male','1990-04-23',NULL,1,'Single',NULL,NULL,NULL,NULL,'2014-07-10 10:32:42','2014-07-10 10:32:42','',NULL,NULL,NULL,NULL,NULL,'admin','','Philippines','','','','','','',NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,'','','','Administrator',NULL);

					/*Table structure for table `enrollment_notes` */

					DROP TABLE IF EXISTS `enrollment_notes`;

					CREATE TABLE `enrollment_notes` (
					  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
					  `notes` text,
					  `sort` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `enrollment_notes` */

					/*Table structure for table `enrollments` */

					DROP TABLE IF EXISTS `enrollments`;

					CREATE TABLE `enrollments` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `studid` varchar(255) DEFAULT NULL,
					  `name` varchar(255) DEFAULT NULL,
					  `fname` varchar(255) DEFAULT NULL,
					  `middle` varchar(255) DEFAULT NULL,
					  `firstname` varchar(255) DEFAULT NULL,
					  `lastname` varchar(255) DEFAULT NULL,
					  `date_of_birth` date DEFAULT NULL,
					  `year` varchar(255) DEFAULT NULL,
					  `sy_from` varchar(255) DEFAULT NULL,
					  `sy_to` varchar(255) DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `unit` varchar(255) NOT NULL,
					  `semister` varchar(255) DEFAULT NULL,
					  `recent` varchar(255) DEFAULT NULL,
					  `sts` varchar(255) DEFAULT NULL,
					  `user_id` int(11) DEFAULT NULL,
					  `subject` varchar(255) DEFAULT NULL,
					  `status2` varchar(255) DEFAULT NULL,
					  `semester` int(11) DEFAULT NULL,
					  `age` int(11) DEFAULT NULL,
					  `is_drop` tinyint(1) NOT NULL DEFAULT '0',
					  `course` varchar(255) DEFAULT NULL,
					  `telephone` varchar(255) DEFAULT NULL,
					  `disability` varchar(255) DEFAULT '',
					  `nationality` varchar(255) DEFAULT NULL,
					  `address` varchar(255) DEFAULT NULL,
					  `mobile` varchar(255) DEFAULT NULL,
					  `sex` varchar(255) DEFAULT NULL,
					  `discount` int(11) DEFAULT NULL,
					  `total` int(11) DEFAULT NULL,
					  `less` int(11) DEFAULT NULL,
					  `net` int(11) DEFAULT NULL,
					  `place_of_birth` varchar(255) DEFAULT NULL,
					  `civil_status` varchar(255) DEFAULT NULL,
					  `guardian` varchar(255) DEFAULT NULL,
					  `occupation` varchar(255) DEFAULT NULL,
					  `residence_address` varchar(255) DEFAULT NULL,
					  `business_address` varchar(255) DEFAULT NULL,
					  `education_parent` varchar(255) DEFAULT NULL,
					  `no_family` int(11) DEFAULT NULL,
					  `occupation_parent` varchar(255) DEFAULT NULL,
					  `income` int(11) DEFAULT NULL,
					  `primary` varchar(255) DEFAULT NULL,
					  `primary_date` varchar(255) DEFAULT NULL,
					  `intermediate` varchar(255) DEFAULT NULL,
					  `intermediate_date` varchar(255) DEFAULT NULL,
					  `secondary` varchar(255) DEFAULT NULL,
					  `secondary_date` varchar(255) DEFAULT NULL,
					  `college` varchar(255) DEFAULT NULL,
					  `college_date` varchar(255) DEFAULT NULL,
					  `course_completed` varchar(255) DEFAULT NULL,
					  `course_completed_date` varchar(255) DEFAULT NULL,
					  `fake_email` varchar(255) DEFAULT NULL,
					  `status` varchar(255) DEFAULT NULL,
					  `scholar` varchar(255) DEFAULT NULL,
					  `is_active` varchar(255) DEFAULT NULL,
					  `old_or_new` varchar(255) DEFAULT NULL,
					  `total_units` int(11) DEFAULT NULL,
					  `total_lab_units` int(11) DEFAULT NULL,
					  `total_lec_units` int(11) DEFAULT NULL,
					  `father_name` varchar(255) DEFAULT NULL,
					  `father_occupation` varchar(255) DEFAULT NULL,
					  `father_contact_no` varchar(255) DEFAULT NULL,
					  `mother_name` varchar(255) DEFAULT NULL,
					  `mother_occupation` varchar(255) DEFAULT NULL,
					  `mother_contact_no` varchar(255) DEFAULT NULL,
					  `parents_address` varchar(255) DEFAULT NULL,
					  `guardian_relation` varchar(255) DEFAULT NULL,
					  `guardian_contact_no` varchar(255) DEFAULT NULL,
					  `guardian_address` varchar(255) DEFAULT NULL,
					  `present_address` varchar(255) DEFAULT NULL,
					  `provincial_address` varchar(255) DEFAULT NULL,
					  `elementary` varchar(255) DEFAULT NULL,
					  `elementary_date` varchar(255) DEFAULT NULL,
					  `vocational` varchar(255) DEFAULT NULL,
					  `vocational_date` varchar(255) DEFAULT NULL,
					  `tertiary` varchar(255) DEFAULT NULL,
					  `tertiary_date` varchar(255) DEFAULT NULL,
					  `elementary_address` varchar(255) DEFAULT NULL,
					  `elementary_degree` varchar(255) DEFAULT NULL,
					  `secondary_address` varchar(255) DEFAULT NULL,
					  `secondary_degree` varchar(255) DEFAULT NULL,
					  `vocational_address` varchar(255) DEFAULT NULL,
					  `vocational_degree` varchar(255) DEFAULT NULL,
					  `tertiary_address` varchar(255) DEFAULT NULL,
					  `tertiary_degree` varchar(255) DEFAULT NULL,
					  `others_address` varchar(255) DEFAULT NULL,
					  `others_degree` varchar(255) DEFAULT NULL,
					  `others` varchar(255) DEFAULT NULL,
					  `others_date` varchar(255) DEFAULT NULL,
					  `studentid` int(11) DEFAULT NULL,
					  `is_activated` int(11) DEFAULT '0',
					  `religion` varchar(255) DEFAULT NULL,
					  `is_paid` int(11) NOT NULL DEFAULT '0',
					  `y` int(11) DEFAULT NULL,
					  `s` int(11) DEFAULT NULL,
					  `c` int(11) DEFAULT NULL,
					  `year_id` int(11) DEFAULT NULL,
					  `course_id` int(11) DEFAULT NULL,
					  `semester_id` int(11) DEFAULT NULL,
					  `barangay` varchar(255) DEFAULT NULL,
					  `subject_load_deducted` int(11) NOT NULL DEFAULT '0',
					  `date_of_admission` varchar(255) DEFAULT NULL,
					  `admission_credentials` varchar(255) DEFAULT NULL,
					  `date_of_graduation` varchar(255) DEFAULT NULL,
					  `is_scholar` tinyint(4) DEFAULT NULL,
					  `province` varchar(255) DEFAULT NULL,
					  `municipal` varchar(255) DEFAULT NULL,
					  `living_with_parents` int(11) NOT NULL DEFAULT '0',
					  `full_paid` int(1) NOT NULL DEFAULT '0',
					  `partial_paid` int(1) NOT NULL DEFAULT '0',
					  `is_graduating` tinyint(1) DEFAULT NULL,
					  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
					  `street_no` int(11) DEFAULT NULL,
					  `is_used` smallint(1) DEFAULT '0',
					  `block_system_setting_id` int(11) DEFAULT NULL,
					  `payment_plan_id` bigint(20) DEFAULT NULL,
					  `confirm_userid` bigint(20) DEFAULT NULL,
					  `confirm_date` datetime DEFAULT NULL,
					  `is_foreigner` tinyint(1) DEFAULT '0',
					  `coursefinance_id` bigint(20) DEFAULT NULL,
					  `tuition_fee_multiplier` float DEFAULT '0' COMMENT 'Tuition Value',
					  `other_fee` float DEFAULT '0' COMMENT 'Total Additional or Other Fee',
					  `tuition_fee` float DEFAULT '0' COMMENT 'Total Tuition Fee (tuition fee * total_units)',
					  `misc_fee` float DEFAULT '0' COMMENT 'Total Misc Fee',
					  `other_school_fee` float DEFAULT '0',
					  `lab_fee` float DEFAULT '0' COMMENT 'Total Lab Fee',
					  `nstp_fee` float DEFAULT '0' COMMENT 'Total Nstp Fee',
					  `total_plan_due` float DEFAULT '0' COMMENT 'Tuition + Misc + lab + nstp',
					  `total_deduction` float DEFAULT '0',
					  `total_scholarship` float DEFAULT '0',
					  `total_excess_amount` float DEFAULT '0' COMMENT 'Total Applied Excess Amount',
					  `total_previous_amount` float DEFAULT '0',
					  `total_amount_due` float DEFAULT '0' COMMENT 'Total of all due',
					  `total_amount_paid` float DEFAULT '0' COMMENT 'Total of amount paid',
					  `total_balance` float DEFAULT '0' COMMENT 'Current Balance',
					  `payment_status` set('UNPAID','FULLY PAID','PARTIAL PAID') DEFAULT 'UNPAID' COMMENT 'Status',
					  `recompute_by` varchar(255) DEFAULT NULL,
					  `recompute_date` datetime DEFAULT NULL,
					  `is_paid_as_previous` tinyint(1) DEFAULT '0' COMMENT 'When this enrollment is paid in other enrollment as student previous account',
					  `paid_by_eid` varchar(100) DEFAULT NULL,
					  `is_apply_as_excess` tinyint(1) DEFAULT '0' COMMENT 'When balance is negative and used as payment as excess in another enrollment',
					  `excess_payment_eid` varchar(100) DEFAULT NULL COMMENT 'Enrollment Id of the one who used the excess payment',
					  PRIMARY KEY (`id`),
					  KEY `StudentID` (`studid`)
					) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

					/*Data for the table `enrollments` */

					insert  into `enrollments`(`id`,`studid`,`name`,`fname`,`middle`,`firstname`,`lastname`,`date_of_birth`,`year`,`sy_from`,`sy_to`,`updated_at`,`created_at`,`unit`,`semister`,`recent`,`sts`,`user_id`,`subject`,`status2`,`semester`,`age`,`is_drop`,`course`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_lec_units`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`payment_plan_id`,`confirm_userid`,`confirm_date`,`is_foreigner`,`coursefinance_id`,`tuition_fee_multiplier`,`other_fee`,`tuition_fee`,`misc_fee`,`other_school_fee`,`lab_fee`,`nstp_fee`,`total_plan_due`,`total_deduction`,`total_scholarship`,`total_excess_amount`,`total_previous_amount`,`total_amount_due`,`total_amount_paid`,`total_balance`,`payment_status`,`recompute_by`,`recompute_date`,`is_paid_as_previous`,`paid_by_eid`,`is_apply_as_excess`,`excess_payment_eid`) values (1,'test2','test2, test2 test2','test2','test2','test2','test2','0000-00-00',NULL,'2013','2014','2014-01-14 18:32:24','2014-01-14 18:32:24','',NULL,NULL,NULL,5,NULL,NULL,NULL,0,0,NULL,NULL,'test2','test2',NULL,'test2','Male',NULL,NULL,NULL,NULL,'test2','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'test2','test2',NULL,NULL,NULL,NULL,'test2','cross',NULL,'1',NULL,NULL,NULL,NULL,'test2','test2','test2','test2','test2','test2','test2',NULL,NULL,NULL,'test2',NULL,'test2','test2','test2','test2','test2','test2','test2',NULL,'test2',NULL,'test2','test2','test2','test2','test2','test2','test2','test2',NULL,1,'test2',0,NULL,NULL,NULL,3,1,1,'test2',0,NULL,NULL,NULL,NULL,'0','test2',1,0,0,0,0,0,0,1,1,NULL,NULL,0,NULL,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'UNPAID',NULL,NULL,0,NULL,0,NULL);
					insert  into `enrollments`(`id`,`studid`,`name`,`fname`,`middle`,`firstname`,`lastname`,`date_of_birth`,`year`,`sy_from`,`sy_to`,`updated_at`,`created_at`,`unit`,`semister`,`recent`,`sts`,`user_id`,`subject`,`status2`,`semester`,`age`,`is_drop`,`course`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_lec_units`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`payment_plan_id`,`confirm_userid`,`confirm_date`,`is_foreigner`,`coursefinance_id`,`tuition_fee_multiplier`,`other_fee`,`tuition_fee`,`misc_fee`,`other_school_fee`,`lab_fee`,`nstp_fee`,`total_plan_due`,`total_deduction`,`total_scholarship`,`total_excess_amount`,`total_previous_amount`,`total_amount_due`,`total_amount_paid`,`total_balance`,`payment_status`,`recompute_by`,`recompute_date`,`is_paid_as_previous`,`paid_by_eid`,`is_apply_as_excess`,`excess_payment_eid`) values (2,'20142478','Raven, Mark I','Mark','I','Mark','Raven','1998-01-07',NULL,'2013','2014','2014-01-14 18:35:57','2014-01-14 18:35:57','',NULL,NULL,NULL,6,NULL,NULL,NULL,16,0,NULL,NULL,'','Fil',NULL,'','Male',NULL,NULL,NULL,NULL,'Baguio City','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Easter College','2013',NULL,NULL,NULL,NULL,'','New',NULL,'1',NULL,NULL,NULL,NULL,'Mr Raven','Teacher','09225856255','Mrs Raven','Teacher','09225856255','Pinsao Pilot',NULL,NULL,NULL,'Pu3ok 2',NULL,'Easter College','Easter College','','','','','Guisad',NULL,'Guisad',NULL,'','','','','','','','',NULL,1,'Christian',1,NULL,NULL,NULL,3,1,1,'Pinsao Pilot',0,NULL,NULL,NULL,NULL,'12','Baguio City',1,0,1,0,0,34,1,1,1,NULL,NULL,0,NULL,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'UNPAID',NULL,NULL,0,NULL,0,NULL);
					insert  into `enrollments`(`id`,`studid`,`name`,`fname`,`middle`,`firstname`,`lastname`,`date_of_birth`,`year`,`sy_from`,`sy_to`,`updated_at`,`created_at`,`unit`,`semister`,`recent`,`sts`,`user_id`,`subject`,`status2`,`semester`,`age`,`is_drop`,`course`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_lec_units`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`payment_plan_id`,`confirm_userid`,`confirm_date`,`is_foreigner`,`coursefinance_id`,`tuition_fee_multiplier`,`other_fee`,`tuition_fee`,`misc_fee`,`other_school_fee`,`lab_fee`,`nstp_fee`,`total_plan_due`,`total_deduction`,`total_scholarship`,`total_excess_amount`,`total_previous_amount`,`total_amount_due`,`total_amount_paid`,`total_balance`,`payment_status`,`recompute_by`,`recompute_date`,`is_paid_as_previous`,`paid_by_eid`,`is_apply_as_excess`,`excess_payment_eid`) values (3,'pedro','pedro, pedro pedro','pedro','pedro','pedro','pedro','0000-00-00',NULL,'2013','2014','2014-01-22 15:33:13','2014-01-22 15:33:13','',NULL,NULL,NULL,8,NULL,NULL,NULL,0,0,NULL,NULL,'pedro','pedro',NULL,'pedro','Male',NULL,NULL,NULL,NULL,'pedro','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'pedro','pedro',NULL,NULL,NULL,NULL,'pedro','cross',NULL,'1',NULL,NULL,NULL,NULL,'pedro','pedro','pedro','pedro','pedro','pedro','pedro',NULL,NULL,NULL,'pedro',NULL,'pedro','pedro','pedro','pedro','pedro','pedro','pedro',NULL,'pedro',NULL,'pedro','pedro','pedro','pedro','pedro','pedro','pedro','pedro',NULL,1,'pedro',0,NULL,NULL,NULL,3,1,1,'pedro',0,NULL,NULL,NULL,NULL,'0','pedro',1,0,0,0,0,0,1,1,2,7,NULL,0,NULL,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'UNPAID',NULL,NULL,0,NULL,0,NULL);
					insert  into `enrollments`(`id`,`studid`,`name`,`fname`,`middle`,`firstname`,`lastname`,`date_of_birth`,`year`,`sy_from`,`sy_to`,`updated_at`,`created_at`,`unit`,`semister`,`recent`,`sts`,`user_id`,`subject`,`status2`,`semester`,`age`,`is_drop`,`course`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_lec_units`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`payment_plan_id`,`confirm_userid`,`confirm_date`,`is_foreigner`,`coursefinance_id`,`tuition_fee_multiplier`,`other_fee`,`tuition_fee`,`misc_fee`,`other_school_fee`,`lab_fee`,`nstp_fee`,`total_plan_due`,`total_deduction`,`total_scholarship`,`total_excess_amount`,`total_previous_amount`,`total_amount_due`,`total_amount_paid`,`total_balance`,`payment_status`,`recompute_by`,`recompute_date`,`is_paid_as_previous`,`paid_by_eid`,`is_apply_as_excess`,`excess_payment_eid`) values (4,'00000001','sly, sly sly','sly','sly','sly','sly','0000-00-00',NULL,'2013','2014','2014-01-24 11:15:03','2014-01-24 11:15:03','',NULL,NULL,NULL,9,NULL,NULL,NULL,0,0,NULL,NULL,'sly','sly',NULL,'sly','Male',NULL,NULL,NULL,NULL,'sly','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'sly','sly',NULL,NULL,NULL,NULL,'sly','new',NULL,'1',NULL,NULL,NULL,NULL,'sly','sly','sly','sly','sly','sly','sly',NULL,NULL,NULL,'sly',NULL,'sly','sly','sly','sly','sly','sly','sly',NULL,'sly',NULL,'sly','sly','sly','sly','sly','sly','sly','sly',NULL,1,'sly',1,NULL,NULL,NULL,3,1,1,'sly',0,NULL,NULL,NULL,NULL,'0','sly',1,0,1,0,0,0,1,1,1,7,NULL,0,NULL,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'UNPAID',NULL,NULL,0,NULL,0,NULL);
					insert  into `enrollments`(`id`,`studid`,`name`,`fname`,`middle`,`firstname`,`lastname`,`date_of_birth`,`year`,`sy_from`,`sy_to`,`updated_at`,`created_at`,`unit`,`semister`,`recent`,`sts`,`user_id`,`subject`,`status2`,`semester`,`age`,`is_drop`,`course`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_lec_units`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`payment_plan_id`,`confirm_userid`,`confirm_date`,`is_foreigner`,`coursefinance_id`,`tuition_fee_multiplier`,`other_fee`,`tuition_fee`,`misc_fee`,`other_school_fee`,`lab_fee`,`nstp_fee`,`total_plan_due`,`total_deduction`,`total_scholarship`,`total_excess_amount`,`total_previous_amount`,`total_amount_due`,`total_amount_paid`,`total_balance`,`payment_status`,`recompute_by`,`recompute_date`,`is_paid_as_previous`,`paid_by_eid`,`is_apply_as_excess`,`excess_payment_eid`) values (5,'00000002','tester, tester tester','tester','tester','tester','tester','0000-00-00',NULL,'2013','2014','2014-03-31 10:24:12','2014-03-31 10:24:12','',NULL,NULL,NULL,13,NULL,NULL,NULL,0,0,NULL,NULL,'tester','tester',NULL,'tester','Male',NULL,NULL,NULL,NULL,'tester','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'tester','tester',NULL,NULL,NULL,NULL,'tester','new',NULL,'1',NULL,NULL,NULL,NULL,'tester','tester','tester','tester','tester','tester','tester',NULL,NULL,NULL,'tester',NULL,'tester','tester','tester','tester','tester','tester','tester',NULL,'tester',NULL,'tester','tester','tester','tester','tester','tester','tester','tester',NULL,1,'tester',1,NULL,NULL,NULL,3,1,1,'tester',0,NULL,NULL,NULL,NULL,'Abra','tester',1,0,1,0,0,0,1,1,1,7,NULL,0,NULL,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'UNPAID',NULL,NULL,0,NULL,0,NULL);
					insert  into `enrollments`(`id`,`studid`,`name`,`fname`,`middle`,`firstname`,`lastname`,`date_of_birth`,`year`,`sy_from`,`sy_to`,`updated_at`,`created_at`,`unit`,`semister`,`recent`,`sts`,`user_id`,`subject`,`status2`,`semester`,`age`,`is_drop`,`course`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_lec_units`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`payment_plan_id`,`confirm_userid`,`confirm_date`,`is_foreigner`,`coursefinance_id`,`tuition_fee_multiplier`,`other_fee`,`tuition_fee`,`misc_fee`,`other_school_fee`,`lab_fee`,`nstp_fee`,`total_plan_due`,`total_deduction`,`total_scholarship`,`total_excess_amount`,`total_previous_amount`,`total_amount_due`,`total_amount_paid`,`total_balance`,`payment_status`,`recompute_by`,`recompute_date`,`is_paid_as_previous`,`paid_by_eid`,`is_apply_as_excess`,`excess_payment_eid`) values (6,'00000003','Lagasca, Mark Raven R','Mark Raven','R','Mark Raven','Lagasca','1997-04-03',NULL,'2013','2014','2014-04-28 12:51:08','2014-04-28 12:51:08','',NULL,NULL,NULL,16,NULL,NULL,NULL,16,0,NULL,NULL,'none','Filipino',NULL,'09225856255','Male',NULL,NULL,NULL,NULL,'Baguio City','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Easter College','2013',NULL,NULL,NULL,NULL,'mark@yahoo.com','new',NULL,'1',NULL,NULL,NULL,NULL,'Mr Lagasca','Surveyor','09225856255','Mrs Lagasca','Teacher','09225856255','34 Purok 2 Pinsao Pilot',NULL,NULL,NULL,'purok 2 ',NULL,'Easter College','Easter College','','','','','Guisad',NULL,'Guisad',NULL,'','','','','','','','',NULL,1,'Christian',0,NULL,NULL,NULL,3,1,1,'Pinsao Pilot',0,NULL,NULL,NULL,NULL,'Benguet','Baguio City',1,0,0,0,0,34,0,1,1,14,NULL,0,NULL,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'UNPAID',NULL,NULL,0,NULL,0,NULL);
					insert  into `enrollments`(`id`,`studid`,`name`,`fname`,`middle`,`firstname`,`lastname`,`date_of_birth`,`year`,`sy_from`,`sy_to`,`updated_at`,`created_at`,`unit`,`semister`,`recent`,`sts`,`user_id`,`subject`,`status2`,`semester`,`age`,`is_drop`,`course`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_lec_units`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`payment_plan_id`,`confirm_userid`,`confirm_date`,`is_foreigner`,`coursefinance_id`,`tuition_fee_multiplier`,`other_fee`,`tuition_fee`,`misc_fee`,`other_school_fee`,`lab_fee`,`nstp_fee`,`total_plan_due`,`total_deduction`,`total_scholarship`,`total_excess_amount`,`total_previous_amount`,`total_amount_due`,`total_amount_paid`,`total_balance`,`payment_status`,`recompute_by`,`recompute_date`,`is_paid_as_previous`,`paid_by_eid`,`is_apply_as_excess`,`excess_payment_eid`) values (7,'00000006','Mayao, Aldren M','Aldren','M','Aldren','Mayao','1997-06-18',NULL,'2014','2015','2014-07-01 11:49:15','2014-06-20 19:52:36','',NULL,NULL,NULL,17,NULL,NULL,NULL,17,0,NULL,NULL,'','FILIPINO',NULL,'','Male',NULL,NULL,NULL,NULL,'BAGUIO CITY','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Easter College, Inc','2013',NULL,NULL,NULL,NULL,'','new',NULL,'1',NULL,27,12,23,'Mr Mayao','Teacher','09225856255','Mrs Mayao','Teacher','09225856255','Pinsao Pilot',NULL,NULL,NULL,'Pinsao Pilot',NULL,'Easter College, Inc','Easter College, Inc','','','','','Guisad',NULL,'Guisad',NULL,'','','','','','','','',NULL,1,'CHRISTIAN',1,NULL,NULL,NULL,3,1,1,'Pinsao Pilot',0,NULL,NULL,NULL,NULL,'Benguet','Baguio City',1,0,1,0,0,34,1,2,1,14,NULL,0,3,459,0,12393,5591,200,3912,0,22096,0,0,0,0,22096,4419.2,17676.8,'PARTIAL PAID','4','2014-06-27 03:03:26',0,NULL,0,NULL);
					insert  into `enrollments`(`id`,`studid`,`name`,`fname`,`middle`,`firstname`,`lastname`,`date_of_birth`,`year`,`sy_from`,`sy_to`,`updated_at`,`created_at`,`unit`,`semister`,`recent`,`sts`,`user_id`,`subject`,`status2`,`semester`,`age`,`is_drop`,`course`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_lec_units`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`payment_plan_id`,`confirm_userid`,`confirm_date`,`is_foreigner`,`coursefinance_id`,`tuition_fee_multiplier`,`other_fee`,`tuition_fee`,`misc_fee`,`other_school_fee`,`lab_fee`,`nstp_fee`,`total_plan_due`,`total_deduction`,`total_scholarship`,`total_excess_amount`,`total_previous_amount`,`total_amount_due`,`total_amount_paid`,`total_balance`,`payment_status`,`recompute_by`,`recompute_date`,`is_paid_as_previous`,`paid_by_eid`,`is_apply_as_excess`,`excess_payment_eid`) values (8,'00000007','aaron, aaron aaron','aaron','aaron','aaron','aaron','0000-00-00',NULL,'2014','2015','2014-06-27 15:38:12','2014-06-21 16:00:46','',NULL,NULL,NULL,18,NULL,NULL,NULL,0,0,NULL,NULL,'aaron','aaron',NULL,'aaron','Male',NULL,NULL,NULL,NULL,'aaron','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'aaron','aaron',NULL,NULL,NULL,NULL,'aaron','new',NULL,'1',NULL,27,12,23,'aaron','aaron','aaron','aaron','aaron','aaron','aaron',NULL,NULL,NULL,'aaron',NULL,'aaron','aaron','aaron','aaron','aaron','aaron','aaron',NULL,'aaron',NULL,'aaron','aaron','aaron','aaron','aaron','aaron','aaron','aaron',NULL,1,'aaron',0,NULL,NULL,NULL,3,1,1,'aaron',0,NULL,NULL,NULL,NULL,'Abra','aaron',1,0,0,0,0,0,1,2,1,14,NULL,0,3,459,0,12393,5591,200,3912,0,22096,0,0,0,0,22096,0,22096,'UNPAID','4','2014-06-27 03:03:12',0,NULL,0,NULL);
					insert  into `enrollments`(`id`,`studid`,`name`,`fname`,`middle`,`firstname`,`lastname`,`date_of_birth`,`year`,`sy_from`,`sy_to`,`updated_at`,`created_at`,`unit`,`semister`,`recent`,`sts`,`user_id`,`subject`,`status2`,`semester`,`age`,`is_drop`,`course`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_lec_units`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`payment_plan_id`,`confirm_userid`,`confirm_date`,`is_foreigner`,`coursefinance_id`,`tuition_fee_multiplier`,`other_fee`,`tuition_fee`,`misc_fee`,`other_school_fee`,`lab_fee`,`nstp_fee`,`total_plan_due`,`total_deduction`,`total_scholarship`,`total_excess_amount`,`total_previous_amount`,`total_amount_due`,`total_amount_paid`,`total_balance`,`payment_status`,`recompute_by`,`recompute_date`,`is_paid_as_previous`,`paid_by_eid`,`is_apply_as_excess`,`excess_payment_eid`) values (11,'00000011','Guevarra, Dennis D','Dennis','D','Dennis','Guevarra','1997-01-12',NULL,'2014','2015','2014-06-27 15:37:39','2014-06-26 23:12:29','',NULL,NULL,NULL,21,NULL,NULL,NULL,17,0,NULL,NULL,'','FILIPINO',NULL,'09225856255','Male',NULL,NULL,NULL,NULL,'BAGUIO CITY','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Easter College, Inc','2013',NULL,NULL,NULL,NULL,'dennis@yahoo.com','new',NULL,'1',NULL,27,12,23,'Mr Guevarra','Police','09225856255','Mrs Guevarra','Teacher','09225856255','34 PURIK 2 PINSAO PILOT',NULL,NULL,NULL,'Purok 3',NULL,'Easter College, Inc','Easter College, Inc','','','','','Guisad',NULL,'Guisad',NULL,'','','','','','','','',NULL,1,'Protestant',0,NULL,NULL,NULL,3,1,1,'Pinsao Pilot',0,NULL,NULL,NULL,NULL,'Benguet','Baguio City',1,0,0,0,0,34,1,2,1,14,NULL,0,3,459,0,12393,5591,200,3912,0,22096,0,0,0,0,22096,0,22096,'UNPAID','4','2014-06-27 03:03:39',0,NULL,0,NULL);

					/*Table structure for table `events` */

					DROP TABLE IF EXISTS `events`;

					CREATE TABLE `events` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `start_date` datetime DEFAULT NULL,
					  `end_date` datetime DEFAULT NULL,
					  `title` varchar(255) DEFAULT NULL,
					  `description` text,
					  `is_holiday` tinyint(1) DEFAULT NULL,
					  `to_all` tinyint(1) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `events` */

					/*Table structure for table `examinations` */

					DROP TABLE IF EXISTS `examinations`;

					CREATE TABLE `examinations` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `year_id` int(11) DEFAULT NULL,
					  `semester_id` int(11) DEFAULT NULL,
					  `course_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `name` varchar(255) DEFAULT NULL,
					  `start_date` datetime DEFAULT NULL,
					  `end_date` datetime DEFAULT NULL,
					  `user_id` int(11) DEFAULT NULL,
					  `employee_id` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `examinations` */

					/*Table structure for table `examsubjects` */

					DROP TABLE IF EXISTS `examsubjects`;

					CREATE TABLE `examsubjects` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `examination_id` int(11) DEFAULT NULL,
					  `subject_id` int(11) DEFAULT NULL,
					  `min_mark` int(11) DEFAULT NULL,
					  `max_mark` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `start_time` datetime DEFAULT NULL,
					  `end_time` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `examsubjects` */

					/*Table structure for table `fees` */

					DROP TABLE IF EXISTS `fees`;

					CREATE TABLE `fees` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `name` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `belongs_to` varchar(255) DEFAULT NULL,
					  `category_id` int(11) DEFAULT NULL,
					  `is_active` tinyint(1) DEFAULT '1',
					  `is_deduction` tinyint(1) NOT NULL DEFAULT '0',
					  `position` int(11) DEFAULT NULL,
					  `show_in_new_student` tinyint(1) DEFAULT NULL,
					  `donot_show_in_old` tinyint(1) DEFAULT NULL,
					  `is_other` tinyint(1) DEFAULT '0',
					  `is_misc` tinyint(1) DEFAULT '0',
					  `is_nstp` tinyint(1) DEFAULT '0',
					  `is_package` tinyint(1) DEFAULT '0',
					  `value` decimal(13,2) NOT NULL DEFAULT '0.00',
					  `is_tuition_fee` smallint(1) DEFAULT '0',
					  `is_lab` tinyint(1) DEFAULT '0',
					  `is_other_school` tinyint(1) DEFAULT '0',
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

					/*Data for the table `fees` */

					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (1,'Tuition Fee','2014-01-14 16:33:40','2014-06-04 10:35:06',NULL,NULL,1,0,NULL,NULL,0,0,0,0,0,459.00,1,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (2,'ACTIVITY FEE','2014-01-14 16:36:26','2014-06-04 11:35:32',NULL,NULL,1,0,NULL,NULL,0,0,1,0,0,346.00,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (3,'ADMISSION FEE(EVALUATION/DIAGNOSTIC EXAMS)','2014-01-14 16:37:14','2014-06-04 11:36:01',NULL,NULL,1,0,NULL,NULL,0,0,1,0,0,335.00,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (4,'ATHLETICS','2014-01-14 16:37:38','2014-06-04 11:36:36',NULL,NULL,1,0,NULL,NULL,0,0,1,0,0,510.00,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (5,'AUDIO-VISUAL','2014-01-14 16:38:04','2014-06-04 11:37:11',NULL,NULL,1,0,NULL,NULL,0,0,1,0,0,712.00,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (6,'DEVELOPMENTAL FEE','2014-01-14 16:38:41','2014-06-04 11:37:53',NULL,NULL,1,0,NULL,NULL,0,0,1,0,0,189.40,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (7,'GUIDANCE AND COUNSELLING','2014-01-14 16:39:05','2014-06-04 11:38:40',NULL,NULL,1,0,NULL,NULL,0,0,1,0,0,189.40,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (8,'ID VALIDATION STICKER (PER TERM)','2014-01-14 16:39:27','2014-06-04 11:39:30',NULL,NULL,1,0,NULL,NULL,0,0,1,0,0,13.50,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (9,'INTERNET FEE','2014-01-14 16:39:57','2014-06-04 11:40:07',NULL,NULL,1,0,NULL,NULL,0,0,1,0,0,188.75,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (10,'LIBRARY','2014-01-14 16:40:20','2014-06-04 11:40:41',NULL,NULL,1,0,NULL,NULL,0,0,1,0,0,685.00,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (11,'MEDICAL AND DENTAL','2014-01-14 16:40:41','2014-06-04 11:41:23',NULL,NULL,1,0,NULL,NULL,0,0,1,0,0,245.00,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (12,'PRISAA','2014-01-14 16:41:13','2014-06-04 11:42:12',NULL,NULL,1,0,NULL,NULL,0,0,1,0,0,100.00,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (13,'REGISTRATION FEE','2014-01-14 16:41:33','2014-06-04 11:42:45',NULL,NULL,1,0,NULL,NULL,0,0,1,0,0,1027.00,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (14,'RESEARCH DEVELOPMENT FEE','2014-01-14 16:41:54','2014-06-04 11:43:20',NULL,NULL,1,0,NULL,NULL,0,0,1,0,0,90.00,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (15,'SOCIO-CULTURAL','2014-01-14 16:42:16','2014-06-04 11:43:52',NULL,NULL,1,0,NULL,NULL,0,0,1,0,0,191.00,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (16,'STAFF AND FACULTY DEVELOPMENT','2014-01-14 16:42:38','2014-06-04 11:45:49',NULL,NULL,1,0,NULL,NULL,0,0,1,0,0,292.00,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (17,'UNIVERSITY INFORMATION','2014-01-14 16:43:04','2014-06-04 11:47:07',NULL,NULL,1,0,NULL,NULL,0,0,1,0,0,113.00,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (18,'LABORATORY FEE','2014-01-14 16:47:38','2014-06-04 11:50:21',NULL,NULL,1,0,NULL,NULL,0,0,0,0,0,326.00,0,1,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (20,'INSURANCE','2014-01-14 17:07:33','2014-06-04 11:47:38',NULL,NULL,1,0,NULL,NULL,0,0,1,0,0,200.00,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (21,'STUDENT HANDBOOK','2014-01-14 17:08:14','2014-06-04 11:46:29',NULL,NULL,1,0,NULL,NULL,0,0,1,0,0,92.00,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (22,' ID','2014-01-14 17:08:38','2014-06-04 11:34:45',NULL,NULL,1,0,NULL,NULL,0,0,1,0,0,72.00,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (23,'nstp','2014-06-04 11:57:06','2014-06-04 11:57:06',NULL,NULL,1,0,NULL,NULL,0,0,0,1,0,459.00,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (24,'SB','2014-06-26 17:55:29','2014-06-26 17:57:41',NULL,NULL,1,0,NULL,NULL,0,0,0,0,0,200.00,0,0,1);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (25,'Uniform','2014-06-26 17:58:40','2014-06-26 17:58:40',NULL,NULL,1,0,NULL,NULL,0,1,0,0,0,500.00,0,0,0);
					insert  into `fees`(`id`,`name`,`created_at`,`updated_at`,`belongs_to`,`category_id`,`is_active`,`is_deduction`,`position`,`show_in_new_student`,`donot_show_in_old`,`is_other`,`is_misc`,`is_nstp`,`is_package`,`value`,`is_tuition_fee`,`is_lab`,`is_other_school`) values (26,'ID Replacement','2014-06-26 18:00:11','2014-06-26 18:00:11',NULL,NULL,1,0,NULL,NULL,0,1,0,0,0,100.00,0,0,0);

					/*Table structure for table `feevals` */

					DROP TABLE IF EXISTS `feevals`;

					CREATE TABLE `feevals` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `value` float NOT NULL DEFAULT '0',
					  `fee_id` int(11) DEFAULT NULL,
					  `enrollment_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `employeecategory_id` int(11) DEFAULT NULL,
					  `employee_id` int(11) DEFAULT NULL,
					  `is_active` tinyint(1) NOT NULL DEFAULT '1',
					  `is_deduction` tinyint(1) NOT NULL DEFAULT '0',
					  `position` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `feevals` */

					/*Table structure for table `gengrades` */

					DROP TABLE IF EXISTS `gengrades`;

					CREATE TABLE `gengrades` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `student_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `year_id` int(11) DEFAULT NULL,
					  `semester_id` int(11) DEFAULT NULL,
					  `course_id` int(11) DEFAULT NULL,
					  `examination_id` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `gengrades` */

					/*Table structure for table `genpayslips` */

					DROP TABLE IF EXISTS `genpayslips`;

					CREATE TABLE `genpayslips` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `employee_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `payslip` varchar(255) DEFAULT NULL,
					  `created_by` varchar(255) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `genpayslips` */

					/*Table structure for table `grades` */

					DROP TABLE IF EXISTS `grades`;

					CREATE TABLE `grades` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `grade` int(11) NOT NULL DEFAULT '0',
					  `student_id` int(11) DEFAULT NULL,
					  `subject_id` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `value` decimal(10,2) DEFAULT NULL,
					  `gengrade_id` int(11) DEFAULT NULL,
					  `examsubject_id` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `grades` */

					/*Table structure for table `grading_periods` */

					DROP TABLE IF EXISTS `grading_periods`;

					CREATE TABLE `grading_periods` (
					  `id` bigint(20) NOT NULL AUTO_INCREMENT,
					  `grading_period` varchar(255) NOT NULL,
					  `is_set` tinyint(4) DEFAULT NULL,
					  `orders` smallint(6) DEFAULT '1',
					  `created_at` datetime NOT NULL,
					  `updated_at` datetime NOT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

					/*Data for the table `grading_periods` */

					insert  into `grading_periods`(`id`,`grading_period`,`is_set`,`orders`,`created_at`,`updated_at`) values (1,'First Prelim',1,1,'2014-01-14 16:22:35','2014-06-04 12:07:14');
					insert  into `grading_periods`(`id`,`grading_period`,`is_set`,`orders`,`created_at`,`updated_at`) values (2,'Second Prelim',0,2,'2014-01-22 14:15:08','2014-05-12 14:40:15');
					insert  into `grading_periods`(`id`,`grading_period`,`is_set`,`orders`,`created_at`,`updated_at`) values (3,'Midterm',0,3,'2014-01-22 14:15:18','2014-01-22 14:15:18');
					insert  into `grading_periods`(`id`,`grading_period`,`is_set`,`orders`,`created_at`,`updated_at`) values (4,'Final',0,4,'2014-01-22 14:15:30','2014-01-22 14:15:30');

					/*Table structure for table `group_payments` */

					DROP TABLE IF EXISTS `group_payments`;

					CREATE TABLE `group_payments` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `start_date` datetime DEFAULT NULL,
					  `end_date` datetime DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `group_payments` */

					/*Table structure for table `issues` */

					DROP TABLE IF EXISTS `issues`;

					CREATE TABLE `issues` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `user_id` int(11) DEFAULT NULL,
					  `comment` text,
					  `issue_type` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `resolved` varchar(255) DEFAULT NULL,
					  `issued_by` varchar(255) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `issues` */

					/*Table structure for table `items` */

					DROP TABLE IF EXISTS `items`;

					CREATE TABLE `items` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `item` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
					  `brand` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
					  `product_id` int(11) NOT NULL,
					  `kind` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
					  `size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
					  `unit` int(11) NOT NULL,
					  `unit_left` int(11) DEFAULT NULL,
					  `price` float(11,2) DEFAULT '0.00',
					  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `serial` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
					  `purchase_price` float(11,2) DEFAULT '0.00',
					  `margin` decimal(10,0) DEFAULT NULL,
					  `status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
					  `min` float DEFAULT NULL,
					  `max` float DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

					/*Data for the table `items` */

					/*Table structure for table `jobs` */

					DROP TABLE IF EXISTS `jobs`;

					CREATE TABLE `jobs` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `jobs` */

					/*Table structure for table `kinds` */

					DROP TABLE IF EXISTS `kinds`;

					CREATE TABLE `kinds` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
					  `brand_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

					/*Data for the table `kinds` */

					/*Table structure for table `lessons` */

					DROP TABLE IF EXISTS `lessons`;

					CREATE TABLE `lessons` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `title` varchar(255) DEFAULT NULL,
					  `remarks` text,
					  `subject_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `file_file_name` varchar(255) DEFAULT NULL,
					  `file_content_type` varchar(255) DEFAULT NULL,
					  `file_file_size` int(11) DEFAULT NULL,
					  `user_id` int(11) DEFAULT NULL,
					  `subject` varchar(255) DEFAULT NULL,
					  `year_id` int(11) DEFAULT NULL,
					  `semester_id` int(11) DEFAULT NULL,
					  `course_id` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `lessons` */

					/*Table structure for table `libraries` */

					DROP TABLE IF EXISTS `libraries`;

					CREATE TABLE `libraries` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `title` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `author` varchar(255) DEFAULT NULL,
					  `date_due` datetime DEFAULT NULL,
					  `borrower` varchar(255) DEFAULT NULL,
					  `year` int(11) DEFAULT NULL,
					  `bar_code` int(11) DEFAULT NULL,
					  `section` varchar(255) DEFAULT NULL,
					  `date_published` datetime DEFAULT NULL,
					  `librarycategory_id` int(11) DEFAULT NULL,
					  `status` varchar(100) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `libraries` */

					/*Table structure for table `librarycards` */

					DROP TABLE IF EXISTS `librarycards`;

					CREATE TABLE `librarycards` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `date` datetime DEFAULT NULL,
					  `library_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `date_to_be_returned` date DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `librarycards` */

					/*Table structure for table `librarycategory` */

					DROP TABLE IF EXISTS `librarycategory`;

					CREATE TABLE `librarycategory` (
					  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
					  `category` varchar(100) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `librarycategory` */

					/*Table structure for table `librarysearches` */

					DROP TABLE IF EXISTS `librarysearches`;

					CREATE TABLE `librarysearches` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `title` varchar(255) DEFAULT NULL,
					  `author` varchar(255) DEFAULT NULL,
					  `year` int(11) DEFAULT NULL,
					  `bar_code` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `librarysearches` */

					/*Table structure for table `librarystatus` */

					DROP TABLE IF EXISTS `librarystatus`;

					CREATE TABLE `librarystatus` (
					  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
					  `status` varchar(100) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `librarystatus` */

					/*Table structure for table `main_menus` */

					DROP TABLE IF EXISTS `main_menus`;

					CREATE TABLE `main_menus` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `controller` varchar(100) DEFAULT NULL COMMENT 'Name of Controller',
					  `caption` varchar(100) DEFAULT NULL COMMENT 'Title of Menu',
					  `menu_grp` varchar(30) DEFAULT NULL,
					  `menu_sub` varchar(30) DEFAULT NULL,
					  `menu_num` int(11) DEFAULT '0',
					  `menu_lvl` int(11) DEFAULT '0',
					  `menu_icon` varchar(100) DEFAULT NULL,
					  `visible` smallint(1) DEFAULT '1',
					  `attr` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `remarks` varchar(255) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=latin1;

					/*Data for the table `main_menus` */

					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (1,'configure/school','School Settings','0_SUB','',1,2,NULL,1,NULL,'2014-02-14 13:25:08','2014-02-14 13:25:08','Includes school name, address, logo and others');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (2,'events','Calendar','0_SUB','',2,2,NULL,1,NULL,'2014-02-14 13:25:08','2014-02-14 13:25:08','Master File of Calendar or Events');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (3,'years','Year','1_SUB','',5,2,NULL,1,NULL,'2014-02-14 13:25:08','2014-02-14 13:25:08','Master File of Year');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (4,'semesters','Semester','1_SUB','',6,2,NULL,1,NULL,'2014-02-14 13:25:08','2014-02-14 13:25:08','Master File of Semester');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (5,'courses','Course','1_SUB','',7,2,NULL,1,NULL,'2014-02-14 13:25:08','2014-02-14 13:25:08','Master File of Course');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (6,'academic_years','Academic Year','1_SUB','',9,2,NULL,1,NULL,'2014-02-14 13:25:08','2014-02-21 12:25:44','Master File of Academic Year');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (7,'grading_periods','Grading Period','1_SUB','',10,2,NULL,1,NULL,'2014-02-14 13:25:08','2014-02-14 13:25:08','Master File of Grading Period');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (8,'open_semester','Open Semester','1_SUB','',11,2,NULL,1,NULL,'2014-02-14 13:25:09','2014-02-14 13:25:09','Sets the current semester for the system');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (9,'block_section_settings','Block Section Settings','1_SUB','',12,2,NULL,1,NULL,'2014-02-14 13:25:09','2014-02-14 13:25:09','Master File of Block Sections');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (10,'remarks','Remarks','3_SUB','',17,2,NULL,1,NULL,'2014-02-14 13:25:09','2014-02-14 13:25:09','Master File of Remarks');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (11,'fees','Fee Creator','3_SUB','',18,2,NULL,1,NULL,'2014-02-14 13:25:09','2014-02-14 13:25:09','Master File of Fees');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (12,'coursefinances','Course Fee Management','3_SUB','',19,2,NULL,1,NULL,'2014-02-14 13:25:09','2014-02-14 13:25:09','Settings of Fees, Course Finance Master File');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (13,'remaining_balance_percentages','Required Payment Percentage','3_SUB','',20,2,NULL,1,NULL,'2014-02-14 13:25:09','2014-02-14 13:25:09','');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (14,'permit_settings','Grade Slip','4_SUB','',22,2,NULL,1,NULL,'2014-02-14 13:25:09','2014-02-14 13:25:09','Settings of Grade Slip Report');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (15,'employeecategories','Employee Category','5_SUB','',25,2,NULL,1,NULL,'2014-02-14 13:25:09','2014-02-14 13:25:09','Master File for Employee Category');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (16,'open_enrollment','Open Enrollment','6_SUB','',27,2,NULL,1,NULL,'2014-02-14 13:25:09','2014-02-14 13:25:09','Opens or Closes enrollment for the current Semester');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (17,'search/search_student','Officially Enrolled','8_SUB','',29,2,NULL,1,NULL,'2014-02-14 13:25:10','2014-06-27 07:04:00','Student Search, contains actions like profile, fees and others');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (18,'search/search_enrollee','Enrollees','8_SUB','',30,2,NULL,1,NULL,'2014-02-14 13:25:10','2014-02-14 13:25:10','Search for enrollees who is currently UNPAID');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (20,'search/list_students/paid','List of Paid Students','8_SUB','',32,2,NULL,1,NULL,'2014-02-14 13:25:10','2014-02-14 13:25:10','Shows student with PAID Accounts');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (21,'search/list_students/unpaid','Unpaid Accounts','8_SUB','',33,2,NULL,1,NULL,'2014-02-14 13:25:10','2014-02-14 13:25:10','Shows student with UNPAID Accounts');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (22,'search/list_students/fullpaid','Fully Paid Students','8_SUB','',34,2,NULL,1,NULL,'2014-02-14 13:25:10','2014-02-14 13:25:10','Shows student with FULLY PAID Accounts');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (23,'search/list_students/partialpaid','Partial Paid Students','8_SUB','',35,2,NULL,1,NULL,'2014-02-14 13:25:10','2014-02-14 13:25:10','Shows student with PARTIAL PAID Accounts');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (24,'group_payments','Daily Collection','8_SUB','',36,2,NULL,1,NULL,'2014-02-14 13:25:11','2014-02-14 13:25:11','Report for daily collection or payments');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (25,'search/master_list_by_course','Master list by course','8_SUB','',37,2,NULL,1,NULL,'2014-02-14 13:25:11','2014-02-14 13:25:11',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (26,'inventory','Inventory','11_SUB','',50,2,NULL,1,NULL,'2014-02-14 13:25:11','2014-02-14 13:25:11','');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (27,'borrow_items/create','Borrow Items','11_SUB','',51,2,NULL,1,NULL,'2014-02-14 13:25:11','2014-02-14 13:25:11',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (28,'borrow_items/records','Records','11_SUB','',52,2,NULL,1,NULL,'2014-02-14 13:25:11','2014-02-14 13:25:11','Records of Borrowed Items (Library)');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (29,'announcements','Announcements','14_SUB','',56,2,NULL,1,NULL,'2014-02-14 13:25:11','2014-02-14 13:25:11','Master File for Announcements');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (30,'enrollees/daily_enrollees','Daily Enrollees','15_SUB','',61,2,NULL,1,NULL,'2014-02-14 13:25:12','2014-02-14 13:25:12','Show daily enrolless');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (31,'subjects','Subject','16_SUB','',63,2,NULL,1,NULL,'2014-02-14 13:25:12','2014-02-14 13:25:12','Master File of Subject');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (32,'fees/student_charges','Student Charges','18_SUB','',66,2,NULL,1,NULL,'2014-02-14 13:25:12','2014-02-14 13:25:12','Show the record of student charges');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (33,'excels/student_charges','Download Student Charges Excel','18_SUB','',67,2,NULL,1,NULL,'2014-02-14 13:25:12','2014-02-14 13:25:12','Download for student charges');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (34,'payments/student_payments','Student Payments','18_SUB','',68,2,NULL,1,NULL,'2014-02-14 13:25:12','2014-02-14 13:25:12','Show the record of student payments');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (35,'excels/student_payments','Download Student Payments Excel','18_SUB','',69,2,NULL,1,NULL,'2014-02-14 13:25:13','2014-02-14 13:25:13','Download for Student payments');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (36,'fees/student_deductions','Student Deductions','18_SUB','',70,2,NULL,1,NULL,'2014-02-14 13:25:13','2014-02-14 13:25:13','');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (37,'excels/student_deductions','Download Student Deductions','18_SUB','',71,2,NULL,1,NULL,'2014-02-14 13:25:13','2014-02-14 13:25:13',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (38,'payments/remaining_balance_report','Remaining Balance Report','18_SUB','',72,2,NULL,1,NULL,'2014-02-14 13:25:13','2014-02-14 13:25:13',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (39,'payments/download_remaining_balance_report','Download Remaining Balance Report','18_SUB','',73,2,NULL,1,NULL,'2014-02-14 13:25:13','2014-02-14 13:25:13',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (40,'excels/download_all_student_profile','Download All Student Profile','18_SUB','',74,2,NULL,1,NULL,'2014-02-14 13:25:13','2014-02-14 13:25:13',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (41,'exam_permit','Generate Exam Permit','18_SUB','',75,2,NULL,1,NULL,'2014-02-14 13:25:13','2014-02-14 13:25:13','Generation of Exam Permit');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (42,'admission_slips','Issue Admission Slip','21_SUB','',90,2,NULL,1,NULL,'2014-02-14 13:25:13','2014-02-14 13:25:13','');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (43,'employees','Search Employee','23_SUB','',94,2,NULL,1,NULL,'2014-02-14 13:25:13','2014-02-14 13:25:13',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (44,'employees/create','Add Employee','23_SUB','',95,2,NULL,1,NULL,'2014-02-14 13:25:13','2014-02-14 13:25:13',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (45,'employee_attendance/calendar','Employee Attendance','23_SUB','',96,2,NULL,1,NULL,'2014-02-14 13:25:13','2014-02-14 13:25:13',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (46,'book_category','Category','26_SUB','',101,2,NULL,1,NULL,'2014-02-14 13:25:13','2014-02-14 13:25:13','Master file book category');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (47,'books','Books','26_SUB','',102,2,NULL,1,NULL,'2014-02-14 13:25:13','2014-02-14 13:25:13','Master file of Books');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (48,'total_income','Total Income','29_SUB','',107,2,NULL,1,NULL,'2014-02-14 13:25:14','2014-02-14 13:25:14','Report for total income');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (49,'student_scholar','Total Scholars','29_SUB','',108,2,NULL,1,NULL,'2014-02-14 13:25:14','2014-02-14 13:25:14','Report for scholars');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (50,'subject_grades','Subject Grades','31_SUB','',116,2,NULL,1,NULL,'2014-02-14 13:25:14','2014-02-14 13:25:14',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (51,'generate_grade_slip','Generate Grade Slip','31_SUB','',117,2,NULL,1,NULL,'2014-02-14 13:25:14','2014-02-14 13:25:14','Generation of grade slip');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (52,'enrollees/dropped_students','Dropped Students','31_SUB','',119,2,NULL,1,NULL,'2014-02-14 13:25:14','2014-02-14 13:25:14',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (53,'confirm_enrollees','Confirm Enrollees','31_SUB','',120,2,NULL,1,NULL,'2014-02-14 13:25:14','2014-02-14 13:25:14','Enrollment confirmation');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (54,'ched','CHED','32_SUB','',123,2,NULL,1,NULL,'2014-02-14 13:25:14','2014-02-14 13:25:14','Report for CHED');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (55,'tesda/tesda_by_course','TESDA by Course','33_SUB','',126,2,NULL,1,NULL,'2014-02-14 13:25:14','2014-02-14 13:25:14','Report for TESDA');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (56,'students_geographical_statistic','Students geographical statistics','35_SUB','',132,2,NULL,1,NULL,'2014-02-14 13:25:14','2014-02-14 13:25:14',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (57,'international_student','International Students','35_SUB','',133,2,NULL,1,NULL,'2014-02-14 13:25:14','2014-02-14 13:25:14',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (58,'enrollees/nstp_students','NSTP Data List','35_SUB','',134,2,NULL,1,NULL,'2014-02-14 13:25:14','2014-02-14 13:25:14',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (59,'messages','Message','37_SUB','',137,2,NULL,1,NULL,'2014-02-14 13:25:14','2014-02-14 13:25:14',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (60,'section_offering','Section Offering','37_SUB','',138,2,NULL,1,NULL,'2014-02-14 13:25:14','2014-02-14 13:25:14',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (61,'registration','Registration','37_SUB','',139,2,NULL,1,NULL,'2014-02-14 13:25:15','2014-02-14 13:25:15','Student Registration Approval');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (62,'profile','Profile','37_SUB','',140,2,NULL,1,NULL,'2014-02-14 13:25:15','2014-02-14 13:25:15','Student Profile');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (63,'schedule','Schedule','37_SUB','',141,2,NULL,1,NULL,'2014-02-14 13:25:15','2014-02-14 13:25:15',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (64,'grade','Grades','37_SUB','',142,2,NULL,1,NULL,'2014-02-14 13:25:15','2014-02-14 13:25:15',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (65,'student/search_schedule','Student Schedule','39_SUB','',147,2,NULL,1,NULL,'2014-02-14 13:25:15','2014-02-14 13:25:15',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (66,'student/soa','Generate SOA','39_SUB','',148,2,NULL,1,NULL,'2014-02-14 13:25:15','2014-02-14 13:25:15','Generation of Student of Account');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (67,'student/promisory','Student Promisory','39_SUB','',150,2,NULL,1,NULL,'2014-02-14 13:25:15','2014-02-14 13:25:15',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (68,'student_master_list','Student Master List','39_SUB','',151,2,NULL,1,NULL,'2014-02-14 13:25:15','2014-02-14 13:25:15',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (69,'lessons','Lessons','41_SUB','',153,2,NULL,1,NULL,'2014-02-14 13:25:15','2014-02-14 13:25:15',NULL);
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (70,'classes/classroom','My Classes','41_SUB','',154,2,NULL,1,NULL,'2014-02-14 13:25:15','2014-02-14 13:25:15','Student Class (For Teachers)');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (71,'payment_plan','Payment Plan','41_SUB',NULL,155,2,NULL,1,NULL,NULL,NULL,'Payment plan ( Number of Division for student payments )');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (72,'payment_type','Payment Type','41_SUB',NULL,156,2,NULL,1,NULL,NULL,NULL,'Master File of Payment Type (Cash, Check etc)');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (76,'users/activate_users','Activate/De-activate Employees',NULL,NULL,0,2,NULL,1,NULL,'2014-02-21 12:57:44','2014-02-21 12:57:44','Activates or deactivate employees in using the system.');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (77,'scholarship','Scholarship Creator','42_SUB',NULL,157,2,NULL,1,NULL,NULL,NULL,'Scholarship Creator');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (92,'rooms','View Rooms','room_00',NULL,1,2,NULL,1,NULL,'2014-04-25 18:15:14','2014-04-25 18:15:14','Create Read Update Delete of Rooms');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (93,'student/reset_password','Reset Student Password','student',NULL,1,2,NULL,1,NULL,'2014-06-04 11:22:08','2014-06-04 11:22:08','Reset Student Account Password');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (94,'confirm_enrollees/lists','List of Confirm Enrollee','confirm_enrollee',NULL,1,2,NULL,1,NULL,'2014-06-27 07:47:49','2014-06-27 07:54:11','List of Confirm Enrollees currently enrolling');
					insert  into `main_menus`(`id`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`attr`,`created_at`,`updated_at`,`remarks`) values (95,'curriculum','Curriculum','curri',NULL,1,2,NULL,1,NULL,'2014-06-30 17:23:55','2014-06-30 17:25:00','Create, update and delete curicullum records.');

					/*Table structure for table `main_menus_method` */

					DROP TABLE IF EXISTS `main_menus_method`;

					CREATE TABLE `main_menus_method` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `main_menu_id` bigint(20) DEFAULT NULL,
					  `method` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `main_menus_method` */

					/*Table structure for table `masterlists` */

					DROP TABLE IF EXISTS `masterlists`;

					CREATE TABLE `masterlists` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `course_id` int(11) DEFAULT NULL,
					  `semester_id` int(11) DEFAULT NULL,
					  `year_from` int(11) DEFAULT NULL,
					  `year_to` int(11) DEFAULT NULL,
					  `count` int(11) DEFAULT NULL,
					  `count_male` int(11) DEFAULT NULL,
					  `count_female` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `masterlists` */

					/*Table structure for table `menus` */

					DROP TABLE IF EXISTS `menus`;

					CREATE TABLE `menus` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `main_menu_id` bigint(20) DEFAULT NULL,
					  `department` varchar(255) DEFAULT NULL COMMENT 'Name of Department',
					  `controller` varchar(100) DEFAULT NULL COMMENT 'Name of Controller',
					  `caption` varchar(100) DEFAULT NULL COMMENT 'Title of Menu',
					  `menu_grp` varchar(30) DEFAULT NULL,
					  `menu_sub` varchar(30) DEFAULT NULL,
					  `menu_num` int(11) DEFAULT '0',
					  `menu_lvl` int(11) DEFAULT '0',
					  `menu_icon` varchar(100) DEFAULT NULL,
					  `visible` smallint(1) DEFAULT '1',
					  `help` text,
					  `attr` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=306 DEFAULT CHARSET=latin1;

					/*Data for the table `menus` */

					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (25,NULL,'finance','#','Excel','18','18_SUB',65,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:15');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (26,32,'finance','fees/student_charges','Student Charges','18_SUB','',66,2,'glyphicon glyphicon-globe',0,NULL,NULL,NULL,'2014-02-15 11:15:23');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (27,33,'finance','excels/student_charges','Download Student Charges Excel','18_SUB','',67,2,'glyphicon glyphicon-globe',0,NULL,NULL,NULL,'2014-02-15 11:15:23');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (28,34,'finance','payments/student_payments','Student Payments','18_SUB','',68,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:23');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (29,35,'finance','excels/student_payments','Download Student Payments Excel','18_SUB','',69,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:23');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (30,36,'finance','fees/student_deductions','Student Deductions','18_SUB','',70,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:23');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (31,37,'finance','excels/student_deductions','Download Student Deductions','18_SUB','',71,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:23');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (32,38,'finance','payments/remaining_balance_report','Remaining Balance Report','18_SUB','',72,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:23');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (33,39,'finance','payments/download_remaining_balance_report','Download Remaining Balance Report','18_SUB','',73,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:23');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (34,40,'finance','excels/download_all_student_profile','Download All Student Profile','18_SUB','',74,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:23');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (35,41,'finance','exam_permit','Generate Exam Permit','18_SUB','',75,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:23');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (36,NULL,'cashier','#','Student Section','8','8_SUB',28,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:16');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (40,20,'cashier','search/list_students/paid','List of Paid Students','8_SUB','',32,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:24');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (41,21,'cashier','search/list_students/unpaid','Unpaid Accounts','8_SUB','',33,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:24');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (42,22,'cashier','search/list_students/fullpaid','Fully Paid Students','8_SUB','',34,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:24');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (43,23,'cashier','search/list_students/partialpaid','Partial Paid Students','8_SUB','',35,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:24');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (44,24,'cashier','group_payments','Daily Collection','8_SUB','',36,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:24');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (45,25,'cashier','search/master_list_by_course','Master list by course','8_SUB','',37,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:24');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (46,NULL,'cashier','#','Excel','9','9_SUB',38,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:16');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (47,32,'cashier','fees/student_charges','Student Charges','9_SUB','',39,2,'glyphicon glyphicon-globe',0,NULL,NULL,NULL,'2014-02-15 11:15:24');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (48,33,'cashier','excels/student_charges','Download Student Charges Excel','9_SUB','',40,2,'glyphicon glyphicon-globe',0,NULL,NULL,NULL,'2014-02-15 11:15:24');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (49,34,'cashier','payments/student_payments','Student Payments','9_SUB','',41,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:24');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (50,35,'cashier','excels/student_payments','Download Student Payments Excel','9_SUB','',42,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:24');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (51,36,'cashier','fees/student_deductions','Student Deductions','9_SUB','',43,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:24');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (52,37,'cashier','excels/student_deductions','Download Student Deductions','9_SUB','',44,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:24');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (53,38,'cashier','payments/remaining_balance_report','Remaining Balance Report','9_SUB','',45,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:25');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (54,39,'cashier','payments/download_remaining_balance_report','Download Remaining Balance Report','9_SUB','',46,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:25');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (55,40,'cashier','excels/download_all_student_profile','Download All Student Profile','9_SUB','',47,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:25');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (56,41,'cashier','exam_permit','Generate Exam Permit','9_SUB','',48,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:25');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (57,NULL,'custodian','#','Inventory','11','11_SUB',49,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:17');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (58,26,'custodian','inventory','Inventory','11_SUB','',50,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:25');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (59,27,'custodian','borrow_items/create','Borrow Items','11_SUB','',51,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:25');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (60,28,'custodian','borrow_items/records','Records','11_SUB','',52,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:25');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (61,NULL,'custodian','#','Student','12','12_SUB',53,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:17');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (62,17,'custodian','search/search_student','Officially Enrolled','12_SUB','',54,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:25');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (63,NULL,'dean','#','School Section','14','14_SUB',55,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:17');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (64,29,'dean','announcements','Announcements','14_SUB','',56,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:25');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (65,26,'dean','inventory','Inventory','14_SUB','',57,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:25');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (66,NULL,'dean','#','Student Section','15','15_SUB',58,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:17');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (67,17,'dean','search/search_student','Officially Enrolled','15_SUB','',59,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-17 18:45:25');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (68,18,'dean','search/search_enrollee','Assessment()','15_SUB','',60,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-17 18:45:25');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (69,30,'dean','enrollees/daily_enrollees','Daily Enrollees','15_SUB','',61,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-17 18:45:25');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (70,NULL,'dean','#','Course Settings','16','16_SUB',62,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:17');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (71,31,'dean','subjects','Subject','16_SUB','',63,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:26');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (72,25,'dean','search/master_list_by_course','Master list by course','16_SUB','',64,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:26');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (73,NULL,'hrd','#','Employee Section','23','23_SUB',93,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:18');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (74,43,'hrd','employees','Search Employee','23_SUB','',94,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-21 14:31:22');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (75,44,'hrd','employees/create','Add Employee','23_SUB','',95,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-21 14:31:22');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (77,45,'hrd','employee_attendance/calendar','Employee Attendance','23_SUB','',96,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-21 14:31:21');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (79,NULL,'hrd','#','Student Section','24','24_SUB',97,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:18');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (80,31,'hrd','subjects','Subject Master List','24_SUB','',98,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:26');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (81,25,'hrd','search/master_list_by_course','Master List by Course','24_SUB','',99,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:26');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (82,NULL,'librarian','#','Library Section','26','26_SUB',100,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:18');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (83,46,'librarian','book_category','Category','26_SUB','',101,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:26');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (84,47,'librarian','books','Books','26_SUB','',102,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:26');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (91,NULL,'registrar','#','Student Section','31','31_SUB',111,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:19');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (92,17,'registrar','search/search_student','Officially Enrolled','31_SUB','',112,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:26');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (93,25,'registrar','search/master_list_by_course','Master list by course','31_SUB','',113,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:26');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (95,30,'registrar','enrollees/daily_enrollees','Daily Enrollees','31_SUB','',115,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:26');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (96,50,'registrar','subject_grades','Subject Grades','31_SUB','',116,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:26');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (97,51,'registrar','generate_grade_slip','Generate Grade Slip','31_SUB','',117,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:26');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (98,20,'registrar','search/list_students/paid','Paid Students','31_SUB','',118,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:26');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (99,52,'registrar','enrollees/dropped_students','Dropped Students','31_SUB','',119,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:26');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (100,NULL,'registrar','#','Report','32','32_SUB',121,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:19');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (101,31,'registrar','subjects','Subject','32_SUB','',122,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:26');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (102,54,'registrar','ched','CHED','32_SUB','',123,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:26');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (103,NULL,'registrar','#','Download','33','33_SUB',124,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:19');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (104,40,'registrar','excels/download_all_student_profile','All Student Profile','33_SUB','',125,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:27');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (105,55,'registrar','tesda/tesda_by_course','TESDA by Course','33_SUB','',126,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:27');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (106,NULL,'teacher','#','Lessons','41','41_SUB',152,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:19');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (107,69,'teacher','lessons','Lessons','41_SUB','',153,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:27');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (108,70,'teacher','classes/classroom','My Classes','41_SUB','',154,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:27');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (109,NULL,'admin','#','School Section','0','0_SUB',1,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-04-25 18:10:16');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (110,1,'admin','configure/school','School Settings','0_SUB','',1,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-27 18:06:06');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (111,2,'admin','events','Calendars','0_SUB','',2,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-27 17:02:21');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (112,29,'admin','announcements','Announcement','0_SUB','',3,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-27 17:00:39');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (113,NULL,'admin','#','Course Section','1','1_SUB',4,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:20');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (114,3,'admin','years','Year','1_SUB','',5,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:27');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (115,4,'admin','semesters','Semester','1_SUB','',6,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:27');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (116,5,'admin','courses','Course','1_SUB','',7,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:27');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (117,31,'admin','subjects','Subject','1_SUB','',8,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:27');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (121,9,'admin','block_section_settings','Block Section Settings','1_SUB','',12,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:27');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (122,NULL,'admin','#','Student Section','2','2_SUB',13,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:20');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (123,17,'admin','search/search_student','Officially Enrolled','2_SUB','',1,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-06-04 11:23:28');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (124,25,'admin','search/master_list_by_course','Master list by course','2_SUB','',2,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-06-04 11:23:28');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (125,NULL,'admin','#','Finance Settings','3','3_SUB',16,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:20');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (126,10,'admin','remarks','Remarks','3_SUB','',17,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:27');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (127,11,'admin','fees','Fee Creator','3_SUB','',18,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:27');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (128,12,'admin','coursefinances','Course Fee Management','3_SUB','',19,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:27');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (129,13,'admin','remaining_balance_percentages','Required Payment Percentage','3_SUB','',20,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:28');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (135,NULL,'guidance','#','Student Section','21','21_SUB',87,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:21');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (136,17,'guidance','search/search_student','Officially Enrolled','21_SUB','',88,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:28');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (137,52,'guidance','enrollees/dropped_students','Dropped Students','21_SUB','',89,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:28');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (138,42,'guidance','admission_slips','Issue Admission Slip','21_SUB','',90,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:28');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (139,31,'guidance','subjects','Subject','21_SUB','',91,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:28');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (140,25,'guidance','search/master_list_by_course','Master list by course','21_SUB','',92,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:28');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (141,NULL,'librarian','#','Student','27','27_SUB',103,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:21');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (142,17,'librarian','search/search_student','Officially Enrolled','27_SUB','',104,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:28');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (151,NULL,'school_administrator','#','Records','35','35_SUB',127,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:21');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (152,17,'school_administrator','search/search_student','Officially Enrolled','35_SUB','',128,2,'glyphicon glyphicon-globe',0,NULL,NULL,NULL,'2014-02-17 18:02:23');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (153,30,'school_administrator','enrollees/daily_enrollees','Daily Enrollees','35_SUB','',129,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-17 18:02:24');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (154,25,'school_administrator','search/master_list_by_course','Master list by course','35_SUB','',130,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-17 18:02:24');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (155,49,'school_administrator','student_scholar','List of Scholars','35_SUB','',131,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-17 18:02:24');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (156,56,'school_administrator','students_geographical_statistic','Students geographical statistics','35_SUB','',132,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-17 18:02:24');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (157,57,'school_administrator','international_student','International Students','35_SUB','',133,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-17 18:02:24');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (158,58,'school_administrator','enrollees/nstp_students','NSTP Data List','35_SUB','',134,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-17 18:02:25');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (159,52,'school_administrator','enrollees/dropped_students','Dropped Students','35_SUB','',135,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-17 18:02:25');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (160,NULL,'student','#','Student Menus','37','37_SUB',136,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:22');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (161,59,'student','messages','Message','37_SUB','',137,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:28');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (162,60,'student','section_offering','Section Offering','37_SUB','',138,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:28');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (163,61,'student','registration','Registration','37_SUB','',139,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:28');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (164,62,'student','profile','Profile','37_SUB','',140,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:28');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (165,63,'student','schedule','Schedule','37_SUB','',141,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:28');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (166,64,'student','grade','Grades','37_SUB','',142,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (167,NULL,'student_affairs','#','Students','39','39_SUB',144,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:22');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (168,17,'student_affairs','search/search_student','Officially Enrolled','39_SUB','',145,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (169,49,'student_affairs','student_scholar','List of Scholars','39_SUB','',146,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (170,NULL,'admin','#','Enrollment','6','6_SUB',26,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-14 12:35:22');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (171,16,'admin','open_enrollment','Open Enrollment','6_SUB','',27,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (172,11,'student','fees','Accounts','37_SUB','',143,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (175,65,'student_affairs','student/search_schedule','Student Schedule','39_SUB','',147,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (176,66,'student_affairs','student/soa','Generate SOA','39_SUB','',148,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (177,41,'student_affairs','exam_permit','Generate Exam Permit','39_SUB','',149,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (178,67,'student_affairs','student/promisory','Student Promisory','39_SUB','',150,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (179,68,'student_affairs','student_master_list','Student Master List','39_SUB','',151,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (180,NULL,'finance','#','Student Section','19','19_SUB',56,1,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-20 11:02:24');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (184,20,'finance','search/list_students/paid','List of Paid Students','19_SUB','',80,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (185,21,'finance','search/list_students/unpaid','Unpaid Accounts','19_SUB','',81,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (186,22,'finance','search/list_students/fullpaid','Fully Paid Students','19_SUB','',82,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (187,23,'finance','search/list_students/partialpaid','Partial Paid Students','19_SUB','',83,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (188,24,'finance','group_payments','Daily Collection','19_SUB','',84,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (189,25,'finance','search/master_list_by_course','Master list by course','19_SUB','',85,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (190,66,'finance','student/soa','Generate SOA','19_SUB','',86,2,'glyphicon glyphicon-globe',1,NULL,NULL,NULL,'2014-02-15 11:15:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (202,NULL,'president','#','School Section','0','0_SUB',0,1,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-02-15 11:57:46','2014-02-15 11:57:46');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (204,29,'president','excels/student_payments','Download Student Payments Excel','0_SUB',NULL,69,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-02-15 11:57:46','2014-03-14 14:37:26');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (205,71,'admin','payment_plan','Payment Plan','3_SUB',NULL,155,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-02-16 16:58:25','2014-02-16 16:58:25');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (206,72,'admin','payment_type','Payment Type','3_SUB',NULL,156,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-02-16 17:25:18','2014-02-16 17:25:18');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (207,NULL,'vice_president','#','School Section','0','0_SUB',0,1,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-02-17 16:45:29','2014-02-17 16:45:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (208,24,'vice_president','group_payments','Daily Collection','0_SUB',NULL,36,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-02-17 16:45:29','2014-02-17 16:45:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (209,35,'vice_president','excels/student_payments','Download Student Payments Excel','0_SUB',NULL,69,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-02-17 16:45:57','2014-02-17 16:45:57');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (265,76,'hrd','users/activate_users','Activate/De-activate Employees','23_SUB',NULL,97,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-02-21 14:30:57','2014-02-21 14:31:22');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (268,24,'president','group_payments','Daily Collection','0_SUB',NULL,36,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-02-28 10:10:37','2014-02-28 10:10:37');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (269,77,'admin','scholarship','Scholarship Creator','3_SUB',NULL,157,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-03-28 17:41:02','2014-03-28 17:41:02');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (271,NULL,'admin','#','Room Section','271','271_SUB',271,1,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-04-25 18:18:30','2014-04-25 18:18:30');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (272,92,'admin','rooms','View Rooms','271_SUB',NULL,1,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-04-25 18:29:25','2014-04-25 18:29:25');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (273,6,'admin','academic_years','Academic Year','0_SUB',NULL,9,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-04-25 19:07:03','2014-04-25 19:07:03');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (274,7,'admin','grading_periods','Grading Period','0_SUB',NULL,10,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-04-25 19:07:04','2014-04-25 19:07:04');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (275,8,'admin','open_semester','Open Semester','0_SUB',NULL,11,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-04-25 19:07:04','2014-04-25 19:07:04');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (276,93,'admin','student/reset_password','Reset Student Password','2_SUB',NULL,3,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-06-04 11:22:37','2014-06-04 11:23:28');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (277,NULL,'admin','#','Employee Section','277','277_SUB',277,1,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-06-21 16:05:07','2014-06-21 16:05:07');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (278,76,'admin','users/activate_users','Activate/De-activate Employees','277_SUB',NULL,0,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-06-21 16:05:07','2014-06-21 16:05:07');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (279,44,'admin','employees/create','Add Employee','277_SUB',NULL,95,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-06-21 16:05:07','2014-06-21 16:05:07');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (281,43,'admin','employees','Search Employee','277_SUB',NULL,94,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-06-21 16:10:04','2014-06-21 16:10:04');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (293,NULL,'registrar','#','Enrollment','293','293_SUB',112,1,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-06-27 17:51:55','2014-06-27 17:52:14');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (294,53,'registrar','confirm_enrollees','Confirm Enrollees','293_SUB',NULL,1,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-06-27 17:51:55','2014-06-27 17:52:34');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (295,18,'registrar','search/search_enrollee','Enrollees','293_SUB',NULL,3,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-06-27 17:51:55','2014-06-27 17:52:34');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (296,94,'registrar','confirm_enrollees/lists','List of Confirm Enrollee','293_SUB',NULL,2,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-06-27 17:51:55','2014-06-27 17:52:34');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (297,NULL,'finance','#','Enrollment','297','297_SUB',57,1,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-06-27 17:55:47','2014-06-27 17:57:11');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (298,18,'finance','search/search_enrollee','Enrollees','297_SUB',NULL,30,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-06-27 17:55:47','2014-06-27 17:55:47');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (299,17,'finance','search/search_student','Officially Enrolled','297_SUB',NULL,29,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-06-27 17:55:47','2014-06-27 17:55:47');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (300,NULL,'cashier','#','Enrollment','300','300_SUB',29,1,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-06-27 17:56:29','2014-06-27 17:56:48');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (301,18,'cashier','search/search_enrollee','Enrollees','300_SUB',NULL,30,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-06-27 17:56:29','2014-06-27 17:56:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (302,17,'cashier','search/search_student','Officially Enrolled','300_SUB',NULL,29,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-06-27 17:56:29','2014-06-27 17:56:29');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (304,95,'admin','curriculum','Curriculum','1_SUB',NULL,1,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-06-30 17:25:12','2014-06-30 17:25:12');
					insert  into `menus`(`id`,`main_menu_id`,`department`,`controller`,`caption`,`menu_grp`,`menu_sub`,`menu_num`,`menu_lvl`,`menu_icon`,`visible`,`help`,`attr`,`created_at`,`updated_at`) values (305,18,'admin','search/search_enrollee','Enrollees','6_SUB',NULL,30,2,'glyphicon glyphicon-globe',1,NULL,NULL,'2014-07-01 11:48:30','2014-07-01 11:48:30');

					/*Table structure for table `menus_method` */

					DROP TABLE IF EXISTS `menus_method`;

					CREATE TABLE `menus_method` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `menu_id` bigint(20) DEFAULT NULL,
					  `method` varchar(255) DEFAULT NULL,
					  `access` tinyint(1) DEFAULT '0',
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=big5;

					/*Data for the table `menus_method` */

					/*Table structure for table `messages` */

					DROP TABLE IF EXISTS `messages`;

					CREATE TABLE `messages` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `conversation_id` bigint(11) DEFAULT NULL COMMENT 'CONVERSATION ID',
					  `sender_id` bigint(20) DEFAULT NULL COMMENT 'SENDER ID',
					  `recipient_id` bigint(20) DEFAULT NULL COMMENT 'RECIPIENT ID',
					  `message` text COMMENT 'MESSAGE CONTENT',
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `read` smallint(1) DEFAULT '0',
					  `deleted` smallint(1) DEFAULT '0',
					  `date_read` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `messages` */

					/*Table structure for table `migrate_student_subjects` */

					DROP TABLE IF EXISTS `migrate_student_subjects`;

					CREATE TABLE `migrate_student_subjects` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `tracking` int(11) DEFAULT NULL,
					  `sc_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `migrate_student_subjects` */

					/*Table structure for table `migrations` */

					DROP TABLE IF EXISTS `migrations`;

					CREATE TABLE `migrations` (
					  `version` int(3) NOT NULL
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `migrations` */

					insert  into `migrations`(`version`) values (1);

					/*Table structure for table `open_semester_employee_settings` */

					DROP TABLE IF EXISTS `open_semester_employee_settings`;

					CREATE TABLE `open_semester_employee_settings` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `user_id` int(11) DEFAULT NULL,
					  `employee_id` int(11) DEFAULT NULL,
					  `open_semester_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

					/*Data for the table `open_semester_employee_settings` */

					insert  into `open_semester_employee_settings`(`id`,`user_id`,`employee_id`,`open_semester_id`,`created_at`,`updated_at`) values (1,1,NULL,15,'2014-01-13 12:01:45','2014-06-05 06:19:34');
					insert  into `open_semester_employee_settings`(`id`,`user_id`,`employee_id`,`open_semester_id`,`created_at`,`updated_at`) values (2,2,NULL,8,'2014-01-14 17:24:27','2014-01-14 17:24:27');
					insert  into `open_semester_employee_settings`(`id`,`user_id`,`employee_id`,`open_semester_id`,`created_at`,`updated_at`) values (3,4,NULL,15,'2014-01-14 17:55:15','2014-06-20 18:39:36');
					insert  into `open_semester_employee_settings`(`id`,`user_id`,`employee_id`,`open_semester_id`,`created_at`,`updated_at`) values (4,7,NULL,8,'2014-01-22 15:24:44','2014-06-20 18:59:24');
					insert  into `open_semester_employee_settings`(`id`,`user_id`,`employee_id`,`open_semester_id`,`created_at`,`updated_at`) values (5,3,NULL,8,'2014-01-27 17:24:33','2014-01-27 17:24:33');
					insert  into `open_semester_employee_settings`(`id`,`user_id`,`employee_id`,`open_semester_id`,`created_at`,`updated_at`) values (6,10,NULL,15,'2014-01-29 11:26:27','2014-06-20 18:48:38');
					insert  into `open_semester_employee_settings`(`id`,`user_id`,`employee_id`,`open_semester_id`,`created_at`,`updated_at`) values (7,11,NULL,8,'2014-01-29 14:03:53','2014-01-29 14:03:53');
					insert  into `open_semester_employee_settings`(`id`,`user_id`,`employee_id`,`open_semester_id`,`created_at`,`updated_at`) values (8,14,NULL,15,'2014-04-28 06:09:06','2014-06-21 14:57:13');
					insert  into `open_semester_employee_settings`(`id`,`user_id`,`employee_id`,`open_semester_id`,`created_at`,`updated_at`) values (9,12,NULL,15,'2014-06-06 17:28:47','2014-06-06 17:28:47');
					insert  into `open_semester_employee_settings`(`id`,`user_id`,`employee_id`,`open_semester_id`,`created_at`,`updated_at`) values (10,22,NULL,15,'2014-07-10 10:36:56','2014-07-10 10:36:56');

					/*Table structure for table `open_semesters` */

					DROP TABLE IF EXISTS `open_semesters`;

					CREATE TABLE `open_semesters` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `academic_year_id` int(11) DEFAULT NULL,
					  `semester_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `use` int(1) NOT NULL DEFAULT '0',
					  `open_enrollment` smallint(1) DEFAULT '0',
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

					/*Data for the table `open_semesters` */

					insert  into `open_semesters`(`id`,`academic_year_id`,`semester_id`,`created_at`,`updated_at`,`use`,`open_enrollment`) values (8,1,1,'2012-12-08 01:53:08','2014-05-12 13:57:02',0,1);
					insert  into `open_semesters`(`id`,`academic_year_id`,`semester_id`,`created_at`,`updated_at`,`use`,`open_enrollment`) values (14,1,2,'2014-01-14 18:05:38','2014-01-14 18:05:38',0,0);
					insert  into `open_semesters`(`id`,`academic_year_id`,`semester_id`,`created_at`,`updated_at`,`use`,`open_enrollment`) values (15,2,1,'2014-06-04 12:08:18','2014-06-05 06:18:57',1,1);

					/*Table structure for table `orders` */

					DROP TABLE IF EXISTS `orders`;

					CREATE TABLE `orders` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `sale_id` int(11) NOT NULL,
					  `unit` int(255) DEFAULT NULL,
					  `price` int(11) NOT NULL,
					  `product` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
					  `amount` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `deducted` int(11) NOT NULL DEFAULT '1',
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

					/*Data for the table `orders` */

					/*Table structure for table `other_informations` */

					DROP TABLE IF EXISTS `other_informations`;

					CREATE TABLE `other_informations` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `recognition` varchar(255) DEFAULT NULL,
					  `organization` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `employee_id` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

					/*Data for the table `other_informations` */

					insert  into `other_informations`(`id`,`recognition`,`organization`,`created_at`,`updated_at`,`employee_id`) values (1,'reg','reg','2014-01-22 15:24:13','2014-01-22 15:24:13',3);
					insert  into `other_informations`(`id`,`recognition`,`organization`,`created_at`,`updated_at`,`employee_id`) values (2,'reg','reg','2014-01-22 15:24:13','2014-01-22 15:24:13',3);
					insert  into `other_informations`(`id`,`recognition`,`organization`,`created_at`,`updated_at`,`employee_id`) values (3,'reg','reg','2014-01-22 15:24:13','2014-01-22 15:24:13',3);
					insert  into `other_informations`(`id`,`recognition`,`organization`,`created_at`,`updated_at`,`employee_id`) values (4,'reg','reg','2014-01-22 15:24:13','2014-01-22 15:24:13',3);
					insert  into `other_informations`(`id`,`recognition`,`organization`,`created_at`,`updated_at`,`employee_id`) values (5,'reg','reg','2014-01-22 15:24:13','2014-01-22 15:24:13',3);

					/*Table structure for table `otrs` */

					DROP TABLE IF EXISTS `otrs`;

					CREATE TABLE `otrs` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `special_order_no` varchar(255) DEFAULT NULL,
					  `series` varchar(255) DEFAULT NULL,
					  `issued_on` datetime DEFAULT NULL,
					  `remarks` text,
					  `date_issued` datetime DEFAULT NULL,
					  `prepared_by` varchar(255) DEFAULT NULL,
					  `position` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `enrollment_id` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `otrs` */

					/*Table structure for table `payment_plan` */

					DROP TABLE IF EXISTS `payment_plan`;

					CREATE TABLE `payment_plan` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `name` varchar(100) DEFAULT NULL,
					  `division` smallint(2) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

					/*Data for the table `payment_plan` */

					insert  into `payment_plan`(`id`,`name`,`division`,`created_at`,`updated_at`) values (1,'Installment',5,'2014-01-14 17:16:46','2014-01-14 18:00:12');
					insert  into `payment_plan`(`id`,`name`,`division`,`created_at`,`updated_at`) values (2,'Cash',1,'2014-01-14 17:58:39','2014-01-14 17:58:39');

					/*Table structure for table `payment_plan_req` */

					DROP TABLE IF EXISTS `payment_plan_req`;

					CREATE TABLE `payment_plan_req` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `payment_plan_id` bigint(20) DEFAULT NULL,
					  `number` int(11) DEFAULT NULL,
					  `grading_period_id` varchar(20) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `payment_plan_req` */

					/*Table structure for table `payment_type` */

					DROP TABLE IF EXISTS `payment_type`;

					CREATE TABLE `payment_type` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `payment_type` varchar(100) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

					/*Data for the table `payment_type` */

					insert  into `payment_type`(`id`,`payment_type`,`created_at`,`updated_at`) values (1,'CHECK','2014-06-05 06:11:32','2014-06-21 16:51:52');
					insert  into `payment_type`(`id`,`payment_type`,`created_at`,`updated_at`) values (2,'CASH','2014-06-05 06:11:43','2014-06-05 06:11:43');

					/*Table structure for table `payments_breakdown` */

					DROP TABLE IF EXISTS `payments_breakdown`;

					CREATE TABLE `payments_breakdown` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `enrollment_id` bigint(20) NOT NULL,
					  `spr_id` bigint(20) NOT NULL,
					  `table_id` varchar(100) NOT NULL COMMENT 'student_plan_modes id, student_enrollment_fees id , student_previous_account id',
					  `fee_id` varchar(100) DEFAULT NULL COMMENT 'if type is other fee',
					  `payment_type` varchar(100) NOT NULL,
					  `fee_type` set('TUITION','OTHER','PREVIOUS') DEFAULT NULL,
					  `amount` float DEFAULT NULL COMMENT 'Amount Applied in the Fee',
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `is_deleted` tinyint(1) DEFAULT '0',
					  `delete_remarks` varchar(255) DEFAULT NULL,
					  `delete_date` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

					/*Data for the table `payments_breakdown` */

					insert  into `payments_breakdown`(`id`,`enrollment_id`,`spr_id`,`table_id`,`fee_id`,`payment_type`,`fee_type`,`amount`,`created_at`,`updated_at`,`is_deleted`,`delete_remarks`,`delete_date`) values (1,7,1,'31',NULL,'CASH','TUITION',4419.2,'2014-07-01 11:49:15','2014-07-01 11:49:15',0,NULL,NULL);

					/*Table structure for table `payslips` */

					DROP TABLE IF EXISTS `payslips`;

					CREATE TABLE `payslips` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `value` int(11) DEFAULT NULL,
					  `fee_id` int(11) DEFAULT NULL,
					  `employee_id` int(11) DEFAULT NULL,
					  `is_deduction` tinyint(1) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `genpayslip_id` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `payslips` */

					/*Table structure for table `permit_settings` */

					DROP TABLE IF EXISTS `permit_settings`;

					CREATE TABLE `permit_settings` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `name` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `permit_settings` */

					/*Table structure for table `photos` */

					DROP TABLE IF EXISTS `photos`;

					CREATE TABLE `photos` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `user_id` int(255) NOT NULL,
					  `description` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `image_file_name` varchar(255) DEFAULT NULL,
					  `image_content_type` varchar(255) DEFAULT NULL,
					  `image_file_size` int(11) DEFAULT NULL,
					  `image_updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `photos` */

					/*Table structure for table `products` */

					DROP TABLE IF EXISTS `products`;

					CREATE TABLE `products` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `product` varchar(255) NOT NULL,
					  `min` int(11) NOT NULL,
					  `max` int(11) NOT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `products` */

					/*Table structure for table `profiles` */

					DROP TABLE IF EXISTS `profiles`;

					CREATE TABLE `profiles` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `enrollment_id` int(11) DEFAULT NULL,
					  `last_name` varchar(255) DEFAULT NULL,
					  `first_name` varchar(255) DEFAULT NULL,
					  `middle_name` varchar(255) DEFAULT NULL,
					  `full_name` varchar(255) DEFAULT NULL,
					  `gender` varchar(255) DEFAULT NULL,
					  `civil_status` varchar(255) DEFAULT NULL,
					  `date_of_birth` varchar(255) DEFAULT NULL,
					  `place_of_birth` varchar(255) DEFAULT NULL,
					  `age` int(11) DEFAULT NULL,
					  `disability` varchar(255) DEFAULT NULL,
					  `nationality` varchar(255) DEFAULT NULL,
					  `religion` varchar(255) DEFAULT NULL,
					  `mobile` varchar(255) DEFAULT NULL,
					  `email` varchar(255) DEFAULT NULL,
					  `present_address` varchar(255) DEFAULT NULL,
					  `father_name` varchar(255) DEFAULT NULL,
					  `father_occupation` varchar(255) DEFAULT NULL,
					  `father_contact_no` int(11) DEFAULT NULL,
					  `mother_name` varchar(255) DEFAULT NULL,
					  `mother_occupation` varchar(255) DEFAULT NULL,
					  `mother_contact_no` int(11) DEFAULT NULL,
					  `parents_address` varchar(255) DEFAULT NULL,
					  `guardian_name` varchar(255) DEFAULT NULL,
					  `guardian_relation` varchar(255) DEFAULT NULL,
					  `guardian_contact_no` int(11) DEFAULT NULL,
					  `guardian_address` varchar(255) DEFAULT NULL,
					  `elementary` varchar(255) DEFAULT NULL,
					  `elementary_address` varchar(255) DEFAULT NULL,
					  `elementary_date` varchar(255) DEFAULT NULL,
					  `secondary` varchar(255) DEFAULT NULL,
					  `secondary_address` varchar(255) DEFAULT NULL,
					  `secondary_date` varchar(255) DEFAULT NULL,
					  `vocational` varchar(255) DEFAULT NULL,
					  `vocational_address` varchar(255) DEFAULT NULL,
					  `vocational_degree` varchar(255) DEFAULT NULL,
					  `vocational_date` varchar(255) DEFAULT NULL,
					  `tertiary` varchar(255) DEFAULT NULL,
					  `tertiary_address` varchar(255) DEFAULT NULL,
					  `tertiary_degree` varchar(255) DEFAULT NULL,
					  `tertiary_date` varchar(255) DEFAULT NULL,
					  `others` varchar(255) DEFAULT NULL,
					  `others_address` varchar(255) DEFAULT NULL,
					  `others_degree` varchar(255) DEFAULT NULL,
					  `others_date` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `provincial_address` varchar(255) DEFAULT NULL,
					  `user_id` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `profiles` */

					/*Table structure for table `province` */

					DROP TABLE IF EXISTS `province`;

					CREATE TABLE `province` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `province` varchar(100) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;

					/*Data for the table `province` */

					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (3,'Abra','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (4,'Agusan del Norte','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (5,'Agusan del Sur','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (6,'Aklan','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (7,'Albay','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (8,'Antique','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (9,'Apayao','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (10,'Aurora','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (11,'Basilan','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (12,'Bataan','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (13,'Batanes','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (14,'Batangas','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (15,'Benguet','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (16,'Biliran','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (17,'Bohol','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (18,'Bukidnon','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (19,'Bulacan','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (20,'Cagayan','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (21,'Camarines Norte','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (22,'Camarines Sur','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (23,'Camiguin','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (24,'Capiz','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (25,'Catanduanes','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (26,'Cavite','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (27,'Cebu','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (28,'Compostela Valley','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (29,'Cotabato','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (30,'Davao del Norte','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (31,'Davao del Sur','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (32,'Davao Oriental','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (33,'Eastern Samar','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (34,'Guimaras','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (35,'Ifugao','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (36,'Ilocos Norte','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (37,'Ilocos Sur','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (38,'Iloilo','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (39,'Isabela','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (40,'Kalinga','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (41,'La Union','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (42,'Laguna','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (43,'Lanao del Norte','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (44,'Lanao del Sur','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (45,'Leyte','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (46,'Maguindanao','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (47,'Marinduque','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (48,'Masbate','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (49,'Metro Manila','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (50,'Misamis Occidental','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (51,'Misamis Oriental','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (52,'Mountain Province','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (53,'Negros Occidental','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (54,'Negros Oriental','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (55,'Northern Samar','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (56,'Nueva Ecija','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (57,'Nueva Vizcaya','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (58,'Occidental Mindoro','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (59,'Oriental Mindoro','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (60,'Palawan','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (61,'Pampanga','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (62,'Pangasinan','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (63,'Quezon','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (64,'Quirino','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (65,'Rizal','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (66,'Romblon','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (67,'Samar','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (68,'Sarangani','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (69,'Siquijor','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (70,'Sorsogon','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (71,'South Cotabato','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (72,'Southern Leyte','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (73,'Sultan Kudarat','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (74,'Sulu','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (75,'Surigao del Norte','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (76,'Surigao del Sur','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (77,'Tarlac','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (78,'Tawi Tawi','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (79,'Zambales','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (80,'Zamboanga del Norte','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (81,'Zamboanga del Sur','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (82,'Zamboanga Sibugay','2014-01-28 15:30:13','2014-01-28 15:30:13');
					insert  into `province`(`id`,`province`,`created_at`,`updated_at`) values (83,'Others','2014-01-28 15:30:13','2014-01-28 15:30:13');

					/*Table structure for table `refunds` */

					DROP TABLE IF EXISTS `refunds`;

					CREATE TABLE `refunds` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `value` float DEFAULT NULL,
					  `enrollment_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `refunds` */

					/*Table structure for table `registrars` */

					DROP TABLE IF EXISTS `registrars`;

					CREATE TABLE `registrars` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `login` varchar(40) DEFAULT NULL,
					  `name` varchar(100) DEFAULT '',
					  `email` varchar(100) DEFAULT NULL,
					  `crypted_password` varchar(40) DEFAULT NULL,
					  `salt` varchar(40) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `remember_token` varchar(40) DEFAULT NULL,
					  `remember_token_expires_at` datetime DEFAULT NULL,
					  `activation_code` varchar(40) DEFAULT NULL,
					  `activated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`),
					  UNIQUE KEY `index_registrars_on_login` (`login`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `registrars` */

					/*Table structure for table `remaining_balance_percentages` */

					DROP TABLE IF EXISTS `remaining_balance_percentages`;

					CREATE TABLE `remaining_balance_percentages` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `percentage` int(11) DEFAULT '0',
					  `float_percentage` float(11,2) NOT NULL DEFAULT '0.00',
					  `semester_id` int(11) NOT NULL DEFAULT '0',
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `remaining_balance_percentages` */

					/*Table structure for table `remarks` */

					DROP TABLE IF EXISTS `remarks`;

					CREATE TABLE `remarks` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `value` varchar(255) DEFAULT NULL,
					  `is_deduction` tinyint(1) DEFAULT NULL,
					  `is_payment` tinyint(1) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

					/*Data for the table `remarks` */

					insert  into `remarks`(`id`,`value`,`is_deduction`,`is_payment`,`created_at`,`updated_at`) values (1,'DOWNPAYMENT',0,1,'2014-06-05 06:18:17','2014-06-05 06:18:17');

					/*Table structure for table `roles` */

					DROP TABLE IF EXISTS `roles`;

					CREATE TABLE `roles` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `name` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `roles` */

					/*Table structure for table `roles_users` */

					DROP TABLE IF EXISTS `roles_users`;

					CREATE TABLE `roles_users` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `user_id` int(11) NOT NULL,
					  `role_id` int(11) NOT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=MyISAM DEFAULT CHARSET=utf8;

					/*Data for the table `roles_users` */

					/*Table structure for table `room_schedules` */

					DROP TABLE IF EXISTS `room_schedules`;

					CREATE TABLE `room_schedules` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `room_id` int(11) DEFAULT NULL,
					  `time_schedule_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `room_schedules` */

					/*Table structure for table `rooms` */

					DROP TABLE IF EXISTS `rooms`;

					CREATE TABLE `rooms` (
					  `id` bigint(11) NOT NULL AUTO_INCREMENT,
					  `name` varchar(128) NOT NULL,
					  `description` varchar(128) NOT NULL,
					  `academic_year_id` varchar(50) DEFAULT NULL,
					  `section_assigned` tinyint(1) DEFAULT '0',
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

					/*Data for the table `rooms` */

					insert  into `rooms`(`id`,`name`,`description`,`academic_year_id`,`section_assigned`,`created_at`,`updated_at`) values (1,'H205','Room for BLMS',NULL,0,'2014-06-04 12:37:24','2014-06-04 14:46:02');
					insert  into `rooms`(`id`,`name`,`description`,`academic_year_id`,`section_assigned`,`created_at`,`updated_at`) values (2,'B403','Room for BLMS',NULL,0,'2014-06-04 13:10:24','2014-06-04 13:28:22');
					insert  into `rooms`(`id`,`name`,`description`,`academic_year_id`,`section_assigned`,`created_at`,`updated_at`) values (3,'E710','Room for BLMS',NULL,0,'2014-06-04 13:11:01','2014-06-04 13:53:04');
					insert  into `rooms`(`id`,`name`,`description`,`academic_year_id`,`section_assigned`,`created_at`,`updated_at`) values (4,'B503','Room for BLMS',NULL,0,'2014-06-04 13:11:49','2014-06-04 13:29:41');
					insert  into `rooms`(`id`,`name`,`description`,`academic_year_id`,`section_assigned`,`created_at`,`updated_at`) values (5,'A511','Room for BLMS',NULL,0,'2014-06-04 13:12:28','2014-06-04 13:27:31');
					insert  into `rooms`(`id`,`name`,`description`,`academic_year_id`,`section_assigned`,`created_at`,`updated_at`) values (6,'B604','Room for BLMS',NULL,0,'2014-06-04 13:13:40','2014-06-04 13:39:06');
					insert  into `rooms`(`id`,`name`,`description`,`academic_year_id`,`section_assigned`,`created_at`,`updated_at`) values (7,'PUYAT SPORTS','PESLFB3',NULL,0,'2014-06-04 13:23:27','2014-06-04 13:23:27');
					insert  into `rooms`(`id`,`name`,`description`,`academic_year_id`,`section_assigned`,`created_at`,`updated_at`) values (8,'B507','Room for BLMS',NULL,0,'2014-06-04 13:23:48','2014-06-04 13:32:32');
					insert  into `rooms`(`id`,`name`,`description`,`academic_year_id`,`section_assigned`,`created_at`,`updated_at`) values (9,'A101','EDUCATION BLDG','2',0,'2014-06-21 14:46:33','2014-06-21 14:46:33');
					insert  into `rooms`(`id`,`name`,`description`,`academic_year_id`,`section_assigned`,`created_at`,`updated_at`) values (10,'A102','EDUCATION BLDG','2',0,'2014-06-21 14:47:04','2014-06-21 14:47:04');

					/*Table structure for table `sales` */

					DROP TABLE IF EXISTS `sales`;

					CREATE TABLE `sales` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `from_warehouse` int(11) NOT NULL DEFAULT '0',
					  `code` int(11) DEFAULT '0',
					  `customer` varchar(255) DEFAULT NULL,
					  `address` varchar(255) DEFAULT NULL,
					  `cash` int(11) DEFAULT '0',
					  `change` int(11) DEFAULT '0',
					  `casher` varchar(255) DEFAULT NULL,
					  `total` int(11) NOT NULL DEFAULT '0',
					  `grand_total` int(11) NOT NULL DEFAULT '0',
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `sales` */

					/*Table structure for table `schema_migrations` */

					DROP TABLE IF EXISTS `schema_migrations`;

					CREATE TABLE `schema_migrations` (
					  `version` varchar(255) NOT NULL,
					  UNIQUE KEY `unique_schema_migrations` (`version`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `schema_migrations` */

					/*Table structure for table `searchsubjects` */

					DROP TABLE IF EXISTS `searchsubjects`;

					CREATE TABLE `searchsubjects` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `subject` varchar(255) DEFAULT NULL,
					  `year_id` int(11) DEFAULT NULL,
					  `semester_id` int(11) DEFAULT NULL,
					  `course_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `searchsubjects` */

					/*Table structure for table `semesters` */

					DROP TABLE IF EXISTS `semesters`;

					CREATE TABLE `semesters` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `name` varchar(255) DEFAULT NULL,
					  `level` int(5) DEFAULT '1',
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

					/*Data for the table `semesters` */

					insert  into `semesters`(`id`,`name`,`level`,`created_at`,`updated_at`) values (1,'First Semester',1,'2012-10-03 11:58:20','2012-10-03 11:58:20');
					insert  into `semesters`(`id`,`name`,`level`,`created_at`,`updated_at`) values (2,'Second Semester',2,'2012-10-03 11:58:55','2014-07-02 17:11:16');
					insert  into `semesters`(`id`,`name`,`level`,`created_at`,`updated_at`) values (3,'Summer',3,'2012-10-03 11:59:48','2014-07-02 17:11:28');

					/*Table structure for table `semisters` */

					DROP TABLE IF EXISTS `semisters`;

					CREATE TABLE `semisters` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `name` varchar(255) DEFAULT NULL,
					  `year_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `semisters` */

					/*Table structure for table `serialnumbers` */

					DROP TABLE IF EXISTS `serialnumbers`;

					CREATE TABLE `serialnumbers` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `serial` int(11) DEFAULT NULL,
					  `generate_type` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `serialnumbers` */

					/*Table structure for table `settings` */

					DROP TABLE IF EXISTS `settings`;

					CREATE TABLE `settings` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `school_name` varchar(255) DEFAULT NULL,
					  `school_address` varchar(255) DEFAULT NULL,
					  `school_telephone` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `email` varchar(255) DEFAULT NULL,
					  `logo` varchar(255) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

					/*Data for the table `settings` */

					insert  into `settings`(`id`,`school_name`,`school_address`,`school_telephone`,`created_at`,`updated_at`,`email`,`logo`) values (1,'University of Baguio','Baguio City','','0000-00-00 00:00:00','0000-00-00 00:00:00','UB@gmail.com','assets/images/logo/ub_logo.PNG');

					/*Table structure for table `student_attendance` */

					DROP TABLE IF EXISTS `student_attendance`;

					CREATE TABLE `student_attendance` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `enrollment_id` bigint(20) NOT NULL,
					  `number` int(11) DEFAULT '1',
					  `month` varchar(120) DEFAULT NULL,
					  `total_school_days` float DEFAULT '0',
					  `total_school_day_present` float DEFAULT '0',
					  `total_times_tardy` float DEFAULT '0',
					  `user_id` varchar(20) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=MyISAM DEFAULT CHARSET=utf8;

					/*Data for the table `student_attendance` */

					/*Table structure for table `student_deductions` */

					DROP TABLE IF EXISTS `student_deductions`;

					CREATE TABLE `student_deductions` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `date` datetime DEFAULT NULL,
					  `category` varchar(255) DEFAULT NULL,
					  `amount` float DEFAULT NULL,
					  `remarks` text,
					  `enrollment_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `student_deductions` */

					/*Table structure for table `student_deductions_record` */

					DROP TABLE IF EXISTS `student_deductions_record`;

					CREATE TABLE `student_deductions_record` (
					  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
					  `amount` decimal(10,0) NOT NULL DEFAULT '0',
					  `remarks` varchar(255) NOT NULL DEFAULT '',
					  `enrollment_id` int(10) unsigned NOT NULL DEFAULT '0',
					  `user_trans_id` int(10) unsigned NOT NULL DEFAULT '0',
					  `schoolyear_id` int(10) unsigned NOT NULL DEFAULT '0',
					  `deduction_name` varchar(45) NOT NULL DEFAULT '',
					  `academic_year_id` int(11) NOT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `student_deductions_record` */

					/*Table structure for table `student_enrollment_fees` */

					DROP TABLE IF EXISTS `student_enrollment_fees`;

					CREATE TABLE `student_enrollment_fees` (
					  `sef_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
					  `sef_enrollment_id` int(10) unsigned NOT NULL,
					  `sef_fee_name` varchar(255) NOT NULL,
					  `sef_fee_rate` float NOT NULL,
					  `sef_gperiod_id` int(10) unsigned NOT NULL,
					  `sef_schoolyear_id` int(10) unsigned NOT NULL,
					  `fee_id` bigint(20) DEFAULT NULL,
					  `is_tuition_fee` tinyint(1) unsigned NOT NULL DEFAULT '0',
					  `is_misc_fee` tinyint(1) unsigned NOT NULL DEFAULT '0',
					  `is_other_fee` tinyint(1) unsigned NOT NULL DEFAULT '0',
					  `is_other_school` tinyint(1) DEFAULT '0',
					  `is_lab_fee` tinyint(4) NOT NULL DEFAULT '0',
					  `is_nstp_fee` tinyint(4) NOT NULL DEFAULT '0',
					  `amount_paid` float DEFAULT '0',
					  `is_paid` tinyint(1) DEFAULT '0',
					  `spr_id` bigint(20) DEFAULT NULL COMMENT 'Student Payment Record ID',
					  `is_check` tinyint(1) DEFAULT '0',
					  `check_applied` float DEFAULT '0',
					  `created_at` datetime DEFAULT NULL,
					  `is_added` tinyint(1) DEFAULT '0' COMMENT 'If fee is added by Registrar/finance',
					  `added_by` varchar(100) DEFAULT NULL COMMENT 'User Id of register/finance who add this fee',
					  `updated_at` datetime DEFAULT NULL,
					  `recompute_by` bigint(20) DEFAULT NULL,
					  `recompute_date` datetime DEFAULT NULL,
					  `is_deduction_paid` tinyint(1) DEFAULT '0' COMMENT 'If paid by deduction/scholarship/excess',
					  `is_deleted` tinyint(1) DEFAULT '0' COMMENT 'When deleted value is 1',
					  `deleted_by` bigint(20) DEFAULT NULL COMMENT 'User Id of the one who delete',
					  `delete_date` datetime DEFAULT NULL,
					  `delete_remarks` varchar(255) DEFAULT NULL COMMENT 'Remarks When deleted',
					  PRIMARY KEY (`sef_id`)
					) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=latin1;

					/*Data for the table `student_enrollment_fees` */

					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (1,8,'Tuition Fee',459,1,2,1,1,0,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:46',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (2,8,'LABORATORY FEE',326,1,2,18,0,0,0,0,1,0,0,0,NULL,0,0,'2014-06-21 16:00:46',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (3,8,'ACTIVITY FEE',346,1,2,2,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:46',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (4,8,'ADMISSION FEE(EVALUATION/DIAGNOSTIC EXAMS)',335,1,2,3,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:46',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (5,8,'ATHLETICS',510,1,2,4,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:46',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (6,8,'AUDIO-VISUAL',712,1,2,5,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:46',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (7,8,'DEVELOPMENTAL FEE',189,1,2,6,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:46',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (8,8,'GUIDANCE AND COUNSELLING',189,1,2,7,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:46',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (9,8,'ID VALIDATION STICKER (PER TERM)',14,1,2,8,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:46',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (10,8,'INTERNET FEE',189,1,2,9,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:47',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (11,8,'LIBRARY',685,1,2,10,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:47',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (12,8,'MEDICAL AND DENTAL',245,1,2,11,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:47',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (13,8,'PRISAA',100,1,2,12,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:47',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (14,8,'REGISTRATION FEE',1027,1,2,13,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:47',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (15,8,'RESEARCH DEVELOPMENT FEE',90,1,2,14,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:47',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (16,8,'SOCIO-CULTURAL',191,1,2,15,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:47',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (17,8,'STAFF AND FACULTY DEVELOPMENT',292,1,2,16,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:47',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (18,8,'UNIVERSITY INFORMATION',113,1,2,17,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:47',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (19,8,'INSURANCE',200,1,2,20,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:47',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (20,8,'STUDENT HANDBOOK',92,1,2,21,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:47',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (21,8,' ID',72,1,2,22,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:47',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (22,8,'SB',200,1,2,19,0,0,1,0,0,0,0,0,NULL,0,0,'2014-06-21 16:00:47',0,NULL,'2014-06-27 15:38:12',NULL,NULL,0,1,4,'2014-06-27 03:03:12','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (23,7,'Tuition Fee',459,1,2,1,1,0,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (24,7,'LABORATORY FEE',326,1,2,18,0,0,0,0,1,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (25,7,'ACTIVITY FEE',346,1,2,2,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (26,7,'ADMISSION FEE(EVALUATION/DIAGNOSTIC EXAMS)',335,1,2,3,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (27,7,'ATHLETICS',510,1,2,4,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (28,7,'AUDIO-VISUAL',712,1,2,5,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (29,7,'DEVELOPMENTAL FEE',189,1,2,6,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (30,7,'GUIDANCE AND COUNSELLING',189,1,2,7,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (31,7,'ID VALIDATION STICKER (PER TERM)',14,1,2,8,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (32,7,'INTERNET FEE',189,1,2,9,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (33,7,'LIBRARY',685,1,2,10,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (34,7,'MEDICAL AND DENTAL',245,1,2,11,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (35,7,'PRISAA',100,1,2,12,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (36,7,'REGISTRATION FEE',1027,1,2,13,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (37,7,'RESEARCH DEVELOPMENT FEE',90,1,2,14,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (38,7,'SOCIO-CULTURAL',191,1,2,15,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (39,7,'STAFF AND FACULTY DEVELOPMENT',292,1,2,16,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (40,7,'UNIVERSITY INFORMATION',113,1,2,17,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (41,7,'INSURANCE',200,1,2,20,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (42,7,'STUDENT HANDBOOK',92,1,2,21,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (43,7,' ID',72,1,2,22,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (44,7,'SB',200,1,2,19,0,0,1,0,0,0,0,0,NULL,0,0,'2014-06-21 16:19:51',0,NULL,'2014-06-27 15:38:26',12,'2014-06-21 04:04:51',0,1,4,'2014-06-27 03:03:26','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (45,9,'Tuition Fee',459,1,2,1,1,0,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (46,9,'LABORATORY FEE',326,1,2,18,0,0,0,0,1,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (47,9,'ACTIVITY FEE',346,1,2,2,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (48,9,'ADMISSION FEE(EVALUATION/DIAGNOSTIC EXAMS)',335,1,2,3,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (49,9,'ATHLETICS',510,1,2,4,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (50,9,'AUDIO-VISUAL',712,1,2,5,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (51,9,'DEVELOPMENTAL FEE',189,1,2,6,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (52,9,'GUIDANCE AND COUNSELLING',189,1,2,7,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (53,9,'ID VALIDATION STICKER (PER TERM)',14,1,2,8,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (54,9,'INTERNET FEE',189,1,2,9,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (55,9,'LIBRARY',685,1,2,10,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (56,9,'MEDICAL AND DENTAL',245,1,2,11,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (57,9,'PRISAA',100,1,2,12,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (58,9,'REGISTRATION FEE',1027,1,2,13,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (59,9,'RESEARCH DEVELOPMENT FEE',90,1,2,14,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (60,9,'SOCIO-CULTURAL',191,1,2,15,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (61,9,'STAFF AND FACULTY DEVELOPMENT',292,1,2,16,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (62,9,'UNIVERSITY INFORMATION',113,1,2,17,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (63,9,'INSURANCE',200,1,2,20,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (64,9,'STUDENT HANDBOOK',92,1,2,21,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (65,9,' ID',72,1,2,22,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 18:29:22',0,NULL,'2014-06-26 18:29:22',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (66,10,'Tuition Fee',459,1,2,1,1,0,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (67,10,'LABORATORY FEE',326,1,2,18,0,0,0,0,1,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (68,10,'ACTIVITY FEE',346,1,2,2,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (69,10,'ADMISSION FEE(EVALUATION/DIAGNOSTIC EXAMS)',335,1,2,3,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (70,10,'ATHLETICS',510,1,2,4,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (71,10,'AUDIO-VISUAL',712,1,2,5,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (72,10,'DEVELOPMENTAL FEE',189,1,2,6,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (73,10,'GUIDANCE AND COUNSELLING',189,1,2,7,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (74,10,'ID VALIDATION STICKER (PER TERM)',14,1,2,8,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (75,10,'INTERNET FEE',189,1,2,9,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (76,10,'LIBRARY',685,1,2,10,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (77,10,'MEDICAL AND DENTAL',245,1,2,11,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (78,10,'PRISAA',100,1,2,12,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (79,10,'REGISTRATION FEE',1027,1,2,13,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (80,10,'RESEARCH DEVELOPMENT FEE',90,1,2,14,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (81,10,'SOCIO-CULTURAL',191,1,2,15,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (82,10,'STAFF AND FACULTY DEVELOPMENT',292,1,2,16,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (83,10,'UNIVERSITY INFORMATION',113,1,2,17,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (84,10,'INSURANCE',200,1,2,20,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (85,10,'STUDENT HANDBOOK',92,1,2,21,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (86,10,' ID',72,1,2,22,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 19:02:55',0,NULL,'2014-06-26 19:02:55',NULL,NULL,0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (87,11,'Tuition Fee',459,1,2,1,1,0,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (88,11,'LABORATORY FEE',326,1,2,18,0,0,0,0,1,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (89,11,'ACTIVITY FEE',346,1,2,2,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (90,11,'ADMISSION FEE(EVALUATION/DIAGNOSTIC EXAMS)',335,1,2,3,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (91,11,'ATHLETICS',510,1,2,4,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (92,11,'AUDIO-VISUAL',712,1,2,5,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (93,11,'DEVELOPMENTAL FEE',189,1,2,6,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (94,11,'GUIDANCE AND COUNSELLING',189,1,2,7,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (95,11,'ID VALIDATION STICKER (PER TERM)',14,1,2,8,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (96,11,'INTERNET FEE',189,1,2,9,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (97,11,'LIBRARY',685,1,2,10,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (98,11,'MEDICAL AND DENTAL',245,1,2,11,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (99,11,'PRISAA',100,1,2,12,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (100,11,'REGISTRATION FEE',1027,1,2,13,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (101,11,'RESEARCH DEVELOPMENT FEE',90,1,2,14,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (102,11,'SOCIO-CULTURAL',191,1,2,15,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (103,11,'STAFF AND FACULTY DEVELOPMENT',292,1,2,16,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (104,11,'UNIVERSITY INFORMATION',113,1,2,17,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (105,11,'INSURANCE',200,1,2,20,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (106,11,'STUDENT HANDBOOK',92,1,2,21,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (107,11,' ID',72,1,2,22,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (108,11,'SB',200,1,2,24,0,0,0,0,0,0,0,0,NULL,0,0,'2014-06-26 23:12:29',0,NULL,'2014-06-27 15:37:39',NULL,NULL,0,1,4,'2014-06-27 03:03:39','recompute fees');
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (109,11,'Tuition Fee',459,1,2,1,1,0,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (110,11,'LABORATORY FEE',326,1,2,18,0,0,0,0,1,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (111,11,'ACTIVITY FEE',346,1,2,2,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (112,11,'ADMISSION FEE(EVALUATION/DIAGNOSTIC EXAMS)',335,1,2,3,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (113,11,'ATHLETICS',510,1,2,4,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (114,11,'AUDIO-VISUAL',712,1,2,5,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (115,11,'DEVELOPMENTAL FEE',189,1,2,6,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (116,11,'GUIDANCE AND COUNSELLING',189,1,2,7,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (117,11,'ID VALIDATION STICKER (PER TERM)',14,1,2,8,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (118,11,'INTERNET FEE',189,1,2,9,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (119,11,'LIBRARY',685,1,2,10,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (120,11,'MEDICAL AND DENTAL',245,1,2,11,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (121,11,'PRISAA',100,1,2,12,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (122,11,'REGISTRATION FEE',1027,1,2,13,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (123,11,'RESEARCH DEVELOPMENT FEE',90,1,2,14,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (124,11,'SOCIO-CULTURAL',191,1,2,15,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (125,11,'STAFF AND FACULTY DEVELOPMENT',292,1,2,16,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (126,11,'UNIVERSITY INFORMATION',113,1,2,17,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (127,11,'INSURANCE',200,1,2,20,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (128,11,'STUDENT HANDBOOK',92,1,2,21,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (129,11,' ID',72,1,2,22,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (130,11,'SB',200,1,2,24,0,0,0,1,0,0,0,0,NULL,0,0,'2014-06-27 15:37:39',0,NULL,'2014-06-27 15:37:39',4,'2014-06-27 03:03:39',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (131,8,'Tuition Fee',459,1,2,1,1,0,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (132,8,'LABORATORY FEE',326,1,2,18,0,0,0,0,1,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (133,8,'ACTIVITY FEE',346,1,2,2,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (134,8,'ADMISSION FEE(EVALUATION/DIAGNOSTIC EXAMS)',335,1,2,3,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (135,8,'ATHLETICS',510,1,2,4,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (136,8,'AUDIO-VISUAL',712,1,2,5,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (137,8,'DEVELOPMENTAL FEE',189,1,2,6,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (138,8,'GUIDANCE AND COUNSELLING',189,1,2,7,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (139,8,'ID VALIDATION STICKER (PER TERM)',14,1,2,8,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (140,8,'INTERNET FEE',189,1,2,9,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (141,8,'LIBRARY',685,1,2,10,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (142,8,'MEDICAL AND DENTAL',245,1,2,11,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (143,8,'PRISAA',100,1,2,12,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (144,8,'REGISTRATION FEE',1027,1,2,13,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (145,8,'RESEARCH DEVELOPMENT FEE',90,1,2,14,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (146,8,'SOCIO-CULTURAL',191,1,2,15,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (147,8,'STAFF AND FACULTY DEVELOPMENT',292,1,2,16,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (148,8,'UNIVERSITY INFORMATION',113,1,2,17,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (149,8,'INSURANCE',200,1,2,20,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (150,8,'STUDENT HANDBOOK',92,1,2,21,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (151,8,' ID',72,1,2,22,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (152,8,'SB',200,1,2,24,0,0,0,1,0,0,0,0,NULL,0,0,'2014-06-27 15:38:12',0,NULL,'2014-06-27 15:38:12',4,'2014-06-27 03:03:12',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (153,7,'Tuition Fee',459,1,2,1,1,0,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (154,7,'LABORATORY FEE',326,1,2,18,0,0,0,0,1,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (155,7,'ACTIVITY FEE',346,1,2,2,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (156,7,'ADMISSION FEE(EVALUATION/DIAGNOSTIC EXAMS)',335,1,2,3,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (157,7,'ATHLETICS',510,1,2,4,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (158,7,'AUDIO-VISUAL',712,1,2,5,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (159,7,'DEVELOPMENTAL FEE',189,1,2,6,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (160,7,'GUIDANCE AND COUNSELLING',189,1,2,7,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (161,7,'ID VALIDATION STICKER (PER TERM)',14,1,2,8,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (162,7,'INTERNET FEE',189,1,2,9,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (163,7,'LIBRARY',685,1,2,10,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (164,7,'MEDICAL AND DENTAL',245,1,2,11,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (165,7,'PRISAA',100,1,2,12,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (166,7,'REGISTRATION FEE',1027,1,2,13,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (167,7,'RESEARCH DEVELOPMENT FEE',90,1,2,14,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (168,7,'SOCIO-CULTURAL',191,1,2,15,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (169,7,'STAFF AND FACULTY DEVELOPMENT',292,1,2,16,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (170,7,'UNIVERSITY INFORMATION',113,1,2,17,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (171,7,'INSURANCE',200,1,2,20,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (172,7,'STUDENT HANDBOOK',92,1,2,21,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (173,7,' ID',72,1,2,22,0,1,0,0,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);
					insert  into `student_enrollment_fees`(`sef_id`,`sef_enrollment_id`,`sef_fee_name`,`sef_fee_rate`,`sef_gperiod_id`,`sef_schoolyear_id`,`fee_id`,`is_tuition_fee`,`is_misc_fee`,`is_other_fee`,`is_other_school`,`is_lab_fee`,`is_nstp_fee`,`amount_paid`,`is_paid`,`spr_id`,`is_check`,`check_applied`,`created_at`,`is_added`,`added_by`,`updated_at`,`recompute_by`,`recompute_date`,`is_deduction_paid`,`is_deleted`,`deleted_by`,`delete_date`,`delete_remarks`) values (174,7,'SB',200,1,2,24,0,0,0,1,0,0,0,0,NULL,0,0,'2014-06-27 15:38:26',0,NULL,'2014-06-27 15:38:26',4,'2014-06-27 03:03:26',0,0,NULL,NULL,NULL);

					/*Table structure for table `student_finance_categories` */

					DROP TABLE IF EXISTS `student_finance_categories`;

					CREATE TABLE `student_finance_categories` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `enrollment_id` int(11) DEFAULT NULL,
					  `coursefinance_id` int(11) DEFAULT NULL,
					  `nstp` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `student_finance_categories` */

					/*Table structure for table `student_grade_categories` */

					DROP TABLE IF EXISTS `student_grade_categories`;

					CREATE TABLE `student_grade_categories` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `student_grade_id` int(11) DEFAULT NULL,
					  `category` varchar(255) DEFAULT NULL,
					  `value` float DEFAULT NULL,
					  `editable` int(11) NOT NULL DEFAULT '0',
					  `user_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=233 DEFAULT CHARSET=utf8;

					/*Data for the table `student_grade_categories` */

					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (1,1,'Preliminary',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (2,1,'Midterm',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (3,1,'Semi-Finals',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (4,1,'Finals',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (5,2,'Preliminary',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (6,2,'Midterm',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (7,2,'Semi-Finals',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (8,2,'Finals',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (9,3,'Preliminary',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (10,3,'Midterm',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (11,3,'Semi-Finals',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (12,3,'Finals',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (13,4,'Preliminary',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (14,4,'Midterm',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (15,4,'Semi-Finals',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (16,4,'Finals',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (17,5,'Preliminary',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (18,5,'Midterm',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (19,5,'Semi-Finals',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (20,5,'Finals',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (21,6,'Preliminary',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (22,6,'Midterm',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (23,6,'Semi-Finals',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (24,6,'Finals',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (25,7,'Preliminary',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (26,7,'Midterm',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (27,7,'Semi-Finals',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (28,7,'Finals',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (29,8,'Preliminary',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (30,8,'Midterm',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (31,8,'Semi-Finals',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (32,8,'Finals',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (33,9,'Preliminary',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (34,9,'Midterm',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (35,9,'Semi-Finals',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (36,9,'Finals',NULL,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (37,10,'Preliminary',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (38,10,'Midterm',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (39,10,'Semi-Finals',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (40,10,'Finals',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (41,11,'Preliminary',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (42,11,'Midterm',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (43,11,'Semi-Finals',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (44,11,'Finals',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (45,12,'Preliminary',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (46,12,'Midterm',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (47,12,'Semi-Finals',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (48,12,'Finals',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (49,13,'Preliminary',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (50,13,'Midterm',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (51,13,'Semi-Finals',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (52,13,'Finals',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (53,14,'Preliminary',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (54,14,'Midterm',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (55,14,'Semi-Finals',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (56,14,'Finals',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (57,15,'Preliminary',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (58,15,'Midterm',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (59,15,'Semi-Finals',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (60,15,'Finals',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (61,16,'Preliminary',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (62,16,'Midterm',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (63,16,'Semi-Finals',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (64,16,'Finals',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (65,17,'Preliminary',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (66,17,'Midterm',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (67,17,'Semi-Finals',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (68,17,'Finals',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (69,18,'Preliminary',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (70,18,'Midterm',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (71,18,'Semi-Finals',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (72,18,'Finals',NULL,0,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (73,19,'Preliminary',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (74,19,'Midterm',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (75,19,'Semi-Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (76,19,'Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (77,20,'Preliminary',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (78,20,'Midterm',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (79,20,'Semi-Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (80,20,'Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (81,21,'Preliminary',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (82,21,'Midterm',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (83,21,'Semi-Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (84,21,'Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (85,22,'Preliminary',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (86,22,'Midterm',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (87,22,'Semi-Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (88,22,'Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (89,23,'Preliminary',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (90,23,'Midterm',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (91,23,'Semi-Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (92,23,'Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (93,24,'Preliminary',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (94,24,'Midterm',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (95,24,'Semi-Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (96,24,'Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (97,25,'Preliminary',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (98,25,'Midterm',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (99,25,'Semi-Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (100,25,'Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (101,26,'Preliminary',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (102,26,'Midterm',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (103,26,'Semi-Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (104,26,'Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (105,27,'Preliminary',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (106,27,'Midterm',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (107,27,'Semi-Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (108,27,'Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (109,28,'Preliminary',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (110,28,'Midterm',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (111,28,'Semi-Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (112,28,'Finals',NULL,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (113,29,'Preliminary',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (114,29,'Midterm',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (115,29,'Semi-Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (116,29,'Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (117,30,'Preliminary',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (118,30,'Midterm',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (119,30,'Semi-Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (120,30,'Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (121,31,'Preliminary',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (122,31,'Midterm',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (123,31,'Semi-Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (124,31,'Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (125,32,'Preliminary',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (126,32,'Midterm',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (127,32,'Semi-Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (128,32,'Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (129,33,'Preliminary',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (130,33,'Midterm',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (131,33,'Semi-Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (132,33,'Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (133,34,'Preliminary',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (134,34,'Midterm',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (135,34,'Semi-Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (136,34,'Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (137,35,'Preliminary',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (138,35,'Midterm',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (139,35,'Semi-Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (140,35,'Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (141,36,'Preliminary',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (142,36,'Midterm',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (143,36,'Semi-Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (144,36,'Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (145,37,'Preliminary',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (146,37,'Midterm',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (147,37,'Semi-Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (148,37,'Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (149,38,'Preliminary',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (150,38,'Midterm',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (151,38,'Semi-Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (152,38,'Finals',NULL,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (153,39,'Preliminary',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (154,39,'Midterm',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (155,39,'Semi-Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (156,39,'Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (157,40,'Preliminary',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (158,40,'Midterm',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (159,40,'Semi-Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (160,40,'Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (161,41,'Preliminary',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (162,41,'Midterm',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (163,41,'Semi-Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (164,41,'Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (165,42,'Preliminary',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (166,42,'Midterm',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (167,42,'Semi-Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (168,42,'Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (169,43,'Preliminary',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (170,43,'Midterm',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (171,43,'Semi-Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (172,43,'Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (173,44,'Preliminary',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (174,44,'Midterm',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (175,44,'Semi-Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (176,44,'Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (177,45,'Preliminary',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (178,45,'Midterm',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (179,45,'Semi-Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (180,45,'Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (181,46,'Preliminary',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (182,46,'Midterm',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (183,46,'Semi-Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (184,46,'Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (185,47,'Preliminary',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (186,47,'Midterm',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (187,47,'Semi-Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (188,47,'Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (189,48,'Preliminary',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (190,48,'Midterm',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (191,48,'Semi-Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (192,48,'Finals',NULL,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (193,49,'Preliminary',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (194,49,'Midterm',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (195,49,'Semi-Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (196,49,'Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (197,50,'Preliminary',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (198,50,'Midterm',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (199,50,'Semi-Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (200,50,'Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (201,51,'Preliminary',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (202,51,'Midterm',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (203,51,'Semi-Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (204,51,'Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (205,52,'Preliminary',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (206,52,'Midterm',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (207,52,'Semi-Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (208,52,'Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (209,53,'Preliminary',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (210,53,'Midterm',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (211,53,'Semi-Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (212,53,'Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (213,54,'Preliminary',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (214,54,'Midterm',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (215,54,'Semi-Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (216,54,'Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (217,55,'Preliminary',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (218,55,'Midterm',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (219,55,'Semi-Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (220,55,'Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (221,56,'Preliminary',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (222,56,'Midterm',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (223,56,'Semi-Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (224,56,'Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (225,57,'Preliminary',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (226,57,'Midterm',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (227,57,'Semi-Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (228,57,'Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (229,58,'Preliminary',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (230,58,'Midterm',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (231,58,'Semi-Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `student_grade_categories`(`id`,`student_grade_id`,`category`,`value`,`editable`,`user_id`,`created_at`,`updated_at`) values (232,58,'Finals',NULL,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');

					/*Table structure for table `student_grade_files` */

					DROP TABLE IF EXISTS `student_grade_files`;

					CREATE TABLE `student_grade_files` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `user_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `enrollment_id` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

					/*Data for the table `student_grade_files` */

					insert  into `student_grade_files`(`id`,`user_id`,`created_at`,`updated_at`,`enrollment_id`) values (1,1,'2014-01-14 18:32:24','2014-01-14 18:32:24',1);
					insert  into `student_grade_files`(`id`,`user_id`,`created_at`,`updated_at`,`enrollment_id`) values (2,2,'2014-01-14 18:35:57','2014-01-14 18:35:57',2);
					insert  into `student_grade_files`(`id`,`user_id`,`created_at`,`updated_at`,`enrollment_id`) values (3,3,'2014-01-22 15:33:13','2014-01-22 15:33:13',3);
					insert  into `student_grade_files`(`id`,`user_id`,`created_at`,`updated_at`,`enrollment_id`) values (4,4,'2014-01-24 11:15:03','2014-01-24 11:15:03',4);
					insert  into `student_grade_files`(`id`,`user_id`,`created_at`,`updated_at`,`enrollment_id`) values (5,5,'2014-03-31 10:24:12','2014-03-31 10:24:12',5);
					insert  into `student_grade_files`(`id`,`user_id`,`created_at`,`updated_at`,`enrollment_id`) values (6,6,'2014-04-28 12:51:08','2014-04-28 12:51:08',6);

					/*Table structure for table `student_grades` */

					DROP TABLE IF EXISTS `student_grades`;

					CREATE TABLE `student_grades` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `student_grade_file_id` int(11) DEFAULT NULL,
					  `value` float DEFAULT NULL,
					  `category` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `subject_id` int(11) DEFAULT NULL,
					  `subjectid` int(11) DEFAULT NULL,
					  `remarks` varchar(255) DEFAULT NULL,
					  `studentsubject_id` int(11) DEFAULT NULL,
					  `enrollment_id` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

					/*Data for the table `student_grades` */

					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (1,1,NULL,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24',2,2,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (2,1,NULL,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24',3,3,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (3,1,NULL,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24',4,4,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (4,1,NULL,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24',5,5,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (5,1,NULL,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24',6,6,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (6,1,NULL,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24',7,7,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (7,1,NULL,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24',8,8,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (8,1,NULL,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24',9,9,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (9,1,NULL,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24',10,10,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (10,2,NULL,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57',2,2,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (11,2,NULL,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57',3,3,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (12,2,NULL,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57',4,4,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (13,2,NULL,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57',5,5,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (14,2,NULL,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57',6,6,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (15,2,NULL,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57',7,7,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (16,2,NULL,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57',8,8,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (17,2,NULL,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57',9,9,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (18,2,NULL,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57',10,10,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (19,3,NULL,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13',2,2,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (20,3,NULL,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13',3,3,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (21,3,NULL,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13',4,4,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (22,3,NULL,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13',5,5,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (23,3,NULL,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13',6,6,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (24,3,NULL,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13',7,7,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (25,3,NULL,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13',8,8,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (26,3,NULL,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13',9,9,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (27,3,NULL,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13',10,10,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (28,3,NULL,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13',11,11,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (29,4,NULL,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03',2,2,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (30,4,NULL,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03',3,3,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (31,4,NULL,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03',4,4,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (32,4,NULL,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03',5,5,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (33,4,NULL,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03',6,6,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (34,4,NULL,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03',7,7,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (35,4,NULL,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03',8,8,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (36,4,NULL,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03',9,9,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (37,4,NULL,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03',10,10,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (38,4,NULL,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03',11,11,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (39,5,NULL,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12',2,2,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (40,5,NULL,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12',3,3,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (41,5,NULL,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12',4,4,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (42,5,NULL,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12',5,5,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (43,5,NULL,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12',6,6,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (44,5,NULL,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12',7,7,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (45,5,NULL,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12',8,8,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (46,5,NULL,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12',9,9,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (47,5,NULL,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12',10,10,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (48,5,NULL,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12',11,11,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (49,6,NULL,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08',2,2,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (50,6,NULL,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08',3,3,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (51,6,NULL,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08',4,4,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (52,6,NULL,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08',5,5,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (53,6,NULL,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08',6,6,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (54,6,NULL,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08',7,7,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (55,6,NULL,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08',8,8,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (56,6,NULL,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08',9,9,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (57,6,NULL,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08',10,10,NULL,NULL,NULL);
					insert  into `student_grades`(`id`,`student_grade_file_id`,`value`,`category`,`created_at`,`updated_at`,`subject_id`,`subjectid`,`remarks`,`studentsubject_id`,`enrollment_id`) values (58,6,NULL,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08',11,11,NULL,NULL,NULL);

					/*Table structure for table `student_payment_breakdown` */

					DROP TABLE IF EXISTS `student_payment_breakdown`;

					CREATE TABLE `student_payment_breakdown` (
					  `spb_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
					  `spb_fee_name` varchar(45) NOT NULL DEFAULT '',
					  `spb_fee_rate` decimal(10,2) NOT NULL DEFAULT '0.00',
					  `spb_amount_paid` decimal(10,2) NOT NULL DEFAULT '0.00',
					  `spb_remaining_balance` decimal(10,2) NOT NULL DEFAULT '0.00',
					  `spb_is_tuition_fee` tinyint(1) NOT NULL DEFAULT '0',
					  `spb_payment_number` int(10) unsigned NOT NULL DEFAULT '0',
					  `spb_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
					  `spb_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
					  `spb_transacted_by` int(10) unsigned NOT NULL DEFAULT '0',
					  `spb_spr_id` int(10) unsigned NOT NULL DEFAULT '0',
					  `spb_stud_enrollment_id` int(10) unsigned NOT NULL DEFAULT '0',
					  `spb_is_misc_fee` tinyint(1) unsigned NOT NULL DEFAULT '0',
					  `spb_is_other_fee` tinyint(1) unsigned NOT NULL DEFAULT '0',
					  `spb_sef_id` int(10) unsigned NOT NULL DEFAULT '0',
					  `spb_is_elective_fee` tinyint(4) NOT NULL DEFAULT '0',
					  `spb_is_uniform_fee` tinyint(4) NOT NULL DEFAULT '0',
					  `spb_sy_id` int(11) NOT NULL DEFAULT '0',
					  `spb_is_cca_fee` tinyint(1) NOT NULL DEFAULT '0',
					  PRIMARY KEY (`spb_id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `student_payment_breakdown` */

					/*Table structure for table `student_payment_records` */

					DROP TABLE IF EXISTS `student_payment_records`;

					CREATE TABLE `student_payment_records` (
					  `spr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
					  `spr_or_no` varchar(45) NOT NULL,
					  `old_or_no` varchar(255) DEFAULT NULL,
					  `spr_remarks` text NOT NULL,
					  `spr_payment_date` date NOT NULL,
					  `spr_enrollment_id` int(10) unsigned NOT NULL,
					  `spr_gperiod_id` int(10) unsigned NOT NULL,
					  `spr_ammt_paid` decimal(10,2) NOT NULL,
					  `spr_created` datetime NOT NULL,
					  `spr_updated` datetime NOT NULL,
					  `spr_user_trans_id` int(10) unsigned NOT NULL,
					  `spr_user_trans_name` varchar(45) NOT NULL,
					  `spr_schoolyear_id` int(10) unsigned NOT NULL,
					  `spr_payment_number` int(10) unsigned NOT NULL DEFAULT '0',
					  `spr_is_deleted` tinyint(1) NOT NULL DEFAULT '0',
					  `spr_date_deleted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
					  `spr_deleted_by` int(10) unsigned NOT NULL DEFAULT '0',
					  `spr_for_the_month_of` varchar(140) DEFAULT NULL,
					  `spr_mode_of_payment` varchar(150) DEFAULT NULL,
					  `check_date` datetime DEFAULT NULL,
					  `check_number` varchar(100) DEFAULT NULL,
					  `check_confirm_date` datetime DEFAULT NULL,
					  `check_status` set('PENDING','CLEARED','DECLINED') DEFAULT 'PENDING',
					  `is_applied` tinyint(1) DEFAULT '0',
					  `check_confirm_userid` bigint(20) DEFAULT NULL,
					  `check_ids` varchar(255) DEFAULT NULL COMMENT 'HAS SUFFIX T - Tuition O - Others P - Previous',
					  `check_remarks` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`spr_id`)
					) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

					/*Data for the table `student_payment_records` */

					insert  into `student_payment_records`(`spr_id`,`spr_or_no`,`old_or_no`,`spr_remarks`,`spr_payment_date`,`spr_enrollment_id`,`spr_gperiod_id`,`spr_ammt_paid`,`spr_created`,`spr_updated`,`spr_user_trans_id`,`spr_user_trans_name`,`spr_schoolyear_id`,`spr_payment_number`,`spr_is_deleted`,`spr_date_deleted`,`spr_deleted_by`,`spr_for_the_month_of`,`spr_mode_of_payment`,`check_date`,`check_number`,`check_confirm_date`,`check_status`,`is_applied`,`check_confirm_userid`,`check_ids`,`check_remarks`,`created_at`,`updated_at`) values (1,'asdf3234',NULL,'sadfasdf','2014-07-01',7,1,4419.20,'0000-00-00 00:00:00','0000-00-00 00:00:00',1,'admin',2,0,0,'0000-00-00 00:00:00',0,NULL,'CASH',NULL,NULL,NULL,'PENDING',1,NULL,'T_31|',NULL,'2014-07-01 11:49:14','2014-07-01 11:49:15');

					/*Table structure for table `student_plan_modes` */

					DROP TABLE IF EXISTS `student_plan_modes`;

					CREATE TABLE `student_plan_modes` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `enrollment_id` bigint(20) NOT NULL,
					  `payment_plan_id` bigint(20) DEFAULT NULL,
					  `number` int(2) DEFAULT NULL,
					  `name` varchar(100) DEFAULT NULL,
					  `value` float DEFAULT NULL,
					  `amount_paid` float DEFAULT NULL,
					  `check_applied` float DEFAULT '0' COMMENT 'For Check Soon to Copy to Amount_Paid When Confirm',
					  `is_check` tinyint(1) DEFAULT '0' COMMENT 'When the applied Payment is Check',
					  `is_paid` tinyint(1) DEFAULT '0',
					  `spr_id` bigint(20) DEFAULT NULL COMMENT 'Student Payment Record ID',
					  `is_deleted` tinyint(1) DEFAULT '0' COMMENT 'When deleted value is 1',
					  `deleted_by` bigint(20) DEFAULT NULL COMMENT 'Delete by : User Id',
					  `deleted_date` datetime DEFAULT NULL COMMENT 'Date When Deleted',
					  `delete_remarks` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

					/*Data for the table `student_plan_modes` */

					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (1,7,1,1,'Registration',0,NULL,0,0,0,NULL,1,4,'2014-06-27 03:03:26','recompute fees','2014-06-20 19:52:36','2014-06-27 15:38:26');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (2,7,1,2,'2nd Payment',0,NULL,0,0,0,NULL,1,4,'2014-06-27 03:03:26','recompute fees','2014-06-20 19:52:36','2014-06-27 15:38:26');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (3,7,1,3,'3rd Payment',0,NULL,0,0,0,NULL,1,4,'2014-06-27 03:03:26','recompute fees','2014-06-20 19:52:36','2014-06-27 15:38:26');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (4,7,1,4,'4th Payment',0,NULL,0,0,0,NULL,1,4,'2014-06-27 03:03:26','recompute fees','2014-06-20 19:52:36','2014-06-27 15:38:26');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (5,7,1,5,'5th Payment',0,NULL,0,0,0,NULL,1,4,'2014-06-27 03:03:26','recompute fees','2014-06-20 19:52:36','2014-06-27 15:38:26');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (6,8,1,1,'Registration',3988,NULL,0,0,0,NULL,1,4,'2014-06-27 03:03:12','recompute fees','2014-06-21 16:00:47','2014-06-27 15:38:12');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (7,8,1,2,'2nd Payment',3988,NULL,0,0,0,NULL,1,4,'2014-06-27 03:03:12','recompute fees','2014-06-21 16:00:47','2014-06-27 15:38:12');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (8,8,1,3,'3rd Payment',3988,NULL,0,0,0,NULL,1,4,'2014-06-27 03:03:12','recompute fees','2014-06-21 16:00:47','2014-06-27 15:38:12');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (9,8,1,4,'4th Payment',3988,NULL,0,0,0,NULL,1,4,'2014-06-27 03:03:12','recompute fees','2014-06-21 16:00:47','2014-06-27 15:38:12');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (10,8,1,5,'5th Payment',3988,NULL,0,0,0,NULL,1,4,'2014-06-27 03:03:12','recompute fees','2014-06-21 16:00:47','2014-06-27 15:38:12');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (11,7,1,1,'Registration',3988,0,0,0,0,NULL,1,4,'2014-06-27 03:03:26','recompute fees','2014-06-21 16:19:51','2014-06-27 15:38:26');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (12,7,1,2,'2nd Payment',3988,0,0,0,0,NULL,1,4,'2014-06-27 03:03:26','recompute fees','2014-06-21 16:19:51','2014-06-27 15:38:26');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (13,7,1,3,'3rd Payment',3988,0,0,0,0,NULL,1,4,'2014-06-27 03:03:26','recompute fees','2014-06-21 16:19:51','2014-06-27 15:38:26');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (14,7,1,4,'4th Payment',3988,0,0,0,0,NULL,1,4,'2014-06-27 03:03:26','recompute fees','2014-06-21 16:19:51','2014-06-27 15:38:26');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (15,7,1,5,'5th Payment',3988,0,0,0,0,NULL,1,4,'2014-06-27 03:03:26','recompute fees','2014-06-21 16:19:51','2014-06-27 15:38:26');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (16,11,1,1,'Registration',4028,NULL,0,0,0,NULL,1,4,'2014-06-27 03:03:39','recompute fees','2014-06-26 23:12:29','2014-06-27 15:37:39');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (17,11,1,2,'2nd Payment',4028,NULL,0,0,0,NULL,1,4,'2014-06-27 03:03:39','recompute fees','2014-06-26 23:12:29','2014-06-27 15:37:39');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (18,11,1,3,'3rd Payment',4028,NULL,0,0,0,NULL,1,4,'2014-06-27 03:03:39','recompute fees','2014-06-26 23:12:29','2014-06-27 15:37:39');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (19,11,1,4,'4th Payment',4028,NULL,0,0,0,NULL,1,4,'2014-06-27 03:03:39','recompute fees','2014-06-26 23:12:29','2014-06-27 15:37:39');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (20,11,1,5,'5th Payment',4028,NULL,0,0,0,NULL,1,4,'2014-06-27 03:03:39','recompute fees','2014-06-26 23:12:29','2014-06-27 15:37:39');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (21,11,1,1,'Registration',4419.2,0,0,0,0,NULL,0,NULL,NULL,NULL,'2014-06-27 15:37:39','2014-06-27 15:37:39');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (22,11,1,2,'2nd Payment',4419.2,0,0,0,0,NULL,0,NULL,NULL,NULL,'2014-06-27 15:37:39','2014-06-27 15:37:39');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (23,11,1,3,'3rd Payment',4419.2,0,0,0,0,NULL,0,NULL,NULL,NULL,'2014-06-27 15:37:39','2014-06-27 15:37:39');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (24,11,1,4,'4th Payment',4419.2,0,0,0,0,NULL,0,NULL,NULL,NULL,'2014-06-27 15:37:39','2014-06-27 15:37:39');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (25,11,1,5,'5th Payment',4419.2,0,0,0,0,NULL,0,NULL,NULL,NULL,'2014-06-27 15:37:39','2014-06-27 15:37:39');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (26,8,1,1,'Registration',4419.2,0,0,0,0,NULL,0,NULL,NULL,NULL,'2014-06-27 15:38:12','2014-06-27 15:38:12');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (27,8,1,2,'2nd Payment',4419.2,0,0,0,0,NULL,0,NULL,NULL,NULL,'2014-06-27 15:38:12','2014-06-27 15:38:12');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (28,8,1,3,'3rd Payment',4419.2,0,0,0,0,NULL,0,NULL,NULL,NULL,'2014-06-27 15:38:12','2014-06-27 15:38:12');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (29,8,1,4,'4th Payment',4419.2,0,0,0,0,NULL,0,NULL,NULL,NULL,'2014-06-27 15:38:12','2014-06-27 15:38:12');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (30,8,1,5,'5th Payment',4419.2,0,0,0,0,NULL,0,NULL,NULL,NULL,'2014-06-27 15:38:12','2014-06-27 15:38:12');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (31,7,1,1,'Registration',4419.2,4419.2,0,0,1,1,0,NULL,NULL,NULL,'2014-06-27 15:38:26','2014-07-01 11:49:15');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (32,7,1,2,'2nd Payment',4419.2,0,0,0,0,NULL,0,NULL,NULL,NULL,'2014-06-27 15:38:26','2014-06-27 15:38:26');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (33,7,1,3,'3rd Payment',4419.2,0,0,0,0,NULL,0,NULL,NULL,NULL,'2014-06-27 15:38:26','2014-06-27 15:38:26');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (34,7,1,4,'4th Payment',4419.2,0,0,0,0,NULL,0,NULL,NULL,NULL,'2014-06-27 15:38:26','2014-06-27 15:38:26');
					insert  into `student_plan_modes`(`id`,`enrollment_id`,`payment_plan_id`,`number`,`name`,`value`,`amount_paid`,`check_applied`,`is_check`,`is_paid`,`spr_id`,`is_deleted`,`deleted_by`,`deleted_date`,`delete_remarks`,`created_at`,`updated_at`) values (35,7,1,5,'5th Payment',4419.2,0,0,0,0,NULL,0,NULL,NULL,NULL,'2014-06-27 15:38:26','2014-06-27 15:38:26');

					/*Table structure for table `student_previous_accounts` */

					DROP TABLE IF EXISTS `student_previous_accounts`;

					CREATE TABLE `student_previous_accounts` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `value` float DEFAULT NULL COMMENT 'Amount of the Plan',
					  `amount_paid` float DEFAULT NULL COMMENT 'Amount to be paid',
					  `is_paid` tinyint(1) DEFAULT '0' COMMENT 'Payment Status',
					  `spr_id` bigint(20) DEFAULT NULL COMMENT 'Student Payment Record ID',
					  `is_check` tinyint(1) DEFAULT NULL,
					  `check_applied` float DEFAULT '0',
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `enrollment_id` int(11) DEFAULT NULL COMMENT 'Enrollment ID',
					  `prev_enrollment_eid` bigint(20) DEFAULT NULL,
					  `is_deduction_paid` tinyint(1) DEFAULT '0' COMMENT 'If paid by deduction/scholarship/excess',
					  `is_deleted` tinyint(1) DEFAULT '0',
					  `deleted_by` varchar(100) DEFAULT NULL,
					  `date_deleted` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `student_previous_accounts` */

					/*Table structure for table `student_refund` */

					DROP TABLE IF EXISTS `student_refund`;

					CREATE TABLE `student_refund` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `enrollment_id` bigint(20) DEFAULT NULL,
					  `amount` float DEFAULT NULL,
					  `date` datetime DEFAULT NULL,
					  `remarks` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `student_refund` */

					/*Table structure for table `student_scholarship_record` */

					DROP TABLE IF EXISTS `student_scholarship_record`;

					CREATE TABLE `student_scholarship_record` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `enrollment_id` bigint(20) NOT NULL,
					  `stud_e_id` int(11) NOT NULL,
					  `scholarship_id` int(11) NOT NULL,
					  `academic_year_id` int(11) NOT NULL,
					  `scho_amount` float DEFAULT '0',
					  `tuit_per` int(11) NOT NULL DEFAULT '0',
					  `misc_per` int(11) NOT NULL DEFAULT '0',
					  `tuit_cash` decimal(10,2) NOT NULL,
					  `misc_cash` decimal(10,2) NOT NULL,
					  `scholarship_name` varchar(200) NOT NULL,
					  `scholarship_desc` text NOT NULL,
					  `original_tuit_rate` decimal(10,2) NOT NULL,
					  `original_misc_rate` decimal(10,2) NOT NULL,
					  `total_value_deducted` decimal(10,2) NOT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `student_scholarship_record` */

					/*Table structure for table `student_scholarships` */

					DROP TABLE IF EXISTS `student_scholarships`;

					CREATE TABLE `student_scholarships` (
					  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
					  `name` varchar(45) NOT NULL DEFAULT '',
					  `desc` text NOT NULL,
					  `type` set('CASH','PERCENTAGE') DEFAULT 'CASH',
					  `cash_amount` float DEFAULT '0',
					  `percentage` decimal(10,0) DEFAULT '0',
					  `applied_to` set('TUITION','MISC','TUITION & MISC') DEFAULT 'TUITION',
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `student_scholarships` */

					/*Table structure for table `student_subject_grades` */

					DROP TABLE IF EXISTS `student_subject_grades`;

					CREATE TABLE `student_subject_grades` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `enrollment_id` bigint(20) NOT NULL,
					  `subject_id` bigint(20) NOT NULL,
					  `gp_id` bigint(20) NOT NULL COMMENT 'Grading Period ID',
					  `value` float DEFAULT '0',
					  `letter` varchar(25) DEFAULT NULL,
					  `remarks` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=MyISAM DEFAULT CHARSET=utf8;

					/*Data for the table `student_subject_grades` */

					/*Table structure for table `student_total_file` */

					DROP TABLE IF EXISTS `student_total_file`;

					CREATE TABLE `student_total_file` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `enrollment_id` bigint(20) DEFAULT NULL,
					  `additional_charge` float DEFAULT '0',
					  `total_charge` float DEFAULT '0',
					  `prev_account` float DEFAULT '0',
					  `total_amount_due` float DEFAULT '0',
					  `total_deduction` float DEFAULT '0',
					  `less_deduction` float DEFAULT '0',
					  `total_payment` float DEFAULT '0',
					  `balance` float DEFAULT '0',
					  `status` varchar(30) DEFAULT NULL,
					  `coursefinance_id` bigint(20) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `subject_units` float DEFAULT '0',
					  `total_lab` float DEFAULT '0',
					  `total_lec` float DEFAULT '0',
					  `tuition_fee` float DEFAULT '0',
					  `total_tuition_fee` float DEFAULT '0',
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

					/*Data for the table `student_total_file` */

					insert  into `student_total_file`(`id`,`enrollment_id`,`additional_charge`,`total_charge`,`prev_account`,`total_amount_due`,`total_deduction`,`less_deduction`,`total_payment`,`balance`,`status`,`coursefinance_id`,`created_at`,`updated_at`,`subject_units`,`total_lab`,`total_lec`,`tuition_fee`,`total_tuition_fee`) values (1,1,0,7996,0,7996,0,0,0,7996,'UNPAID',2,'2014-01-14 18:32:24','2014-01-14 18:32:24',25,12,21,140,3500);
					insert  into `student_total_file`(`id`,`enrollment_id`,`additional_charge`,`total_charge`,`prev_account`,`total_amount_due`,`total_deduction`,`less_deduction`,`total_payment`,`balance`,`status`,`coursefinance_id`,`created_at`,`updated_at`,`subject_units`,`total_lab`,`total_lec`,`tuition_fee`,`total_tuition_fee`) values (3,3,0,17483,0,17483,0,0,0,17483,'UNPAID',2,'2014-01-22 15:33:13','2014-01-22 15:33:13',27,12,23,481,12987);
					insert  into `student_total_file`(`id`,`enrollment_id`,`additional_charge`,`total_charge`,`prev_account`,`total_amount_due`,`total_deduction`,`less_deduction`,`total_payment`,`balance`,`status`,`coursefinance_id`,`created_at`,`updated_at`,`subject_units`,`total_lab`,`total_lec`,`tuition_fee`,`total_tuition_fee`) values (4,4,0,19163,0,19163,3832.6,0,3832.6,15330.4,'PARTIALLY PAID',2,'2014-01-24 11:15:03','2014-01-24 11:15:42',27,12,23,481,12987);
					insert  into `student_total_file`(`id`,`enrollment_id`,`additional_charge`,`total_charge`,`prev_account`,`total_amount_due`,`total_deduction`,`less_deduction`,`total_payment`,`balance`,`status`,`coursefinance_id`,`created_at`,`updated_at`,`subject_units`,`total_lab`,`total_lec`,`tuition_fee`,`total_tuition_fee`) values (5,2,0,18201,0,18201,3640.2,0,3640.2,14560.8,'PARTIALLY PAID',2,'2014-01-29 13:25:04','2014-01-29 13:37:31',25,12,21,481,12025);
					insert  into `student_total_file`(`id`,`enrollment_id`,`additional_charge`,`total_charge`,`prev_account`,`total_amount_due`,`total_deduction`,`less_deduction`,`total_payment`,`balance`,`status`,`coursefinance_id`,`created_at`,`updated_at`,`subject_units`,`total_lab`,`total_lec`,`tuition_fee`,`total_tuition_fee`) values (6,5,0,19163,0,19163,3832.6,0,3832.6,15330.4,'PARTIALLY PAID',2,'2014-03-31 10:24:12','2014-03-31 10:27:47',27,12,23,481,12987);
					insert  into `student_total_file`(`id`,`enrollment_id`,`additional_charge`,`total_charge`,`prev_account`,`total_amount_due`,`total_deduction`,`less_deduction`,`total_payment`,`balance`,`status`,`coursefinance_id`,`created_at`,`updated_at`,`subject_units`,`total_lab`,`total_lec`,`tuition_fee`,`total_tuition_fee`) values (7,6,0,19163,0,19163,0,0,0,19163,'UNPAID',2,'2014-04-28 12:51:08','2014-04-28 12:51:08',27,12,23,481,12987);

					/*Table structure for table `student_totals` */

					DROP TABLE IF EXISTS `student_totals`;

					CREATE TABLE `student_totals` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `enrollment_id` int(11) DEFAULT NULL,
					  `total_units` decimal(10,2) NOT NULL DEFAULT '0.00',
					  `total_lab` decimal(10,2) NOT NULL DEFAULT '0.00',
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `tuition_fee_per_unit` decimal(8,2) NOT NULL DEFAULT '0.00',
					  `lab_fee_per_unit` decimal(8,2) NOT NULL DEFAULT '0.00',
					  `total_misc_fee` decimal(8,2) NOT NULL DEFAULT '0.00',
					  `total_other_fee` decimal(8,2) NOT NULL DEFAULT '0.00',
					  `tuition_fee` decimal(8,2) NOT NULL DEFAULT '0.00',
					  `lab_fee` decimal(8,2) NOT NULL DEFAULT '0.00',
					  `less_nstp` decimal(8,2) NOT NULL DEFAULT '0.00',
					  `additional_charge` decimal(8,2) NOT NULL DEFAULT '0.00',
					  `total_charge` decimal(8,2) NOT NULL DEFAULT '0.00',
					  `previous_account` decimal(8,2) NOT NULL DEFAULT '0.00',
					  `total_deduction` decimal(8,2) DEFAULT '0.00',
					  `total_amount` decimal(8,2) NOT NULL DEFAULT '0.00',
					  `total_payment` decimal(8,2) NOT NULL DEFAULT '0.00',
					  `remaining_balance` decimal(8,2) NOT NULL DEFAULT '0.00',
					  `total_rle_units` decimal(10,1) NOT NULL DEFAULT '0.0',
					  `total_rle` decimal(10,2) NOT NULL DEFAULT '0.00',
					  `rle_fee` decimal(8,2) NOT NULL DEFAULT '0.00',
					  `preliminary` decimal(8,2) NOT NULL DEFAULT '0.00',
					  `midterm` decimal(8,2) NOT NULL DEFAULT '0.00',
					  `semi_finals` decimal(8,2) NOT NULL DEFAULT '0.00',
					  `finals` decimal(8,2) NOT NULL DEFAULT '0.00',
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `student_totals` */

					/*Table structure for table `studentadvances` */

					DROP TABLE IF EXISTS `studentadvances`;

					CREATE TABLE `studentadvances` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `value` int(11) DEFAULT NULL,
					  `studentfinance_id` int(11) DEFAULT NULL,
					  `student_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `studentadvances` */

					/*Table structure for table `studentbalances` */

					DROP TABLE IF EXISTS `studentbalances`;

					CREATE TABLE `studentbalances` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `value` float DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `studentbalances` */

					/*Table structure for table `studentdownpayments` */

					DROP TABLE IF EXISTS `studentdownpayments`;

					CREATE TABLE `studentdownpayments` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `value` float(10,2) DEFAULT NULL,
					  `studentfinance_id` int(11) DEFAULT NULL,
					  `student_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `date_of_payment` datetime DEFAULT NULL,
					  `is_advance` varchar(255) DEFAULT NULL,
					  `remarks` text,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `studentdownpayments` */

					/*Table structure for table `studentexcess_prev_sem` */

					DROP TABLE IF EXISTS `studentexcess_prev_sem`;

					CREATE TABLE `studentexcess_prev_sem` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `enrollment_id` bigint(20) DEFAULT NULL,
					  `date` datetime DEFAULT NULL,
					  `amount` float DEFAULT '0',
					  `prev_sem_eid` bigint(20) DEFAULT NULL,
					  `remarks` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `is_deleted` tinyint(1) DEFAULT '0' COMMENT 'when deleted value is 1',
					  `deleted_by` varchar(100) DEFAULT NULL COMMENT 'user id of logged user',
					  `deleted_date` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `studentexcess_prev_sem` */

					/*Table structure for table `studentfees` */

					DROP TABLE IF EXISTS `studentfees`;

					CREATE TABLE `studentfees` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `enrollment_id` bigint(20) NOT NULL,
					  `student_id` int(11) DEFAULT NULL,
					  `studentfinance_id` int(11) DEFAULT NULL,
					  `fee_id` int(11) DEFAULT NULL,
					  `value` float(10,2) DEFAULT NULL,
					  `position` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8;

					/*Data for the table `studentfees` */

					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (1,0,5,1,1,481.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (2,0,5,1,2,72.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (3,0,5,1,3,347.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (4,0,5,1,4,335.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (5,0,5,1,5,510.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (6,0,5,1,6,722.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (7,0,5,1,7,189.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (8,0,5,1,9,189.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (9,0,5,1,10,685.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (10,0,5,1,11,246.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (11,0,5,1,12,100.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (12,0,5,1,13,1.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (13,0,5,1,14,90.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (14,0,5,1,15,192.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (15,0,5,1,16,290.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (16,0,5,1,17,78.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (17,0,5,1,18,140.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (18,0,5,1,19,200.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (19,0,5,1,20,45.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (20,0,5,1,21,92.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (21,0,5,1,22,113.00,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (22,0,6,2,1,481.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (23,0,6,2,2,72.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (24,0,6,2,3,347.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (25,0,6,2,4,335.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (26,0,6,2,5,510.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (27,0,6,2,6,722.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (28,0,6,2,7,189.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (29,0,6,2,9,189.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (30,0,6,2,10,685.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (31,0,6,2,11,246.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (32,0,6,2,12,100.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (33,0,6,2,13,1.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (34,0,6,2,14,90.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (35,0,6,2,15,192.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (36,0,6,2,16,290.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (37,0,6,2,17,78.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (38,0,6,2,18,140.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (39,0,6,2,19,200.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (40,0,6,2,20,45.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (41,0,6,2,21,92.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (42,0,6,2,22,113.00,NULL,'2014-01-14 18:35:57','2014-01-14 18:35:57');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (43,0,8,3,1,481.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (44,0,8,3,2,72.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (45,0,8,3,3,347.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (46,0,8,3,4,335.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (47,0,8,3,5,510.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (48,0,8,3,6,722.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (49,0,8,3,7,189.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (50,0,8,3,9,189.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (51,0,8,3,10,685.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (52,0,8,3,11,246.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (53,0,8,3,12,100.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (54,0,8,3,13,1.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (55,0,8,3,14,90.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (56,0,8,3,15,192.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (57,0,8,3,16,290.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (58,0,8,3,17,78.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (59,0,8,3,18,140.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (60,0,8,3,19,200.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (61,0,8,3,20,45.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (62,0,8,3,21,92.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (63,0,8,3,22,113.00,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (64,0,9,4,1,481.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (65,0,9,4,2,72.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (66,0,9,4,3,347.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (67,0,9,4,4,335.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (68,0,9,4,5,510.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (69,0,9,4,6,722.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (70,0,9,4,7,189.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (71,0,9,4,9,189.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (72,0,9,4,10,685.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (73,0,9,4,11,246.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (74,0,9,4,12,100.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (75,0,9,4,13,1.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (76,0,9,4,14,90.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (77,0,9,4,15,192.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (78,0,9,4,16,290.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (79,0,9,4,17,78.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (80,0,9,4,18,140.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (81,0,9,4,19,200.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (82,0,9,4,20,45.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (83,0,9,4,21,92.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (84,0,9,4,22,113.00,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (85,0,13,18,1,481.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (86,0,13,18,2,72.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (87,0,13,18,3,347.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (88,0,13,18,4,335.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (89,0,13,18,5,510.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (90,0,13,18,6,722.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (91,0,13,18,7,189.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (92,0,13,18,9,189.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (93,0,13,18,10,685.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (94,0,13,18,11,246.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (95,0,13,18,12,100.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (96,0,13,18,13,1.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (97,0,13,18,14,90.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (98,0,13,18,15,192.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (99,0,13,18,16,290.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (100,0,13,18,17,78.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (101,0,13,18,18,140.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (102,0,13,18,19,200.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (103,0,13,18,20,45.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (104,0,13,18,21,92.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (105,0,13,18,22,113.00,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (106,0,16,20,1,481.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (107,0,16,20,2,72.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (108,0,16,20,3,347.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (109,0,16,20,4,335.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (110,0,16,20,5,510.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (111,0,16,20,6,722.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (112,0,16,20,7,189.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (113,0,16,20,9,189.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (114,0,16,20,10,685.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (115,0,16,20,11,246.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (116,0,16,20,12,100.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (117,0,16,20,13,1.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (118,0,16,20,14,90.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (119,0,16,20,15,192.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (120,0,16,20,16,290.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (121,0,16,20,17,78.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (122,0,16,20,18,140.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (123,0,16,20,19,200.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (124,0,16,20,20,45.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (125,0,16,20,21,92.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');
					insert  into `studentfees`(`id`,`enrollment_id`,`student_id`,`studentfinance_id`,`fee_id`,`value`,`position`,`created_at`,`updated_at`) values (126,0,16,20,22,113.00,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08');

					/*Table structure for table `studentfinances` */

					DROP TABLE IF EXISTS `studentfinances`;

					CREATE TABLE `studentfinances` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `coursefinance_id` int(11) DEFAULT NULL,
					  `student_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `year_id` int(11) DEFAULT NULL,
					  `semester_id` int(11) DEFAULT NULL,
					  `course_id` int(11) DEFAULT NULL,
					  `discount` float(11,2) DEFAULT '0.00',
					  `total` float(11,2) DEFAULT '0.00',
					  `less` float(11,2) DEFAULT '0.00',
					  `net` float(11,2) DEFAULT '0.00',
					  `status` tinyint(1) NOT NULL DEFAULT '0',
					  `total_unit_fee` float DEFAULT NULL,
					  `total_lab_fee` float DEFAULT NULL,
					  `account_recivable_student` float DEFAULT NULL,
					  `old_account` float DEFAULT NULL,
					  `other` float DEFAULT NULL,
					  `enrollmentid` int(11) DEFAULT NULL,
					  `type` varchar(30) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

					/*Data for the table `studentfinances` */

					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (1,2,5,'2014-01-14 18:32:24','2014-01-14 18:32:24',3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,1,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (2,2,6,'2014-01-14 18:35:57','2014-01-14 18:35:57',3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,2,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (3,2,8,'2014-01-22 15:33:13','2014-01-22 15:33:13',3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,3,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (4,2,9,'2014-01-24 11:15:03','2014-01-24 11:15:03',3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,4,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (5,2,8,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,3,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (6,2,6,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,2,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (7,2,9,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,4,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (8,2,5,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,1,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (9,2,6,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,2,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (10,2,8,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,3,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (11,2,6,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,2,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (12,2,9,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,4,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (13,2,5,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,1,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (14,2,8,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,3,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (15,2,6,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,2,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (16,2,9,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,4,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (17,2,5,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,1,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (18,2,13,'2014-03-31 10:24:12','2014-03-31 10:24:12',3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,5,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (19,2,13,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,5,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (20,2,16,'2014-04-28 12:51:08','2014-04-28 12:51:08',3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,6,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (21,2,16,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,6,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (22,2,8,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,3,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (23,2,6,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,2,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (24,2,9,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,4,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (25,2,5,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,1,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (26,2,13,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,5,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (27,2,16,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,6,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (28,2,8,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,3,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (29,2,6,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,2,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (30,2,9,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,4,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (31,2,5,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,1,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (32,2,13,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,5,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (33,2,16,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,6,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (34,2,8,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,3,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (35,2,6,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,2,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (36,2,9,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,4,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (37,2,5,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,1,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (38,2,13,NULL,NULL,3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,5,NULL);
					insert  into `studentfinances`(`id`,`coursefinance_id`,`student_id`,`created_at`,`updated_at`,`year_id`,`semester_id`,`course_id`,`discount`,`total`,`less`,`net`,`status`,`total_unit_fee`,`total_lab_fee`,`account_recivable_student`,`old_account`,`other`,`enrollmentid`,`type`) values (39,2,17,'2014-06-20 19:52:36','2014-06-20 19:52:36',3,1,1,0.00,0.00,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,7,NULL);

					/*Table structure for table `studentpayments` */

					DROP TABLE IF EXISTS `studentpayments`;

					CREATE TABLE `studentpayments` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `value` decimal(11,2) DEFAULT '0.00',
					  `date` datetime DEFAULT NULL,
					  `remarks` text,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `studentfinance_id` int(11) DEFAULT NULL,
					  `student_id` int(11) DEFAULT NULL,
					  `category` varchar(255) DEFAULT NULL,
					  `account_recivable_student` decimal(11,2) DEFAULT '0.00',
					  `old_account` decimal(11,2) DEFAULT '0.00',
					  `other` float(11,2) DEFAULT '0.00',
					  `total` decimal(11,2) DEFAULT '0.00',
					  `enrollmentid` int(11) DEFAULT NULL,
					  `or_no` varchar(255) DEFAULT NULL,
					  `group_or_no` varchar(255) DEFAULT NULL,
					  `is_excess_payment` tinyint(1) DEFAULT '0',
					  `excess_enrollment_id` bigint(20) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

					/*Data for the table `studentpayments` */

					insert  into `studentpayments`(`id`,`value`,`date`,`remarks`,`created_at`,`updated_at`,`studentfinance_id`,`student_id`,`category`,`account_recivable_student`,`old_account`,`other`,`total`,`enrollmentid`,`or_no`,`group_or_no`,`is_excess_payment`,`excess_enrollment_id`) values (1,0.00,'2014-01-24 00:00:00','','2014-01-24 11:15:42','2014-01-24 11:15:42',4,NULL,NULL,3832.60,0.00,0.00,3832.60,4,'OR-123','',0,NULL);
					insert  into `studentpayments`(`id`,`value`,`date`,`remarks`,`created_at`,`updated_at`,`studentfinance_id`,`student_id`,`category`,`account_recivable_student`,`old_account`,`other`,`total`,`enrollmentid`,`or_no`,`group_or_no`,`is_excess_payment`,`excess_enrollment_id`) values (2,0.00,'2014-01-29 00:00:00','downpayment','2014-01-29 13:37:31','2014-01-29 13:37:31',2,NULL,NULL,3640.20,0.00,0.00,3640.20,2,'1234561','',0,NULL);
					insert  into `studentpayments`(`id`,`value`,`date`,`remarks`,`created_at`,`updated_at`,`studentfinance_id`,`student_id`,`category`,`account_recivable_student`,`old_account`,`other`,`total`,`enrollmentid`,`or_no`,`group_or_no`,`is_excess_payment`,`excess_enrollment_id`) values (3,0.00,'2014-03-31 00:00:00','asdfasdfasd','2014-03-31 10:27:47','2014-03-31 10:27:47',18,NULL,NULL,3832.60,0.00,0.00,3832.60,5,'or123sad','a',0,NULL);

					/*Table structure for table `studentpayments_details` */

					DROP TABLE IF EXISTS `studentpayments_details`;

					CREATE TABLE `studentpayments_details` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `enrollment_id` bigint(20) DEFAULT NULL,
					  `is_paid` tinyint(1) DEFAULT '0',
					  `amount` float DEFAULT NULL,
					  `amount_paid` float DEFAULT NULL,
					  `balance` float DEFAULT NULL,
					  `is_promisory` tinyint(1) DEFAULT '0',
					  `studentpromisory_id` bigint(20) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `date_applied` datetime DEFAULT NULL,
					  `is_current` tinyint(1) DEFAULT '0',
					  `is_downpayment` tinyint(1) DEFAULT '0',
					  `grading_period_id` varchar(20) DEFAULT NULL,
					  `grant_userid` bigint(20) DEFAULT NULL,
					  `number` int(11) DEFAULT '0',
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

					/*Data for the table `studentpayments_details` */

					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (1,1,0,1599.2,0,1599.2,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24',NULL,0,0,NULL,NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (2,1,0,1599.2,0,1599.2,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24',NULL,0,0,NULL,NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (3,1,0,1599.2,0,1599.2,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24',NULL,0,0,NULL,NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (4,1,0,1599.2,0,1599.2,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24',NULL,0,0,NULL,NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (5,1,0,1599.2,0,1599.2,0,NULL,'2014-01-14 18:32:24','2014-01-14 18:32:24',NULL,0,0,NULL,NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (6,2,1,3640.2,3640.2,0,0,NULL,'2014-01-14 18:35:57','2014-01-29 13:37:31','2014-01-29 13:37:31',0,1,'DOWNPAYMENT',NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (7,2,0,3640.2,0,3640.2,0,NULL,'2014-01-14 18:35:57','2014-01-29 13:25:04',NULL,0,0,'1',NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (8,2,0,3640.2,0,3640.2,0,NULL,'2014-01-14 18:35:57','2014-01-29 13:25:04',NULL,0,0,'2',NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (9,2,0,3640.2,0,3640.2,0,NULL,'2014-01-14 18:35:57','2014-01-29 13:25:04',NULL,0,0,'3',NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (10,2,0,3640.2,0,3640.2,0,NULL,'2014-01-14 18:35:57','2014-01-29 13:25:04',NULL,0,0,'4',NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (11,3,0,17483,0,17483,0,NULL,'2014-01-22 15:33:13','2014-01-22 15:33:13',NULL,0,0,'CASH/FULLPAYMENT',NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (12,4,1,3832.6,3832.6,0,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:42','2014-01-24 11:15:42',0,1,'DOWNPAYMENT',NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (13,4,0,3832.6,0,3832.6,1,NULL,'2014-01-24 11:15:03','2014-01-30 18:23:09',NULL,0,0,'1',11,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (14,4,0,3832.6,0,3832.6,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03',NULL,0,0,'2',NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (15,4,0,3832.6,0,3832.6,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03',NULL,0,0,'3',NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (16,4,0,3832.6,0,3832.6,0,NULL,'2014-01-24 11:15:03','2014-01-24 11:15:03',NULL,0,0,'4',NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (17,5,1,3832.6,3832.6,0,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:27:47','2014-03-31 10:27:47',0,1,'DOWNPAYMENT',NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (18,5,0,3832.6,0,3832.6,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12',NULL,1,0,'1',NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (19,5,0,3832.6,0,3832.6,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12',NULL,0,0,'2',NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (20,5,0,3832.6,0,3832.6,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12',NULL,0,0,'3',NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (21,5,0,3832.6,0,3832.6,0,NULL,'2014-03-31 10:24:12','2014-03-31 10:24:12',NULL,0,0,'4',NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (22,6,0,3832.6,0,3832.6,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08',NULL,1,1,'DOWNPAYMENT',NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (23,6,0,3832.6,0,3832.6,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08',NULL,0,0,'1',NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (24,6,0,3832.6,0,3832.6,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08',NULL,0,0,'2',NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (25,6,0,3832.6,0,3832.6,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08',NULL,0,0,'3',NULL,0);
					insert  into `studentpayments_details`(`id`,`enrollment_id`,`is_paid`,`amount`,`amount_paid`,`balance`,`is_promisory`,`studentpromisory_id`,`created_at`,`updated_at`,`date_applied`,`is_current`,`is_downpayment`,`grading_period_id`,`grant_userid`,`number`) values (26,6,0,3832.6,0,3832.6,0,NULL,'2014-04-28 12:51:08','2014-04-28 12:51:08',NULL,0,0,'4',NULL,0);

					/*Table structure for table `studentpromisory` */

					DROP TABLE IF EXISTS `studentpromisory`;

					CREATE TABLE `studentpromisory` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `studentpayment_details_id` bigint(20) DEFAULT NULL,
					  `enrollment_id` bigint(20) DEFAULT NULL,
					  `date` datetime DEFAULT NULL,
					  `promisory` text,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `studentpromisory` */

					/*Table structure for table `studentregistrations` */

					DROP TABLE IF EXISTS `studentregistrations`;

					CREATE TABLE `studentregistrations` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `value` float(10,2) DEFAULT '0.00',
					  `date` datetime DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `studentfinance_id` int(11) DEFAULT NULL,
					  `student_id` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `studentregistrations` */

					/*Table structure for table `students` */

					DROP TABLE IF EXISTS `students`;

					CREATE TABLE `students` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `login` varchar(40) DEFAULT NULL,
					  `name` varchar(100) DEFAULT '',
					  `email` varchar(100) DEFAULT NULL,
					  `crypted_password` varchar(40) DEFAULT NULL,
					  `salt` varchar(40) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `remember_token` varchar(40) DEFAULT NULL,
					  `remember_token_expires_at` datetime DEFAULT NULL,
					  `activation_code` varchar(40) DEFAULT NULL,
					  `activated_at` datetime DEFAULT NULL,
					  `firstname` varchar(255) DEFAULT NULL,
					  `lastname` varchar(255) DEFAULT NULL,
					  `age` int(11) DEFAULT NULL,
					  `telephone` int(11) DEFAULT NULL,
					  `address` varchar(255) DEFAULT NULL,
					  `mobile` int(11) DEFAULT NULL,
					  `enrollment_id` int(11) DEFAULT NULL,
					  `sex` varchar(255) DEFAULT NULL,
					  `middle` varchar(255) DEFAULT NULL,
					  `date_of_birth` date DEFAULT NULL,
					  `place_of_birth` varchar(255) DEFAULT NULL,
					  `civil_status` varchar(255) DEFAULT NULL,
					  `guardian` varchar(255) DEFAULT NULL,
					  `occupation` varchar(255) DEFAULT NULL,
					  `residence_address` varchar(255) DEFAULT NULL,
					  `business_address` varchar(255) DEFAULT NULL,
					  `education_parent` varchar(255) DEFAULT NULL,
					  `no_family` varchar(255) DEFAULT NULL,
					  `occupation_parent` varchar(255) DEFAULT NULL,
					  `income` varchar(255) DEFAULT NULL,
					  `student_number` varchar(255) DEFAULT NULL,
					  `primary` varchar(255) DEFAULT NULL,
					  `primary_date` varchar(255) DEFAULT NULL,
					  `intermediate` varchar(255) DEFAULT NULL,
					  `intermediate_date` varchar(255) DEFAULT NULL,
					  `secondary` varchar(255) DEFAULT NULL,
					  `secondary_date` varchar(255) DEFAULT NULL,
					  `college` varchar(255) DEFAULT NULL,
					  `college_date` varchar(255) DEFAULT NULL,
					  `course_completed` varchar(255) DEFAULT NULL,
					  `course_completed_date` varchar(255) DEFAULT NULL,
					  `studid` varchar(255) DEFAULT NULL,
					  `fake_email` varchar(255) DEFAULT NULL,
					  `old_or_new` varchar(255) DEFAULT NULL,
					  `scholar` varchar(255) DEFAULT NULL,
					  PRIMARY KEY (`id`),
					  UNIQUE KEY `index_students_on_login` (`login`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `students` */

					/*Table structure for table `studentsts` */

					DROP TABLE IF EXISTS `studentsts`;

					CREATE TABLE `studentsts` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `enrollment_id` int(11) DEFAULT NULL,
					  `subject_id` int(11) DEFAULT NULL,
					  `sts_id` int(11) NOT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `studentsubject_id` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `studentsts` */

					/*Table structure for table `studentsubjects` */

					DROP TABLE IF EXISTS `studentsubjects`;

					CREATE TABLE `studentsubjects` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `enrollment_id` int(11) DEFAULT NULL COMMENT 'USERS ID',
					  `subject_id` int(11) DEFAULT NULL,
					  `year_id` int(11) DEFAULT NULL,
					  `semester_id` int(11) DEFAULT NULL,
					  `course_id` int(11) DEFAULT NULL,
					  `is_active` tinyint(1) DEFAULT NULL,
					  `enrollmentid` int(11) DEFAULT NULL COMMENT 'ENROLLMENT ID',
					  `block_system_setting_id` bigint(20) DEFAULT NULL,
					  `is_deducted` smallint(6) DEFAULT NULL,
					  `preliminary` decimal(11,2) NOT NULL DEFAULT '0.00',
					  `midterm` decimal(11,2) NOT NULL DEFAULT '0.00',
					  `semi_finals` decimal(11,2) NOT NULL DEFAULT '0.00',
					  `finals` decimal(11,2) NOT NULL DEFAULT '0.00',
					  `re_exam` decimal(11,2) NOT NULL DEFAULT '0.00',
					  `remidial` decimal(11,2) NOT NULL DEFAULT '0.00',
					  `remarks` varchar(255) DEFAULT NULL,
					  `date_dropped` date DEFAULT NULL,
					  `rating` decimal(11,2) NOT NULL DEFAULT '0.00',
					  `average` decimal(11,2) NOT NULL DEFAULT '0.00',
					  `user_id` bigint(20) DEFAULT NULL,
					  `is_deleted` tinyint(1) DEFAULT '0' COMMENT 'When deleted value is 1',
					  `deleted_by` varchar(20) DEFAULT NULL COMMENT 'User Id of the one who delete',
					  `delete_date` datetime DEFAULT NULL,
					  `is_drop` tinyint(1) DEFAULT '0' COMMENT 'When dropped value is 1',
					  `dropped_by` varchar(20) DEFAULT NULL COMMENT 'User Id of the ony who dropped',
					  `dropped_date` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

					/*Data for the table `studentsubjects` */

					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (1,'2014-01-14 18:32:24','2014-01-14 18:32:24',NULL,2,3,1,1,NULL,1,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (2,'2014-01-14 18:32:24','2014-01-14 18:32:24',NULL,3,3,1,1,NULL,1,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (3,'2014-01-14 18:32:24','2014-01-14 18:32:24',NULL,4,3,1,1,NULL,1,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (4,'2014-01-14 18:32:24','2014-01-14 18:32:24',NULL,5,3,1,1,NULL,1,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (5,'2014-01-14 18:32:24','2014-01-14 18:32:24',NULL,6,3,1,1,NULL,1,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (6,'2014-01-14 18:32:24','2014-01-14 18:32:24',NULL,7,3,1,1,NULL,1,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (7,'2014-01-14 18:32:24','2014-01-14 18:32:24',NULL,8,3,1,1,NULL,1,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (8,'2014-01-14 18:32:24','2014-01-14 18:32:24',NULL,9,3,1,1,NULL,1,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (9,'2014-01-14 18:32:24','2014-01-14 18:32:24',NULL,10,3,1,1,NULL,1,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (10,'2014-01-14 18:35:57','2014-01-14 18:35:57',NULL,2,3,1,1,NULL,2,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (11,'2014-01-14 18:35:57','2014-01-14 18:35:57',NULL,3,3,1,1,NULL,2,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (12,'2014-01-14 18:35:57','2014-01-14 18:35:57',NULL,4,3,1,1,NULL,2,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (13,'2014-01-14 18:35:57','2014-01-14 18:35:57',NULL,5,3,1,1,NULL,2,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (14,'2014-01-14 18:35:57','2014-01-14 18:35:57',NULL,6,3,1,1,NULL,2,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (15,'2014-01-14 18:35:57','2014-01-14 18:35:57',NULL,7,3,1,1,NULL,2,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (16,'2014-01-14 18:35:57','2014-01-14 18:35:57',NULL,8,3,1,1,NULL,2,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (17,'2014-01-14 18:35:57','2014-01-14 18:35:57',NULL,9,3,1,1,NULL,2,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (18,'2014-01-14 18:35:57','2014-01-14 18:35:57',NULL,10,3,1,1,NULL,2,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (19,'2014-01-22 15:33:13','2014-01-22 15:33:13',NULL,2,3,1,1,NULL,3,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (20,'2014-01-22 15:33:13','2014-01-22 15:33:13',NULL,3,3,1,1,NULL,3,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (21,'2014-01-22 15:33:13','2014-01-22 15:33:13',NULL,4,3,1,1,NULL,3,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (22,'2014-01-22 15:33:13','2014-01-22 15:33:13',NULL,5,3,1,1,NULL,3,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (23,'2014-01-22 15:33:13','2014-01-22 15:33:13',NULL,6,3,1,1,NULL,3,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (24,'2014-01-22 15:33:13','2014-01-22 15:33:13',NULL,7,3,1,1,NULL,3,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (25,'2014-01-22 15:33:13','2014-01-22 15:33:13',NULL,8,3,1,1,NULL,3,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (26,'2014-01-22 15:33:13','2014-01-22 15:33:13',NULL,9,3,1,1,NULL,3,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (27,'2014-01-22 15:33:13','2014-01-22 15:33:13',NULL,10,3,1,1,NULL,3,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (28,'2014-01-22 15:33:13','2014-01-22 15:33:13',NULL,11,3,1,1,NULL,3,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (29,'2014-01-24 11:15:03','2014-01-24 11:15:03',NULL,2,3,1,1,NULL,4,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (30,'2014-01-24 11:15:03','2014-01-24 11:15:03',NULL,3,3,1,1,NULL,4,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (31,'2014-01-24 11:15:03','2014-01-24 11:15:03',NULL,4,3,1,1,NULL,4,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (32,'2014-01-24 11:15:03','2014-01-24 11:15:03',NULL,5,3,1,1,NULL,4,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (33,'2014-01-24 11:15:03','2014-01-24 11:15:03',NULL,6,3,1,1,NULL,4,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (34,'2014-01-24 11:15:03','2014-01-24 11:15:03',NULL,7,3,1,1,NULL,4,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (35,'2014-01-24 11:15:03','2014-01-24 11:15:03',NULL,8,3,1,1,NULL,4,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (36,'2014-01-24 11:15:03','2014-01-24 11:15:03',NULL,9,3,1,1,NULL,4,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (37,'2014-01-24 11:15:03','2014-01-24 11:15:03',NULL,10,3,1,1,NULL,4,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (38,'2014-01-24 11:15:03','2014-01-24 11:15:03',NULL,11,3,1,1,NULL,4,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (39,'2014-03-31 10:24:12','2014-03-31 10:24:12',NULL,2,3,1,1,NULL,5,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (40,'2014-03-31 10:24:12','2014-03-31 10:24:12',NULL,3,3,1,1,NULL,5,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (41,'2014-03-31 10:24:12','2014-03-31 10:24:12',NULL,4,3,1,1,NULL,5,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (42,'2014-03-31 10:24:12','2014-03-31 10:24:12',NULL,5,3,1,1,NULL,5,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (43,'2014-03-31 10:24:12','2014-03-31 10:24:12',NULL,6,3,1,1,NULL,5,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (44,'2014-03-31 10:24:12','2014-03-31 10:24:12',NULL,7,3,1,1,NULL,5,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (45,'2014-03-31 10:24:12','2014-03-31 10:24:12',NULL,8,3,1,1,NULL,5,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (46,'2014-03-31 10:24:12','2014-03-31 10:24:12',NULL,9,3,1,1,NULL,5,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (47,'2014-03-31 10:24:12','2014-03-31 10:24:12',NULL,10,3,1,1,NULL,5,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (48,'2014-03-31 10:24:12','2014-03-31 10:24:12',NULL,11,3,1,1,NULL,5,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (49,'2014-04-28 12:51:08','2014-04-28 12:51:08',NULL,2,3,1,1,NULL,6,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (50,'2014-04-28 12:51:08','2014-04-28 12:51:08',NULL,3,3,1,1,NULL,6,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (51,'2014-04-28 12:51:08','2014-04-28 12:51:08',NULL,4,3,1,1,NULL,6,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (52,'2014-04-28 12:51:08','2014-04-28 12:51:08',NULL,5,3,1,1,NULL,6,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (53,'2014-04-28 12:51:08','2014-04-28 12:51:08',NULL,6,3,1,1,NULL,6,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (54,'2014-04-28 12:51:08','2014-04-28 12:51:08',NULL,7,3,1,1,NULL,6,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (55,'2014-04-28 12:51:08','2014-04-28 12:51:08',NULL,8,3,1,1,NULL,6,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (56,'2014-04-28 12:51:08','2014-04-28 12:51:08',NULL,9,3,1,1,NULL,6,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (57,'2014-04-28 12:51:08','2014-04-28 12:51:08',NULL,10,3,1,1,NULL,6,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (58,'2014-04-28 12:51:08','2014-04-28 12:51:08',NULL,11,3,1,1,NULL,6,1,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (59,'2014-06-20 19:52:36','2014-06-20 19:52:36',7,2,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (60,'2014-06-20 19:52:36','2014-06-20 19:52:36',7,3,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (61,'2014-06-20 19:52:36','2014-06-20 19:52:36',7,4,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (62,'2014-06-20 19:52:36','2014-06-20 19:52:36',7,5,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (63,'2014-06-20 19:52:36','2014-06-20 19:52:36',7,6,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (64,'2014-06-20 19:52:36','2014-06-20 19:52:36',7,7,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (65,'2014-06-20 19:52:36','2014-06-20 19:52:36',7,8,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (66,'2014-06-20 19:52:36','2014-06-20 19:52:36',7,9,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (67,'2014-06-20 19:52:36','2014-06-20 19:52:36',7,10,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (68,'2014-06-20 19:52:36','2014-06-20 19:52:36',7,11,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (69,'2014-06-21 16:00:47','2014-06-21 16:00:47',8,2,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (70,'2014-06-21 16:00:47','2014-06-21 16:00:47',8,3,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (71,'2014-06-21 16:00:47','2014-06-21 16:00:47',8,4,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (72,'2014-06-21 16:00:47','2014-06-21 16:00:47',8,5,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (73,'2014-06-21 16:00:47','2014-06-21 16:00:47',8,6,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (74,'2014-06-21 16:00:47','2014-06-21 16:00:47',8,7,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (75,'2014-06-21 16:00:47','2014-06-21 16:00:47',8,8,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (76,'2014-06-21 16:00:47','2014-06-21 16:00:47',8,9,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (77,'2014-06-21 16:00:47','2014-06-21 16:00:47',8,10,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (78,'2014-06-21 16:00:47','2014-06-21 16:00:47',8,11,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (79,'2014-06-26 23:12:29','2014-06-26 23:12:29',11,2,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (80,'2014-06-26 23:12:29','2014-06-26 23:12:29',11,3,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (81,'2014-06-26 23:12:29','2014-06-26 23:12:29',11,4,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (82,'2014-06-26 23:12:29','2014-06-26 23:12:29',11,5,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (83,'2014-06-26 23:12:29','2014-06-26 23:12:29',11,6,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (84,'2014-06-26 23:12:29','2014-06-26 23:12:29',11,7,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (85,'2014-06-26 23:12:29','2014-06-26 23:12:29',11,8,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (86,'2014-06-26 23:12:29','2014-06-26 23:12:29',11,9,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (87,'2014-06-26 23:12:29','2014-06-26 23:12:29',11,10,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);
					insert  into `studentsubjects`(`id`,`created_at`,`updated_at`,`enrollment_id`,`subject_id`,`year_id`,`semester_id`,`course_id`,`is_active`,`enrollmentid`,`block_system_setting_id`,`is_deducted`,`preliminary`,`midterm`,`semi_finals`,`finals`,`re_exam`,`remidial`,`remarks`,`date_dropped`,`rating`,`average`,`user_id`,`is_deleted`,`deleted_by`,`delete_date`,`is_drop`,`dropped_by`,`dropped_date`) values (88,'2014-06-26 23:12:29','2014-06-26 23:12:29',11,11,3,1,1,NULL,NULL,2,NULL,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,0.00,0.00,NULL,0,NULL,NULL,0,NULL,NULL);

					/*Table structure for table `subject_pre_requisite` */

					DROP TABLE IF EXISTS `subject_pre_requisite`;

					CREATE TABLE `subject_pre_requisite` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `curriculum_id` bigint(20) DEFAULT NULL COMMENT 'Link to Table Curriculum',
					  `curriculum_sub_id` bigint(20) DEFAULT NULL COMMENT 'Link to Table Curriculum Subjects',
					  `subject_id` bigint(20) DEFAULT NULL,
					  `remarks` varchar(255) DEFAULT NULL,
					  `is_deleted` tinyint(1) DEFAULT '0' COMMENT 'if deleted value is 1',
					  `deleted_by` varchar(100) DEFAULT NULL,
					  `deleted_date` datetime DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

					/*Data for the table `subject_pre_requisite` */

					insert  into `subject_pre_requisite`(`id`,`curriculum_id`,`curriculum_sub_id`,`subject_id`,`remarks`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (1,1,3,7,NULL,0,NULL,NULL,'2014-07-01 17:39:00','2014-07-01 17:39:00');
					insert  into `subject_pre_requisite`(`id`,`curriculum_id`,`curriculum_sub_id`,`subject_id`,`remarks`,`is_deleted`,`deleted_by`,`deleted_date`,`created_at`,`updated_at`) values (2,1,3,8,NULL,0,NULL,NULL,'2014-07-01 17:39:00','2014-07-01 17:39:00');

					/*Table structure for table `subjects` */

					DROP TABLE IF EXISTS `subjects`;

					CREATE TABLE `subjects` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `ref_id` varchar(25) NOT NULL COMMENT 'Unique ID of Every Subject, To Determine Same subject in every school year',
					  `sc_id` varchar(255) DEFAULT NULL,
					  `code` varchar(255) DEFAULT NULL,
					  `subject` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `units` float(11,2) NOT NULL DEFAULT '0.00',
					  `time` varchar(255) DEFAULT NULL,
					  `day` text,
					  `course_id` int(11) DEFAULT NULL,
					  `room_id` bigint(20) DEFAULT NULL,
					  `room` varchar(100) DEFAULT NULL,
					  `year_id` int(11) DEFAULT NULL,
					  `semester_id` int(11) DEFAULT NULL,
					  `lec` varchar(255) DEFAULT NULL,
					  `lab` varchar(255) DEFAULT NULL,
					  `lab_fee_id` bigint(20) DEFAULT NULL,
					  `original_load` int(11) DEFAULT NULL,
					  `load` int(11) DEFAULT NULL,
					  `subject_load` int(11) NOT NULL DEFAULT '0',
					  `subject_taken` int(11) DEFAULT '0',
					  `lab_kind` varchar(255) DEFAULT NULL,
					  `year_from` varchar(255) DEFAULT NULL,
					  `year_to` varchar(255) DEFAULT NULL,
					  `academic_year` varchar(255) DEFAULT NULL,
					  `employee_id` int(11) DEFAULT NULL,
					  `is_nstp` smallint(1) DEFAULT '0',
					  `nstp_fee_id` bigint(20) DEFAULT NULL,
					  `class_start` time DEFAULT NULL,
					  `class_end` time DEFAULT NULL,
					  `hour` int(11) DEFAULT '0',
					  `min` int(11) DEFAULT '0',
					  `teacher_user_id` bigint(20) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

					/*Data for the table `subjects` */

					insert  into `subjects`(`id`,`ref_id`,`sc_id`,`code`,`subject`,`created_at`,`updated_at`,`units`,`time`,`day`,`course_id`,`room_id`,`room`,`year_id`,`semester_id`,`lec`,`lab`,`lab_fee_id`,`original_load`,`load`,`subject_load`,`subject_taken`,`lab_kind`,`year_from`,`year_to`,`academic_year`,`employee_id`,`is_nstp`,`nstp_fee_id`,`class_start`,`class_end`,`hour`,`min`,`teacher_user_id`) values (2,'0000000017','ANAPHY1','LAA','ANATOMY AND PHYSIOLOGY - Lec','2014-01-14 16:02:44','2014-07-01 13:58:01',3.00,'10:00-11:00','MWF',1,2,'B503',3,1,'3','',18,40,NULL,40,1,NULL,'2014','2015',NULL,NULL,0,NULL,'10:00:00','11:00:00',0,0,15);
					insert  into `subjects`(`id`,`ref_id`,`sc_id`,`code`,`subject`,`created_at`,`updated_at`,`units`,`time`,`day`,`course_id`,`room_id`,`room`,`year_id`,`semester_id`,`lec`,`lab`,`lab_fee_id`,`original_load`,`load`,`subject_load`,`subject_taken`,`lab_kind`,`year_from`,`year_to`,`academic_year`,`employee_id`,`is_nstp`,`nstp_fee_id`,`class_start`,`class_end`,`hour`,`min`,`teacher_user_id`) values (3,'0000000018','ANAPHL1','LAA','ANATOMY AND PHYSIOLOGY-LAB','2014-01-14 16:04:24','2014-07-01 13:58:02',2.00,'08:00-10:00','MWF',1,3,'B507',3,1,'','6',18,40,NULL,40,1,NULL,'2014','2015',NULL,NULL,0,NULL,'08:00:00','10:00:00',0,0,15);
					insert  into `subjects`(`id`,`ref_id`,`sc_id`,`code`,`subject`,`created_at`,`updated_at`,`units`,`time`,`day`,`course_id`,`room_id`,`room`,`year_id`,`semester_id`,`lec`,`lab`,`lab_fee_id`,`original_load`,`load`,`subject_load`,`subject_taken`,`lab_kind`,`year_from`,`year_to`,`academic_year`,`employee_id`,`is_nstp`,`nstp_fee_id`,`class_start`,`class_end`,`hour`,`min`,`teacher_user_id`) values (4,'0000000019','ENGLIS2','LAA','COMMUNICATION SKILLS 2','2014-01-14 16:06:00','2014-07-01 13:58:02',3.00,'15:30-17:00','TTH',1,4,'B502',3,1,'3','',18,40,NULL,40,1,NULL,'2014','2015',NULL,NULL,0,NULL,'15:30:00','17:00:00',0,0,15);
					insert  into `subjects`(`id`,`ref_id`,`sc_id`,`code`,`subject`,`created_at`,`updated_at`,`units`,`time`,`day`,`course_id`,`room_id`,`room`,`year_id`,`semester_id`,`lec`,`lab`,`lab_fee_id`,`original_load`,`load`,`subject_load`,`subject_taken`,`lab_kind`,`year_from`,`year_to`,`academic_year`,`employee_id`,`is_nstp`,`nstp_fee_id`,`class_start`,`class_end`,`hour`,`min`,`teacher_user_id`) values (5,'0000000020','PSYCHO1','LAA','GENERAL PSYCHOLOGY','2014-01-14 16:07:47','2014-07-01 13:58:28',3.00,'12:00-13:00','MWF',1,4,'B502',3,1,'3','',18,40,NULL,40,1,NULL,'2014','2015',NULL,NULL,0,NULL,'12:00:00','13:00:00',0,0,15);
					insert  into `subjects`(`id`,`ref_id`,`sc_id`,`code`,`subject`,`created_at`,`updated_at`,`units`,`time`,`day`,`course_id`,`room_id`,`room`,`year_id`,`semester_id`,`lec`,`lab`,`lab_fee_id`,`original_load`,`load`,`subject_load`,`subject_taken`,`lab_kind`,`year_from`,`year_to`,`academic_year`,`employee_id`,`is_nstp`,`nstp_fee_id`,`class_start`,`class_end`,`hour`,`min`,`teacher_user_id`) values (6,'0000000021','PHILOS1','LAA','LOGIC','2014-01-14 16:09:12','2014-07-01 13:58:28',3.00,'11:00-12:00','MWF',1,6,'B502',3,1,'3','',18,40,NULL,40,1,NULL,'2014','2015',NULL,NULL,0,NULL,'11:00:00','12:00:00',0,0,15);
					insert  into `subjects`(`id`,`ref_id`,`sc_id`,`code`,`subject`,`created_at`,`updated_at`,`units`,`time`,`day`,`course_id`,`room_id`,`room`,`year_id`,`semester_id`,`lec`,`lab`,`lab_fee_id`,`original_load`,`load`,`subject_load`,`subject_taken`,`lab_kind`,`year_from`,`year_to`,`academic_year`,`employee_id`,`is_nstp`,`nstp_fee_id`,`class_start`,`class_end`,`hour`,`min`,`teacher_user_id`) values (7,'0000000022','NSTPRO2','LAA','NATIONAL SERVICE TRAINING PROGRAM 2','2014-01-14 16:12:39','2014-07-01 13:58:28',3.00,'14:00-17:00','F',1,1,'TBA',3,1,'3','',18,40,NULL,40,1,NULL,'2014','2015',NULL,NULL,1,NULL,'14:00:00','17:00:00',0,0,15);
					insert  into `subjects`(`id`,`ref_id`,`sc_id`,`code`,`subject`,`created_at`,`updated_at`,`units`,`time`,`day`,`course_id`,`room_id`,`room`,`year_id`,`semester_id`,`lec`,`lab`,`lab_fee_id`,`original_load`,`load`,`subject_load`,`subject_taken`,`lab_kind`,`year_from`,`year_to`,`academic_year`,`employee_id`,`is_nstp`,`nstp_fee_id`,`class_start`,`class_end`,`hour`,`min`,`teacher_user_id`) values (8,'0000000023','ORGCHM2','LAA','ORGANIC CHEMISTRY','2014-01-14 16:14:58','2014-07-01 13:58:28',3.00,'11:00-12:30','TTH',1,8,'B502',3,1,'3','',18,40,NULL,40,1,NULL,'2014','2015',NULL,NULL,0,NULL,'11:00:00','12:30:00',0,0,15);
					insert  into `subjects`(`id`,`ref_id`,`sc_id`,`code`,`subject`,`created_at`,`updated_at`,`units`,`time`,`day`,`course_id`,`room_id`,`room`,`year_id`,`semester_id`,`lec`,`lab`,`lab_fee_id`,`original_load`,`load`,`subject_load`,`subject_taken`,`lab_kind`,`year_from`,`year_to`,`academic_year`,`employee_id`,`is_nstp`,`nstp_fee_id`,`class_start`,`class_end`,`hour`,`min`,`teacher_user_id`) values (9,'0000000024','ORGCHL2','LAA','ORGANIC CHEMISTRY-LAB','2014-01-14 16:16:47','2014-07-01 13:58:28',2.00,'08:00-11:00','TTH',1,3,'B507',3,1,'','6',18,40,NULL,40,1,NULL,'2014','2015',NULL,NULL,0,NULL,'08:00:00','11:00:00',0,0,15);
					insert  into `subjects`(`id`,`ref_id`,`sc_id`,`code`,`subject`,`created_at`,`updated_at`,`units`,`time`,`day`,`course_id`,`room_id`,`room`,`year_id`,`semester_id`,`lec`,`lab`,`lab_fee_id`,`original_load`,`load`,`subject_load`,`subject_taken`,`lab_kind`,`year_from`,`year_to`,`academic_year`,`employee_id`,`is_nstp`,`nstp_fee_id`,`class_start`,`class_end`,`hour`,`min`,`teacher_user_id`) values (10,'0000000025','FILPPP2','LAA','PAGBASA AT PAGSULAT TUNGO SA PANANALIKSIK','2014-01-14 16:20:36','2014-07-01 13:58:28',3.00,'14:00-15:00','TTH',1,8,'B502',3,1,'3','',18,40,NULL,40,1,NULL,'2014','2015',NULL,NULL,0,NULL,'14:00:00','15:00:00',0,0,15);
					insert  into `subjects`(`id`,`ref_id`,`sc_id`,`code`,`subject`,`created_at`,`updated_at`,`units`,`time`,`day`,`course_id`,`room_id`,`room`,`year_id`,`semester_id`,`lec`,`lab`,`lab_fee_id`,`original_load`,`load`,`subject_load`,`subject_taken`,`lab_kind`,`year_from`,`year_to`,`academic_year`,`employee_id`,`is_nstp`,`nstp_fee_id`,`class_start`,`class_end`,`hour`,`min`,`teacher_user_id`) values (11,'0000000026','PESLBB2','LAA','TABLE TENNIS','2014-01-14 16:21:58','2014-07-01 13:58:29',2.00,'14:00-16:00','M',1,7,'D401',3,1,'2','',18,40,NULL,40,1,NULL,'2014','2015',NULL,NULL,0,NULL,'14:00:00','16:00:00',0,0,15);

					/*Table structure for table `subjects_load_file` */

					DROP TABLE IF EXISTS `subjects_load_file`;

					CREATE TABLE `subjects_load_file` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `subject_id` bigint(20) DEFAULT NULL,
					  `enrollment_id` bigint(20) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

					/*Data for the table `subjects_load_file` */

					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (1,2,4,'2014-01-24 11:15:42','2014-01-24 11:15:42');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (2,3,4,'2014-01-24 11:15:42','2014-01-24 11:15:42');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (3,4,4,'2014-01-24 11:15:42','2014-01-24 11:15:42');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (4,5,4,'2014-01-24 11:15:42','2014-01-24 11:15:42');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (5,6,4,'2014-01-24 11:15:42','2014-01-24 11:15:42');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (6,7,4,'2014-01-24 11:15:42','2014-01-24 11:15:42');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (7,8,4,'2014-01-24 11:15:42','2014-01-24 11:15:42');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (8,9,4,'2014-01-24 11:15:42','2014-01-24 11:15:42');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (9,10,4,'2014-01-24 11:15:42','2014-01-24 11:15:42');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (10,11,4,'2014-01-24 11:15:42','2014-01-24 11:15:42');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (11,2,2,'2014-01-29 13:37:31','2014-01-29 13:37:31');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (12,3,2,'2014-01-29 13:37:31','2014-01-29 13:37:31');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (13,4,2,'2014-01-29 13:37:31','2014-01-29 13:37:31');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (14,5,2,'2014-01-29 13:37:31','2014-01-29 13:37:31');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (15,6,2,'2014-01-29 13:37:31','2014-01-29 13:37:31');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (16,7,2,'2014-01-29 13:37:31','2014-01-29 13:37:31');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (17,8,2,'2014-01-29 13:37:31','2014-01-29 13:37:31');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (18,9,2,'2014-01-29 13:37:31','2014-01-29 13:37:31');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (19,10,2,'2014-01-29 13:37:31','2014-01-29 13:37:31');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (20,2,5,'2014-03-31 10:27:47','2014-03-31 10:27:47');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (21,3,5,'2014-03-31 10:27:47','2014-03-31 10:27:47');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (22,4,5,'2014-03-31 10:27:47','2014-03-31 10:27:47');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (23,5,5,'2014-03-31 10:27:47','2014-03-31 10:27:47');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (24,6,5,'2014-03-31 10:27:47','2014-03-31 10:27:47');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (25,7,5,'2014-03-31 10:27:47','2014-03-31 10:27:47');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (26,8,5,'2014-03-31 10:27:47','2014-03-31 10:27:47');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (27,9,5,'2014-03-31 10:27:47','2014-03-31 10:27:47');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (28,10,5,'2014-03-31 10:27:47','2014-03-31 10:27:47');
					insert  into `subjects_load_file`(`id`,`subject_id`,`enrollment_id`,`created_at`,`updated_at`) values (29,11,5,'2014-03-31 10:27:47','2014-03-31 10:27:47');

					/*Table structure for table `subjecttimeschedules` */

					DROP TABLE IF EXISTS `subjecttimeschedules`;

					CREATE TABLE `subjecttimeschedules` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `day` varchar(255) DEFAULT NULL,
					  `timef_h` varchar(255) DEFAULT NULL,
					  `timef_m` varchar(255) DEFAULT NULL,
					  `timef_state` varchar(255) DEFAULT NULL,
					  `timet_h` varchar(255) DEFAULT NULL,
					  `timet_m` varchar(255) DEFAULT NULL,
					  `timet_state` varchar(255) DEFAULT NULL,
					  `subject_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `subjecttimeschedules` */

					/*Table structure for table `summary_of_credits` */

					DROP TABLE IF EXISTS `summary_of_credits`;

					CREATE TABLE `summary_of_credits` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `course_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `summary_of_credits` */

					/*Table structure for table `sys_par` */

					DROP TABLE IF EXISTS `sys_par`;

					CREATE TABLE `sys_par` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `tmp_studid_series` varchar(50) DEFAULT '0',
					  `studid_series` varchar(50) DEFAULT '0' COMMENT 'Series Student ID',
					  `subj_refid_series` varchar(25) DEFAULT NULL COMMENT 'Series Reference Id for Subjects',
					  `is_auto_id` tinyint(1) DEFAULT '1',
					  `bootstrap_theme` set('AMELIA','CURELEAN','DEFAULT','FLATLY','JOURNAL','LUMEN','READABLE','SIMPLEX','SLATE','SPACELAB','SUPERHERO','UNITED','YETI','UB') DEFAULT 'DEFAULT',
					  `reg_success_msg` text,
					  `is_menu_accordion` tinyint(1) DEFAULT '0',
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `show_menu` tinyint(1) DEFAULT '1',
					  `application_layout` varchar(100) DEFAULT 'application' COMMENT 'Layout to be Used by the system (views/layouts/application_layout.php)',
					  `welcome_layout` varchar(100) DEFAULT 'welcome' COMMENT 'Layout to used in welcome screen and login',
					  `is_setup` tinyint(1) DEFAULT '0' COMMENT 'Is system was setup after fresh install',
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

					/*Data for the table `sys_par` */

					insert  into `sys_par`(`id`,`tmp_studid_series`,`studid_series`,`subj_refid_series`,`is_auto_id`,`bootstrap_theme`,`reg_success_msg`,`is_menu_accordion`,`created_at`,`updated_at`,`show_menu`,`application_layout`,`welcome_layout`,`is_setup`) values (1,'00000039','00000013','0000000027',1,'YETI','Your have been successfully registered. Thank you.',1,NULL,'2014-07-02 19:06:09',1,'application2','welcome2',0);

					/*Table structure for table `teachers` */

					DROP TABLE IF EXISTS `teachers`;

					CREATE TABLE `teachers` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `login` varchar(40) DEFAULT NULL,
					  `name` varchar(100) DEFAULT '',
					  `email` varchar(100) DEFAULT NULL,
					  `crypted_password` varchar(40) DEFAULT NULL,
					  `salt` varchar(40) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `remember_token` varchar(40) DEFAULT NULL,
					  `remember_token_expires_at` datetime DEFAULT NULL,
					  `activation_code` varchar(40) DEFAULT NULL,
					  `activated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`),
					  UNIQUE KEY `index_teachers_on_login` (`login`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `teachers` */

					/*Table structure for table `temp_enrollments` */

					DROP TABLE IF EXISTS `temp_enrollments`;

					CREATE TABLE `temp_enrollments` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `temp_start_id` bigint(20) DEFAULT NULL,
					  `is_drop` tinyint(1) NOT NULL DEFAULT '0',
					  `status2` varchar(255) DEFAULT NULL,
					  `enrollmentid` int(11) DEFAULT NULL,
					  `name` varchar(255) DEFAULT NULL,
					  `fname` varchar(255) DEFAULT NULL,
					  `year` varchar(255) DEFAULT NULL,
					  `course` varchar(255) DEFAULT NULL,
					  `subject` varchar(255) DEFAULT NULL,
					  `unit` varchar(255) NOT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `middle` varchar(255) DEFAULT NULL,
					  `semister` varchar(255) DEFAULT NULL,
					  `sy_from` varchar(255) DEFAULT NULL,
					  `sy_to` varchar(255) DEFAULT NULL,
					  `recent` varchar(255) DEFAULT NULL,
					  `sts` varchar(255) DEFAULT NULL,
					  `user_id` int(11) DEFAULT NULL,
					  `semester` int(11) DEFAULT NULL,
					  `firstname` varchar(255) DEFAULT NULL,
					  `lastname` varchar(255) DEFAULT NULL,
					  `age` int(11) DEFAULT NULL,
					  `telephone` varchar(255) DEFAULT NULL,
					  `disability` varchar(255) DEFAULT '',
					  `nationality` varchar(255) DEFAULT NULL,
					  `address` varchar(255) DEFAULT NULL,
					  `mobile` varchar(255) DEFAULT NULL,
					  `sex` varchar(255) DEFAULT NULL,
					  `discount` int(11) DEFAULT NULL,
					  `total` int(11) DEFAULT NULL,
					  `less` int(11) DEFAULT NULL,
					  `net` int(11) DEFAULT NULL,
					  `date_of_birth` date DEFAULT NULL,
					  `place_of_birth` varchar(255) DEFAULT NULL,
					  `civil_status` varchar(255) DEFAULT NULL,
					  `guardian` varchar(255) DEFAULT NULL,
					  `occupation` varchar(255) DEFAULT NULL,
					  `residence_address` varchar(255) DEFAULT NULL,
					  `business_address` varchar(255) DEFAULT NULL,
					  `education_parent` varchar(255) DEFAULT NULL,
					  `no_family` int(11) DEFAULT NULL,
					  `occupation_parent` varchar(255) DEFAULT NULL,
					  `income` int(11) DEFAULT NULL,
					  `primary` varchar(255) DEFAULT NULL,
					  `primary_date` varchar(255) DEFAULT NULL,
					  `intermediate` varchar(255) DEFAULT NULL,
					  `intermediate_date` varchar(255) DEFAULT NULL,
					  `secondary` varchar(255) DEFAULT NULL,
					  `secondary_date` varchar(255) DEFAULT NULL,
					  `college` varchar(255) DEFAULT NULL,
					  `college_date` varchar(255) DEFAULT NULL,
					  `course_completed` varchar(255) DEFAULT NULL,
					  `course_completed_date` varchar(255) DEFAULT NULL,
					  `studid` varchar(255) DEFAULT NULL,
					  `fake_email` varchar(255) DEFAULT NULL,
					  `status` varchar(255) DEFAULT NULL,
					  `scholar` varchar(255) DEFAULT NULL,
					  `is_active` varchar(255) DEFAULT NULL,
					  `old_or_new` varchar(255) DEFAULT NULL,
					  `total_units` int(11) DEFAULT NULL,
					  `total_lab_units` int(11) DEFAULT NULL,
					  `total_units_fee` int(11) DEFAULT NULL,
					  `total_lab_units_fee` int(11) DEFAULT NULL,
					  `father_name` varchar(255) DEFAULT NULL,
					  `father_occupation` varchar(255) DEFAULT NULL,
					  `father_contact_no` varchar(255) DEFAULT NULL,
					  `mother_name` varchar(255) DEFAULT NULL,
					  `mother_occupation` varchar(255) DEFAULT NULL,
					  `mother_contact_no` varchar(255) DEFAULT NULL,
					  `parents_address` varchar(255) DEFAULT NULL,
					  `guardian_relation` varchar(255) DEFAULT NULL,
					  `guardian_contact_no` varchar(255) DEFAULT NULL,
					  `guardian_address` varchar(255) DEFAULT NULL,
					  `present_address` varchar(255) DEFAULT NULL,
					  `provincial_address` varchar(255) DEFAULT NULL,
					  `elementary` varchar(255) DEFAULT NULL,
					  `elementary_date` varchar(255) DEFAULT NULL,
					  `vocational` varchar(255) DEFAULT NULL,
					  `vocational_date` varchar(255) DEFAULT NULL,
					  `tertiary` varchar(255) DEFAULT NULL,
					  `tertiary_date` varchar(255) DEFAULT NULL,
					  `elementary_address` varchar(255) DEFAULT NULL,
					  `elementary_degree` varchar(255) DEFAULT NULL,
					  `secondary_address` varchar(255) DEFAULT NULL,
					  `secondary_degree` varchar(255) DEFAULT NULL,
					  `vocational_address` varchar(255) DEFAULT NULL,
					  `vocational_degree` varchar(255) DEFAULT NULL,
					  `tertiary_address` varchar(255) DEFAULT NULL,
					  `tertiary_degree` varchar(255) DEFAULT NULL,
					  `others_address` varchar(255) DEFAULT NULL,
					  `others_degree` varchar(255) DEFAULT NULL,
					  `others` varchar(255) DEFAULT NULL,
					  `others_date` varchar(255) DEFAULT NULL,
					  `studentid` int(11) DEFAULT NULL,
					  `is_activated` int(11) DEFAULT '0',
					  `religion` varchar(255) DEFAULT NULL,
					  `is_paid` int(11) NOT NULL DEFAULT '0',
					  `y` int(11) DEFAULT NULL,
					  `s` int(11) DEFAULT NULL,
					  `c` int(11) DEFAULT NULL,
					  `year_id` int(11) DEFAULT NULL,
					  `course_id` int(11) DEFAULT NULL,
					  `semester_id` int(11) DEFAULT NULL,
					  `total_student_payment` float NOT NULL DEFAULT '0',
					  `total_previous_account` float NOT NULL DEFAULT '0',
					  `total_student_deduction` float NOT NULL DEFAULT '0',
					  `total_additional_charges` float NOT NULL DEFAULT '0',
					  `barangay` varchar(255) DEFAULT NULL,
					  `subject_load_deducted` int(11) NOT NULL DEFAULT '0',
					  `date_of_admission` varchar(255) DEFAULT NULL,
					  `admission_credentials` varchar(255) DEFAULT NULL,
					  `date_of_graduation` varchar(255) DEFAULT NULL,
					  `is_scholar` tinyint(4) DEFAULT NULL,
					  `province` varchar(255) DEFAULT NULL,
					  `municipal` varchar(255) DEFAULT NULL,
					  `living_with_parents` int(11) NOT NULL DEFAULT '0',
					  `full_paid` int(1) NOT NULL DEFAULT '0',
					  `partial_paid` int(1) NOT NULL DEFAULT '0',
					  `is_graduating` tinyint(1) DEFAULT NULL,
					  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
					  `street_no` int(11) DEFAULT NULL,
					  `is_used` smallint(1) DEFAULT '0',
					  `block_system_setting_id` int(11) DEFAULT NULL,
					  `studid_auto` varchar(50) DEFAULT NULL,
					  `confirm_status` varchar(50) DEFAULT 'UNFINISH',
					  `confirm_userid` bigint(20) DEFAULT NULL,
					  `confirm_date` datetime DEFAULT NULL,
					  `is_foreigner` tinyint(1) DEFAULT '0',
					  PRIMARY KEY (`id`),
					  KEY `StudentID` (`studid`)
					) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

					/*Data for the table `temp_enrollments` */

					insert  into `temp_enrollments`(`id`,`temp_start_id`,`is_drop`,`status2`,`enrollmentid`,`name`,`fname`,`year`,`course`,`subject`,`unit`,`created_at`,`updated_at`,`middle`,`semister`,`sy_from`,`sy_to`,`recent`,`sts`,`user_id`,`semester`,`firstname`,`lastname`,`age`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`date_of_birth`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`studid`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_units_fee`,`total_lab_units_fee`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`total_student_payment`,`total_previous_account`,`total_student_deduction`,`total_additional_charges`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`studid_auto`,`confirm_status`,`confirm_userid`,`confirm_date`,`is_foreigner`) values (1,1,0,NULL,NULL,'test, test test','test',NULL,NULL,NULL,'','2014-01-14 18:08:08','2014-05-01 05:46:31','test',NULL,'2013','2014',NULL,NULL,NULL,NULL,'test','test',0,NULL,'test','test',NULL,'test','Male',NULL,NULL,NULL,NULL,'0000-00-00','test','Widowed',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'test','test',NULL,NULL,NULL,NULL,'test','test','New',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'test','test','test','test','test','test','test',NULL,NULL,NULL,'test',NULL,'test','test','test','test','test','test','test',NULL,'test',NULL,'test','test','test','test','test','test','test','test',NULL,0,'test',0,NULL,NULL,NULL,3,1,1,0,0,0,0,'test',0,NULL,NULL,NULL,NULL,'0','test',1,0,0,0,0,0,0,NULL,'00000004','CONFIRM',14,NULL,0);
					insert  into `temp_enrollments`(`id`,`temp_start_id`,`is_drop`,`status2`,`enrollmentid`,`name`,`fname`,`year`,`course`,`subject`,`unit`,`created_at`,`updated_at`,`middle`,`semister`,`sy_from`,`sy_to`,`recent`,`sts`,`user_id`,`semester`,`firstname`,`lastname`,`age`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`date_of_birth`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`studid`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_units_fee`,`total_lab_units_fee`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`total_student_payment`,`total_previous_account`,`total_student_deduction`,`total_additional_charges`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`studid_auto`,`confirm_status`,`confirm_userid`,`confirm_date`,`is_foreigner`) values (2,2,0,NULL,NULL,'test, test test','test',NULL,NULL,NULL,'','2014-01-14 18:08:48','2014-01-14 18:08:48','test',NULL,'2013','2014',NULL,NULL,NULL,NULL,'test','test',0,NULL,'test','test',NULL,'test','Male',NULL,NULL,NULL,NULL,'0000-00-00','test','Widowed',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'test','test',NULL,NULL,NULL,NULL,'test','test','New',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'test','test','test','test','test','test','test',NULL,NULL,NULL,'test',NULL,'test','test','test','test','test','test','test',NULL,'test',NULL,'test','test','test','test','test','test','test','test',NULL,0,'test',0,NULL,NULL,NULL,3,1,1,0,0,0,0,'test',0,NULL,NULL,NULL,NULL,'0','test',1,0,0,0,0,0,0,NULL,NULL,'PENDING',NULL,NULL,0);
					insert  into `temp_enrollments`(`id`,`temp_start_id`,`is_drop`,`status2`,`enrollmentid`,`name`,`fname`,`year`,`course`,`subject`,`unit`,`created_at`,`updated_at`,`middle`,`semister`,`sy_from`,`sy_to`,`recent`,`sts`,`user_id`,`semester`,`firstname`,`lastname`,`age`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`date_of_birth`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`studid`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_units_fee`,`total_lab_units_fee`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`total_student_payment`,`total_previous_account`,`total_student_deduction`,`total_additional_charges`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`studid_auto`,`confirm_status`,`confirm_userid`,`confirm_date`,`is_foreigner`) values (3,3,0,NULL,NULL,'Raven, Mark I','Mark',NULL,NULL,NULL,'','2014-01-14 18:13:11','2014-05-01 05:46:57','I',NULL,'2013','2014',NULL,NULL,NULL,NULL,'Mark','Raven',16,NULL,'','Fil',NULL,'','Male',NULL,NULL,NULL,NULL,'1998-01-07','Baguio City','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Easter College','2013',NULL,NULL,NULL,NULL,'20142478','','New',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Mr Raven','Teacher','09225856255','Mrs Raven','Teacher','09225856255','Pinsao Pilot',NULL,NULL,NULL,'Pu3ok 2',NULL,'Easter College','2007','','','','','Guisad',NULL,'Guisad',NULL,'','','','','','','','',NULL,0,'Christian',0,NULL,NULL,NULL,3,1,1,0,0,0,0,'Pinsao Pilot',0,NULL,NULL,NULL,NULL,'12','Baguio City',1,0,0,0,0,34,0,NULL,'00000005','CONFIRM',14,NULL,0);
					insert  into `temp_enrollments`(`id`,`temp_start_id`,`is_drop`,`status2`,`enrollmentid`,`name`,`fname`,`year`,`course`,`subject`,`unit`,`created_at`,`updated_at`,`middle`,`semister`,`sy_from`,`sy_to`,`recent`,`sts`,`user_id`,`semester`,`firstname`,`lastname`,`age`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`date_of_birth`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`studid`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_units_fee`,`total_lab_units_fee`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`total_student_payment`,`total_previous_account`,`total_student_deduction`,`total_additional_charges`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`studid_auto`,`confirm_status`,`confirm_userid`,`confirm_date`,`is_foreigner`) values (5,5,0,NULL,NULL,'test, test test','test',NULL,NULL,NULL,'','2014-01-14 18:19:34','2014-01-14 18:19:34','test',NULL,'2013','2014',NULL,NULL,NULL,NULL,'test','test',0,NULL,'test','test',NULL,'test','Male',NULL,NULL,NULL,NULL,'0000-00-00','test','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'test','test',NULL,NULL,NULL,NULL,'test','test','Cross',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'test','test','test','test','test','test','test',NULL,NULL,NULL,'test',NULL,'test','test','test','test','test','test','test',NULL,'test',NULL,'test','test','test','test','test','test','test','test',NULL,0,'test',0,NULL,NULL,NULL,3,1,1,0,0,0,0,'test',0,NULL,NULL,NULL,NULL,'0','test',1,0,0,0,0,0,0,NULL,NULL,'PENDING',NULL,NULL,0);
					insert  into `temp_enrollments`(`id`,`temp_start_id`,`is_drop`,`status2`,`enrollmentid`,`name`,`fname`,`year`,`course`,`subject`,`unit`,`created_at`,`updated_at`,`middle`,`semister`,`sy_from`,`sy_to`,`recent`,`sts`,`user_id`,`semester`,`firstname`,`lastname`,`age`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`date_of_birth`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`studid`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_units_fee`,`total_lab_units_fee`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`total_student_payment`,`total_previous_account`,`total_student_deduction`,`total_additional_charges`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`studid_auto`,`confirm_status`,`confirm_userid`,`confirm_date`,`is_foreigner`) values (6,6,0,NULL,NULL,'test, test test','test',NULL,NULL,NULL,'','2014-01-14 18:21:07','2014-01-14 18:21:07','test',NULL,'2013','2014',NULL,NULL,NULL,NULL,'test','test',0,NULL,'test','test',NULL,'test','Male',NULL,NULL,NULL,NULL,'0000-00-00','test','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'test','test',NULL,NULL,NULL,NULL,'test','test','Cross',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'test','test','test','test','test','test','test',NULL,NULL,NULL,'test',NULL,'test','test','test','test','test','test','test',NULL,'test',NULL,'test','test','test','test','test','test','test','test',NULL,0,'test',0,NULL,NULL,NULL,3,1,1,0,0,0,0,'test',0,NULL,NULL,NULL,NULL,'0','test',1,0,0,0,0,0,0,NULL,NULL,'PENDING',NULL,NULL,0);
					insert  into `temp_enrollments`(`id`,`temp_start_id`,`is_drop`,`status2`,`enrollmentid`,`name`,`fname`,`year`,`course`,`subject`,`unit`,`created_at`,`updated_at`,`middle`,`semister`,`sy_from`,`sy_to`,`recent`,`sts`,`user_id`,`semester`,`firstname`,`lastname`,`age`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`date_of_birth`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`studid`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_units_fee`,`total_lab_units_fee`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`total_student_payment`,`total_previous_account`,`total_student_deduction`,`total_additional_charges`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`studid_auto`,`confirm_status`,`confirm_userid`,`confirm_date`,`is_foreigner`) values (7,7,0,NULL,NULL,'test, test test','test',NULL,NULL,NULL,'','2014-01-14 18:21:32','2014-01-14 18:21:32','test',NULL,'2013','2014',NULL,NULL,NULL,NULL,'test','test',0,NULL,'test','test',NULL,'test','Male',NULL,NULL,NULL,NULL,'0000-00-00','test','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'test','test',NULL,NULL,NULL,NULL,'test','test','Cross',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'test','test','test','test','test','test','test',NULL,NULL,NULL,'test',NULL,'test','test','test','test','test','test','test',NULL,'test',NULL,'test','test','test','test','test','test','test','test',NULL,0,'test',0,NULL,NULL,NULL,3,1,1,0,0,0,0,'test',0,NULL,NULL,NULL,NULL,'0','test',1,0,0,0,0,0,0,NULL,NULL,'PENDING',NULL,NULL,0);
					insert  into `temp_enrollments`(`id`,`temp_start_id`,`is_drop`,`status2`,`enrollmentid`,`name`,`fname`,`year`,`course`,`subject`,`unit`,`created_at`,`updated_at`,`middle`,`semister`,`sy_from`,`sy_to`,`recent`,`sts`,`user_id`,`semester`,`firstname`,`lastname`,`age`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`date_of_birth`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`studid`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_units_fee`,`total_lab_units_fee`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`total_student_payment`,`total_previous_account`,`total_student_deduction`,`total_additional_charges`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`studid_auto`,`confirm_status`,`confirm_userid`,`confirm_date`,`is_foreigner`) values (8,8,0,NULL,NULL,'test2, test2 test2','test2',NULL,NULL,NULL,'','2014-01-14 18:29:32','2014-01-14 18:29:32','test2',NULL,'2013','2014',NULL,NULL,NULL,NULL,'test2','test2',0,NULL,'test2','test2',NULL,'test2','Male',NULL,NULL,NULL,NULL,'0000-00-00','test2','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'test2','test2',NULL,NULL,NULL,NULL,'test2','test2','Cross',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'test2','test2','test2','test2','test2','test2','test2',NULL,NULL,NULL,'test2',NULL,'test2','test2','test2','test2','test2','test2','test2',NULL,'test2',NULL,'test2','test2','test2','test2','test2','test2','test2','test2',NULL,0,'test2',0,NULL,NULL,NULL,3,1,1,0,0,0,0,'test2',0,NULL,NULL,NULL,NULL,'0','test2',1,0,0,0,0,0,0,NULL,NULL,'PENDING',NULL,NULL,0);
					insert  into `temp_enrollments`(`id`,`temp_start_id`,`is_drop`,`status2`,`enrollmentid`,`name`,`fname`,`year`,`course`,`subject`,`unit`,`created_at`,`updated_at`,`middle`,`semister`,`sy_from`,`sy_to`,`recent`,`sts`,`user_id`,`semester`,`firstname`,`lastname`,`age`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`date_of_birth`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`studid`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_units_fee`,`total_lab_units_fee`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`total_student_payment`,`total_previous_account`,`total_student_deduction`,`total_additional_charges`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`studid_auto`,`confirm_status`,`confirm_userid`,`confirm_date`,`is_foreigner`) values (10,15,0,NULL,NULL,'Student Example, Student Example Student Example','Student Example',NULL,NULL,NULL,'','2014-04-28 13:12:28','2014-04-28 13:12:28','Student Example',NULL,'2013','2014',NULL,NULL,NULL,NULL,'Student Example','Student Example',0,NULL,'Student Example','Student Example',NULL,'Student Example','Male',NULL,NULL,NULL,NULL,'0000-00-00','Student Example','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Student Example','Student Example',NULL,NULL,NULL,NULL,'ER-00000008','Student Example','new',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Student Example','Student Example','Student Example','Student Example','Student Example','Student Example','Student Example',NULL,NULL,NULL,'Student Example',NULL,'Student Example','Student Example','Student Example','Student Example','Student Example','Student Example','Student Example',NULL,'Student Example',NULL,'Student Example','Student Example','Student Example','Student Example','Student Example','Student Example','Student Example','Student Example',NULL,0,'Student Example',0,NULL,NULL,NULL,3,1,1,0,0,0,0,'Student Example',0,NULL,NULL,NULL,NULL,'Abra','Student Example',1,0,0,0,0,0,0,NULL,NULL,'PENDING',NULL,NULL,0);
					insert  into `temp_enrollments`(`id`,`temp_start_id`,`is_drop`,`status2`,`enrollmentid`,`name`,`fname`,`year`,`course`,`subject`,`unit`,`created_at`,`updated_at`,`middle`,`semister`,`sy_from`,`sy_to`,`recent`,`sts`,`user_id`,`semester`,`firstname`,`lastname`,`age`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`date_of_birth`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`studid`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_units_fee`,`total_lab_units_fee`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`total_student_payment`,`total_previous_account`,`total_student_deduction`,`total_additional_charges`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`studid_auto`,`confirm_status`,`confirm_userid`,`confirm_date`,`is_foreigner`) values (11,16,0,NULL,NULL,'Sylver Sample, Sylver Sample Sylver Sample','Sylver Sample',NULL,NULL,NULL,'','2014-04-28 15:49:28','2014-04-28 15:49:28','Sylver Sample',NULL,'2013','2014',NULL,NULL,NULL,NULL,'Sylver Sample','Sylver Sample',0,NULL,'Sylver Sample','Sylver Sample',NULL,'Sylver Sample','Male',NULL,NULL,NULL,NULL,'0000-00-00','Sylver Sample','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Sylver Sample','Sylver Sample',NULL,NULL,NULL,NULL,'ER-00000010','Sylver Sample','new',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Sylver Sample','Sylver Sample','Sylver Sample','Sylver Sample','Sylver Sample','Sylver Sample','Sylver Sample',NULL,NULL,NULL,'Sylver Sample',NULL,'Sylver Sample','Sylver Sample','Sylver Sample','Sylver Sample','Sylver Sample','Sylver Sample','Sylver Sample',NULL,'Sylver Sample',NULL,'Sylver Sample','Sylver Sample','Sylver Sample','Sylver Sample','Sylver Sample','Sylver Sample','Sylver Sample','Sylver Sample',NULL,0,'Sylver Sample',0,NULL,NULL,NULL,3,1,1,0,0,0,0,'Sylver Sample',0,NULL,NULL,NULL,NULL,'Abra','Sylver Sample',1,0,0,0,0,0,0,NULL,NULL,'PENDING',NULL,NULL,0);
					insert  into `temp_enrollments`(`id`,`temp_start_id`,`is_drop`,`status2`,`enrollmentid`,`name`,`fname`,`year`,`course`,`subject`,`unit`,`created_at`,`updated_at`,`middle`,`semister`,`sy_from`,`sy_to`,`recent`,`sts`,`user_id`,`semester`,`firstname`,`lastname`,`age`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`date_of_birth`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`studid`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_units_fee`,`total_lab_units_fee`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`total_student_payment`,`total_previous_account`,`total_student_deduction`,`total_additional_charges`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`studid_auto`,`confirm_status`,`confirm_userid`,`confirm_date`,`is_foreigner`) values (14,19,0,NULL,NULL,'Raven, MARK L','MARK',NULL,NULL,NULL,'','2014-06-05 17:56:27','2014-06-05 17:56:27','L',NULL,'2014','2015',NULL,NULL,NULL,NULL,'MARK','Raven',17,NULL,'','FILIPINO',NULL,'09225856255','Male',NULL,NULL,NULL,NULL,'2014-06-03','BAGUIO CITY','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Easter College, Inc','2013',NULL,NULL,NULL,NULL,'ER-00000015','','new',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'MR RAVEN','ENGR','09225856255','MRS RAVEN','TEACHER','09225856255','34 PURIK 2 PINSAO PILOT',NULL,NULL,NULL,'Purok 3',NULL,'Easter Collefe, Inc','Easter Collefe, Inc','','','','','Guisad',NULL,'Guisad',NULL,'','','','','','','','',NULL,0,'CHRISTIAN',0,NULL,NULL,NULL,3,1,1,0,0,0,0,'Pinsao Pilot',0,NULL,NULL,NULL,NULL,'Benguet','Baguio City',1,0,0,0,0,34,0,2,NULL,'UNFINISH',NULL,NULL,0);
					insert  into `temp_enrollments`(`id`,`temp_start_id`,`is_drop`,`status2`,`enrollmentid`,`name`,`fname`,`year`,`course`,`subject`,`unit`,`created_at`,`updated_at`,`middle`,`semister`,`sy_from`,`sy_to`,`recent`,`sts`,`user_id`,`semester`,`firstname`,`lastname`,`age`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`date_of_birth`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`studid`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_units_fee`,`total_lab_units_fee`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`total_student_payment`,`total_previous_account`,`total_student_deduction`,`total_additional_charges`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`studid_auto`,`confirm_status`,`confirm_userid`,`confirm_date`,`is_foreigner`) values (15,20,0,NULL,NULL,'Eval, Eval Eval','Eval',NULL,NULL,NULL,'','2014-06-05 18:04:32','2014-06-05 18:04:32','Eval',NULL,'2014','2015',NULL,NULL,NULL,NULL,'Eval','Eval',0,NULL,'Eval','Eval',NULL,'Eval','Male',NULL,NULL,NULL,NULL,'0000-00-00','Eval','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Eval','Eval',NULL,NULL,NULL,NULL,'ER-00000017','Eval','new',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Eval','Eval','Eval','Eval','Eval','Eval','Eval',NULL,NULL,NULL,'Eval',NULL,'Eval','Eval','Eval','Eval','Eval','Eval','Eval',NULL,'Eval',NULL,'Eval','Eval','Eval','Eval','Eval','Eval','Eval','Eval',NULL,0,'Eval',0,NULL,NULL,NULL,3,1,1,0,0,0,0,'Eval',0,NULL,NULL,NULL,NULL,'Abra','Eval',1,0,0,0,0,0,0,NULL,NULL,'UNFINISH',NULL,NULL,0);
					insert  into `temp_enrollments`(`id`,`temp_start_id`,`is_drop`,`status2`,`enrollmentid`,`name`,`fname`,`year`,`course`,`subject`,`unit`,`created_at`,`updated_at`,`middle`,`semister`,`sy_from`,`sy_to`,`recent`,`sts`,`user_id`,`semester`,`firstname`,`lastname`,`age`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`date_of_birth`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`studid`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_units_fee`,`total_lab_units_fee`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`total_student_payment`,`total_previous_account`,`total_student_deduction`,`total_additional_charges`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`studid_auto`,`confirm_status`,`confirm_userid`,`confirm_date`,`is_foreigner`) values (16,21,0,NULL,NULL,'Bullock, Sandra S','Sandra',NULL,NULL,NULL,'','2014-06-20 14:32:14','2014-06-20 18:20:53','S',NULL,'2014','2015',NULL,NULL,NULL,NULL,'Sandra','Bullock',17,NULL,'','FILIPINO',NULL,'','Female',NULL,NULL,NULL,NULL,'1997-06-10','BAGUIO CITY','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Easter College, Inc','2013',NULL,NULL,NULL,NULL,'ER-00000018','','new',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Mr Bullock','Nurse','09225856255','Mrs. Bullock','Nurse','09225856255','Pinsao Pilot',NULL,NULL,NULL,'Pinsao Pilot',NULL,'Easter College, Inc','2009','','','','','Guisad',NULL,'Guisad',NULL,'','','','','','','','',NULL,0,'CHRISTIAN',0,NULL,NULL,NULL,3,1,1,0,0,0,0,'Pinsao Pilot',0,NULL,NULL,NULL,NULL,'Abra','Baguio City',1,0,0,0,0,34,0,2,NULL,'UNCONFIRM',14,NULL,0);
					insert  into `temp_enrollments`(`id`,`temp_start_id`,`is_drop`,`status2`,`enrollmentid`,`name`,`fname`,`year`,`course`,`subject`,`unit`,`created_at`,`updated_at`,`middle`,`semister`,`sy_from`,`sy_to`,`recent`,`sts`,`user_id`,`semester`,`firstname`,`lastname`,`age`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`date_of_birth`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`studid`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_units_fee`,`total_lab_units_fee`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`total_student_payment`,`total_previous_account`,`total_student_deduction`,`total_additional_charges`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`studid_auto`,`confirm_status`,`confirm_userid`,`confirm_date`,`is_foreigner`) values (19,24,0,NULL,NULL,'Salvador, Allei B','Allei',NULL,NULL,NULL,'','2014-06-21 17:43:14','2014-06-21 17:45:50','B',NULL,'2014','2015',NULL,NULL,NULL,NULL,'Allei','Salvador',17,NULL,'','FILIPINO',NULL,'','Female',NULL,NULL,NULL,NULL,'1997-01-15','BAGUIO CITY','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Easter College, Inc','2013',NULL,NULL,NULL,NULL,'ER-00000024','','new',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Mr Salvador','ENGR','09225856255','Mrs Salvador','None','09225856255','Pinsao Pilot',NULL,NULL,NULL,'Pinsao Pilot',NULL,'Easter College, Inc','2009','','','','','Guisad',NULL,'Guisad',NULL,'','','','','','','','',NULL,0,'CHRISTIAN',0,NULL,NULL,NULL,3,1,1,0,0,0,0,'Pinsao Pilot',0,NULL,NULL,NULL,NULL,'Benguet','Baguio City',1,0,0,0,0,34,0,2,'00000008','CONFIRM',14,NULL,0);
					insert  into `temp_enrollments`(`id`,`temp_start_id`,`is_drop`,`status2`,`enrollmentid`,`name`,`fname`,`year`,`course`,`subject`,`unit`,`created_at`,`updated_at`,`middle`,`semister`,`sy_from`,`sy_to`,`recent`,`sts`,`user_id`,`semester`,`firstname`,`lastname`,`age`,`telephone`,`disability`,`nationality`,`address`,`mobile`,`sex`,`discount`,`total`,`less`,`net`,`date_of_birth`,`place_of_birth`,`civil_status`,`guardian`,`occupation`,`residence_address`,`business_address`,`education_parent`,`no_family`,`occupation_parent`,`income`,`primary`,`primary_date`,`intermediate`,`intermediate_date`,`secondary`,`secondary_date`,`college`,`college_date`,`course_completed`,`course_completed_date`,`studid`,`fake_email`,`status`,`scholar`,`is_active`,`old_or_new`,`total_units`,`total_lab_units`,`total_units_fee`,`total_lab_units_fee`,`father_name`,`father_occupation`,`father_contact_no`,`mother_name`,`mother_occupation`,`mother_contact_no`,`parents_address`,`guardian_relation`,`guardian_contact_no`,`guardian_address`,`present_address`,`provincial_address`,`elementary`,`elementary_date`,`vocational`,`vocational_date`,`tertiary`,`tertiary_date`,`elementary_address`,`elementary_degree`,`secondary_address`,`secondary_degree`,`vocational_address`,`vocational_degree`,`tertiary_address`,`tertiary_degree`,`others_address`,`others_degree`,`others`,`others_date`,`studentid`,`is_activated`,`religion`,`is_paid`,`y`,`s`,`c`,`year_id`,`course_id`,`semester_id`,`total_student_payment`,`total_previous_account`,`total_student_deduction`,`total_additional_charges`,`barangay`,`subject_load_deducted`,`date_of_admission`,`admission_credentials`,`date_of_graduation`,`is_scholar`,`province`,`municipal`,`living_with_parents`,`full_paid`,`partial_paid`,`is_graduating`,`is_deleted`,`street_no`,`is_used`,`block_system_setting_id`,`studid_auto`,`confirm_status`,`confirm_userid`,`confirm_date`,`is_foreigner`) values (23,28,0,NULL,NULL,'Castillo, Nelson B','Nelson',NULL,NULL,NULL,'','2014-06-27 08:51:47','2014-06-27 09:13:21','B',NULL,'2014','2015',NULL,NULL,NULL,NULL,'Nelson','Castillo',16,NULL,'','FILIPINO',NULL,'','Male',NULL,NULL,NULL,NULL,'1998-01-14','Cavite','Single',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'BCNHS','2014',NULL,NULL,NULL,NULL,'ER-00000032','nelson@yahoo.com','new',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Mr Castillo','Teacher','09225856255','Mrs Castillo','Nurse','09225856255','San Luis Baguio City',NULL,NULL,NULL,'San Luis Baguio City',NULL,'BCS','2010','','','','','Baguio City',NULL,'Governor pack road',NULL,'','','','','','','','',NULL,0,'Catholic',0,NULL,NULL,NULL,3,1,1,0,0,0,0,'San Luis',0,NULL,NULL,NULL,NULL,'Benguet','Baguio City',1,0,0,0,0,34,0,2,'00000012','CONFIRM',14,'2014-06-27 09:13:21',0);

					/*Table structure for table `temp_payment` */

					DROP TABLE IF EXISTS `temp_payment`;

					CREATE TABLE `temp_payment` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `temp_start_id` bigint(20) DEFAULT NULL,
					  `payment_plan_id` bigint(20) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

					/*Data for the table `temp_payment` */

					insert  into `temp_payment`(`id`,`temp_start_id`,`payment_plan_id`,`created_at`,`updated_at`) values (1,8,2,'2014-01-14 18:30:10','2014-01-14 18:30:10');

					/*Table structure for table `temp_start` */

					DROP TABLE IF EXISTS `temp_start`;

					CREATE TABLE `temp_start` (
					  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `studid` varchar(100) DEFAULT NULL,
					  `course_id` int(11) DEFAULT NULL,
					  `year_id` int(11) DEFAULT NULL,
					  `type` varchar(50) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

					/*Data for the table `temp_start` */

					insert  into `temp_start`(`id`,`studid`,`course_id`,`year_id`,`type`,`created_at`,`updated_at`) values (7,'test',1,3,'block','2014-01-14 18:21:32','2014-01-14 18:21:32');
					insert  into `temp_start`(`id`,`studid`,`course_id`,`year_id`,`type`,`created_at`,`updated_at`) values (13,'ER-00000004',1,3,'irregular','2014-03-31 10:13:09','2014-03-31 10:13:09');
					insert  into `temp_start`(`id`,`studid`,`course_id`,`year_id`,`type`,`created_at`,`updated_at`) values (15,'ER-00000008',1,3,'irregular','2014-04-28 13:12:28','2014-04-28 13:12:28');
					insert  into `temp_start`(`id`,`studid`,`course_id`,`year_id`,`type`,`created_at`,`updated_at`) values (16,'ER-00000010',1,3,'irregular','2014-04-28 15:49:28','2014-04-28 15:49:28');
					insert  into `temp_start`(`id`,`studid`,`course_id`,`year_id`,`type`,`created_at`,`updated_at`) values (19,'ER-00000015',1,3,'block','2014-06-05 17:56:27','2014-06-05 17:56:27');
					insert  into `temp_start`(`id`,`studid`,`course_id`,`year_id`,`type`,`created_at`,`updated_at`) values (20,'ER-00000017',1,3,'block','2014-06-05 18:04:32','2014-06-05 18:04:32');
					insert  into `temp_start`(`id`,`studid`,`course_id`,`year_id`,`type`,`created_at`,`updated_at`) values (21,'ER-00000018',1,3,'block','2014-06-20 14:32:14','2014-06-20 14:32:14');
					insert  into `temp_start`(`id`,`studid`,`course_id`,`year_id`,`type`,`created_at`,`updated_at`) values (24,'ER-00000024',1,3,'block','2014-06-21 17:43:14','2014-06-21 17:43:14');
					insert  into `temp_start`(`id`,`studid`,`course_id`,`year_id`,`type`,`created_at`,`updated_at`) values (28,'ER-00000032',1,3,'block','2014-06-27 08:51:47','2014-06-27 08:51:47');

					/*Table structure for table `temp_studentsubjects` */

					DROP TABLE IF EXISTS `temp_studentsubjects`;

					CREATE TABLE `temp_studentsubjects` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `subject_id` int(11) DEFAULT NULL,
					  `block_system_setting_id` int(11) DEFAULT NULL,
					  `temp_start_id` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8;

					/*Data for the table `temp_studentsubjects` */

					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (11,'2014-01-14 18:29:34','2014-01-14 18:29:34',1,1,8);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (12,'2014-01-14 18:29:34','2014-01-14 18:29:34',2,1,8);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (13,'2014-01-14 18:29:34','2014-01-14 18:29:34',3,1,8);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (14,'2014-01-14 18:29:34','2014-01-14 18:29:34',4,1,8);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (15,'2014-01-14 18:29:34','2014-01-14 18:29:34',5,1,8);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (16,'2014-01-14 18:29:34','2014-01-14 18:29:34',6,1,8);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (17,'2014-01-14 18:29:34','2014-01-14 18:29:34',7,1,8);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (18,'2014-01-14 18:29:34','2014-01-14 18:29:34',8,1,8);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (19,'2014-01-14 18:29:34','2014-01-14 18:29:34',9,1,8);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (20,'2014-01-14 18:29:34','2014-01-14 18:29:34',10,1,8);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (31,'2014-04-28 13:12:34','2014-04-28 13:12:34',2,NULL,15);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (32,'2014-04-28 13:12:34','2014-04-28 13:12:34',3,NULL,15);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (33,'2014-04-28 13:12:34','2014-04-28 13:12:34',4,NULL,15);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (34,'2014-04-28 13:12:34','2014-04-28 13:12:34',9,NULL,15);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (35,'2014-04-28 13:12:34','2014-04-28 13:12:34',10,NULL,15);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (36,'2014-04-28 13:12:34','2014-04-28 13:12:34',11,NULL,15);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (37,'2014-04-28 15:49:34','2014-04-28 15:49:34',2,NULL,16);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (38,'2014-04-28 15:49:34','2014-04-28 15:49:34',3,NULL,16);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (39,'2014-04-28 15:49:34','2014-04-28 15:49:34',4,NULL,16);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (40,'2014-04-28 15:49:34','2014-04-28 15:49:34',5,NULL,16);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (51,'2014-06-05 19:38:44','2014-06-05 19:38:44',2,2,19);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (52,'2014-06-05 19:38:44','2014-06-05 19:38:44',3,2,19);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (53,'2014-06-05 19:38:44','2014-06-05 19:38:44',4,2,19);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (54,'2014-06-05 19:38:44','2014-06-05 19:38:44',5,2,19);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (55,'2014-06-05 19:38:44','2014-06-05 19:38:44',6,2,19);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (56,'2014-06-05 19:38:44','2014-06-05 19:38:44',7,2,19);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (57,'2014-06-05 19:38:44','2014-06-05 19:38:44',8,2,19);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (58,'2014-06-05 19:38:44','2014-06-05 19:38:44',9,2,19);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (59,'2014-06-05 19:38:44','2014-06-05 19:38:44',10,2,19);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (60,'2014-06-05 19:38:44','2014-06-05 19:38:44',11,2,19);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (71,'2014-06-20 18:00:42','2014-06-20 18:00:42',2,2,21);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (72,'2014-06-20 18:00:42','2014-06-20 18:00:42',3,2,21);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (73,'2014-06-20 18:00:42','2014-06-20 18:00:42',4,2,21);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (74,'2014-06-20 18:00:42','2014-06-20 18:00:42',5,2,21);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (75,'2014-06-20 18:00:42','2014-06-20 18:00:42',6,2,21);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (76,'2014-06-20 18:00:42','2014-06-20 18:00:42',7,2,21);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (77,'2014-06-20 18:00:42','2014-06-20 18:00:42',8,2,21);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (78,'2014-06-20 18:00:42','2014-06-20 18:00:42',9,2,21);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (79,'2014-06-20 18:00:42','2014-06-20 18:00:42',10,2,21);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (80,'2014-06-20 18:00:42','2014-06-20 18:00:42',11,2,21);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (141,'2014-06-21 17:43:25','2014-06-21 17:43:25',2,2,24);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (142,'2014-06-21 17:43:25','2014-06-21 17:43:25',3,2,24);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (143,'2014-06-21 17:43:25','2014-06-21 17:43:25',4,2,24);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (144,'2014-06-21 17:43:25','2014-06-21 17:43:25',5,2,24);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (145,'2014-06-21 17:43:25','2014-06-21 17:43:25',6,2,24);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (146,'2014-06-21 17:43:25','2014-06-21 17:43:25',7,2,24);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (147,'2014-06-21 17:43:25','2014-06-21 17:43:25',8,2,24);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (148,'2014-06-21 17:43:25','2014-06-21 17:43:25',9,2,24);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (149,'2014-06-21 17:43:25','2014-06-21 17:43:25',10,2,24);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (150,'2014-06-21 17:43:25','2014-06-21 17:43:25',11,2,24);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (151,'2014-06-26 13:49:24','2014-06-26 13:49:24',2,2,25);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (152,'2014-06-26 13:49:24','2014-06-26 13:49:24',3,2,25);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (153,'2014-06-26 13:49:24','2014-06-26 13:49:24',4,2,25);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (154,'2014-06-26 13:49:24','2014-06-26 13:49:24',5,2,25);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (155,'2014-06-26 13:49:24','2014-06-26 13:49:24',6,2,25);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (156,'2014-06-26 13:49:24','2014-06-26 13:49:24',7,2,25);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (157,'2014-06-26 13:49:24','2014-06-26 13:49:24',8,2,25);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (158,'2014-06-26 13:49:24','2014-06-26 13:49:24',9,2,25);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (159,'2014-06-26 13:49:24','2014-06-26 13:49:24',10,2,25);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (160,'2014-06-26 13:49:24','2014-06-26 13:49:24',11,2,25);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (171,'2014-06-27 08:52:07','2014-06-27 08:52:07',2,2,28);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (172,'2014-06-27 08:52:07','2014-06-27 08:52:07',3,2,28);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (173,'2014-06-27 08:52:07','2014-06-27 08:52:07',4,2,28);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (174,'2014-06-27 08:52:07','2014-06-27 08:52:07',5,2,28);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (175,'2014-06-27 08:52:07','2014-06-27 08:52:07',6,2,28);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (176,'2014-06-27 08:52:07','2014-06-27 08:52:07',7,2,28);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (177,'2014-06-27 08:52:07','2014-06-27 08:52:07',8,2,28);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (178,'2014-06-27 08:52:07','2014-06-27 08:52:07',9,2,28);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (179,'2014-06-27 08:52:07','2014-06-27 08:52:07',10,2,28);
					insert  into `temp_studentsubjects`(`id`,`created_at`,`updated_at`,`subject_id`,`block_system_setting_id`,`temp_start_id`) values (180,'2014-06-27 08:52:07','2014-06-27 08:52:07',11,2,28);

					/*Table structure for table `time_schedules` */

					DROP TABLE IF EXISTS `time_schedules`;

					CREATE TABLE `time_schedules` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `from` varchar(255) DEFAULT NULL,
					  `to` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `time_schedules` */

					/*Table structure for table `time_slots` */

					DROP TABLE IF EXISTS `time_slots`;

					CREATE TABLE `time_slots` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `room` varchar(255) DEFAULT NULL,
					  `day` varchar(255) DEFAULT NULL,
					  `time_schedule_from` varchar(255) DEFAULT NULL,
					  `time_schedule_to` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `subject` varchar(255) DEFAULT NULL,
					  `teacher` varchar(255) DEFAULT NULL,
					  `room_id` int(11) DEFAULT NULL,
					  `room_name` varchar(255) DEFAULT NULL,
					  `year` varchar(255) DEFAULT NULL,
					  `semester` varchar(255) DEFAULT NULL,
					  `course` varchar(255) DEFAULT NULL,
					  `year_id` int(11) DEFAULT NULL,
					  `semester_id` int(11) DEFAULT NULL,
					  `course_id` int(11) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `time_slots` */

					/*Table structure for table `time_test` */

					DROP TABLE IF EXISTS `time_test`;

					CREATE TABLE `time_test` (
					  `class_start` time DEFAULT NULL,
					  `class_end` time DEFAULT NULL,
					  `day` varchar(255) DEFAULT NULL
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;

					/*Data for the table `time_test` */

					/*Table structure for table `training_programs` */

					DROP TABLE IF EXISTS `training_programs`;

					CREATE TABLE `training_programs` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `title` varchar(255) DEFAULT NULL,
					  `from` varchar(255) DEFAULT NULL,
					  `to` varchar(255) DEFAULT NULL,
					  `conducted` varchar(255) DEFAULT NULL,
					  `employee_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

					/*Data for the table `training_programs` */

					insert  into `training_programs`(`id`,`title`,`from`,`to`,`conducted`,`employee_id`,`created_at`,`updated_at`) values (1,'reg','reg','reg','reg',3,'2014-01-22 15:24:13','2014-01-22 15:24:13');
					insert  into `training_programs`(`id`,`title`,`from`,`to`,`conducted`,`employee_id`,`created_at`,`updated_at`) values (2,'reg','reg','reg','reg',3,'2014-01-22 15:24:13','2014-01-22 15:24:13');
					insert  into `training_programs`(`id`,`title`,`from`,`to`,`conducted`,`employee_id`,`created_at`,`updated_at`) values (3,'reg','reg','reg','reg',3,'2014-01-22 15:24:13','2014-01-22 15:24:13');
					insert  into `training_programs`(`id`,`title`,`from`,`to`,`conducted`,`employee_id`,`created_at`,`updated_at`) values (4,'reg','reg','reg','reg',3,'2014-01-22 15:24:13','2014-01-22 15:24:13');
					insert  into `training_programs`(`id`,`title`,`from`,`to`,`conducted`,`employee_id`,`created_at`,`updated_at`) values (5,'reg','reg','reg','reg',3,'2014-01-22 15:24:13','2014-01-22 15:24:13');

					/*Table structure for table `transcripts` */

					DROP TABLE IF EXISTS `transcripts`;

					CREATE TABLE `transcripts` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `year_id` int(11) DEFAULT NULL,
					  `semester_id` int(11) DEFAULT NULL,
					  `course_id` int(11) DEFAULT NULL,
					  `enrollment_id` int(11) DEFAULT NULL,
					  `subject_id` int(11) DEFAULT NULL,
					  `final_grade` float(11,2) DEFAULT '0.00',
					  `earned_units` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `transcripts` */

					/*Table structure for table `user_tracks` */

					DROP TABLE IF EXISTS `user_tracks`;

					CREATE TABLE `user_tracks` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `url` varchar(255) DEFAULT NULL,
					  `user_id` int(11) DEFAULT NULL,
					  `track_controller` varchar(255) DEFAULT NULL,
					  `track_model` varchar(255) DEFAULT NULL,
					  `track_action` varchar(255) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;

					/*Data for the table `user_tracks` */

					/*Table structure for table `users` */

					DROP TABLE IF EXISTS `users`;

					CREATE TABLE `users` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `login` varchar(40) DEFAULT NULL,
					  `name` varchar(100) DEFAULT '',
					  `email` varchar(100) DEFAULT NULL,
					  `crypted_password` varchar(40) DEFAULT NULL,
					  `salt` varchar(40) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `remember_token` varchar(40) DEFAULT NULL,
					  `remember_token_expires_at` datetime DEFAULT NULL,
					  `activation_code` varchar(40) DEFAULT NULL,
					  `activated_at` datetime DEFAULT NULL,
					  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
					  `admin` int(11) NOT NULL DEFAULT '0',
					  `student` int(11) NOT NULL DEFAULT '0',
					  `employee` int(11) NOT NULL DEFAULT '0',
					  `employee_id` int(11) DEFAULT NULL,
					  `registrar` tinyint(1) NOT NULL DEFAULT '0',
					  `finance` tinyint(1) NOT NULL DEFAULT '0',
					  `teacher` tinyint(1) NOT NULL DEFAULT '0',
					  `custodian` tinyint(1) DEFAULT '0',
					  `dean` tinyint(1) NOT NULL DEFAULT '0',
					  `hrd` tinyint(1) NOT NULL DEFAULT '0',
					  `president` tinyint(1) NOT NULL DEFAULT '0',
					  `librarian` tinyint(1) DEFAULT '0',
					  `cashier` int(11) DEFAULT '0',
					  `guidance` int(11) NOT NULL DEFAULT '0',
					  `vp_assistant` int(11) DEFAULT '0',
					  `assistant_to_the_president` int(11) DEFAULT '0',
					  `department` varchar(100) DEFAULT NULL,
					  PRIMARY KEY (`id`),
					  UNIQUE KEY `index_users_on_login` (`login`)
					) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

					/*Data for the table `users` */

					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (1,'support','admin','support@gmail.com','7b2212787a2af21f6239fc579f6975f75a809a35','4f4298e32eb2adebc3fffb6a952420070ee4834f','2011-04-08 07:53:40','2013-11-17 17:53:04',NULL,NULL,NULL,'2011-05-04 02:42:46',1,1,0,0,NULL,0,0,0,NULL,0,0,0,0,NULL,0,0,0,'admin');
					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (2,'hrd','hrd','hrd@gmail.com','534be876e5bcce565b58e01f49af6881652c59ef','ed6038fe4bfe2d9f921cebcaa97ee48022a41294','2011-04-12 06:55:25','2012-03-07 01:57:53',NULL,NULL,NULL,'2011-05-04 02:42:46',1,0,0,1,NULL,0,0,0,NULL,0,1,0,NULL,NULL,0,0,0,'hrd');
					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (3,'regisrar','Registrar R Registrar','','30f12504ce291c35de21c7e3c04409eacd5c33c6','41e0ed5c771bcfb534195610e3739f8bc9a917dd','2014-01-14 17:26:24','2014-04-28 05:57:20',NULL,NULL,NULL,NULL,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,'registrar');
					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (4,'finance','Finance F Finance','','af2497e90a3dd7293fcaf0949eb37170259f779d','a5a412331f0e3ebc2e8923e8641b961965bd8643','2014-01-14 17:50:05','2014-01-14 17:50:05',NULL,NULL,NULL,NULL,1,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,'finance');
					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (5,'test2','test2, test2 test2','test2','e9f7cc5843ac625b4ed6ecde280132a133533681','ae453700d0fd8c193379880a1bfe5427ac6808fd','2014-01-14 18:32:24','2014-01-14 18:32:24',NULL,NULL,NULL,NULL,1,0,0,0,NULL,0,0,0,0,0,0,0,0,0,0,0,0,'student');
					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (6,'20142478','Raven, Mark I','','1888cddc7a6f8e02ac397ff5467b136f5b9496f8','9e6d621e7039baddffa34b404deea8fa348dbf1c','2014-01-14 18:35:57','2014-01-14 18:35:57',NULL,NULL,NULL,NULL,1,0,0,0,NULL,0,0,0,0,0,0,0,0,0,0,0,0,'student');
					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (7,'registree','reg reg reg','reg','10ea3a478643d9e6365fad8c32e0b15c62057745','6a155ebf6ab5042ed1f98559b111254088d38e35','2014-01-22 15:24:13','2014-01-22 15:24:13',NULL,NULL,NULL,NULL,1,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,'registrar');
					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (8,'pedro','pedro, pedro pedro','pedro','06ca5288093222b4e0abfaa946f6b1c5a42f184d','f149ac24a0c18d46ff64044897fb63fc7cd6fc05','2014-01-22 15:33:13','2014-01-22 15:33:13',NULL,NULL,NULL,NULL,1,0,0,0,NULL,0,0,0,0,0,0,0,0,0,0,0,0,'student');
					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (9,'00000001','sly, sly sly','sly','67f0b53a11f5f0d5a5e309a37d751ae251fb201f','68f2bce56fac5f5606d76206fde2485929d58ade','2014-01-24 11:15:03','2014-01-24 11:15:03',NULL,NULL,NULL,NULL,1,0,0,0,NULL,0,0,0,0,0,0,0,0,0,0,0,0,'student');
					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (10,'cashier','cashier m cashier','','a8282be7ee60ce75abb784e7a46729a773ad47be','f609b23514ee18208a86109e266a5ed7f9ed1c2e','2014-01-29 10:24:12','2014-01-29 13:07:01',NULL,NULL,NULL,NULL,1,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,'cashier');
					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (11,'studentaffairs','students a affairs','','b61c74d2159e9057523a4fc0b386631ebf16740c','c22cfef22a2c06f746ade45e31553e323b20664a','2014-01-29 14:02:15','2014-01-29 14:02:15',NULL,NULL,NULL,NULL,1,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,'student_affairs');
					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (12,'finance-1','--- --- ---','','2726f3c071660288c0a7b4dc6f7dbf45495c1578','a8f1a3d855949a4b1df325033d56f90bb24c917a','2014-01-29 14:28:16','2014-01-29 14:28:16',NULL,NULL,NULL,NULL,1,0,0,0,6,0,0,0,0,0,0,0,0,0,0,0,0,'finance');
					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (13,'00000002','tester, tester tester','tester','051e1d2d25a3f05c4b1995be46785225d4aed414','234a294d2f6b7c96ac5cae66f53c7ccaa3f58885','2014-03-31 10:22:02','2014-03-31 10:24:12',NULL,NULL,NULL,NULL,1,0,0,0,NULL,0,0,0,0,0,0,0,0,0,0,0,0,'student');
					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (14,'registrar','Registrar R Registrar','','f24a9666b482f4a437902ceddf739d2e912f6557','ca367470da2f2bad959015f23ba2708a5f2832ce','2014-04-28 06:02:20','2014-04-28 06:02:20',NULL,NULL,NULL,NULL,1,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,'registrar');
					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (15,'teachermakiling','maria m makiling','','e936e21b34e7d67b60692b0af890ccbc3d2228c3','75f6e4949c5b67662fb4cabb29af0c5839681c21','2014-04-28 06:06:36','2014-04-28 06:06:36',NULL,NULL,NULL,NULL,1,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,'teacher');
					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (16,'00000003','Lagasca, Mark Raven R','mark@yahoo.com','cfd30d1d435bdd6812b49115334e4fe0834eec44','1fc4cad59f1a173ff11726f6cc28a8bf48de2839','2014-04-28 12:51:08','2014-04-28 12:51:08',NULL,NULL,NULL,NULL,1,0,0,0,NULL,0,0,0,0,0,0,0,0,0,0,0,0,'student');
					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (17,'00000006','Mayao, Aldren M','','9fac51514b5118c98fca623112c5a2ddf51bf8ee','38e1f6349bae756c08ebfa81b9283f2bc3c5fc52','2014-06-20 19:52:36','2014-06-20 19:52:36',NULL,NULL,NULL,NULL,1,0,0,0,NULL,0,0,0,0,0,0,0,0,0,0,0,0,'student');
					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (18,'00000007','aaron, aaron aaron','aaron','6ca698dba1613a130a7fa4fd36e03e81a4204a88','6461259c6145ad068c150e5ce0281b9d409464e9','2014-06-21 16:00:46','2014-06-21 16:00:46',NULL,NULL,NULL,NULL,1,0,0,0,NULL,0,0,0,0,0,0,0,0,0,0,0,0,'student');
					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (21,'00000011','Guevarra, Dennis D','dennis@yahoo.com','8594d49a8bcd47bc046c0221e380eb826dd4a4b8','6f4d5f8e19c8025d214fc39d5e703a5423252794','2014-06-26 23:12:29','2014-06-26 23:12:29',NULL,NULL,NULL,NULL,1,0,0,0,NULL,0,0,0,0,0,0,0,0,0,0,0,0,'student');
					insert  into `users`(`id`,`login`,`name`,`email`,`crypted_password`,`salt`,`created_at`,`updated_at`,`remember_token`,`remember_token_expires_at`,`activation_code`,`activated_at`,`is_activated`,`admin`,`student`,`employee`,`employee_id`,`registrar`,`finance`,`teacher`,`custodian`,`dean`,`hrd`,`president`,`librarian`,`cashier`,`guidance`,`vp_assistant`,`assistant_to_the_president`,`department`) values (22,'admin','I Am Admin','admin@gmail.com','57ad27ed2c65125e55f1f986fbf922ee7ed3481d','7b2cf28cf2459cc3e268b395c95be334222a1605','2014-07-10 10:32:43','2014-07-10 10:32:43',NULL,NULL,NULL,'2014-07-10 10:36:40',1,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,'admin');

					/*Table structure for table `voluntary_works` */

					DROP TABLE IF EXISTS `voluntary_works`;

					CREATE TABLE `voluntary_works` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `name` varchar(255) DEFAULT NULL,
					  `address` varchar(255) DEFAULT NULL,
					  `employee_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

					/*Data for the table `voluntary_works` */

					insert  into `voluntary_works`(`id`,`name`,`address`,`employee_id`,`created_at`,`updated_at`) values (1,'reg','reg',3,'2014-01-22 15:24:13','2014-01-22 15:24:13');
					insert  into `voluntary_works`(`id`,`name`,`address`,`employee_id`,`created_at`,`updated_at`) values (2,'reg','reg',3,'2014-01-22 15:24:13','2014-01-22 15:24:13');
					insert  into `voluntary_works`(`id`,`name`,`address`,`employee_id`,`created_at`,`updated_at`) values (3,'reg','reg',3,'2014-01-22 15:24:13','2014-01-22 15:24:13');
					insert  into `voluntary_works`(`id`,`name`,`address`,`employee_id`,`created_at`,`updated_at`) values (4,'reg','reg',3,'2014-01-22 15:24:13','2014-01-22 15:24:13');
					insert  into `voluntary_works`(`id`,`name`,`address`,`employee_id`,`created_at`,`updated_at`) values (5,'reg','reg',3,'2014-01-22 15:24:13','2014-01-22 15:24:13');

					/*Table structure for table `work_experiences` */

					DROP TABLE IF EXISTS `work_experiences`;

					CREATE TABLE `work_experiences` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `from` varchar(255) DEFAULT NULL,
					  `to` varchar(255) DEFAULT NULL,
					  `positioin` varchar(255) DEFAULT NULL,
					  `agency` varchar(255) DEFAULT NULL,
					  `employee_id` int(11) DEFAULT NULL,
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  `position` varchar(255) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

					/*Data for the table `work_experiences` */

					insert  into `work_experiences`(`id`,`from`,`to`,`positioin`,`agency`,`employee_id`,`created_at`,`updated_at`,`position`) values (1,'reg','reg',NULL,'reg',3,'2014-01-22 15:24:13','2014-01-22 15:24:13','reg');
					insert  into `work_experiences`(`id`,`from`,`to`,`positioin`,`agency`,`employee_id`,`created_at`,`updated_at`,`position`) values (2,'reg','reg',NULL,'reg',3,'2014-01-22 15:24:13','2014-01-22 15:24:13','reg');
					insert  into `work_experiences`(`id`,`from`,`to`,`positioin`,`agency`,`employee_id`,`created_at`,`updated_at`,`position`) values (3,'reg','reg',NULL,'reg',3,'2014-01-22 15:24:13','2014-01-22 15:24:13','reg');
					insert  into `work_experiences`(`id`,`from`,`to`,`positioin`,`agency`,`employee_id`,`created_at`,`updated_at`,`position`) values (4,'reg','reg',NULL,'reg',3,'2014-01-22 15:24:13','2014-01-22 15:24:13','reg');
					insert  into `work_experiences`(`id`,`from`,`to`,`positioin`,`agency`,`employee_id`,`created_at`,`updated_at`,`position`) values (5,'reg','reg',NULL,'reg',3,'2014-01-22 15:24:13','2014-01-22 15:24:13','reg');

					/*Table structure for table `years` */

					DROP TABLE IF EXISTS `years`;

					CREATE TABLE `years` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `year` varchar(255) DEFAULT NULL,
					  `level` int(2) DEFAULT '1',
					  `created_at` datetime DEFAULT NULL,
					  `updated_at` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

					/*Data for the table `years` */

					insert  into `years`(`id`,`year`,`level`,`created_at`,`updated_at`) values (3,'1st Year',1,'2014-01-14 10:47:26','2014-01-14 10:47:26');
					insert  into `years`(`id`,`year`,`level`,`created_at`,`updated_at`) values (4,'Second Year',2,'2014-07-02 17:41:34','2014-07-02 17:41:34');
					insert  into `years`(`id`,`year`,`level`,`created_at`,`updated_at`) values (5,'Third Year',3,'2014-07-02 17:41:51','2014-07-02 17:41:51');
					insert  into `years`(`id`,`year`,`level`,`created_at`,`updated_at`) values (6,'Fourth Year',4,'2014-07-02 17:42:05','2014-07-02 17:42:05');

					/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
					/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
					/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
";
	}
?>