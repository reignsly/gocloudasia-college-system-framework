<?php
/*
	Count Confirm Enrollees
 */
	function count_confirm_enrollees(){

		$ci =& get_instance();
		$ci->load->model('M_temp_enrollments');
		$cos = $ci->cos;
		
		$count = 0;

		$sql ="SELECT count(id) as confirmed
			FROM (`temp_enrollments`)
			WHERE `temp_enrollments`.`semester_id` =  ?
			AND `temp_enrollments`.`sy_from` =  ?
			AND `temp_enrollments`.`sy_to` =  ?
			AND `temp_enrollments`.`is_deleted` =  0
			AND `temp_enrollments`.`status` =  'new'
			AND `temp_enrollments`.`confirm_status` =  'PENDING'
			ORDER BY `temp_enrollments`.`name`";
		$data[] = $cos->user->semester_id;
		$data[] = $cos->user->year_from;
		$data[] = $cos->user->year_to;
		$q = $ci->db->query($sql, $data);
		
		if($q->num_rows() > 0){
			$rs = $q->row();
			$count = $rs->confirmed;
		}

		return $count;
	}

	function count_list_confirm_enrollees(){

		$ci =& get_instance();
		$ci->load->model('M_temp_enrollments');
		$cos = $ci->cos;
		
		$count = 0;

		$sql ="SELECT count(id) as confirmed
			FROM (`temp_enrollments`)
			WHERE `temp_enrollments`.`semester_id` =  ?
			AND `temp_enrollments`.`sy_from` =  ?
			AND `temp_enrollments`.`sy_to` =  ?
			AND `temp_enrollments`.`is_deleted` =  0
			AND `temp_enrollments`.`confirm_status` =  'CONFIRM'
			ORDER BY `temp_enrollments`.`name`";
		$data[] = $cos->user->semester_id;
		$data[] = $cos->user->year_from;
		$data[] = $cos->user->year_to;
		$q = $ci->db->query($sql, $data);
		
		if($q->num_rows() > 0){
			$rs = $q->row();
			$count = $rs->confirmed;
		}

		return $count;
	}

	function count_enrollees(){

		$ci =& get_instance();
		$ci->load->model('M_temp_enrollments');
		$cos = $ci->cos;
		
		$count = 0;

		$sql = "SELECT 
					count(id) as enroll_count
				FROM (`enrollments`)
				WHERE `enrollments`.`semester_id` =  ?
				AND `enrollments`.`sy_from` =  ?
				AND `enrollments`.`sy_to` =  ?
				AND `enrollments`.`is_deleted` =  0
				AND `enrollments`.`is_paid` =  0";

		if($cos->enrollment){


			$data[] = $cos->user->semester_id;
			$data[] = $cos->user->year_from;
			$data[] = $cos->user->year_to;
			$q = $ci->db->query($sql, $data);
			
			if($q->num_rows() > 0){
				$rs = $q->row();
				$count = $rs->enroll_count;
			}
		}
		
		
		return $count;
	}

	/**
	 * Get Subject Prerequisite by curriculum id and subject id
	 * @param  int $curriculum_id curriculum table id
	 * @param  int $subject_id    subjects table id
	 * @return object               
	 */	
	function get_subject_prerequisite($curriculum_id, $subject_id)
	{
		$ci =& get_instance();
		$ci->load->model('M_subject_pre_requisite','m');
		return $this->m->get_pre_requisite_by_subject_id($curriculum_id, $subject_id);
	}

	function count_official_enrollees(){

		$ci =& get_instance();
		$ci->load->model('M_temp_enrollments');
		$cos = $ci->cos;
		
		$count = 0;

		$sql = "SELECT 
					count(id) as enroll_count
				FROM (`enrollments`)
				WHERE `enrollments`.`semester_id` =  ?
				AND `enrollments`.`sy_from` =  ?
				AND `enrollments`.`sy_to` =  ?
				AND `enrollments`.`is_deleted` =  0
				AND `enrollments`.`is_paid` =  1";

		if($cos->user){


			$data[] = $cos->user->semester_id;
			$data[] = $cos->user->year_from;
			$data[] = $cos->user->year_to;
			$q = $ci->db->query($sql, $data);
			
			if($q->num_rows() > 0){
				$rs = $q->row();
				$count = $rs->enroll_count;
			}
		}
		return $count;
	}