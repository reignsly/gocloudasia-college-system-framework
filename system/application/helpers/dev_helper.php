<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
     
    if(!function_exists('pd'))
    {
            function pd($d)
            {
                    echo '<pre>';
                            print_r($d);
                    echo '</pre>';
                    die();
            }
    }
     
    if(!function_exists('vd'))
    {
            function vd($d)
            {
                    echo '<pre>';
                            var_dump($d);
                    echo '</pre>';
                    die();
            }
    }

    /**
     * uses util.php library
     */
    if(!function_exists('ud'))
    {
            function ud($d)
            {
                    echo '<pre>';
                            util::var_dump($d);
                    echo '</pre>';
                    die();
            }
    }

    /**
     * uses util.php library
     */
    if(!function_exists('up'))
    {
            function up($d)
            {
                    echo '<pre>';
                            util::var_dump($d);
                    echo '</pre>';
            }
    }
     
    if(!function_exists('pp'))
    {
            function pp($d)
            {
                    echo '<pre>';
                            print_r($d);
                    echo '</pre>';
            }
    }
     
    if(!function_exists('vp'))
    {
            function vp($d, $clr = false)
            {
					if($clr){
						echo '<div class="well"></div>';
					}
                    echo '<pre>';
                            print_r($d);
                    echo '</pre>';
            }
    }

    if(!function_exists('object_to_view_data'))
    {
        function object_to_view_data($bato)
        {
            $CI=& get_instance();
            
            if($bato){
                foreach ($bato as $key => $value) {
                    $CI->view_data[$key] = $value;
                }
            }
        }
    }

    if(!function_exists('iset'))
    {
        function iset($value)
        {
            return isset($value)?$value:"";
        }
    }

    /**
     * Convert string to lower and UCwords
     */
    if(!function_exists('uc_words'))
    {
        function uc_words($value)
        {
            return ucwords(strtolower($value));
        }
    }