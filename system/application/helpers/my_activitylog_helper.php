<?php

	/** 
	 * @action add or edit or delete or others
	 * @user Username of logged in
	 * @remarks should contain id of add or delete or edit or others
	 */
	function activity_log($action="", $user = false, $remarks = "") {
		
		$ci =& get_instance();
		$ci->load->model('M_activity_log');
		$ci->load->helper('url');
		
		$activity['created_at'] = NOW;
		$activity['updated_at'] = NOW;
		$activity['action'] = $action;
		$activity['user'] = $user === false ? isset($ci->session->userdata['userlogin']) ? $ci->session->userdata['userlogin'] : '' : $user;
		$activity['remarks'] = $remarks;
		$activity['url'] = current_url();
		
		$rs = $ci->M_activity_log->create_log($activity);
	}

	function arr_str($data = array())
	{
		$ret = "";
		if(is_array($data) || is_object($data)){
			foreach ($data as $key => $value) {
				if($ret != ""){
					$ret .= " - ";
				}
				$ret .= "[".$key.":".$value."]";
			}
		}
		return $ret;
	}