<?php
//require APPPATH.DIRECTORY_SEPARATOR.'extensions'.DIRECTORY_SEPARATOR.'enrollment_validation_config'.EXT;

/*
		'name_of_field' => array(										
							'field' => '',
							'label' => '',
							'rules' => ''
				
		)
*/

$config = array(
		'grading_system' => array(
			array(
				'field' => 'grading_system[range_start]',
				'label' => 'Range From',
				'rules' => 'required|trim|float'
			),
			array(
				'field' => 'grading_system[range_end]',
				'label' => 'Range To',
				'rules' => 'required|trim|float'
			),
			array(
				'field' => 'grading_system[value]',
				'label' => 'Value',
				'rules' => 'required|trim'
			)
		),
		'packages' => array(
			array(
				'field' => 'package[package]',
				'label' => 'Package Name',
				'rules' => 'required|trim'
			),
			array(
				'field' => 'package[remarks]',
				'label' => 'Remarks',
				'rules' => 'required|trim'
			),
			array(
				'field' => 'package[level]',
				'label' => 'Package Level',
				'rules' => 'required|trim|integer'
			)
		),
		'packages_add_menu' => array(
			array(
				'field' => 'department_id',
				'label' => 'Department',
				'rules' => 'required|trim'
			)
		),
		'curriculum' => array(
						array(
							'field' => 'name',
							'label' => 'curriculum Name',
							'rules' => 'required|trim'
						),
						array(
							'field' => 'course_id',
							'label' => 'Course',
							'rules' => 'required'
						)
		),
		'curriculum_add_subjects' => array(
						array(
							'field' => 'year_id',
							'label' => 'Year',
							'rules' => 'required|trim'
						),
						array(
							'field' => 'semester_id',
							'label' => 'Semester',
							'rules' => 'required'
						)
		),
		'curriculum_add_subjects2' => array(
						array(
							'field' => 'subject_id',
							'label' => 'Subject',
							'rules' => 'required|trim'
						),
						array(
							'field' => 'year_id',
							'label' => 'Year',
							'rules' => 'required|trim'
						),
						array(
							'field' => 'semester_id',
							'label' => 'Semester',
							'rules' => 'required'
						)
		),
		'curriculum_add_subjects3' => array(
						array(
							'field' => 'year_id',
							'label' => 'Year',
							'rules' => 'required|trim'
						),
						array(
							'field' => 'semester_id',
							'label' => 'Semester',
							'rules' => 'required'
						)
		),
		'update_payment_record' => array(
						array(
							'field' => 'spr_or_no',
							'label' => 'OR Number',
							'rules' => 'required|trim|alpha_dash|is_unik_edit[student_payment_records.spr_or_no]'
						),
						array(
							'field' => 'date_of_payment',
							'label' => 'Date of Payment',
							'rules' => 'required'
						),
						array(
							'field' => 'remarks',
							'label' => 'Remarks for payment',
							'rules' => 'required'
						)
		),
		'add_payment_record' => array(
				array(
										
									'field' => 'ammount_paid',
									'label' => 'Amount',
									'rules' => 'required|numeric'
				
				),
				array(
										
									'field' => 'spr_or_no',
									'label' => 'O.R. Number',
									'rules' => 'required|trim'
				
				)
				,array(
										
									'field' => 'date_of_payment',
									'label' => 'Payment Date',
									'rules' => 'required|trim'
				
				),array(
										
									'field' => 'payment_type',
									'label' => 'Payment Type',
									'rules' => 'required|trim'
				
				)
		),
		'new_message' => array(
				array(
										
									'field' => 'recipient_id',
									'label' => 'Receipt',
									'rules' => 'required'
				
				),
				array(
										
									'field' => 'message',
									'label' => 'Message Content',
									'rules' => 'required|trim'
				
				)
		),
		'main_menus' => array(
				array(
										
									'field' => 'controller',
									'label' => 'Controller',
									'rules' => 'required'
				
				),
				array(
										
									'field' => 'caption',
									'label' => 'Caption',
									'rules' => 'required|trim'
				
				)
		),
		'student_deduction_record' => array(
				array(
										
									'field' => 'deduction_name',
									'label' => 'Deduction Name',
									'rules' => 'required'
				
				),
				array(
										
									'field' => 'remarks',
									'label' => 'Remarks',
									'rules' => 'required|trim'
				
				),
				array(
														
									'field' => 'amount',
									'label' => 'Deduction Amount',
									'rules' => 'required|trim|numeric'
								
				)
		),
		'student_scholarship' => array(
			      array(
							'field' => 'name',
							'label' => 'Name',
							'rules' => 'required'
						),
						array(
							'field' => 'desc',
							'label' => 'Description',
							'rules' => 'required'
						)
		),
		'update_student_profile' => array(
				array(
										
									'field' => 'fname',
									'label' => '',
									'rules' => 'required'
				
			),
				array(
										
									'field' => 'lastname',
									'label' => '',
									'rules' => 'required'
				
				),
				array(
										
									'field' => 'middle',
									'label' => '',
									'rules' => 'required'
				
				),
				array(
										
									'field' => 'gender',
									'label' => '',
									'rules' => 'required'
				
				),
				array(
										
									'field' => 'civil_status',
									'label' => '',
									'rules' => 'required'
				
				),
				array(
										
									'field' => 'date_of_birth',
									'label' => '',
									'rules' => 'required'
				
				),
				array(
										
									'field' => 'place_of_birth',
									'label' => '',
									'rules' => 'required'
				
				)
		),
		'record_payment' => array(
			array(
				'field' => 'receipt_number',
				'label' => 'Receipt number',
				'rules' => 'required|is_unique[studentpayments.or_no]'
			),
			array(
				'field' => 'date_of_payment',
				'label' => 'Date of payment',
				'rules' => 'required'
			)
		),
		'departments' => array(
			array(
				'field' => 'departments[department]',
				'label' => 'Code',
				'rules' => 'required|is_unique[departments.department]'
			),
			array(
				'field' => 'departments[description]',
				'label' => 'Description',
				'rules' => 'required'
			)
		),
		'departments2' => array(
			array(
				'field' => 'departments[description]',
				'label' => 'Description',
				'rules' => 'required'
			)
		),
		'edit_menu_1' => array(
			array(
				'field' => 'menu_group[caption]',
				'label' => 'Caption',
				'rules' => 'required'
			),
			array(
				'field' => 'menu_group[menu_num]',
				'label' => 'Order',
				'rules' => 'required|numeric'
			)
		),
		'record_payment2' => array(
			array(
				'field' => 'receipt_number',
				'label' => 'Receipt number',
				'rules' => 'required|is_unique[studentpayments.or_no]'
			),
			array(
				'field' => 'studid',
				'label' => 'Student ID Number',
				'rules' => 'required|is_existing'
			),
			array(
				'field' => 'date_of_payment',
				'label' => 'Date of payment',
				'rules' => 'required'
			)
		),
		'promissory_note' => array(
			array(
				'field' => 'value',
				'label' => 'Value',
				'rules' => 'required'
			),
			array(
				'field' => 'date',
				'label' => 'Date',
				'rules' => 'required'
			)
		),
		'courses' => array(
			array(
				'field' => 'courses[course_code]',
				'label' => 'Code',
				'rules' => 'required|trim'
			),
			array(
				'field' => 'courses[course]',
				'label' => 'Course',
				'rules' => 'required|trim'
			)
		),
		'coursefinances' => array(
			array(
				'field' => 'coursefinances[category]',
				'label' => 'Category',
				'rules' => 'required',
			),
			array(
				'field' => 'coursefinances[code]',
				'label' => 'Code',
				'rules' => 'required',
			),
		),
		'coursefinances_create_fees' => array(
			array(
				'field' => 'coursefees[code]',
				'label' => 'Value',
				'rules' => 'numeric',
			),
		),
		
		'borrow_items' => array(
			array(
				'field' => 'borrow_items[date]',
				'label' => 'Date',
				'rules' => 'required',
			),
			array(
				'field' => 'borrow_items[user_id]',
				'label' => 'Borrowers',
				'rules' => 'required',
			),
			array(
				'field' => 'borrow_items[item_id]',
				'label' => 'Item',
				'rules' => 'required',
			),
			array(
				'field' => 'borrow_items[unit]',
				'label' => 'Quantity',
				'rules' => 'required|numeric',
			),
		),
		
		'master_subjects' => array(
			array(
				'field' => 'subjects[code]',
				'label' => 'Course No.',
				'rules' => 'required|trim|is_unique[master_subjects.code]',
			),
			array(
				'field' => 'subjects[subject]',
				'label' => 'Description',
				'rules' => 'required|trim',
			),
			array(
				'field' => 'subjects[units]',
				'label' => 'No. of Units',
				'rules' => 'required|trim|numeric',
			)
		),
		'subjects' => array(
			array(
				'field' => 'code',
				'label' => 'Course No.',
				'rules' => 'required|trim',
			),
			array(
				'field' => 'subject',
				'label' => 'Description',
				'rules' => 'required|trim',
			),
			array(
				'field' => 'units',
				'label' => 'No. of Units',
				'rules' => 'required|trim|numeric',
			),
			array(
				'field' => 'subjects[time]',
				'label' => 'Time',
				'rules' => 'required|trim',
			),
			array(
				'field' => 'subjects[original_load]',
				'label' => 'Maximum Slots',
				'rules' => 'required|trim|numeric',
			),
		),
		'users' => array(
			array(
				'field' => 'user[employees_attributes][personal_info][employeeid]',
				'label' => 'Employee ID',
				'rules' => 'required|trim',
			),
			array(
				'field' => 'user[employees_attributes][personal_info][last_name]',
				'label' => 'Last Name',
				'rules' => 'required|trim',
			),
			array(
				'field' => 'user[employees_attributes][personal_info][first_name]',
				'label' => 'First Name',
				'rules' => 'required|trim',
			),
			array(
				'field' => 'user[employees_attributes][personal_info][middle_name]',
				'label' => 'Middle Name',
				'rules' => 'required|trim',
			),
		),
		'users_edit' => array(
			array(
				'field' => 'user[employees_attributes][personal_info][last_name]',
				'label' => 'Last Name',
				'rules' => 'required|trim',
			),
			array(
				'field' => 'user[employees_attributes][personal_info][first_name]',
				'label' => 'First Name',
				'rules' => 'required|trim',
			),
			array(
				'field' => 'user[employees_attributes][personal_info][middle_name]',
				'label' => 'Middle Name',
				'rules' => 'required|trim',
			),
		),
		'enrollment' => array(
				array(
					'field' => 'year',
					'label' => 'year',
					'rules' => 'required'
				),
				array(
					'field' => 'course',
					'label' => 'course',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[last_name]',
					'label' => 'Lastname',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[first_name]',
					'label' => 'Firstname',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[middle_name]',
					'label' => 'Middle name',
					'rules' => 'required'
				),
				array(
					'field' => 'course',
					'label' => 'Course',
					'rules' => 'required'
				),
				array(
					'field' => 'year',
					'label' => 'Year',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[gender]',
					'label' => 'Gender',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[civil_status]',
					'label' => 'Civil Status',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[date_of_birth]',
					'label' => 'Date of Birth',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[place_of_birth]',
					'label' => 'Birth Place',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[age]',
					'label' => 'Age',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[disablity]',
					'label' => '',
					'rules' => ''
				),
				array(
					'field' => 'profile[nationality]',
					'label' => '',
					'rules' => ''
				),
				array(
					'field' => 'profile[religion]',
					'label' => 'Religion',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[mobile]',
					'label' => 'Mobile',
					'rules' => 'integer'
				),
				array(
					'field' => 'profile[email]',
					'label' => '',
					'rules' => 'valid_email'
				),
				array(
					'field' => 'profile[present_address]',
					'label' => 'Present Address',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[father_name]',
					'label' => 'Father\'s Name',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[father_occupation]',
					'label' => 'Father\'s Occupation',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[father_contact_no]',
					'label' => 'Father\'s Contact Number',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[mother_name]',
					'label' => 'Mother\'s name',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[mother_contact_no]',
					'label' => 'Mother\'s Contact Number',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[mother_occupation]',
					'label' => 'Mother\'s Occupation',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[parents_address]',
					'label' => 'Parent\'s Address',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[guardian_name]',
					'label' => 'Guardian\'s name',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[guardian_relation]',
					'label' => 'Relation with Guardian',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[guardian_contact_no]',
					'label' => 'Contact No. of Guardian',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[guardian_address]',
					'label' => 'Guardian\'s Address',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[elementary]',
					'label' => 'Elementary School Name',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[elementary_address]',
					'label' => 'Elementary School Address',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[elementary_date]',
					'label' => 'Elementary School Year Graduated/Last Attended',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[secondary]',
					'label' => 'Secondary Schoool Name',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[secondary_address]',
					'label' => 'Secondary School Address',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[secondary_date]',
					'label' => 'Secondary School Year Graduated/Last Attended',
					'rules' => 'required'
				),
				array(
					'field' => 'profile[vocational]',
					'label' => '',
					'rules' => ''
				),
				array(
					'field' => 'profile[vocational_address]',
					'label' => '',
					'rules' => ''
				),
				array(
					'field' => 'profile[vocational_degree]',
					'label' => '',
					'rules' => ''
				),
				array(
					'field' => 'profile[vocational_date]',
					'label' => '',
					'rules' => ''
				),
				array(
					'field' => 'profile[tertiary]',
					'label' => '',
					'rules' => ''
				),
				array(
					'field' => 'profile[tertiary_address]',
					'label' => '',
					'rules' => ''
				),
				array(
					'field' => 'profile[tertiary_degree]',
					'label' => '',
					'rules' => ''
				),
				array(
					'field' => 'profile[tertiary_date]',
					'label' => '',
					'rules' => ''
				),
				array(
					'field' => 'profile[others]',
					'label' => '',
					'rules' => ''
				),
				array(
					'field' => 'profile[others_address]',
					'label' => '',
					'rules' => ''
				),
				array(
					'field' => 'profile[others_degree]',
					'label' => '',
					'rules' => ''
				),
				array(
					'field' => 'profile[date]',
					'label' => '',
					'rules' => ''
				),
		),
		'mavc_enrollment' => array(
			array(
				'field' => 'user[enrollment_attributes][lastname]',
				'label' => 'Last Name',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][fname]',
				'label' => 'First Name',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][middle]',
				'label' => 'Middle Name',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][name_ext]',
				'label' => 'Name Extention (Jr)',
				'rules' => 'trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][street_no]',
				'label' => 'Street No. or House No.',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][barangay]',
				'label' => 'Barangay',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][municipal]',
				'label' => 'City or Municipality',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][province]',
				'label' => 'Province',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][date_of_birth]',
				'label' => 'Date of birth',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][place_of_birth]',
				'label' => 'Place of birth',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][sex]',
				'label' => 'Gender',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][civil_status]',
				'label' => 'Civil Status',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][mobile]',
				'label' => 'Cellphone No. or Telephone No.',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][fake_email]',
				'label' => 'Email Address',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][nationality]',
				'label' => 'Nationality',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][is_foreigner]',
				'label' => 'Is Foreigner',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][religion]',
				'label' => 'Religion',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][father_name]',
				'label' => 'Father Name',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][father_contact_no]',
				'label' => 'Father Contact No',
				'rules' => 'trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][mother_name]',
				'label' => 'Mother Name',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][mother_contact_no]',
				'label' => 'Mother Contact No.',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][guardian_name]',
				'label' => 'Guardian Name',
				'rules' => 'trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][guardian_contact_no]',
				'label' => 'Guardian Contact No.',
				'rules' => 'trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][elementary]',
				'label' => 'Elementary',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][elementary_date]',
				'label' => 'Year Graduated',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][secondary]',
				'label' => 'Secondary',
				'rules' => 'required|trim|xss_clean'
			),array(
				'field' => 'user[enrollment_attributes][secondary_date]',
				'label' => 'Year Graduated',
				'rules' => 'required|trim|xss_clean'
			),
		)
);